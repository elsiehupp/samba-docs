Samba Features added/changed (by release)
    <namespace>0</namespace>
<last_edited>2021-01-22T15:00:01Z</last_edited>
<last_editor>Fraz</last_editor>

New features in Samba by the different versions
------------------------

This page is a new try to collect the important information from the changelog in on page.
* all information in one page
* searchable in the wiki
* direct access to the bugzilla

===============================
`Samba 4.14 Features added/changed`
===============================

------------------------

===============================
`Samba 4.13 Features added/changed`
===============================

------------------------

===============================
`Samba 4.12 Features added/changed`
===============================

------------------------

===============================
`Samba 4.11 Features added/changed`
===============================

------------------------

===============================
`Samba 4.10 Features added/changed`
===============================

------------------------

===============================
`Samba 4.9 Features added/changed`
===============================

------------------------

===============================
`Samba 4.8 Features added/changed`
===============================

------------------------

===============================
`Samba 4.7 Features added/changed`
===============================

------------------------

===============================
`Samba 4.6 Features added/changed`
===============================

------------------------

===============================
`Samba 4.5 Features added/changed`
===============================

------------------------

===============================
`Samba 4.4 Features added/changed`
===============================

------------------------

===============================
`Samba 4.3 Features added/changed`
===============================

------------------------

===============================
`Samba 4.2 Features added/changed`
===============================

------------------------

===============================
`Samba 4.1 Features added/changed`
===============================

------------------------

===============================
`Samba 4.0 Features added/changed`
===============================

------------------------

===============================
`Samba 3.6 Features added/changed`
===============================

------------------------

===============================
`Samba 3.5 Features added/changed`
===============================

------------------------

===============================
`Samba 3.4 Features added/changed`
===============================

------------------------

===============================
`Samba 3.3 Features added/changed`
===============================

------------------------

===============================
`Samba 3.2 Features added/changed`
===============================

------------------------

===============================
`Samba 3.0 Features added/changed`
===============================

------------------------

New features in Samba in a summary page
------------------------

===============================
`Samba Features added/changed`
===============================

------------------------

----
`Category:Release Notes`