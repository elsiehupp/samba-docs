Setting up Samba as a Standalone Server
    <namespace>0</namespace>
<last_edited>2020-01-24T10:46:37Z</last_edited>
<last_editor>Hortimech</last_editor>
/
===============================

Introduction
===============================

In small networks, such as a home network, or to share folders on a host that is not part of a domain, you often do not want to set up an `Active_Directory_Domain_Controller|Active Directory` or `NT4_Domains|NT4 domain`.

The following documentation describes how to set up a Samba standalone server providing:
* a share that is accessible anonymously (guest access).
* a share that requires authentication against a local user database on the Samba host.

=================
Creating a Basic guest only smb.conf File
=================

The following is a minimal configuration for a Samba standalone server that only allows guest access:

 [global]
         map to guest = Bad User
         log file = /amb///
         log level = 1

 [guest]
         # This share allows anonymous (guest) access
         # without authentication!
         path = //gu///
         read only = no
         guest ok = yes
         guest only = yes

{{Imbox
| type = warning
| text = This example defines a share that is accessible without authentication. Guest shares can be a security problem. For example on a laptop that is connected to different networks, such as home, school, and work networks. Use guest shares with care and never use a guest share with authenticated users.

=================
Creating a Basic authenticated access smb.conf File
=================

The following is a minimal configuration for a Samba standalone server:

 [global]
         log file = /amb///
         log level = 1

 [demo]
         # This share requires authentication to access
         path = //de///
         read only = no

* You can set a workgroup name with ``workgroup = xxxxxxxx</ where 'xxxxxxxx' is the required name. If the parameter isn't set, the default workgroup name 'WORKGROUP' will be used.
* The log parameters are not necessary for a minimal setup. However they are useful to set the log file and increasing the log level in case of problems.
* Whilst these are only minimal smb.conf files, you can add other parameters, such as 'unix password sync = yes' to ensure the Unix & Samba passwords are kept in sync. See 'man smb.conf' for more info.

=================
Creating a Local User Account
=================

To provide authentication on a standalone host, you have to create the accounts locally on the operating system and additionally in the Samba database. By default, Samba uses the ``tdbsam</back end and stores the database in the ``/usr/loca/iva/DOOTT/de&gt/TT Opti/ a different locati/``smb.conf`` file using the ``passdb backend</code&gt/r. See the ``smb.conf 5</code&g/e for details./

* Create a ``demoUser</account on the local system:

 # useradd -M -s /gin /

* ``-M</parameter if the user requires a home directory on this host. For Samba access, the account does not require a valid shell.

* To enable the ``demoUser</account on the local system:

 # passwd demoUser
 Enter new UNIX password:
 Retype new UNIX password:
 passwd: updated successfully

* a local password is required to enable the account. Samba denies access if the account is disabled locally. Local log ins using this password are not possible if the account was created without a valid shell.

* Add the ``demoUser</account to the Samba database:

 # smbpasswd -a demoUser
 New SMB password:
 Retype new SMB password:
 Added user demoUser.

* ord assigned in these steps is the one used by the user to log in to the domain.

=================
Local Group Management
=================

* To create a ``demoGroup</group:

 # groupadd demoGroup

* To add the ``demoUser</account to the group:

 # usermod -aG demoGroup demoUser

=================
Creating the Shared Directories
=================

To create the shares directories:

 # mkdir -p //gu///
 # mkdir -p //de///

=================
Setting ACLs on the Shared Directories
=================

Set the following POSIX permissions:

 # chgrp -R demoGroup //gu///
 # chgrp -R demoGroup //de///

 # chmod 2775 //gu///
 # chmod 2770 //de///

This configures write access to  members of the ``demoGroup</group in both directories. Other users have read access in the ``/srv/samb/t;//nd no/ the /t;/s/emo/`` directory. The SGID/SHH/ed by/ bit/>/>) in the mode set on the directories - inherits the group of the parent directory i/ting it to the users primary group when new files are created.

For further information, see `Setting_up_a_Share_Using_POSIX_ACLs|Setting up a Share Using POSIX ACLs`.

=================
Starting Samba
=================

Start the ``smbd</daemon:

 # smbd

Samba does not include start scripts. See your distribution's documentation how further information how to automatically start a service at boot time.

=================
Testing the Share Access
=================

* Access the ``demo</share as user ``demoUser``/

 # smbclient -U demoUser ///
 Enter demoUser's password:
 Domain=[WORKGROUP] OS=[Windows 6.1] Server=[Samba x.y.z]
 smb:
   .                                   D        0  Sun Jan  3 21:6:
   ..                                  D        0  Sun Jan  3 19:6:
   demo.txt                            A        0  Sun Jan  3 21:6:

 		9943040 blocks of size 1024. 7987416 blocks available
 smb:it

* Access the ``demo</share as guest. The access is denied:

 # smbclient -U guest ///
 Enter guest's password:
 Domain=[WORKGROUP] OS=[Windows 6.1] Server=[Samba x.y.z]
 tree connect failed:S_ACCESS_DENIED

=================
Advanced share settings
=================

This section describes some advanced share configuration parameters. For further information about the used parameters, see the ``smb.conf (5)</man page.

Using the ``force</Parameters 
------------------------

 [demo]
         path = //de///
         read only = no
         force create mode = 0660
         force directory mode = 2770
         force user = demoUser
         force group = demoGroup

The ``force create mode</and ``force directory mode``/s force Samba to create new files and folders with the set permissions.

The ``force user</and ``force group``/s map all connections to the specified user and group. Note that this can cause security problems if all users connecting to a share are mapped to a specific user account or group in the background.

User and Group-based Share Access 
------------------------

See `Setting_up_a_Share_Using_POSIX_ACLs#Configuring_User_and_Group-based_Share_Access|Configuring User and Group-based Share Access`.

Host-based Share Access 
------------------------

See `Setting_up_a_Share_Using_POSIX_ACLs#Configuring_Host-based_share_access|Configuring Host-based Share Access`.

----
`Category:  Server`