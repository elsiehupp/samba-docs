Reporting a Bug in the Samba Documentation
    <namespace>0</namespace>
<last_edited>2017-11-22T19:15:14Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

To report a bug in the Samba documentation, such as the Wiki or man pages:

* Log in to the Samba bug tracking system: https://bugzilla.samba.org/
* Select the ``Samba 4.1 and newer`` product.
* Select the ``Documentation`` component.
* Fill the fields and include the following details in the bug description:
* * Wiki URL or name of the man page
* * Detailed information about what is wrong

Alternatively, `Special:CreateAccount|request a Wiki account` and update the page.