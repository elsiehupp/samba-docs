Bug Reporting
    <namespace>0</namespace>
<last_edited>2020-07-27T15:57:02Z</last_edited>
<last_editor>Aaptel</last_editor>

Samba 
------------------------

* Report all suspected [https://www.samba.org/samba/security/ Security issues] to **security@samba.org** to allow us to follow our `Samba_Security_Process|Security Process`.

* Read the instructions: https://www.samba.org/~asn/reporting_samba_bugs.txt

* Report the problem in the Samba bug tracking system: https://bugzilla.samba.org/

* Always include the Samba version number in your bug reports!

.. note:

    Before you report a bug, verify that you are running a `Samba_Release_Planning|maintained version` of Samba and `Updating_Samba|update`, if necessary.<br />

cifs.ko 
------------------------

If you run into problems with the SMB client in the Linux kernel (cifs.ko, mount.cifs) don't hesitate to report it.

.. warning:

   Similarly, please report all **security** issues or defects in the kernel client or its userspace tools (cifs-utils, mount.cifs, ...) to **security@samba.org** and never on IRC, public mailing lists or in Bugzilla.

For the kernel client things are a bit more complex. There are 2 bug tracking systems which you should look at to see if your problem has already been reported:

* [https://bugzilla.samba.org/buglist.cgi?component=kernel%20fs&list_id=14383&product=CifsVFS&resolution=--- Samba bugzilla kernel section]
* [https://bugzilla.kernel.org/buglist.cgi?component=CIFS&product=File%20System&resolution=--- Linux bugzilla cifs section]

You can also ask questions on the [http://vger.kernel.org/vger-lists.html#linux-cifs linux-cifs mailing-list] (linux-cifs@vger.kernel.org ) ([https://marc.info/?l=linux-cifs&r=1&w=3 web archive here]).

If you report a bug, a network capture and a kernel console log output is always helpful.

     # make the kernel as verbose as possible
     echo 'module cifs +p' > /sys/kernel/debug/dynamic_debug/control
     echo 'file fs/cifs/* +p' > /sys/kernel/debug/dynamic_debug/control
     echo 1 > /proc/fs/cifs/cifsFYI
     echo 1 > /sys/module/dns_resolver/parameters/debug
     
     # get kernel output + network trace
     dmesg --clear
     tcpdump -s 0 -w trace.pcap & pid=$!
     sleep 3
     mount.cifs ....cd ... ls.. etc.. (the thing that fails)
     sleep 3
     kill $pid
     dmesg > trace.log

This should produce a **trace.pcap** and a **trace.log** file.

Be sure to mention your **kernel version**, **server software and version**.

Additional information on debugging the CIFS/SMB3 kernel client can be found at:
https://wiki.samba.org/index.php/LinuxCIFS_troubleshooting