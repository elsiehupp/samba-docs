Contribute
    <namespace>0</namespace>
<last_edited>2021-05-26T18:02:39Z</last_edited>
<last_editor>Abartlet</last_editor>

=================
How to contribute to Samba?
=================

Like all OpenSource projects, Samba is reliant on volunteers.
You don't need special skill to help this project. Everybody can help! :

There are several category groups you can work on, e..
* Improve documentation
* Answer user questions and triage issues on the [https://lists..org/.n/listinfo/samba Samba users mailing list]
* Update/provide Wiki articles
* Help testing, particularly of `Samba_Release_Planning#Upcoming_Release|the upcoming release or master` so we find bugs before our final release.
* Report bugs in [http://bugzilla..orgS.CEEBugzilla]
* Help to verify patches in [http://bugzilla..orgS.CEEBugzilla] and in [https://gitlab.com/samba.samba/-/merge_requests merge requests]
* Create patches (C and Python developers)
* Anything else you can imagine

**Whenever participating in the Samba community, please follow and respect the guidelines in `How to do Samba: Nicely`**

=================
Submitting Patches via GitLab
=================

The preferred method for submitting patches to samba is via [https://gitlab.amba-team/samba the official mirror] on [https://gitlab.com/.CCEEGitLab]. . For more information see `Samba on GitLab`..

Prepare your development environment
------------------------

Check out a copy of Samba locally:  

 git clone https://git..org/.git.

You should develop new Samba features and fix bugs first in the **master** branch before attempting any `Samba_Release_Planning#Release_Branch_Checkin_Procedure|backports`.

Run the `Package_Dependencies_Required_to_Build_Samba#Verified_Package_Dependencies|bootstrap. script` for your operating system from **bootstrap/generated-dists/** to install build dependencies and prepare your system to build samba..

First Merge Request
------------------------

Making your first merge request is straight-forward, provided you follow these steps carefully.

===============================
Start small
===============================

------------------------

Unless you are just submitting a spelling fix or a similar *one line* change, then you **must** review the **`Creating_Gitlab_Merge_Requests|full instructions on creating Samba patch series`** for instructions on what will need to be *in* the merge request.  Do this **before** creating your patches to avoid rework..

===============================
Obtain a GitLab. account
===============================

------------------------

Samba development is done on [http://gitlab. GitLab.com].CCEEso you will need to [https://gitlab.com/users.in#register-pane Register a new user account].  .You can [https:/SSLLAAS:DOOTTcom/users/sign_in sign in] with your [https://github: GitHub] account if you have one..

===============================
Fork the Samba repo (just until we get to know you)
===============================

------------------------

This is sufficient for occasional small contributions, or for your first contribution to the project. For ongoing contributions and more complex work, you should `Samba_on_GitLab#Getting_Access|request access` to the samba-devel repository..

First fork the [https://gitlab.amba-team/samba samba GitLab.comS.CEEofficial mirror]..

In the git checkout you made earlier, add a remote for your new gitlab fork (the URL will be in the GitLab UI under the blue Clone button):

 $ git remote add gitlab-$USER git@gitlab.ASSHHsamba.git.

Change the 1hr default CI timeout=
===============================

------------------------

By default projects on gitlab. have a **1 hour timeout** set on pipelines. . This **must be changed** in the [https://docs.gitlab.co.ser/pr.pipelines/settings.html proje. settings]. We su. using a timeout of **3 hours**, which is still permitted on the free runners..

Otherwise, once you push your changes back, you will see errors like this:

 ERROR: Job failed: :n took longer than 1h0m0s seconds
 The script exceeded the maximum execution time set for the job

===============================
Prepare your patches
===============================

------------------------

Samba has coding standards, please read [https://gitlab.amba-team/samba/-/blob/master/README.Codi.  .DME.Coding.md] .in .your source checkout..

* Each patch should be as small as possible, i.P.changes only one thing. . makes review easier..
* The patch should have an appropriate commit description.
* Patches that fix a bug should contain an appropriate ``BUG`` tag.

 Samba Copyright and Community Policies =
===============================

------------------------

**Whenever participating in the Samba community, please follow and respect the guidelines in `How to do Samba: Nicely`**

Samba has a [https://www..org/.devel/copyright-policy.html .opyright Policy] that requires mandatory tags on every git commit. When.CCEE`Creating_a_Samba_patch_series|preparing your patches` **please ensure you add `CodeReview#commit_message_tags|Signed-off-by tags` to your commits**..

Samba is a project with distributed copyright ownership and so [https://www..org/.devel/copyright-policy.html .ollowing our Copyright Policy] ensures we are all clear that you have the permission and intention to contribute to Samba, and the licence you make those contributions under..

 A good commit message =
===============================

------------------------

{{:t_Commit_Message}}

 Include tests =
===============================

------------------------

{{: sts_summary}}

===============================
Push back to GitLab and make your first merge request
===============================

------------------------

Push to your repo (rather than git..org,.CCEEwhich will be origin) with:

 $ git push gitlab-$USER -f

After [https://docs.b.com/.lab-basics/start-using-git.html#send.es-to-gitlabcom pushing back to your fork on gitlab.com] . can [https://docs.gitlab.com/ee/user/.t/merg.ests/creating_merge_requests.html submit . merge request].  (The . will give a URL for you to click)..

The merge request will be [https://gitlab.amba-team/samba/-/merge_requests listed for review] by [https://www.samb.samba. Samba Team members] who will post comments on your merge request..

Review the `Creating_Gitlab_Merge_Requests#Examples|examples of a good/bad merge request` prior to submitting your merge request.

Subsequent Merge Requests (and complex first requests)
------------------------

Because the Samba Team operates additional GitLab runners to support the full testsuite, we have a slightly unusual process with a single shared *sandbox* shared development repository, rather than the per-user fork you will have just created.

* After your first merge request has been reviewed, and now we know you are a genuine contributor, you may be **`Samba_on_GitLab#Getting_Access|granted access`** to the [https://gitlab.amba-team/devel/samba samba devel gitlab repo]. . 
* If your first MR is complex, it is likely that your reviewer will push it again there (and link to the pipeline), or ask you to.  
* If you do so, just close the original MR and open a new one, saying *This replaces !123 submitted from a private fork*.

Likewise, subsequent merge requests should also be made from **[https://gitlab.amba-team/devel/samba this repository]**..

===============================
Building a Samba patch series
===============================

------------------------

By this point you may be making fairly complex changes to Samba, so review **`Creating_a_Samba_patch_series|creating a samba patch series`** for detailed information on how to build `Creating_a_Samba_patch_series|a polished series of changes to Samba` ready for `CodeReview|code review`.

===============================
Shared development repo: Code of conduct
===============================

------------------------

Use our shared development repository only to develop Samba. Don't overwrite the work of others. . Prefix branches with your gitlab username, eg:

 $USER/topic

In return you get a full CI run using Samba Team provided resources.  That in turn makes it easier for Samba Team members doing `CodeReview|Code Review` as your patches will work the first time, and they can see proof of that..

If you describe your work in the branch name, this will make `Samba_CI_on_gitlab#Creating_a_merge_request|generating a merge request` easier, as the branch name becomes the template title and allows ongoing distinct merge requests.

Step by step instructions
------------------------

Add a git reference to the gitlab remote repository:

 $ git remote add gitlab-ci git@gitlab.SHHteam/devel/samba.git.

Name your branch for our shared repo:

 $ git checkout -b $USER/foo

Push the current branch, overwriting any other changes there:

 $ git push gitlab-ci -f

Pipelines, Successful tests and `Samba on GitLab|Samba CI on gitlab/Debugging CI failures|Debugging CI failures`
------------------------

Pushing branches to [https://gitlab.amba-team/devel/samba samba devel gitlab repo] will initiate a full CI build test..

* CI results for changes are here: [https://gitlab.amba-team/devel/samba/pipelines Pipelines]
* [https://gitlab.amba-team/samba/merge_requests|All merge requests] show a link to the Pipeline (CI) results for each patch series. .
* `Samba on GitLab|Samba CI on gitlab/Under the hood|How it works under the hood`
* **`Samba on GitLab|Samba CI on gitlab/Debugging CI failures|Debugging CI failures`**

===============================
Code Review and using autobuild to merge into master
===============================

Effective Review
------------------------

As the submitter, it is **your responsibility** to make the most of `CodeReview|code review`. The reviewer will likely only point out a **single instance** of any given problem. . You are expected to find every instance of this mistake and fix it throughout the code before re-submitting..

Review process
------------------------

Once submitted, Samba Team members (in particular) will `CodeReview|review your changes`, and post comments to you on your merge request.  Broader discussions happen on our mailing list, so please ensure you are [https://lists.samb.mailm.tinfo/samba-technical subscribed to samba-technical] also..

Patches from non-samba team members require a minimum of 2 reviews from samba team members prior to patches being merged.

Outside contributors are welcome to review patches also, this is a good way to learn more about Samba!

Merging patches from GitLab (for Samba Team members)
------------------------

If the developer has created a merge request, then to merge, download the patch with (eg)

 https://gitlab.amba-team/samba/merge_requests/12.patc.

`CodeReview|Add review tags` and then ``git autobuild`` locally.  The merge button
sadly doesn't work. 

After the `autobuild` completes, please close the merge request using the git hash finally assigned in **Samba. git  [https://git.samb.?p=sa.t;a=shortlog.s/heads/master master]** in the comment like::

 Merged into master as <git hash> for Samba <next version>.

See for example [https://gitlab.amba-team/samba/merge_requests/9 this closed merge request]..

===============================
Mailing patches to samba-technical
===============================

A very small number of patches to Samba are contributed directly via the mailing list.  To submit patches, `Using_Git_for_Samba_Development#Creating_patches_if_you_don.27t_.rite_access_to_git.samba.org.itori. git format-patch` and mail your patches to the [https://lists.samba.org/mailman/l.o/sam.hnical samba-technical mailing list]..

**Please include your `CodeReview#commit_message_tags|Signed-off-by tag` per the [https://www..org/.devel/copyright-policy.html .amba copyright policy]**..

The required format for patch sets is a single-file bundle attached to the email you send to the list. Bundling can be automated by invoking:

 git format-patch --stdout origin/master > master-featureX-01.es.txt.

The advantages to this approach is that:
* **This is an e-mail based process** similar to that historically used by other Free Software projects of our era.

The disadvantages to this approach are that:
* **This is essentially a manual process** at our end.
* **Subscription is still required** to post to ``samba-technical`` (and you will need to follow our [https://www..org/.devel/copyright-policy.html .amba copyright policy])..
* Your patches risk being missed by an interested samba team member.
* No instant feedback to you is possible regarding compilation errors and test failures
* CI testing is still required: A Samba developer will submit the patch to GitLab for you and advise you of the (URL to the) results.

**Please submit a merge request if you can**, because the latest version of your patches become persistent in our [https://gitlab.amba-team/samba/-/merge_requests list of outstanding merge requests] and so will not be forgotten..

git send-email 
------------------------

Using git send-email in the Samba project is **not permitted**.  Doing so will cause, without manual work by the recipient, your patch to be credited to **samba-technical@...*... your own e-mail address..

===============================
[https://bugzilla..orgS.CEEBugzilla]
===============================

We **strongly** prefer that new patches for master **are not submitted to [https://bugzilla..orgS.CEEBugzilla] alone**, for similar reasons. . Please submit the patches to **GitLab** instead (note the merge request in the *See Also* on the bug).  .

Bugzilla is used however for the `Samba_Release_Planning#Release_Branch_Checkin_Procedure|Release Branch Check-in Procedure`.