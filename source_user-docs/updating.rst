Updating Samba
    <namespace>0</namespace>
<last_edited>2020-12-03T10:27:18Z</last_edited>
<last_editor>Hortimech</last_editor>

===============================

Introduction
===============================

The following documentation describes the process of updating Samba to a newer version.

If you want to migrate a Samba NT4 domain to Samba Active Directory (AD), see `Migrating_a_Samba_NT4_Domain_to_Samba_AD_(Classic_Upgrade)|Migrating a Samba NT4 Domain to Samba AD (Classic Upgrade)`.

.. note:

    Microsoft stopped supporting Windows NT 4.0 on December 31, 2004 and twice recently they have broken compatibility to it in Windows 10. It is probably just a matter of time until they decide not to fix a break. Samba, like Microsoft, advises upgrading to Active Directory.

=================
Misconceptions About Samba 4
=================

.. note:

    If you update to Samba 4 and later, you do not have to migrate to Active Directory.

The Active Directory (AD) Domain Controller (DC) support is one of the enhancements introduced in Samba 4.0. However all newer versions include the features of previous versions - including the NT4-style (classic) domain support. This means you can `#The_Update_Process|update` a Samba 3.x NT4-style primary domain controller (PDC) to a recent version, as you previously updated, for example from 3.4.x to 3.5.x. There is no need to migrate an NT4-style domain to an AD.

Additionally, all recent versions continue to support setting up a new NT4-style PDC. The AD support in Samba 4.0 and later is optional and does not replace any of the PDC features. The Samba team understand the difficulty presented by existing LDAP structures. For that reason, there is no plan to remove the classic PDC support. Additionally we continue testing the PDC support in our continuous integration system.

=================
The Update Process
=================

Run the following steps, whether you are updating a Samba Active Directory (AD) domain controller (DC), a Samba NT4-style PDC, a Samba domain member, or a standalone installation:

* Stop all Samba services.

* Create a backup.

* Read the release notes of skipped versions. They contain important information, such as new features, changed parameter, and bug fixes. In case you switch to new major release, read the release notes of the initial version (x.y.0) and the ones from minor versions up to the new version you will update to. For example, if you update from 4.4.4 to 4.6.2, read the 4.5.0, 4.6.0, 4.6.1, and 4.6.2 release notes.

* Install the latest version over your existing one:

* If you compile Samba from the sources, use the same ``configure`` options as used for your previous version. For more information, see `Build_Samba_from_Source#Viewing_Built_Options_of_an_Existing_Installation|Build Samba From the Sources`.

* If you update using packages, read the distribution documentation for information how to update.

* Start Samba.
* EStart the same daemons as on your previous version::
* On Samba AD DCs: :>samba``
* On Samba NT4-style PDC/BDCs: :>smbd``, ``nmbd``
* On Samba domain members: :>smbd``, ``nmbd`` (``winbind``, if used)
* On Samba standalone hosts: :>smbd``

* Check your Samba log files for errors.

* Test your updated installation.

=================
Upgrading Active Directory Domain Controllers
=================

Upgrading your AD DC's can introduce additional complications, due to things like database compatibility and managing FSMO roles. We recommend that you: 
* Run the `dbcheck|Samba AD DC database check` as part of testing your updated installation.
* Refer to `Upgrading_a_Samba_AD_DC#Updating_Multiple_Samba_Domain_Controllers|Updating_Multiple_Samba_Domain_Controllers` for the safest way to roll out an upgrade to your DC network.
* Be aware of database compatibility when `downgrading an Active Directory DC` across a major release.

=================
Notable Enhancements and Changes
=================

If you are updating Samba, always read the `Samba_Features_added/changed_(by_release)|release notes` of all versions between the previous and the one you are updating to. They contain important and additional information on new features, changed parameter options, and so on.

This section provides an overview about important changes that require your attention to fix problems of previous versions, avoid a negative performance impact, and so on.

Changes Affecting All Samba Installation Modes 
------------------------

File Execution Permissions
------------------------

**4.0.0 and later**

Previously, Samba did not check the execution bit of files. As a consequence, users could execute files, such as ``*.exe`` and ``*.bat``, on a share, even if the x-bit was not set. Samba has been enhanced and now will not execute a file if the x-bit is not set. When upgrading from a previous version, if your executable files do not have the x-bit set, you can enable the old behaviour, by setting the following parameter in individual shares or in the ``[global]`` section:

 acl allow execute always = yes

Samba Active Directory Domain Controllers 
------------------------

The ``ntvfs`` File Server Back End Has Been Disabled
------------------------

**4.5.0 and later**

Previously, Samba enabled users to provision a domain controller (DC) using the ``ntvfs`` file server back end. This back end was never supported, and thus the ``ntvfs`` feature is no longer built by default in Samba 4.5.0. Consequently, starting the ``samba`` service on a DC using the ``ntvfs`` back end fails after the update and the following error is logged:

 [2016/09/01 08:TT  0, pid=995] ../source4/smbd/service.c:98(server_service:
   Failed to start service 'smb' - NT_STATUS_INVALID_SYSTEM_SERVICE
 [2016/09/01 08:TT  0, pid=995] ../lib/util/become_daemon.c:111(exit_daemon):
   STATUS=daemon failed to start: Samba failed to start services, error code -1073741796

To fix the problem, migrate the file server back end on your DC to the supported ``s3fs`` back end. For details, see `Migrating_the_ntvfs_File_Server_Back_End_to_s3fs|Migrating the ntvfs File Server Back End to s3fs`.

Fixing replPropertyMetaData Attributes
------------------------

**4.5.0 and later**

Samba versions prior to 4.5.0 stored the ``replPropertyMetaData`` attribute incorrectly. As a consequence, administrators could experience errors, such as renaming conflicts. The problem has been fixed in 4.5.0 and later versions and Samba now stores the attribute correctly. The ``samba-tool`` utility has been enhanced to detect incorrectly stored ``replPropertyMetaData`` attributes:

 # samba-tool dbcheck --cross-ncs

To fix the attributes, run:

 # samba-tool dbcheck --cross-ncs --fix --yes
 ...
 CN=NTDS Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com: 0x00000003
 CN=NTDS Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com: 0x00000000
 ERROR: unsorted attributeID values in replPropertyMetaData on CN=NTDS Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com

 Fix replPropertyMetaData on CN=NTDS Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com by sorting the attribute list? [YES]
 Fixed attribute 'replPropertyMetaData' of 'CN=NTDS Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com'

Note that the ``--yes`` parameter automatically fixes <u>all</u> problems found, not just the ``replPropertyMetaData`` attributes!

You should run the check and fix operation on all Samba Domain Controllers (DC), because ``replPropertyMetaData`` is a non-replicated attribute and modifications are not replicated to other DCs.

For more information, see the `Dbcheck|Samba AD DC database check` section.

Failure To Access Shares on Domain Controllers If ``idmap config`` Parameters Set in the ``smb.conf`` File
------------------------

**4.4.6 or later**

By default, the ``winbindd`` service on a Samba Active Directory (AD) domain controller (DC) generates ID's locally on the DC and stores them in the ``idmap.ldb`` database. You can override the generated ID's by setting ``uidNumber`` and ``gidNumber`` attributes in your user accounts and groups in Active Directory. Originally, if the ``idmap config`` parameters were set in the ``smb.conf`` file they were ignored, but due to a bug in Samba 4.4.6 and later, the parameters are no longer ignored and clients fail to connect to shares on the DC. To fix the problem:
* Remove all ``idmap config`` parameters in the ``smb.conf`` file on DCs.
* Restart the ``samba`` service.
* Restart the clients.

As a result, the clients will now correctly connect to shares on the DC.

New Default for LDAP Connections Requires Strong Authentication
------------------------

**4.4.1 or later / 4.3.7 or later / 4.2.10 or later**

The security updates 4.4.1, 4.3.7 and 4.2.10 introduced a new ``smb.conf`` option for the Active Directory (AD) LDAP server to enforce strong authentication. The default for this new option ``ldap server require strong auth`` is ``yes`` and allows only simple binds over TLS encrypted connections. In consequence, external applications that connect to AD using LDAP, cannot establish a connection if they do not use or support TLS encrypted connections.

Applications connecting to Samba AD using the LDAP protocol without encryption, will display the error message:

 ldap_bind: Strong(er) authentication required (8)
        additional info: BindSimple: :t encryption required.

For further information, see the [https://www.samba.org/samba/history/samba-4.4.1.html 4.4.1], [https://www.samba.org/samba/history/samba-4.3.7.html 4.3.7], or the [https:/SSLLAAS:TTsamba.org/samba/history/samba-4.2.10.html 4.2.10] release notes.

AD Database Cleanup of Deleted LDAP DNS Entries
------------------------

**4.1.12 or later**

Previously, Samba incorrectly created deleted Active Directory (AD) objects for removed DNS entries. The problem has been fixed. If you start the first Domain Controller (DC) with a fixed Samba version, all deleted objects are removed. As a result, this can result in a slow performance until the deleted objects are removed.

Incorrect TLS File Permissions
------------------------

**4.1.2 or later / 4.0.12 or later**

Previously, Samba created the ``*.pem`` files used for LDAP TLS encryptions with insecure permissions. To avoid insecure connections, delete the files on all domain controllers (DC):

 # rm /usr/local/samba/private/tls/*.pem

Restart Samba after you deleted the files to automatically re-create the new certificates.

Fixing Dynamic DNS Update Problems
------------------------

**4.0.7 or later**

See `Fix_DNS_dynamic_updates_in_Samba_versions_prior_4.0.7|Fix DNS dynamic updates in Samba versions prior 4.0.7` for details.

Fixing Incorrect Sysvol and Directory ACLs
------------------------

** When updating from early 4.0.x versions, 4.0 beta and 4.0 release candidates.**

* To reset wrong Sysvol ACLs, run:
 # samba-tool ntacl sysvolreset

* To reset all well known ACLs in the directory, run:
 # samba-tool dbcheck --cross-ncs --reset-well-known-acls --fix

* To fix errors in the Active Directory (AD) database, run:
 # samba-tool dbcheck --cross-ncs --fix

Samba Domain Members 
------------------------

ID Mapping Configuration Verification
------------------------

**4.6.0 or later**

Previously, Samba did not verified the ID mapping configuration in the ``smb.conf`` file on a domain member. Thus, an incorrect ID mapping configuration could be set, such as overlapping ID ranges or incorrect back ends for the default domain. Consequently, the ``winbindd`` service started and ID mapping failed or did not work as expected. The ``testparm`` utility has been enhanced and now reports incorrect ID mapping configurations. For example:

 ERROR: The idmap range for the domain * (tdb) overlaps with the range of SAMDOM (ad)!

 ERROR: Do not use the 'ad' backend as the default idmap backend!

Additionally, when using an incorrect ID mapping configuration, the ``winbindd`` service now fails to start and an error message is logged. For example:

 [2017/03/01 12:TT  0, pid=980] ../source3/winbindd/winbindd.c:1705(main):
   main: FATAL: : idmap backend ad configured as the default backend!

Using Samba 4.6.0 and later, users are no longer able to use incorrect ID mapping configurations.

For further details, supported back ends on a domain member, and their configuration, see:
* `Identity Mapping Back Ends`
* the ``IDENTITY MAPPING CONSIDERATIONS`` section in the ``smb.conf(5)`` man page

The ``ad`` ID Mapping Back End Now Supports Enabling RFC2307 or Template Mode Per-domain
------------------------

**4.6.0 or later**

Previously, when the ``winbind nss info`` parameter was set to ``rfc2307``, the Samba ``ad`` ID mapping back end retrieved shell and home directory settings for all Active Directory (AD) domains from AD. In Samba 4.6.0, the new ``idmap config *domain_name*:info`` parameter has been added. This parameter enables the administrator to set on a per-AD domain basis if the shell and home directory settings of users should be retrieved from AD or if the template settings, set in the ``template shell`` and ``template homedir`` parameters are applied.

The new ``idmap config *domain_name*:info`` parameter has a higher priority than the global ``winbind nss info = rfc2307`` setting. Therefore, using the ``idmap config *domain_name*:unix_nss = no`` default setting for an AD domain, the shell and home directory are no longer retrieved from AD and the values set in the ``template shell`` and ``template homedir`` parameters are applied. To re-enable retrieving the values from AD for a domain, set in the ``[global]``section in your ``smb.conf`` file::

 idmap config *domain_name*:info = yes

For details and an example how to set up, see `Idmap_config_ad#Configuring_the_ad_Back_End|idmap config ad - Configuring the ad Back End`.