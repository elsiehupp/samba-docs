SysVol replication (DFS-R)
    <namespace>0</namespace>
<last_edited>2019-10-25T18:45:11Z</last_edited>
<last_editor>Hortimech</last_editor>

Samba in its current state doesn't support SysVol replication via DFS-R (Distributed File System Replication) or the older FRS (File Replication Service) used in Windows Server 2000/2003 for Sysvol replication.

We Currently advise administrators to use one of the following workarounds:

* `Rsync based SysVol replication workaround|Rsync based SysVol replication workaround` (Samba DCs only): Quick setup, easy to configure.
* `Bidirectional Rsync/Unison based SysVol replication workaround|Bidirectional Rsync/Unison based SysVol replication workaround` (Samba DCs only): More complex, requires third party script, each DC requires a cron job against each other DC
* `Bidirectional Rsync/osync based SysVol replication workaround|Bidirectional Rsync/osync based SysVol replication workaround` (Samba DCs only): More complex, requires third party script, each DC requires a cron job against each other DC
* `Robocopy_based_SysVol_replication_workaround|Robocopy based SysVol replication workaround` (Samba DCs -> Windows DCs): Quick set, easy to configure, uses MS robocopy

.. note:

    Whichever method you choose, you will need to sync idmap.ldb from the DC holding the PDC Emulator FSMO role to all other DCS. This will ensure that all DCs use the same IDs.

`Category:Active Directory`