Distributed File System (DFS)
    <namespace>0</namespace>
<last_edited>2021-02-17T19:27:40Z</last_edited>
<last_editor>Darkpixel</last_editor>

FRS versus DFS versus DFS-R 
------------------------

FRS (File Replication Service) is used to replicate SYSVOL 
in Windows Server 2000 and 2003. It is slowly being replaced 
by Microsoft with DFS-R (Distributed File System Replication)
in newer versions of Windows Server.

*Samba does not yet include SYSVOL replication, 
neither with FRS, nor with DFS-R.*

DFS-R is often viewed as subfunction of DFS (see below), and both are often
used together, but they can also be used completely separately:
DFS with multiple targets can be used without DFS-R (if the folder 
contents are updated with some other method), and DFS-R can 
replicate shared folders that are not DFS-targets.

Samba partially supports DFS, but not DFS-R yet.

Some users have reported success using [https://syncthing.net/ Syncthing] to emulate DFSR.

DFS 
------------------------

What is DFS
------------------------

DFS (Distributed File System) is a feature that provides several functions:
alternative name space, load balancing, and automatic failover.

Technically DFS provides access to a shared directory that contains no files,
only junctions, and optionally subdirectories with more junctions.
Junctions are similar to soft links as known from Unix file systems, 
but ones that point to shared directories, and they can also point
to shared directories on other servers. The clients first ask the
DFS server for the junction, then access the file server where this
junction points to.

The main use of this is to create an alternative name space (directory tree view),
that hides details of the underlying infrastructure from the users.
The paths that the users see (called DFS names) do not change when servers are renamed, 
or when some of the directories are moved to a different server.
The administrators can simply replace the outdated DFS name with a new one,
that points to the new target.

A DFS name can optionally point to more than one target
(i.e. tell the client several alternative junctions to different shared folders).
In this case clients can access either of the targets. This provides
load balancing, and automatic failover to another server, if one server fails. 

stand-alone versus domain-based DFS
------------------------

DFS can be either stand-alone, or domain-based.

The stand-alone variant runs on one single server and 
causes the DFS names to contain the name of that server,
they look like shares that are provided by that server.

The domain based setup causes the DFS names to contain only the
name of the domain, not the name of any specific server. Several
servers in the domain can act as DFS servers in parallel.
Clients automatically select one of the DFS servers and send 
their queries to that one.

Configure stand-alone DFS in Samba
------------------------

The method to set up the stand-alone variant with Samba is described here:

https://www.samba.org/samba/docs/man/Samba-HOWTO-Collection/msdfs.html

In short it requires in the config file smb.conf the global option 
    ost msdfs = yes
(which is default on AD-DCs)
and in one share declaration the two options
    ath = /export/dfsroot
    sdfs root = yes
Then in the specified path create entries by running commands like
    n -s msdfs:storage0\\share0 linka
    n -s msdfs:server1\\share,server2\\share linkb

The first example defines one target, the second example creates two
alternative targets that share the same contents.

Windows clients will see these as junctions.

Configure Member server DFS in Samba
------------------------

    Similar to above but required one additional setting in smb.conf the global option so it would be
    ost msdfs = yes
    fs object = dfs_samba4

Other just follow what happen on above.

Configure domain-based DFS in Samba
------------------------

The procedure is the same as for stand-alone DFS, but it requires the vfs_object dfs_samba4 (which is default in AD-DC, but beware of the pitfalls of the ``vfs object`` option),
and the DFS names must be declared on each DC
([https://lists.samba.org/archive/samba/2014-June/182705.html because the endpointmapper for DFS is not implemented]).

There is currently (July 2014) a discussion in the mailing list in which several users report great difficulties with domain-based DFS in samba 4, maybe related with the implementation of smb protocol version 3.

Limitations
------------------------

Neither the path dfsroot specified in the DFS share, nor any
of the share paths in the ln-commands must contain upper case characters.
The links might be allowed to contain upper case characters.

DFS works only for Administrator in Samba 4.1.4, and 
[https://groups.google.com/forum/?_escaped_fragment_=topic/linux.samba/mi4O5lHE8Vc#!topic/linux.samba/mi4O5lHE8Vc requires a patch] to work for all users.

There is currently an ongoing discussion in the mailing list whether or not domain-based DFS really works.

DFS-R (replication) is not yet implemented.

----
`Category:Active Directory`