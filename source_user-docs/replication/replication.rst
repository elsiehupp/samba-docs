Replication
    <namespace>0</namespace>
<last_edited>2017-02-26T21:24:11Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

* * `Distributed File System (DFS)`
* :* `SysVol replication (DFS-R)`
* ::* `Rsync based SysVol replication workaround` (Samba DCs only)
* ::* `Bidirectional Rsync/Unison based SysVol replication workaround` (Samba DCs only)
* ::* `Bidirectional Rsync/osync based SysVol replication workaround` (Samba DCs only)
* ::* `Robocopy based SysVol replication workaround` (Samba DCs -> Windows DCs)
* * `Directory Replication Service (DRS)`

----
`Category:Active Directory`