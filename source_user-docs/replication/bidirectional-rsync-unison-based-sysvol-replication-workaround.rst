Bidirectional Rsync/Unison based SysVol replication workaround
    <namespace>0</namespace>
<last_edited>2020-05-05T10:51:15Z</last_edited>
<last_editor>Hortimech</last_editor>

===============================

Introduction
===============================

Samba AD currently doesn't provide support for SysVol replication. To achieve this important feature in a Multi-DC environment, until it's implemented, workarounds are necessary to keep it in sync. This HowTo provides a basic workaround solution based on rsync and unison.

=================
Information on unison + rsync replication
=================

This HowTo describes a solution for SysVol replication, that is based on rsync and unison. As Compare to the rsync method, it is bidirectional. This howto only covers a two DC setup.

It has the following advantages: 
* Quick setup
* Configuration is very easy
* Can work with windows (Please add in)

We will use rsync through a SSH tunnel.

=================
Setup the SysVol replication
=================

Some assumptions:
 You are running all commands as root.
 rsync location  /usr/bin/rsync
 sysvol is located at /var/lib/samba/sysvol on both DC1 and DC2
 unison location /usr/bin/unison
 The first DC is DC1
 The second DC is DC2
 sysvolsync log location /var/log/sysvol-sync.log

Change the paths if your setup is different.

Setup on the Domain Controller with the PDC Emulator FSMO role
------------------------

* Install rsync by using your package manager or compile from source. Make sure, that you use a version that supports extended ACLs!
* You don't need to setup the rsync server.
* Install unison by using your package manager or compile from source. (On Gentoo you need to do ``eselect unison`` to create the link)

 Creating SSH Public Key and ssh-copy to DC2=
===============================

------------------------

 ssh-keygen -t rsa
 ssh-copy-id -i ~/.ssh/id_rsa.pub root@DC2

You can try to access DC2 via ssh 
 ssh DC2

 Setup ssh Control =
===============================

------------------------

If the remote system enforces rate limits on incoming ssh connections, unison will fail if you try to run it this way.
So we create the first ssh connection as a controlpath file in the location specified, all subsequent connections will reuse on the first connection.

 mkdir ~/.ssh/ctl
 cat << EOF > ~/.ssh/ctl/config
 Host *
 ControlMaster auto
 ControlPath ~/.ssh/ctl/%h_%p_%r
 ControlPersist 1
 EOF

 Setup Sysvolsync Log files =
===============================

------------------------

Do the following on DC1 so that you can check what happens during the sync.
Please include this file into logrotate as the log size is not controlled.

 touch /var/log/sysvol-sync.log
 chmod 640 /var/log/sysvol-sync.log

 Setup Unison defaults running parameters =
===============================

------------------------

Please run the following on DC1 

 install -o root -g root -m 0750 -d /root/.unison
 cat << EOF > /root/.unison/default.prf
 # Unison preferences file
 # Roots of the synchronization
 #
 # copymax & maxthreads params were set to 1 for easier troubleshooting.
 # Have to experiment to see if they can be increased again.
 root = /var/lib/samba
 # Note that 2 x / behind DC2, it is required
 root = ssh://root@DC2//var/lib/samba 
 # 
 # Paths to synchronize
 path = sysvol
 #
 #ignore = Path stats    ## ignores /var/www/stats
 auto=true
 batch=true
 perms=0
 rsync=true
 maxthreads=1
 retry=3
 confirmbigdeletes=false
 servercmd=/usr/bin/unison
 copythreshold=0
 copyprog = /usr/bin/rsync -XAavz --rsh='ssh -p 22' --inplace --compress
 copyprogrest = /usr/bin/rsync -XAavz --rsh='ssh -p 22' --partial --inplace --compress
 copyquoterem = true
 copymax = 1
 logfile = /var/log/sysvol-sync.log
 EOF

Setup SysVol on DC2
------------------------

* On DC2 Install rsync by using your package manager or compile from source. Make sure, that you use a version that supports extended ACLs!
* On DC2 Install unison by using your package manager or compile from source. (On Gentoo you need to do ``eselect unison`` to create the link)
* Make sure, that you have `Joining_a_Samba_DC_to_an_Existing_Active_Directory#Built-in_User_.26_Group_ID_Mappings|identical IDs of built-in groups on all DCs`.

1st Trial == 
You now use rsync to create the directory structure with extended attributes
Then the unison setup will only copy the extended attributes on files.

Please make a **backup** of your sysvol, just in case, this is because there is no ``dry-run``

 /usr/bin/rsync -XAavz --log-file /var/log/sysvol-sync.log --delete-after -f"+ */" -f"- *"  /var/lib/samba/sysvol root@DC2:Hvar/lib/samba  &&  /usr/bin/unison

*   path on DC2 is just /var/lib/samba which is different from DC1, it is by design, there is nothing wrong!**

Add to Crontab on DC1 
------------------------

On DC1 run the following:
 crontab -e 
 */5 * * * * /usr/bin/unison -silent

=================
When you try to resync the folder
=================
* g: : follow the steps below OR you can end up with an empty sysvol folder.**
# Disable Cron on DC1, like Add a "#" on the line with ``crontab -e``
# Check if rsync or unison are currently running in ``ps -aux`` if yes, wait for it to finish OR kill it (if it is zombie)
# Remove the hash files on both DC1 and DC2 on ``/root/.unison``
# Now check your sysvol and resync
# Confirm that everything is ok again
# Re-enable the cron on DC1 again

=================
FAQ
=================

* How can I do this on windows?
** I don't have an answer, please post on the mailing list

* What to do if I've more than two DC's?
** In Theory, We would just make more cron jobs on DC1 and the complete sync will be perform next sync to all server.

* Why can't I simply use a distributed filesystem like GlusterFS, Lustre, etc. for SysVol?
** A cluster file system with Samba requires CTDB to be able to do it safely. And CTDB and AD DC are incompatible.

----
`Category Directory`