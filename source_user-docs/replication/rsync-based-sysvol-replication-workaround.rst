Rsync based SysVol replication workaround
    <namespace>0</namespace>
<last_edited>2019-07-18T18:18:27Z</last_edited>
<last_editor>Hortimech</last_editor>

=================
Introduction
===============================

Samba AD currently doesn't provide support for SysVol replication. To achive this important feature in a Multi-DC environment, until it's implemented, workarounds are necessary to keep it in sync. This HowTo provides a basic workaround solution based on rsync.

.. note:

    It has been pointed out that using rsync without SSH is insecure and could lead to a 'M-I-M' attack, whilst this is probably unlikely for most users, it is suggested you consider one of the other methods to replicate Sysvol. 

=================
Information on rsync-based replication
=================

`Image:DC_PDC_emulator.png|thumb|Group Policy Management Console option for PDC`

This HowTo describes a solution for SysVol replication based on rsync. This tool is unidirectional, this means files can only be transferred in one direction. That's why for rsync-based SysVol replication, you have to choose one DC on which you do all modifications (GPO edits, logon script changes, etc.). All other DC's retrieve the changes from this host, this is because modifications on them are overwritten when syncing.

A good choice for this "master" host is the one that contains the FSMO role "PDC Emulator", because you can configure the Group Policy Management Console to connect only to this machine (default), instead of just choosing any of your DC's. You can define at „Action“ / „Change Domain Controller“ which machine the GPMC connects to. There you should select „The domain controller with the Operations Master token for the PDC emulator“ (default).

Even if the unidirectional replication of rsync is a limitation, it has also many advantages:
* already available on most distributions and can be installed through it's package manager (if it's not already installed anyway)
* very quickly setup
* very easily configured
* etc.

If you prefer to use rsync through a SSH tunnel, you can adapt the command to your needs, but usually there's no confidential content on the SysVol share. It should be sufficient for most to use an unencrypted transfer. The rsync module on the PDC Emulator is also defined as read only, because it is used only as the source, so no content can be pushed to it.

=================
Setup the SysVol replication
=================

Setup on the Domain Controller with the PDC Emulator FSMO role 
------------------------

* Install rsync by using your package manager or by compiling from source. Make sure that you use a version that supports extended ACL's!

* If you start your rsync-server through xinetd, you can use the following configuration file (<tt>/etc/xinetd.d/rsync</tt>):

 service rsync
 {
    disable         = no
    only_from       = 10.99.0.0/28     # Restrict to your DC address(es) or ranges, to prevent other hosts retrieving the content, too.
    socket_type     = stream
    wait            = no
    user            = root
    server          = /usr/bin/rsync
    server_args     = --daemon
    log_on_failure += USERID
 }

* Create the file <tt>/etc/rsyncd.conf</tt> (adapt the path variable to your PDC Emulators SysVol path):

 [SysVol]
 path = /usr/local/samba/var/locks/sysvol/
 comment = Samba Sysvol Share
 uid = root
 gid = root
 read only = yes
 auth users = sysvol-replication
 secrets file = /usr/local/samba/etc/rsyncd.secret

* Create a file <tt>/usr/local/samba/etc/rsyncd.secret</tt> (permissions must not be world-readable!) with the following content (adapt the password!):

 sysvol-replication:pa$$w0rd

* Restart xinetd.

Setup on all other Domain Controller(s) 
------------------------

* Make sure, that you have `Joining_a_Samba_DC_to_an_Existing_Active_Directory#Built-in_User_.26_Group_ID_Mappings|identical IDs of built-in groups on all DCs`.

* Install rsync by using your package manager or compile from source. Make sure, that you use a version that supports extended ACLs!

* Create a password file <tt>/usr/local/samba/etc/rsync-sysvol.secret</tt> and fill it with the password you set on the PDC Emulator for the <tt>sysvol-replication</tt> rsync account (permissions of that file must not be world-readable!):

 pa$$w0rd

* For replicating the SysVol folder, run the following command (<tt>--dry-run</tt> means that no modifications are actually made):

 # rsync --dry-run -XAavz --delete-after --password-file=/usr/local/samba/etc/rsync-sysvol.secret rsync://sysvol-replication@{IP-of-your-PDC-Emulator}/SysVol/ /path/to/your/sysvol/folder/

 receiving file list ... done                                                                                                                                                                                   
 created directory /usr/local/samba/var/locks/sysvol                                                                                                                                                            
 ./                                                                                                                                                                                                             
 samdom.example.com/                                                                                                                                                                                            
 samdom.example.com/Policies/                                                                                                                                                                                   
 samdom.example.com/Policies/{31B2F340-016D-11D2-945F-00C04FB984F9}/                                                                                                                                            
 samdom.example.com/Policies/{31B2F340-016D-11D2-945F-00C04FB984F9}/GPT.INI                                                                                                                                     
 samdom.example.com/Policies/{31B2F340-016D-11D2-945F-00C04FB984F9}/MACHINE/                                                                                                                                    
 samdom.example.com/Policies/{31B2F340-016D-11D2-945F-00C04FB984F9}/MACHINE/Registry.pol                                                                                                                        
 samdom.example.com/Policies/{31B2F340-016D-11D2-945F-00C04FB984F9}/USER/                                                                                                                                       
 samdom.example.com/Policies/{31B2F340-016D-11D2-945F-00C04FB984F9}/USER/Documents & Settings/                                                                                                                  
 samdom.example.com/Policies/{31B2F340-016D-11D2-945F-00C04FB984F9}/USER/Scripts/                                                                                                                               
 samdom.example.com/Policies/{31B2F340-016D-11D2-945F-00C04FB984F9}/USER/Scripts/Logoff/
 samdom.example.com/Policies/{31B2F340-016D-11D2-945F-00C04FB984F9}/USER/Scripts/Logon/
 samdom.example.com/Policies/{6AC1786C-016F-11D2-945F-00C04FB984F9}/
 samdom.example.com/Policies/{6AC1786C-016F-11D2-945F-00C04FB984F9}/GPT.INI
 samdom.example.com/Policies/{6AC1786C-016F-11D2-945F-00C04FB984F9}/MACHINE/
 samdom.example.com/Policies/{6AC1786C-016F-11D2-945F-00C04FB984F9}/USER/
 samdom.example.com/scripts/
 samdom.example.com/scripts/logonscript.bat

 sent 124 bytes  received 1064 bytes  2376.00 bytes/sec
 total size is 1207  speedup is 1.02 (DRY RUN)

* **Warning: Make sure that the destination folder is really your SysVol folder, because the command will replicate to the given directory and removes everything in it that isn't also on the source! You could damage your system! So check the output carefully to see if the replication is doing what you expect!**

* If everything looks sane, run the command without the <tt>--dry-run</tt> option and let rsync do the replication.

* To automate synchronisation, you can run the command via cron (e. g. every 5 minutes).

 */5 * * * *          rsync -XAavz --delete-after --password-file=/usr/local/samba/etc/rsync-sysvol.secret rsync://sysvol-replication@{IP-of-your-PDC-Emulator}/SysVol/ /path/to/your/sysvol/folder/

* Repeat these steps on every DC (except your PDC Emulator!).

=================
FAQ
=================

* How can I get multi-direction replication ?
** There is some testing using unison with rsync to achieve this.
** Have a look here : 
** https://wiki.samba.org/index.php/Bidirectional_Rsync/Unison_based_SysVol_replication_workaround

* Why can't I simply use a distributed filesystem like GlusterFS, Lustre, etc. for SysVol?
** A cluster file system with Samba requires CTDB to be able to do it safely. And CTDB and AD DC are incompatible.

----
`Category:Active Directory`