Robocopy based SysVol replication workaround
    <namespace>0</namespace>
<last_edited>2020-10-15T09:37:56Z</last_edited>
<last_editor>Hortimech</last_editor>

=================
Introduction
===============================

`Image:DC_PDC_emulator.png|thumb|Group Policy Management Console option for PDC`

Samba Active Directory Domain Controllers currently don't support SysVol replication through Distributed File System Replication (DFS-R). Until it is implemented a workaround is necessary. For replication between Samba DCs an easy to configure `Rsync_based_SysVol_replication_workaround|rsync based workaround` exists. However this isn't usable in an mixed environment on Windows Domain Controllers (DC), because rsync doesn't preserve the file system access control lists (ACL) across Linux and Windows. This documentation describes a workaround for SysVol replication that is based on robocopy, to pull the share content from one defined Samba DC.

Robocopy is - like rsync - unidirectional. This means that files can only transferred into one direction. Due to that limitation, one DC has to be defined as source, on which do all modifications (GPO edits, logon script changes, etc.) are done. Defining one source host for modifications is necessary, due to modifications done in SysVol folder of other hosts will be overwritten when using a unidirectional syncing workaround like with robocopy. A recommendation is, to define the Domain Controller holding the `Flexible_Single-Master_Operations_(FSMO)_Roles#PDC_Emulator|"PDC Emulator" FSMO role` as the source. One reason for that choice is, that e. g. the Group Policy Management Console uses this DC as default, when doing changes.

Even if the unidirectional replication of Robocopy might be a limitation, it also has advantages:
* no need to manually keep the SysVol share consistent
* quick and easy setup

In the following we assume that Windows Domain Controller is already set up and joined to the domain. The source Samba DC, where we're replicating the SysVol content from, is DC1.

We also assume that the "robocopy.exe" is accessible on your Windows DC in

 C:\Windows\System32\Robocopy.exe

=================
Setting up the Windows Task Job
=================

* Log on to the Windows Domain Controller as Domain Administrator

* Click "Start" / "Run" / type "taskschd.msc" and click "OK"

* Right-click to "Task Scheduler Library" / choose "Create Task"

* * Enter a task name: e. g. "SysVol Replication Workaround"

* * Select "Run whether user is logged on or not"

* * Navigate to the "Triggers" tab and click the "New" button

* :* Select "Daily", check "Repeat task every: 5 minutes" and "for a duration of: 1 day". Choose the time for the first start ~15 min in the future, to have enough time for the remaining steps.

* :* Click "OK" to save the trigger.

* * Navigate to the "Actions" tab and click the "New" button

* :* Choose "Start a program" and fill the fields with following path/arguments:

 Program/script:               C:\Windows\System32\Robocopy.exe
 Add arguments (optional):     \\DC1\SYSVOL\samdom.example.com\ C:\Windows\SYSVOL\domain\ /mir /sec

* :* Click "OK" to save the action.

* * Save the task with "OK".

* * Enter the Domain Administrator credentials and click "OK".

=================
Restart the File Replication Service
=================

* "Start" / "Run" / enter "services.msc" and click "OK"

* Restart the "File Replication Service"

=================
Start an Initial Synchronisation
=================

If you don't want to wait for the first synchronisation, right-click the task created in the task scheduler and choose "Run". After a few seconds, depending on the amount of files on the SysVol folder, the task finishes and the SysVol share on the Windows DC is populated.

=================
Troubleshooting
=================

If the "SysVol" and "Netlogon" share is not visible on the Windows Domain Controller, see [https://support.microsoft.com/en-us/kb/947022 https://support.microsoft.com/en-us/kb/947022].

----
`Category:Active Directory`