Directory Replication Service (DRS)
    <namespace>0</namespace>
<last_edited>2017-02-26T21:24:09Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

DRS (Directory Replication Service) replicates the contents of the Active Directory (i.e. user/group management, ACL changes, etc.) and is part of Samba since 4.0.

See [https://technet.microsoft.com/en-us/library/cc772726%28v=ws.10%29.aspx How the Active Directory Replication Model Works] at MS TechNet for further information about the AD replication model.

----
`Category Directory`