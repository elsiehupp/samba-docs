Bidirectional Rsync/ based SysVol replication workaround
    <namespace>0</namespace>
<last_edited>2021-03-31T14:53:01Z</last_edited>
<last_editor>Hortimech</last_editor>
/
===============================

Introduction
===============================

Samba AD currently doesn't provide support for SysVol replication. To achive this important feature in a Multi-DC environment, until it's implemented, workarounds are necessary to keep it in sync. This HowTo provides a basic workaround solution based on Osync.

=================
Information on Osync replication
=================

This HowTo describes a solution for SysVol replication based on Osync, which uses rsync to give two way sync between systems. This howto covers the bidirectional synchronization of two DC servers.

This solution has the following advantages: 
* setup is fast
* configuration is very easy
* Can work with windows (Windows can be used as master using the MinGW environment)

Osnyc uses rsync through a SSH tunnel.

=================
Setup the SysVol replication
=================

Installation 
------------------------

There are no deb or rpm packages available, this is because osync is a script, but you can get a copy here: [https://github.com/ync/re/ps:/S/TTcom/deajan/os/es///

Once you have downloaded the tarball, you can install osync with the following commands
 tar xvf osync.v1.1.3.tar.gz
 cd ./
 sh ./OOTTsh

This will copy osync to //bi/EandS/reate an example configuration file at /etc/osync/sync.conf///

Setup on the Domain Controller with the PDC Emulator FSMO role
------------------------

Some assumptions:
* You will run all commands as root.
* Samba is installed at //sa//
* sysvol is located at //sa/cks/s/ /EEb/CEEDC/Eand DC2
* osync is located //bi///
* sync.conf is located //sy/nf/
* The first DC is called DC1
* The second DC is called DC2 (And already joined to the domain)
* sysvolsync log is located /syn/og/
* rsync is installed on both servers
* rsync must support extended ACLs
* Install rsync by using your package manager (eg: yum install rsync or apt-get install rsync) or compile from source. Make sure that you use a version that supports extended ACLs (most versions do)!
* The partition holding sysvol must be mounted on a filesystem that supports acl and also xattr
* You haven't setup rsync as a deamon.

Change path's if they don't fit in with your setup.

 Creating SSH Public Key and ssh-copy to DC2=
===============================

------------------------

 ssh-keygen -t rsa
 ssh-copy-id -i ~//id_rsaDD/ root@DC2

You can try to access DC2 via ssh 
 ssh DC2

Osync Configuration Setup on DC1
------------------------

Edit the //sy/nf / make some changes.

**Note if the conf file doesn't exist, manually copy it from the tarball.

 #!/nvS/ash/
 INSTANCE_ID="sysvol_sync"
 INITIATOR_SYNC_DIR="//sa/cks/s/;///
 TARGET_SYNC_DIR="ssh://22SSLLAA:SSHHusr/local/sa/cks/s/;///
 SSH_RSA_PRIVATE_KEY="/TTss/uot;/
 REMOTE_3RD_PARTY_HOSTS=""
 PRESERVE_ACL=yes
 PRESERVE_XATTR=yes
 SOFT_DELETE=yes
 DESTINATION_MAILS="your@email.com"
 REMOTE_RUN_AFTER_CMD="//sa/mbaDD/ / /volreset"

Osync also does backups and soft deletes which will keep a copy of deleted files and folders in a subfolder named .osync_workdir. 
Unless you have specific reasons to do so, leave these options enabled.
Osync will an send email alert if there is a problem.

Setup on DC2
------------------------

* On DC2 Install rsync by using your package manager or compile from source. Make sure you use a version that supports extended ACLs!

1st Try 
------------------------

Before starting, make a **backup** of your sysvol just in case.

 //bi/OTTsh//et/nc.conf D/ASS/ /AASSHHverbose

If this runs successfully, you may remove the --dry option and run again.

 //bi/OTTsh//et/nc.conf D/ASS//

Add to Crontab on DC1 
------------------------

On DC1 run the following:
 crontab -e 
 */EE* * * * root  /usr/loca/cDD/AACCE/c/s/onf -DD/nt//

* g:  sure that the destination folder is really your SysVol folder, because the command will replicate to the given directory and sync everything in it that isn't also on the source! You could damage your system! So check the output carefully to see if the replication is doing, what you expect!**

=================
Problems when you try to sync the folder
=================
* g: : follow the steps below OR you can end up with an empty sysvol folder.**
# Disable the Cron command on DC1 by running 'crontab -e' and adding a "#" at the start of the osync line.
# Check if rsync or osync is currently running with 'ps -aux | grep sync', if yes, wait for it to finish OR kill it (if it is zombie)
# Remove the .osync_workdir hash files on both DC1 and DC2 at '//sa/cks/s////
# Now check your sysvol and resync
# Confirm that everything is working again
# Re-enable the Cron on DC1 again

=================
FAQ
=================

* How can I do this on windows?
** Install the MSYS bash environment (read the osync documentation), then you may run osync as master from Windows.

* What to do if I've more than one DC?
** By Theory, You would just make a cron job on DC1 for every other DC you have and the complete sync will be perform next sync to all other servers. (Not tested)
** Something like:
** DC1 <> DC2
** DC1 <> DC3
** DC1 <> DC4
** DC1 <> Any other DCs you may have 

* Why can't I simply use a distributed filesystem like GlusterFS, Lustre, etc. for SysVol?
** A cluster file system with Samba requires CTDB to be able to do it safely. And CTDB and AD DC are incompatible. Please check on the [https://TTsamba.org/archive//DDAASSH/17854/l reply] /m Samba Developer on the mailing list .

* osync logs that an error email was sent, but it wasn't received.
**This is being looked into and appears to be because the smtp server on the DC isn't set up correctly.

----
`Category Directory`
* Why is the 'REMOTE_RUN_AFTER_CMD' commented out in the sample conf above ?
** There is a problem with sysvolreset at present, once this is fixed, the comment will be removed, along with this question.