Idmap config rid
    <namespace>0</namespace>
<last_edited>2020-05-02T14:29:26Z</last_edited>
<last_editor>Hortimech</last_editor>

===============================

Introduction
===============================

The ``rid`` ID mapping back end implements a read-only API to retrieve account and group information from an Active Directory (AD) Domain Controller (DC) or NT4 primary domain controller (PDC). The back end assigns IDs from an individual per-domain range set in the ``smb.conf`` file and stores them in them in a local database. For details, how the local ID and the relative identifier (RID) are calculated, see the ``idmap_rid(8)`` man page. Because the ``rid`` back end is read-only, it is unable to assign new ID, such as for ``BUILTIN`` groups. Thus this back end cannot be set as ``idmap config *`` default ID mapping back end.

For alternatives, see `Identity_Mapping_Back_Ends|Identity Mapping Back Ends`.

{{Imbox
| type = warning
| text = ID mapping back ends are not supported in the ``smb.conf`` file on a Samba Active Directory (AD) domain controller (DC).<br />For details, see `Updating_Samba#Updating_Samba#Failure_To_Access_Shares_on_Domain_Controllers_If_idmap_config_Parameters_Set_in_the_smb.conf_File|Failure to Access Shares on Domain Controllers If idmap config Parameters Set in the smb.conf File`.

=================
Advantages and Disadvantages of the ``rid`` Back End
=================

Advantages:
* Easy to set up.
* Used IDs are tracked automatically.
* Requires only read access to domain controllers.
* All domain user accounts and groups are automatically available on the domain member.
* No attributes need to be set for domain users and groups.

Disadvantages:
* All users on the domain member get the same login shell and home directory base path assigned.
* User and group IDs are only the same on other domain members using the ``rid`` back end, if the same ID ranges are configured for the domain.
* All accounts and groups are automatically available on the domain member and individual entries cannot be excluded.

=================
Planning the ID Ranges
=================

Before configuring the ``rid`` back end in the ``smb.conf`` file, select unique ID ranges Samba can use for each domain. The ranges must be continuous and big enough to enable Samba to assign an ID for every future user and group created in the domain.

.. warning:

   The ID ranges of the ``*`` default domain and all other domains configured in the ``smb.conf`` file must not overlap. 

=================
Configuring the ``rid`` Back End
=================

* To configure the ``rid`` back end using the ``10000-999999`` ID range for the ``SAMDOM`` domain, set the following in the ``[global]`` section of your ``smb.conf`` file:

 security = ADS
 workgroup = SAMDOM
 realm = SAMDOM.EXAMPLE.COM

 log file = /var/log/samba/%m.log
 log level = 1

 # Default ID mapping configuration for local BUILTIN accounts
 # and groups on a domain member. The default (*) domain:
 # - must not overlap with any domain ID mapping configuration!
 # - must use a read-write-enabled back end, such as tdb.
 idmap config * : backend = tdb
 idmap config * : range = 3000-7999
 # - You must set a DOMAIN backend configuration
 # idmap config for the SAMDOM domain
 idmap config SAMDOM : backend = rid
 idmap config SAMDOM : range = 10000-999999

.. warning:

   Setting the default back end is mandatory.

.. warning:

   For every domain, set these parameters individually. The ID ranges of the ``*`` default domain and all other domains configured in the ``smb.conf`` file must not overlap.

* Configure the template settings. For example, to set ``/bin/bash`` as shell and ``/home/%U`` as home directory path, add:

 # Template settings for login shell and home directory
 template shell = /bin/bash
 template homedir = /home/%U

The values are applied to all users in all domains. Samba resolves the ``%U`` variable to the session user name. For details, see the ``VARIABLE SUBSTITUTIONS`` section in the ``smb.conf(5)`` man page.

* Reload Samba:

 # smbcontrol all reload-config

For further details, see the ``smb.conf(5)`` and ``idmap_rid(8)`` man page.

----
`Category:Active Directory`
`Category:Domain Members`
`Category:NT4 Domains`