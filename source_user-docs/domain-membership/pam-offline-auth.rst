PAM Offline Authentication
    <namespace>0</namespace>
<last_edited>2021-01-26T09:41:24Z</last_edited>
<last_editor>Hortimech</last_editor>

Offline Authentication using winbindd 
------------------------

In order to enable offline authentication configure Samba to use winbind in nsswitch and for PAM (`Authenticating Domain Users Using PAM`)

Then make sure smb.conf contains:

 "winbind offline logon = yes"

{{Imbox
| type = warning
| text = If you are using a Debian based distro there is a bug regarding gencache.tdb, which has been placed in the lock directory ``/run/samba``. This means that it is removed at reboot and hence, winbind offline logon will not work. Until [https://bugzilla.samba.org/show_bug.cgi?id=14618 Bug #14618] is fixed, the workaround is to place ``lock directory = /var/cache/samba`` in your ``smb.conf``.

Enabling offline authentication in pam_winbind 
------------------------

First of all, make sure that you can login using PAM and your windows credentials, e.g. using ssh:
 ssh YOURDOM\\youruser@localhost

You cannot continue if login via PAM (pam_winbind) is not working.

Now, pam_winbind needs to set the offline flag as well, you can do so by either

* adding "cached_login = yes" to /etc/security/pam_winbind.conf. That file should look like this:

 #
 # pam_winbind configuration file
 #
 # /etc/security/pam_winbind.conf
 #
 [global]
 # request a cached login if possible
 # (needs "winbind offline logon = yes" in smb.conf)
 cached_login = yes

This will enable offline ability globally for all applications using PAM. If you want to have more fine grained control about services that use pam_winbind's offline mode then you can do so by

* adding the "cached_login" option into individual pam-configuration files (usualy below /etc/pam.d/$SERVICE)

Testing offline authentication 
------------------------

Start winbindd, authenticate successfully at least once while winbind is online

 /etc/init.d/winbind start

    binfo -K YOURDOM\\youruser%password

Now you can switch winbindd to offline mode by hand (for testing) with the smbcontrol command.

 smbcontrol winbind offline

If you now repeat the command

 wbinfo -K YOURDOM\\youruser%password

You should get
 user_flgs: LOGON_CACHED_ACCOUNT
in the output.

Your system is now prepared to use pam_winbind while offline. Please try to login to your localhost, e.g. using ssh
 ssh YOURDOM\\youruser@localhost

----
`Category Directory`
`Category Members`
`Category Domains`