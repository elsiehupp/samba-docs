Samba Domain Member Port Usage
    <namespace>0</namespace>
<last_edited>2017-02-26T21:01:52Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

=================
Identifying Listening Ports and Interfaces
=================

To identify ports and network interfaces your Samba domain member is listening on, run:

 # netstat -tulpn | egrep "smbd|nmbd|winbind"
 tcp        0      0 127.0.0.1:139               0.0.0.0:*                   LISTEN      43270/smbd          
 tcp        0      0 10.1:. .     0.0.0.0:*       . .I.    43270/smbd          
 tcp        0      0 127.:. .     0.0.0.0:*       . .I.    43270/smbd          
 tcp        0      0 10.1:. .     0.0.0.0:*       . .I.    43270/smbd          
 ...

The output displays that the services are listening on ``localhost`` (``127.&.d. and the network interface with the IP address ``10.99.0.1</cod. O. .aces, the port.code>139/tcp`` and ``445/tcp`` are opened. For further information on the ou.see the ``netstat (8)`` manual page..

To bind Samba to specific interfaces, see `Configure_Samba_to_Bind_to_Specific_Interfaces|Configure Samba to Bind to Specific Interfaces`.

=================
Samba Domain Member Port Usage
=================

{| class="wikitable"
!Service
!Port
!protocol
|-
|End Point Mapper (DCE/RPC Locator Service)
|135
|tcp
|-
|NetBIOS Name Service
|137
|udp
|-
|NetBIOS Datagram
|138
|udp
|-
|NetBIOS Session
|139
|tcp
|-
|SMB over TCP
|445
|tcp
|}

----
`Category:Domain Members`