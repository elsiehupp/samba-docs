Configuring FreeDOS to Access a Samba Share
    <namespace>0</namespace>
<last_edited>2017-02-26T21:01:44Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

===============================

Introduction
===============================

This documentation describes how to access Samba shares from FreeDOS using the ``MS Client 3.0</DOOTT

.. note:

    DOS is not an official supported OS by Samba.

=================
Installing FreeDOS
=================

* Download the latest version of the FreeDOS installer CD from http://freedos.org/download//

* Install FreeDOS on your hard drive. 
* the ``user defined</mode.
* the following program packages to install::
* :
* :

=================
Downloading the Network Interface Card Driver and MS Client
=================

* Download the DOS version of your network interface card (NIC) driver.
* un FreeDOS in a VM, use the following NIC models and NDIS2 drivers for DOS::
* "wikitable"
!Hypervisor
!NIC Model
!Driver
|-
|KVM
|RTL8139
|http://realtek.com.tw Select: <cod:oads``/e>Comm/ Network ICs`` / ``Net/face Cont/;/code> / ``10/100M Fast Ethernet/gt; / &lt/CI``/e>Software</code/ Download/>NDIS:or DOS</////
|-
|VirtualBox
|PCNET
|http://OOTTamd.com/enDDAASS/h/utilities /<co/for Windows 3.1``/
|}

* ssary, extract the downloaded driver archive on a Windows or Linux machine.

* Download the two ``MS Client 3.0</installation disks from:
* LAASS:HHftp.microsoft.com/ients//sk3DDAA/Texe/
* LAASS:HHftp.microsoft.com/ients//sk3DDAA/Texe/

* For an easy transfer to the FreeDOS client, create a CD with the NIC driver and the ``MS Client</DOOTT

=================
Installation and Configuration
=================

* Start FreeDOS and insert the CD with the Network Interface Card (NIC) driver and ``MS Client</DOOTT

* Create a temporary directory:
 > mkdir C:

* Create temporary directories for the ``MS Client</disks:
 > mkdir C:K1\
 > mkdir C:K2\

* Unpack the ``MS Client</into the ``DISK*``/OOTT For example, if ``D:ASSHHcode> is your CD drive::
 > cd C:K1\
 > D:SSHH1.EXE
 > cd C:K2\
 > D:SSHH2.EXE

* Create a temporary directory for the NIC driver and copy the files from the CD. For example:
 > mkdir C:\
 > copy D:.* C:\TEMP\NI:

* Install ``MS Client</
* lt;code>SETUP.EXE</:
 > cd C:K1\
 > setup.exe

* ``*Network Adapter not shown on list below</and enter the path to your NIC driver. For example: <cod:MP\NIC\<SSL: >

* the following environment settings::
* :  setti:r environment.
* ::
* ::t;Basic redirec:LAASSHHcode>
* ::>Do not run ne:nt</
* :: >Do Not Logon :lt;/
* :ation::
* ::Link IPX Compatible Transport</protocol
* ::soft TCP/de>/
* :;code>Microsoft NetBEUI</

* ou saved the options, the setup asks for the driver disk. Enter the path to the second disk. For example::
 C:K2\

* Reboot your computer.

* After the reboot, the following error is displayed:
 MS-DOS LAN Manager v2.1 Netbind
 Error:ble to open PROTMAN$
 NET0111:cessing NEMM.DOS  TCP 1.0 not loaded.
    ET0111:cessing NEMM.DOS.  Tiny <nowiki>RFC 1.0</;  not loaded.
 Press any key to continue
 NET0111:cessing NEMM.DOS.  NMTSR 10 not loaded.
 Press any key to continue
 Error 3658:OOTTSYS driver is not installed.

* r is caused by different configuration files used in FreeDOS and MS-DOS. The ``MS Client</setup added the drivers to the ``CONFIG.SYS``/HDOS file. However FreeDOS uses the ``FDCONFIG.SYS``. To/

* he ``edit</tool and open the ``C:\FDCONFI:``/T
* the content of the file with the follwing commands::
 SET DOSDIR=C:
 LASTDRIVE=Z
 BUFFERS=20
 FILES=40

 DOS=HIGH
 DOS=UMB
 DOSDATA=UMB

 DEVICE=C:\JEMMEX.EXE NOEMS X=TEST I=TEST NOVME NOINVLPG
 DEVICEHIGH=C:LP.SYS

 SHELLHIGH=C:\COMMAND.COM C:\FDOS\BI:HHE:1024 /TBAT:

* e changes and close the file.

* Edit the ``AUTOEXEC.BAT</file:
* he ``edit</tool and open the ``C:\AUTOEXE:``/T
* the content of the file with the follwing commands::
 @ECHO OFF
 SET PATH=C:DIR%\BIN
 SET NLSPATH=%DOSDIR%\NLS
 SET TEMP=%DOSDIR%\TEMP
 SET TMP=%TEMP%

 LH FDAPM APMDOS
 LH DOSLFN

 DEVLOAD /DI/E.SYS /H /D:/S5///
 SHSUCDX /SSH/?S/:?FDCD0001,D/002,D /D:?F://

 SET AUTOFILE=%0
 SET CFGFILE=C:.SYS

* ialize the network at startup, append the following commands::

 LH C:DOOTTEXE INITIALIZE
 C:IND.COM
 LH C:DOOTTCOM
 LH C:SR.EXE
 LH C:RFC.EXE
 LH C:R.EXE
 C:FR.EXE

* e changes and close the file.

* If you are using dynamic IP settings, skip the following step:
* gure a static IP::
* he ``edit</tool and open the ``C:\NET\PRO:TINI``/T
* the following settings to use a static IP::
 [TCPIP]
 ...
 DefaultGateway0=0 0 0 0
 SubNetMask0=255 0 0 0
 IPAddress0=0 0 0 0
 DisableDHCP=1
 ...

.. note:

    The IP addresses and network mask options use spaces instead of a dot as octet separator.

* e changes and close the file.

* Reboot your client.

* Ping an IP address in your network to verify the network connection.

=================
Mounting a Samba Share
=================

* To list computers in your network:
 > net view

* To mount a share from a server, run:
 > net use X:\SHARENAME

----
`Category:mbers`
`Category:ns`