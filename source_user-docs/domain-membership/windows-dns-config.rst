Windows DNS Configuration
    <namespace>0</namespace>
<last_edited>2017-02-26T21:01:51Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

__TOC__

===============================

Introduction
===============================

In an Active Directory (AD), DNS is an necessary component to locate domain controllers (DC) and services, such as Kerberos and LDAP. This documentation describes how to set the DNS server setting manually on a Windows operating system. If your configure client IP and DNS settings using a DHCP service, please consult your DHCP server's documentation for how to deploy the setting.

=================
Configuring the DNS Server Settings
=================

You can run the following steps on any Windows operating system that is actively maintained by Microsoft:

* Click ``Start``, enter ``Network and Sharing Center`` into the search field, and start the application.
* If you are running Windows 8.1, enter the string on the Metro screen to search.
* `Image:Search_Network_and_Sharing_Center.png`

* Click ``Change adapter settings``.

* Right-click to the network adapter connected the network running the Active Directory (AD) and select ``Properties``.

* * If your DNS server runs the IPv4 protocol: select ``Internet Protocol Version 4 (TCP/IPv4)``
* :* Select ``Use the following DNS server addresses``
* :* Enter the IP address of the DNS server in the ``Preferred DNS server`` field. Optionally, specify a second DNS server in the ``Alternate DNS server`` field for failover reasons.
* :: Note that all DNS servers must be able to resolve the AD DNS zones.
* :: `Image:Set_DNS_Server_Addresses.png`
* :* Click ``OK``
* * If your DNS server runs the IPv6 protocol: select ``Internet Protocol Version 6 (TCP/IPv6)``
* :* Select ``Use the following DNS server addresses``
* :* Enter the IP address of a DNS server in the ``Preferred DNS server`` field. Optionally, specify a second DNS server in the ``Alternate DNS server`` field.
* :: Note that all DNS servers must be able to resolve the AD DNS zones.
* :* Click ``OK``.

* Click ``OK`` to save the changes.

=================
Testing DNS Resolution
=================

To verify that your DNS configuration is working, see `Testing_the_DNS_Name_Resolution|Testing the DNS Name Resolution`.

----
`Category:Active Directory`
`Category:Domain Members`