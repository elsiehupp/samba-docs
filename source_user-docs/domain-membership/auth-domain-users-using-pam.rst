Authenticating Domain Users Using PAM
    <namespace>0</namespace>
<last_edited>2017-02-26T21:01:43Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

===============================

Introduction
===============================

To enable domain users to log in locally or to authenticate to services installed on the domain member, such as SSH, you must enable PAM to use the ``pam_winbind`` module.

.. warning:

   Incorrect PAM settings can you lock out from your system.

=================
System Requirements
=================

Before enabling the ``pam_winbind`` module:
* On a Samba domain member: Join the machine to the domain and configure the name services switch (NSS). For details, see: `Setting_up_Samba_as_a_Domain_Member#Configuring_the_Name_Service_Switch|Setting up Samba as a Domain Member - Configuring the Name Service Switch`.
* On a Samba Active Directory (AD) domain controller (DC), configure Winbindd. For details, see `Configuring_Winbindd_on_a_Samba_AD_DC|Configuring Winbindd on a Samba AD DC`.

=================
Adding the ``pam_winbind`` Module to the PAM Modules Directory
=================

If you built Samba, you must create a symbolic link to the ``pam_winbind`` module in the PAM modules directory. For details, see `Pam_winbind_Link|pam_winbind Link`.

=================
Configuring PAM
=================

Using Operating System-specific Utilities 
------------------------

If you distribution provides a utility to configure PAM, do not edit the PAM configuration files manually.

Operating system-specific PAM configuration tools:
* Red Hat-based operating systems: ``authconfig-tui`` and ``authconfig``
* Debian-based operating systems: ``pam-auth-update``
* SUSE-based operating systems: ``yast``

See your operating system's documentation for details about using the utilities.

Manually Configuring PAM 
------------------------

To manually configure PAM to enable domain users to authenticate to a service, you must update the service-specific PAM configuration file. For example, to enable SSH authentication for domain users on a Red Hat-based operating system, edit the ``/etc/pam.d/password-auth-ac`` configuration file and add the highlighted configuration entries:

 #%PAM-1.0
 auth        required      pam_env.so
 auth        sufficient    pam_unix.so nullok try_first_pass
 auth        requisite     pam_succeed_if.so uid >= 1000 quiet_success
 **auth        sufficient    pam_winbind.so use_first_pass**
 auth        required      pam_deny.so

 account     required      pam_unix.so broken_shadow
 account     sufficient    pam_localuser.so
 account     sufficient    pam_succeed_if.so uid < 1000 quiet
 **account     [default=bad success=ok user_unknown=ignore] pam_winbind.so**
 account     required      pam_permit.so

 password    requisite     pam_cracklib.so try_first_pass retry=3 type
===============================

 password    sufficient    pam_unix.so sha512 shadow nullok try_first_pass use_authtok
 **password    sufficient    pam_winbind.so use_authtok**
 password    required      pam_deny.so

 session     optional      pam_keyinit.so revoke
 session     required      pam_limits.so
 session     [success=1 default=ignore] pam_succeed_if.so service in crond quiet use_uid
 session     required      pam_unix.so

For further details, see the PAM documentation.

----
`Category:Active Directory`
`Category:Domain Members`
`Category:NT4 Domains`