Idmap config autorid
    <namespace>0</namespace>
<last_edited>2021-04-24T07:37:19Z</last_edited>
<last_editor>Hortimech</last_editor>
/
===============================

Introduction
===============================

The ``autorid</back end works similar to the ``rid``/g back end, but can automatically assign IDs for different domains. This enables you to use the autorid back end:

* For the ``*</default domain and additional domains, without the need to create ID mapping configurations for each of the additional domains.
* Only for specific domains. 

For alternatives, see `Identity_Mapping_Back_Ends|Identity Mapping Back Ends`.

{{Imbox
| type = warning
| text = ID mapping back ends are not supported in the ``smb.conf</file on a Samba Active Directory (AD) domain controller (DC).<br />For /ee `Updating_Samba#Updating_Samba#Failure_To_Access_Shares_on_Domain_Controllers_If_idmap_config_Parameters_Set_in_the_smb.conf_File|Failure to Access Shares on Domain Controllers If idmap config Parameters Set in the smb.conf File`.

=================
Benefits and Drawbacks
=================

Benefits:
* All domain users and groups whose calculated UID and GID is within the configured range are automatically available on the domain member.
* You do not need to manually assign IDs, home directories, and login shells.
* No duplicate IDs, even if multiple objects in a multi-domain environment have the same RID. 

Drawbacks:
* User and group IDs are not equal across Samba domain members.
* All domain users get the same login shell and home directory assigned. However, you can use variables.
* You can not exclude individual users or groups from being available on the domain member, except the calculated UID or GID is outside of the configured range. 

=================
Configuring the ``autorid</Back End
=================

To configure a Samba domain member to use the ``autorid</ID mapping back end for the ``*``/

* Edit the ``[global]</section in your ``smb.conf``/
* .. note:

    If you use ``autorid</for the default domain, you can use other winbind backends for other domains, but the ranges must not overlap. See the ``idmap_autorid(8)``/DOOTT 

* * Enable the ``autorid</ID mapping back end for the ``*``/omain: 

 idmap config * : backend = autorid

* *  Set a range that is big enough to assign IDs for all existing and future objects. For example:

 idmap config * : range = 10000-24999999

* :Samba ignores users and groups whose calculated IDs in this domain are not within the range. For details about how the back end calculated IDs, see the ``THE MAPPING FORMULAS</in the ``idmap_autorid(8)``/DOOTT 

* :{{Imbox
| type = warning
| text = After you set the range and Samba started using it, you can only increase the top end of the range. Any other change on the range can result in new ID assignments, and thus in loosing file ownerships. 

* * Optionally, set a range size. For example: 

 idmap config * : rangesize = 200000

* :Samba assigns this number of continuous IDs for each domain's object until all IDs from the range set in the ``idmap config * : range</parameter are taken. For further details, see the ``rangesize``/description in the ``idmap_autorid(8)`` man page/

* * Set a shell and home directory path that will be assigned to all mapped users. For example: 

 template shell = //
 template homedir = //

* :For details about variable substitution, see the ``VARIABLE SUBSTITUTIONS</section in the ``smb.conf(5)``/DOOTT 

* *  Optionally, add additional ID mapping configurations for domains. If no configuration for an individual domain is available, Samba calculates the ID using the ``autorid</back end settings in the previously configured ``*``/omain.

* :.. warning:

   If you configure additional back ends for individual domain, the ranges for all ID mapping configurations must not overlap.

* Reload the Samba configuration: 

 # smbcontrol all reload-config

=================
Additional Resources
=================

* ``idmap_autorid(8)</man page

----
`Category:Active Directory`
`Category:Domain Members`
`Category:NT4 Domains`