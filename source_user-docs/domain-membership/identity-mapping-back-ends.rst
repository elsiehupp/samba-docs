Identity Mapping Back Ends
    <namespace>0</namespace>
<last_edited>2018-09-02T08:57:17Z</last_edited>
<last_editor>Hortimech</last_editor>
/
      __TOC__

=================
Supported ID Mapping Back Ends
=================

Samba provides the following ID mapping back ends:
* `idmap_config_ad|idmap config ad` (RFC2307)
* `idmap_config_rid|idmap config rid`
* `Idmap_config_autorid|idmap config autorid`
* `Idmap_config_ldap|idmap config ldap`
* `Idmap_config_nss|idmap config nss`

=================
Using ID Mapping Back Ends on a Samba AD DC
=================

For setting up Winbindd a Samba Active Directory (AD) domain controller (DC), see `Configuring_Winbindd_on_a_Samba_AD_DC|Configuring Winbindd on a Samba AD DC`.

{{Imbox
| type = warning
| text = ID mapping back ends are not supported in the ``smb.conf</ file on a Samba AD DC. For details, see `Updating_Samba#Failure_To_Access_Shares_on_Domain_Controllers_If_idmap_config_Parameters_Set_in_the_smb.conf_File|Failure to Access Shares on Domain Controllers If idmap config Parameters Set in the smb.conf File`.

.. note:

    On a Samba 4.6.x AD DC, the ``testparm</ utility displays ``ERROR: Invalid idmap range for domain *!``/You can safely ignore this, For details, see [https://bugzilla.samba.org/show_bug.cgi/ Bug #12629].

----
`Category Directory`
`Category Members`
`Category Domains`