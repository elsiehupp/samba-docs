Idmap config ad
    <namespace>0</namespace>
<last_edited>2020-01-26T11:15:01Z</last_edited>
<last_editor>Hortimech</last_editor>

===============================

Introduction
===============================

The ``ad`` ID mapping back end implements a read-only API to read account and group information from Active Directory (AD). The back end is based on RFC 2307. For further details, see https://www.rfc-editor.org/rfc/rfc2307.txt.

For alternatives, see `Identity_Mapping_Back_Ends|Identity Mapping Back Ends`.

{{Imbox
| type = warning
| text = ID mapping back ends are not supported in the ``smb.conf`` file on a Samba Active Directory (AD) domain controller (DC).<br />For details, see `Updating_Samba#Failure_To_Access_Shares_on_Domain_Controllers_If_idmap_config_Parameters_Set_in_the_smb.conf_File|Failure To Access Shares on Domain Controllers If idmap config Parameters Set in the smb.conf File`.

{{Imbox
| type = warning
| text = If you use the winbind 'ad' backend, you **must** add a gidNumber attribute to the ``Domain Users`` group in AD. You **must** also give any users, that you want to be visible to Unix, a uidNumber attribute. From Samba version 4.6.0 , you can also add a gidNumber attribute containing the gidNumber of a group and, providing smb.conf is set up correctly, this will be used as the users Unix primary group. All of these uidNumber & gidNumber attributes must contain numbers inside the range you set for the ``DOMAIN`` in the Unix domain members ``smb.conf``. 

=================
Advantages and Disadvantages of the ``ad`` Back End
=================

Advantages:
* Central administration of IDs inside Active Directory (AD).
* Consistent IDs on all Samba clients and servers using the ``ad`` back end.
* The required attributes only need creating once, this can be done when the user or group is created
* IDs are only cached locally, they are stored in the AD database on DC's. This means that if the local cache becomes corrupt the file ownerships are not lost.

Disadvantages:
* If the Windows ``Active Directory Users and Computers`` (ADUC) program is not used, you have to manual track ID values to avoid duplicates.
* The values for the RFC2307 attributes are not created automatically, they must be added manually.

Winbind NSS info mode-specific features:
* ``rfc2307``: Individual login shells and home directory paths for users.
* ``template``: The login shells and home directory base paths are the same for all users.

=================
Planning the ID Ranges
=================

Before configuring the ``ad`` back end in the ``smb.conf`` file, you must select unique ID ranges for each domain. The ranges must be continuous and big enough to enable Samba to assign an ID for every future user and group created in the domain.

.. warning:

   The ID ranges of the ``*`` default domain and all other domains configured in the ``smb.conf`` file must not overlap. 

=================
Prerequisites
=================

To enable Samba to retrieve user and group information from Active Directory (AD):

* Users must have, at least, the ``uidNumber`` attribute set. When using the ``rfc2307`` ``winbind NSS info`` mode, user accounts must also have the ``loginShell`` and ``unixHomeDirectory`` set.
* Groups must have, at least, the ``gidNumber`` attribute set.
* Computers, or: 'machine network accounts', must have the ``uidNumber`` attribute set to access shares on samba domain members.
* The Users and Computers Primary Group must have a ``gidNumber`` attribute set.
* The user, computer, and group IDs must be within the range configured in the ``smb.conf`` for this domain.
* User and computer IDs must be unique for all users and computers, and group IDs must be unique for all groups. Duplicate IDs or reusing IDs of previously deleted accounts enable the new user, computer, or group to access files created by the other or previous ID owner. When using the ADUC utility, the user and group IDs are automatically tracked inside AD and incremented when creating a new user or group. 
* Computer IDs (``uidNumber`` attribute) are not automatically tracked inside AD and must be set manually in the ADUC Attribute Editor tab when a computer is joined to the domain.

.. warning:

   If the ``Active Directory Users and Groups`` `Maintaining_Unix_Attributes_in_AD_using_ADUC | (ADUC) utility is used` to assign the UNIX attributes, the NIS extensions have to be installed. For details, see `Setting_up_RFC2307_in_AD|Setting up RFC2307 in AD`.

=================
The ``RFC2307`` and ``template`` Mode Options
=================

**Before Samba version 4.6.0:**

The ``ad`` ID mapping back end supports two modes, set in the ``winbind nss info`` parameter in the ``[global]`` section of the ``smb.conf`` file:

* ``winbind nss info = rfc2307``: All information is read from Active Directory (AD):
* * Users: Account name, UID, login shell, home directory path, and primary group.
* * Groups: Group name and GID.

* ``winbind nss info = template``: Only the following values are read from AD:
* * Users: Account name, UID, and primary group.
* : The login shell and home directory are automatically set by user-independent settings in the ``smb.conf`` file.
* * Groups: Group name and GID

**From Samba version 4.6.0:**

You no longer use the ``winbind nss info`` parameter, it has been replaced by ``idmap config DOMAIN : unix_nss_info``

The ``ad`` ID mapping back end supports two modes, set in the ``idmap config DOMAIN : unix_nss_info`` parameter in the ``[global]`` section of the ``smb.conf`` file:

* ``idmap config DOMAIN : unix_nss_info = yes``: All information is read from Active Directory (AD):
* * Users: Account name, UID, login shell, home directory path, and primary group.
* * Groups: Group name and GID.
;* These settings are set on a DOMAIN basis, this means you can have different settings for each DOMAIN.  
* * If a user lacks the RFC2307 attributes, the login shell and home directory are automatically set by user-independent settings in the ``smb.conf`` file.

* ``idmap config DOMAIN : unix_nss_info = no``: Only the following values are read from AD:
* * Users: Account name, UID, and primary group.
* : The login shell and home directory are automatically set by user-independent settings in the ``smb.conf`` file.
* * Groups: Group name and GID
* * This is the default setting.

There is now a new setting ``unix_primary_group``, this allows you to use another group for the users primary group instead of Domain Users.
* * If this is set with ``unix_primary_group = yes``, the users primary group is obtained from the gidNumber attribute found in the users AD object.
* * If this is set with ``unix_primary_group = no``, the users primary group is calculated via the "primaryGroupID" attribute.
* * The default is 'no'

=================
Configuring the ``ad`` Back End
=================

**Before Samba version 4.6.0:**

* To configure the ``ad`` back end using the ``10000-999999`` ID range for the ``SAMDOM`` domain, set the following in the ``[global]`` section of your ``smb.conf`` file:

 security = ADS
 workgroup = SAMDOM
 realm = SAMDOM.EXAMPLE.COM

 log file = /var/log/samba/%m.log
 log level = 1

 winbind nss info = rfc2307

 # Default ID mapping configuration for local BUILTIN accounts
 # and groups on a domain member. The default (*) domain:
 # - must not overlap with any domain ID mapping configuration!
 # - must use a read-write-enabled back end, such as tdb.
 idmap config * : backend = tdb
 idmap config * : range = 3000-7999
 # - You must set a DOMAIN backend configuration
 # idmap config for the SAMDOM domain
 idmap config SAMDOM:backend = ad
 idmap config SAMDOM:schema_mode = rfc2307
 idmap config SAMDOM:range = 10000-999999

 vfs objects = acl_xattr
 map acl inherit = yes
 store dos attributes = yes

**From Samba version 4.6.0:**

* To configure the ``ad`` back end using the ``10000-999999`` ID range for the ``SAMDOM`` domain, set the following in the ``[global]`` section of your ``smb.conf`` file:

 security = ADS
 workgroup = SAMDOM
 realm = SAMDOM.EXAMPLE.COM

 log file = /var/log/samba/%m.log
 log level = 1

 # Default ID mapping configuration for local BUILTIN accounts
 # and groups on a domain member. The default (*) domain:
 # - must not overlap with any domain ID mapping configuration!
 # - must use a read-write-enabled back end, such as tdb.
 idmap config * : backend = tdb
 idmap config * : range = 3000-7999
 # - You must set a DOMAIN backend configuration
 # idmap config for the SAMDOM domain
 idmap config SAMDOM:backend = ad
 idmap config SAMDOM:schema_mode = rfc2307
 idmap config SAMDOM:range = 10000-999999
 idmap config SAMDOM:unix_nss_info = yes

 vfs objects = acl_xattr
 map acl inherit = yes
 store dos attributes = yes

.. warning:

   Setting the default back end is mandatory.

.. warning:

   You must set the range for every domain, including the ``*`` default domain. You must set the back end and schema mode for every domain, except the ``*`` default domain. The ID ranges of all domains configured in the smb.conf file must not overlap.

* Configure the Winbind NSS info mode:

* * To enable the ``template`` mode and set, for example, ``/bin/bash`` as shell and ``/home/%U`` as home directory path:

 # Template settings for login shell and home directory
 template shell = /bin/bash
 template homedir = /home/%U

* : The settings are applied to all users in each domain that has the ``schema_mode = rfc2307`` parameter set. From Samba 4.6.0, the global template settings can be overwritten on a domain-basis by enabling the ``idmap config *domain_name*:unix_nss_info`` parameter.

* : Samba resolves the ``%U`` variable to the session user name. For details, see the ``VARIABLE SUBSTITUTIONS`` section in the ``smb.conf(5)`` man page.

* By default, Samba sets the Windows primary group as primary group for mapped domain user entries on Unix. The Windows primary group is retrieved from the ``primaryGroupID`` attribute of each user entry, this is usually set to the ``Domain Users`` group RID. This RID is then used to obtain the ``gidNumber`` attribute from the Windows primary group. 

* If you are running Samba 4.6.0 or later, you can optionally configure Samba to use the primary group set in the ``gidNumber`` attribute in the users entry instead. For example, when using the ``Active Directory Users and Computers`` application, this attribute is displayed in the ``UNIX Attributes`` tab. To use the group ID set in the users ``gidNumber`` attribute as primary group for each user instead of the Windows primary group, enable the following parameter in the ``[global]`` section in your ``smb.conf`` file:

 idmap config SAMDOM:unix_primary_group = yes

.. warning:

   Whichever setting you use, the group (or groups) set as the users primary group must have the ``gidNumber`` attribute set. For example, if you only use the ``Domain Users`` group as the primary group for all accounts, then the ``Domain Users`` group must have a ``gidNumber`` attribute set. Winbind is unable to map accounts that use primary groups that do not have the ``gidNumber`` attribute set.

.. warning:

   Whichever setting you use, do not change the users ``primaryGroupID`` attribute, Windows relies on all users being a member of ``Domain Users``. If you require your Unix users to have a primary group other than ``Domain Users``, you should use Samba version 4.6.0 or later. 

* Reload Samba:

 # smbcontrol all reload-config

For further details, see the ``smb.conf(5)`` and ``idmap_ad(5)`` man page.

----
`Category:Active Directory`
`Category:Domain Members`