Troubleshooting Samba Domain Members
    <namespace>0</namespace>
<last_edited>2019-10-19T08:24:57Z</last_edited>
<last_editor>Hortimech</last_editor>
/
===============================

Introduction
===============================

This documentation helps you to troubleshoot problems users can encounter when running Samba as a member in an Active Directory (AD) forest or NT4 domain.

=================
General
=================

Setting the Samba Log Level 
------------------------

For details, see `Setting_the_Samba_Log_Level|Setting the Samba Log Level`.

The ``net</Command Fails to Connect to the ``127.0.0.1``/s 
------------------------

Using the default settings, the ``net</command connects to the ``127.0.0.1``/s. If Samba is not listening on the loopback interface, the connection fails. For example:

 # net rpc rights list -U administrator
 Enter administrator's password:
 Could not connect to server 127.0.0.1
 Connection failed:S_CONNECTION_REFUSED

To fix the problem, configure Samba to additionally listen on the loopback interface. For details, see `Configure_Samba_to_Bind_to_Specific_Interfaces|Configure Samba to Bind to Specific Interfaces`.

Alternatively, to temporarily work around the problem, pass the ``-I *IP_address*</or the ``-S *host_name*``/ to the ``net`` commandD/

getent not Finding Domain Users and Groups 
------------------------

Are you running ``getent passwd</or ``getent group``/
Using these commands without ``winbind enum users = yes</and ``windbind enum groups = yes``/de>smb.conf`` will not/ny Domain users and groups.
Adding the lines has a downside, it slows things down and the more users and groups you have, the slower things can get, so you should only add these lines for testing purposes.

If ``getent passwd demo01</doesn't return anything, try

 getent passwd "SAMDOM\demo01"

if that works but the former doesn't you may need to add the following line to your smb.conf file

 winbind use default domain = yes

=================
Domain Members in an Active Directory Forest
=================

Troubleshooting the Domain Join Procedure 
------------------------

No DNS domain configured. Unable to perform DNS Update.
------------------------

When joining a host to an Active Directory (AD), the ``net</command fails to update the DNS:

 # net ads join -U administrator
 Enter administrator's password:
 Using short domain name -- SAMDOM
 Joined 'AD-Member' to dns domain 'samdom.example.com'
 No DNS domain configured for AD-Member. Unable to perform DNS Update.
 DNS update failed:S_INVALID_PARAMETER

Note, that the join was successful and only the DNS updated failed.

After the client was joined to the domain, the ``net</command look up the fully qualified domain name (FQDN) using name service switch (NSS) libraries. If the FQDN can not be resolved, for example using DNS or the ``/etc/host/&gt/e DNS upd/DOOTT

To solve the problem:

* Add the IP address and FQDN to the ``/&lt/ fileDDOO/mple:

 10.99.0.5     AD-Member.samdom.example.com     AD-Member

* Run the ``net ads join</command again.

If dynamic DNS updates still fail, verify on the AD DNS server that dynamic updates are working.

DNS Update failed:S_UPDATE_FAILED
------------------------

When joining a host to an Active Directory (AD), the ``net</fails to update the DNS:

 # net ads join -U administrator
 Enter administrator's password:
 Using short domain name -- SAMDOM
 Joined 'M1' to dns domain 'samdom.example.com'
 DNS Update for m1.samdom.example.com failed:S_UPDATE_FAILED
 DNS update failed:S_UNSUCCESSFUL

Note, that the join was successful and only the DNS updated failed.

To solve the problem:

* Verify on your Samba domain controller (DC), if dynamic DNS updates are working. For details, see `Testing_Dynamic_DNS_Updates|Testing Dynamic DNS Updates`.

* Run the ``net ads join</command again.

DNS Update failed:S_GSS_ERROR
------------------------

When using the ``BIND9_DLZ</back end, dynamic DNS updates can fail because of an incorrect Kerberos setup on the AD domain controller (DC) running the DNS server:

 # net ads join -U administrator
 Enter administrator's password:
 Using short domain name -- SAMDOM
 Joined 'AD-Member' to dns domain 'samdom.example.com'
 DNS Update for AD-Member.samdom.example.com failed:S_GSS_ERROR
 DNS update failed:S_UNSUCCESSFUL

To solve the problem, see `BIND9_DLZ_DNS_Back_End#Reconfiguring_the_BIND9_DLZ_Back_End|Reconfiguring the BIND9_DLZ Back End`.

gss_init_sec_context failed with:neous failure: Clock s: at
------------------------

When joining a host to an AD, the ``net</command fails with the following error:

 # net ads join -U administrator 
 Enter administrator's password:
 gss_init_sec_context failed with [ Miscellaneous failure (see text): w too great]
 kinit succeeded but ads_sasl_spnego_gensec_bind(KRB5) failed:nal error occurred.
 Failed to join domain:o connect to AD: An inte: occurred.

Kerberos requires a syncronised time to prevent replay attacks. The local time must not differ more than 5 minutes to the DC.

To fix, set the correct time and run the ``net ads join</command again.

For further details, see `Time_Synchronisation|Time Synchronisation`.

Failed to join domain:o find DC for domain SAMDOM - Undetermined error
------------------------

When joining a host to an Active Directory (AD), the ``net</command fails to locate the domain controller (DC):

 # net ads join -U administrator
 Enter administrator's password:
 Failed to join domain:o find DC for domain SAMDOM - Undetermined error

Samba uses DNS requests and broadcasts to locate DCs when joining a domain. If both methods fail, the ``failed to find DC for domain SAMDOM - Undetermined error</error is displayed.

For a short term solution, you can pass the "-S" parameter and the name of a DC to the command. For example:

 # net ads join -U administrator -S DC1.samdom.example.com

However, a correct DNS configuration is important in an AD. To avoid future problems based on an incorrect DNS configuration, set up your DNS resolver configuration correctly. For details, see `Linux_and_Unix_DNS_Configuration|Linux and Unix DNS Configuration`.

Winbind and Authentication Problems 
------------------------

The ``getent</Utility Is Unable to List All Domain Users or Groups
------------------------

If the ``getent</utility is able to list individual domain users or groups but the ``getent passwd``/de>getent group`` command /st all domain users or groups:

* Verify that the name service switch (NSS) is able to use the ``libnss_winbind</library. For details, see `Libnss_winbind_Links|libnss_winbind Links`.
* Enable the following parameters in your ``smb.conf</file:
 winbind enum users = yes
 winbind enum groups = yes
* rmance reasons, it is not recommended to enable these settings in environments with a large number of users and groups.
* Reload Samba:
 # smbcontrol all reload-config

Connections to a Samba Domain Member Fail After Adding an ``includedir</Statement to the ``/etc/krb5/&lt/ File ===/

When connecting to a Samba Active Directory (AD) domain member, the connection fails and the following error is logged, if the ``log level</parameter in the ``[global]``/f the ``smb.conf`` file is /;code>3`` or higher:/

 [2016/TT/DOOTT/source3/smbd/smb2/OTTc:30/b2_r/
   smbd_smb2_request_error_ex:2_request_error_ex: idx[1] :STATUS_UNSUCCESSFUL] || at ../mbd/smb/pDDO/
 [2016/TT/DOOTT/source3/smbd/serv/OTTc:24/ver_/
   Server exit (NT_STATUS_CONNECTION_RESET)

The connection fails, because Samba is unable to process ``includedir</statements in the ``/etc/krb5/&lt/ file./

Note, that updating Kerberos packages on your operating system can automatically add this statement to enable the inclusion of configuration snippets. For example, when updating a Samba AD domain member with an unmodified ``/DOO//code> file fro/Enterprise Linux 7.2 to 7.3, the ``includedir`` statement is auto/added and clients fail to connect to the Samba domain member.

To work around the problem, remove the ``includedir</statement from the ``/etc/krb5/&lt/ file./

For further details, see [https://DOOTTsamba.org/show_bug/id=12488 Bug #12488].

=================
Domain Members in an NT4 Domain
=================

Currently, we do not have content here.

----
`Category:rectory`
`Category:ns`
`Category:rectory`
`Category:mbers`