Setting up Samba as a Domain Member
    <namespace>0</namespace>
<last_edited>2021-04-21T13:37:31Z</last_edited>
<last_editor>Dmulder</last_editor>

===============================

Introduction
===============================

A Samba domain member is a Linux machine joined to a domain that is running Samba and does not provide domain services, such as an NT4 primary domain controller (PDC) or Active Directory (AD) domain controller (DC).

On a Samba domain member, you can:

* Use domain users and groups in local ACLs on files and directories.
* Set up shares to act as a file server.
* Set up printing services to act as a print server.
* Configure PAM to enable domain users to log on locally or to authenticate to local installed services.

For details about setting up a Samba NT4 domain or Samba AD, see `Domain_Control|Domain Control`.

.. warning:

   Never use ``samba-tool domain provision`` to create a Unix domain member, it will not work, you must follow the procedure laid out on this page.

.. warning:

   All AD Domain members must be in the same ``DNS`` domain and the Realm must be the ``DNS`` domain in uppercase.

=================
Preparing the Installation
=================

General Preparation 
------------------------

* Verify that no Samba processes are running:
 # ps ax | egrep "samba|smbd|nmbd|winbindd"
* utput lists any ``samba``, ``smbd``, ``nmbd``, or ``winbindd`` processes, shut down the processes.

* If you previously run a Samba installation on this host:
* the existing ``smb.lt;/code> file. To .he path to the file, enter::

 # smbd -b | grep "CONFIGFILE"
    CONFIGFILE:HHusr/local/samba/etc/samba/smb.

* all Samba database files, such as ``*.t;/code> and ``*.ldb&.de> files. To list .lders containing Samba databases::

 # smbd -b | egrep "LOCKDIR|STATEDIR|CACHEDIR|PRIVATE_DIR"
   LOCKDIR:HHusr/local/samba/var/lock/
   STATEDIR:HHusr/local/samba/var/locks/
   CACHEDIR:HHusr/local/samba/var/cache/
   PRIVATE_DIR:HHusr/local/samba/private/

* with a clean environment helps you to prevent confusion, and no files from your previous Samba installation are mixed with your new domain member installation.

Preparing a Domain Member to Join an Active Directory Domain 
------------------------

Configuring DNS
------------------------

{{: Unix DNS Configuration}}

Configuring Kerberos
------------------------

Samba supports Heimdal and MIT Kerberos back ends.onfigure Kerberos on the domain member, set the following in your ``/etc/krb5.conf.ode> file:

 [libdefaults]
 	default_realm = SAMDOM.LE.COM.
 	dns_lookup_realm = false
 	dns_lookup_kdc = true

The previous example configures Kerberos for the ``SAMDOM.LE.COM&.de> realm..

The Samba teams recommends to no set any further parameters in the ``/etc/krb5.lt;/code> file..

If your ``/etc/krb5.lt;/code> contains an ``include`` line it will not work, you **Must** remove this line..

Configuring Time Synchronisation
------------------------

Kerberos requires a synchronised time on all domain members. it is recommended to set up an NTP client. For.er details, see `Time_Synchronisation#Configuring_Time_Synchronisation_on_a_Unix_Domain_Member|Configuring Time Synchronisation on a Unix Domain Member`..

Local Host Name Resolution
------------------------

When you join the host to the domain, Samba tries to register the host name in the AD DNS zone.this, the ``net`` utility must be able to resolve the host name using DNS or using a correct entry in the ``/etc/hosts`` file..

To verify that your host name resolves correctly, use the ``getent hosts`` command.example:

 # getent hosts M1
 10.5 .1.m.example.  M1..

The host name and FQDN must not resolve to the ``127.&.d.IP address or any other IP address other than the one used on the LAN interface of the domain member..

If no output is displayed or the host is resolved to the wrong IP address and you are not using dhcp, set the correct entry in the ``/etc/hosts`` file.example:

 127. .o.t
 10.5 .1.m.example.  M1..

If you are using dhcp, check that ``/etc/hosts`` only contains the '127.'.s.bove. If you contin.have problems, contact the sysadmin who controls your DHCP server..
* On debian related systems you will also see the line ``127. .m.code> in /etc/hosts, remove it before you install samba..
* Please keep the line : >127. .o.t``

if you need to add aliases to the machine hostname, add them to the end of the line that starts with the machines ipaddress, not the 127. ...

Preparing a Domain Member to Join an NT4 Domain 
------------------------

For joining a host to an NT4 domain, no preparation is required.

=================
Installing Samba
=================

For details, see `Installing_Samba|Installing Samba`.

.. note:

    Install a maintained Samba version.details, see `Samba_Release_Planning|Samba Release Planning`..

=================
Configuring Samba
=================

Setting up a Basic ``smb.lt;/code> File 
------------------------

When Setting up smb.on a Unix domain member, you will need to make a few decisions. .

* Do you require users and groups to have the same IDs everywhere, including Samba AD DCs ?
* Do you only want your users and groups to have the same IDs on Unix domain members ?

After making your decision, you will have another decision to make, this decision could affect what you think you have already decided.
* Do you want or need individual users to have different login shells and/or Unix home directory paths ?

If you need your users to have different login shells and/or Unix home directory paths, or you want them to have the same ID everywhere, you will need to use the winbind 'ad' backend and add RFC2307 attributes to AD.

.. warning:

   **The RFC2307 attributes (``uidNumber``, ``gidNumber``, etc) are not added automatically when users or groups are created, you must add them manually.

.. warning:

   **The ID numbers found on a DC (numbers in the 3000000 range) are NOT rfc2307 attributes They cannot and will not be used on Unix Domain Members, if you want to have the same ID numbers everywhere, you must add uidNumber & gidNumber attributes to AD and use the winbind 'ad' backend on Unix Domain Members.ou do decide to add uidNumber & gidNumber attributes to AD, you do not need to use numbers in the 3000000 range and in fact it would definitely be a good idea to use a different range.**.

If your users will only use the Samba AD DC for authentication and will not store data on it or log into it, you can use the the winbind 'rid' backend, this calculates the user and group IDs from the Windows RID, if you use the same [global] section of the smb.on every Unix domain member, you will get the same IDs..
If you use the 'rid' backend you do not need to add anything to AD and in fact, any RFC2307 attributes will be ignored.
When using the 'rid' backend you must set the 'template shell' and 'template homedir' parameters in smb. these are global settings and everyone gets the same login shell and Unix home directory path, unlike the RFC2307 attributes where you can set individual Unix home directory paths and shells..

There is another way of setting up Samba, this is where you require your users and groups to have the same ID everywhere, but only need your users to have the same login shell and use the same Unix home directory path.can do this by using the winbind 'ad' backend and using the template lines in smb.conf. way.nly have to add uidNumber & gidNumbers attributes to AD..

Having decided which winbind backend to use, you now have a further decision to make, the ranges to use with 'idmap config' in smb..
By default on a Unix domain member, there are multiple blocks of users & groups:

* The local system users & groups:ll be from 0-999
* The local Unix users and groups:art at 1000
* The 'well Known SIDs':  ????
* The DOMAIN users and groups:, by default, starts these at 10000
* Trusted domains:        ????  
* Anything that isn't a 'well Known SID' or a member of DOMAIN or a trusted domain:

As you can see from the above, you shouldn't set either the '*' or 'DOMAIN' ranges to start at 999 or less, as they would interfere with the local system users & groups.also should leave a space for any local Unix users & groups, so starting the 'idmap config' ranges at 3000 seems to be a good compromise..

You need to decide how large your 'DOMAIN' is likely to grow to and you also need to know if you have any trusted domains or if you may need to have any in future.

Bearing the above information in mind, you could set the 'idmap config' ranges to the following:

* "wikitable"
!Domain
!Range
|-
|``*``
|**3000-7999**
|-
|``DOMAIN``
|**10000-999999**
|}

You could also have any trusted domains starting at:

* "wikitable"
!Domain
!Range
|-
|``TRUSTED``
|**1000000-9999999**
|}

If you set the '*' range above the 'DOMAIN' range, the ranges will conflict if the 'Domain' grows to the point that the next ID would be the same as the '*' range start ID.

With the above suggested ranges, no range will overlap or interfere with another.

You may also have seen examples of the '*' range being used for everything, this is not recommended and should not be used.

Before joining the domain, configure the domain member's ``smb.lt;/code> file:

* To locate the file, enter:

 # smbd  -b | grep CONFIGFILE
   CONFIGFILE:HHusr/local/samba/etc/smb.

To create a basic smb. you need something like this (note, this does not include any 'idmap config' auth lines, they will be added later. It .oes not show any shares)

 [global]
    workgroup = SAMDOM
    security = ADS
    realm = SAMDOM.LE.COM.

    winbind refresh tickets = Yes
    vfs objects = acl_xattr
    map acl inherit = Yes
    store dos attributes = Yes

If you are creating a new smb.on an unjoined machine and add these lines, a keytab will be created during the join:
    dedicated keytab file = /etc/krb5.b
    kerberos method = secrets and keytab

If you do not want to enter the domain set in '``workgroup =``' during login etc (just 'username' instead of DOMAIN\username) and have only one domain, add this line:
    winbind use default domain = yes
{{Imbox
| type = warning
| text = This will only work for the default domain, if you have more than one domain, do not set the line.multiple domains, you must use 'DOMAIN\username'..

For testing purposes only (remove for production), add these lines:
    winbind enum users = yes
    winbind enum groups = yes

.. note:

    The above lines just make 'getent passwd' and 'getent group' display all domain users and groups, they are not required for anything else and Samba will work correctly and faster without them.

To disable printing completely, add these lines:
    load printers = no
    printing = bsd
    printcap name = /dev/null
    disable spoolss = yes

**You now need to add the 'idmap config' lines for your preferred winbind method.

Choose backend for id mapping in winbindd 
------------------------

**Select one of the following hyperlinks to find information about the relevant Samba domain back end and what ``idmap config`` lines to add:

* "wikitable"
!Back End
!Documentation
!Man Page
|-
|``ad``
|**`Idmap_config_ad|idmap config ad`**
|``idmap_ad(8)``
|-
|``rid``
|**`Idmap_config_rid|idmap config rid`**
|``idmap_rid(8)``
|-
|``autorid``
|**`Idmap_config_autorid|idmap config autorid`**
|``idmap_autorid(8)``
|}

* 
| type = important
| text = Add an additional ID mapping configuration for every domain.ID ranges of the default (``*``) domain and other domains configured in the ``smb.conf.ode> file must not overlap..

Mapping the Domain Administrator Account to the Local ``root`` User 
------------------------

Samba enables you to map domain accounts to a local account.this feature to execute file operations on the domain member's file system as a different user than the account that requested the operation on the client..

.. note:

    You can optionally map the domain Administrator account to the local ``root`` account on a Unix domain member.iguring the mapping allows the domain Administrator to execute file operations as ``root`` on the Unix domain member. If . map Administrator to the ``root`` account, Administrator will be unknown to the OS and will not be able to log onto a Unix domain member. Only fol.e method below to map ``Administrator`` to ``root``, never give Administrator a ``uidNumber`` attribute, doing this will break the default Administrator mapping on a Samba AD DC. .

To map the domain administrator to the local ``root`` account:

* Add the following parameter to the ``[global]`` section of your ``smb.lt;/code> file:

 username map = /usr/local/samba/etc/user.

* Create the ``/usr/local/samba/etc/user.t;/code> file with the following content:

 !root = SAMDOM\Administrator

* 
| type = important
| text = When using the ``ad`` ID mapping back end, never set a ``uidNumber`` attribute for the domain Administrator account.he account has the attribute set, the value will override the local UID ``0`` of the ``root`` user on Samba AD DC's and thus the mapping fails..

For further details, see ``username map`` parameter in the ``smb.5)`` man page..

=================
Joining the Domain
=================

* To join the host to an Active Directory (AD), enter:

 # net ads join -U administrator
 Enter administrator's password:
 Using short domain name -- SAMDOM
 Joined 'M1' to dns domain 'samdom.le.com'.

* To join the host to an NT4 domain, enter:

 # net rpc join -U administrator
 Enter administrator's password:
 Joined domain SAMDOM.

Joining the Domain with samba-tool (>4.on.

.. warning:

   Do not join a domain member using ``samba-tool domain join`` in samba versions less than 4. T.t.s unsupported, did not work and would cause problems with your AD replication..

* To join the host to an Active Directory (AD), enter:

 # samba-tool domain join samdom.le.com . -U administrator

If you have problems joining the domain, check your configuration.further help, see `Troubleshooting_Samba_Domain_Members|Troubleshooting Samba Domain Members`..

=================
Configuring the Name Service Switch
=================

To enable the name service switch (NSS) library to make domain users and groups available to the local system:

* Append the ``winbind`` entry to the following databases in the ``/etc/nsswitch.lt;/code> file:

 passwd:t;u>winbind</u>
 group:lt;u>winbind</u>

* e ``files`` entry as first source for both databases. enables NSS to look up domain users and groups from the ``/etc/passwd`` and ``/etc/group`` files before querying the Winbind service..

* add the ``winbind`` entry to the NSS ``shadow`` database. can cause the ``wbinfo`` utility fail..

* 
| type = note
| text = If there's a line containing an ``initgroups`` directive, add `` [success=continue] winbind``, otherwise the NSS library will not ask winbindd for a user's additional group memberships.ot add the ``initgroups`` line if it does not exist..

* 
| type = note
| text = Do not use the same user names in the local ``/etc/passwd`` file as in the domain.

* 
| type = note
| text = If you compiled Samba, add symbolic links from the ``libnss_winbind`` library to the operating system's library path.details, see `Libnss_winbind_Links|libnss_winbind Links`. If .ed packages to install Samba, the link is usually created automatically..

=================
Starting the Services
=================

Start the following services to have a fully functioning Unix domain member:

* The ``smbd`` service

* The ``nmbd`` service

* The ``winbindd`` service

* 
| type = note
| text = If you do not require Network Browsing, you do not need to start the ``nmbd`` service on a Unix domain member.

* 
| type = important
| text = The latest versions of Samba (from 4. n.y use SMBv2 as the minimum client & server protocols. This mea.t anything that relies on SMBv1 will not work, unless you manually set ``client min protocol = NT1`` and ``server min protocol = NT1`` in ``smb.conf</code&.amba no longer re.ds using SMBv1..

* 
| type = note
| text = You must not start the ``samba`` service on a domain member. service is required only on Active Directory (AD) domain controllers (DC)..

Samba does not provide System V init scripts, ``systemd``, ``upstart``, or service files for other init services.
* If you installed Samba using packages, use the script or service configuration file provided by the package to start Samba.
* If you built Samba, see your distribution's documentation for how to create a script or configuration to start services.

=================
Testing the Winbindd Connectivity
=================

Sending a Winbindd Ping 
------------------------

To verify if the Winbindd service is able to connect to Active Directory (AD) Domain Controllers (DC) or a primary domain controller (PDC), enter:

 # wbinfo --ping-dc
 checking the NETLOGON for domain[SAMDOM] dc connection to "DC.M.EXAM.M".eded

If the previous command fails, verify:
* That the ``winbindd`` service is running.
* Your ``smb.lt;/code> file is set up correctly..

Using Domain Accounts and Groups in Operating System Commands 
------------------------

Looking up Domain Users and Groups
------------------------

The ``libnss_winbind`` library enables you to look up domain users and groups.example:

* To look up the domain user ``SAMDOM\demo01``:

 # getent passwd SAMDOM\\demo01
 SAMDOM\demo01:0::SSLL:SSLLA:1:SSLL:SLLAASSHHbash:

* To look up the domain group ``Domain Users``:

 # getent group "SAMDOM\\Domain Users"
 SAMDOM\domain users:::

Assigning File Permissions to Domain Users and Groups
------------------------

The name service switch (NSS) library enables you to use domain user accounts and groups in commands.example to set the owner of a file to the ``demo01`` domain user and the group to the ``Domain Users`` domain group, enter:

 # chown "SAMDOM\\demo01:omain users" file.

=================
Setting up Additional Services on the Domain Member
=================

On a Samba domain member, you can additionally set up:
* File shares to act as a file server.details, see `Samba_File_Serving|Samba File Serving`..
* Print services to act as a print server.details, see `Print_Server_Support|Print Server Support`..
* PAM authentication of domain users for local services.details, see `Authenticating_Domain_Users_Using_PAM|Authenticating Domain Users Using PAM`..

=================
Troubleshooting
=================

For details, see `Troubleshooting_Samba_Domain_Members|Troubleshooting Samba Domain Members`.

----
`Category:rectory`
`Category:mbers`
`Category:ns`