Installing Samba
    <namespace>0</namespace>
<last_edited>2017-04-27T16:35:04Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

* `Operating System Requirements`
* * `Package Dependencies Required to Build Samba`
* * `File System Support`
* `Build Samba from Source`
* `Distribution-specific Package Installation`

.. note:

    Install a maintained Samba version. For details, see `Samba_Release_Planning|Samba Release Planning`.
