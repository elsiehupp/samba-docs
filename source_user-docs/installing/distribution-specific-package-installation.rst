Distribution-specific Package Installation
    <namespace>0</namespace>
<last_edited>2021-02-04T16:43:02Z</last_edited>
<last_editor>Dmulder</last_editor>

===============================

Introduction
===============================

The following is a distribution-specific list of commands to install Samba.

Note, that the list of commands is neither provided nor actively verified by the Samba team. If you see any missing packages or incorrect package names, please update the command or send the information to the [https://lists.samba.org/mailman/listinfo/samba Samba mailing list].

=================
Red Hat Enterprise Linux / CentOS / Scientific Linux
=================

Version 7 and 8 
------------------------

 # yum install samba

The ``samba`` package only supports Samba as a domain member and NT4 PDC or BDC. Red Hat does not provide packages for running Samba as an AD DC. As an alternative:
* Build Samba. For details, see `Build_Samba_from_Source|Build Samba from Source`.
* Use 3rd-party packages with AD support from a trusted source.

Version 6 
------------------------

 # yum install samba4

The ``samba4`` package only supports Samba as a domain member and NT4 PDC or BDC. Red Hat does not provide packages for running Samba as an AD DC. As an alternative:
* Build Samba. For details, see `Build_Samba_from_Source|Build Samba from Source`.
* Use 3rd-party packages with AD support from a trusted source.

=================
Debian
=================

 # apt-get install acl attr samba samba-dsdb-modules samba-vfs-modules winbind libpam-winbind libnss-winbind libpam-krb5 krb5-config krb5-user

Note: For a DC you do not need libpam-winbind libnss-winbind libpam-krb5, unless you require AD users to login

Note2: For a DC, you will also need to install ``dnsutils``

=================
Ubuntu
=================

 # apt-get install acl attr samba samba-dsdb-modules samba-vfs-modules winbind libpam-winbind libnss-winbind libpam-krb5 krb5-config krb5-user

Note: For a DC you do not need libpam-winbind libnss-winbind libpam-krb5, unless you require AD users to login

Note2: For a DC, you will also need to install ``dnsutils``

=================
FreeBSD
=================

 # pkg install net/samba44

Note: If you want to use the ``idmap_ad`` Winbind back end (on e.g. an AD Member Server), you have to build the port by hand and select the **EXP_MODULES** configuration option!

=================
SUSE Linux Enterprise / openSUSE
=================

 # zypper install samba samba-winbind samba-ad-dc