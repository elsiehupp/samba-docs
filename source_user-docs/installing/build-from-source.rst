Build Samba from Source
    <namespace>0</namespace>
<last_edited>2020-09-22T08:55:23Z</last_edited>
<last_editor>Hortimech</last_editor>

===============================

Introduction
===============================

Most users are running `Distribution-specific_Package_Installation|packages shipped with their distribution` or from 3rd parties, such as SerNet ([http://www.samba.plus Samba+]/[http:SSLLAASS:HHwww.samba.plus/older-packages/ Enterprise]). However, in some situations you decide to compile Samba yourself, like
* outdated packages are shipped with your distribution
* no packages are available for your distribution or OS
* you want to apply a patch from a developer to fix a problem before a new version is released

Compiling Samba requires only a few steps:
* Install all dependencies. See `Package Dependencies Required to Build Samba`.
* Download the sources from [https://www.samba.org samba.org]
* Extract the source package
* Run
* gt;./configure``
* gt;make``
* gt;sudo make install``

You can run most of the required steps in this documentation without root privileges. If root permissions are required, the command is prefixed with ``sudo``. Please see the ``sudo (8)`` manual page and your distribution documentation how to set up ``sudo``.

The following documentation is valid for every type of Samba installation, like an Active Directory (AD) domain controller (DC), a domain member (AD and NT4 domain), an NT4 PDC, and standalone server.

=================
Samba Operating System Requirements
=================

To build Samba, install all required libraries and tools first. See `Operating_System_Requirements|Operating System Requirements`.

=================
Obtaining Samba
=================

Stable Version (Recommended) 
------------------------

Always download the latest version of Samba from https://www.samba.org/. 

Samba maintains three series of minor versions. For a maximum of security and stability, the Samba team recommends that you only install the latest available version of a series. Older versions do not contain the latest bug and security fixes. For further information, see `Samba_Release_Planning|Samba Release Planning`.

.. note:

    In case if you are asking for help on the [https://lists.samba.org/mailman/listinfo/samba Samba Mailing List] and not running the latest version, the list members usually advice you to update your installation first to verify that the bug has not been fixed in the meantime.

To download, use a tool like ``wget``. For example:

 $ wget <nowiki>https://download.samba.org/pub/samba/stable/samba-x.y.z.tar.gz</nowiki>

Development Versions 
------------------------

**Do not use a development version in production!**

To download a development version, use ``git`` to clone the repository. For details, see `Creating a Samba patch series|Using Git for Samba Development`.

Before a new major version is released, the Samba team publishes release candidates for testing purposes. You can download release candidates from https://download.samba.org/pub/samba/rc/

=================
Extracting the Source Package
=================

To extract the downloaded source package, run:

 $ tar -zxf samba-x.y.z.tar.gz

=================
configure
=================

Change into the directory with the extracted sources:

 $ cd samba-x.y.z/

The ``configure`` script is located in the root of the sources directory. The main purpose of the script is to create a ``Makefile`` which is used by the command ``make``. The ``configure`` script enables you to set various options, like installation paths. If you do not want to `#Customizing_the_Configure_Options|customize` any paths, and enable or disable parts of Samba, run the following command without any option:

 $ ./configure

If the command fails, read the error message and fix the problem. One common problem are missing dependencies. For details, see `Operating_System_Requirements|Operating System Requirements`.

The following example shows the output, if the ``configure`` script cannot find the GnuTLS headers:

 Checking for gnutls >= 1.4.0 and broken versions : not found
 /usr/src/samba-x.y.z/source4/lib/tls/wscript:CC: Buildin: the AD DC requires
 GnuTLS (eg libgnutls-dev, gnutls-devel) for ldaps:// support and for the BackupKey protocol

If the ``configure`` script exits without an error, you see the following output:

 'configure' finished successfully (1m2.432s)

Continue with the ```#make|make``` step, if no error has occurred.

Python
------------------------

If full python development headers are not installed you may see

 Checking for header Python.h   : Distutils not installed? Broken python installation? Get python-config now! 
 The configuration failed

Here you have three choices:
* Add the following build settings to './configure':
 --disable-python --without-ad-dc

This does mean that you will not be able to provision Samba as an AD DC.

* (for Samba 4.10) build with python2 with
 PYTHON=python2 ./configure
 PYTHON=python2 make -j
* install the Python 3.x development package for your system (eg python3-devel, python36-devel or python3-dev)

Customizing the Configure Options 
------------------------

While running the ``configure`` script without any options is sufficient to build Samba, you can customize installation paths and enable or disable features. To display the list of options, run:

 $ ./configure --help

The output shows two major kind of options:
* `#enable.2Fdisable.2Fwith.2Fwithout_options|--enable/--disable and --with/--without options`
* `#Installation_directories|installation directories`

enable/disable/with/without Options
------------------------

The ``configure`` script provides several ``--enable-*`` / ``-disable-*`` and ``--with-*`` / ``--without-*`` options. They allow you to enable and disable features. Each option you can turn to its opposite if you use ``--enable`` instead of ``--disable``, ``--with`` instead of ``--without``, and the other way around.

* <u>Example 1:ASSHHu> Disable CUPS support (enabled by default)::

 --enable-cups
           Build with cups support (default=yes)

 you do not require CUPS support, disable the feature passing the ``--disable-cups`` option to the ``configure`` command::

 $ ./configure ... --disable-cups

* <u>Example 2:ASSHHu> Compile Samba without AD DC capabilities::

 --without-ad-dc
           disable AD DC functionality (enables Samba 4 client and Samba 3 code base).

* EIf you build Samba for a host that should not act as an AD DC, you can disable this feature passing the ``--without-ad-dc`` option to the ``configure`` command::

 $ ./configure ... --without-ad-dc

* <u>Example 3:ASSHHu> Build Samba with debug information::

 $ ./configure ... --enable-debug 

**Warning: Do not enable or disable features if you have not fully understood the cause of the option! Changing options can prevent your Samba installation from working as expected!**

Installation Directories
------------------------

If you use the default settings, the binaries, configuration files, libraries, and other files are installed in the ``/usr/local/samba/`` directory. This enables you to keep the complete Samba installation in one location. However, you can specify individual paths. For example:

* To install the daemon binaries like ``smbd`` and ``samba`` in the ``/sbin/`` directory instead of ``/usr/local/samba/sbin/``, run:
 $ ./configure ... --sbindir=/sbin/

* To set the default path to the ``smb.conf`` file to ``/etc/samba/`` instead of ``/usr/local/samba/etc/``, run:
 $ ./configure ... --sysconfdir=/etc/samba/

* To store the Samba man pages in the ``/usr/share/man/`` directory, run:
 $ ./configure ... --mandir=/usr/share/man/

=================
make
=================

To start the compilation, run

 $ make

The ``make`` command is able to run multiple jobs in parallel. For example, to run 2 ``make`` sub-tasks at the same time, run:

 $ make -j 2

If the compilation exits without an error, you see the following output:

 Waf: Leaving directory `/usr/src/samba-x.y.z/bin'
 'build' finished successfully (9m3.667s)

=================
make test (Optional)
=================

This optional step runs the Samba self test suite.

 $ make test

.. note:

    You can only run the self test suite, if you built Samba using the ``--enable-selftest`` option.

=================
make install
=================

To install the compiled software, you require ``root`` permissions to write to the destination directories and set the correct permissions.

To install Samba, run:

 $ sudo make install 

If the installation exits without an error, you see the following output:

 Waf: Leaving directory `/usr/src/samba-x.y.z/bin'
 'install' finished successfully (18.243s)

=================
Adding Samba Commands to the $PATH Variable
=================

If you built Samba, add the directories containing the commands to the beginning of your ``$PATH`` variable. For example:

 export PATH=/usr/local/samba/bin/:Husr/local/samba/sbin/:$PATH:

To permanently update your ``$PATH``, see your distribution's documentation.

=================
Upgrading a Self-compiled Samba Installation
=================

To update a self-compiled Samba installation, run the same steps like for a new installation while using the same ``configure`` options.

=================
Applying a Patch
=================

Some situations require that you apply a patch to Samba. For example, a bug has been fixed and you you cannot wait until the new Samba version is released. To apply the patch to the Samba sources, run:

* Change into the Samba sources directory.
 $ cd samba-x.y.z/

* Download the patch. For example:
 $ wget -O /tmp/patch.txt <nowiki>https://bugzilla.samba.org/attachment.cgi?id=...</nowiki>

* Apply the patch to the sources:
 $ patch -p 1 < /tmp/patch.txt

* Recompile and install Samba. See `Updating Samba`.

=================
Additional information
=================

Viewing Built Options of an Existing Installation 
------------------------

To display the options used to built Samba, run

 $ smbd -b

=================
Using an init script to manage the Samba AD DC Service
=================

{{: the Samba AD DC Service Using an Init Script}}

=================
Using systemd to manage the Samba AD DC Service
=================

{{: the Samba AD DC Service Using Systemd}}