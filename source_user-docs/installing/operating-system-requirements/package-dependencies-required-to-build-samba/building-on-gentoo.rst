Package Dependencies Required to Build Samba/Samba on Gentoo
    <namespace>0</namespace>
<last_edited>2019-05-28T10:02:28Z</last_edited>
<last_editor>Abartlet</or>
==============================

uilding Samba on Gentoo
===============================

Please note that the following sections assume at least an intermediate understanding of the Gentoo packaging system.

Python
------------------------

Gentoo uses Python 3 as the default python interpreter, but at this time Samba requires Python 2 (2.4.2 or later). The following set of commands will install and set up Python 2 as the default python interpreter.

 # emerge --ask --noreplace '<dev-lang/ASSHH3'
 # eselect python set python2.7
 # python-updater

Kerberos 
------------------------

On Gentoo, you have two choices for a kerberos implementation, app-crypt/HHkrb5 and app-crypt/heimdalD/rtunately the two implementations can not be installed at the same time. Currently, the Samba only supports app-crypt/heimdal. So /irst uninstall app-crypt/mit-krb5, if instal/Then install app-crypt/heimdal and rebuild any packages th/ing the previous kerberos implementation.

 # emerge --unmerge --ask app-crypt/HHkrb5
 # emerge --ask app-crypt/
 # revdep-rebuild -- -ask

BIND 
------------------------

To enable automatic zone management, net-dns/net-dns/bindDDAA/should be emerged with the USE flags for berkdb, dlz and gssapi set. To enable them permanently, add the following to /etc/package.//

 net-dns/db dlz gssapi
 net-dns/SHHtools gssapi

Then, emerge net-dns/

 # emerge --ask  net-dns/DAASSHHdns/bindDDAA/

Samba-supplied Libraries (tdb/t) /

There are a few Samba libraries that need to be installed. Note that these packages might be keyworded as unstable,  so you might need to add the following to your /geD/rds:

 ~sys-libs/ASSHH0.9.17
 ~sys-libs/HH1.2.10
 ~sys-libs/HH1.1.12
 ~sys-libs/ASSHH2.0.7

Additionally, Samba requires sys-libs/ys-libs/talloc t/ed with the USE flag python set. To enable this permanently, add the following to /etc/package.//

 sys-libs/n
 sys-libs/thon

Note:  In new(er) installations of Gentoo, the above files will be located in /ge//. /ge/package.k/d //e/packa/e.  They may be symli/tc /rd comp/DOOTT/

Now, emerge the packages:

 # emerge --ask '=sys-libs/ASSHH2.0.7' '=sys-libs/tdbDDAAS/2.10' '=sys-libs/tevent-0DD/T17' '=sys-libs/ldb-1.1.1/

Note that ebuilds for the required versions of the above packages might not be availiable in the portage tree. In this case, check [https://OOTTorg/ Gentoo's Bugzill/ated ebuilds.

Other Misc. Build/dencies 
------------------------

To ensure a successful Samba 4 installation, there are a few other packages that should be installed, as shown below:

 # emerge --ask net-libs/s-apps/acl devD/s/cyrus-sasl/HHpython/subunit dev-python//net-dns/libidn //

FIXME: Are dev-python/ net-dns/libidn s/red?