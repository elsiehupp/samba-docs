Package Dependencies Required to Build Samba
    <namespace>0</namespace>
<last_edited>2021-03-26T06:47:23Z</last_edited>
<last_editor>Abartlet</last_editor>

=================
Operating System-independent Overview
=================

The following is an operating system-independent list of libraries and utilities <u>required to build</u> and install Samba. Depending on your distribution, the name of packages can differ. Usually library packages are named ``lib*-devel`` or ``lib*-dev``. For a list of distribution specific package installation commands, see `#Distribution-specific_Packages_Required_to_Build_Samba|Distribution-specific Packages Required to Build Samba`.

.. note:

    If you do not want to build Samba yourself, see `Distribution-specific_Package_Installation|Distribution-specific Package Installation`.

Mandatory 
------------------------

{| class="wikitable"
!Libraries and utilities
!Required for
|-
|python3
|Several utilities, such as ``samba-tool`` and the build system (`Waf|Waf`), are written in Python 3.x.
|-
|perl
|
|-
|Parse::Yapp
|Used in PIDL, our IDL compiler.
|-
|acl
|Required only on `Active_Directory_Domain_Controller|Samba Active Directory domain controllers` and member servers using `Setting_up_a_Share_Using_Windows_ACLs|Windows ACLs`.
|-
|xattr
|Required only on `Active_Directory_Domain_Controller|Samba Active Directory domain controllers` and member servers using `Setting_up_a_Share_Using_Windows_ACLs|Windows ACLs`.
|-
|gnutls >= 3.4.7
|Required for cryptography
|-
|zlib
|Provides the crc32 checksum across Samba and compression for DRSUAPI (AD replication)
|}

Optional 
------------------------

{| class="wikitable"
!Libraries and utilities
!Required for
|-
|krb5-devel
|MIT Kerberos support (except a very bare-bones file server and if not using internal Heimdal). Requires MIT Kerberos 1.15.1 or later.
|-
|krb5-server
|MIT Kerberos support (Samba as an AD DC if not using the internal Heimdal). Requires MIT Kerberos 1.15.1 or later.
|-
|blkid
|
|-
|dbus
|vfs_snapper
|-
|jansson-devel
|Audit logging (Samba 4.7 and later), Samba AD DC
|-
|readline
|
|-
|bsd or setproctitle
|Process title updating support.
|-
|xsltproc or docbook
|Man pages and other documentation.
|-
|pam-devel
|PAM support. For example, to `Authenticating_Domain_Users_Using_PAM|authenticate domain users using PAM`.
|-
|cups
|`Setting_up_Samba_as_a_Print_Server#CUPS|CUPS printer sharing support`.
|-
|openldap
|`NT4_Domains|NT4 Domains` support, including the `Migrating_a_Samba_NT4_Domain_to_Samba_AD_(Classic_Upgrade)|Samba NT4 to AD migration (Classic Upgrade)`.
|-
|python3-markdown
|For samba-tool domain schemaupgrade
|-
|patch
|For samba-tool domain schemaupgrade
|-
|gpgme-devel
|For reading passwords in samba-tool user passwordsync from encrypted cleartext
|-
|python3-gpg or python3-gpgme
|For storing passwords for samba-tool user passwordsync in encrypted cleartext
|-
|flex
|Generates C source from the .l lexical analyzer files in our source tree.  Used when building the embedded Heimdal or spotlight support
|}

Selftest 
------------------------

The following additional packages / utilities are required only for running some tests.

{| class="wikitable"
!Libraries and utilities
!Required for
|-
|bash
|Some blackbox tests are bash-specific
|-
|python3-iso8601
|Used by our selftest and required from the system in samba 4.14 and later.
|-
|python3-cryptography
|Used by our selftest to test Kerberos
|-
|python3-asn1
|Used by our selftest to test ASN.1 protocols like LDAP and Kerberos
|}

===============================
Packages Required to Build Samba
===============================

{{:Verified Package Dependencies}}

Manually maintained Distribution-specific Package lists
------------------------

**This list is for older Samba versions (4.10 and earlier) and distributions not included in the table above**

.. warning:

   The following list of commands is neither provided nor actively verified by the Samba team. Additionally, it might be possible that you require additional or less packages than shown in the later commands - depending on the purpose you install Samba.

Samba Active Directory Domain Controller
------------------------

The following installation commands include the BIND DNS server. If you are using the Samba internal DNS server, omit the BIND package(s). However, you require the package containing the ``nsupdate`` utility to enable dynamic DNS support.

 Debian / Ubuntu =
===============================

------------------------

 # apt-get install acl attr autoconf bind9utils bison build-essential \
   debhelper dnsutils docbook-xml docbook-xsl flex gdb libjansson-dev krb5-user \
   libacl1-dev libaio-dev libarchive-dev libattr1-dev libblkid-dev libbsd-dev \
   libcap-dev libcups2-dev libgnutls28-dev libgpgme-dev libjson-perl \
   libldap2-dev libncurses5-dev libpam0g-dev libparse-yapp-perl \
   libpopt-dev libreadline-dev nettle-dev perl perl-modules pkg-config \
   python-all-dev python-crypto python-dbg python-dev python-dnspython \
   python3-dnspython python-gpgme python3-gpgme python-markdown python3-markdown \
   python3-dev xsltproc zlib1g-dev liblmdb-dev lmdb-utils

Notes:
* Before Debian 8 and Ubuntu 14.04, ``libgnutls28-dev`` was known as ``libgnutls-dev``.
* perl-modules was replace by perl-modules-5.24 on Debian 9
* perl-modules was replace by perl-modules-5.26 on Ubuntu 17.10
* python-gpgme and python3-gpgme were replaced by python-gpg and python3-gpg on Debian 9 and Ubuntu 17.10
* If you are building Samba on a system that uses systemd, you will also require the libsystemd-dev package
* To use a `Running a Samba AD DC with MIT Kerberos KDC | MIT Kerberos KDC`, you will need ``libkrb5-dev`` and ``krb5-kdc``, version 1.15.1 or greater.

 Red Hat Enterprise Linux 8 / CentOS 8  =
===============================

------------------------

Install the following packages to build Samba as an Active Directory (AD) domain controller (DC) on a minimal Red Hat Enterprise Linux (RHEL) 8 or CentOS 8 installation:

 # yum install docbook-style-xsl gcc gdb gnutls-devel gpgme-devel jansson-devel \
       keyutils-libs-devel krb5-workstation libacl-devel libaio-devel \
       libarchive-devel libattr-devel libblkid-devel libtasn1 libtasn1-tools \
       libxml2-devel libxslt lmdb-devel openldap-devel pam-devel perl \
       perl-ExtUtils-MakeMaker perl-Parse-Yapp popt-devel python3-cryptography \
       python3-dns python3-gpg python36-devel readline-devel rpcgen systemd-devel \
       tar zlib-devel

To install all required packages, you must enable the following repositories:
{| class="wikitable"
!RHEL 8
!CentOS 8
|-
|``Base``
|``Base``
|-
|``AppStream``
|``AppStream``
|-
|``CodeReady Linux Builder``*
|``PowerTools``
|-
|``EPEL``**
|``EPEL``**
|}
<nowiki>*</nowiki> For further details about the CodeReady Linux Builder repository, see https://access.redhat.com/articles/4348511.

<nowiki>**</nowiki> The Extra Packages for Enterprise Linux (EPEL) repository is not part of the distribution. For further details about EPEL, see https://fedoraproject.org/wiki/EPEL.

For enabling PowerTools repository on CentOS 8, please use following commands:

 # yum -y install dnf-plugins-core
 # yum config-manager --set-enabled PowerTools

If the DC should act as a print server with CUPS back end, additionally install the following package

 # yum install cups-devel

 Red Hat Enterprise Linux 7 / CentOS 7 / Scientific Linux 7 =
===============================

------------------------

Install the following packages to build Samba as an Active Directory (AD) domain controller (DC) on a minimal Red Hat Enterprise Linux 7, CentOS 7, or Scientific Linux 7 installation:

     yum install attr bind-utils docbook-style-xsl gcc gdb krb5-workstation \
        libsemanage-python libxslt perl perl-ExtUtils-MakeMaker \
        perl-Parse-Yapp perl-Test-Base pkgconfig policycoreutils-python \
        python2-crypto gnutls-devel libattr-devel keyutils-libs-devel \
        libacl-devel libaio-devel libblkid-devel libxml2-devel openldap-devel \
        pam-devel popt-devel python-devel readline-devel zlib-devel systemd-devel \
        lmdb-devel jansson-devel gpgme-devel pygpgme libarchive-devel

.. note:

    Red Hat Enterprise Linux 7 does not include all required packages to build a Samba AD DC. Enable the external Extra Packages for Enterprise Linux (EPEL) repository before you install the packages. For details, see https://fedoraproject.org/wiki/EPEL. Enabling the EPEL repository is not requied on CentOS 7 and Scientific Linux 7.

.. note:

    Red Hat Enterprise Linux 7 and deritivites do not provide a GnuTLS version >= 3.4.7, even when EPEL is used.  Users building Samba 4.12 will need to obtain GnuTLS from outside RHEL7 / CentOS7 / EPEL to use Samba 4.12.

If the DC should act as print server (not recommended) with CUPS back end, additionally install:

 # yum install cups-devel

 Fedora =
===============================

------------------------

To install the build dependencies for Samba on Fedora, run the following command:

 # dnf builddep libldb samba

 openSUSE =
===============================

------------------------

 # zypper source-install --build-deps-only libldb1 samba

 Gentoo =
===============================

------------------------

See `Package_Dependencies_Required_to_Build_Samba/Building Samba on Gentoo|Building Samba on Gentoo` 

Samba Domain Member
------------------------

 Red Hat Enterprise Linux 8 / CentOS 8  =
===============================

------------------------

Install the following packages to build Samba as a domain member on a minimal Red Hat Enterprise Linux (RHEL) 8 or CentOS 8 installation:

 # yum install autoconf automake docbook-style-xsl gcc gdb jansson-devel \
       krb5-devel krb5-workstation libacl-devel libarchive-devel \ 
       libattr-devel libtasn1-tools libxslt lmdb-devel make openldap-devel \
       pam-devel python36-devel rpcgen

To install all required packages, you must enable the following repositories:
{| class="wikitable"
!RHEL 8
!CentOS 8
|-
|``Base``
|``Base``
|-
|``AppStream``
|``AppStream``
|-
|``CodeReady Linux Builder``*
|``PowerTools``
|-
|``EPEL``**
|``EPEL``**
|}
<nowiki>*</nowiki> For further details about the CodeReady Linux Builder repository, see https://access.redhat.com/articles/4348511.

<nowiki>**</nowiki> The Extra Packages for Enterprise Linux (EPEL) repository is not part of the distribution. For further details about EPEL, see https://fedoraproject.org/wiki/EPEL.

For enabling PowerTools repository on CentOS 8, please use following commands:

 # yum -y install dnf-plugins-core
 # yum config-manager --set-enabled PowerTools

If the domain member should act as a print server with CUPS back end, additionally install the following package:

 # yum install cups-devel

 Red Hat Enterprise Linux 7 / CentOS 7 / Scientific Linux 7 =
===============================

------------------------

 # yum install autoconf automake gcc gdb krb5-devel krb5-workstation \
       openldap-devel make pam-devel python-devel docbook-style-xsl \
       libacl-devel libattr-devel libxslt 

Samba NT4 PDC
------------------------

To be added.