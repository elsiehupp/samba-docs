Terms and Abbreviations
    <namespace>0</namespace>
<last_edited>2014-07-24T21:06:34Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

=================
A
=================

Access Control Entry (ACE) 
------------------------

Element in an Access Control List (`#Access_Control_List_.28ACL.29|ACL`).

Access Control List (ACL) 
------------------------

Collection of all `#Access_Control_Entry_.28ACE.29|ACE`'s that define the permissions of a share, file, directory, etc.

`File:iew_diagram.png`

Active Directory (AD) 
------------------------

Directory service developed by Microsoft. Samba version 3 could only be member of an AD as file server, Samba since version 4 can also be Domain Controller of an AD. For a detailed description, see [https://en.wikipedia.org/wiki/Active_Directory https://en.wikipedia.org/wiki/Active_Directory].

Active Directory Users and Computers (ADUC) 
------------------------

It is an `#Microsoft_Management_Console_.28MMC.29|MMC` snap-in for managing e. g. user and computer accounts.

=================
B
=================

Backup Domain Controller (BDC) 
------------------------

In an NT4 domain, the BDC is a computer having a copy of the user and groups database. Changes are always done on the `#Primary_Domain_Controller_.28PDC.29|PDC`. The Backup Domain Controller is only read-only. Changes are pushed from the PDC via Master-Slave-Replication to the BDC(s). In case of an outage of an PDC, a BDC can be promoted to an PDC.

Active Directory does not have PDCs/BDCs any more.

=================
C
=================

=================
D
=================

Distributed File System (DFS) 
------------------------

The main use of DFS, is to create an alternative name space (directory tree view), that hides details of the underlying infrastructure from the users. Technically DFS provides access to a shared directory that contains no files, only junctions, and optionally subdirectories with more junctions. 

Junctions are similar to softlinks as known from Unix file systems, but ones that point to shared directories and they can also point to shared directories on other servers.

Domain Name System (DNS) 
------------------------

Distributed database that stores the `#FQDN|FQDNs`, IP addresses, and additional informations about computers and services on the Internet or in Intranets.

`#Active_Directory_.28AD.29|AD` uses `#Domain_Name_System_.28DNS.29|DNS` to also store informations about the AD Domain, for example the list of DCs.

DNS servers allow to retrieve information from DNS, for example to convert domain names to IP addresses and vice versa, or to query which are the DCs of an AD Domain. AD clients need this to find out where they can log on to the AD domain.

Domain 
------------------------

User accounts, computers and other security principals, that are registred and maintained within a central database. In an `#Active_Directory_.28AD.29|Active Directory` domains can be connected via `#Trust|Trusts` in a `#Tree|Tree` or `#Forest|Forest`.

Domain Controller (DC) 
------------------------

Server with Samba/Microsoft `#Active_Directory_.28AD.29|Active Directory` services installed. A `#Domain_Controller_.28DC.29|Domain Controller` is authorative for the domain, it is part of. AD DCs are doing multi-master replication.

In an AD, all Domain Controllers are equal, byside the `#Flexible_Single_Master_Operator_.28FSMO.29|FSMO` roles.

An AD Domain Controller should not to be confused with `#Primary_Domain_Controller_.28PDC.29|PDC`/`#Backup_Domain_Controller_.28BDC.29|BDC`!

=================
E
=================

=================
F
=================

File Replication Service (FRS) 
------------------------

File Replication Service is used to replicate the `#SysVol_share|SysVol` content in Windows Server 2000 and 2003. It was being replaced by Microsoft with `#Distributed_File_System_.28DFS.29|DFS`-R (Distributed File System Replication) in newer versions of Windows Server. Samba does not support SysVol replication with FRS.

Forest 
------------------------

A forest is a group of `#Domain|domains` with `#Trust|trust` between the domains, that are not a within `#Tree|tree`. This is typically created when one company purchases another company, both companies already had domain trees and now a trust is established between them. Forests are often only an intermediate step, later replaced by a tree.

Fully qualified Domain Name (FQDN) 
------------------------

See [https://en.wikipedia.org/wiki/Fully_qualified_domain_name https://en.wikipedia.org/wiki/Fully_qualified_domain_name].

Flexible Single Master Operation (FSMO) 
------------------------

These are the few tasks that are always delegated exclusively to only one single DC of an AD-domain. 

Usually in an `#Active_Directory_.28AD.29|AD` forest with several DCs, there is the rule that all DC tasks can be done by any of the DCs. If there is more than one DC, then any DC that does nothing else but DC can simply be replaced by another DC, and if such a DC fails, this happens automatically, without anything getting lost. 

The FSMO roles are the exceptions to this rule. They are the remainder of the older scheme from NT4, where the DCs were not all equal:d to be one Primary Domain Controller, and all others were Backup Domain Controllers.

In AD there are still a few special tasks that cannot be arbitrarily shared, and that are therefore delegated to one single DC. One example is the allocation of `#Relative_Identifier_.28RID.29|RID`s, because they must be unique. If several DCs would create them, they would have to take special care to never create identical ones. This is much simpler when it is done by only one DC.

Usually all FSMO roles are delegated to the same DC. In a new AD domain the first DC takes all FSMO roles. If that DC is ever replaced, the FSMO roles must be manually transfered to other DCs. For this reason it is important that the admin knows which of the DCs have which of the FSMO roles.

There are five different roles:

* Schema Master (one for the forest)
* Domain Naming Master (one for the forest)
* PDC Emulator (one for each domain)
* RID Master (one for each domain)
* Infrastructure Master (one for each domain)

Functional Level 
------------------------

Please describe.

===============================
G
=================

Global Catalog (GC) 
------------------------

Usually every `#Domain_Controller_.28DC.29|Domain Controller` stores in its own domain `#Partition|partition` only objects of its own Domain. The Global Catalog is a mechanism, that allows to retrieve information about objects from all Domains. This is useful for creating e. g. a list of all users in a `#Forest|Forest` and save the necessity to do this for all Domains separately.

The first Domain Controller in a Forest, always require to have the GC configured as well. For all additional ones, it can be determined if it should act as a GC. Domain Controllers working as a Global Catalog are acting different:

* They store all Objects of all Domains in a forest. This results in a higher replication load.

* GC servers replicate their content with other GC servers in a forest

* The Global Catalog is also LDAP based, like the whole AD. It uses port 3268/tcp (LDAP) and 3269/tcp (LDAPS). Connections to this ports are read only.

* GC servers have special SRV DNS records to allow clients to locate them:

* OOTT_tcp.gc._msdcs.*DNSForestName*: GC serv:orest. Only DC with configured GC register this SRV record.

* OOTT_tcp.*SiteName*._sites.gc._msdcs.*DNSForestName*: GC serv: site *SiteName*. Only DC with configured GC register this SRV record.

* OTT_tcp.*DNSForestName*: GC serv:s domain. This server doesn't have to be a DC, to register this SRV record. Samba currently doesn't support other servers than DCs acting as GC server!

* TT_tcp.*SiteName*._sites.*DNSForestName*: GC serv:s forest in *SiteName*. This server doesn't have to be a DC, to register this SRV record. Samba currently doesn't support other servers than DCs acting as GC server!

Group Policy Object (GPO) 
------------------------

Please describe.

Group 
------------------------

Please describe.

Globally Unique Identifier (GUID) 
------------------------

Every object inside `#Active_Directory_.28AD.29|Active Directory` get assigned a globally unique identifier (GUID) - a 128 value that is uniq across the world and stored in the objectGUID property.

The GUID is used to identify objects in AD. `#Security_Identifier_.28SID.29|SID`s can change. E. g. if a user moves to a different (sub)domain, the user objects gets a new SID, because the other domain has a different SID. But the GUID stays.

=================
H
=================

Host 
------------------------

Computer in a network.

=================
I
=================

=================
J
=================

=================
K
=================

Key Distribution Center (KDC) 
------------------------

Please describe.

Kerberos 
------------------------

Kerberos is a protocol for secure authentication. `#Host|Hosts` joined to a domain uses Kerberos as default authentication protocol, while clients which are not joined or not part of the same trusted environment use `#NT_LAN_Manager_.28NTLM.29|NTLM` for authentication.

AES encrypted tickets are used to verify identities. Kerberos uses port 88/udp.

=================
L = 

Lightweight Directory Access Protocol (LDAP) 
------------------------

Please describe.

Lightweight Directory Access Protocol over SSL (LDAPS) 
------------------------

Acronym for LDAP over SSL.

LDAP Data Interchange Format (LDIF) 
------------------------

Please describe.

=================
M
=================

Microsoft Management Console (MMC) 
------------------------

Graphical interface for managing services and resources under Microsoft Windows. Snap-ins can be used for administering different Windows services, users, etc. An often used snap-in in an `#Active Directory|Active Directory` environment is e. g. `#Active_Directory_Users_and_Computers_.28ADUC.29|ADUC` (Active Directory Users and Computers).

=================
N
=================

Namespace 
------------------------

Please describe.

Naming Context (NC) 
------------------------

Please describe.

NetBios 
------------------------

Please describe.

NetLogon Share 
------------------------

Please describe.

Network Time Protocol (NTP) 
------------------------

Please describe.

NT LAN Manager (NTLM) 
------------------------

NTLM is a propretary authentication protocol developed by Microsoft.

In an Active Directory, NTLM is used to authenticate, when a machine isn't joined to a domain or not in a trusted environment. Otherwise `#Kerberos|Kerberos` is used, what adds greater securiy on a network. 

NTLM has known a security weakness:s can be captured for replay-attacks to the server and reflection attacks to the client.

=================
O
=================

Object Identifier 
------------------------

Please describe.

Organizational Unit (OU) 
------------------------

OUs are a kind of container in an `#Active_Directory_.28AD.29|Active Directory`. It can contain objects like user, groups, computers, etc. Also other OUs can be inside an Organization Unit.

`#Group_Policy_Object_.28GPO.29|Group Policies` can be applied to Organizational Units.

=================
P
=================

Partition 
------------------------

Please describe.

Primary Domain Controller (PDC) 
------------------------

Please describe.

PDC Emulator 
------------------------

Please describe.

=================
Q
=================

=================
R
=================

Replication 
------------------------

Please describe.

Relative Identifier (RID) 
------------------------

Users, groups and computers have a uniq `#Security_Identifier_.28SID.29|SID`. The RID value is appended to the domain SID and uniquely identifies an object in a domain.

Example (1121 is the RID of the object in this example):

* Domain SID:H1-5-21-3134998938-619743855-3616620706

* SID from an object (e. g. a user, group or computer account) in the same domain:SHH1-5-21-3134998938-619743855-3616620706*-1121

Read Only Domain Controller (RODC) 
------------------------

Read Only Domain Controllers are a special kind of `#Domain_Controller_.28DC.29|Domain Controllers`. They host only read-only partitions of the `#Active_Directory_.28AD.29|Active Directory` database.

Typically RODCs are deployed to locations where physical security can't be guaranteed.

RootDSE 
------------------------

Please describe.

Remote Server Administration Tools (RSAT) 
------------------------

Please describe.

=================
S
=================

Security Account Manager (SAM) 
------------------------

Please describe.

Schema 
------------------------

Please describe.

Security Descriptor 
------------------------

A collection of information denoting the ownership, permission and auditing information for a file, folder, or other system entity.

A security descriptor is generally made up of:
* User and group owner identifiers.
* Discretionary `#Access_Control_List_.28ACL.29|Access Control List`.
* System Access Control List, which can be used for auditing access attempts.

Security Principal 
------------------------

Please describe.

Security Identifier (SID) 
------------------------

Every domain has it's own SID. Objects like users and groups belonging to the same domain having an SID, that always starts with the domain SID. Example:

* Domain SID:H1-5-21-3134998938-619743855-3616620706

* SID from an object (user, group, etc.) in the same domain:SHH1-5-21-3134998938-619743855-3616620706*-1121

As you can see, the object SID always starts with the domain SID and ends with an in that domain uniqe `#Relative_Identifier_.28RID.29|RID`.

If users are moved to a different domain in the same forest, then the SID changes to the one from the new domain. The previous one is kept in the attribute sIDHistory, which can hold multiple values. Contrary to SIDs, the `#Globally_Unique_Identifier_.28GUID.29|GUID` never changes in the live of an object - even across domains.

Site 
------------------------

Please describe.

Service Principal Name (SPN)
------------------------

Please describe.

Snap-In 
------------------------

Please describe.

SysVol share 
------------------------

The SysVol share is an important part of `#Active_Directory_.28AD.29|Active Directory` `#Domain_Controller_.28DC.29|Domain Controllers`s in a domain. `#Group_Policy_Object_.28GPO.29|Group Policy Objects` and logon scripts are stored on it and are accessed by `#Domain|domain` computers and users.

The content of SysVol folders is replicated via `#File_Replication_Service_.28FRS.29|File Replication Service (FRS)` across Domain Controllers in the same domain. Samba currently doesn't support SysVol replication!

SysVol structure:

* Domainname *(e. g. samdom.example.com)*
** Policies
*** {31B2F340-016D-11D2-945F-00C04FB984F9} *(Default Domain Policy)*
**** GPT.INI *(contains information about the Default Domain Policy)*
**** MACHINE *(containts the machine part of the Default Domain Policy)*
**** USER *(contains the user part of the Default Domain Policy)*
*** {6AC1786C-016F-11D2-945F-00C04FB984F9} *(Default Domain Controller Policy)*
**** GPT.INI *(contains information about the Default Domain Controller Policy)*
**** MACHINE *(containts the machine part of the Default Domain Controller Policy)*
**** USER *(contains the user part of the Default Domain Controller Policy)*
** scripts *(this folder is also shared as „NetLogon“ and contains e. g. the logon scripts. It's required e. g. for NT4 backward compatibility).

=================
T
=================

Tree 
------------------------

A tree is a group of `#Domain|domains` with `#Trust|trusts` between the domains, that share the same base domain name. This typically is created for separate divisions or for different regional branches of the same company.

Example:uring.samdom.example.com is a subdomain of samdom.example.com

Trust 
------------------------

=================
U
=================

User Principal Name (UPN) 
------------------------

Please describe.

=================
V
=================

=================
W
=================

Windows Internet Naming Service (WINS) 
------------------------

Please describe.

=================
X
=================

=================
Y
=================

=================
Z =