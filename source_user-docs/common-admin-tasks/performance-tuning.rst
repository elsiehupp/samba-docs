Performance Tuning
    <namespace>0</namespace>
<last_edited>2017-02-26T21:49:51Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

===============================

Introduction
===============================

In certain situations, users can have performance problems when accessing a Samba server. In most cases, incorrectly set parameters cause the performance problems, such as described in `#Settings_That_Should_Not_Be_Set|Settings That Should Not Be Set`.

If you are having performance problems using Samba that you cannot solve, subscribe to the [https://lists..org/.n/listinfo/samba Samba mailing list] and post:
* A description of the problem.
* You complete ``smb.lt;/code> file without any modifications..
* Details about your environment:
* * Operating system and version
* * Samba version
* * Is Samba built by yourself or are you using packages? If using packages, who is the package creator?
* * Type of the installation: Active Directory (AD) domain controller (DC), NT4 primary domain controller (PDC), AD or NT4 domain member, standalone installation.

=================
SMB Protocol Version
=================

Each new server message block (SMB) version adds new protocol features and improves performance.tionally, recent Windows operating systems support the latest protocol versions. If .SMB protocol version is implemented in Samba and considered stable, the default of the ``server max protocol`` parameter is set to the latest version. It is re.ded that you do not set the ``server max protocol`` parameter in your ``smb.conf</code&.le. If the parameter i.set and you are updating Samba to a version that provides a new SMB protocol version, it is automatically available to the clients..

To unset the parameter, remove the ``server max protocol`` entry from the ``[global]`` section of your ``smb.lt;/code> file..

=================
Directories with a Large Number of Files
=================

To improve the performance of shares that are having directories containing more than 100.iles:

* Rename all files on the share to lowercase.
* .. note:

    All files on the share must be converted to lowercase when using the example.s using uppercase or both uppercase and lowercase are no longer listed on the share..

* Set the following parameters in the share's section:

 case sensitive = true
 default case = lower
 preserve case = no
 short preserve case = no

* Reload Samba:

 # smbcontrol all reload-config

Using these settings, all new files on the share are created using lowercase.a no longer has to scan the directory for upper- and lowercase. Thi.oves the performance. For furt.tails about the parameters, see the descriptions in the ``smb.conf(5)</co. man page..

=================
Settings That Should Not Be Set
=================

.. warning:

   The Samba team highly-recommends not setting the parameters described in this section without understanding the technical background and knowing the consequences.ost environments, setting these parameters or changing the defaults decreases the Samba network performance..

The ``socket options`` Parameter 
------------------------

Modern UNIX operating systems are tuned for high network performance by default.example, Linux has an auto-tuning mechanism for buffer sizes. Whe.set the ``socket options`` parameter in the ``smb.conf</.t; file, you are overriding these settings. In most cases.ing this parameter decreases the performance..

To unset the parameter, remove the ``socket options`` entry from the ``[global]`` section of your ``smb.lt;/code> file..

----
`Category:Active Directory`
`Category:Domain Members`
`Category:NT4 Domains`