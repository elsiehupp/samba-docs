Setting up a Share Using POSIX ACLs
    <namespace>0</namespace>
<last_edited>2021-02-26T10:36:04Z</last_edited>
<last_editor>Hortimech</last_editor>

===============================

Introduction
===============================

Samba supports shares with POSIX access control lists (ACL) on Unix domain members, they enable you to manage permissions locally on the Samba host using UNIX utilities. The Unix file system must support extended attributes, this will enable you to use extended POSIX ACLs to set multiple users and groups in ACLs - similar to Windows ACLs. For details, see `#Setting_Extended_ACLs|Setting Extended ACLs`.

.. note:

    You are advised that a better option is to use Windows ACLs, this will allow you to set up fine-granular ACLs. For details, see `Setting_up_a_Share_Using_Windows_ACLs|Setting up a Share Using Windows ACLs`.

.. warning:

   If you set share permissions with POSIX ACLs, then you must not use Windows ACLs, Never set the permissions from Windows.

Samba supports shares with POSIX ACLs on:
* Domain members
* NT4 PDC and BDCs
* Standalone hosts

.. warning:

   On a Samba Active Directory (AD) domain controller (DC), Windows ACL support is enabled globally, and therefore shares with POSIX ACLs are not supported. You must use Windows ACLs.

=================
Preparing the Host
=================

Before you are able to create a share, set up Samba. For details, see:
* `Setting_up_Samba_as_a_Domain_Member|Setting up Samba as a Domain Member`
* `Setting_up_Samba_as_an_NT4_PDC_(Quick_Start)|Setting up Samba as an NT4 PDC (Quick Start)`
* `Setting_up_Samba_as_an_NT4_BDC|Setting up Samba as an NT4 BDC`
* `Setting_up_Samba_as_a_Standalone_Server|Setting up Samba as a Standalone Server`

=================
Making Files Executable
=================

Using the default setting, users are only able to execute files, such as ``*.exe`` and ``*.bat``, on a Samba share if they have the POSIX x-bit set. For example, the following file is executable for the ``root`` user and members of the ``Domain Users`` group:

 -rw<u>x</u>r-<u>x</u>--- 1 root "Domain Users" 133160 1. Jan 00:00 /srv/samba/Demo/example.exe

In some scenarios it is necessary to enable users to execute all files on a share, regardless if the x-bit is set. To enable, set in the ``[global]`` or in a specific share section of your ``smb.conf``:

 acl allow execute always = yes

=================
Adding a Share
=================

To share the ``/srv/samba/Demo/`` directory using the ``Demo`` share name:

* Create the directory:

 # mkdir -p /srv/samba/Demo/

* Add the ``[Demo]`` share definition to your ``smb.conf`` file:

 [Demo]
        path = /srv/samba/Demo/
        read only = no

* These are the minimum parameters required to set up a writeable share. Optionally, you can set share permissions. For details, see `#Setting_Share_Permissions|Setting Share Permissions`.

* Reload the Samba configuration:

 # smbcontrol all reload-config

=================
Setting ACLs
=================

Setting Standard UNIX ACLs 
------------------------

The standard access control lists (ACL) on a UNIX operating system supports setting permissions for one owner, one group, and everyone else (other). If you need to set multiple ACLs on a directory, see `#Setting_Extended_ACLs|Setting Extended ACLs`.

For example, to set the owner of the ``/srv/samba/Demo/`` directory to ``root``, grant read and write permissions to the owner and the ``Domain Users`` group, and deny access to all other users, enter:

 # chmod 2770 /srv/samba/Demo/
 # chown root:"Domain Users" /srv/samba/Demo/

.. note:

    Setting the SGID bit (``<u>2</u>770``) automatically inherits the directory's group to all new files and directories created, instead setting it to the user's primary group.

For further details about the permissions, see the ``chmod(1)`` and ``chown(1)`` man page.

Setting Extended ACLs 
------------------------

If your file system supports extended access control lists (ACL), you can use extended POSIX ACLs. They enable you to set permissions for multiple users and groups on a file or directory - similar to Windows ACLs. However, POSIX ACLs are limited to the following general permissions modes:
* None
* Read
* Write
* Full control

For example, to set read, write, and execute permissions for the ``Domain Admins`` group, read and execute permissions for the ``Domain Users`` group, and deny access to everyone else on the ``/srv/samba/Demo/`` directory:

* Add the ``inherit acls = yes`` parameter to the share's configuration. For example:
 [Demo]
        path = /srv/samba/Demo/
        read only = no
        inherit acls = yes
* The ``inherit acls = yes`` parameter enables ACL inheritance of extended ACLs. For further details, see the parameter description in the ``smb.conf`` man page.

* Reload Samba:

 # smbcontrol all reload-config

* Verify that the directory is stored on a file system that supports extended ACLs. For details, see `File System Support`.

* Disable auto-granting permissions for the primary group of user accounts:
 # setfacl -m group::--- /srv/samba/Demo/
 # setfacl -m default:group::--- /srv/samba/Demo/
* The primary group of the directory is additionally mapped to the dynamical ``CREATOR GROUP`` principal. If you use extended POSIX ACLs on a Samba share, this principal is automatically added and you cannot remove it. For further details about the ``CREATOR GROUP`` principal, see [https://support.microsoft.com/de-at/help/243330/well-known-security-identifiers-in-windows-operating-systems Well-known security identifiers in Windows operating systems].

* Set the permissions on the directory:

* * Grant read, write, and execute permissions to the ``Domain Admins`` group:
 # setfacl -m group:"SAMDOM\Domain Admins":rwx /srv/samba/Demo/

* * Grant read and execute permissions to the ``Domain Users`` group:
 # setfacl -m group:"SAMDOM\Domain Users":r-x /srv/samba/Demo/

* * Set permissions for the ``other`` ACL entry to deny access to users that do not match other ACL entries:
 # setfacl -R -m other::--- /srv/samba/Demo/

* These settings are only applied to the directory itself. In Windows, this is converted to ``This folder only``.

* To configure that the same permissions set in the previous step are inherited to new file system objects created in this directory, enter:

 # setfacl -m default:group:"SAMDOM\Domain Admins":rwx /srv/samba/Demo/
 # setfacl -m default:group:"SAMDOM\Domain Users":r-x /srv/samba/Demo/
 # setfacl -m default:other::--- /srv/samba/Demo/

* With this settings, the ``This folder only`` mode for the principals now changed to ``This folder, subfolders, and files``.

The ACLs set in the previous steps are mapped to the following Windows ACLs:

{| class="wikitable"
!Principal
!Access
!Applies to
!Comments
|-
|SAMDOM\Domain Admins
|Full control
|This folder, subfolders, and files
|
|-
|SAMDOM\Domain Users
|Read & execute
||This folder, subfolders, and files
|
|-
|Everyone
|None
|This folder, subfolders, and files
|Samba maps the permissions for this principal from the UNIX ``other`` ACL entry.
|-
|*directory_owner* (Unix User\*directory_owner*) *
|Full control
|This folder only
|Samba maps the owner of the directory to this entry.
|-
|*directory_primary_group* (Unix User\*directory_primary_group*) *
|None
|This folder only
|Samba maps the primary group of the directory to this entry.
|-
|CREATOR OWNER *
|Full control
|Subfolders and files only
|On new file system objects, the creator inherits automatically the permissions of this principal.
|-
|CREATOR GROUP *
|None
|Subfolders and files only
|On new file system objects, the creator's primary group inherits automatically the permissions of this principal.
|}

<nowiki>*</nowiki> Configuring or removing these principals from the ACLs is only supported when using Windows ACLs. For details, see `Setting up a Share Using Windows ACLs`.

For further details, see the ``setfacl`` man page.

=================
Setting Share Permissions
=================

*Optional*: Samba enables you to set permissions on each share which are validated when a user connects.

Access to the content on a share, is controlled using file system access control lists (ACL). For details, see `#Setting_POSIX_ACLs_on_a_Samba_Share|Setting POSIX ACLs on a Samba Share`

Configuring User and Group-based Share Access 
------------------------

Share-based access control enables you to grant or deny access to a share for certain users and groups. For example, to enable all members of the ``Domain Users`` group to access a share while access is denied for the ``example_user`` account, add the following parameters to the share's configuration:

        valid users = +SAMDOM\"Domain Users"
        invalid users = SAMDOM\example_user

The ``invalid users`` parameter has a higher priority than the ``valid users`` parameter. For example, if the ``example_user`` account is a member of the ``Domain Users`` group, access is denied for this account in the previous example.

For further details, see the parameter descriptions in the ``smb.conf(5)`` man page.

Configuring Host-based Share Access 
------------------------

Host-based access control enables you to grant or deny access to a share based on host names, IP addresses, or IP ranges. For example, to enable the 127.0.0.1 IP address, the 10.99.0.0/24 IP range, and the ``GoodHost`` host name to access a share, and additionally deny access for the ``BadHost`` host name, add the following parameters to the share's configuration:

        hosts allow = 127.0.0.1 10.99.0.0/24 GoodHost
        hosts deny = BadHost

The ``hosts deny`` parameter has a higher priority than the ``hosts allow`` parameter. For example, if ``BadHost`` resolves to an IP address that is listed in the ``hosts allow`` parameter, access to this host is denied.

For further details, see the parameter descriptions in the ``smb.conf(5)`` man page.

----
`Category:Active Directory`
`Category:Domain Members`
`Category:File Serving`
`Category:NT4 Domains`