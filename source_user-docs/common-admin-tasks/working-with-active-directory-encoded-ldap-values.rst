Working with Active Directory encoded LDAP values
    <namespace>0</namespace>
<last_edited>2017-02-26T21:49:14Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

Many values in Active Directory LDAP are not stored in a human-friendly format: this page is meant to provide basic tools to encode / decode theses values.

Many encoded values can be easily decoded by using options both "--cross-ncs" and "--show-binary" of ldbsearch and ldbedit commands.

=================
accountExpires
=================

Expiration date/time of an account: https://msdn.microsoft.com/en-us/library/ms675098%28v=vs.85%29.aspx

 <nowiki>
#!/bin/bash

# Returns an input date in the "accountExpires" format
# Input Date format can be something like "2016-03-19 11:58 UTC+1"

inputDate="$1"

# since 1601 to 1970
interval1=$((( 0 - $(date --date=1601-01-01 +%s) )))
# since 1970 to input date
interval2=$(date --date="$inputDate" +%s)
# total * 10 000 000
echo $((( ( interval1 + interval2 ) * 10000000 )))</nowiki>

 <nowiki>
#!/bin/bash

# Converts an encoded "accountExpires" value to a human-readable one 

accountExpires="$1"

timeInSeconds=$((( accountExpires / 10000000 )))
interval1601to1970=$((( 0 - $(date --date=1601-01-01 +%s) )))
timeSince1970=$((( $timeInSeconds - $interval1601to1970 )))
echo $(date --date @"$timeSince1970")</nowiki>

=================
dnsRecord
=================

Base64 binary blobs containing many informations, such as IP address, expiration time...: https://msdn.microsoft.com/en-us/library/ee898781.aspx

 <nowiki>
#!/usr/bin/perl

# Prints data from "dnsRecord" AD attribute

# adapted from script by natxo, VinsWorldcom and choroba
# found on http://perlmonks.org/index.pl?node_id=1152619
# not much tested

use strict;
use warnings;
use Socket;
use MIME::Base64;

my $blob = $ARGV[0];

$blob = decode_base64($blob);

my (
    $dataLength,    # 2 bytes
    $type,          # 2 bytes
    $version,       # 1 byte
    $rank,          # 1 byte
    $flags,         # 2 bytes 
    $serial,        # 4 bytes 
    $ttl,           # 4 bytes 
    $reserved,      # 4 bytes 
    $timestamp,     # 4 bytes
    $data ) = unpack( 'S S C C S L N L L a*', $blob );

print $dataLength, "\n";
print "$type\n";
print "$version\n";
print "$rank\n";
print "$flags\n";
print "$serial\n";
print "$ttl\n";
print "$reserved\n";
print "$timestamp\n";
print inet_ntoa($data) . "\n";</nowiki>

=================
userAccountControl
=================

Contains many account properties: https://msdn.microsoft.com/en-us/library/ms680832%28v=vs.85%29.aspx

ADS_UF_ACCOUNTDISABLE 
------------------------

If the account is disabled or not: bit of value "2".

LDAP filter to search disabled accounts:
 UserAccountControl:1.2.840.113556.1.4.803:=2

----
`Category:Active Directory`