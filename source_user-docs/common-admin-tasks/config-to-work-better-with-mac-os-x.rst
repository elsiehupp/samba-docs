Configure Samba to Work Better with Mac OS X
    <namespace>0</namespace>
<last_edited>2021-02-24T18:20:33Z</last_edited>
<last_editor>StefanKania</last_editor>

Below are suggested parameters to use in smb.conf file of the Samba server to improve operability with Mac OS X clients.
Note that some parameters may not work with your version of Samba - read the smb.conf and vfs_fruit man pages (on Linux) for your system.
Other than those shown in the [TimeMachineBackup] share below, I recommend you include all parameters in the [Global] section of smb.conf. 
For ease of copy > paste, a clean smb.conf section is included at the bottom of this page.

 [Global]
Apple extensions ("AAPL") run under SMB2/3 protocol, make that the minimum (probably shouldn't be running SMB1 anyway...) - defaults to SMB2_2 in Samba 4.11+:
 min protocol = SMB2 
Apple extensions require support for extended attributes(xattr) - defaults to yes in Samba 4.9+:
 ea support = yes
Load in modules (order is critical!) and enable AAPL extensions:
 vfs objects = fruit streams_xattr  
How to store OS X metadata:
 fruit:metadata = stream

For additional setting see the manpage vfs_fruit.

Server icon in Finder (added in Samba 4.5):
 fruit:model = MacSamba
File cleanup:
 fruit:veto_appledouble = no
 ...added in Samba 4.3
 fruit:posix_rename = yes 
 ...added in Samba 4.5
 fruit:zero_file_id = yes
 ..added in Samba 4.8
 fruit:wipe_intentionally_left_blank_rfork = yes 
 fruit:delete_empty_adfiles = yes 

For Spotlight backend indexing using Elasticsearch (added in Samba 4.12):
 [share]
 spotlight backend = elasticsearch
See smb.conf for 4.12 for other Elasticsearch parameters.
Gnome tracker is still available (= tracker) or no indexing (= noindex), the default.

For Time Machine backup share (added in Samba 4.8):
 [TimeMachineBackup]
 vfs objects = fruit streams_xattr
 fruit:time machine = yes

As far as I know, testparm will not validate vfs_fruit parameters. (my server runs an old version of Samba :-), but after you have built your smb.conf, you can check for errors anyway with ``#: testparm`` or ``#: testparm -v`` (which will give you the defaults as well.

Here is the smb.conf code  - NOTE - THIS IS NOT A COMPLETE SMB.CONF!!!
 [Global]
 min protocol = SMB2
 vfs objects = fruit streams_xattr  
 fruit:metadata = stream
 fruit:model = MacSamba
 fruit:posix_rename = yes 
 fruit:veto_appledouble = no
 fruit:wipe_intentionally_left_blank_rfork = yes 
 fruit:delete_empty_adfiles = yes 

 [TimeMachineBackup]
 vfs objects = fruit streams_xattr
 fruit:time machine = yes
 #  fruit:time machine max size = SIZE

From Finder, connect to your Samba server using "smb://User@Server".
Note that TM backups over smb may now be possible with your server.
Other Mac models can be found in "/System/Library/CoreServices/CoreTypes.bundle/Contents/Info.plist".  Use "Quick Look", Xcode or plutil to view or convert plist.