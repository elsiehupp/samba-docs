Changing the IP Address of a Samba AD DC
    <namespace>0</namespace>
<last_edited>2020-07-16T14:03:33Z</last_edited>
<last_editor>Hortimech</last_editor>

__TOC__

=================
Changing the IP Address of an AD DC in a Single-DC domain Environment
=================

{{Imbox
| type = warning
| text = Changing the IP address in a single-DC domain environment can cause problems with your Active Directory (AD). Verify that you have a restorable backup before changing the IP address. For details, see `Back_up_and_Restoring_a_Samba_AD_DC|Back up and Restoring a Samba AD DC`.

For example, to change the IP address of the domain controller (DC) from ``10.99.0.1`` to ``10.88.0.1``:

* Disconnect or shut down clients connected to the AD DC.

* Shut down the ``samba`` service.

* Set the new IP address on the network interface and update the DNS server IP address in the ``/etc/resolv.conf`` and ``/etc/hosts`` files. For details, see your operating system's documentation.

* If you use the ``BIND9_DLZ`` DNS back end:

* * If BIND is bound to the old IP address, update the ``listen-on`` parameter in the ``option{}`` section in the ``/etc/named.conf`` file:
 # listen-on port 53 { 127.0.0.1;  <s>10.99.0.1</s> **10.88.0.1**; };

* * Restart BIND.

* * Verify that BIND is listening on the new IP address:

 # netstat -tulpn | grep ":53"
 tcp        0      0 **10.88.0.1:53**       0.0.0.0:*          LISTEN      1109/named
 tcp        0      0 127.0.0.1:53       0.0.0.0:*          LISTEN      1109/named
 udp        0      0 **10.88.0.1:53**       0.0.0.0:*                      1109/named
 udp        0      0 127.00.1:53        0.0.0.0:*                      1109/named

* If you set the IP address in the ``interfaces`` parameter in the ``[global]`` section of your ``smb.conf`` file, update the address:

 interfaces = 127.0.0.1 <s>10.99.0.1</s> **10.88.0.1**

* Start the ``samba`` service.

* Update the DNS entries:

 # samba_dnsupdate --verbose

* The ``samba_dnsupdate`` utility automatically updates records in the local database, using a file called ``dns_update_list``. This file can be found in the ``private`` directory.

* Verify that the DC's DNS records are resolved correctly:

 # host -t A samdom.example.com
 samdom.example.com has address 10.88.0.1

 # host -t A DC.samdom.example.com
 DC.samdom.example.com has address 10.88.0.1

 # host -t A gc._msdcs.samdom.example.com
 gc._msdcs.samdom.example.com has address 10.88.0.1

* Update your AD clients to use the new IP address of the AD DNS server.

=================
Changing the IP Address of an AD DC in a Multi-DC domain Environment
=================

To change the IP address of an Active Directory (AD) domain controller (DC) in a multi-DC domain environment:

* Demote the DC. For details, see `Demoting a Samba AD DC`.

* Set the new IP address in the DC's network configuration.

* Rejoin the DC to the domain. For details, see `Joining_a_Samba_DC_to_an_Existing_Active_Directory|Joining a Samba DC to an Existing Active Directory`.

* If the DC is a DNS server for AD DNS zones, set the new IP address in all domain member's DNS settings.

----
`Category:Active Directory`