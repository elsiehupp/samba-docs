Configuring Windows Profile Folder Redirections
    <namespace>0</namespace>
<last_edited>2018-05-03T19:45:09Z</last_edited>
<last_editor>RenegadeTech</last_editor>

===============================

Introduction
===============================

Using the default settings, roaming Windows user profiles include folder that can contain a large amount of data, such as ``Documents``, ``Downloads``, and ``Pictures``. When logging in, the data is transferred from the Server to the domain member and back when the user logs out. Folder redirection enables you to redirect paths of folders outside of the Windows user profile to reduce the size of the profile.

Because the user profile can contain sensitive information, you should redirect the folder to a secured area that only the profile owner can access, such as the `Windows User Home Folders|user's home folder`.

=================
Setting Folder Redirections
=================

In an Active Directory 
------------------------

Using group policies, you can assign settings to organizational units (OU) or to a domain. This enables you, for example, to automatically set folder redirections to all users in the OU or domain. If you move the account to a different OU or domain, the settings are removed or updated. Using this way, you do not have to set the redirection manually for each user account.

Using Group Policy Folder Redirection
------------------------

Using a group policy object (GPO) is the preferred way to set folder redirections.

.. note:

    Windows does not support dynamically-generated user home folders provided by the Samba ``[homes]`` section. If you used this way to provide home folders, set up a group policy preference instead. See `#Using_a_Group_Policy_Preference|Using a Group Policy Preference`.

To create a group policy object (GPO) for the domain that automatically redirects profile folders to user's home folder:

* Log in to a computer using an account that is allowed you to edit group policies, such as the AD domain ``Administrator`` account.

* Open the ``Group Policy Management Console``. If you are not having the Remote Server Administration Tools (RSAT) installed on this computer, see `Installing RSAT|Installing RSAT`.

* Right-click to your AD domain and select ``Create a GPO in this domain, and Link it here``.

* `Image:GPMC_Create_GPO.png`

* Enter a name for the GPO, such as ``Folder Redirections``. The new GPO is shown below the domain entry.

* Right-click to the newly-created GPO and select ``Edit`` to open the ``Group Policy Management Editor``.

* Navigate to the ``User Configuration`` &rarr; ``Policies`` &rarr; ``Windows Settings`` &rarr; ``Folder Redirection`` entry.

* Right-click to the folder to redirect, such as ``Documents``, and select ``Properties``.

* Set the following:
* * On the ``Target`` tab:
* :* Setting: ``Basic - Redirect everyone's folder to the same location``
* :* Target folder location: ``Redirect to the user's home directory``
* * On the ``Settings`` tab:
* :* Unselect ``Grant the user exclusive rights.``
* :* Unselect ``Move the contents of Documents to the new location.``
* :* Select ``Also apply redirection to Windows 2000, Windows 2000 Server, Windows XP, and Windows Server 2003 operating systems.``
* :* Select ``Leave the folder in the new location when policy is removed.``
(If you choose to set these options differently and run into problems such as Event ID 502 in the application event log when a user logs in, see [https://support.microsoft.com/en-us/help/2493506/redirecting-the-user-s-documents-folder-to-their-home-directory-fails this Microsoft support article] which boils down to either setting both *Grant user exclusive* and *Also apply to Windows 2000* or neither of them.)

* :`Image:GPME_Folder_Redirection_Documents.png`

* * Click ``OK``.

* Optionally, redirect other folders in the same way.

* Close the ``Group Policy Management Editor``. The GPOs are automatically saved on the ``Sysvol`` share on the domain controller (DC).

* Close the ``Group Policy Management Console``.

The policy is applied to users in domain at the next log in.

Using a Group Policy Preference
------------------------

When you use the Samba ``[homes]`` section to dynamically generate user home folders, you must set registry keys using a group policy preference to redirect folders. If you provide home folders using a different share name, see `#Using Group Policy Folder Redirection|Using Group Policy Folder Redirection`.

To create a group policy preference for the domain that automatically redirects profile folders to user's home folder:

* Log in to a computer using an account that is allowed you to edit group policies, such as the AD domain ``Administrator`` account.

* Open the ``Group Policy Management Console``. If you are not having the Remote Server Administration Tools (RSAT) installed on this computer, see `Installing RSAT|Installing RSAT`.

* Right-click to your AD domain and select ``Create a GPO in this domain, and Link it here``.

* `Image:GPMC_Create_GPO.png`

* Enter a name for the GPO, such as ``Folder Redirections``. The new GPO is shown below the domain entry.

* Right-click to the newly-created GPO and select ``Edit`` to open the ``Group Policy Management Editor``.

* Navigate to the ``User Configuration`` &rarr; ``Preferences`` &rarr; ``Windows Settings`` entry.

* Right-click to the ``Registry`` entry in the navigation and select ``New`` &rarr; ``Registry Item``.

* Set the following:
* * Action: ``Replace``
* * Hive: ``HKEY_CURRENT_USER``
* * Key Path: ``Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders``
* * Value name: For example, to redirect the ``Documents`` folder, enter: ``Personal``
* : For a list of other registry keys of folders you can redirect, see the ``HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders`` entry in your local Windows registry.
* * Value type: ``REG_EXPAND_SZ``
* * Value data: For example: ``\\server\%USERNAME%\Documents``
* : Windows automatically replaces the ``%USERNAME%`` variable with the name of the current user when the policy is applied.

* `Image:GPME_Folder_Redirection_GP_Preference_Documents.png`

* Optionally, redirect other folders in the same way.

* Close the ``Group Policy Management Editor``. The GPOs are automatically saved on the ``Sysvol`` share on the domain controller (DC).

* Close the ``Group Policy Management Console``.

The policy is applied to users in domain at the next log in.

In an NT4 Domain 
------------------------

NT4 policies are only supported by the following Windows versions:
* Windows NT 4.0 - Windows XP
* Windows NT Server 4.0 - Windows Server 2003 R2

To create a folder redirection for the ``Default User Policy`` entry:

* Log in to a computer using an account that is allowed you to edit NT4 policies, such as the NT4 domain ``Administrator`` account.

* Open the ``System Policy Editor`` (poledit.exe). This application is stored on the Windows Server CD-ROM and part of the MS Office 2000 Resource Kit. For further details, see [http://support.microsoft.com/kb/910203 KB910203].

* Select ``Options`` &rarr; ``Policy Template`` and open an ``*.adm`` file that contains policies for folder redirection.
* `Image:Poledit_Opening_an_ADM_File.png`

* Create a new policy or open an existing one.

* Double-click ``Default User``.

* Navigate to the folder redirection. The location depents on the structure of the ADM file you use.

* Select the folder to redirect and enter the path to the destination. For example, to redirect the ``Documents`` folder to ``H:\My Documents``:
* `Image:Poledit_Folder_Redirection_Documents.png`

* Optionally, redirect other folders in the same way.

* Click ``OK``

* Save the policy in the ``\\*PDC_name*\netlogon\ntconfig.pol`` file. Note that all domain users must have permissions to read the file.

The policy is applied to users in domain at the next log in.

----
`Category:Active Directory`
`Category:NT4 Domains`