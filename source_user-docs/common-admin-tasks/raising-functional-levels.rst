Raising the Functional Levels
    <namespace>0</namespace>
<last_edited>2021-04-12T16:47:43Z</last_edited>
<last_editor>Heupink</last_editor>

===============================

Introduction
===============================

The Active Directory (AD) functional levels determine the domain or forest capabilities. For details, see:

* [http://technet.microsoft.com/en-us/library/understanding-active-directory-functional-levels%28WS.10%29.aspx Understanding Active Directory Domain Services (AD DS) Functional Levels]

* [http://blogs.technet.com/b/askds/archive/2011/06/14/what-is-the-impact-of-upgrading-the-domain-or-forest-functional-level.aspx What is the Impact of Upgrading the Domain or Forest Functional Level?]

.. warning:

   If you raise any of the functional levels, you will need to restart the Samba AD DC(s).

=================
Supported Functional Levels
=================

You can set the following functional levels in Active Directory (AD) via samba-tool.

{| class="wikitable"
!Functional Level
!Included in Samba Version
|-
|2012_R2
|4.4 and later*
|-
|2012
|4.4 and later*
|-
|2008_R2
|4.0 and later
|-
|2008
|4.0 and later
|-
|2003
|4.0 and later
|}

<nowiki>*</nowiki> Functional level is included for use against Windows, but **not supported in Samba**. Kerberos improvements from Windows Server 2012 and 2012 R2 are not implemented in Samba.

=================
Raising the Domain Functional Level
=================

Using samba-tool 
------------------------

To raise the domain functional level on a Samba Active Directory (AD) domain controller (DC), use ``samba-tool``. For example, to set the domain functional level to ``2008_R2``:

 # samba-tool domain level raise --domain-level=2008_R2

For a list of supported domain functional levels, see `#Supported_Functional_Levels|Supported Functional Levels`.

Using the Windows Active Directory Domains and Trusts Utility 
------------------------

.. warning:

   Raising the domain functional level using the ``Active Directory Domains and Trusts`` utility is currently not supported.<br />For details, see https://bugzilla.samba.org/show_bug.cgi?id=10360

Run the following steps on a Windows machine having the remote server administration tools (RSAT) installed:

* Log in as domain administrator.

* Open the ``Active Directory Domains and Trusts`` utility.

* Right-click the domain on the left side and select ``Raise Domain Functional Level``.

* `Image:Raise_Domain_Functional_Level.png`

* Select the functional level.

* Click ``OK``.

=================
Raising the Forest Functional Level
=================

Using samba-tool 
------------------------

.. note:

    You can not set the forest functional level higher than the domain functional level.

To raise the forest functional level on a Samba Active Directory (AD) domain controller (DC), use ``samba-tool``. For example, to set the forest functional level to ``2012_R2``:

 # samba-tool domain level raise --forest-level=2012_R2

For a list of supported forest functional levels, see `#Supported_Functional_Levels|Supported Functional Levels`.

Using the Windows Active Directory Domains and Trusts Utility 
------------------------

.. warning:

   Raising the domain functional level using the ``Active Directory Domains and Trusts`` utility is currently not supported.<br />For details, see https://bugzilla.samba.org/show_bug.cgi?id=10360

Run the following steps on a Windows machine having the remote server administration tools (RSAT) installed:

* Log in as domain administrator.

* Open the ``Active Directory Domains and Trusts`` utility.

* Right-click ``Active Directory Domains and Trusts`` on the left side and select ``Raise Forest Functional Level``.

* `Image:Raise_Forest_Functional_Level.png`

* Select the functional level.

* Click ``OK``.

----
`Category:Active Directory`