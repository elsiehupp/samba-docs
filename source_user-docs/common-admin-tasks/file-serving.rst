Samba File Serving
    <namespace>0</namespace>
<last_edited>2017-03-11T00:20:13Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

General information:
* `Setting up a Share Using POSIX ACLs`
* `Setting up a Share Using Windows ACLs`

Advanced share configurations:
* `Windows User Home Folders`
* `Roaming Windows User Profiles`
* `Setting up Samba as a Standalone Server`