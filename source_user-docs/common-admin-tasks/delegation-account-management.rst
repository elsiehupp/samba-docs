Delegation/Account management
    <namespace>0</namespace>
<last_edited>2017-02-26T21:48:00Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

Usually you don't want to be logged in the whole day as Domain Administrator. But to do changes on user accounts and groups, you need special permissions in the AD. Per default, all members of the BuiltIn group "Account Operators" can do this job. So simply add the user/s who should be able do administrate accounts and groups to this group.

But the "Account Operators" group doesn't have permissions, that are required for doing all changes on the "UNIX attributes" tab. To archive this, follow these steps:

* Open the ADUC console as domain administrator.

* Right-click to the container "System" / "RpcServices" and choose "Properties".

* Go to the "Security" tab.

* Click the "Add" button and search for the "Account Operators" group.

* Select permissions "Full control" for this group.

* Click the "Advanced" button.

* Select the "Account Operators" entry, click "Edit" and change the "Apply onto" field to "This object and all child objects" and close all windows with "OK" to save the changes.

----
`Category:Active Directory`