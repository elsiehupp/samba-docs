Delegation/ Machines to a Domain
    <namespace>0</namespace>
<last_edited>2018-07-29T11:13:26Z</last_edited>
<last_editor>Hortimech</last_editor>
/
===============================

Introduction
===============================

Delegating permissions in an Active Directory (AD) enables the administrator to assign permissions in the directory to unprivileged. For example, to enable a help desk employees to join machines to the domain without knowing the domain administrator credentials.

=================
Adding the Delegation
=================

To enable the ``supporters</ group to join and remove machines to and from the domain:

* Open the ``Active Directory Users and Computers</ (ADUC) console as domain administrator.

* Create a new group ``supporters</DOOTT

* Right-click to the ``Computer</ container and select ``Delegate control``/

* Click ``Next</DOOTT

* Click ``Add</ and select the group ``supporters``/and click ``Next``./

* Select ``Create a custom task to delegate</ and click ``Next``/

* Select ``Only the following objects in the folder</ and check ``Computer objects``/from the list. Additionally select the options ``Create selected objects in the folder`` /CCEE``Delete selected objects in this folder``. Cl/CEE``Next``./

* Select ``General</ and ``Property-specific``/Eselect the following permissions from the list.
* ``Reset password</
* ``Read and write account restrictions</
* ``Read and write DNS host name attributes</
* ``Validated write to DNS host name</
* ``Validated write to service principal name</
* ``Write servicePrincipalName</

* Click ``Next</DOOTT

* Click ``Finish</DOOTT

To enable the group to join machines to multiple containers or organizational units (OU), repeat the steps on them.

=================
Revoking the Delegation
=================

To disable members of the ``supporter</ group to join and remove machines to and from the domain:

* Open the ``Active Directory Users and Computers</ (ADUC) console as domain administrator.

* Right-click to the container or organizational unit (OU) you want to revoke the permissions and select ``Properties</DOOTT

* Navigate to the ``security</ tab.

* Remove the ``supporter</ group from the list.

* Click ``OK</DOOTT

----
`Category Directory`