VPN Single SignOn with Samba AD
    <namespace>0</namespace>
<last_edited>2020-04-20T07:05:32Z</last_edited>
<last_editor>Thctlo</last_editor>

=================
Creating a Single Sign On VPN with Samba4 on Ubuntu/Debian Server
=================
These instructions are pretty rough and were written before Samba AD was first released, but they "worked for me" and I hope they give others some guidance. I've tried to go into as much detail as possible (painfully so) but I'm sure there are things that I'm missing. Please expand upon this HOWTO if you do find errors.

Overview 
------------------------

1. The purpose of this guide, is to provide a step by step guidelines how to create a L2TP VPN server, which is fully integrated with the Samba4 Server.

Network Topology == 
2. Before going over how to actually build and configure the VPN server, we need first to understand our network topology. Basically our network consists of a Layer 2 switch, a Firewall Server (which in our case is also the network gateway), one Samba4 Domain Controller and one or more Linux/Windows client machines.

                          NetID                                  --------- Windows XP - 172.16.0.10/24
                      172.16.0.0/24                             /
                          ------                   --------    /
                         |      |                 |        |  /
                         |      |                 |        | /
    nternet----Public-IP--|  FW  |--172.16.0.1/24--| Switch | ------------- Samba4 DC - 172.16.0.2/24
                         |      |                 |        | \
                         |      |                 |        |  \
                          ------                   --------    \
                                                                \
                                                                 ---------- Fedora Linux - 172.16.0.50/24
       
Plese note that the Domain Controller (Samba4) can also be installed on the Firewall itself, but this is strongly discouraged for security reasons.

Service Topology == 
3. An L2TP VPN service is built from different software packages, which are integrated together. For a better understanding how it works, please take a look at the following diagram:

http://www.jacco2.dds.nl/networking/topo.png 

For more info please see [http://www.jacco2.dds.nl/networking/freeswan-l2tp.html Using a Linux L2TP/IPsec VPN server]

Install & Configure Your Samba4 Domain Controller 
------------------------

4. This guide assumes you have one/or more Samba4 Domain Controllers running on your network.
For the purposes of this guide, I will use "DC.Example.com" as our Domain Controller hostname and "Example.com" as our Domain Name. If you are unfamiliar with how to install samba4 on Debian/Ubuntu Server, please see `Setting up Samba as an Active Directory Domain Controller`.

Install & Configure a RADIUS Server 
------------------------

5. Once you have a Samba4 Server up and running, our next step is to install and configure a RADIUS Server as an alternative to the Microsoft [http://en.wikipedia.org/wiki/Internet_Authentication_Service IAS or NPS]. 

There are several Open Source RADIUS implementations. This guide uses [http://freeradius.org/ FreeRADIUS].

Please note the that in our example the FreeRADIUS software is installed on our firewall server. It could also be installed on the domain controller or another server entirely.

6. Install the FreeRADIUS software on your Ubuntu/Debian Server

 sudo apt-get install freeradius freeradius-common freeradius-krb5 freeradius-ldap freeradius-utils

7. Configure the RADIUS server parameters in /etc/freeradius/radiusd.conf (or, /etc/raddb/radiusd.conf) as following:
 prefix = /usr
 exec_prefix = /usr
 sysconfdir = /etc
 localstatedir = /var
 sbindir = ${exec_prefix}/sbin
 logdir = /var/log/freeradius
 raddbdir = /etc/freeradius
 radacctdir = ${logdir}/radacct
 confdir = ${raddbdir}
 run_dir = ${localstatedir}/run/freeradius
 db_dir = ${raddbdir}
 libdir = /usr/lib/freeradius
 pidfile = ${run_dir}/freeradius.pid
 max_request_time = 30
 cleanup_delay = 5
 max_requests = 1024

 listen {
        type = auth
        ipaddr = 172.16.0.1
        port = 0
        interface = eth0
 }
 listen {
        type = auth
        ipaddr = 127.0.0.1
        port = 0
        interface = lo
 }
 listen {
        type = acct
        ipaddr = 172.16.0.1
        port = 0
        interface = eth0
 }
 listen {
        type = acct
        ipaddr = 127.0.0.1
        port = 0
        interface = lo
 }

 hostname_lookups = no
 allow_core_dumps = no
 regular_expressions     = yes
 extended_expressions    = yes

 log {
        destination = files
        file = ${logdir}/radius.log
        syslog_facility = daemon
        stripped_names = no
        auth = no
        auth_badpass = no
        auth_goodpass = no
 }
 checkrad = ${sbindir}/checkrad
 security {
        max_attributes = 200
        reject_delay = 1
        status_server = yes
 }
 proxy_requests  = no
 $INCLUDE clients.conf
 thread pool {
        start_servers = 5
        max_servers = 32
        min_spare_servers = 3
        max_spare_servers = 10
        max_requests_per_server = 0
 }
 modules {
        $INCLUDE ${confdir}/modules/
 } 
 instantiate {
        exec
        expr
        expiration
        logintime
 }
 $INCLUDE policy.conf
 $INCLUDE sites-enabled/

The above configuration causes the RADIUS service to bind to our main network interface (eth0 in my example), and configures it to accept both authentication and accounting packets.

**If you install this service on the Domain Controller, make sure to change the ipaddr to your DC's ip address.**

8. Now we need to configure which clients can use the RADIUS service. This is done at the /etc/freeradius/clients.conf file. Please note that since in our example we have installed the FreeRADIUS on the Firewall server itself, the L2TP service which will define later connect to the RADIUS service via the local host, so basically there is nothing to do here except changing the default RADIUS client password. 

 client localhost {
        ipaddr = 127.0.0.1
        netmask = 32
        secret          = samba4
        shortname       = localhost
 }

However, if you have installed the FreeRADIUS server on the DC machine, then you will have to configure the FW server as a RADIUS client member:

 client 172.16.0.1 {
       secret      = samba4
       shortname   = fw
       nastype     = other
 }

It is also a good advice to define here an additional client for debugging purpose. We will use it later, once we will try to test and if our Radius Server can authenticate with the Samba4 domain controller.

9. Our next step is to disable the inner tunnel requests for EAP-TTLS and PEAP types on the RADIUS Server. This can be easy done by deleting the inner-tunnel file at the /etc/freeradius/sites-enabled folder.

 sudo rm /etc/freeradius/sites-enabled/inner-tunnel

Eventually you should end up with the default site only, which should look like this:
 authorize {
        preprocess
        auth_log
        chap
        mschap
        suffix
        ldap
        expiration
        logintime
        pap
 }
 authenticate {
        Auth-Type PAP {
                pap
        }
        Auth-Type CHAP {
                chap
        }
        Auth-Type MS-CHAP {
                mschap
        }
        Auth-Type LDAP {
                ldap
        }
 }
 preacct {
        preprocess
        acct_unique
        suffix
        files
 }
 accounting {
        detail
        radutmp
        attr_filter.accounting_response
 }
 session {
        radutmp
 }
 post-auth {
        exec
        Post-Auth-Type REJECT {
                attr_filter.access_reject
        }
 }
 pre-proxy {
 }
 post-proxy {
 }

10. Our last task is to configure the FreeRaidus modules. There are at least one relevant module which need to be configure, which is the LDAP module. 

Let's start configure first the LDAP module. You will have to configure it and change the required parameters to reflect your own configuration (Like the identity parameter (the user that bind to the LDAP server), the basedn (which in our example is DC=Example,DC=Com). 

 ldap {
        server = "DC"
        identity = "cn=VPN,cn=users,dc=example,dc=com"
        password = MyDomainVPN
        basedn = "dc=example,dc=com"
        filter = "(sAMAccountName=%{Stripped-User-Name:{User-Name}})"
        ldap_connections_number = 5
        timeout = 4
        timelimit = 3
        net_timeout = 1
        tls {
                start_tls = no
        }
        access_attr = "msNPAllowDialin"
        dictionary_mapping = ${confdir}/ldap.attrmap
        edir_account_policy_check = no
 }

The most important parameter is the **access_attr = "msNPAllowDialin"**, which is the file that tells our RADIUS server, if the user is configure to allow a VPN access.  The parameters change from TRUE to FALSE *(case sensitive)* via the ADUC Dial-in TAB:

`Image:Dialin.jpg`

11. If you want to use MS-CHAP or MS-CHAPv2 authentication, you will have to configure the winbind service on the firewall before continue. 

**Please Note:as for version 4.0.0alpha12-GIT-1a27343 of Samba4, the ntlm_auth command, which is required for MS-CHAP or MS-CHAPv2 authentication, is still under development. So if you installed your FreeRADIUS under the Domain Controller, you will not be able to work with MS-CHAP or MS-CHAPv2 authentication. **MS-CHAP or MS-CHAPv2 authentication only work at the moment with Samba3 versions**. For more info about the status of winbind with samab4 please see [http:SSLLAASS:HHwiki.samba.org/index.php/Samba4/Winbind here]

Please note, later Samba version 4.2+ require mschapv2, more info here [https://wiki.samba.org/index.php/Authenticating_Freeradius_against_Active_Directory, Authenticating Freeradius against Active Directory]

 mschap {
        use_mppe = no
        require_encryption = yes
        require_strong = yes
        with_ntdomain_hack = no
        ntlm_auth = "/usr/bin/ntlm_auth --request-nt-key --username=%{Stripped-User-Name:{User-Name:-:
        --challenge=%{mschap::-:HH-nt-response=%{mschap:NT-Respons:00}":
 }
**Note:orget set **use_mppe = yes** if you  use  **require-mppe-128** option in pptpd.conf*

For more info about MS-CHAP, please see Andrew Bartlett paper on [http://download.samba.org/pub/unpacked/lorikeet/pppd/final-report.pdf Integrating Windows Authentication into a wireless VPN solution]. 

One more good tutorial by Charles Schwartz about integration FreeRADIUS to AD can be found [http://homepages.lu/charlesschwartz/radius/freeRadius_AD_tutorial.pdf here.]

12. At this point you should be able to test if the FreeRADIUS is working with the samba4 ldap server. For testing proposes, please change one of your existing users to get "Allow access" permission under the Dial-in TAB. I can recommend on using [http://www.vasco.com/products/vacman/vacman_middleware/vacman_middleware.aspx VACMAN RADIUS Client Simulator from Vasco] which can really help for testing our FreeRADIUS configuration. If you do plan to test it, please remember to update your /etc/freeradius/clients.conf file, and defining the new RADIUS client parameters. If everything goes fine, you will end up with this::

`Image:OTTjpg`
`Image:in.jpg`

Or if you go with MS-CHAPv2

`Image:inCHAP.jpg`

Install & Configure the L2TP Service 
------------------------

13. Our next step is to install a L2TP service. I personally prefer the implementation by the [http://www.xelerance.com/software/xl2tpd/ Xelerance Corporation], so this guide will assume you also will be using this service. (However, I would prefer to have a L2TP implementation that can support DHCP over IPSEC, but as far as I know it, no such software exists at the moment). 

The Installation is quite simple using the apt-get engine exists in Debian/Ubuntu:
 sudo apt-get install xl2tpd

Now we need to do some changes to /etc/xl2tpd/xl2tpd.conf configuration file:

 [global]
 port = 1701
 [lns default]
 ip range = 172.16.0.201-172.16.0.254
 local ip = 172.16.0.200
 require authentication = yes
 name = VPN-Server
 ppp debug = yes
 pppoptfile = /etc/ppp/options.xl2tpd
 length bit = yes

All we did is basically providing the ip address of the L2TP tunnel, and the ip address range for the VPN clients. The PPP option file (pppoptfile) is also very impotent deceleration, since it is were we tell the L2TP server to use our configured RADIUS Server.

My /etc/ppp/options.xl2tpd looks like that:

 name VPN-Server
 ipcp-accept-local
 ipcp-accept-remote
 proxyarp
 ms-dns 172.16.0.1
 idle 1800
 connect-delay 5000
 mtu 1410
 mru 1410
 lock
 silent
 auth
 debug
 refuse-pap
 refuse-chap
 refuse-mschap
 require-mschap-v2
 require-mppe-128
 crtscts
 noccp
 novj
 nobsdcomp
 novjccomp
 nodetach
 nodefaultroute
 noreplacedefaultroute  
 nologfd
 plugin radius.so
 plugin radattr.so

Please note that if you installed the Samba4 on the Firewall server, then MS-CHAP/MS-CHAPv2 authentication will not work, and you will need to use PAP authentication only, as following:

 require-pap
 refuse-chap
 refuse-mschap
 refuse-mschap-v2

One more thing you need to make sure about is the libradiusclient library. The xl2tpd service use the radius.so plug-in which exists in the ppp package.  This plug-in depends on the libradiusclient library package. There are two libradiusclient packages exists in the Debian/Ubuntu repository, but I recommend to use the libradiusclient-ng2 package, with some minor tweaking which need to be done. 

First we install the libradiusclient-ng2:
 apt-get install libradiusclient-ng2
Note:8.04 libradiusclient-ng2 has been replaced by libradcli.

OpenSwan
14. xl2tpd
15. RADIUS?
16. client config

----
`Category:rectory`