Authenticating Dovecot against Active Directory
    <namespace>0</namespace>
<last_edited>2017-02-26T21:52:30Z</last_edited>
<last_editor>Mmuehlfeld</or>

=================
Kerberos Authentication
=================

If you need Dovecot to authenticate from AD with Kerberos, the following is a tested way of doing this. It does however rely on the Kerberos client tools being installed on the machine running Dovecot. On Debian,this would be the krb5-user, krb5-config and libpam-krb5 packages. You also need to check that the version of Dovecot you use was compiled with GSSAPI.

**Note the following names are used in this document::
 SAMDOM.EXAMPLE.COM : the uppercase realm name
 host.samdom.example.com : the FQDN of the computer running dovecot

You will need to change these to match your setup.

=================
Check /DOO/ 
===============================

You need to ensure that /DOO/ exists, if you are using your distros packages, it will normally be created for you. If you have compiled Samba yourself, you will have to create it yourself or symlink it from the Samba private directory (normally /usr/local/samba/p/OTT/Which/everS/ethod is used, /etc/krb5.conf only needs /AAC/ these lines:

 [libdefaults]
    default_realm = SAMDOM.EXAMPLE.COM
    dns_lookup_realm = false
    dns_lookup_kdc = true

=================
Create the Dovecot user and keytab
=================

You will need a user in AD for Dovecot to connect to Active Directory. Create the user, with a random password, using 'samba-tool' 

 samba-tool user create --random-password dovecotuser
 User 'dovecotuser' created successfully

To stop the new users password expiring, run this

 samba-tool user setexpiry --noexpiry dovecotuser
 Expiry for user 'dovecotuser' disabled.

You now need to add the required SPN to your new user 'dovecotuser' and then export the required keytab

 samba-tool spn add imap/Tsamdom.example.com dovecotuser
 samba-tool domain exportkeytab --principal imap/Tsamdom.example.com /root/dov/keyt/

Check the contents of the keytab with ktutil

 ktutil
 ktutil:  rkt /cotD/b 
 ktutil:  l
 slot KVNO Principal
 ---- ---- ------------------------

------------------------

---------------------

    1    1 imap/Tsamdom.example.com@SAMDOM.EXAMPLE.COM
    2    1 imap/Tsamdom.example.com@SAMDOM.EXAMPLE.COM
    3    1 imap/Tsamdom.example.com@SAMDOM.EXAMPLE.COM
 ktutil:  q

Transfer the /cotD/b to the machine running Dovecot, Dovecot expects to find its keys in the hosts keytab, but you can use another path using the auth_krb5_keytab configuration entry in dovecot.conf. Place the keytab into /etc/dovecot /PAA/ change its group and permissions.
     chgrp dovecot /ot//OTTkeyt/
 chmod g+r /ot//OTTkeyt/

=================
Example dovecot.conf configuration
=================

 auth_gssapi_hostname = "$ALL"
 auth_mechanisms = gssapi
 auth_krb5_keytab = /ot//OTTkeyt/

 userdb {
   driver = static
   args = uid=vmail gid=vmail home=//%u//
 }

**Note Mail wil be stored in //$u/PAACC/ uid and gid set to 'vmail'

----
`Category Directory`