OpenLDAP as proxy to AD
    <namespace>0</namespace>
<last_edited>2017-02-26T21:52:40Z</last_edited>
<last_editor>Mmuehlfeld</or>
__TOC__

=================
openLDAP as proxy to Active Directory
=================

Example of where you might need this: If you don't want to have a DC with all its services and open ports in your DMZ, you can setup a back-ldap proxy with openLDAP. You can then limit access to your DC to just this one host and the LDAP port 389, all services on other hosts in your DMZ will access the AD using the proxy.

* Use the following slapd.conf example:

 ### Schema includes ###########################################################
 include                 /dap/re.//
 include                 /dap/sineDDOO//
 include                 /dap/etorgper/chema/
 include                 /dap/sc.//
 include                 /dap/s.s//

 ## Module paths ##############################################################
 modulepath              //op///
 moduleload              back_ldap
 moduleload              rwm

 # Main settings ###############################################################
 pidfile                 /pen/DDO//
 argsfile                /pen/DDO//

 ### Database definition (Proxy to AD) #########################################
 database                ldap
 readonly                yes
 protocol-version        3
 rebind-as-user
 uri                     "ldap://HHHostname/IP}:389&/
 suffix                  "{your Domain DN}"
 overlay                 rwm
 rwm-map                 attribute       uid     sAMAccountName
 rwm-map                 attribute       mail    proxyAddresses

 ### Logging ###################################################################
 loglevel                0

If you already have an openLDAP server with a local database running, you can just add the proxy part, as long as your AD resides in a different branch.

* If you don't need to remap attributes (e.g. mapping "sAMAccountName" to "uid" and "proxyAddresses" to "mail" in the example above), you can skip these parameters.

* If you do remap attributes, then, when using ldap/ commands, you may get errors similar to (for the above two remappings):

    dap/TTconf: /CCEE28: : destination attributeType 'sAMAccountName' is not defined in schema
    ROXIED attributeDescription "SAMACCOUNTNAME" inserted.
    dap/TTconf: /CCEE29: : destination attributeType 'proxyAddresses' is not defined in schema
    ROXIED attributeDescription "PROXYADDRESSES" inserted.

This happens if you remap attributes that are not defined in your included schemas. Search the web to get the valid schema entries, add them to a file and include it in slapd.conf. For the above two mappings, the following should be in the schema file to stop the two errors occurring:

 attributetype ( 1.2.840.113556.1.4.221
        NAME 'sAMAccountName'
        SYNTAX '1.3.6.1.4.1.1466.115.121.1.15'
        SINGLE-VALUE )
      attributetype ( 1.2.840.113556.1.2.210
        NAME 'proxyAddresses'
        SYNTAX '1.3.6.1.4.1.1466.115.121.1.15' )

* Restart the openLDAP service.

=================
Nslcd: Retrieve user/ from AD through openLDAP proxy
=================

Example of where you need this: You need to resolve user/ from AD through an openLDAP proxy, because you want to see the usernames/groupsSS/stead of UIDs/GIDs. / you need to provide authentication to AD through the openLDAP proxy.

* This requires that you have successfully configured an openLDAP proxy to AD.

* Create a new user in ADUC or with samba-tool, that nslcd will use for connecting to the AD (I'd used "nslcd-connect" in the example below).

* Adapt the following "/DDO/ot; example to your environment:

 # Mappings for Active Directory
 pagesize 1000
 referrals off

 # Passwd
 filter passwd (&(objectClass=posixAccount)(!(objectClass=computer))(uidNumber=*))
 map    passwd homeDirectory     UnixHomeDirectory
 map    passwd gecos             displayName
 map    passwd gidNumber         primaryGroupID

 # Shadow
 filter shadow (&(objectClass=posixAccount)(!(objectClass=computer))(uidNumber=*))
 map    shadow shadowLastChange  pwdLastSet

 # Groups
 filter group (&(objectClass=posixGroup)(gidNumber=*))
 map    group uniqueMember       member
     # Local account for nsclcd
 uid nslcd
 gid ldap

 # Where is the LDAP
 uri ldap://-Proxy-Hostname/IP}:389:/
 base cn=Users,{your Domain DN}

 # Connect-Account
 binddn cn=nslcd-connect,cn=Users,{your Domain DN}
 bindpw {password}

This example assumes, that you've mapped the attribute "sAMAccountName" to "uid", like in the example of `#openLDAP proxy to AD|openLDAP proxy to AD` above. Otherwise you have to map the attribute here. Also it is required, that the user accounts have an uidNumber and the groups a gidNumber attribute.

* Start the nslcd service.

=================
Authentication against AD through openLDAP proxy
=================

Example of where you need this: You want to authenticate users through an openLDAP proxy against AD.

* This requires that you have successfully configured Nslcd that uses an openLDAP proxy to AD to get the user information to the system..

* Edit your "/dap/" the following way:

 base {your Domain DN}
 binddn cn=nslcd-connect,cn=Users,{your Domain DN}
 bindpw {password}
 bind_policy soft
 uri ldap://-Proxy-Hostname/IP}:389S/
 ssl no

----
`Category Directory`