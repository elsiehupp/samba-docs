Delegating administrative permissions to non-administrators
    <namespace>0</namespace>
<last_edited>2017-02-26T21:47:59Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

===============================

Introduction
===============================

Active Directory allows you to delegate permission for administration tasks to users and/or groups. This is an important feature that allows you to prevent working with domain admin permissions the whole time or giving the domain admin password to all in your IT department.

Possible fields of application:
* Allow supporters to join machines to the domain
* Allow the human resources to update general user information
* Allow the HelpDesk workers to create users, reset passwords and unlock accounts

Delegations can be configured on the whole domain or on specific OUs.

=================
Samba versions supporting delegations
=================

You should at least run 4.0.0 final (older versions haven't been tested)!

Known issues/limitations 
------------------------

* 4.0.0 - current: Delegations working fine, but because of ACL issues, you have to add 'acl:search=false' as a workaround to your smb.conf (See https://bugzilla.samba.org/show_bug.cgi?id=9788).

* When upgrading from a version prior 4.0.5 to that version or later: If you run 'samba-tool dbcheck --reset-well-known-acls --fix' to reset the directory ACLs (recommended in 4.0.5 release notes to fix missing ACLs from previous provisioning), you'll loose all existing delegations. But you should fix the wrong directory ACLs that where provisioned by earlier versions!

=================
Performance and maintanance of delegations
=================

Delegations are simply said ACLs on directory attributes and containers. If you would delegate permissions for several users accounts, this would increase the number of ACLs, what could cause performance impacts somewhen. Also if the delegated permissions should be revoked for an account, you have to remove its ACLs, what brings unneccessary administration work.

That's why it is recommented, that you delegate permissions only to groups and not to accounts. If you want to grant/revoke permissions for an account, you only have to change the group membership.

----
`Category:Active Directory`