Roaming Windows User Profiles
    <namespace>0</namespace>
<last_edited>2021-05-19T08:34:02Z</last_edited>
<last_editor>Hortimech</last_editor>

===============================

Introduction
===============================

A Windows profile is a set of files that contains all settings of a user including per-user configuration files and registry settings. In an Active Directory or NT4 domain you can set that the profile of a user is stored on a server. This enables the user to log on to different Windows domain members and use the same settings.

When using roaming user profiles, a copy of the profile is downloaded from the server to the Windows domain member when a user logs into. Until the user logs out, all settings are stored and updated in the local copy. During the log out, the profile is uploaded to the server.

=================
Windows Roaming Profile Versions
=================

Depending on the operating system version, Windows uses separate profile folders for a user to support Windows version-specific features. Version 2 profiles and later append the ``.V*`` suffix to the user's profile folder.

The following Windows profile versions exist:

* {| class="wikitable"
!Windows Client OS Version
!Windows Server OS Version
!Profile Suffix
!Example Profile Folder Name
|-
|Windows NT 4.0 - Windows Vista
|Windows NT Server 4.0 - Windows Server 2008
|*none*
|user
|-
|Windows 7
|Windows Server 2008 R2
|V2
|user.V2
|-
|Windows 8.0 - 8.1*
|Windows Server 2012 - 2012 R2*
|V3
|user.V3
|-
|Windows 8.1*
|Windows Server 2012 R2*
|V4
|user.V4
|-
|Windows 10 (1507 to 1511)
|Windows Server 2016
|V5
|user.V5
|-
|Windows 10 (1607 and later)
|
|V6
|user.V6
|}

* <nowiki>*</nowiki> Using the default settings, Windows 8.1 and Windows Server 2012 R2 use V3 profiles. However, the profiles are incompatible with Windows 8.0 and Windows Server 2012. For this reason it is recommended that you configure Windows 8.1 and Windows Server 2012 R2 to use V4 profiles. For further details, see: [https://support.microsoft.com/en-us/help/2890783/incompatibility-between-windows-8.1-roaming-user-profiles-and-those-in-earlier-versions-of-windows Incompatibility between Windows 8.1 roaming user profiles and those in earlier versions of Windows].

When you set the profile path for a user, you always set the path without any version suffix. For example:
 \\server\profiles\user_name

=================
Setting up the Share on the Samba File Server
=================

Using Windows ACLs 
------------------------

To create a share, for example, ``profiles`` for hosting the roaming profiles on a Samba file server: 

* Create a new share.

    profiles]
          comment = Users profiles
          path = /srv/samba/profiles/
          browseable = No
          read only = No
          csc policy = disable
          vfs objects = acl_xattr

For further details, see `Setting up a Share Using Windows ACLs`. Set the following permissions:

* * Share tab permissions:
* :{| class="wikitable"
!Principal
!Allow
|-
|Everyone
|Full Control / Change / Read
|}

* * Security tab file system permissions on the root of the ``profiles`` share:

* :{| class="wikitable"
!Principal
!Access
!Applies to
|- style="vertical-align:top;"
|Domain Users *
|Traverse folder / execute file<br />List folder / read data<br />Create folder / append data
|This folder only
|-
|CREATOR OWNER
|Full control
|Subfolders and files only
|-
|Domain Admins
|Full control
|This folder, subfolders and files
|-
|SYSTEM **
|Full control
|This folder, subfolders and files
|}
* :<nowiki>*</nowiki> You can alternatively set other groups, to enable the group members to store their user profile on the share. When using different groups, apply the permissions as displayed for ``Domain Users`` in the previous example.

* :<nowiki>**</nowiki> For details, see `The SYSTEM Account`.

* : Verify that permission inheritance is disabled on the root of the share. If any permission entry in the ``Advanced Security Settings`` window displays a path in the ``Inherited from`` column, click the ``Disable inheritance`` button. On Windows 7, unselect the ``Include inheritable permissions from this object's parent`` check box to set the same setting.

* :`Image:Profiles_Folder_File_System_ACLs.png`

These settings enable members of the ``Domain Users`` group to store their roaming profiles on the share, without being able to access other user's profiles. Members of the ``Domain Admins`` group are able to access all directories on the share.

Using POSIX ACLs on a Unix domain member 
------------------------

On a Unix domain member server, you can set up the ``profiles`` share using POSIX ACLs instead of using Windows access control lists (ACL). This will not work on a Samba Active Directory Controller.

.. note:

    Whilst it is possible to use POSIX ACLs for the profiles share on an Unix domain member, it is recommended that you set up the permissions from Windows. To do this, see `#Using_Windows_ACLs|Setting up the Profiles Share on the Samba File Server - Using Windows ACLs`.

{{Imbox
| type = warning
| text = When setting up the share on a Samba Active Directory (AD) domain controller (DC), you cannot use POSIX ACLs. On an Samba DC, only shares using extended ACLs are supported. For further details, see `Setting_up_a_Share_Using_Windows_ACLs#Enable_Extended_ACL_Support_in_the_smb.conf_File|Enable Extended ACL Support in the smb.conf File`. To set up the share on a Samba AD DC, see `#Using_Windows_ACLs|Setting up the Profiles Share on the Samba File Server - Using Windows ACLs`.

* Add the following share configuration section to your ``smb.conf`` file:

    profiles]
          comment = Users profiles
          path = /srv/samba/profiles/
          browseable = No
          read only = No
          force create mode = 0600
          force directory mode = 0700
          csc policy = disable
          store dos attributes = yes
          vfs objects = acl_xattr

* For details about the parameters used, see the descriptions in the ``smb.conf(5)`` man page.

* Create the directory and set permissions:

 # mkdir -p /srv/samba/profiles/
 # chgrp -R "Domain Users" /srv/samba/profiles/
 # chmod 1750 /srv/samba/profiles/

* These settings enable members of the ``Domain Users`` group to store their roaming profiles on the share, without being able to access other user's profiles. Alternatively, you can set a different group.

* Reload Samba:

 # smbcontrol all reload-config

=================
Assigning a Roaming Profile to a User
=================

Depending on the Windows version, Windows uses different folders to store the roaming profile of a user. However, when you set the profile path for a user, you always set the path to the folder without any version suffix. For example:
 \\server\profiles\user_name

For further details, see `#The_Windows_Roaming_Profile_Versions|The Windows Roaming Profile Versions`.

Note that you must not set a trailing backslash.

In an Active Directory 
------------------------

Using ``Active Directory Users and Computers``
------------------------

In an Active Directory, you can use the ``Active Directory Users and Computers`` Windows application to set the path to the user's profile folder. If you do not have the Remote Server Administration Tools (RSAT) installed, see `Installing RSAT|Installing RSAT`.

To assign ``\\server\profiles\demo`` as profile folder to the ``demo`` account:

* Log in to a computer using an account that is enabled to edit user accounts.

* Open the ``Active Directory Users and Computers`` application.

* Navigate to the directory container that contains the ``demo`` account.

* Right-click to the ``demo`` user account and select ``Properties``.

* Select the ``Profile`` tab.

* Fill the path to the home folder into the ``Profile path`` field.
* Set the path always without any profile version suffix and without trailing backslash. For details, see `#The_Windows_Roaming_Profile_Versions|The Windows Roaming Profile Versions`.

* `Image:ADUC_Set_Profile_Folder.png`.

* Click ``OK``.

The setting is applied the next time the user logs in.

Using a Group Policy Object
------------------------

Using group policy objects (GPO), you can assign settings to organizational units (OU) or to a domain. This enables you, for example, to automatically assign profile paths to all users that log on to a computer that is a member of the OU or domain. If you move the computer to a different OU or domain, the setting is removed or updated. Using this way, you do not have to assign manually the settings to each user account.

.. note:

    Windows only supports assigning a profile path using GPOs on a per-computer basis. This means that the path is also applied to local users on domain members, which have no access to the profile share. To set the profile path on a per-user basis, see `#Using_Active_Directory_Users_and_Computers|Using Active Directory Users and Computers`.

To create a group policy object (GPO) for the domain that automatically assigns the ``\\server\path\*user_name*`` path to every user that logs on to a Windows domain member:

* Log in to a computer using an account that is allowed to edit group policies, such as the AD domain ``Administrator`` account.

* Open the ``Group Policy Management Console``. If you are not having the Remote Server Administration Tools (RSAT) installed on this computer, see `Installing RSAT|Installing RSAT`.

* Right-click to your AD domain and select ``Create a GPO in this domain, and Link it here``.

* `Image:GPMC_Create_GPO.png`

* Enter a name for the GPO, such as ``Profiles on *server*``. The new GPO is shown below the domain entry.

* Right-click to the newly-created GPO and select ``Edit`` to open the ``Group Policy Management Editor``.

* Navigate to the ``Computer Configuration`` &rarr; ``Policies`` &rarr; ``Administrative Templates`` &rarr; ``System`` &rarr; ``User Profiles`` entry.

* Double-click the ``Set roaming profile path for all users logging onto this computer`` policy to edit:

* * Enable the policy and set the profile path. For example:

 \\server\profiles\%USERNAME%

* : Windows replaces the ``%USERNAME%`` variable with the user name during login. Set the path without trailing backslash.

* :`Image:GPME_Set_Profiles_Properties.png`

* * Click ``OK``.

* Close the ``Group Policy Management Editor``. The GPOs are automatically saved on the ``Sysvol`` share on the domain controller (DC).

* Close the ``Group Policy Management Console``.

The GPO is applied at the next reboot of the Windows domain members or when they reload the group policies.

Using ``ldbedit`` on a Domain Controller
------------------------

On a domain controller (DC), to assign, for example, the ``\\server\profiles\demo\`` path as profile folder to the ``demo`` account:

* Edit the ``demo`` user account:

 # ldbedit -H /usr/local/samba/private/sam.ldb 'sAMAccountName=demo'

* The accounts attributes are displayed in an editor. Append the following attribute and value to the end of the list:

 profilePath: \\server\profiles\demo

* You must not set a trailing backslash to the path.

* Save the changes.

The setting is applied the next time the user logs in. 

In an NT4 Domain 
------------------------

In an Samba NT4 domain, to set ``\\server\profiles\%U`` as path to the profile folder:

* Add the following parameter to the ``[global]`` section in your ``smb.conf`` file:

 logon path = \\%L\Profiles\%U

* During logging in to the domain member, Samba automatically replaces the ``%U`` variable with the session user name. For further details, see the ``Variable Substitutions`` section in the ``smb.conf(5)`` man page.

* Reload Samba:

 # smbcontrol all reload-config

=================
Configuring Windows Profile Folder Redirections
=================

See `Configuring Windows Profile Folder Redirections`.