User and Group management
    <namespace>0</namespace>
<last_edited>2020-04-22T11:21:21Z</last_edited>
<last_editor>Hortimech</last_editor>


=================
 User and Group and Computer accountd management with samba-tool
=================

Adding Users into Samba Active Directory 
------------------------

You add / delete users with samba-tool

Unlike Samba 3, running Samba 4 as an AD DC or Unix AD domain member does not require a local Unix user for each Samba user that is created.

An example of adding a User + Login Profile for the user ``fbaggins``

This assumes that ADSMember is being used as a Unix Member server that stores the profile and shares and the new users password will be ``P4ssw0rd*``

 $ samba-tool user create fbaggins P4ssw0rd*
    -use-username-as-cn --surname="Baggins"
    -given-name="Frodo" --initials=S
    -mail-address=fbaggins@samdom.example.com
    -company="Hobbiton Inc." --script-path=shire.bat
    -profile-path=\\\\ADSMember.samdom.example.com\\profiles\\fbaggins
    -home-drive=F
    -home-directory=\\\\ADSMember.samdom.example.com\\fbaggins
    -job-title="Goes there and back again"

.. note:

    You do not need to supply all of the above options when creating a new user. For details of available options, run ``samba-tool user create --help`` in a terminal.

To inspect the allocated user ID and SID, use the following commands:

 $ wbinfo --name-to-sid USERNAME
 S-1-5-21-4036476082-4153129556-3089177936-1005 SID_USER (1)

 $ wbinfo --sid-to-uid S-1-5-21-4036476082-4153129556-3089177936-1005
 3000011

samba-tool:Users from Samba Active Directory
------------------------

 # samba-tool user delete username

samba-tool: group in Samba Active Directory
------------------------

 ~# samba-tool group add groupname
 Added group groupname

samba-tool: Unix group in Samba Active Directory
------------------------

 ~# samba-tool group add groupname --nis-domain=samdom --gid-number=<next available GID>
 Added group groupname

samba-tool: group from Samba Active Directory
------------------------

 ~# samba-tool group delete groupname
 Added group groupname

 samba-tool: rs to a group in Samba Active Directory
------------------------

 ~# samba-tool group addmembers "Domain Users" user[,otheruser[,thirduser[,...`]
 Added members to group Domain Users

 samba-tool: mbers from a group in Samba Active Directory
------------------------

 ~# samba-tool group removemembers "Domain Users" user[,otheruser[,thirduser[,...`]
 Removed members from group Domain Users

samba-tool:bers of a group in Samba Active Directory
------------------------

 ~# samba-tool group listmembers "Domain Users" | grep username
    ser

samba-tool: user, create a group, add the user to the group in Samba Active Directory
------------------------

 ~# samba-tool user create username
   User 'username' created successfully

 ~# samba-tool group add groupname
    dded group groupname

 ~# samba-tool group addmembers groupname username
    dded members to group groupname

----
`Category:gement`