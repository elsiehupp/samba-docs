Setting up a Share Using Windows ACLs
    <namespace>0</namespace>
<last_edited>2021-05-14T12:39:13Z</last_edited>
<last_editor>Hortimech</last_editor>

===============================

Introduction
===============================

Extended access control lists (ACL) enable you to set permissions on shares, files, and directories using Windows ACLs and applications. Samba supports shares using extended ACLs on:
* Domain members
* Active Directory (AD) domain controllers (DC)
* NT4 primary domain controller (PDC)
* NT4 backup domain controllers (BDC)
* Standalone hosts

=================
Preparing the Host
=================

You need to set up Samba before you are able to create a share. Depending on what type of Samba server you require, see:
* `Setting_up_Samba_as_a_Domain_Member|Setting up Samba as a Domain Member`
* `Active_Directory_Domain_Controller|Setting up Samba as AD DC`
* `Setting_up_Samba_as_an_NT4_PDC_(Quick_Start)|Setting up Samba as an NT4 PDC (Quick Start)`
* `Setting_up_Samba_as_an_NT4_BDC|Setting up Samba as an NT4 BDC`
* `Setting_up_Samba_as_a_Standalone_Server|Setting up Samba as a Standalone Server`

File System Support 
------------------------

The file system, the share will be created on, must support:
* user and system ``xattr`` name spaces.
* extended access control lists (ACL).

For further details, see `File_System_Support|File system support`.

Samba Extended ACL Support 
------------------------

To create a share with extended access control list (ACL) support, the ``smbd`` service must have been built with ACL support enabled. A Samba host working as an Active Directory (AD) domain controller (DC), is always enabled with extended ACL support.

To verify if Samba has been built with ACL support, enter:

 # smbd -b | grep HAVE_LIBACL
    HAVE_LIBACL

If no output is displayed:
* Samba was built using the ``--with-acl-support=no`` parameter.
* The Samba ``configure`` script was unable to locate the required libraries for ACL support. For details, see `Package Dependencies Required to Build Samba`.

Enable Extended ACL Support on a Unix domain member 
------------------------

Ideally you have a system that supports `NFS4_ACL_overview|NFS4 ACLs`. The following example is for systems like Linux, where you don't have those kind of ACLs. To configure shares using extended access control lists (ACL) on a Unix domain member, you must enable the support in the ``smb.conf`` file. To enable extended ACL support globally, add the following settings to the ``[global]`` section of your ``smb.conf`` file:

 vfs objects = acl_xattr
 map acl inherit = yes
 # the next line is only required on Samba versions less than 4.9.0
 store dos attributes = yes

.. warning:

   On a Samba Active Directory (AD) domain controller (DC), extended ACL support is automatically enabled globally. You must not enable the support manually.

Alternatively, to enable extended ACL support only for a specific share, add the parameters to the share's section.

For further details about the parameters, see the ``smb.conf(5)`` man page.

Granting the ``SeDiskOperatorPrivilege`` Privilege 
------------------------

Only users and groups having the ``SeDiskOperatorPrivilege`` privilege granted can configure share permissions.

.. note:

    Only users or groups that are known to Unix can be used. This means that if you use the winbind 'ad' backend on Unix domain members, you must add a uidNumber attribute to users, or a gidNumber to groups in AD.  

.. note:

    If you use the winbind 'ad' backend on Unix domain members and you add a gidNumber attribute to the ``Domain Admins`` group in AD, you will break the mapping in ``idmap.ldb``. ``Domain Admins`` is mapped as ``ID_TYPE_BOTH`` in  ``idmap.ldb``, this is to allow the group to own files in ``Sysvol`` on a Samba AD DC. It is suggested you create a new AD group (``Unix Admins`` for instance), give this group a ``gidNumber`` attribute and add it to the ``Administrators`` group and then, on Unix, use the group wherever you would normally use ``Domain Admins``. 

To grant the privilege to the ``Unix Admins`` group, enter:

 # net rpc rights grant "SAMDOM\Unix Admins" SeDiskOperatorPrivilege -U "SAMDOM\administrator"
 Enter SAMDOM\administrator's password:
 Successfully granted rights.

.. note:

    It is recommended to grant the privilege to a group instead of individual accounts. This enables you to add and revoke the privilege by updating the group membership.

To list all users and groups having the ``SeDiskOperatorPrivilege`` privilege granted, enter:

 # net rpc rights list privileges SeDiskOperatorPrivilege -U "SAMDOM\administrator"
 Enter administrator's password:
 SeDiskOperatorPrivilege:
   BUILTIN\Administrators
   SAMDOM\Unix Admins

.. warning:

   You need to grant the ``SeDiskOperatorPrivilege`` privilege on the Samba server that holds the share.

=================
Adding a Share
=================

To share the ``/srv/samba/Demo/`` directory using the ``Demo`` share name:

* As the ``root`` user, create the directory:

 # mkdir -p /srv/samba/Demo/

* To enable accounts other than the domain user ``Administrator`` to set permissions on Windows, grant ``Full control`` (``rwx``) to the user or group you granted the ``SeDiskOperatorPrivilege`` privilege. For example (if using the 'ad' backend):

 # chown root:x Admins" /srv/samba/Demo/
 # chmod 0770 /srv/samba/Demo/

* Otherwise for any other backend:

 # chown root:ain Admins" /srv/samba/Demo/
 # chmod 0770 /srv/samba/Demo/

* Add the ``[Demo]`` share definition to your ``smb.conf`` file:

 [Demo]
        path = /srv/samba/Demo/
        read only = no

* share-specific settings and file system permissions are set using the Windows utilities.

* 
| type = important
| text = Do not set ``ANY`` additional share parameters, such as ``force user`` or ``valid users``. Adding them to the share definition can prevent you from configuring or using the share.

If you are setting the shares permissions from Windows (recommended), you should add this line to your share:
 acl_xattr:stem acl = yes

This will make Samba ignore the system ACL's (ugo).

* Reload the Samba configuration:

 # smbcontrol all reload-config

=================
Setting Share Permissions and ACLs
=================

When you configure a share with extended access control lists (ACL) support, you set the share permissions using Windows utilities instead of adding parameters to the share section in the ``smb.conf`` file.

To set permissions and ACLs on the ``Demo`` share:

* Log on to a Windows host using an account that has the ``SeDiskOperatorPrivilege`` privilege granted. e.g. ``SAMDOM\Administrator`` or ``SAMDOM\john`` where ``john`` is a member of ``Unix Admins``.

* Click ``Start``, enter ``Computer Management``, and start the application.

* Select ``Action`` / ``Connect to another computer``.

* Enter the name of the Samba host and click ``OK`` to connect the console to the host.

* Open the ``System Tools`` / ``Shared Folders`` / ``Shares`` menu entry.

* omputer:t_Shares.png`

* Right-click to the share and select ``Properties``.

* Select the ``Share Permissions`` tab and check the share permissions, you need to see ``Everyone``. For example:
* hareDDO:

.. warning:

   If the permissions are as above, you do not need to change anything, if not, change it to just allow ``Everyone`` :>Full Control, Change and Read``. You should only need to make changes to the ``Security`` tab.

* ores the share tab permissions in the ``/usr/local/samba/var/locks/share_info.tdb`` database.

* Select the ``Security`` tab.

* Click the ``Edit`` button and set the file system ACLs on the share's root directory. For example:

* emo_Sha:y.png`

* ils about using the ``SYSTEM`` account on a Samba share see `The SYSTEM Account`.

* ils where the ACLs are stored, see `#File_System_ACLs_in_the_Back_End|File System ACLs in the Back End`.

* Click the ``Add`` button.

* Click ``Advanced`` button

* Click ``Find Now``

* Select a user or group from the list, ``Domain Users`` for instance.

* Click ``OK``

* Click ``OK``

* Select permissions to grant, ``Full control`` for instance.

* A windows security box should open, asking if you want to continue, Click ``Yes``

* If you check the list of ``Group or user names``, you should find ``Domain Users`` listed

* Click ``OK`` to close the ``Permissions for Demo`` window.

* Click ``OK`` to store the updated settings.

For further details about configuring share permissions and ACLs, see the Windows documentation.

=================
Setting ACLs on a Folder
=================

To set file system permissions on a folder located on a share that uses extended access control lists (ACL):

* Log on to a Windows host using an account that has ``Full control`` on the folder you want to modify the file system ACLs.

* Navigate to the folder.

* Right-click to the folder and select ``Properties``.

* Select the ``Security`` tab and click the ``Edit`` button.

* Set the permission. For example:

* older_P:.png`

* ils about using the ``SYSTEM`` account on a Samba share see `The SYSTEM Account`.

* ils where the ACLs are stored, see `#File_System_ACLs_in_the_Back_End|File System ACLs in the Back End`.

* Click ``OK`` to close the ``Permissions for Folder`` window.

* Click ``OK`` to store the updated settings.

For further details about setting ACLs, see the Windows documentation.

=================
File System ACLs in the Back End
=================

Samba stores the file system permissions in extended file system access control lists (ACL) and in an extended attribute. For example:

* To list the extended ACLs of the ``/srv/samba/Demo/`` directory, enter:

 # getfacl /srv/samba/Demo/
 # file:ASSHHsamba/Demo/
 # owner:
 # group:
 user::
 user::
 group::AASSHH
 group:SSHH:AASSHH
 group:0users:rwx:
 group:dmins:rwx:
 mask::
 other::AASSHH
 default:::
 default::rwx::
 default:AASSH::
 default:t:DDA:SHHD:
 default:ain\0:x:
 default:x\040::
 default:::
 default:AASSH::

* To list the ``security.NTACL`` extended attribute of the ``/srv/samba/Demo/`` directory, enter:

 # getfattr -n security.NTACL -d /srv/samba/Demo/
 # file:ASSHHsamba/Demo/
 security.NTACL=0sBAAEAAAAAgAEAAIAAQC4zK0lHchKFvwXwbPR/h8P8sXMj5dNIT5QQuWsYwO3RAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcG9zaXhfYWNsAEbGxuGu39MBuiZRk2pYxeL5ZWc4au0ikqRAk53MkjVd2b4quyk2WwcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEABJy0AAAA0AAAAAAAAADsAAAAAQUAAAAAAAUVAAAASSVmaZneO8cxOHk/9AEAAAEFAAAAAAAFFQAAAEklZmmZ3jvHMTh5P0oIAAACAMQABwAAAAALFACpABIAAQEAAAAAAAEAAAAAAAAUAAAAEAABAQAAAAAAAQAAAAAACxQA/wEfAAEBAAAAAAADAAAAAAALFACpABIAAQEAAAAAAAMBAAAAAAMkAP8BHwABBQAAAAAABRUAAABJJWZpmd47xzE4eT9KCAAAAAAkAP8BHwABBQAAAAAABRUAAABJJWZpmd47xzE4eT/0AQAAAAMkAL8BEwABBQAAAAAABRUAAABJJWZpmd47xzE4eT8BAgAA

The previous example of file system ACLs and the extended attribute is mapped to the following Windows ACLs:

{| class="wikitable"
!Principal
!Permissions
!Applies to
|-
|Domain Users (SAMDOM\Domain Users)
|Modify, Read & execute, List folder contents, Read, Write
|(This folder, subfolders and files)
|-
|Unix Admins (SAMDOM\Unix Admins)
|Full control
|(This folder, subfolders and files)
|}

* To get the ACL in a more readable form, enter:

 # samba-tool ntacl get /usr/local/samba/var/locks/sysvol --as-sddl
 # O:AI(:OWD:O)(A;OICIIO;GRGX;;;AU)(A;;0x001200a9;;;AU)(A;OICIIO;GA;;;SY)(A;;0x001f01ff;;;SY)(A;OICIIO;WOWDGRGWGX;;;BA)(A;;0x001e01bf;;;BA)(A;OICIIO;GRGX;;;SO)(A;;0x001200a9;;;SO)

=================
Troubleshooting = 

For troubleshooting, see:
* `Troubleshooting_Samba_Domain_Members|Troubleshooting Samba Domain Members`
* `Samba_AD_DC_Troubleshooting|Samba AD DC Troubleshooting`

----
`Category:rectory`
`Category:mbers`
`Category:ing`
`Category:ns`