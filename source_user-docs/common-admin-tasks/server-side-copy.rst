Server-Side Copy
    <namespace>0</namespace>
<last_edited>2019-08-01T15:31:30Z</last_edited>
<last_editor>Torch1</last_editor>

===============================

Introduction
===============================

Samba 4.1.0 was the first release to ship with support for server-side copy operations via the SMB2 [http://msdn.microsoft.com/en-us/library/cc246475.aspx FSCTL_SRV_COPYCHUNK] request. Clients making use of server-side copy support, such as Windows Server 2012 and Windows 8, can experience considerable performance improvements for file copy operations, as file data need not traverse the network. <u>This feature is enabled by default on the smbd file server.</u>

Note - not enabled for OS X (Macs) unless server Samba includes vfs_fruit module and fruit:copyfile = yes in smb.conf.

Samba 4.7.0 introduced support for [https://msdn.microsoft.com/en-us/library/mt846251.aspx FSCTL_DUPLICATE_EXTENTS_TO_FILE], which similarly allows for offloaded clone operations.

=================
Client Support
=================

Windows 
------------------------

* **Windows Server 2012 and later**: via Windows Explorer or Robocopy
* **Windows 8 and later**: via Windows Explorer or Robocopy
* **Windows Server 2008**: via Robocopy only
* **Windows 7**: via Robocopy only

*For other tools, ask the respective vendor for SMB2 FSCTL_SRV_COPYCHUNK support.*

Linux 
------------------------

The Linux Kernel CIFS client [https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=9bf0c9cd431440a831e60c0a0fd0bc4f0e083e7f includes support] for issuing SMB2 FSCTL_SRV_COPYCHUNK_WRITE server-side copy requests. This feature can currently only be utilised by issuing a special CIFS_IOC_COPYCHUNK_FILE ioctl, as done by [http://oss.sgi.com/cgi-bin/gitweb.cgi?p=xfs/cmds/xfstests.git;a=blob;f=src/cloner.c;h=18c44b9770f5ab61316c639f8a647bc08011766c;hb=HEAD cloner] in the xfstests Git repository.

Linux Kernel support for FSCTL_DUPLICATE_EXTENTS_TO_FILE was [https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=02b1666544c08e245cb4e2253ed575f8128943d6 added with kernel version 4.2], and can be issued via *cp --reflink* on supported systems with [https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=ca9e7a1c85594f61d7ffb414071e6cae82eae23a SMB3+ mounts].

=================
Filesystem Support
===============================

FSCTL_SRV_COPYCHUNK works with any filesystem backing the Samba share. There are some additional improvements for `#Btrfs_Enhanced_Server-Side_Copy_Offload|Btrfs` in comparison with `#Server-Side_Copy_Offload|other filesystems`. FSCTL_DUPLICATE_EXTENTS_TO_FILE is currently only supported atop `#Btrfs_Enhanced_Server-Side_Copy_Offload|Btrfs`.

=================
Traditional File Copy
=================

Prior to Samba 4.1.0, clients traditionally copied files by reading the source file data over the network and writing it back to the server through to the destination file on disk.

{| border="1" cellpadding="5" cellspacing="0"
|-
|
`File:Traditional_copy.png` 
|
**Sequence**
# Client issues read requests to server
# Server reads file data off disk
# Server sends data to client
# Client sends duplicate file data to server
# Server writes duplicate file data to disk

**Result**
* File data traverses network and disk
* Duplicate file data stored on disk
|}

=================
Server-Side Copy Offload
=================

With Samba 4.1.0 and later, clients can offload copy operations to the server using Copy-Chunk requests. In processing such a request, the network round-trip is avoided.

{| border="1" cellpadding="5" cellspacing="0"
|-
|
`File:Serverside_copy.png` 
|
**Sequence**
# Client issues FSCTL_SRV_COPYCHUNK requests to server
# Server reads file data off disk
# Server writes duplicate file data to disk

**Result**
* File data traverses disk only
** Network round trip avoided
* Duplicate file data stored on disk
|}

Samba's smbd file server supports SMB2 Copy-Chunk requests by default, no additional configuration is required.

**Limitations:**

* The `#Client_Support|client must support`, and issue SMB2 FSCTL_SRV_COPYCHUNK server-side copy requests.

=================
Btrfs Enhanced Server-Side Copy Offload
=================

Along with server-side copy support, a Btrfs specific Samba VFS Module was newly added with 4.1.0. This module improves the performance and efficiency of server-side copy operations on Btrfs backed shares, by using the Btrfs clone-range IOCTL. As of Samba 4.7.0, the VFS Module also offers support for FSCTL_DUPLICATE_EXTENTS_TO_FILE.

{| border="1" cellpadding="5" cellspacing="0"
|-
|
`File:Btrfs_serverside_copy.png` 
|
**Sequence**
# Client issues FSCTL_SRV_COPYCHUNK or FSCTL_DUPLICATE_EXTENTS_TO_FILE requests to server
# Server issues BTRFS_IOC_CLONE_RANGE filesystem ioctl

**Result**
* File data does **not** traverse network or disk
** Filesystem meta-data update only
* No duplication of file data
** Source and destination files share the same on disk data extents
|}

Btrfs enhanced server-side copy can be enabled in *smb.conf* on a per share basis with:

.. code-block::

       [share]
   path = /mnt/btrfs_fs/        # must reside within a Btrfs filesystem
   vfs objects = btrfs

**Limitations:**

* The client's FSCTL_SRV_COPYCHUNK or FSCTL_DUPLICATE_EXTENTS_TO_FILE server-side copy request must be Btrfs filesystem aligned.
** Otherwise, Samba FSCTL_SRV_COPYCHUNK falls back to a `#Server-Side_Copy_Offload|regular server-side copy`.
** FSCTL_DUPLICATE_EXTENTS_TO_FILE does not offer any local fall back, and instead leaves it up to the client.
* All other limitations associated with `#Server-Side_Copy_Offload|regular server-side copies`.

=================
Offload Data Transfer (ODX)
=================

ODX makes use of the [http://msdn.microsoft.com/en-us/library/windows/hardware/hh451101%28v=vs.85%29.aspx FSCTL_OFFLOAD_READ] and [http://msdn.microsoft.com/en-us/library/windows/hardware/hh451122%28v=vs.85%29.aspx FSCTL_OFFLOAD_WRITE] requests, such that the underlying storage subsystem independently transfers data, represented by tokens, between source and destination. This allows for offloaded data transfer between independent servers, provided that they are connected to an intelligent storage subsystem capable of generating and handling the ODX tokens. See Neal Christiansen's [http://www.snia.org/sites/default/orig/SDC2011/presentations/monday/NealChristiansen_Win8FileSystemPerformance.pdf SNIA SDC slides] for further details.

Samba does not currently support ODX - SMB2 FSCTL_SRV_COPYCHUNK server-side copies offer similar benefits, without the need for an ODX capable storage array or emulation within Samba.

----
`Category:Virtual File System Modules‏‎`
`Category:File Serving`