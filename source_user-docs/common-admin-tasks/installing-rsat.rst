Installing RSAT
    <namespace>0</namespace>
<last_edited>2021-02-14T22:27:55Z</last_edited>
<last_editor>TimMillerDyck</last_editor>

===============================

Introduction
===============================

To administer Active Directory (AD) from Windows, use the Microsoft Remote Server Administration Tools (RSAT). The tools are available for all platforms, Microsoft actively supports.

=================
Download
=================

* Windows 10 (1809 and later):ng RSAT is now integrated in these Windows 10 versions.

* Windows 10 (1703 - 1803):LLAASS:HHwww.microsoft.com/en-us/download/details.aspx?id=45520

* Windows 8.1:LAASS:HHwww.microsoft.com/en-us/download/details.aspx?id=39296

* Windows 8:LAASS:HHwww.microsoft.com/en-us/download/details.aspx?id=28972

* Windows 7:LAASS:HHwww.microsoft.com/en-us/download/details.aspx?id=7887

* Windows Vista:LAASS:HHwww.microsoft.com/en-us/download/details.aspx?id=21090

In Windows Server operating systems, the Microsoft Remote Server Administration Tools (RSAT) are included.

=================
Installation
=================

Windows 10 (1809 and later) 
------------------------

In Window 10 1809 and later, you install RSAT as an optional feature. Note that this requires an active internet connection.

To install RSAT:

* Click ``Start``, enter ``Apps & Features`` into the search field, and start the application.

* Click ``Optional features``.

* Click ``Add a feature``.

* Select a feature and click ``Install``.

* "wikitable"
!Feature
!Description
|-
|RSAT:licy Management Tools
|Provides the Group Policy MMC Snap-ins:nt Tool, Management Editor and Starter GPO Editor.
|-
|RSAT:irectory Domain Services and Lightweight Directory Services Tools
|Provides the ``Active Directory Users and Computers`` (ADUC) and ``Active Directory Sites and Services`` MMC Snap-in.
|-
|RSAT: r Tools
|DNS MMC Snap-in for remote DNS management.
|-
|RSAT: sktop Services Tool
|Optional. Adds the ``Remote Desktop Services Profile`` tab to the ADUC user object's properties and installs the ``RDP server administration`` MMC Snap-in. Install this feature to configure remote desktop protocol (RDP) settings in ADUC.
|}

Alternatively, RSAT tools can be installed using the built-in Windows DISM tool. As an example, run the following command in an elevated command prompt to install the RSAT Group Policy Management Tools, RSAT Active Directory Domain Services and Lightweight Directory Services Tools and RSAT DNS Server Tools.

``
<nowiki>
dism /online /add-capability /CapabilityName:TGroupPolicy.Management.Tools~~~~0.0.1.0 /CapabilityName:RsatDDOO:TTools~~~~0.0.1.0 /CapabilityName:Rsat.ActiveD:OOTTDS-LDS.Tools~~~~0.0.1.0
</nowiki>
``

Windows 8 and Windows 10 (1703 - 1803) 
------------------------

* Start the downloaded installer and follow the instructions. All features are installed automatically.

Windows Vista and 7 
------------------------

* Start the downloaded installer and follow the instructions.

* Click ``Start``, enter ``Programs and Features`` into the search field, and start the application.

* Select the features to install:
* owing are the recommended features to administer a Samba Active Directory installation::

* "wikitable"
!Feature
!Description
|-
|Group Policy Management Tools
|Provides the Group Policy MMC Snap-ins:nt Tool, Management Editor and Starter GPO Editor.
|-
|Active Directory Module for Windows PowerShell
|Optional. Enables Active Directory (AD) PowerShell cmdlets.
|-
|AD DS Tools
|Provides the ``Active Directory Users and Computers`` (ADUC) and ``Active Directory Sites and Services`` MMC Snap-in.
|-
|Server for NIS Tools
|Adds the ``UNIX Attributes`` tab to ADUC objects properties. It enables you to configure `Idmap_config_ad|RFC2307 attributes`.
|-
|DNS Server tools
|DNS MMC Snap-in for remote DNS management.
|-
|Remote Desktop Services Tool
|Optional. Adds the ``Remote Desktop Services Profile`` tab to the ADUC user object's properties and installs the ``RDP server administration`` MMC Snap-in. Install this feature to configure remote desktop protocol (RDP) settings in ADUC.
|}

* Click ``OK`` to install the features.

You can find the installed tools in the ``Administrative tools`` menu in your start menu. Alternatively, add the Snap-ins in the MMC using the ``File`` / ``Add/Remove Snap-in`` menu.

Windows Server 
------------------------

* Start the ``Server Manager``.

* On Windows Server 2012, 2012 R2, and 2016:
* lt;code>Add roles and features``.
* ``Role-based or feature-based installation``.
* the host on which to install the features.
* lt;code>Next`` on the ``Roles`` page.

* On Windows Server 2008 and 2008 R2:
* lt;code>Features`` in the navigation tree and click ``Add Features``.

* Select the features to install:
* owing are the recommended features to administer a Samba Active Directory installation::

* "wikitable"
!Feature
!Description
|-
|Group Policy Management
|Provides the Group Policy MMC Snap-ins:nt Tool, Management Editor and Starter GPO Editor.
|-
|AD DS Snap-Ins and Command-Line Tools
|Optional. Provides the ``Active Directory Users and Computers`` (ADUC) and ``Active Directory Sites and Services`` MMC Snap-in.
|-
|Server for NIS Tools
|Adds the ``UNIX Attributes`` tab to ADUC objects properties. It enables you to configure `Idmap_config_ad|RFC2307 attributes`.<br />This feature is not supported in Windows Server 2016. For details, see `#Missing_.22Unix_Attributes.22_tab_in_ADUC_on_Windows_10|Missing "Unix Attributes" tab in ADUC on Windows 10 and Windows Server 2016`.
|-
|Active Directory Module for Windows PowerShell
|Enables Active Directory (AD) PowerShell cmdlets.
|-
|DNS Server tools
|DNS MMC Snap-in for remote DNS management.
|}

=================
Enabling the ``Advanced Features`` Mode
=================

Many Remote Server Administration Tools (RSAT) provide additional features and options after enabling the ``Advanced Features`` option. To activate:

* Select the root of the navigation tree on the left side.

* Open the ``View`` menu.

* Select ``Advanced Features``.

* DUC_Ena:nced_Features.png`

=================
Missing ``Unix Attributes`` tab in ADUC on Windows 10 and Windows Server 2016
=================

Windows 10 and Windows Server 2016 do not support the ``Server for NIS Tools`` option. Without this feature, the Active Directory User and Computer (ADUC) console does not show the ``Unix Attributes`` tab on user and group objects. To work around this problem, set the attributes in Active Directory (AD) manually or use a different Windows operating system.

To manually set the attributes, use the ``Attributes`` tab on user and group object's properties. Note that this tab is only visible if you enabled the advanced features in ADUC. For further details, see `#Enabling_the_Advanced_Features_Mode|Enabling the "Advanced Features" Mode`.

The fields from the ``Unix Attributes`` tab are mapped to the following AD attributes of the object:

* Users:

* "wikitable"
!Field on the "Unix Attributes" tab
!Active Directory attribute
!Example value
|-
|NIS Domain
|msSFU30NisDomain
|samdom
|-
|UID
|uidNumber
|10000
|-
|Logon Shell
|loginShell
|/bin/bash
|-
|Home Directory
|unixHomeDirectory
|/home/user_name
|-
|Primary group name/GID
|primaryGroupID
|10000
|}
* example values to match your environment.

* Groups:

* "wikitable"
!Field on the "Unix Attributes" tab
!Active Directory attribute
!Example value
|-
|NIS Domain
|msSFU30NisDomain
|samdom
|-
|GID (Group ID)
|gidNumber
|10000
|}
* example values to match your environment.

.. note:

    If you set user IDs (UID) and group IDs (GID) manually, you must also track the last used UID and GID numbers manually.

=================
Reporting Problems and Incompatibilities
=================

To report problems or incompatibilities when using the Microsoft Remote Server Administration Tools (RSAT), see `Bug_Reporting|Bug Reporting`.

----
`Category:rectory`
`Category:gement`
`Category:icy Management`
`Category: