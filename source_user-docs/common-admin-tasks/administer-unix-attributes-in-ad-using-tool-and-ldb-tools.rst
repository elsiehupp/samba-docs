Administer Unix Attributes in AD using samba-tool and ldb-tools
    <namespace>0</namespace>
<last_edited>2018-07-23T14:24:27Z</last_edited>
<last_editor>Hortimech</last_editor>


===============================

Introduction
===============================

The following describes how to set/edit the RFC2307 attributes used by `Idmap_config_ad|idmap_ad`. This requires to have `Setting_up_RFC2307_in_AD#Verifying_the_Domain_Controller_and_Active_Directory_Setup|NIS extensions` installed in your AD. To administer the UNIX attributes via the Command line you should install ldb-tools ``ldbsearch, ldbmodify etc``, if not already installed. Modifications on user and group objects will be done by the Domain Administrator, if you haven't set any `Delegation/Account_management|delegations`.

=================
Names and Addresses used on this page
=================

* username               : sambauser
* groupname              : sambagroup
* Computer name          : sambacomputer
* domain naming context  : DC=samdom,DC=example,DC=com
* Netbios domain name    : samdom (aka workgroup)
* ID range               : 10000-999999
* Domain Users gidNumber : 10000
* login shell            : /bin/bash

.. note:

    Ensure that you use unique values for new users and groups.

=================
Creating a Unix user with samba-tool
=================

* Open a terminal on a DC and enter the following command:

 samba-tool user create sambauser passw5rd* --nis-domain=samdom --unix-home=/home/sambauser --uid-number=10005 --login-shell=/bin/bash --gid-number=10000

=================
Adding Unix attributes to an existing user account
=================

* Create an ldif (/tmp/user.ldif) containing something similar to the following information.

 dn: CN=sambauser,CN=User,DC=samdom,DC=example,DC=com
 changetype: modify
 add: uid
 uid: sambauser
 -
 add: msSFU30Name
 msSFU30Name: sambauser
 -
 add: msSFU30NisDomain
 msSFU30NisDomain: samdom
 -
 add: uidNumber
 uidNumber: 10001
 -
 add: gidNumber
 gidNumber: 10000
 -
 add: loginShell
 loginShell: /bin/bash
 -
 add: unixHomeDirectory
 unixHomeDirectory: /home/sambauser

* Close and save the ldif.

* Add the data with the following command

 ldbmodify -H /usr/local/samba/private/sam.ldb /tmp/user.ldif -U Administrator

=================
Creating a Unix group with samba-tool
=================

* Open a terminal on a DC and enter the following command:

 samba-tool group add sambagroup --nis-domain=samdom --gid-number=12345

=================
Adding Unix attributes to an existing group
=================

* Create an ldif (/tmp/group.ldif) containing something similar to the following information.

 dn: CN=sambagroup,CN=User,DC=samdom,DC=example,DC=com
 changetype: modify
 add: msSFU30NisDomain
 msSFU30NisDomain: samdom
 -
 add: msSFU30Name
 msSFU30Name: sambagroup
 -
 add: gidNumber
 gidNumber: 10001

* Close and save the ldif.

* Add the data with the following command

 ldbmodify -H /usr/local/samba/private/sam.ldb /tmp/group.ldif -U Administrator

=================
Adding Unix attributes to an existing computer account
=================

You need to set the uidNumber attribute to access samba shares on a domain with the Windows machine network account. 

* Create an ldif (/tmp/computer.ldif) containing something similar to the following information.

 dn: CN=sambacomputer,CN=Computers,DC=samdom,DC=example,DC=com
 changetype: modify
 add: uidNumber
 uidNumber: 10001

* Close and save the ldif.

* Add the data with the following command

 ldbmodify -H /usr/local/samba/private/sam.ldb /tmp/computer.ldif -U Administrator