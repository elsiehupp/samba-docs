Client specific logging
    <namespace>0</namespace>
<last_edited>2017-02-26T21:53:17Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

When diagnosing a problem, Samba developers are likely to request a level 10 log file.

There are different reasons for creating client specific log files:
* If the error appears only on specific clients and you won't change the config for all clients.
* If you're running Samba with many clients level 10 logs can fill your disk space very fast and slow down your system.

Create a new config file /etc/samba/smb.conf.client-debug
 [global]
 # no log file size limitation
 max log size = 0
 # specific log file name
 log file = /var/log/samba/log.%I
 # set the debug level
 log level = 10
 # add the pid to the log
 debug pid = yes
 # add the uid to the log
 debug uid = yes
 # add the debug class to the log
 debug class = yes
 # add microsecond resolution to timestamp
 debug hires timestamp = yes

Add the following line to your smb.conf at the end of the global section
 include = /etc/samba/smb.conf.client-%I

To activate level 10 logging for e.g. client 192.168.0.123 create a symbolic link:
 ln -s /etc/samba/smb.conf.client-debug /etc/samba/smb.conf.client-192.168.0.123

You do not have to restart all of Samba. If the client 192.168.0.123 connects to your samba the smbd includes the /etc/samba/smb.conf.client-192.168.0.123 config file and writes the debug information to /var/log/samba/log.192.168.0.123.
These changes will have no effect to other clients except 192.168.0.123. For additional clients you can simply create additional symlinks.

For changing the log level temporary you can use smbcontrol runtime.
 # set debug level for all smbd
 smbcontrol smbd debug 10

 # set debug level for process with pid 12345
 smbcontrol 12345 debug 10

 # request debug level for process with pid 12345
 smbcontrol 12345 debuglevel

----
`Category:Domain Members`
`Category:NT4 Domains`