Configure Samba to Bind to Specific Interfaces
    <namespace>0</namespace>
<last_edited>2017-02-26T21:50:42Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

If your Server uses multiple network interfaces, you can configure Samba to bind only to specific interfaces. For example, if Samba is installed on a router with one network interface connected to the internet and one to the internal network.

To bind all Samba services to the ``eth0</and the ``loopback``/>lo``) device:/

* Add the following parameters to the ``[global]</section of your ``smb.conf``/

 bind interfaces only = yes
 interfaces = lo eth0

* ode>interfaces</parameter enables you to use alternative values, such as IP addresses instead of device names. For further details, see the ``smb.conf (5)``/DOOTT

* t you should always enable Samba to listen on the ``loopback</(``lo``/DOOTT Several utilities connect to the loopback IP if no host name is provided.

* Restart the Samba service(s).

----
`Category:rectory`
`Category:mbers`
`Category:ns`