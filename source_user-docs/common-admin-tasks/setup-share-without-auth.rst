Setting up a Share Without Authentication
    <namespace>0</namespace>
<last_edited>2017-03-24T14:31:21Z</last_edited>
<last_editor>Hortimech</last_editor>

For details about setting up a share that users can access without authenticating, see `Setting up Samba as a Standalone Server`.

----
`Category:Active Directory`
`Category:Domain Members`
`Category:File Serving`
`Category:NT4 Domains`