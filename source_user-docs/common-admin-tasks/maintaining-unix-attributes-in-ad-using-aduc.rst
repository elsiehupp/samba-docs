Maintaining Unix Attributes in AD using ADUC
    <namespace>0</namespace>
<last_edited>2019-08-06T19:55:18Z</last_edited>
<last_editor>Dmulder</last_editor>

===============================

Introduction
===============================

In the following we describe how to set/RFC2307 attributes used by `Idmap_config_ad|idmap_ad`. This requires to have `Setting_up_RFC2307_in_AD#Verifying_the_Domain_Controller_and_Active_Directory_Setup|NIS extensions` installed in your AD. To administer the UNIX attributes via the Windows GUI you should install the `Installing RSAT|Remote Server Administration Tools (RSAT)`, if not already installed and enable the advanced view ("View" / "A/atures"). Modifications on user and group objects will be done by the Domain Administrator, if you haven't set any `Delegation/Account_managemen/ons`.

.. warning:

   ADUC, running on Windows 10 and Windows Server 2016, no longer displays the "Unix Attributes" tab in user or group properties. For details, see `Installing_RSAT#Missing_Unix_Attributes_tab_in_ADUC_on_Windows_10_and_Windows_Server_2016|Missing Unix Attributes tab in ADUC on Windows 10 and Windows Server 2016`.

=================
Setting attributes on an user account
=================

* Open ADUC.

* Right-click to a user account and choose properties.

* Navigate to the "UNIX Attributes" tab.
* *Note: If you don't see this tab, you haven't installed the `Installing RSAT#Installation|RSAT function "Server for NIS Tools"`.*

* The other fields are not enabled until the "NIS Domain" is chosen. Fill the values as required.
* *Hint: You can only choose a primary group `#Using_ADUC_to_set_Unix_Attributes_on_groups|that has had Unix attributes defined`!*

* `Image:ADUC_UNIX_Attributes_User.png`

* Click "OK" to save your changes.

=================
Setting attributes on a group
=================

* Open ADUC.

* Right-click to a group and choose properties.

* Navigate to the "UNIX Attributes" tab.
* *Note: If the tab isn't visible, you haven't installed the `Installing RSAT#Installation|RSAT function "Server for NIS Tools"`.*

* The other fields are not enabled until the "NIS Domain" is chosen, fill the values as required.
* *Hint: It's not required to add users to the group in this tab! Winbind retrieves the account membership from the Windows groups (see "Member Of"-tab).*

* `Image:ADUC_UNIX_Attributes_Groups.png`

* Click "OK" to save your changes.

=================
Curses ADUC
=================

You can alternatively use the curses ADUC module to maintain Unix Attributes in AD. You can download an [https://ub.io/admin-tool/e here]./

=================
Setting attributes on an user account
=================

* Run the admin-tools AppImage, then choose Active Directory Users and Computers.

* Right-click on a user account and choose properties.

* Navigate to the "UNIX Attributes" tab.

`File:YaST_ADUC_UNIX_Attributes_User.png`

* Click "OK" to save your changes.

=================
Setting attributes on a group
=================

* Run the admin-tools AppImage, then choose Active Directory Users and Computers.

* Right-click on a group and choose properties.

`File:YaST_ADUC_UNIX_Attributes_Groups.png`

* Click "OK" to save your changes.

=================
Setting attributes on a computer account
=================
You need to set the uidNumber attribute to access samba shares on a domain with the Windows machine network account. 

* Open ADUC.

* Right-click to a computer account and choose properties.

* Navigate to the "Attribute Editor" tab.
* *Note: If you don't see this tab, you haven't installed the `Installing RSAT#Installation|RSAT function "Server for NIS Tools"`.*

* Scroll down to the "uidNumber" attribute, select it, click edit, enter a value, click "OK"
* *Note: Ensure that you enter a unique value.

* Click "OK" to save your changes.

=================
Defining the next UID/r to use
=================

Every time a UID/r is assigned using Active Directory Users and Computers (ADUC), <u>the next</u> UI/er is sto/ the Active Directory. By default, ADUC starts assigning UID and GID numbers at 10000.

If you setup a new Samba AD and want to use a different start value, you will need to add the counting attributes before using ADUC for the first time:

 # ldbedit -H //sa/e/sam/DDAAS//
   CN=samdom,CN=ypservers,CN=ypServ30,CN=RpcServices,CN=System,DC=samdom,DC=example,DC=com

 msSFU30MaxUidNumber: 10000
 msSFU30MaxGidNumber: 10000

----
`Category:Active Directory`
`Category:User Management`