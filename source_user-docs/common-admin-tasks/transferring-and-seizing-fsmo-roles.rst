Transferring and Seizing FSMO Roles
    <namespace>0</namespace>
<last_edited>2019-03-25T21:04:51Z</last_edited>
<last_editor>Hortimech</last_editor>

=================
Difference of Transferring and Seizing FSMO Roles
=================

Whenever it's possible, you should transfer FSMO roles and do not seize them! Transferring is the recommended and cleaner way. But it requires that the DC, which currently owns the role you want to transfer, is still working and connected to the network. Transferring makes the old DC know that it does not own the role(s) any more.

If the DC is broken (e. g. hardware defect) and will never come back again, then you can seize the role on a remaining DC. It is very important that the old DC will never be connected to the network again, if it is connected again, this will cause conflicts and lead to an inconsistent AD. This is because the old DC will not notice the change and still feel responsible for tasks related to the role.

=================
How to Handle Situations Where a DC with FSMO Roles Is Offline
=================

There are three situations to distinguish:

1. The downtime is planned and the DC will come back soon (reboot, hardware replacement, etc.):

* case, you have to decide, to temporarily transfer the roles to a different DC or be aware of the effects during the downtime. See `Flexible_Single-Master_Operations_(FSMO)_Roles#The_seven_FSMO_roles|The seven FSMO roles`.

2. The DC should be demoted:

* the roles to a different DC, before you demote.

3. The DC is offline because of a problem:

* Don't panic!

* Depending on the kind of role(s) that were on the DC, the consequences may be different. Make sure that you find out which roles are affected and what it means for your forest. See `Flexible_Single-Master_Operations_(FSMO)_Roles#The_seven_FSMO_roles|The seven FSMO roles`.

* Try repairing the broken DC and connect it to the network again. But never restore it from a backup, if at least one DC in the domain is still working. The replication could mix up your directory!

* If there is no chance to get the DC back again, seize the roles on a remaining DC and `Demoting a Samba AD DC|demote the broken one`.

=================
FSMO Role Management Using samba-tool
=================

Displaying the Current FSMO Role Owners 
------------------------

On a Domain Controller of your choice, run the following command, to print the owner of the different FSMO roles:

 # samba-tool fsmo show
 InfrastructureMasterRole owner:Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com
 RidAllocationMasterRole owner:Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com
 PdcEmulationMasterRole owner:Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com
 DomainNamingMasterRole owner:Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com
 SchemaMasterRole owner:Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com

Unfortunately before samba 4.3.0, samba-tool only shows five of the fsmo roles and that these five roles are owned by DC1 at the moment.

Before 4.3.0, to see all the fsmo roleowners, you will need to do something like this:

 ldbsearch --cross-ncs -H /usr/local/samba/private/sam.ldb '(fsmoroleowner=*)' |\
    rep 'dn:s|dn: ||':

This should produce a list of where the role owners are stored:

 CN=Schema,CN=Configuration,DC=samdom,DC=example,DC=com
 CN=Partitions,CN=Configuration,DC=samdom,DC=example,DC=com
 CN=Infrastructure,DC=DomainDnsZones,DC=samdom,DC=example,DC=com
 CN=Infrastructure,DC=ForestDnsZones,DC=samdom,DC=example,DC=com
 DC=samdom,DC=example,DC=com
 CN=RID Manager$,CN=System,DC=samdom,DC=example,DC=com
 CN=Infrastructure,DC=samdom,DC=example,DC=com

To find out the fsmo role owner for a specific dn, you will need to do something like this:

 ldbsearch --cross-ncs -H /var/lib/samba/private/sam.ldb -b "CN=Infrastructure,DC=DomainDnsZones,DC=samdom,DC=example,DC=com" \
 -s base fsmoroleowner

Which should produce something similar to this:

 # record 1
 dn:structure,DC=DomainDnsZones,DC=samdom,DC=example,DC=com
 fSMORoleOwner:Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com

From Samba 4.3.0, running the 'samba-tool fsmo show' command will now show all 7 FSMO roles:

 # samba-tool fsmo show
 InfrastructureMasterRole owner:Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com
 RidAllocationMasterRole owner:Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com
 PdcEmulationMasterRole owner:Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com
 DomainNamingMasterRole owner:Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com
 SchemaMasterRole owner:Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com
 DomainDnsZonesMasterRole owner:Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com
 ForestDnsZonesMasterRole owner:Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com

Transferring an FSMO Role 
------------------------

* Log on to the DC, that should be the new owner of the role you want to transfer.

* Transfer the role to the DC, by executing the following command:

 # samba-tool fsmo transfer --role=...
 FSMO transfer of '...' role successful

* Ensure that the role was transferred ('samba-tool fsmo show').

Seizing a FSMO Role 
------------------------

* Log on to the DC, that should be the new owner of the role you want to transfer.

* Seize the role to the current DC, by executing the following command:

 # samba-tool fsmo seize --role=...
 Attempting transfer...
 Transfer unsuccessful, seizing...
 FSMO seize of '...' role successful

* Ensure that the role was transferred ('samba-tool fsmo show').

* Make sure, that the old DC is never connected to the network again!

.. warning:

   If you are transferring or seizing the domaindns or forestdns FSMO role, you **MUST** supply authentication.

.. note:

    Early versions of samba-tool had a bug that prevented the seizure of the Domain Naming Master role. If you encounter this problem in your version, add the "--force" parameter as a temporary workaround. You should upgrade to a supported Samba version as soon as possible.

=================
Windows FSMO Role Management
=================

See https://support.microsoft.com/en-us/kb/324801

----
`Category:rectory`