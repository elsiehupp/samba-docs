..  FAQ
    <namespace>0</namespace>
    <last_edited>2020-01-28T08:01:12Z</last_edited>
    <last_editor>Abartlet</last_editor>

==========================
Frequently Asked Questions
==========================


Introduction
------------

The questions listed here are frequently asked on the [http://lists.samba.org/archive/samba/ Samba mailing list].

General Samba Questions
-----------------------

**When Will the next Samba Version Be Released?**

For details, see :doc:`release-planning`.

**Can I Get Help with a Problem in an Unsupported Samba Version?**

Update to a supported version first. It is likely that the problem has been fixed in the meantime. Samba is actively developed and new minor versions fix several bugs and major versions additionally include new features. If you cannot update to the latest version in the current stable release series, update to the latest version in any other supported series. For details, see :doc:`Samba_Release_Planning|Samba Release Planning`.

If you are running a Samba version shipped with your distribution and that is no longer supported by Samba, contact your distribution's support for help.

**How Do I Update Samba?**

See :doc:`updating`.

**What Is the Maximum Size of a LDB or TDB Database File?**

* TDB files and LDB files using TDB

    The maximum size is 4 GB because the databases use 32-bit structures.

    Previously, there was a project called ``NTDB`` that should address the size limit and other problems. However, the project has been stopped because of problems migrating the databases.

* LDB files based on LMDB, specifically the sam.ldb on the AD DC

The size specified by the --backend-store-size=SIZE parameter to *samba-tool domain provision* and *samba-tool domain join* controls the maximum DB size.  The default is 8GB.  As LMDB is a true 64-bit database, the maximum is limited only by the storage available on the system.

==============================================
Samba as an Active Directory Domain Controller
==============================================

General 
-------

**Is Samba as an Active Directory Domain Controller Stable Enough for an Production Environment?**

Samba AD is stable for production environments. The AD DC support was introduced in the 4.0 version, which was released in December 2012. However, Samba AD has some unimplemented features, such as Sysvol replication.

**What Does ``ldap_bind: Strong(er) authentication required (8) additional info: :le: Transpo: Eencryption required`` Mean?**

See :doc:`Updating_Samba#New Default for LDAP Connections Requires Strong Authentication|Default for LDAP Connections Requires Strong Authentication`.

**I Am Running Samba as an AD DC. Which Windows Server Version Can I Join as an DC to the Forest?**

The following Windows server versions are supported as a DC together with a Samba DC:

+-------------------------------+---------------------------------------------------------------+
| Windows Server Version        | Comments                                                      |
+-------------------------------+---------------------------------------------------------------+
| Windows Server 2016           | Not supported.                                                |
+-------------------------------+---------------------------------------------------------------+
| Windows Server 2012 / 2012 R2 | Supported in Samba >=4.5. For details, see                    |
|                               | :doc:`joining-a-windows-server-2012-2012-r2-dc-to-a-samba-ad`.|
+-------------------------------+---------------------------------------------------------------+
| Windows Server 2008 / 2008 R2 | Supported in Samba >=4.0. For details, see                    |
|                               | :doc:`joining-a-windows-server-2008-2008-r2-dc-to-a-samba-ad`.|
+-------------------------------+---------------------------------------------------------------+
| Windows Server 2003 / 2003R2  | Not tested. Windows Server 2003 and 2003 R2                   |
|                               | are no longer supported by Microsoft.                         |
+-------------------------------+---------------------------------------------------------------+
| Windows 2000                  | Not tested. Windows Server 2003 and 2003 R2                   |
|                               | are no longer supported by Microsoft.                         |
+-------------------------------+---------------------------------------------------------------+

One of the limiting items is the AD schema version. For details, see :doc:`AD_Schema_Version_Support|AD Schema Version Support`.

**Why Is the Network Neighbourhood empty or Does Not Show All Machines in the Domain?**

The Samba AD DC ``smbd`` daemon does not support browsing. 

It is planned to add this feature. However, there are no development resources and thus no date when this feature will be included.

**What Does ``Warning: No NC replicated for Connection!`` Mean?**

When running the ``samba-tool drs showrepl`` command, the following warning is displayed at the end of the output:

    Warning: No NC replicated for Connection!

The warning appears because Samba incorrectly sets some flags when registering the DC for replication. The warning is harmless and can be ignored.

**Can I Use the Samba AD DC as a Fileserver?**

Whilst it is not recommended, yes you can, but you should be aware of its limitations, amongst which is that you cannot obtain the users Unix home directories or login shell from AD, you must use template lines in smb.conf

Configuration 
-------------

**Why Do I Not Have a ``server services`` parameter in My ``smb.conf`` File?**

The ``server services`` options in the ``smb.conf`` file are set during provisioning a Samba AD DC based on the settings you made during this process. If this parameter is not listed in the ``[global]`` section of your ``smb.conf`` file, the default values are used.

For details, see the ``smb.conf (5)`` man page.

**Can I Disable Some of the ``server services`` options in the ``smb.conf`` File?**

The ``server services`` options in the ``smb.conf`` file are set during provisioning a Samba AD DC based on the settings you made during this process.

Removing or modifying any of the options can result in an incorrectly operating Samba AD DC!

However, there are a few situations where you can manually update the options:

* To disable the network printing spooler:
  the ``spoolss`` option to ``-spoolss``.

* To switch the DNS back end:
  details, see :doc:`advanced-config/changing-dns-back-end-of-ad-dc`.

**How Do I Enable Guest Access to a Share on a Samba AD DC?**

On non-AD DCs, you can set the ``map to guest`` parameter in the ``smb.conf`` file to ``bad user`` to enable guest access. However, guest access is based on the ``guest account`` parameter, that is not implemented in the Samba AD mode.

**Can I Change the ID Range on a DC?**

Yes, very easily, just give your users ``uidNumber`` attributes containing numbers inside the range you want to use, you should also give ``Domain Users`` a ``gidNumber`` attribute containing a number inside the same range.

.. warning:

   Do not add any of the ``idmap_ad`` lines used on a domain member to your Samba AD DC smb.conf. They will have no affect and could lead to problems. 

Directory Schema 
----------------

**Which Active Directory Schema Versions Does Samba Support When Set up as a DC?**

For details, see :doc:`AD_Schema_Version_Support|AD Schema Version Support`.

**Is It Possible to Extend the Samba AD Schema?**

For details, see :doc:`Samba_AD_schema_extensions|Samba AD Schema Extensions`.

Kerberos 
--------

**What Does ``UpdateRefs failed with WERR_DS_DRA_BAD_NC/NT`` Mean?**

On the first start of a Samba DC in an existing Windows AD forest, the following error message is logged:

    UpdateRefs failed with WERR_DS_DRA_BAD_NC/NT code 0xc00020f8 for <DC_objectUID>._msdcs.samdom.example.com CN=RID Manager$,CN=System,DC=samba,DC=example,DC=com

This error is logged by the knowledge consistency checker (KCC), until the Windows DC has established the connections to the Samba DC.

To fix the problem, run:

* on your Windows DC:

.. code-block::

    C repadmin /kcc

* or alternatively on your Samba DC:

.. code-block::

    $ samba-tool drs kcc -Uadministrator Windows_DC.samdom.example.com

Replication 
-----------

**Do Samba AD DCs Support Replication?**

* Everything stored inside the AD, is replicated between DCs. For example: users, groups, and DNS records.

* In the current state, Samba does not support the distributed file system replication (DFS-R) protocol used for Sysvol replication. To work around, see :doc:`SysVol_replication_(DFS-R)|Sysvol Replication (DFS-R)`.

**Is a Samba DC Able to Replicate the Directory Content with an Non-AD LDAP Server?**

Active Directory uses a different schema than other LDAP servers and thus replicating with non-AD DCs is not supported or planned to be supported.

DNS 
---

**Can I Set Multiple Forwarder Servers for the Internal DNS Server?**

Setting multiple DNS forwarder servers is supported in Samba 4.5 and later versions.

For details, see :doc:`Samba_Internal_DNS_Back_End#Setting_up_a_DNS_Forwarder|Setting up a DNS Forwarder`.

**How Do I Set up the BIND DNS Server to Replicate AD DNS Zones?**

Updates of the Active Directory DNS zones are transferred automatically to other AD DNS servers using directory replication.

Zone transfers to non-AD DNS servers is not supported.

**Can I Use the ``.local`` Top-level Domain for My AD DNS Zone?**

Using the ``.local`` top-level domain is not recommended. For details, see :doc:`Active_Directory_Naming_FAQ#Using_an_Invalid_TLD|Using an Invalid TLD`.

Trust Support 
-------------

**Does Samba AD Supports Trust Relationship?**

The trust feature is experimental and has several limitations, such as:

* SID filtering rules are not applied
* You cannot add users and groups of a trusted domain into domain groups.

Group Policy Support 
--------------------

**Is It Possible to Set User Specific Password Policies in Samba AD, Such as on an Organisational Unit?**

Password settings are validated and applied by the DC to not enable modified Windows or Unix clients to bypass the rules. However, Samba does not support GPO restrictions and only serve GPOs to clients by the Sysvol share.

Use the ``samba-tool domain passwordsettings`` command to update password policies on a DC for a domain.

**What Does ``The permissions for this GPO in the SYSVOL folder are inconsistent with those in Active Directory`` Mean?**

When you click in the Group Policy Management Console to a GPO, the following error is displayed:

    The permissions for this GPO in the SYSVOL folder are inconsistent with those in Active Directory. It is recommended that these permissions be consistent. To change the SYSVOL permissions to those in Active Directory, click OK.

See the page :doc:`Sysvolreset` for troubleshooting steps.

LDAP 
----

**Do Samba AD DCs Support OpenLDAP or Other LDAP Servers as the Back End?**

Active Directory requires features, such as ACLs stored within the directory and a different schema, that are not supported by LDAP servers.

One of the main reasons people ask for OpenLDAP as the back end for AD, is that they are currently running Samba as an NT4 PDC using the OpenLDAP back end and want to migrate to Samba AD without manual transferring directory data to AD. However, even if OpenLDAP gets to be a supported back end on a Samba AD DC, the directory schema would be the AD schema. This means, you will have to update external applications accessing the directory using, such as you have to do it when you use the Samba internal LDAP server. Additionally you will have to import attributes manually from the old LDAP server that are not included in the AD schema.

**Is It Planned to Support OpenLDAP as Back End for Samba AD?**

Currently, there is no active work on this project.

The biggest problem is that a significant part of the complexity of the AD DC is in the LDB modules. Creating a general-purpose OpenLDAP back end requires rewriting many of these modules as OpenLDAP overlays, outside the standard Samba programming environment.  

Specific problems include:
* the metadata required for both DRS replication and dirsync
* schema manipulation
* transactions
* access control lists (ACL)

The Samba team decided not to peruse this as a development avenue, and no viable approach to re-opening this functionality has been proposed.

**Does the Samba Internal LDAP Server Supports Anonymous Searches?**

Samba honours the ``dSHeuristics`` flag. For details, see http://support.microsoft.com/kb/326690

However, enabling anonymous access to the AD raises security problems and is not recommended. Configure LDAP authentication or Kerberos support in your client instead.

Samba as an Domain Member
-------------------------

**Do I Provision a Samba Domain Member Using ``samba-tool``?**

From the roles the ``samba-tool domain provision --help`` command offers, the only supported provision role is ``DC`` (Active Directory domain controller).

Provisioning any other role, results in an incorrectly  working version of an AD DC. If you do provision a different role, remove all Samba database files and the generated ``smb.conf`` file and join the domain member using the ``net`` command. For details, see :doc:`Setting_up_Samba_as_a_Domain_Member|Setting up Samba as a Domain Member`.

**Which Windows Server Versions Are Supported as a Domain Member in a Samba AD? **

For details, see :doc:`Joining_a_Windows_Client_or_Server_to_a_Domain#Supported_Windows_Versions|Supported Windows Versions`.

**I Have Set up a Domain Member Using The ``idmap_ad`` Back End, but ``getent passwd`` and ``getent group`` Do Not Show Users, Computers or Groups **

Try explicitly asking for a user or group i.e. ``getent passwd auser``, this is because winbind doesn't enumerate users & groups by default any more.

Computers are never enumerated but only shown when queried explicitly i.e. ``getent passwd SAMDOM\hostname$``. 

If you want to show all users and groups, you will need to add these lines to smb.conf:

.. code-block:

    winbind enumerate users = yes
    winbind enumerate groups = yes

.. note:

    You should only add the lines for testing purposes 

If, after trying the above, you still do not get any users, groups or computers, check that:

* Your users have a ``uidNumber`` attribute containing a unique number inside the range set in smb.conf.
* : If : have ``idmap config DOMAIN : range =:10000-999999`` in smb.conf, your users ``uidNumber`` attributes should start at '10000' and go upto '999999', any number outside this range will be ignored.
* The Windows group ``Domain Users`` has a ``gidNumber`` attribute containing a number inside the same range, if ``Domain Users`` does not have a ``gidNumber`` ALL users will be ignored.
* Your computers have a ``uidNumber`` attribute as outlined above for users. Computers do not need a ``gidNumber``.
* Check that libnss_winbind is setup correctly, see :doc:`Libnss_winbind_Links|here`.
* Check that the ``passwd`` and ``group`` lines in /etc/nsswitch.conf have had 'winbind' added, see :doc:`Setting_up_Samba_as_a_Domain_Member#Configuring_the_Name_Service_Switch|here`.

If you set the uidNumber attribute for a computer, also known as the "Windows machine network account" e.g. SAMDOM\hostname$, the computer will have access to samba shares if they permit groups like ``Domain Computers``. This can be useful during startup.

Samba as NT4 Primary Domain Controller
--------------------------------------

**Do I Have to Migrate to Samba AD?**

One of the common misconceptions is: "Samba 4" means "Active Directory only": is wrong!

The Active Directory (AD) Domain Controller (DC) support is one of the enhancements in Samba 4.0. However all newer versions include the features of previous versions - including the NT4-style (classic) domain support. This means you can update a Samba 3.x NT4-style PDC to a recent version, like you updated in the past - for example from 3.5.x to 3.6.x. There is no need to migrate an NT4-style domain to an AD.

Additionally, all recent versions continue to support setting up a new NT4-style PDC. The AD support in Samba 4.0 and later is optional and does not replace any for the PDC feature. The Samba team understand the difficulty presented by existing LDAP structures. For that reason, there is no plan to remove the classic PDC support. Additionally we continue testing the PDC support in our continuous integration system.

**What Does ``User Administrator in your existing directory has SID ..., expected it to be ...-500 `` Mean?**

In your current NT4 domain, the RID of the domain administrator account is not ``500``. For details, see [http://support.microsoft.com/kb/243330/en Windows well-known security identifiers].

To fix:

* Remove the account. It will be recreated automatically during the classic upgrade.
* Update the RID of the account manually to ``500`` in your current Samba back end.

However, you have to reconfigure applications or file system ACLs listing the domain administrator, because in the back end the ``objectSID`` attribute is used to identify a user. Thus, after changing the RID of the account, it is a different account.

Samba as an standalone server
-----------------------------

**Why does Windows Network Neighborhood not show Samba server(s)?**

If you are using SMB2 or SMB3, network browsing uses WSD/LLMNR, which is not yet supported by Samba [https://bugzilla.samba.org/show_bug.cgi?id=11473]. SMB1 is disabled by default on the latest Windows versions for security reasons. It is still possible to access the Samba resources directly via \\name or \\ip.address.

If SMB1 is enabled on Windows, check that NetBIOS over TCP/IP is also
enabled, and that nmbd is started on the server.