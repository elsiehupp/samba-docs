Obtaining Samba
    <namespace>0</namespace>
<last_edited>2016-11-02T20:16:56Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

=================
Stable Version
=================

Running only stable versions of Samba is recommended.

Download the Samba sources only from [https://www.samba.org/ https://www.samba.org/].

Samba always maintains three release series:
* `Samba_Release_Planning#Current_Stable_Release|current stable`
* `Samba_Release_Planning#Maintenance_Mode|maintenance mode`
* `Samba_Release_Planning#Security_Fixes_Only_Mode|security fixes only mode`

The Samba team recommends that you always install the latest version of a `Samba_Release_Planning|supported release`. This guarantees that your installation contains all available security fixes.

In case you discover any problem, please update your installation to the latest version of the `Samba_Release_Planning#Current_Stable_Release|current stable` release before. This ensures that the problem has not been fixed in the meantime and reduces duplicate bug reports and unnecessary questions on the [https://lists.samba.org/mailman/listinfo/samba Samba Mailing List].

For further information about the release planning and the Samba life cycle, see `Samba_Release_Planning|Samba Release Planning`.

=================
Development Versions
=================

{{Imbox
| type = warning
| text = Never run a development version in a production environment. They contain untested features that can cause damages and data loss. Use development version only for testing purposes.

The samba-master Branch 
------------------------

The ``samba-master`` branch contains the source code of the `Samba_Release_Planning#Upcoming_Release|next release series`. To download:

 $ git clone git://git.samba.org/samba.git samba-master

Development Version of a Specific Branch 
------------------------

The ``v*-test`` branches contain the latest fixes and enhancements since the last release of the version source tree.

For example, to clone the latest development branch for Samba 4.5 using the git protocol:

 $ git clone -b v4-5-test git://git.samba.org/samba.git samba-v4-5-test

To clone the same branch using the HTTP protocol:

 $ git clone -b v4-5-test http://git.samba.org/samba.git samba-v4-5-test

Updating a Local Git Repository 
------------------------

To update a local cloned repository, run in the source directory:

 $ git pull