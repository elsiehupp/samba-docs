CTDB and Clustered Samba
    <namespace>0</namespace>
<last_edited>2021-02-01T05:59:56Z</last_edited>
<last_editor>MartinSchwenke</last_editor>

===============================

Introduction
===============================

CTDB is a clustered database component in clustered Samba that provides a high-availability load-sharing CIFS server cluster.

The main functions of CTDB are:

* Provide a clustered version of the TDB database with automatic rebuild/recovery of the databases upon node failures.

* Monitor nodes in the cluster and services running on each node.

* Manage a pool of public IP addresses that are used to provide services to clients. Alternatively, CTDB can be used with LVS.

Combined with a cluster filesystem CTDB provides a full high-availablity (HA) environment for services such as clustered Samba, NFS and other services.

=================
Supported platforms
=================

CTDB is primarily developed and tested on Linux.  A subset of features may work on other platforms.

=================
Getting and Building CTDB
=================

Samba and CTDB must be installed on all nodes of a cluster.  CTDB is part of Samba (&gt;= 4.2.0), so to get CTDB you need to [https://www.samba.org/samba/download/ get Samba].

More details:

* `Getting and Building CTDB`
* `Upgrading a CTDB cluster`

=================
Configuring Clusters with CTDB
=================

* `Basic CTDB configuration`
* `Setting up a cluster filesystem`
* `Configuring the CTDB recovery lock`
* `Adding public IP addresses`
* `Configuring clustered Samba`
* `Setting up CTDB for Clustered NFS`
* `Setting up CTDB with other services`
* `Advanced CTDB configuration`
* `Samba_CTDB_GPFS_Cluster_HowTo|CTDB with GPFS`
* `GFS_CTDB_HowTo|CTDB with GFS`

=================
Analysing CTDB problems
=================

* `Merged logs`
* `CTDB log file search patterns`

=================
Developing CTDB
=================

* `CTDB database design`
* `Running CTDB tests`

=================
Historical and Background Information
=================

Note that some of this information is out of date.

* `CTDB Project`
* `Clustered Samba`
* `Samba & Clustering`
* [http://samba.org/~obnox/presentations/sambaXP-2009/ Michael Adam's clustered NAS articles]