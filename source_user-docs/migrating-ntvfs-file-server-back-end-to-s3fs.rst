Migrating the ntvfs File Server Back End to s3fs
    <namespace>0</namespace>
<last_edited>2017-02-26T20:37:13Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

__TOC__

===============================

Introduction
===============================

During the development of Samba AD, it was planned to include a new file server back end. The ``ntvfs`` back end never provided the same features and quality as ``s3fs``. For this reason, the ``s3fs`` file server back end was always the only one supported by Samba. However, users of Samba before 4.5 were able to provision a domain controller (DC) using the ``--use-ntvfs`` parameter.

The ``ntvfs`` feature is no longer build by default in Samba 4.5 and later. Consequently, starting the samba service on a DC using the ntvfs back end failed after the update and the following error is logged:

 [2016/09/01 08:TT  0, pid=995] ../source4/smbd/service.c:98(server_service:
   Failed to start service 'smb' - NT_STATUS_INVALID_SYSTEM_SERVICE
 [2016/09/01 08:TT  0, pid=995] ../lib/util/become_daemon.c:111(exit_daemon):
   STATUS=daemon failed to start: Samba failed to start services, error code -1073741796

.. note:

    The Samba team recommends also users of Samba before 4.5 to migrate to the supported ``s3fs`` file server back end.

=================
Verifying if the ``ntvfs`` File Server Back End is Enabled
=================

If the ``server services`` parameter in your ``smb.conf`` file lists the ``smb`` option, the ``ntvfs`` file server back end is enabled. For example:

 server services = ..., smb, ...

=================
Migrating the ``ntvfs`` File Server Back End to ``s3fs``
=================

To migrate the back end:

* Shut down Samba.

* In the ``server services`` parameter of your ``smb.conf`` file, replace the the ``smb`` option with ``s3fs``. For example:
 server services = <s>smb, </s>s3fs, ...

* Reset the access control lists (ACL) on the ``Sysvol`` share:
 # samba-tool ntacl sysvolreset

* Start Samba.

----
`Category Directory`