..  <title>User Documentation</title>
    <namespace>0</namespace> 
    <last-edited>2020-03-31t20:30:32z</last-edited>
    <last-editor>gary</last-editor>

..  Sphinx Docs documentation master file, created by
    sphinx-quickstart on Fri May 28 00:25:35 2021.
    You can adapt this file completely to your liking, but it should at least
    contain the root toctree directive.

Welcome to Sphinx Docs's documentation!
=======================================

.. toctree::

    :maxdepth: 2
    :caption: Contents:

    faq
    release-planning
    features-added-changed-(by-release)|release-notes
    obtaining
    installing

        operating-system-requirements

            package-dependencies-required-to-build
            file-system-support

        build-from-source
        distribution-specific-package-installation

    updating

    domain-control

        active-directory-domain-controller

            active-directory-naming-faq
            setting-up-as-active-directory-domain-controller
            joining-dc-to-existing-active-directory
            joining-windows-server-2008-2008-r2-dc-to-ad
            joining-windows-server-2012-2012-r2-dc-to-ad
            migrating-nt4-domain-to-ad-(classic-upgrade)
            demoting-ad-dc
            ad-dns-back-ends

        internal-dns-back-end
        bind9-dlz-dns-back-end

              setting-up-bind-dns-server
              config-dhcp-to-update-dns-records-with-bind9

        testing-dynamic-dns-updates

            managing-ad-dc-service
            config-winbindd-on-ad-dc
            time-synchronisation
            verifying-directory-replication-statuses
            manually-replicating-directory-partitions
            running-ad-dc-with-mit-kerberos-kdc
            migrating-ntvfs-file-server-back-end-to-s3fs
            ad-dc-port-usage
            ad-dc-troubleshooting
            password-settings-objects
            running-ad-domain-controllers-in-large-domains
            using-lmdb-database-backend

        nt4-domains

            setting-up-as-nt4-pdc-(quick-start)

        nt4-pdc-port-usage

            setting-up-as-nt4-bdc
            required-settings-for-nt4-domains

    domain-membership

        joining-windows-client-or-server-to-domain

            windows-dns-config

        joining-linux-or-unix-host-to-domain

            linux-and-unix-dns-config
            setting-up-as-domain-member

        identity-mapping-back-ends

              idmap-config-ad-(rfc2307)
              idmap-config-rid
              idmap-config-autorid
              local-accounts

        auth-domain-users-using-pam
        pam-offline-auth

            domain-member-port-usage

        joining-mac-os-x-client-to-domain

            mac-os-x-dns-config

        config-freedos-to-access-share
        troubleshooting-domain-members

    standalone-server

    ctdb-and-clustered

    advanced-config

        server-logging

            audit-logging-setup

        dns-admin

            backend
            dns-admin
            dns-troubleshooting

        dns-tkey

        rfc2307-setup
        print-server

            print-server-setup
            win-auto-print-driver-setup
            creating-custom-paper-sizes
            network-printer-port-setup
            virtual-pdf-printer

        active-directory-sites
        ad-schema

            ad-schema-version-support
            ad-schema-extensions

        virtual-file-system-modules
        generating-keytabs

    replication

        distributed-file-system-(dfs)

            sysvol-replication-(dfs-r)

        rsync-based-sysvol-replication-workaround-(-dcs-only)
        bidirectional-rsync/unison-based-sysvol-replication-workaround-(-dcs-only)
        bidirectional-rsync/osync-based-sysvol-replication-workaround-(-dcs-only)
        robocopy-based-sysvol-replication-workaround-(-dcs->-windows-dcs)

        directory-replication-service-(drs)

    common-admin-tasks

        windows-remote-server-admin-tools-(rsat)

            installing-rsat

        remote-and-local-management-of

            user-and-group-management

        maintaining-unix-attributes-in-ad-using-aduc
        admin-unix-attributes-in-ad-using-tool-and-ldb-tools

        backup-and-restore

            back-up-and-restoring-ad-dc
            back-up-and-restoring-domain-member

        file-serving

            setting-up-share-using-windows-acls
            setting-up-share-using-posix-acls
            special-file-shares

        user-home-folders
        roaming-windows-user-profiles

              config-windows-profile-folder-redirections

        setting-up-share-without-auth

        flexible-single-master-operations-(fsmo)-roles

            transferring-and-seizing-fsmo-roles

        ad-functional-levels

            raising-functional-levels

        changing-ip-address-of-ad-dc
        config-ldap-over-ssl-(ldaps)-on-ad-dc
        delegating-administrative-permissions-to-non-administrators

            delegation/joining-machines-to-domain|joining-machines-to-domain
            delegation/account-management|account-management

        admin-workstations

            managing-local-groups-on-domain-members-via-gpo-restricted-groups

        working-with-active-directory-encoded-ldap-values
        performance-tuning
        config-to-bind-to-specific-interfaces
        server-side-copy|improving-performance-with-server-side-copy
        ad-smart-card-login
        vpn-single-signon-with-ad
        auth-other-services-against-ad

            auth-apache-against-active-directory
            openssh-single-sign-on
            auth-dovecot-against-active-directory

        openldap-as-proxy-to-ad
        client-specific-logging
        config-to-work-better-with-mac-os-x

    security

        security-documentation|security-documentation

    terms-and-abbreviations
    getting-help

        [https://lists.samba.org/mailman/listinfo/-mailing-list]
        [https://www.samba.org/samba/support/-commercial-support]

    contribute

        bug-reporting
        how-to-write-documentation



Indices and tables
==================

    :ref:genindex
    :ref:modindex
    :ref:search
