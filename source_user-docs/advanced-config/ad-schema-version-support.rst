AD Schema Version Support
    <namespace>0</namespace>
<last_edited>2019-07-16T23:19:11Z</last_edited>
<last_editor>Garming</last_editor>

=================
AD Schema Versions
=================

The official Active Directory (AD) schema versions are:

{| class="wikitable"
!Windows Server Version
!Directory Schema Version
|-
|Windows Server 2016
|87
|-
|Windows Server 2012R2
|69
|-
|Windows Server 2012
|56
|-
|Windows Server 2008R2
|47
|-
|Windows Server 2008
|44
|-
|Windows Server 2003R2
|31
|-
|Windows Server 2003
|30
|-
|Windows 2000
|13
|}

======================

Samba AD Schema Version Support
======================

Samba supports the following Active Directory schema versions:

{| class="wikitable"
!Samba Version
!Highest Supported Schema Version
|-
|4.11 and later
|69
|-
|4.5 - 4.10
|69 *
|-
|4.0 - 4.4
|47
|}

<nowiki>*</nowiki> Experimental support. To report problems, click https://bugzilla.samba.org.

=================
Live Upgrade
=================

To upgrade the schema version on a running Samba installation to 2012_R2, use this command:

 > samba-tool domain schemaupgrade

Note: This tool only works from 2008 R2. In order to upgrade from earlier versions, the ldf files must be manually sourced from the Windows adprep tool and run with the ``--ldf-file=`` option.

=================
Determine the AD Schema Version on a Samba DC
=================

To determine the Active Directory (AD) schema version on a Samba domain controller (DC):

 # ldbsearch -H /usr/local/samba/private/sam.ldb -b 'cn=Schema,cn=Configuration,dc=samdom,dc=example,dc=com' -s base objectVersion
 # record 1
 dn: CN=Schema,CN=Configuration,DC=samdom,DC=example,DC=com
 objectVersion: 47

 # returned 1 records
 # 1 entries
 # 0 referrals

=================
Determine the AD Schema Version on a Windows Client or Server
=================

dsquery 
------------------------

To determine the Active Directory (AD) schema version from the Windows command line:

 > dsquery * "cn=Schema,cn=Configuration,dc=samdom,dc=example,dc=com" -scope base -attr objectversion
 objectversion
 47

PowerShell 
------------------------

To determine the Active Directory (AD) schema version from the Windows PowerShell:

 > Get-ADObject (Get-ADRootDSE).schemaNamingContext -properties objectVersion
 DistinguishedName : CN=Schema,CN=Configuration,DC=contoso,DC=com
 Name              : Schema
 ObjectClass       : dMD
 ObjectGUID        : 92801cf6-f22d-4b7f-ba70-00af98790881
 objectversion     : 47

----
`Category:Active Directory`