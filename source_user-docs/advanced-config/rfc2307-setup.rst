Setting up RFC2307 in AD
    <namespace>0</namespace>
<last_edited>2021-04-05T08:47:58Z</last_edited>
<last_editor>Hortimech</last_editor>

===============================

Introduction
===============================

The use of [https://rfc-editor.org/rfc/rfc2/xtS/FC 2307] attributes allows the storage of Unix user and group information in an LDAP directory. In an Active Directory (AD) with Linux integration, this has several advantages::
* Central administration of IDs in AD.
* Consistent IDs on all Linux domain members that use the Samba ``idmap_ad</ ID map back end.
* Fast configuration of attributes.
* No local ID mapping databases that can corrupt and lead to lose of file ownerships.
* Enable the administrator to set individual login shells and home directory paths for users.
* Login shell and home directory settings are the same on all domain members using Samba ``idmap_ad</ ID map back end and ``winbind nss info = rfc2307``/parameter.
* Easy management from Windows clients using the Active Directory Users and Computers (ADUC) Microsoft management console (MMC). For details, see `Maintaining_Unix_Attributes_in_AD_using_ADUC|Maintaining Unix Attributes in AD using ADUC`.

RFC2307 on AD Domain Controllers 
------------------------

It is recommended to only have the sysvol and netlogon shares on an AD DC, so using RFC2307 id-mappings on the DC is not required. If you want to enable RFC2307 ID mappings on the DC for whatever reason e.g. you have other shares on the DC (not recommended) and are using the winbind 'ad' backend on Unix domain members, you need to ensure that the ``idmap_ldb rfc2307</ parameter exists in the ``[global]``/section of your ``smb.conf`` / on the Samba DC and is set to ``yes`` ::/

    dmap_ldb rfc2307 = yes

.. note:

    It is not recommended to use RFC2307 mappings on Samba AD DC's. The default idmap.ldb mechanism is fine for domain controllers and less error prone.

 Verifying That the NIS Extensions Are Installed in Active Directory 
------------------------

Verify that the ``ypServ30</ LDAP tree exists in your Active Directory (AD):

 # ldbsearch -H //sa/e/sam/  /HHs / -b \
 CN=ypservers,CN=ypServ30,CN=RpcServices,CN=System,DC=samdom,DC=example,DC=com cn

The output should be:

 # record 1
 dn: CN=ypservers,CN=ypServ30,CN=RpcServices,CN=System,DC=samdom,DC=example,DC=com
 cn: ypservers

 # returned 1 records
 # 1 entries
 # 0 referrals

If the ``ldbsearch</ command returns 1 record, the NIS Extensions are installed.

.. note:

    The NIS Extensions are only required if you are going to use the ADUC Unix Attributes tabs to manage your users and groups.

Provisioning a New Samba Active Directory with RFC2307 Enabled 
------------------------

When you provision a new Samba AD forest, pass the ``--use-rfc2307</ to the ``samba-tool domain provision``/command to auto-install the NIS extensions. For example:

 # samba-tool domain provision --use-rfc2307 ...

For details, see `Setting_up_Samba_as_an_Active_Directory_Domain_Controller#Provisioning_a_Samba_Active_Directory|Provisioning a Samba Active Directory`.

Additionally, enable the the Samba RFC2307 module. For details, see `#Enabling_the_RFC2307_Configuration_Parameter|Enabling the RFC2307 Configuration Parameter`.

Installing the NIS Extensions
------------------------

Do not run this procedure if you provisioned your Active Directory (AD) with the ``--use-rfc2307</ parameter. For details, see `#Provisioning_a_New_Samba_Active_Directory_with_RFC2307_Enabled|Provisioning a New Samba Active Directory with RFC2307 Enabled`.

{{Imbox
| type = warning
| text = Updating the Schema can break your AD. Verify you have a working backup before updating the schema.

To install the NIS extensions:

* Locate the domain controller (DC) with the ``Schema Master</ flexible single-master operations (FSMO) role:

 # samba-tool fsmo show | grep SchemaMasterRole
 SchemaMasterRole owner: CN=NTDS Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com

* EThe output shows the name of the DC owning this role. Run all further steps on this DC.

* Shut down the Samba service.

* Create a copy of the ``ypServ30.ldif</ schema file. For example:

 # cp //sa/setup/DOOTT/CCEE/////

* Replace the variables in copied LDIF file with the domain distinguished name (DN), NetBIOS name, and the NIS domain of your setup. For example:
* DN}: :>DC=samdom,DC=example,DC=com</
* SNAME}: :>DC1</
* AIN}: :>samdom</

 # sed -i -e 's/N}/<u&gt/,DC=example,DC=com</u>/g' //
          -e 's/NAME}/<u&gt/>/g' //
          -e 's/IN}/<u&gt/;/u>/g' //
          /v30/

* Import the modified LDIF file to the local ``//sa/e/sam/lt;/c/ / AD /

 # ldbmodify -H //sa/e/sam/  /pServ30/ -DDAA/=&q/ update allowed"=true
 Modified 55 records successfully

* Start the Samba service.

The AD replicates the updated schema to all DCs in the forest.

----
`Category Directory`