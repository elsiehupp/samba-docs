Setting up Samba as a Print Server
    <namespace>0</namespace>
<last_edited>2019-11-18T15:15:50Z</last_edited>
<last_editor>BBaumbach</last_editor>

===============================

Introduction
===============================

If you set up Samba as a print server, clients in your network are able to send print jobs to the Samba host using the server message block (SMB) protocol. The examples shown in this documentation use a raw printer in the back end. This configuration requires that the print job is formatted by a driver on the client and thus can be processed by the printer without further processing or filtering.

=================
Supported Print Server Back Ends
=================

Samba supports the multiple print server back ends, such as [https://www.cups.org/ CUPS] and [http:SSLLAASS:HHwww.lprng.com/ LPRng]. For a complete list, see the ``printing`` parameter in the ``smb.conf(5)`` man page.

.. note:

    You must set up the printer server back end locally on the Samba host. Samba cannot forward print jobs to a remote host. However, you can configure the local printer server back end to forward the job to a remote print server.

For details how to set up the back end, see the print server's documentation.

Samba ``CUPS`` or ``IPRINT`` Back End Support 
------------------------

When using the ``CUPS`` or ``IPRINT`` print server back end, Samba must have been built with CUPS support enabled. To verify, enter:

 # smbd -b | grep "HAVE_CUPS"
    HAVE_CUPS

If no output is displayed:
* Samba was built using the ``--disable-cups`` parameter.
* The Samba ``configure`` script was unable to locate the required libraries for CUPS support. For details, see `Package Dependencies Required to Build Samba`.

=================
Adding a printer to the Print Server Back End
=================

CUPS 
------------------------

To add a raw printer to an CUPS print server:

* Open the CUPS admin web interface in your browser. For example, <nowiki>https://servername:631SSLLA:</nowiki>

* Select the ``Administration`` tab and click ``Add Printer``.

* Select the connection type and enter the corresponding URL to the printer's queue or to the remote print server queue. For example:
* SSHHbased printers: <cod:SLLAASSHHSSLLAAS: r_name*/*queue*``
* ternet Printing Protocol)-based printers: <cod:SLLAASSHHSSLLAAS: r_name*/ipp/port``
* rver Message Block)-based printers: <cod:SLLAASSHHSSLLAAS:ame*:*password*@*domain*SSL:indows_print_server_host_name*/*printer_name*``
* :ing a job to a print server running Windows Vista or newer, or Windows Server 2008 or newer requires authentication.

* Enter a name for the printer. This name is used in the ``smb.conf`` when sharing the printer using Samba.

* Select the ``Raw`` printer vendor and model.

* Save the settings.

LPRng 
------------------------

To add a raw printer to a LPRng print server:

* Add the following line to the ``/etc/printcap`` file:

 *printer_name*:SSHHvar/spool/lpd/*printer_name*/:sh:mx=0::Pr:name:ress:
* ter name is used in the smb.conf when sharing the printer using Samba.
* her details about the options used, see the ``printcap(5)`` man page.

* To create the spool directory, enter:

 # checkpc -f

* Restart the LPRng service.

=================
Enabling the ``spoolssd`` Service
=================

The Samba ``spoolssd`` is a service that is integrated into the smbd service. If you configured Samba as a print server, you can additionally enable ``spoolssd`` to increase performance on print servers with a high number of jobs or printers.
* ``spoolssd``, Samba forks the ``smbd`` process or each print job and initializes the ``printcap`` cache. In case of a large number of printers, the ``smbd`` service can become unresponsive for multiple seconds when initializing the cache. The ``spoolssd`` service enables you to start pre-forked ``smbd`` processes that are processing print jobs without any delay. The main ``spoolssd`` ``smbd`` process uses a low amount of memory, and forks and terminates child processes

To enable the ``spoolssd`` service:

* Edit the ``[global]`` section in your ``smb.conf`` file:

* following parameters::

 rpc_server: external
 rpc_daemon:= fork

* lly, you can set the following parameters::
* :itable"
!Parameter
!Default
!Description
|-
|spoolssd:in_children
|5
|Minimum number of child processes
|-
|spoolssd:ax_children
|25
|Maximum number of child processes
|-
|spoolssd:pawn_rate
|5
|Samba forks this number of new child processes, up to the value set in ``spoolssd:ax_children``, if a new connection is established
|-
|spoolssd:ax_allowed_clients
|100
|Number of clients, a child process serves
|-
|spoolssd:hild_min_life
|60
|Minimum lifetime of a child process in seconds. 60 seconds is the minimum.
|}

* Restart Samba.

After the restart, Samba automatically starts ``smbd`` sub-processes:
 # ps axf
 ...
 30903 smbd
 30912  \_ smbd
 30913      \_ smbd
 30914      \_ smbd
 30915      \_ smbd
 ...

=================
Enabling the Print Server Support in Samba
=================

To enable the print server support:

* Set the printing back end in the ``printing`` parameter of the ``[global]`` section in your ``smb.conf`` file. For example:
 printing = CUPS

* Add the following section to your ``smb.conf``:

 [printers]
        path = /var/spool/samba/
        printable = yes

* Create the spool directory set in the ``path`` parameter:

 # mkdir -p /var/spool/samba/
 # chmod 1777 /var/spool/samba/

* Reload Samba:

 # smbcontrol all reload-config

=================
Sharing a Printer
=================

Automatic Sharing of All Printers Configured in the Print Server Back End == 

Using the default setting, all printers configured in the print server back end are automatically shared.

Disabling the Automatic Printer Sharing
------------------------

To disable the automatic printer sharing:

* Add the following parameter to the ``[global]`` section of your ``smb.conf`` file:

 load printers = no

* Reload Samba:

 # smbcontrol all reload-config

Manual Sharing of Printers 
------------------------

To manually share a printer:

* Verify that the automatic printer sharing is disabled. See `#Disabling_the_Automatic_Printer_Sharing|Disabling the Automatic Printer Sharing`.

* Add the share for the printer to your ``smb.conf`` file:

 [*Samba_printer_name*]
        path = /var/spool/samba/
        printable = yes
        printer name = *printer_name_in_the_back_end*

* ``printer name`` parameter to the name of the printer used in the local print server back end.

* Reload Samba:

 # smbcontrol all reload-config

=================
Setting up Automatic Printer Driver Download for Windows Clients
=================

See `Setting_up_Automatic_Printer_Driver_Downloads_for_Windows_Clients|Setting up Automatic Printer Driver Downloads for Windows Clients`.

----
`Category:rectory`
`Category:mbers`
`Category:ns`
`Category:]
`Category:  Server`