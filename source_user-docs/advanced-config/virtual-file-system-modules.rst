Virtual File System Modules
    <namespace>0</namespace>
<last_edited>2018-05-18T11:48:10Z</last_edited>
<last_editor>Ddiss</last_editor>

__TOC__

The Virtual File System (VFS) in Samba enable administrators to extend the functionality of Samba through modules.

=================
VFS Modules Provided by Samba
=================

Samba provides the following VFS modules:

{| class="wikitable"
!Name of the VFS Module
!Feature Provided by the Module
|-
|`Using the acl_tdb VFS Module|acl_tdb`
|Stores Windows Access Control Lists (ACL) in a Trivial Database (TDB) file.
|-
|`Using the acl_xattr VFS Module|acl_xattr`
|Enables to use fine-granular Windows ACL.
|-
|`Using the aio_fork VFS Module|aio_fork`
|Implements asynchronous I/O in a VFS module.
|-
|`Using the aio_linux VFS Module|aio_linux`
|Implements asynchronous I/O in a VFS module using Linux kernel AIO calls.
|-
|`Using the aio_pthread VFS Module|aio_pthread`
|Implements asynchronous I/O in a VFS using a ``pthread`` pool.
|-
|`Using the audit VFS Module|audit`
|Logs client operations.
|-
|`Using the btrfs VFS Module|btrfs`
|On ``Btrfs`` file systems, this module enables [https://wiki.samba.org/index.php/Server-Side_Copy#Btrfs_Enhanced_Server-Side_Copy_Offload enhanced server-side copy], compression and snapshots.
|-
|`Using the cacheprime VFS Module|cacheprime`
|Prime the Kernel file data cache.
|-
|`Using the cap VFS Module|cap`
|Provides support for Columbia Appletalk Protocol (CAP) encoded file names.
|-
|`Using the catia VFS Module|catia`
|Translates illegal characters in file names used by the ``Catia`` application.
|-
|`Using the commit VFS Module|commit`
|Synchronizes cached data to disk after the size reached a specified size.
|-
|`Using the crossrename VFS Module|crossrename`
|Enables server-side rename operations across file system boundaries.
|-
|`Using the default_quota VFS Module|default_quota`
|Enables you to set a default quota.
|-
|`Using the dirsort VFS Module|dirsort`
|Sors directory content alphabetically before sending the entries to the client.
|-
|`Using the extd_audit VFS Module|extd_audit`
|Records selected VFS operations.
|-
|`Using the fake_perms VFS Module|fake_perms`
|Enables read-only profiles by sending faked permissions to the Windows client.
|-
|`Using the fileid VFS Module|fileid`
|Generates ``file_id`` structs with unique device ID values for cluster setups.
|-
|`Using the fruit VFS Module|fruit`
|Provides enhanced compatibility with Apple Server Message Block (SMB) clients and interoperability with Netatalk 3 Apple Filing Protocol (AFP) file servers.
|-
|`Using the full_audit VFS Module|full_audit`
|Records selected client operations.
|-
|`Using the gpfs VFS Module|gpfs`
|Enables General Parallel File System (GPFS) specific extensions, such as ACL.
|-
|`Using the linux_xfs_sgid VFS Module|linux_xfs_sgid`
|Enables a workaround for an old Linux XFS bug.
|-
|`Using the media_harmony VFS Module|media_harmony`
|Enable multiple ``Avid`` clients to share a network drive.
|-
|`Using the netatalk VFS Module|netatalk`
|Hides ``.AppleDouble`` files generated by AFP clients.
|-
|`Using the offline VFS Module|offline`
|Flags all files on a share as offline.
|-
|`Using the prealloc VFS Module|prealloc`
|Preallocate matching files to a predetermined size.
|-
|`Using the preopen VFS Module|preopen`
|Assists applications that want to read numberes files in sequence with a very strict latency requirement.
|-
|`Using the readahead VFS Module|readahead`
|Pre-loads the Kernel buffer cache.
|-
|`Using the readonly VFS Module|readonly`
|On a writeable share, the module sets the share read-only for a specific time period.
|-
|`Using the recycle VFS Module|recycle`
|Moves files to a temporary directory rather than deleting them immediately.
|-
|`Using the shadow_copy VFS Module|shadow_copy`
|Enables Windows clients to use browse snapshots as shadow copies.
|-
|`Using the shadow_copy2 VFS Module|shadow_copy2`
|Enables Windows clients to use browse snapshots as shadow copies. Contains enhanced functionality compared to the ``shadow_copy`` module.
|-
|`Using the shell_snap VFS Module|shell_snap`
|Runs shell scripts to create or delete a snapshot when a client issues the operation using the File Server Remote VSS Protocol (FSRVP).
|-
|`Using the snapper VFS Module|snapper`
|Exposes snapshots managed by ``Snapper`` to Windows clients as shadow copies.
|-
|`Using the streams_depot VFS Module|streams_depot`
|Enables storing Alternative Data Streams (ADS) in a central directory.
|-
|`Using the streams_xattr VFS Module|streams_xattr`
|Enables ADS support.
|-
|`Using the syncops VFS Module|syncops`
|Ensures that meta data operations on a file system are performed synchronously.
|-
|`Using the time_audit VFS Module|time_audit`
|Logs system calls that take longer than a defined time.
|-
|`Using the tsmsm VFS Module|tsmsm`
|Handles offline files with ``Tivoli Storage Manager Space Management``.
|-
|`Using the unityed_media VFS Module|unityed_media`
|Enable multiple ``Avid`` clients to share a network drive. This is an enhanced version of the ``media_harmony`` module.
|-
|`Using the worm VFS Module|worm`
|Provides Write Once Read Many (WORM) functionality.
|-
|`Using the xattr_tdb VFS Module|xattr_tdb`
|Stores extended attributes in a TDB file.
|}

=================
Developing VFS Modules
=================

For details about developing Samba VFS modules, see:

* `Writing_a_Samba_VFS_Module|Writing a Samba VFS Module` (Samba 4.0 and later)

* `The_new_version_of_Writing_a_Samba_VFS|The new version of Writing a Samba VFS` (Samba 3.6 and earlier)

----
`Category:Active Directory`
`Category:NT4 Domains`
`Category:Domain Members`
`Category:Virtual File System Modules`