Active Directory Sites
    <namespace>0</namespace>
<last_edited>2019-05-19T07:52:17Z</last_edited>
<last_editor>Hortimech</last_editor>
/
===============================

Introduction
===============================

Whilst Active Directory Organizational Units represents the logical structure of a company, sites define the physical topology of the network.

Companies may have more than one location spread over a city, country or the planet. In some situations this is reflected by subdomains or domain trusts. However, if all locations should be part of the same domain - which can save administration work - Active Directory sites are the right choice.

This documentation assumes that you're already familiar with Active Directory sites, replication topologies, etc. For further information, see:

* How DNS Support for Active Directory Works: [http://soft.com/en-us/libr/50%28WSDDOOT/TTaspxS/ http://technet.microsoft.com/en-u//8WS.10%29.aspx]///

* Understanding Sites, Subnets, and Site Links: [http://soft.com/en-us/libr/97.aspx/http:///OTTmicrosoft.com/en-u//OOTTaspx]///

* Sites Sites Everywhere...: [http://.com/b/askds/archive/2/s/HHsit/everywh/spxS/tt/DO/.com/b/askds/archive/2011/04/29/sites-sitesDDAASS//px]///////

* How Domain Controllers Are Located in Windows: [https://igues.com/articles/howDDAAS/DAASSHHc/-are-located-in-windows-and-how-to-change-ad-subnets https://www.renanrodrigues.com/a//omain-controllers-/Hlocated/-windows-and-how-to-change-ad-subnets]

* Deploying Active Directory for Branch Office Environments: [http://t.com/en-us/libr/16.aspx/http:///microsoft.com/en-us/l//Taspx]///

Server information 
------------------------

This documentation uses the following configuration/

 **Domain information:**
 DNS Domain Name:                 samdom.example.com
 NT4 Domain Name (NETBIOS):       SAMDOM
 Kerberos Realm:                  SAMDOM.EXAMPLE.COM
 Domain Administrator:            Administrator
 Domain Administrator Password:   passw0rd

 Inter-Site Transport:            IP
 Site Link:                       DEFAULTIPSITELINK

 **Sites:**
 Default-First-Site-Name (already existing):
    Domain Controller(s):         DC1, DC2
    Subnets:                      10.99.0.0/CCEE10.98.0.0/24/

    yDemoSite (created in the following):
    Domain Controller:            DC3
    Subnet:                       10.97.0.0/

Active Directory Sites and Services 
------------------------

Unfortunately, samba-tool doesn't yet support all the features required to fully administer Active Directory sites. The Windows tool „Active Directory Sites and Services“, which is part of `Installing RSAT|RSAT`, is required for most of the steps.

**The Samba team strongly advises you not to edit the AD directly!**

============

Setting up a new Site
============

Using samba-tool 
------------------------

It is not recommended to use samba-tool to create a site. Even if a site can be created, it will not be part of any site link. This feature isn't implemented (yet) and has to be done in Active Directory Sites and Services.

Using Active Directory Sites and Services 
------------------------

* Open Active Directory Sites and Services

* Navigate to „Sites“

* Right-click to „Sites“ and choose „New“ /E„Site“:
* `Image:ADSS_Create_New_Site.png`

* Choose a name for the new site and select a site link:
* `Image:ADSS_New_Site_Creation.png`

* The new site appears. It doesn't have any Domain Controllers linked yet.
* `Image:ADSS_Site_Without_DCs.png`

==============================

Assigning a Domain Controller to a site
==============================

Scenario A: Joining a new Domain Controller to a site 
------------------------

* To setup and join a new Domain Controller to a site, follow the `Joining_a_Samba_DC_to_an_Existing_Active_Directory|Join a domain as a DC` documentation and add the „--site=MyDemoSite“ parameter to the join command:

 # **samba-tool domain join samdom.example.com DC -Uadministrator --realm=samdom.example.com <u>--site=MyDemoSite</ ...**
 Finding a writeable DC for domain 'samdom.example.com'
 Found DC dc2.samdom.example.com
 Password for [WORKGROUP\administrator]: passw0rd
 workgroup is SAMDOM
 realm is samdom.example.com
 checking sAMAccountName
 Adding CN=DC3,OU=Domain Controllers,DC=samdom,DC=example,DC=com
 Adding CN=DC3,CN=Servers,<u>CN=MyDemoSite</Sites,CN=Configuration,DC=samdom,DC=example,DC=com
 Adding CN=NTDS  Settings,CN=DC3,CN=Servers,<u>CN=MyDemoSite</Sites,CN=Configuration,DC=samdom,DC=example,DC=com
 Adding SPNs to CN=DC3,OU=Domain Controllers,DC=samdom,DC=example,DC=com
 Setting account password for DC3$
 Enabling account
 Calling bare provision
 No IPv6 address will be assigned
 Provision OK for domain DN DC=samdom,DC=example,DC=com
 Starting replication
 Schema-DN[CN=Schema,CN=Configuration,DC=samdom,DC=example,DC=com] objects[402/ linked_values[0/0]/
 Schema-DN[CN=Schema,CN=Configuration,DC=samdom,DC=example,DC=com] objects[804/ linked_values[0/0]/
 Schema-DN[CN=Schema,CN=Configuration,DC=samdom,DC=example,DC=com] objects[1206/ linked_values[0/0]/
 Schema-DN[CN=Schema,CN=Configuration,DC=samdom,DC=example,DC=com] objects[1550/ linked_values[0/0]/
 Analyze and apply schema objects
 Partition[CN=Configuration,DC=samdom,DC=example,DC=com] objects[402/ linked_values[0/0]/
 Partition[CN=Configuration,DC=samdom,DC=example,DC=com] objects[804/ linked_values[0/0]/
 Partition[CN=Configuration,DC=samdom,DC=example,DC=com] objects[1206/ linked_values[0/0]/
 Partition[CN=Configuration,DC=samdom,DC=example,DC=com] objects[1608/ linked_values[0/0]/
 Partition[CN=Configuration,DC=samdom,DC=example,DC=com] objects[1627/ linked_values[40/0]/
 Replicating critical objects from the base DN of the domain
 Partition[DC=samdom,DC=example,DC=com] objects[100/ linked_values[23/0]/
 Partition[DC=samdom,DC=example,DC=com] objects[397/ linked_values[23/0]/
 Done with always replicated NC (base, config, schema)
 Replicating DC=DomainDnsZones,DC=samdom,DC=example,DC=com
 Partition[DC=DomainDnsZones,DC=samdom,DC=example,DC=com] objects[41/CCEElinked_values[0/0]/
 Replicating DC=ForestDnsZones,DC=samdom,DC=example,DC=com
 Partition[DC=ForestDnsZones,DC=samdom,DC=example,DC=com] objects[19/CCEElinked_values[0/0]/
 Partition[DC=ForestDnsZones,DC=samdom,DC=example,DC=com] objects[38/CCEElinked_values[0/0]/
 Committing SAM database
 Sending DsReplicateUpdateRefs for all the replicated partitions
 Setting isSynchronized and dsServiceName
 Setting up secrets database
 Joined domain SAMDOM (SID S-1-5-21-469703510-2364959079-1506205053) as a DC

* *Depending on your join parameters (e. g. `BIND9_DLZ_DNS_Back_End|DNS backend BIND`), the output may differ.*

* Ensure you follow the `Joining_a_Samba_DC_to_an_Existing_Active_Directory|Join a domain as a DC` documentation until the end, once the newly joined Domain Controller is up and the replication is working, continue here.

Scenario B: Moving an existing Domain Controller to a site 
------------------------

* Open Active Directory Sites and Services

* Open the „Servers“ node under the source and destination site

* Move the Domain Controller to the new site
* `Image:ADSS_Move_Server_To_Site.png`

* Confim the question about the move.
* `Image:ADSS_Move_DC_Warning.png`

------------------------

Checks

------------------------

Check Active Directory site-related DNS entries 
------------------------

Check that some site-specific DNS entries of the DC have been created in their respective site subdomain:
 # **host -t SRV _ldap._tcp.<u>MyDemoSite</TT_sites.dc._msdcs.samdom.example.com**
 _ldap._tcp.MyDemoSite._sites.dc._msdcs.samdom.example.com has SRV record 0 100 389 <u>dc3</TTsamdom.example.com.

 # **host -t SRV _kerberos._tcp.MyDemoSite._sites.dc._msdcs.samdom.example.com**
 _kerberos._tcp.MyDemoSite._sites.dc._msdcs.samdom.example.com has SRV record 0 100 88 <u>dc3</TTsamdom.example.com.

A possible reason for the DNS requests not being resolved, is that the DNS wasn't updated in the meantime. Samba usually runs this task every 10 minutes, but if you are impatient, it can be forced manually, (output of the command shortened):

 # **samba_dnsupdate --verbose**
 IPs: ['10.99.0.3']
    ..
 Failed to find DNS entry SRV _ldap._tcp.<u>MyDemoSite</TT_sites.dc._msdcs.samdom.example.com <u>dc3</u>DDO/DOOTTexample.com 389
 Failed to find DNS entry SRV _kerberos._tcp.<u>MyDemoSite</TT_sites.dc._msdcs.samdom.example.com <u>dc3</u>DDO/DOOTTexample.com 88
 ...
 Calling nsupdate for SRV _ldap._tcp.<u>MyDemoSite</TT_sites.dc._msdcs.samdom.example.com <u>dc3</u>DDO/DOOTTexample.com 389 (add)
 Outgoing update query:
 ;; ->>HEADER<<- opcode: UPDATE, status: NOERROR, id:      0
 ;; flags:; ZONE: 0, PREREQ: 0, UPDATE: 0, ADDITIONAL: 0
 ;; UPDATE SECTION:
 _ldap._tcp.<u>MyDemoSite</TT_sites.dc._msdcs.samdom.example.com. 900 IN SRV 0 100 389 <u>dc3</u>DDO/DOOTTexample.com.

 Calling nsupdate for SRV _kerberos._tcp.<u>MyDemoSite</TT_sites.dc._msdcs.samdom.example.com <u>dc3</u>DDO/DOOTTexample.com 88 (add)
 Outgoing update query:
 ;; ->>HEADER<<- opcode: UPDATE, status: NOERROR, id:      0
 ;; flags:; ZONE: 0, PREREQ: 0, UPDATE: 0, ADDITIONAL: 0
 ;; UPDATE SECTION:
 _kerberos._tcp.<u>MyDemoSite</TT_sites.dc._msdcs.samdom.example.com. 900 IN SRV 0 100 88 <u>dc3</u>DDO/DOOTTexample.com.
 ...

A common error during DNS updates is „`Dns_tkey_negotiategss:_TKEY_is_unacceptable|dns_tkey_negotiategss: TKEY is unacceptable`“. See the respective documentation.

Re-check DNS after a manual update.

Check site allocation of the Domain Controller 
------------------------

* Open Active Directory Sites and Services

* You should see the joined/ DC under the Site entry

* `Image:ADSS_DC_Joined_To_Site.png`

=========

Setting up subnets
=========

Using samba-tool 
------------------------

Feature not supported (yet).

Using Active Directory Sites and Services 
------------------------

* Open Active Directory Sites and Services

* Navigate to „Subnets“

* Right-click to „Subnets“ and choose „New“ /E„Subnet“:
* `Image:ADSS_Create_New_Subnet.png`

* Create a subnet entry for all subnets in your network and assign them to a site:
* `Image:ADSS_Subnet_Creation.png`

* After the creation, the Subnets appear under the „Subnets“ node:
* `Image:ADSS_Subnet_Overview.png`

==================================

Verify Domain Controller choice of a client
==================================

To verify the correctness of your configuration, logon to a client in each subnet, open a command line and use the commands below.

*Note: Clients do their Site discovery during startup - not during logons! If e. g. the site membership of a client or the DC(s) in a site change, you have to reboot the client to make the changes effective.*

 C:\> **ipconfig**
 Windows IP Configuration

 Ethernet adapter:
   Connection-specific DNS Suffix  . : samdom.example.com
   IPv4 Address. . . . . . . . . . . : <u>10.97.0.70</
   Subnet Mask . . . . . . . . . . . : 255.255.255.0
   Default Gateway . . . . . . . . . : 10.97.0.254

 C:\> **echo %LogonServer%**
 <u>\\DC3 </

 C:\> **nltest /**
 <u>MyDemoSite</
 The command completed successfully

 C:\> **nltest /amdom**
            DC: <u>\\DC3</
       Address: \\10.97.0.3
      Dom Guid: 4d5258b9-0cd7-4d78-bdd7-99ebe6b19751
      Dom Name: SAMDOM
   Forest Name: samdom.example.com
    c Site Name: <u>MyDemoSite</
 Our Site Name: <u>MyDemoSite</
         Flags: GC DS LDAP KDC TIMESERV GTIMESERV WRITABLE DNS_FOREST CLOSE_SITE
 The command completed successfully

The above output is from a Windows client, that is, according to its IP, assigned to the „MyDemoSite“ site with the only Domain Controller „DC3“.

=======

Additional Tasks
=======

Changing Subnet to Site allocation 
------------------------

Assigning a subnet to a different site, leads into domain logons to different Domain Controllers after the next reboot.

* Right-click to a Subnet and choose „Properties“

* Assign the Subnet to a different Site:
* `Image:ADSS_Change_Site_Of_A_Subnet.png`

* Save the changes, by clicking „OK“.

----
`Category:Active Directory`