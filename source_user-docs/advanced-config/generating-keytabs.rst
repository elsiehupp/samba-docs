Generating Keytabs
    <namespace>0</namespace>
<last_edited>2016-09-15T09:24:59Z</last_edited>
<last_editor>Hortimech</last_editor>

Active directory requires Kerberos service principal names to be mapped to a user account before a keytab can be generated.

You can add SPN names to a user with samba-tool, this is provided with your samba 4 installation.

 samba-tool spn add host/fdqn@KerberosRealm <sAMAccount name> 

This should return without error.

Once the SPN is added, you can then generate a keytab for the user with samba-tool, by running the following:

 samba-tool domain exportkeytab  <name>.keytab  --principal=[<sAMAccount name> | <SPN>]

This should then produce a keytab called <name>.keytab containing the users upn or the spn, depending on which is given with '--principal' and this can then be copied to your target machine or service.

**Note:** replace <sAMAccount name> with a valid user name, <SPN> with the spn you added earlier and <name> with whatever you what the keytab to be called, this can also include a path to where you want the keytab to be created.
You should only use <sAMAccount name> or <SPN>, you should not use both.

=================
Adding Enctypes to an Account
=================

By default a users keytab will contain the following enctypes:
     arcfour-hmac aes256-cts-hmac-sha1-96 aes128-cts-hmac-sha1-96 des-cbc-md5 des-cbc-crc

But if you export a keytab using '--principal' it will only contain these enctypes:

 arcfour-hmac des-cbc-md5 des-cbc-crc

**To add the two stronger enctypes:**

Log into A DC as root, then run 'kinit Administrator'. You can then use the 'net ads enctypes set' command to add the enctypes

 net ads enctypes set <ACCOUNTNAME>

This should print something like this:

 'ACCOUNTNAME' uses "msDS-SupportedEncryptionTypes": 31 (0x0000001f)
 [X] 0x00000001 DES-CBC-CRC
 [X] 0x00000002 DES-CBC-MD5
 [X] 0x00000004 RC4-HMAC
 [X] 0x00000008 AES128-CTS-HMAC-SHA1-96
 [X] 0x00000010 AES256-CTS-HMAC-SHA1-96