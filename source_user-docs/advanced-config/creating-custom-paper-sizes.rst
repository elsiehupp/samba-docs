Creating Custom Paper Sizes
    <namespace>0</namespace>
<last_edited>2017-02-26T21:14:50Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

To create custom paper sizes on a Samba print server:

* `Setting_up_Samba_as_a_Print_Server#Granting_the_SePrintOperatorPrivilege_Privilege|Connect to the print server using the Print Management console`.

* Navigate to the ``Print Servers`` entry, double-click your print server, and right-click to the ``Forms`` entry.

* Select ``Manage Forms``.

* `Image:Print_Management_Forms.png`

* Select ``Create a new form`` and fill the fields.
* `Image:Print_Management_Create_New_Form.png`

* Click ``Save Form``.

* Click ``OK``.

The new form is stored in the ``/usr/local/samba/var/locks/registry.tdb`` database on the Samba print server.

You can select the form when preconfiguring a printer. For details, see `Setting_up_Automatic_Printer_Driver_Downloads_for_Windows_Clients#Preconfiguring_a_Printer|Preconfiguring a Printer`.

----
`Category:Active Directory`
`Category:Domain Members`
`Category:NT4 Domains`
`Category:Printing`
`Category:Standalone Server`