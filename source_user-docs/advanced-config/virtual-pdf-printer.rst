Virtual PDF Printer
    <namespace>0</namespace>
<last_edited>2017-02-26T21:14:53Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

Beside the various PDF printer programs, which require an installation on clients, it might be useful, to have a virtual Samba printer. This enables users to create and automatically store PDF files on the server, as simple as printing. No additional software installation is required on the clients, if your `Setting_up_Samba_as_a_Print_Server|Samba Print Server` provides Point'n'Print support.

=================
Requirements
=================

* `Setting_up_Samba_as_a_Print_Server|Samba configured as a Print Server`
* ps2pdf (Part of GhostScript, what is shipped typically with every distribution)
* Bash (script is written in Bash)

=================
Setup
=================

* Download the Bash script `File:Pdfprint.sh.txt`

* Rename the script to „Pdfprint.sh“ and place it in /usr/local/bin/

* Make the script executable
 # chmod 755 /usr/local/bin/Pdfprint.sh

* Add the following configuration to your smb.conf (adjust the spool path and print command, if necessary):
 [PDFprinter]
         comment = Samba Virtual PDF Printer
         path = /var/spool/samba
         printable = Yes
         lpq command
=================
         lprm command
=================
         print command = /usr/local/bin/Pdfprint.sh -s /var/spool/samba/%s \
                         -d /home/%U -o %U -m 600
* For a complete list of all usable variables, see the smb.conf manpage ("Variable substitution", "print command")

* Check out the scripts help for other parameters, that might be useful
 # /usr/local/bin/Pdfprint.sh -h

* Reload Samba

* If you want to use automatic Point'n'Print driver installation,  `Setting_up_Samba_as_a_Print_Server#Uploading_printer_drivers_for_Point.27n.27Print_driver_installation|upload a PostScript driver to your Samba server`. You can use any PostScript driver, like the [http://www.hp.com/go/upd HP Universal Print Driver PostScript].

* If you don't want to use Point'n'Print driver installation, add the following line to the printer configuration:
 use client driver = yes

* `Setting_up_Samba_as_a_Print_Server#Associating_a_shared_printer_with_a_driver_and_preconfiguring|Associate the PDFprinter with the uploaded driver`

=================
Using the virtual PDF printer
=================

* Connect the printer on your client.

* Simply send a print job to the PDFprinter and the PDF is stored in the configured path with the defined permissions.

=================
Solving problems
=================

If the PDF isn't stored in its destination, enable the logging option of the script.

 [PDFprinter]
         .....
         print command = /usr/local/bin/Pdfprint.sh ..... -l /var/log/samba/Pdfprint.log

----
`Category:Active Directory`
`Category:Domain Members`
`Category:NT4 Domains`
`Category:Printing`
`Category:Standalone Server`