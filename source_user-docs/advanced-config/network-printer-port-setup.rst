Setting up Network Printer Ports
    <namespace>0</namespace>
<last_edited>2017-02-26T21:14:52Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

===============================

Introduction
===============================

In Windows, printers must have a port assigned. This can be a local port, such as ``LPT1``, or for example for a remote port, ``LPD Port Monitor``. Per default, Samba auto-generates the ``Samba Printer Port`` for all shared printers. However, in certain scenarios, you require to use unique port names.

=================
How Samba Provides Custom Ports to the Windows Hosts
=================

Samba enables you to use a script that outputs only the port names:

* The script can only output the port names by using ``echo`` commands. For example:

 #!/bin/bash
 echo "DemoPrinter1"
 echo "DemoPrinter2"
 echo "DemoPrinter3"

* You can write a script that generates the port names from configuration files, databases, or other sources.

.. warning:

   Every line the script outputs is used as a printer port. Make sure that the script does not output other content.

=================
Setting up Printer Ports
=================

To enable Samba to display individual printer ports:

* Create a shell script. For example, ``/usr/local/bin/samba-ports.sh`` that outputs each port in a new line. For details, see `#How_Samba_Provides_Custom_Ports_to_the_Windows_Hosts|How Samba Provides Custom Ports to the Windows Hosts`.

* Make the script executable:

 # chmod 755 /usr/local/bin/samba-ports.sh

* Add the following parameter to the ``[global]`` section in your ``smb.conf`` file:

 enumports command = /usr/local/bin/samba-ports.sh

* Reload Samba:

 # smbcontrol all reload-config

The ports are now usable in the properties when preconfiguring a printer.

`Image:Printer_Ports.png`

For details, see `Setting_up_Automatic_Printer_Driver_Downloads_for_Windows_Clients#Preconfiguring_a_Printer|Preconfiguring a Printer`.

----
`Category:Active Directory`
`Category:Domain Members`
`Category:NT4 Domains`
`Category:Printing`
`Category:Standalone Server`