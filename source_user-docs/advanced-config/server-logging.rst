Configuring Logging on a Samba Server
    <namespace>0</namespace>
<last_edited>2017-07-23T16:50:28Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

===============================

Introduction
===============================

On a Samba server you can use logging to write detailed log files to find and debug problems, or to monitor events, such as users connecting to a share. Setting a log level enable you to control the amount of data that is logged. Additionally, you can use debug classes you to set individual log levels for certain events, such as authentication or Winbind-related events, while logging all other events on a different level.

=================
Setting the Log File Name
=================

Samba logs events to the file set in the ``log file`` parameter. You can either set this parameter to one static file name to log all events to one file, or you can use variables to create individual log files. For example, to create log files for each connecting host named ``*NetBIOS_name*.log`` in the ``/var/log/samba/`` folder:

* Set the ``log file`` parameter in the ``[global]`` section in the ``smb.conf``:

 log file = /var/log/samba/%m.log

* For further details, see:
* * ``log file`` parameter description in the ``smb.conf (5)`` man page
* * ``VARIABLE SUBSTITUTIONS`` section in the ``smb.conf (5)`` man page

* Reload Samba:

 # smbcontrol all reload-config

=================
Setting the Maximum Log File Size
=================

To control the maximum size of Samba log files, set the ``max log size`` parameter. The parameter takes the value in KB. If the size exceeds the value set, Samba appends ``.old`` to the log file name and writes new log entries to a new file. 

.. note:

    During the log file is rotated, Samba overrides a previously archived version of the log file.

To set the maximum log size to 10 MB:

* Set the ``max log size`` parameter in the ``[global]`` section in the ``smb.conf`` to ``10000``:

 max log size = 10000

* Reload Samba:

 # smbcontrol all reload-config

=================
Setting the Log Level
=================

Using the default settings, logging is disabled. To enable logging, set the ``log level`` parameter in the ``[global]`` section in the ``smb.conf``.

.. note:

    A higher log level includes logging of events from lower levels.

Setting a Universal Log Level 
------------------------

In most scenarios you set set one log level for all events. For example, to set the log level to ``1`` (lowest level were Samba writes log entries):

* Set the ``log level`` parameter in the ``[global]`` section in the ``smb.conf`` to ``1``:

 log level = 1

* Reload Samba:

 # smbcontrol all reload-config

Setting Individual Log Levels for Debug Classes 
------------------------

In certain situations, for example, to debug authentication problems, you need to set a higher log level. However, setting a higher log level causes Samba to log all events on the higher level, what can result in large log files. Samba enables you to set individual log levels for certain debug classes, while logging all other events on a different level.

For example, to set the default log level to ``1`` and log authentication and Winbind-related events on log level ``5``:

* Set the ``log level`` parameter in the ``[global]`` section in the ``smb.conf``:

 log level = 1 auth:5 winbind:5

* For a list of supported debug classes, see the ``log level`` parameter description in the ``smb.conf (5)`` man page.

* Reload Samba:

 # smbcontrol all reload-config

=================
Related Documentation
=================

* `Setting up Audit Logging`

----
`Category:Active Directory`
`Category:Domain Members‏‎`
`Category:NT4 Domains‏‎`
`Category:Standalone Server‏‎`