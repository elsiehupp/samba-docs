Samba AD schema extensions
    <namespace>0</namespace>
<last_edited>2020-12-22T01:19:50Z</last_edited>
<last_editor>Kirin</last_editor>

=================
Schema Extension in Samba Active Directory
=================

Samba AD supports the same kind of schema extensions as Microsoft Active Directory. Schema updates in AD are a sensitive action and you must be prepared to do a `Back_up_and_Restoring_a_Samba_AD_DC|full restore of the DC` holding the role of schema master if something goes wrong.

This is even more true in Samba 4 given it does fully validate all the changes to the schema and so some changes can lead to a un-start-able samba provision.

Disabled by default 
------------------------

See this [https://lists.samba.org/archive/samba-technical/2020-June/135415.html mail from metze] for the current details on why schema updates in Samba AD are currently disabled by default. 

In order to allow them, the option *dsdb:schema update allowed* must be set to true in the *smb.conf* or passed on the command line.

Dangerous, but just don't be silly
------------------------

A reasonable summary of Samba's state of schema updates is that it is:
* not exactly compatible with Windows behaviour
* additions of normal attributes are pretty safe
* modifications to a mayContain is likewise fairly safe
* you can still break your Samba if you start making unusual modifications (links on schema objects)
* **never ever** change a ``ldapDisplayName``

=================
Preparing to Make Schema Changes
=================

Schema changes in Samba 4 is different to a normal LDAP server (IE 389 Directory Server, OpenLDAP). In Samba 4 and Active Directory, schema changes are one way. You may only add schema, but you can never remove it. Additionally, you can modify schema, but only to extend specific attributes - again, never removing it.

This means that mistakes in the design of your schema, or your choice of schema imports may not be possible to undo.

It is highly recommended that you test all schema changes in a development and staging environment before committing these to production like systems. Further you should test a production-restored database in a staging environment for your schema changes to understand the process. Restoring your production environment from backup is costly in time and resources, and may cause an extended outage as a result, so all testing should be done separately.

You should document this process and steps so that in a true disaster case, you can confidently perform recovery operations.

You should also consider the ergonomics of your schema changes and how they may work with access controls. For example, let's use the following schema (OpenLDAP formatted, not AD formatted):

``
objectClass: olcSchemaConfig

cn: openssh-openldap

olcAttributeTypes: {0}( 1.3.6.1.4.1.24552.500.1.1.1.13 NAME 'sshPublicKey' DESC 'MANDATORY: OpenSSH Public key' EQUALITY octetStringMatch SYNTAX 1.3.6.1.4.1.1466.115.121.1.40 )

olcObjectClasses: {0}( 1.3.6.1.4.1.24552.500.1.1.2.0 NAME 'ldapPublicKey' DESC 'MANDATORY: OpenSSH LPK objectclass' SUP top AUXILIARY MUST ( sshPublicKey $ uid ) )
``

Notice the sshPublicKey attribute is "MUST" not "MAY"? This means that if a user was granted self-edit rights over their sshPublicKey attribute, and it did not exist before you begin, they would also require the ability to extend their objectClasses. Extended objectClasses can lead to security issues, so a user should not be required to self-manage this.

A better design is to make sshPublicKey "MAY" in this case, so that the administrator can grant the sshPublicKey class to all accounts, and then users can have 0 or more keys in a self editable form. 

This attention to detail is critical in Samba 4 schema changes, as you can not undo once committed which may have long term, subtle and frustrating consequences.

=================
Verified Schema Extensions
=================

As getting an LDIF that won't ruin the provision can be hard, this page will list LDIFs that are known not to break the database.

NIS Extensions 
------------------------

See `Setting_up_RFC2307_in_AD#Enabling_RFC2307_in_an_Existing_Active_Directory|Installing the NIS Extensions`.

Automounter 
------------------------

This extension allow you to store automount information in LDAP. In order to add this extension, follow these steps:

* Download `Media:Automount_template.ldif.txt|Automount_template.ldif.txt`, this is a template that will be transformed in the next steps
* Locate the rootDN of your provision: *ldbsearch -H ldap://ip_of_server -U administrator -s base dn
* Run *cat automount_template.ldif  | sed 's/DOMAIN_TOP_DN/value_of_rootDN_obtained_in_previous_step/' > automount.ldif *
* Split the resulting file into two, one containing the attributes, the other containing the objectclasses
* Name the two new files *automount_attrs.ldif* and *automount_classes.ldif*
* Stop Samba4 on the schema master
* Copy *automount_attrs.ldif* and *automount_classes.ldif* to the schema master server (if you were working on a different server)
* Apply the ldifs with commands similar to:
** *ldbmodify -H path_to_sam_ldb automount_attrs.ldif --option="dsdb:schema update allowed"=true
** *ldbmodify -H path_to_sam_ldb automount_classes.ldif --option="dsdb:schema update allowed"=true

sshPublicKeys 
------------------------

This allows storage of sshPublicKeys in Samba 4 LDAP that can be used with tools like sss_ssh_authorizedkeys command.

LDIF: `Media:Sshpubkey.ldif.txt|sshpubkey.ldif`

Ldap Compat (EntryUUIDs) 
------------------------

Many applications use ldap EntryUUID as a primary key. If you are migrating a posix directory to Samba 4, you may need to preserve these id's. The two major id's are EntryUUID (OpenLDAP) and nsUniqueId (389 Directory Server). This allows them to be stored in Samba 4

LDIF: `Media:Ldapcompat.ldif.txt|ldapcompat.ldif`

===============================
Building your own schema extensions
===============================

OID
------------------------

* You will need an OID.  Ideally your organisation or company has an OID arc, or you are converting an existing schema
* This will be the ``attributeID`` or ``classID``
** [http://oid-info.com/faq.htm#10 How to get a unique OID arc]
** Samba OIDs are under: [http://oid-info.com/get/1.3.6.1.4.1.7165 1.3.6.1.4.1.7165]
** Samba's registry of internally allocated OIDs are in [https://git.samba.org/?p=samba.git;a=blob;f=source4/setup/schema_samba4.ldif source4/setup/schema_samba4.ldif] and [https://git.samba.org/?p=samba.git;a=blob;f=examples/LDAP/samba.schema examples/LDAP/samba.schema].

===============================
Internal use and experimental Samba OIDs
===============================

------------------------

``1.3.6.1.4.1.7165.777.0.X`` is reserved in Samba for use by Samba installations internally.

===============================
Wiki OIDs
===============================

------------------------

If you use a OID from this range ``1.3.6.1.4.1.7165.777.2.x``, please note your use below.

* ``1.3.6.1.4.1.7165.777.1.1.x`` Attributes in examples in this wiki page
* ``1.3.6.1.4.1.7165.777.1.2.x`` Classes in examples in this wiki page

* ``1.3.6.1.4.1.7165.777.2.x`` Next available arc.

Syntax for attributes
------------------------

* See [https://docs.microsoft.com/en-us/windows/win32/ad/choosing-a-syntax Microsoft's Documentaion on choosing a syntax]
* You need to choose the Syntax ID ``attributeSyntax`` (eg 2.5.5.10), Samba will map that to the and OM ID (eg 4) for you.
* Decide if this will be ``isSingleValued`` (``TRUE`` or by default ``FALSE``)
Name
------------------------

* You need a clear, ideally globally unique, name for your attribute or class, this will be the ``ldapDisplayName`` and ``CN``
* Consider re-using names from [schema.org schema.org] if there is no existing example for LDAP
* LDAP schema are traditionally camalCase with a lowercase initial letter

Fill in your this template
------------------------

This will create a new auxiliary class to allow your new attribute to be stored

 dn: CN=${NAME},CN=Schema,CN=Configuration,${DOMAIN_TOP_DN}
 objectClass: attributeSchema
 attributeID: ${OID}
 lDAPDisplayName: ${NAME}
 description: ${DESCRIPTION}
 attributeSyntax: 2.5.5.5
 isSingleValued: TRUE

 dn: CN=${CLASS_NAME},CN=Schema,CN=Configuration,${DOMAIN_TOP_DN}
 objectClass: classSchema
 governsID: ${CLASS_OID}
 lDAPDisplayName: ${CLASS_NAME}
 subClassOf: top
 objectClassCategory: 3
 description: ${DESCRIPTION}
 mayContain: ${NAME}

Example
------------------------

* From [https://schema.org/Role Role on schema.org]

The startDate is recorded as a string per [https://groups.google.com/forum/#!topic/mozilla.dev.tech.ldap/Ast6WzQbgsQ this advice] and per the ISO 8601 date format that a [https://schema.org/Date Date is specified as for schema.org schemas].

 dn: CN=startDate,CN=Schema,CN=Configuration,DC=samba,DC=example,DC=com
 objectClass: attributeSchema
 attributeID: 1.3.6.1.4.1.7165.777.1.1.1
 lDAPDisplayName: startDate
 description: Role start date
 attributeSyntax: 2.5.5.5
 isSingleValued: TRUE

 dn: CN=endDate,CN=Schema,CN=Configuration,DC=samba,DC=example,DC=com
 objectClass: attributeSchema
 attributeID: 1.3.6.1.4.1.7165.777.1.1.2
 lDAPDisplayName: endDate
 description: Role end date
 attributeSyntax: 2.5.5.5
 isSingleValued: TRUE

 dn: CN=roleName,CN=Schema,CN=Configuration,DC=samba,DC=example,DC=com
 objectClass: attributeSchema
 attributeID: 1.3.6.1.4.1.7165.777.1.1.3
 lDAPDisplayName: roleName
 description: Role name
 attributeSyntax: 2.5.5.12
 isSingleValued: TRUE

 dn: CN=role,CN=Schema,CN=Configuration,DC=samba,DC=example,DC=com
 objectClass: classSchema
 governsID: 1.3.6.1.4.1.7165.777.1.2.1
 lDAPDisplayName: role
 subClassOf: top
 objectClassCategory: 3
 description: A Role
 mayContain: startDate
 mayContain: endDate
 mayContain: roleName

Adding your schema
------------------------

* Ensure you do this first on a `Create_a_samba_lab-domain|lab-domain backup of your production domain`
* Create two LDIF files, attrs.ldif (with the attributes) and classes.ldif (the object classes) and copy to the schema master server (if you were working on a different server)
* Apply the ldifs with commands similar to:
 ldbadd -H path_to_sam_ldb attrs.ldif --option="dsdb:schema update allowed"=true
 ldbadd -H path_to_sam_ldb classes.ldif --option="dsdb:schema update allowed"=true

Test your new schema
------------------------

* Modify an object to have your new objectclass additionally listed
* Modify the same object to add the attribute.  Samba currently, incorrectly, requires that this be a distinct modification.

----
`Category:Active Directory`