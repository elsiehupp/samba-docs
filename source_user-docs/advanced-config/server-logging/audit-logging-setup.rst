..  <title>Setting up Audit Logging</title>
    <namespace>0</namespace>
    <last_edited>2021-04-15T23:35:16Z</last_edited>
    <last_editor>Abartlet</last_editor>

===============================

Introduction
===============================

Samba 4.7 and later supports logging of authentication and authorization events, and Samba 4.9 supported logging of AD DC database changes. This enables you to log, for example, failed authentication requests or password resets.

Audit logging is a local setting and you must enable this feature on each Samba server individually. Events are logged on the Samba server the event was performed on. To store all logs on a centralized server, set up a centralized syslog server, configure Samba to log to the syslog daemon, and configure the syslog daemon to send the logs to the centralized server. For details, see:
* The ``logging``, ``syslog``, and ``syslog only`` parameter descriptions in the ``smb.conf(5)`` man page
* The documentation of your syslog daemon

Additionally, you can use utilities, such as [https://www.fail2ban.org Fail2Ban], to monitor the log files and disable a user account on successive failed log ins. For details about setting these utilities, see their corresponding documentation.

* 
| type = note
| text = Samba will emit some logging in the file server and domain member configuration, but support is comprehensive **only in the AD DC**.

The Difference Between Authentication and Authorization 
------------------------

;Authentication
* EAuthentication happens when Samba verifies a user name and password combination.

; Authorization
* EAuthorization happens when the session starts. For example, when opening a distributed computing environment / remote procedure calls (DCE/RPC) pipe, returning from a session setup on server message block (SMB) protocol, or finishing the simple authentication and security layer (SASL) bind on LDAP.
* 
| type = note
| text = Samba supports logging of successful authorization events **but not** unsuccessful authorization events. It can log **both** successful and unsuccessful authentication events.

Authentication and Authorization Examples 
------------------------

The following examples explain when Samba logs authentication and when authorization events. Note that this is not a complete list.

* The Kerberos key distribution center (KDC) on an Active Directory (AD) domain controller (DC) logs an <u>authentication</u> event when a user logs into the domain.
* EIn an multi-domain controller (DC) environment, an authentication request is only logged on the DC the request was sent to. 
* If you connect to a share on a domain member:
* The domain member logs an <u>authorization</u> event.
* The KDC on the Samba DC logs an <u>authentication</u> event when using Kerberos authentication.
* :g Kerberos authentication, the KDC is responsible for authentication. Therefore, Samba on an AD domain member can not log the authentication event.
* The domain member logs an <u>authentication</u> event when using NT LAN Manager (NTLM) authentication.

.. note:

    When using NTLM, <u>authentication</u> and <u>authorization</u> events are always logged as a pair. However, when using Kerberos, one <u>authentication</u> event is logged on the DC when it issues the Ticket Granting Ticket (TGT), and each time you access a service, an <u>authorization</u> event is logged.

=================
Logging Formats
=================

The audit log feature supports the standard and JSON format. You can enable each format individually or both together using the different log debug classes.

Standard Log Format 
------------------------

Audit logging using the standard log format is automatically supported in Samba and does not require additional libraries.

The following example displays a log entry of an successful user authentication on a Samba DC using the standard log format:

 [2017/07/04 21:TT  4, pid=21757] ../auth/auth_log.c:848(log_successfu: nt_human_readable)
   Successful AuthZ: [SMB2,krb5] user [SAMDOM]\[Administrator] [S-1-5-21-469703510-2364959079-1506205053-500] at [Di, 04 Jul 2017 21:07:41DDO:SP:T] Remote host [ipv4:10.99.0.81: local host:[ipv4:10.99.0.1:445]::

Logging in JSON Format 
------------------------

If the ``jansson`` library was installed on the system where you build Samba, JSON support is automatically built into Samba.

The following example displays a log entry of an successful user authentication on a Samba DC using the JSON format:

 [2017/07/04 21:TT  4, pid=21757] ../auth/auth_log.c:220(log_json):
   JSON Authorization: {"type": :thorization", "timestamp": "2:H07-04T21:07:41.410408+0200&quo:CE:horization": {"version": {&q:quot;: 1, &q:quot;: 0}, :": "S-1DDAASS:H21-469703510-2364959079:06205053-500", "serviceDescription": "SMB2", "localAddress": "ipv4:10DDOO:0.1:445", "remoteAddress": :ipv4:10.99DDOOT::58828", &:portProtection": "SMB&quot: E"authType&quo "krb5", :;domain": "SAMDOM", :unt": "Administrator", :nServer": "DC1", &quot:ags": "0x00000210"}}:::

Verifying if JSON Support is Enabled in Samba
------------------------

To verify if Samba has been build with JSON support, enter:

 # smbd -b | grep HAVE_JSON_OBJECT
 HAVE_JSON_OBJECT

If no output is displayed, Samba was compiled without JSON support. To build Samba with JSON support, install the ``jansson`` header files and rebuild Samba. For details, see:
* `Package Dependencies Required to Build Samba`
* `Build Samba from Source`

=================
The Audit Logging Debug Classes
=================

Depending on the log level, Samba logs different events. However, to limit the amount of log entries, you can only increase the log level for the audit-related debug classes. For details, see `Configuring_Logging_on_a_Samba_Server#Setting_Individual_Log_Levels_for_Debug_Classes|Setting Individual Log Levels for Debug Classes`.

You can use the following debug classes to control the audit log level independently from the default log level:
* ``auth_audit``: Standard log format
* ``auth_json_audit`` JSON format. This feature requires built-in JSON support in Samba. See `#Verifying_if_JSON_Support_is_Enabled_in_Samba|Verifying if JSON Support is Enabled in Samba`.

=================
Enabling Authentication Audit Logging
=================

To set the default log level to ``1`` and enabling logging of failed and successful authentication requests (``3``):

* Set in the ``[global]`` section in the ``smb.conf`` file:

 log level = 1 auth_audit: Eauth_json_audit:3:

 further details, see the ``log level`` parameter description in the [https://www.samba.org/samba/docs/current/man-html/smb.conf.5.html#LOGLEVEL ``smb.conf (5)`` man page].

* 
| type = note
| text = A higher log level includes logging of events from lower levels.

* Restart Samba.

=================
Enabling AD DC Database Audit Logging
=================

To set the default log level to ``1`` and enabling logging of database changes as JSON:

* Set in the ``[global]`` section in the ``smb.conf`` file:

 log level = 1 dsdb_json_audit: Edsdb_password_json_audit:5 :oup_json_audit:5 dsdb_t:_json_audit:5:

 further details, see the ``log level`` parameter description in the [https://www.samba.org/samba/docs/current/man-html/smb.conf.5.html#LOGLEVEL ``smb.conf (5)`` man page].

* 
| type = note
| text = A higher log level includes logging of events from lower levels.

* Restart Samba.

----
`Category Directory`
`Category Members‏‎`
`Category Domains‏‎`
`Category:  Server‏‎`