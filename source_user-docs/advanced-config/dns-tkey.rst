Dns tkey negotiategss: TKEY is unacceptable
    <namespace>0</namespace>
<last_edited>2020-12-28T11:31:16Z</last_edited>
<last_editor>Hortimech</last_editor>

===============================

Introduction
===============================

On a Samba Active Directory (AD) domain controller (DC) using the ``BIND9_DLZ`` DNS back end, dynamic DNS updates can with the ``dns_tkey_negotiategss: TKEY is unacceptable`` error. For example:

 # samba_dnsupdate --verbose
 ...
 **dns_tkey_negotiategss: TKEY is unacceptable**
 Failed nsupdate: 1
 Calling nsupdate for SRV _gc._tcp.default-first-site-name._sites.samdom.example.com dc1.samdom.example.com 3268
 Outgoing update query:
 ;; ->>HEADER<<- opcode: UPDATE, status: NOERROR, id:      0
 ;; flags:; ZONE: 0, PREREQ: 0, UPDATE: 0, ADDITIONAL: 0
 ;; UPDATE SECTION:
 _gc._tcp.default-first-site-name._sites.samdom.example.com. 900 IN SRV 0 100 3268 dc1.samdom.example.com.

 **dns_tkey_negotiategss: TKEY is unacceptable**
 Failed nsupdate: 1
 Failed update of 20 entries

This documentation describes how to debug and fix fix the problem.

=================
Verifying the dns.keytab File Content
=================

.. note:

    The binddns dir was changed at Samba 4.8.0 from ``/usr/local/samba/private/dns`` to ``/usr/local/samba/bind-dns/dns``. Ensure you use the correct path in the following commands.

List the keys in the ``/usr/local/samba/private/dns.keytab`` file. The keytab file must list multiple Kerberos principals for the host. For example:

 # klist -k /usr/local/samba/private/dns.keytab
 Keytab name: FILE:dns.keytab
 KVNO Principal
 ---- ------------------------

------------------------

------------------------

--
    1 DNS/dc1.samdom.example.com@SAMDOM.EXAMPLE.COM
    1 dns-DC1@SAMDOM.EXAMPLE.COM
    ...

If no Kerberos principals are listed:

* Remove the keytab file:

 # rm /usr/local/samba/private/dns.keytab

* Remove the account, BIND uses on this domain controller (DC), to connect to the Active Directory (AD). For example, to delete the account for the ``DC1`` domain controller, enter:

 # samba-tool user delete dns-DC1

* Recreate the keytab file file and the AD account for the DC. For details, see `#Verifying_That_the_BIND_AD_Account_Exists_for_the_DC|Verifying That the BIND AD Account Exists for the DC`.

=================
Verifying That the BIND AD Account Exists for the DC
=================

For each Samba domain controller (DC) using the ``BIND9_DLZ`` DNS back end, a dedicated account must exist, to enable BIND to access the AD.

For example, to verify if the account exists in AD for the ``DC1`` domain controller, enter:

 # ldbsearch -H /usr/local/samba/private/sam.ldb 'cn=dns-DC1' dn
 # record 1
 dn: CN=dns-DC1,CN=Users,DC=samdom,DC=example,DC=com

If the account is not found:

* Reset the DNS settings:
 # samba_upgradedns --dns-backend=BIND9_DLZ
 Reading domain information
 DNS accounts already exist
 No zone file /usr/local/samba/private/dns/SAMDOM.EXAMPLE.COM.zone
 DNS records will be automatically created	
 DNS partitions already exist
 **Adding dns-DC1 account**
 See /usr/local/samba/private/named.conf for an example configuration include file for BIND
 and /usr/local/samba/private/named.txt for further documentation required for secure DNS updates
 Finished upgrading DNS

.. note:

    If the account creation failed, change temporarily the DNS back end. For details, see `#Temporarily_Changing_the_DNS_Back_End|Temporarily Changing the DNS Back End`.<br />
    https://bugzilla.samba.org/show_bug.cgi?id=10882 Bug #10882]

* Verify that the correct ``BIND9_DLZ`` module for your BIND version is enabled. For details, see `BIND9_DLZ_DNS_Back_End#Configuring_the_BIND9_DLZ_Module|Configuring the BIND9_DLZ Module`.

* Restart BIND.

=================
Temporarily Changing the DNS Back End
=================

If recreating the BIND Active Directory (AD) account for the domain controller (DC) failed, temporarily change the DNS back end:

* Change the DNS back end to ``SAMBA_INTERNAL``:

 # samba_upgradedns --dns-backend=SAMBA_INTERNAL
 Reading domain information
 DNS accounts already exist
 No zone file /usr/local/samba/private/dns/SAMDOM.EXAMPLE.COM.zone
 DNS records will be automatically created
 DNS partitions already exist
 Finished upgrading DNS

* Change the DNS back end back to ``BIND9_DLZ``:

 # samba_upgradedns --dns-backend=BIND9_DLZ
 Reading domain information
 DNS accounts already exist
 No zone file /usr/local/samba/private/dns/SAMDOM.EXAMPLE.COM.zone
 DNS records will be automatically created
 DNS partitions already exist
 Adding dns-DC1 account
 See /usr/local/samba/private/named.conf for an example configuration include file for BIND
 and /usr/local/samba/private/named.txt for further documentation required for secure DNS updates
 Finished upgrading DNS

* Restart BIND.

=================
Verifying File Permissions
=================

* Verify that BIND is able to read the Kerberos keytab and ``/etc/krb5.conf`` file. For details, see `BIND9_DLZ_DNS_Back_End#Setting_up_Dynamic_DNS_Updates_Using_Kerberos|Setting up Dynamic DNS Updates Using Kerberos`.

* BIND must be able to write to the temporary directory. For details, which temporary folder your BIND installation uses, see the BIND and your operating system's documentation

=================
Testing Dynamic DNS Updates
=================

For details, see `Testing_Dynamic_DNS_Updates|Testing Dynamic DNS Updates`.

----
`Category:Active Directory`
`Category:DNS`