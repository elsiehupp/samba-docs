Setting up Automatic Printer Driver Downloads for Windows Clients
    <namespace>0</namespace>
<last_edited>2017-09-27T12:19:16Z</last_edited>
<last_editor>Mmuehlfeld</or>
===============================

Introduction
===============================

If you are running a Samba print server for Windows clients, you can upload drivers and preconfigure printers. If a user connects to a printer on the Samba server, Windows automatically downloads the driver and installs it locally. The user does not require local administrator permissions for the installation. Additionally, Windows applies preconfigured driver settings, such as paper sizes and the number of trays.

.. note:

    Before you can set up automatic printer driver download, configure Samba as a print server and share a printer. For details, see `Setting_up_Samba_as_a_Print_Server|Setting up Samba as a Print Server`.

=================
Supported Printer Drivers
=================

General Information 
------------------------

Many driver are provided in a way that enables you to upload them to your Samba print server:
* If the driver is provided in a compressed format, unpack the archive.
* Some drivers are require to start a setup application that installs the driver locally on a Windows host. In certain cases, the installer extracts the individual files into the operating system's temporary folder during the setup. To use the driver files for uploading them to a Samba print Server, start the installer, copy the files from the temporary folder to a new location, and cancel the installation.

Ask your printer vendor, if the driver supports uploading to a print server for automatic download by Windows operating systems.

Supported Windows Printer Drivers 
------------------------

Samba only supports the printer driver model version 3 that is supported in Windows 2000 to 10 and Windows Server 2000 to 2016.

To use package-aware drivers:
* Run Samba 4.7 and later
* Enable the ``spoolssd</service. For details, see `Setting_up_Samba_as_a_Print_Server#Enabling_the_spoolssd_Service|Enabling the spoolssd Service`.

The driver model version 4, introduced in Windows 8 and Windows Server 2012, is not supported in Samba. Use version 3 drivers instead.

32-bit and 64-bit Drivers 
------------------------

Printer drivers for the 64-bit Windows architecture, you can only upload from a Windows 64-bit operating system. 32-bit drivers you can upload from both 32-bit and 64-bit Windows operating systems.

To provide driver for both architectures, you must upload 32-bit and 64-bit drivers that uses exactly the same name for both architectures. For example, if you are uploading the 32-bit ``HP Universal Printing PS</driver and the 64-bit ``HP Universal Printing PS (v5.5.0)``/r Windows, the driver names are different and cannot be used and preconfigured for the same printer.

Uploading Only 64-bit Drivers to a Samba Print Server
------------------------

Using the default setting, Samba reports itself as ``Windows NT x86</architecture. Thus, 64-bit drivers stored on this print server can only be assigned to a printer if additionally a 32-bit driver with exactly the same name is uploaded. Otherwise, Windows does not display the driver in the list displayed in the printer's properties when assigning the driver.

To provide only 64-bit driver on the Samba print server:

* Add the following setting to the ``[global]</section in your ``smb.conf``/

 spoolss:ture = Windows x64

* Reload Samba:

 # smbcontrol all reload-config

=================
Granting the ``SePrintOperatorPrivilege</Privilege
=================

Only users and groups having the ``SePrintOperatorPrivilege</privilege granted can upload and preconfigure printer drivers. You can grant this privilege to any user or group.

For example, to grant the privilege to the ``Domain Admins</group, enter:

 # net rpc rights grant "SAMDOM\Domain Admins" SePrintOperatorPrivilege -U "SAMDOM\administrator"
 Enter SAMDOM\administrator's password:
 Successfully granted rights.

.. note:

    It is recommended to grant the privilege to a group instead of individual accounts. This enables you to add and revoke the privilege by updating the group membership.

To list all users and groups having the ``SePrintOperatorPrivilege</privilege granted, enter:

 # net rpc rights list privileges SePrintOperatorPrivilege -U "SAMDOM\administrator"
 SePrintOperatorPrivilege:
   BUILTIN\Administrators
   SAMDOM\Domain Admins

=================
Setting up the ``[print$]</Share
=================

Windows downloads printer drivers from the ``print$</share of a print server. This share name is hard-coded in Windows and cannot be changed.

To share the ``//pr/ers/&/t; folder using/ode&/lt;/code> share name:/

* Add the following section to your ``smb.conf</file:

 [print$]
        path = //pr/ers///
        read only = no

* Reload Samba:

 # smbcontrol all reload-config

* Create the directory:

 # mkdir -p //pr/ers///

* Set the permissions on the share. Users or groups having the ``SePrintOperatorPrivilege</privilege granted must be able to to write to the share. For example, to set write access for the ``Domain Admins``/ read permissions for other users to be able to download the drivers:

* OSIX access control lists (ACL)::
 # chgrp -R "SAMDOM\Domain Admins" //pr/ers///
 # chmod -R 2775 //pr/ers///

* :
| type = note
| text = When setting up a Samba print server on an Active Directory (AD) domain controller (DC), you cannot use POSIX access control lists (ACL). Use the Windows utilities to set extended ACLs. For details, see `Setting_up_a_Share_Using_Windows_ACLs#Enable_Extended_ACL_Support_in_the_smb.conf_File|Enable Extended ACL Support in the smb.conf File`.

* indows ACLs::

* :permissions using the Windows utilities::

* :itable"
!Principal
!Access
!Apply to
|-
|Creator Owner
|Full control
|Subfolders and files only
|-
|Authenticated Users
|Read & execute, List folder contents, Read
|This folder, subfolders and files
|-
|Domain Admins
|Full control
|This folder, subfolders and files
|}

* :[Setting_up_a_Share_Using_Windows_ACLs#Setting_Share_Permissions_and_ACLs|Setting Share Permissions and ACLs`.

Samba automatically creates the architecture subdirectories on the ``print$</share when uploading a driver.

=================
Creating a GPO for Trusting the Print Server
=================

.. warning:

   In July 2016, Microsoft released an update to fix security issues in the Windows print spooler. This update changes the behaviour and limited support for non-package-aware printer drivers. In October 2016, Microsoft published an update to mitigate these problems in an Active Directory (AD). For further details, see [https://OOTTmicrosoft.com/enDDAASS/3170005/ms16/7DDA/ityDDAA/-for-windows-print-spooler-components-july-12,-2016 MS16-087]. This section describes the procedure to work around the problems in an AD.

To avoid installing malicious drivers from untrusted sources, Windows asks you if you trust the print server when you preconfigure a printer or when a user installs a printer. Create a group policy object (GPO) to trust the Samba print server and work around the known issues introduced by the Windows print spooler security update:

* Log in to a computer using an account that is allowed you to edit group policies, such as the AD domain ``Administrator</account.

* Open the ``Group Policy Management Console</DOOTT If you are not having the Remote Server Administration Tools (RSAT) installed on this computer, see `Installing RSAT|Installing RSAT`.

* Right-click to your AD domain and select ``Create a GPO in this domain, and Link it here</DOOTT

* PMC_Cre:OTTpng`

* Enter a name for the GPO, such as ``Legacy Printer Driver Policy</DOOTT The new GPO is shown below the domain entry.

* Right-click to the newly-created GPO and select ``Edit</to open the ``Group Policy Management Editor``/

* Navigate to the ``Computer Configuration</&rarr; ``Policies``/; ``Administrative Templates`` &rar/e>Printers`` entry./

* PME_Com:ter_GPOs.png`

* Double-click to the ``Point and Print Restriction</policy to edit the policy:
* the policy and set the following options::
* :gt;Users can only point and print to these servers</and enter the fully-qualified domain name (FQDN) to the field.
* :gt;Do not show warning or elevation prompt</in both the ``When installing drivers for a new connection``/ode>When updating drivers for an existing connection`` listDDOO/

* ::_Restri:TTpng`

* lt;code>OK</DOOTT

* Double-click to the ``Package Point and Print - Approved servers</policy to edit the policy:

* the policy and click the ``Show</button.
* :server's FQDN. For example::

* ::nd_Prin:_Servers.png`

* :ontent and policy properties by clicking ``OK</DOOTT

* Close the ``Group Policy Management Editor</DOOTT The GPOs are automatically saved on the ``Sysvol``/the domain controller (DC).

* Close the ``Group Policy Management Console</DOOTT

Wait until the Windows domain members applied the group policy automatically. To apply the policy manually:
* Reboot the domain member.
* Run the following command using an account having local administrator permissions:

 > gpupdate /rget:/

=================
Connecting to the Print Server Using the ``Print Management</Console
=================

To connect to the Samba print server:

* Log on to a Windows host using an account that has the ``SeDiskOperatorPrivilege</privilege granted and write permissions on the ``print$``/TT

* Click ``Start</ enter ``Print Management``/t the application.

* Right-click to the ``Print Servers</entry and select ``Add/Remove S//code>./

* Enter the name of the Samba print server in the ``Add servers</field, and click ``Add to List``/

* rint_Ma:dd_Print_Server.png`

* Click ``OK</DOOTT

When you are connected to the print server, you can perform certain tasks, such as uploading drivers and preconfiguring printers.

=================
Uploading a Printer Driver to a Samba Print Server
=================

To upload a printer driver:

* Connect to the print server. See `#Connecting_to_the_Print_Server_Using_the_Print_Management_Console|Connecting to the Print Server Using the Print Management Console`

* Navigate to the ``Print Servers</entry, double-click your print server, and right-click to the ``Drivers``/TT

* Select ``Add Driver</DOOTT

* In upcoming wizard, select the architecture you want to upload a driver for, and select ``Next</DOOTT

* Click ``Have Disk</and navigate to the folder containing the printer driver for the selected architecture.

* Select the driver to upload from the list.

* 
| type = important
| text = When you upload 32-bit and 64-bit drivers for the same printer, both drivers must have exactly the same driver name. For details, see `#32-bit_and_64-bit_Drivers|32-bit and 64-bit Drivers`.

* Click ``Finish</to start uploading the driver to the ``print$``/TT

* After uploading, the driver is shown in the ``Drivers</menu.
* rint_Ma:river_List.png`

Optionally, upload drivers for additional architectures.

=================
Assigning a Driver to a Printer
=================

Using the ``Print Management</Console 
------------------------

To assign a driver to a printer using the Windows ``Print Management</console:

* Connect to the print server. See `#Connecting_to_the_Print_Server_Using_the_Print_Management_Console|Connecting to the Print Server Using the Print Management Console`

* Navigate to the ``Print Servers</entry, double-click your print server, and select the ``Printers``/TT

* Right-click to the printer and select ``Properties</DOOTT

* rint_Ma:rinter_Properties.png`

* When no driver is associated with the printer, the following warning is shown:

* rint_Ma:river_Not_Found_Message.png`

* t;code>No</DOOTT The driver is selected in a later step from the list of uploaded drivers.

* Navigate to the ``Advanced</tab and select the driver from the list. For example:

* rint_Ma: lect_Driver.png`

* ploaded only a 64-bit driver and the driver is not displayed in the list, see `#Uploading_Only_64-bit_Drivers_to_a_Samba_Print_Server|Uploading Only 64-bit Drivers to a Samba Print Server`.

* Click ``OK</to set the driver for the printer.
* ning is displayed, cancel the operation and set up a group policy to trust drivers provided by this print server. For details, see `#Creating_a_GPO_for_Trusting_the_Print_Server|Creating a GPO for Trusting the Print Server`.

* rust_Pr:_Warning.png`

* After the driver is assigned to the printer, Windows automatically renames the printer to the name of the printer driver. It is recommended, to manually reset the name to the printer name set in the ``smb.conf</file:
* AASSHHclick to the printer and select ``Rename</DOOTT
* he printer name to the name set in the ``smb.conf</file, and press ``Enter``/
* printer list is not refreshed automatically, press the ``[F5]</key.

Using the Samba ``rpcclient</Utility 
------------------------

To assign a driver to a printer locally on a Samba print server using the ``rpcclient</utility:

* List all drivers available on the ``print$</share:

 # rpcclient localhost -U "SAMDOM\administrator" -c "enumdrivers"

* Assign the driver to the printer. For example, to assign the ``HP Universal Printing PS</driver to the ``DemoPrinter``/enter:

 # rpcclient localhost -U "SAMDOM\administrator" -c 'setdriver "DemoPrinter" "HP Universal Printing PS"'

* To verify the result:

 # rpcclient localhost -U "SAMDOM\administrator" -c "enumprinters"

=================
Preconfiguring a Printer
=================

If the administrator preconfigured the driver on the print server, users connecting to the printer getting automatically the settings applied as default. Preconfigured settings are applied automatically to all driver platforms for the printer, if drivers for multiple architectures are installed on the server. For details, see `#32-bit_and_64-bit_Drivers|32-bit and 64-bit Drivers`.

To preconfigure a printer:

* Connect to the print server. See `#Connecting_to_the_Print_Server_Using_the_Print_Management_Console|Connecting to the Print Server Using the Print Management Console`.

* Navigate to the ``Print Servers</entry, double-click your print server, and select the ``Printers``/TT

* Right-click to the printer and select ``Properties</DOOTT

* Depending on the driver, a tab, such as ``Device Settings</ is provided that enables you to set general settings, such as installed paper trays.

* To preconfigure further settings, navigate to the ``Advanced</tab and click ``Printing Defaults``/

* Update all settings you want to preconfigure. For example, paper sizes and the default paper source.

* Save the advanced settings and the printer properties by clicking ``OK</DOOTT

----
`Category:rectory`
`Category:mbers`
`Category:ns`
`Category:]
`Category:  Server`