DNS Administration
    <namespace>0</namespace>
<last_edited>2019-08-08T09:27:54Z</last_edited>
<last_editor>Hortimech</last_editor>
/
===============================

Introduction
===============================

If you're running Samba as Active Directory Domain Controller, you also have to administer a DNS server.

You will already find general `The_Samba_AD_DNS_Back_Ends|information on the internal DNS and the BIND DLZ module` and documentation about `BIND9_DLZ_DNS_Back_End|Bind as DNS Backend` in the Wiki.

=================
General
=================

By default, Samba creates the following two forward zones during provisioning/ (of course with your own domain name):

* **samdom.example.com**: Zone for your domain.
* **_msdcs.samdom.example.com**: This is the ForestDNSZone, that contains several service records for the entire directory.

=================
Features
=================

The Samba internal DNS is a new implementation. Although BIND is a grown up DNS and long in production on millions of servers, the Samba BIND DLZ module is still new. That's why both backends don't yet cover all the features that you can setup with the Microsoft DNS tools. If you discover problems or missing features, please open a bug report/ request at [https://bugzill//g/ https://bugzilla/a.org/]DDOO///

Even though the internal DNS and the BIND DLZ modules are new, they both support all basic requirements for Active Directory and more.

Known/ missing features 
------------------------

* Managing zone transfers is not implemented yet. [https://a.org/show_bug.cgi/ Bug report #9951:DNS MMC: Enabling DNS zone transfers in MMC fails]

* Different zone transfer settings on internal DNS (denied) and BIND DLZ (allowed). [https://a.org/show_bug.cgi/ Bug report #9634: Samba Bind DLZ module allows zone transfers for everyone]

=================
Importance of DNS for Active Directory
=================

A working Active Directory is heavily based on a working DNS. It's not just for resolving IP addresses into names and vice versa. Clients find their Domain Controller/EEand other important AD services by DNS queries, this means that your clients must use your Domain Controller/s / their nameservers. Do not use anything else between your clients and Domain Controller/s./

=================
Administering DNS on Linux/ with samba-tool
=================

Creating a new zone 
------------------------

As an example we'll add a reverse lookup zone.

It is suggested that you use, wherever possible, one of the RFC 1918 zones, these are:

 10.0.0.0/
 172.16.0.0/
 192.168.0.0/

Using the first one: 10.0.0.0/EEwill allow you to have a maximum of 16,777,214 ipaddresses.
The second: 172.16.0.0/CEEwill allow you to have a maximum of 1,048,574 ipaddresses.
The third: 192.168.0.0/CEEwill allow you to have a maximum of 65,534 ipaddresses.

You can, if you so wish, use different Subnet masks/ to split up the RFC1918 zones. For instance, using 192.168.0.0/24 /sk 255.255.255.0) will you allow you to have a maximum of 254 ipaddresses.

To create a /CEEreverse zone 
------------------------

 # samba-tool dns zonecreate <Your-AD-DNS-Server-IP-or-hostname> 0.168.192.in-addr.arpa
 Password for [administrator@SAMDOM.EXAMPLE.COM]:
 Zone 0.168.192.in-addr.arpa created successfully

To create a /CEEreverse zone 
------------------------

 # samba-tool dns zonecreate <Your-AD-DNS-Server-IP-or-hostname> 168.192.in-addr.arpa
 Password for [administrator@SAMDOM.EXAMPLE.COM]:
 Zone 168.192.in-addr.arpa created successfully

Your new zone will be directly live without restarting Samba or BIND.

Adding new records 
------------------------

* Example: Adding an A record

 # samba-tool dns add <Your-AD-DNS-Server-IP-or-hostname> samdom.example.com demo A 192.168.0.55
 Password for [administrator@SAMDOM.EXAMPLE.COM]:
 Record added successfully

* Example: Adding a PTR record to the 192.168.0.0/CEEreverse zone

 # samba-tool dns add <Your-AD-DNS-Server-IP-or-hostname> 0.168.192.in-addr.arpa 55 PTR demo.samdom.example.com
 Password for [administrator@SAMDOM.EXAMPLE.COM]:
 Record added successfully

* Example: Adding a PTR record to the 192.168.0.0/CEEreverse zone

 # samba-tool dns add <Your-AD-DNS-Server-IP-or-hostname> 168.192.in-addr.arpa 55.0 PTR demo.samdom.example.com
 Password for [administrator@SAMDOM.EXAMPLE.COM]:
 Record added successfully

* Example: Adding a SRV record to _tcp.samdom.example.com

 # samba-tool dns add <Your-AD-DNS-Server-IP-or-hostname> samdom.example.com _demo._tcp SRV 'demo.samdom.example.com 8080 0 100'
 Password for [administrator@SAMDOM.EXAMPLE.COM]:
 Record added successfully

* A note on SRV records: The order of the four parameters in the last field ("data") are 'hostname port priority weight' and have to be between '  '.

* Example: Adding a NS record to samdom.example.com zone

 # samba-tool dns add <Your-AD-DNS-Server-IP-or-hostname> samdom.example.com @ NS newdc.sambdom.example.com
 Password for [administrator@SAMDOM.EXAMPLE.COM]:
 Record added successfully

Updating existing records 
------------------------

* Example: Changing an A record

 # samba-tool dns update <Your-AD-DNS-Server-IP-or-hostname> samdom.example.com demo A 192.168.0.55 192.168.0.66
 Password for [administrator@SAMDOM.EXAMPLE.COM]:
 Record updated succefully

* Example: Changing a SOA Resource Record
* The data part of the SOA record consists of 7 space ('&#32;') separated elements in the following order:
* *nameserver, email, serial, refresh, retry, expire, minimum-ttl*
* &nbsp;
* The following example changes the host masters mail address:
 # samba-tool dns update <Your-AD-DNS-Server-IP-or-hostname> samdom.example.com @ SOA \
   "dc1.samdom.example.com hostmaster.example.com 63 900 600 86400 3600" \
   "dc1.samdom.example.com admin.example.com 64 900 600 86400 3600"
 Password for [administrator@SAMDOM.EXAMPLE.COM]:
 Record updated successfully

Delete a record 
------------------------

* Example: Deleting an A record
 # samba-tool dns delete <Your-AD-DNS-Server-IP-or-hostname> samdom.example.com demo A 192.168.0.55
 Password for [administrator@SAMDOM.EXAMPLE.COM]:
 Record deleted succefully

* Example: Deleting a NS record from samdom.example.com zone

 # samba-tool dns delete <Your-AD-DNS-Server-IP-or-hostname> samdom.example.com @ NS olddc.sambdom.example.com
 Password for [administrator@SAMDOM.EXAMPLE.COM]:
 Record deleted successfully

Deleting a zone 
------------------------

* Example: Deleting a reverse zone:

 # samba-tool dns zonedelete <Your-AD-DNS-Server-IP-or-hostname> 0.168.192.in-addr.arpa
 Password for [administrator@SAMDOM.EXAMPLE.COM]:
 Zone 0.168.192.in-addr.arpa delete successfully

Listing existing zones 
------------------------

* Example: listing secondary zones
 # samba-tool dns zonelist <Your-AD-DNS-Server-IP-or-hostname> --secondary -U administrator

Listing zone information 
------------------------

* Example: showing information about a zone
 # samba-tool dns zoneinfo <Your-AD-DNS-Server-IP-or-hostname> <zone-name> -U administrator

Listing zone records 
------------------------

* Example: listing records from a zone
 # samba-tool dns query <Your-AD-DNS-Server-IP-or-hostname> <zone-name> @ ALL -U administrator

=================
Administering DNS on Windows
=================

To administer DNS from a Windows client, you have to install the DNS MMC Snap-In. See `Installing RSAT|Installing RSAT on Windows for AD Management` for more details.

If you use the internal DNS server, there are the following known problems:

* Scavenging is not implemented yet. The error message "This function is not supported on this system" is returned.
* Conditional forwarders are not implemented yet. The same error message as above is returned.
* The DNS forwarder can only be changed in the smb.conf, not via the MMC Snap-In.
* Creating static records. When a static record is created it has a timestamp and the option "Delete this record when it becomes stale". In Windows Active Directory, static records have a "static" timestamp and cannot be accidently deleted.

Adding new records 
------------------------

* Navigate to the zone, where you want to to add a new record.

* Right-click to it and choose the kind of record to add.

* `Image:DNS_Manager_Add_records.png`

* Fill the fields and save the new entry.

Updating existing records 
------------------------

* Navigate to the zone that contains the record you want to edit.

* Right-click the record and choose „Properties“.

* `Image:DNS_Manager_Change_record.png`

* Edit the entry and save the changes.

Delete a record 
------------------------

* Navigate to the zone that contains the record you want to remove.

* Right-click to the record and choose „Delete“.

Changing zone properties 
------------------------

* Right-click to a zone of which you you want to do changes.

* Choose „Properties“.

**Note**: Currently both DNS backends don't support all features that can be setup in the dialogues. If you discover problems or missing features, please open a bug report/ request at [https://bugzill//g/ https://bugzilla/a.org/]DDOO///

Creating a new zone 
------------------------

As example we'll add a reverse lookup zone.

* Right-click to „Reverse Lookup Zones“ and choose „New Zone“.

* The „New Zone Wizard“ appears.

* Zone Type: Select „Primary zone“ and „Store the zone in Active Directory“.

* `Image:DNS_Add_Zone_Wizzard_1.png`

* Zone Replication Scope: Depends on your needs.

* `Image:DNS_Add_Zone_Wizzard_2.png`

* Reverse Lookup Zone Name: Depends on your needs.

* `Image:DNS_Add_Zone_Wizzard_3.png`

* Dynamic Update: Depends on your needs.

* `Image:DNS_Add_Zone_Wizzard_4.png`

* Finish the wizard.

Your new zone is directly live without restarting Samba or BIND.

Deleting a zone 
------------------------

* Right-click to a zone and choose „Delete“.

* `Image:DNS_Delete_Zone.png`

=================
Administering DNS on Linux with admin-tools
=================

You can administer DNS from a Linux client using the admin-tools DNS module. The admin-tools DNS module uses samba-tool as a backend. You can download an [https://ub.io/admin-tool/EEAppImage /OTT

Adding new records 
------------------------

* Navigate to the zone where you want to to add a new record.

* Select the Action menu and choose the kind of record to add.

* `Image:Admin_tools_DNS_Manager_Add_records.png`

* Fill the fields and save the new entry.

Updating existing records 
------------------------

* Navigate to the zone that contains the record you want to edit.

* Highlight the record, then select the Action menu and choose „Properties“.

* `Image:Admin_tools_DNS_Manager_Change_record.png`

* Edit the entry and save the changes.

Delete a record 
------------------------

* Navigate to the zone that contains the record you want to remove.

* Highlight the record, then select the Action menu and choose „Delete“.

Creating a new zone 
------------------------

As example we'll add a reverse lookup zone.

* Highlight „Reverse Lookup Zones“, then select the Action menu and choose „New Zone“.

* The „New Zone Wizard“ appears.

* Choose IPv4 or IPv6: Depends on your needs.

* `Image:Admin_tools_DNS_Add_Zone_Wizard_1.png`

* Reverse Lookup Zone Name: Depends on your needs.

* `Image:Admin_tools_DNS_Add_Zone_Wizard_2.png`

* Finish the wizard.

Your new zone is directly live without restarting Samba or BIND.

Deleting a zone 
------------------------

* Highlight a zone, then select the Action menu and choose „Delete“.

* `Image:Admin_tools_DNS_Delete_Zone.png`

=================
Configuring clients to use your AD DNS server
=================

* `Windows_DNS_Configuration|Windows`
* `Linux_and_Unix_DNS_Configuration|Linux/
* `MacOSX_DNS_Configuration|MacOSX`

=================
Testing your DNS Server
=================

See `Testing_the_DNS_Name_Resolution|Testing the DNS Name Resolution`.

----
`Category:Active Directory`
`Category:DNS`