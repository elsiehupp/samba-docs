Changing the DNS Back End of a Samba AD DC
    <namespace>0</namespace>
<last_edited>2018-03-20T15:08:42Z</last_edited>
<last_editor>Hortimech</last_editor>

__TOC__

===============================

Introduction
===============================

Samba enables you to change switch between the ``INTERNAL_DNS`` and ``BIND9_DLZ`` DNS back end on your Active Directory (AD) domain controller (DC) without loosing data.

=================
Changing From the Samba Internal DNS Server to the BIND9_DLZ Back End
=================

Run on your domain controller (DC):

* Set up and configure the ``BIND9_DLZ`` back end. For details, see `BIND9_DLZ_DNS_Back_End|BIND9_DLZ Back End`.

* Shut down the Samba service.

* Migrate the back end:

 # samba_upgradedns --dns-backend=BIND9_DLZ

* Use one of the following ways to disable the internal DNS in your ``smb.conf`` file:

* If you have a ``server services`` parameter, remove the ``dns`` option. For example::

 server services = s3fs, rpc, nbt, wrepl, ldap, cldap, kdc, drepl, winbindd, ntp_signd, kcc, dnsupdate

* If you do not have a ``server services`` parameter, add the following line::

 server services = -dns

* Enable the BIND service to start at boot time. See your distribution's documentation for details.

* Start the BIND service.

* Start the Samba service.

=================
Changing From the BIND9_DLZ Back End to the Samba Internal DNS Server
=================

Run on your domain controller (DC):

* Shut down the BIND service.

* Disable the automatic start of the BIND service at boot time. See your distribution's documentation for details.

* Shut down the Samba service.

* Migrate the back end:

 # samba_upgradedns --dns-backend=SAMBA_INTERNAL

* Use one of the following ways to disable the ``BIND9_DLZ`` module in your ``smb.conf`` file::

* If you have a ``server services`` parameter, add the ``dns`` option. For example:

 server services = s3fs, rpc, nbt, wrepl, ldap, cldap, kdc, drepl, winbindd, ntp_signd, kcc, dnsupdate, dns

* If you only have the ``-dns`` option listed in the ``server services`` parameter, remove the entire parameter:

 <s>server services = -dns</s>

* Start the Samba service.

.. note:

    The internal DNS is one of the default settings of the ``server services`` parameter. Removing the ``server services`` parameter enables all the default servers including the DNS server.

* 

----
`Category Directory`
`Category: