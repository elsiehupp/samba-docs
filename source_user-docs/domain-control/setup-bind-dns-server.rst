Setting up a BIND DNS Server
    <namespace>0</namespace>
<last_edited>2019-09-02T14:44:40Z</last_edited>
<last_editor>Hortimech</last_editor>

===============================

Introduction
===============================

If you are planning to set up a Samba Active Directory (AD) domain controller (DC) using the ``BIND9_DLZ`` back end, you have to install and configure the BIND DNS server first.

The following describes how to set up a basic BIND installation you can use as Samba AD DC back end.

.. warning:

   Samba AD is not compatible with other DNS servers, even if those that supports tkey-gss updates, because parts of Samba (like the DNS management RPC server and the domain join) assume the replicated DNS entries in the AD Database are the same as those exposed over DNS.  Likewise the security of the system depends on the ACLs on each DNS entry in AD.

.. note:

    If you are changing from the internal dns server to a Bind9 dns server, you will also need to read `Changing_the_DNS_Back_End_of_a_Samba_AD_DC|Changing_the_DNS_Back_End_of_a_Samba_AD_DC`.

=================
Installing BIND
=================

For a list of supported BIND versions, see `BIND9_DLZ_DNS_Back_End#Configuring_the_BIND9_DLZ_Module|Configuring the BIND9_DLZ module`.

.. note:

    You can not run BIND in a changed root environment (chroot), because the ``BIND9_DLZ`` must be able to access the Samba Active Directory (AD) database files directly.

Package Installation 
------------------------

Installing BIND using packages provided with your distribution is the recommended way. Select this installation mode for an easy installation and to automatically receive updates when available. For details how to install packages, see the distribution's package manager documentation.

Make sure that your package provider compiled BIND using the following options:
* ``--with-gssapi=yes`` or ``--with-gssapi=/usr`` (or similar) for secure dynamic DNS updates using Kerberos
* ``--with-dlopen`` dynamically loadable zones (DLZ)

.. note:

    Later versions of Bind9.9.x have the ``--with-dlopen`` option builtin and is not shown by the following command. This happened around Bind 9.9.4

To list the build options:
 # named -V
 BIND 9.x.y built with ... '--with-dlopen=yes' '--with-gssapi=yes' ...

Compiling and Installing BIND 
------------------------

Use this installation mode if you are an advanced user and know how to compile software.

Downloading the Sources
------------------------

Download a supported BIND version from https://www.isc.org/software/bind.

Compiling BIND
------------------------

* Pass at least the following parameters to the ``configure`` command:

 # ./configure --with-gssapi=/usr/include/gssapi --with-dlopen=yes

* Add further parameters, if required.

* To build and install, run:

 # make
 # make install

Adding a User and Group for BIND
------------------------

For security reasons, do not run BIND as ``root`` user.

To create a ``named`` group using GID ``25``:

 # groupadd -g 25 named

To create a ``named`` account with UID ``25``, primary group ``named``, home directory ``/var/named/``, and without a valid shell:

 # useradd -u 25 -g named -d /var/named -M -s /sbin/nologin named

For details, see the ``useradd (8)`` and ``groupadd (8)`` man page.

=================
Configuring BIND
=================

Setting up a named.conf file 
------------------------

To locate the directory, BIND uses to read the ``named.conf`` file:

 # named -V
 BIND 9.x.y built with ... '--sysconfdir=/etc' ...

In the previous example, BIND reads the configuration from the ``/etc/named.conf`` file.

The following is a basic configuration file:

 # Global Configuration Options
 options {

     auth-nxdomain yes;
     directory "/var/named";
     notify no;
     empty-zones-enable no;

     # IP addresses and network ranges allowed to query the DNS server:
     allow-query {
         127.0.0.1;
         10.99.1.0/24;
     };

     # IP addresses and network ranges allowed to run recursive queries:
     # (Zones not served by this DNS server)
     allow-recursion {
         127.0.0.1;
         10.1.1.0/24;
     };

     # Forward queries that can not be answered from own zones
     # to these DNS servers:
     forwarders {
         8.8.8.8;
         8.8.4.4;
     };

     # Disable zone transfers 
     allow-transfer {
         none;
     };
    ;

 # Root Servers
 # (Required for recursive DNS queries)
 zone "." {
    type hint;
    file "named.root";
 };

 # localhost zone
 zone "localhost" {
     type master;
     file "master/localhost.zone";
 };

 # 127.0.0. zone.
 zone "0.0.127.in-addr.arpa" {
     type master;
     file "master/0.0.127.zone";
 };

For details about the used parameters and options, see the ``named.conf (5)`` man page.

The previous example does not contain the ``BIND9_DLZ`` configuration required for setting up a Samba AD DNS server. For details how to set up the module, see `BIND9_DLZ_DNS_Back_End|BIND9_DLZ DNS Back End`.

.. warning:

   You must not add the AD domain forward or reverse zone records to the named.conf files, these zones are stored dynamically in AD.

Downloading the DNS Root Servers List 
------------------------

* Download the latest list of the DNS root servers to the ``/var/named/named.root`` file:

 # wget -q -O /var/named/named.root http://www.internic.net/zones/named.root

* Enable the BIND user to read the root servers list:

 # chown root:named /var/named/named.root
 # chmod 640 /var/named/named.root

.. note:

    Optionally, set up a Cron job to automatically update the file.

Creating the localhost Zone File 
------------------------

* Create the ``localhost`` forward zone in the ``/var/named/master/localhost.zone`` file:

 $TTL 3D

 $ORIGIN localhost.

 @       1D      IN     SOA     @       root (
                        2013050101      ; serial
                        8H              ; refresh
                        2H              ; retry
                        4W              ; expiry
                        1D              ; minimum
                        )

 @       IN      NS      @
         IN      A       127.0.0.1

* Enable the BIND user to read the zone file:
 # chown named:named /var/named/master/localhost.zone
 # chmod 640 /var/named/master/localhost.zone

Creating the 0.0.127.in-addr.arpa Zone File 
------------------------

* Create the ``0.0.127.in-addr.arpa`` reverse zone in the ``/var/named/master/0.0.127.zone`` file:

 $TTL 3D

 @       IN      SOA     localhost. root.localhost. (
                         2013050101      ; Serial
                         8H              ; Refresh
                         2H              ; Retry
                         4W              ; Expire
                         1D              ; Minimum TTL
                         )

        IN      NS      localhost.

 1      IN      PTR     localhost.

* Enable the BIND user to read the zone file:

 # chown named:named /var/named/master/0.0.127.zone
 # chmod 640 /var/named/master/0.0.127.zone

=================
Installing & Configuring BIND on Debian based distros
=================

Installing the required packages 
------------------------

You need to install the following packages:

 apt-get install -y bind9 bind9utils

.. note:

    Before continuing, you will need to provision a DC in a new domain or join as a DC to an existing domain or upgrade from the existing internal DNS server to BIND9_DLZ. Various required files will only be created by doing one of the preceeding actions.

Setting up the named.conf files 
------------------------

On Debian based distros, you have 5 Bind9 files:

 /etc/default/bind9 
 /etc/bind/named.conf
 /etc/bind/named.conf.options
 /etc/bind/named.conf.local
 /etc/bind/named.conf.default-zones

Of these, only two or three need to be configured.

If you only use ipv4, edit : ``/etc/default/bind9`` and Change:

 OPTIONS="-u bind"

To:

 OPTIONS="-u bind -4"

The second file ``/etc/bind/named.conf``, Shouldn't need modification, as it just contains links to the other ``named.conf`` files:

 include "/etc/bind/named.conf.options";
 include "/etc/bind/named.conf.local";
 include "/etc/bind/named.conf.default-zones";

The third file ``/etc/bind/named.conf.options``, is the one you need to configure for your Active Directory and to setup default ACL's for Bind9.

 // Managing acls
 acl internals { 127.0.0.0/8; 192.168.0.0/24; };

 options {
       directory "/var/cache/bind";
       version "Go Away 0.0.7";
       notify no;
       empty-zones-enable no;
       auth-nxdomain yes;
       forwarders { 8.8.8.8; 8.8.4.4; };
       allow-transfer { none; };

       dnssec-validation no;
       dnssec-enable no;
       dnssec-lookaside no;

       // If you only use IPv4. 
       listen-on-v6 { none; };
       // listen on these ipnumbers. 
       listen-on port 53 { 192.168.0.6; 127.0.0.1; ::1; };

       // Added Per Debian buster Bind9. 
       // Due to : resolver: info: resolver priming query complete messages in the logs. 
       // See: https://gitlab.isc.org/isc-projects/bind9/commit/4a827494618e776a78b413d863bc23badd14ea42
       minimal-responses yes;

       //  Add any subnets or hosts you want to allow to use this DNS server
       allow-query { "internals";  };
       allow-query-cache { "internals"; };

       //  Add any subnets or hosts you want to allow to use recursive queries
       recursion yes;
       allow-recursion {  "internals"; };

       // https://wiki.samba.org/index.php/Dns-backend_bind
       // DNS dynamic updates via Kerberos (optional, but recommended)
       // ONE of the following lines should be enabled AFTER you provision or join a DC with bind9_dlz 
       // or AFTER upgrading your dns from internal to bind9_dlz 
       // Before Samba 4.9.0
       // tkey-gssapi-keytab "/var/lib/samba/private/dns.keytab";
       // From Samba 4.9.0 ( You will need to run samba_dnsupgrade if upgrading your Samba version. ) 
       tkey-gssapi-keytab "/var/lib/samba/bind-dns/dns.keytab";

   };

The fourth file ``/etc/bind/named.conf.local``, just needs the addition of one line, to link in another file provided by Samba:

 include "/var/lib/samba/bind-dns/named.conf";

The last file ``/etc/bind/named.conf.default-zones`` does not require any changes.

=================
Starting the Daemon
=================

To start the BIND daemon as the ``named`` user, run

 # named -u named

Alternatively, use your operating system tools, such as ``systemctl`` or ``service``, to start the daemon. See your distribution's documentation for details.

.. note:

    Enable the daemon to start automatically when the system boots. For details, see your distribution's documentation.

.. note:

    You should ensure that you never ``reload`` Bind9, you should check your init files and change ``reload`` to ``restart``, you should also check ``/etc/logrotate.d/named`` etc.

=================
Testing Your Zones
=================

The following examples query the DNS service on the local machine (``127.0.0.1``):

To test the ``localhost`` forward zone:

 # host -t A localhost 127.0.0.1
 Using domain server:
 Name: 127.0.0.1
 Address: 127.0.0.1#53
 Aliases: 
 localhost has address 127.0.0.1

To test the ``0.0.127.in-addr.arpa`` reverse zone:

 # host -t PTR 127.0.0.1 127.0.0.1
 Using domain server:
 Name: 127.0.0.1
 Address: 127.0.0.1#53
 Aliases: 
 1.0.0.127.in-addr.arpa domain name pointer localhost.

=================
Configuring the BIND9_DLZ module
=================

For details, see See `BIND9_DLZ_DNS_Back_End|BIND9_DLZ DNS Back End`.

----
`Category:Active Directory`
`Category:DNS`