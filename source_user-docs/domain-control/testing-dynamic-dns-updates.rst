Testing Dynamic DNS Updates
    <namespace>0</namespace>
<last_edited>2017-02-26T20:35:52Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

To test the dynamic DNS updates, run as the ``root`` user on your Samba domain controller (DC):

 # samba_dnsupdate --verbose --all-names
 IPs: ['10.99.0.1']
 ...
 force update: A samdom.example.com 10.99.0.1
 ...
 21 DNS updates and 0 DNS deletes needed
 Successfully obtained Kerberos ticket to DNS/dc1.samdom.example.com as DC1$
 update(nsupdate): A samdom.example.com 10.99.0.1
 Calling nsupdate for A samdom.example.com 10.99.0.1 (add)
 Outgoing update query:
 ;; ->>HEADER<<- opcode: UPDATE, status: NOERROR, id:      0
 ;; flags:; ZONE: 0, PREREQ: 0, UPDATE: 0, ADDITIONAL: 0
 ;; UPDATE SECTION:
 samdom.example.com.	900	IN	A	10.99.0.1
 ...

This commands forces an update of all records specified in the ``/usr/local/samba/private/dns_update_list`` file.

The ``samba_dnsupdate`` utility updates the DNS. It automatically checks for missing DNS records specified in the ``dns_update_list`` file when the ``samba`` daemon starts and after every 10 minutes.

If dynamic DNS updates fail, see:

* ``BIND9_DLZ`` back end: `BIND9_DLZ_DNS_Back_End#Troubleshooting|BIND9_DLZ Troubleshooting`
* ``INTERNAL_DNS`` back end: `Samba_internal_DNS_Back_End#Troubleshooting|Samba Internal DNS Troubleshooting`

----
`Category:Active Directory`
`Category:DNS`