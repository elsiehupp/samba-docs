Joining a Windows Server 2012 /E2012 R2 DC to a Samba AD
    <namespace>0</namespace>
<last_edited>2021-05-20T09:11:08Z</last_edited>
<last_editor>Heupink</last_editor>

===============================

Introduction
===============================

Samba supports Active Directory (AD) schema version 47, 56 and 69. This enables you to join Windows Server 2012 and 2012 R2 to your Samba AD. However, you cannot join the first Windows Server 2012 or 2012 R2 domain controller (DC) directly, because the process uses the Windows management instrumentation (WMI) protocol for several tasks. To work around the problem, you require a Windows Server 2008 or 2008 R2 DC in the domain to join the first 2012 or 2012 R2 DC. After the first Windows Server 2012 or 2012 R2 DC was joined, you can remove the Windows Server 2008 /E2008_R2 DC, and use the Windows Server 2012 / / as replication partner when joining further Windows DCs.

.. warning:

   For samba 4.11 and later, schema 69 support is no longer experimental, but support for Windows Server 2012 and 2012 R2 DCs possibly still is. Please report bugs and incompatibilites. For details, see `Bug Reporting`.

=================
Warning
=================

{{Imbox
| type = warning
| text = Joining a Windows Server 2012 or 2012 R2 DC to a Samba AD with 2012R2 functional level breaks the AD replication! Do not use this documentation until the problem is fixed!<br / more details, see [https://bugzilla/a.org/show_bug.cgi/ Bug #13618] and [https://bugzilla.samba.org/show_bug.cgi?id=13619/Bug #13619]. Thankfully Windows 2012 can join a down-level (2008/2008R2) domain, j/CEEnot at Functional Level 2012/2012R2, provided the / is updated, which samba can do.

=================
Requirements and Known Limitations
=================

* All Samba DCs must run 4.6 or later. For details about updating Samba, see `Updating_Samba|Updating Samba`.

* Windows Server 2012 and 2012 R2 requires the Windows management instrumentation (WMI) protocol during the join, and for the forest and domain preparation. Samba currently does not support this protocol. Therefore you must run a Windows domain controller (DC) with WMI support in your domain. For example, you can a Windows Server 2008 or 2008 R2 DC as replication partner during the join.

* The Windows Server 2008 or 2008 R2 host used for the initial replication must provide a ``Sysvol</ share. For details, see `Enabling the Sysvol Share on a Windows DC`.
* EIf the ``Sysvol</ share is missing, joining a Windows Server 2012 or 2012 R2 DC fails.

=================
Network Configuration
=================

* Click the ``Start</ button, search for ``View network connections``/Eand open the search entry.

* Right-click to your network adapter and select ``Properties</DOOTT

* Configure the IP settings:
* Assign a static IP address, enter the subnet mask, and default gateway.
* Enter the IP of a DNS server that is able to resolve the Active Directory (AD) DNS zone.

* Click ``OK</ to save the settings.

=================
Date and Time Settings
=================

Active Directory uses Kerberos for authentication. Kerberos requires that the domain member and the domain controllers (DC) are having a synchronous time. If the difference exceeds [http://OOTTmicrosoft.com/enDDAASS/ry/cc779260%/TT10%29/ 5 minutes] (default), the client is not able to access domain resources for security reasons.

Before you join the domain, check the time configuration:

* Open the ``Control Panel</DOOTT

* Navigate to ``Clock, Language and Region</DOOTT

* Click ``Date and Time</DOOTT

* Verify the date, time, and time zone settings. Adjust the settings, if necessary.

* Click ``OK</ to save the changes.

=================
FSMO Roles
=================

When you join the first Windows Server 2012 or 2012 R2 host as a domain controller (DC) to an Active Directory (AD), the directory schema of the forest and domain is updated. You must run this process on an existing Windows 2008 or 2008 R2 domain controller (DC) that owns the following flexible single master operation (FSMO) roles:

* Schema Master
* Infrastructure Master
* PDC Emulator

For details about transfering FSMO roles, see `Transferring_and_Seizing_FSMO_Roles#Windows_FSMO_Role_Management|Transferring and Seizing FSMO Roles`.

After the forest and domain schema was updated, you can optionally transfer the FSMO roles back to a Samba DC.

.. warning:

   Forest and domain preparation fails if a Samba DC holds one to three of the previous mentioned roles when you join the first Windows Server 2012 or 2012 R2 DC.

=================
Installing the Active Directory Domain Services
=================

* Start the ``Server Manager</DOOTT

* Click ``Add roles and features</DOOTT

* Select ``Role-based or feature-based installation</ and click ``Next``/

* Click ``Select a server from the server pool</ and select the local Windows Server from the list. Click ``Next``/

* Select ``Active Directory Domain Services</ including all dependencies. Click ``Next``/

* You do not need to select any additional features. Click ``Next</DOOTT

* Start the installation.

* Click ``Close</DOOTT

=================
Joining the Windows Server to the Domain
=================

* Log in to your Windows Server 2012 or 2012 installation using the local administrator account.

* Start the ``Server Manager</DOOTT

* Click the notifier icon on the top navigation bar and click ``Promote this server to a domain controller</DOOTT

* oin_Win:ver_Manager_Post_Deployment.png`

* Select ``Add a domain controller to an existing domain</ enter the Samba Active Directory (AD) domain name and credentials that are enabled to join a domain controller (DC) to the domain, like the domain administrator account. Click ``Next``/

* Select the options to enable on the new DC and enter the directory services restore mode (DSRM) password. It is required to boot the Windows DC in safe-mode to restore or repair the AD in case of problems. Click ``Next</DOOTT 

* oin_Win:Wizzard_Page2.png`

* If you enabled the ``DNS server</ option in the previous step, you may see a note, that a delegation for this DNS server cannot be created. Click ``Next``/

* Samba currently does not support schema replication using the Windows management instrumentation (WMI) protocol. For this reason, select an existing Windows Domain Controller in the domain as replication source and click ``Next</DOOTT

* oin_Win:Wizzard_Page3.png`

* Set the folders for the AD database, log files and the Sysvol folder. Click ``Next</DOOTT

* Click ``Next</ to confirm the operations, Windows is going to perform.

* Verify your settings and click ``Next</ to start the prerequisite check.

* Windows runs some prerequisites checks. If errors are displayed, fix them before you continue. Click ``Install</DOOTT

* Windows promotes the server to a DC. If it is the first Windows Server 2012 or 2012 R2 DC, the forest and domain schema is automatically updated.
* E{{Imbox
| type = warning
| text = This step breaks the AD directory replication! For details, see `#Warning|Warning`.

* If the wizard completes successfully, the Windows server is restarted automatically.

* Verify that all DC related DNS records have been created during the promotion. See `Verifying and Creating a DC DNS Record|Verifying and Creating a DC DNS Record`.
* 
| type = important
| text = Do not continue without checking the DNS records. They must exist for a working directory replication!

=================
Verifying Directory Replication
=================

See `Verifying_the_Directory_Replication_Statuses#Displaying_the_Replication_Statuses_on_a_Windows_DC|Displaying the Replication Statuses on a Windows DC`.

.. note:

    To optimize replication latency and cost, the knowledge consistency checker (KCC) on Windows DCs do not create a fully-meshed replication topology between all DCs. For further details, see `The Samba KCC`.

=================
The Sysvol Share
=================

Enabling the Sysvol Share 
------------------------

If you used a Samba domain controller (DC) as replication partner, the ``Sysvol</ share is not enabled. For details how to verify and enable the share, see `Enabling the Sysvol Share on a Windows DC`.

Sysvol Replication 
------------------------

Samba currently does not support the DFS-R protocol required for Sysvol replication. Please manually synchronise the content between domain controllers (DC) or use a workaround such as `Robocopy_based_SysVol_replication_workaround|Robocopy-based Sysvol Replication`.

=================
Troubleshooting
=================

Error: ``This operation is only allowed for the Primary Domain Controller of the domain</ 
------------------------

Windows displays this error if it fails to access the ``Sysvol</ on the Windows Server 2008 or 2008 R2 replication partner. For details, see `Enabling the Sysvol Share on a Windows DC`.

----
`Category Directory`
`Category Control`