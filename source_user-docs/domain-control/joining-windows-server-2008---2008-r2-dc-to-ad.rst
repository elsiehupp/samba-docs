Joining a Windows Server 2008 / 2008 R2 DC to a Samba AD
    <namespace>0</namespace>
<last_edited>2017-05-18T14:07:30Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

===============================

Introduction
===============================

You can join Windows Server 2008 and 2008 R2 as an domain controller (DC) to a Samba Active Directory (AD).

If you want to join a computer running a Windows Server operating system as a domain member, see `Joining_a_Windows_Client_or_Server_to_a_Domain|Joining a Windows Client or Server to a Domain`.

=================
Network Configuration
=================

* Click the ``Start`` button, search for ``View network connections``, and open the search entry.

* Right-click to your network adapter and select ``Properties``.

* Configure the IP settings:
* * Assign a static IP address, enter the subnet mask, and default gateway.
* * Enter the IP of a DNS server that is able to resolve the Active Directory (AD) DNS zone.

* Click ``OK`` to save the settings.

=================
Date and Time Settings
=================

Active Directory uses Kerberos for authentication. Kerberos requires that the domain member and the domain controllers (DC) are having a synchronous time. If the difference exceeds [http://technet.microsoft.com/en-us/library/cc779260%28v=ws.10%29.aspx 5 minutes] (default), the client is not able to access domain resources for security reasons.

Before you join the domain, check the time configuration:

* Open the ``Control Panel``.

* Navigrate to ``Clock, Language and Region``.

* Click ``Date and Time``.

* Verify the date, time, and time zone settings. Adjust the settings, if necessary.

* Click ``OK`` to save the changes.

=================
Joining the Windows Server to the Domain
=================

* Select ``Start`` / ``Run``, enter ``dcpromo.exe`` and click ``OK``.

* Windows Server automatically installs missing features, if necessary:

* `Image:Join_Win2008R2_dcpromo_install.png`

* Check ``Use advanced mode installation`` to display additional options in later steps. Click ``OK``.

* Read the ``Operating System Compatibility`` information and click ``Next``.

* Select ``Existing forest`` / ``Add a domain controller to an existing domain``, and click ``Next``.

* Enter the Samba Active Directory (AD) domain name and credentials that are enabled to join a domain controller (DC) to the domain, such as the domain administrator account. Click ``Next``.

* Select the domain to join and click ``Next``.

* If AD sites are configured, select the site to join. Otherwise continue using the ``Default-First-Site-Name`` site. Click ``Next``.

* Select the options to enable on the new DC and click ``Next``.

* `Image:Join_Win2008R2_DC_Options.png`

* If you enabled the ``DNS server`` option in the previous step, you may see a note, that a delegation for this DNS server cannot be created. Click ``Yes`` to continue.

* `Image:Join_Win2008R2_DNS_Delegation_Failed.png`

* Select ``Replicate data over the network from an existing domain controller`` and click ``Next``.

* Select a DC as source for the initial directory replication or let the installation wizard choose an appropriate DC. Click ``Next``.

* Set the folders for the AD database, log files and the Sysvol folder. Click ``Next``.

* Set a Directory Service Restore Mode Administrator Password (DSRM). It is required to boot the Windows DC in safe-mode to restore or repair the AD. Click ``Next``.

* Verify your settings and click ``Next`` to start the DC promotion.

* The wizard starts the installation, replicates the directory, and so on.

* `Image:Join_Win2008R2_Join_Process.png`

* Verify that all DC related DNS records have been created during the promotion. See `Verifying and Creating a DC DNS Record|Verifying and Creating a DC DNS Record`.
* .. warning:

   Do not continue without verifying the DNS records. They must exist for a working directory replication!

* After the wizard completed click ``Finish``.

* Restart the computer.

The Windows server now acts as an AD DC.

=================
Verifying Directory Replication
=================

See `Verifying_the_Directory_Replication_Statuses#Displaying_the_Replication_Statuses_on_a_Windows_DC|Displaying the Replication Statuses on a Windows DC`.

.. note:

    To optimize replication latency and cost, the knowledge consistency checker (KCC) on Windows DCs do not create a fully-meshed replication topology between all DCs. For further details, see `The Samba KCC`.

=================
The Sysvol Share
=================

Enabling the Sysvol Share 
------------------------

If you used a Samba domain controller (DC) as replication partner, the ``Sysvol`` share is not enabled. For details how to verify and enable the share, see `Enabling the Sysvol Share on a Windows DC`.

Sysvol Replication 
------------------------

Samba currently does not support the DFS-R protocol required for Sysvol replication. Please manually synchronise the content between domain controllers (DC) or use a workaround such as `Robocopy_based_SysVol_replication_workaround|Robocopy-based Sysvol Replication`.

----
`Category:Active Directory`
`Category:Domain Control`