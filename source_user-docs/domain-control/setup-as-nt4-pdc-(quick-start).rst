Setting up Samba as an NT4 PDC (Quick Start)
    <namespace>0</namespace>
<last_edited>2017-09-06T13:31:14Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

===============================

Introduction
===============================

Samba officially supports in the latest version acting as an NT4 primary domain controller (PDC).

However, in case of new installations for a domain environment we recommend to set up Samba as an `Active_Directory_Domain_Controller|Active Directory (AD) domain controller (DC)`, because Microsoft officially stopped the support of Windows NT4 at the end of 2004. Even if later published Windows versions can still join an NT4 domain, any future security or enhancement update can break or disable this deprecated feature. For example, Windows 7 and later already require modifications on the client side and Windows 10 additional on the PDC to operate in an NT4 domain. For details, see `Required_Settings_for_Samba_NT4_Domains|Required Settings for Samba NT4 Domains`.

The following documentation is a minimal quick start guide to set up Samba as an NT4 PDC. You have to create user and machine accounts manually on the PDC which are stored within the Samba internal ``tdbsam`` database. You can later enhance this setup with additional features, such as OpenLDAP back end support.

=================
Preconditions
=================

The documentation assumes that:

* the operating system is installed.
* the network on the host is set up and working.
* a `Samba_Release_Planning|supported` Samba version is `Installing_Samba|installed`.

=================
 Set up the Primary Domain Controller
=================

To set up the Primary Domain Controller (PDC):

* To locate the path of the ``smb.conf`` file Samba uses, run:

 # smbd -b | grep "CONFIGFILE"
    CONFIGFILE:HHusr/local/samba/etc/smb.conf

* Use the following content in your ``smb.conf`` file:

 [global]
         netbios name = PDC
         workgroup = SAMDOM
         security = User

         passdb backend = tdbsam
         username map = /usr/local/samba/etc/username.map

         domain logons = Yes

         log file = /var/log/samba/%m
         log level = 1

* a minimum configuration, to set up an NT4 PDC using a local ``tdbsam`` database. For details about the parameters, see the ``smb.conf (5)`` man page.

* Optionally, to enable Windows 10 and Windows Server 2016 and later to connect to this Samba server, additionally set the following in the ``[global]`` section in your ``smb.conf`` file:
    erver max protocol = NT1
* ils, see `Required_Settings_for_Samba_NT4_Domains#Windows_10:_There_A:ly_No_Logon_Servers_Available_to_Service_the_Logon_Request|Required Settings for Samba NT4 Domains`.

* Create the user name mapping file ``/usr/local/samba/etc/username.map`` with the following content:

 root = administrator

* s the NT4 account ``administrator`` to the local ``root`` user.

* Start the ``smbd`` and ``nmbd`` daemons.

=================
User Account Creation
=================

To create a ``demoUser`` user account in the NT4 domain:

* Create the account on the local system:

 # useradd -M -s /sbin/nologin demoUser

* ``-M`` parameter if the user requires a home directory on this host. For Samba access, the account does not require a valid shell.

* To enable the ``demoUser``x account on the local system:

 # passwd demoUser
 Enter new UNIX password:
 Retype new UNIX password:
 passwd: updated successfully

* sword is only required for local log ins. Setting a local password is required to enable the account. Samba denies access if the account is disabled locally. Local log ins using this password are not possible if the account was created without a valid shell.

* Add the ``root`` and ``demoUser`` accounts to the Samba database:

 # smbpasswd -a root
 New SMB password:
 Retype new SMB password:
 Added user demoUser.

 # smbpasswd -a demoUser
 New SMB password:
 Retype new SMB password:
 Added user demoUser.

* words assigned in this step are the ones used by the users to log in to the domain.

* To enable both Samba accounts:

 # smbpasswd -e root
 Enabled user root.

 # smbpasswd -e demoUser
 Enabled user demoUser.

=================
Machine Account creation
=================

To create a machine account for the domain member ``M1``:

.. note:

    Machine accounts use a trailing ``$`` sign.

* Create the local Unix user account:

 # useradd -M -s /sbin/nologin M1$

* Enable the machine account:

 # smbpasswd -m -a M1$
 Added user M1$.

* code>-m`` parameter indicates that the account is a machine trust account.

=================
Windows Client Operating Modifications
=================

Modern Windows client operating systems require some modifies to operate in an NT4 domain. For details, see `Required_Settings_for_Samba_NT4_Domains|Required Settings for Samba NT4 Domains`.

=================
Joining a Client to the Domain
=================

For details, see:
* `Joining_a_Windows_Client_or_Server_to_a_Domain|Joining a Windows Client or Server to a Domain`
* `Setting_up_Samba_as_a_Domain_Member|Setting up Samba as a Domain Member`
* `Joining_a_MacOSX_client_to_a_domain|Joining a MacOSX Client to a Domain`

----
`Category:ntrol`
`Category:ns`