Migrating a Samba NT4 Domain to Samba AD (Classic Upgrade)
    <namespace>0</namespace>
<last_edited>2020-07-18T13:14:18Z</last_edited>
<last_editor>Hortimech</last_editor>

===============================

Introduction
===============================

.. warning:

   **This guide is only relevant if you have a Samba NT4-style domain, that you want to upgrade to Samba Active Directory!**

Many people find themselves in a situation where they have an existing Samba NT4-style domain, complete with an extensive set of domain users, groups and machines. The domain is functioning rather well, but they find themselves running into more and more dead ends. Things that a NT4-style domain just doesn't support.

Samba provides a way to migrate an existing NT4-style domain to a new Samba Active Directory. The following guide describes the upgrade scenario. It is suitable for upgrading an existing Samba installation, as well as running the migrating on a new server, if you had considered e.g. replacing the hardware at the same time.

=================
About classicupgrade
=================

The classicupgrade is a function built into samba-tool. The intent of this function is to do a full replacement of an existing Samba NT4-style domain. It is possible to do the conversion and the users and machines will simply re-connect to the new Samba AD installation without needing to manually re-join.

Doing a classicupgrade is possible from all passwd backends (smbpasswd, tdbsam and ldapsam).

=================
Important notes before you start
=================

**The migration from an NT4-style domain to Active Directory is one way! This means that once your <u>clients contact your migrated AD Domain Controller, they will never be able to access the NT4-style domain again</u> - even if you roll back your changes!**

**It's highly recommended, that <u>before you do the migration</u>, you should test the upgrade process in a separate network from your production! This enables you to avoid unnecessary downtime through unpredictable problems and it won't have any effect on your existing network.**

**It used to be thought that using Windows RIDs for Unix IDs was acceptable, time has proven otherwise. If you have users and groups that use their Windows RID for their Unix ID (uidNumber or gidNumber), you should consider changing these before carrying out the upgrade. You should also remove any Unix IDs (uidNumber or gidNumber) from the 'Well known SIDs'.**

=================
Server information used in this HowTo
=================

Inside this HowTo, we will be using the following configuration/settings:

 AD DC Installation Directory:       /usr/local/samba/
 AD DC Hostname:                     DC1
 AD DNS Name:                        samdom.example.com
 Realm:                              SAMDOM.EXAMPLE.COM
 NT4 Domain Name:                    samdom
 IP Address:                         192.168.1.1
 Databases of the Samba NT4-domain:  /usr/local/samba.PDC/dbdir/
 smb.conf of the Samba NT4-domain:   /usr/local/samba.PDC/etc/smb.PDC.conf

=================
Preparations
=================

Upgrading on a new server 
------------------------

If you had chosen to migrate to Samba Active Directory on a different machine, some further steps of preparation are required.

LDAP
------------------------

If the backend of your PDC is ldapsam, you have the choice of:

* allowing the LDAP backend to stay on it's current host, as it is only required for the classicupgrade. Samba AD is shipped with its own LDAP server and won't use the external LDAP server. In this case, make sure that you check/adapt your configuration (ldap.conf, smb.conf) to retrieve the information from your old LDAP backend host.

* install a temporary openLDAP backend on the new host, import the content of your current installation and after the classicupgrade, shut it down. The following is just a basic set of steps, typically required for this procedure. Consult your distributions documentation for specific requirements.

* LDAP export on the old host::

 # slapcat > ldap.backup.ldif

* Copy the export file to your new server

* Install openLDAP (incl. headers and libraries) on the new host

* Stop the LDAP service on the new host

* Import the ldif::

 # slapadd -l ldap.backup.ldif

* Check/adjust permissions on your LDAP database directory (distro specific)::

 # chown -R ldap /var/lib/ldap/

* Copy the slapd.conf from your old LDAP host to the new one

* Start the LDAP service

Samba
------------------------

You also need to transfer several databases and the smb.conf of your old PDC to the new host, as well. The `#The_classicupgrade_process|classicupgrade process` section will tell you later about everything needed. You just have to copy the required files to the new server before.

Avoiding common problems 
------------------------

Prevent failure due to duplicate SID's
------------------------

A common problem is duplicate SID's in the backend. In a healthy environment, a [https://en.wikipedia.org/wiki/Security_Identifier SID] is unique. However, old Samba versions without sanity checks, wrong manual changes or other things, could have allowed duplicate SID's in your environment. These need to be fixed/removed. Otherwise the classicupgrade is not possible!

If any of your users have a RID less than '1000' and you wish them to exist in the new AD domain, you will need to change their RID, see below for how to do this.

See: `#What_are_the_consequences_of_changing_a_SID.2FRID.3F|What are the consequences changing an SID/RID?`

To detect duplicate SID's in an LDAP backend, you can use the following script on your LDAP server:

 #!/usr/bin/python
 # A quick and dirty python script that checks for duplicate SID's using slapcat.
 import os

 data = os.popen("slapcat 2>&1 | grep sambaSID", 'r')
 line = []
     def anydup(thelist):
         dups = list(set([x for x in thelist if thelist.count(x) > 1]))
         for i in dups:
                 print "Duplicate id: ", i

 for each_line in data:
         line.append(each_line.strip())

 anydup(line)

To find duplicate SID's on other passdb backends (smbpasswd, tdbsam), you have to script around the output of the following two commands:

 # pdbedit -Lv
 # net groupmap list

To change SID's for groups, remove the mapping and re-add it. A new SID with the next free RID is created and used.

 # net groupmap delete ntgroup="demo group"
 Successfully removed demo group from the mapping db

 # net groupmap add ntgroup="Demo Group" unixgroup="demo group"
 No rid or sid specified, choosing a RID
 Got RID 1009
 Successfully added group Demo Group to the mapping db as a domain group

.. note:

    If you have mapped any of the Well Known SID's, e.g. Domain Users, to a Unix group, you should delete these mappings and not recreate them.

For user and machine accounts, you have to manually assign a new RID:

 # net maxrid
 Currently used maximum rid: 3001

 # pdbedit -U 3002 -u demo1
 ...
 User SID:   S-1-5-21-4097619914-84555263-3210783664-3002

Prevent failure due to common user/group names
------------------------

If you have any usernames that are the same as a groupname, you will have to rename one of them. Otherwise the provisioning will fail („ProvisioningError: Please remove common user/group names before upgrade.“). Also, if you have unique groups that, for whatever historical reason, share the same displayName, they will have to be edited so that all the displayNames are different.

slapd.conf sizelimit
------------------------

*Note: The following is only relevant in passdb backend = ldapsam setups:*:

If you have many objects in your PDC LDAP, you should consider adding
 sizelimit {<integer>|unlimited}
to your slapd.conf. This parameter specifies the maximum number of entries returned from a search operation. The default, if not set, is 500. This can cause problems, when having many objects in your LDAP directory and classicupgrade can't retrieve them all!

Active Directory Domain Name 
------------------------

Choose a meaningful and suitable Active Directory domain name / realm. See `The_Samba_AD_DNS_Back_Ends#Selecting_the_Forest_Root_Domain|DNS best practice`.

*Note: Currently Samba does not provide capabilities to change the AD Domain Name afterwards!*

Domain Controller name 
------------------------

If you need to change the Domain Controller name during the migration, simply edit the old PDC smb.conf file that the classicupgrade will use for doing the migration and set/change the netbios name:

 netbios name = DC1

Though it's possible to rename a DC afterwards, it is always additional work and can cause problems (forgotten configuration adaptations, etc.).

.. warning:

   The netbios name set in ``smb.conf`` **must** match the short hostname the Domain Controller will run on.

=================
Installing Samba
=================

Versions 
------------------------

This HowTo is frequently updated to reflect the latest changes. Please see the `Samba_Release_Planning|Samba Release Planning` for more specifics.

Different ways to install 
------------------------

You have a few options to install Samba:

* `Build_Samba_from_Source|Build Samba` yourself.

* Install `Distribution-specific_Package_Installation|binary distribution packages`. Make sure that you use a recent Samba installation with Active Directory Domain Controller capabilities!

* Install from [http:SSLLAASS:HHwww.enterprisesamba.com/samba/ SerNet Enterprise Samba] package.

See `Operating_System_Requirements|Operating System Requirements` for dependencies and recommendations.

Paths 
------------------------

Take care when running Samba commands, if you also have a previous version of Samba installed.  To avoid inadvertently running the wrong version, you should consider putting the /usr/local/samba/bin/ and /usr/local/samba/sbin/ directories in the beginning of your $PATH variable!

You can see what version of Samba, if any, is in your $PATH variable, by running:
 # samba --version

=================
The classicupgrade process
===============================

Before you start, If you are carrying out the upgrade on the same server as your NT4-style domain PDC, you should shutdown your Samba PDC services (smbd, nmbd, winbind), but leave your LDAP server running (if using passwd backend = ldapsam). You should also stop the services from restarting at boot. If you are carrying out the upgrade on a new Computer, you should also stop any running Samba services.

To avoid mixing binaries/libraries/databases from the old and new installations, create two new Samba PDC installation directories:

 # mkdir -p /usr/local/samba.PDC/dbdir/
 # mkdir /usr/local/samba.PDC/etc

The classicupdate process uses information from the databases of your old PDC installation. That is why it is necessary to have them all in one directory. Copy the following databases from your old PDC installation to a new folder. This guide will use /usr/local/samba.PDC/dbdir/ :

 # mv /usr/local/samba/private/secrets.tdb /usr/local/samba.PDC/dbdir/
 # mv /usr/local/samba/private/schannel_store.tdb /usr/local/samba.PDC/dbdir/
 # mv /usr/local/samba/private/passdb.tdb /usr/local/samba.PDC/dbdir/
 # mv /usr/local/samba/var/locks/group_mapping.tdb /usr/local/samba.PDC/dbdir/
 # mv /usr/local/samba/var/locks/account_policy.tdb /usr/local/samba.PDC/dbdir/

This will also prevent problems happening, if your old Samba installation is started automatically at boot time again.

Move your smb.conf to a new location, with a name that indicates it's the one from your old PDC:

 # mv /usr/local/samba/etc/smb.conf /usr/local/samba.PDC/etc/smb.PDC.conf

The classicupgrade will setup a database based on the Samba NT4-style domain SID. A default directory layout is created including accounts, groups, ACLs, etc. Imports of e.g. user and machine accounts are done. 

The classicupgrade step must be run as user root. Otherwise you will get permission denied errors!

To start the <u>classicupgrade with `The_Samba_AD_DNS_Back_Ends#Internal_DNS|Internal DNS` setup</u>, run:

 # samba-tool domain classicupgrade --dbdir=/usr/local/samba.PDC/dbdir/ \
 --realm=SAMDOM.EXAMPLE.COM --dns-backend=SAMBA_INTERNAL /usr/local/samba.PDC/etc/smb.PDC.conf

To start the <u>classicupgrade with `The_Samba_AD_DNS_Back_Ends#BIND_DLZ_plug-in_.28for_BIND_9.8_and_9.9.29|BIND_DLZ DNS` setup</u>, run:
 # samba-tool domain classicupgrade --dbdir=/usr/local/samba.PDC/dbdir/ \
 --realm=SAMDOM.EXAMPLE.COM --dns-backend=BIND9_DLZ /usr/local/samba.PDC/etc/smb.PDC.conf

<u>Parameter explanations:ASSHHu>
 --dbdir=                 Path to samba classic DC directory, containing all databases required for
                          the migration
 --realm=                 Set the realm name
 --dns-backend=           Optional. 
                          Required if `BIND9_DLZ_DNS_Back_End|BIND9_DLZ should be used as DNS backend`.
                          Default is the internal DNS (SAMBA_INTERNAL)

 Optional:
 If you have multiple NICs, classicupgrade auto-chooses the IPv4/v6 address of one NIC to setup 
 the Domain Controller.
 To prevent this, add the following parameters to the classicupgrade command. This will bind Samba to 
 the given interface (eth0) and localhost (Samba should always listen on localhost too).
 --option="interfaces=lo eth0" --option="bind interfaces only=yes"

The following is a <u>sample</u> output of a successful classicupgrade. Depending on your database backend, Samba version and other factors, the output will differ:

 Reading smb.conf
 Provisioning
 Exporting account policy
 Exporting groups
 Exporting users
 Next rid = 1007
 Exporting posix attributes
 Reading WINS database
 Cannot open wins database, Ignoring: [Errno 2] No such file or directory: :SHHusr/local/samba.PDC/dbdir/wins.dat'
 Looking up IPv4 addresses
 Looking up IPv6 addresses
 No IPv6 address will be assigned
 Setting up share.ldb
 Setting up secrets.ldb
 Setting up the registry
 Setting up the privileges database
 Setting up idmap db
 Setting up SAM db
 Setting up sam.ldb partitions and settings
 Setting up sam.ldb rootDSE
 Pre-loading the Samba 4 and AD schema
 Adding DomainDN: DC=samdom,DC=example,DC=com
 Adding configuration container
 Setting up sam.ldb schema
 Setting up sam.ldb configuration data
 Setting up display specifiers
 Modifying display specifiers
 Adding users container
 Modifying users container
 Adding computers container
 Modifying computers container
 Setting up sam.ldb data
 Setting up well known security principals
 Setting up sam.ldb users and groups
 Setting up self join
 Setting acl on sysvol skipped
 Adding DNS accounts
 Creating CN=MicrosoftDNS,CN=System,DC=samdom,DC=example,DC=com
 Creating DomainDnsZones and ForestDnsZones partitions
 Populating DomainDnsZones and ForestDnsZones partitions
 See /usr/local/samba/private/named.conf for an example configuration include file for BIND
 and /usr/local/samba/private/named.txt for further documentation required for secure DNS updates
 Setting up sam.ldb rootDSE marking as synchronized
 Fixing provision GUIDs
 A Kerberos configuration suitable for Samba 4 has been generated at /usr/local/samba/private/krb5.conf
 Setting up fake yp server settings
 Once the above files are installed, your Samba4 server will be ready to use
 Server Role:           active directory domain controller
 Hostname:              DC1
 NetBIOS Domain:        SAMDOM
 DNS Domain:            samdom.example.com
 DOMAIN SID:            S-1-5-21-4097619914-84555263-3210783664
 Importing WINS database
 Importing Account policy
 Importing idmap database
 Cannot open idmap database, Ignoring: [Errno 2] No such file or directory
 Adding groups
 Importing groups
 Group already exists sid=S-1-5-21-4097619914-84555263-3210783664-513, groupname=Domain Users existing_groupname=Domain Users, Ignoring.
 Commiting 'add groups' transaction to disk
 Adding users
 Importing users
 Commiting 'add users' transaction to disk
 Adding users to groups
 Commiting 'add users to groups' transaction to disk
 Setting password for administrator

**Note <u>If you re-run the classicupgrade</u>, you will need to remove the auto-generated smb.conf and the databases::

 # rm -f /usr/local/samba/etc/smb.conf
 # rm -rf /usr/local/samba/private/*

=================
After the classicupgrade
=================

* If your passdb backend was ldapsam, shutdown your LDAP server, Samba Active Directory will start its own LDAP server that binds to the `Samba_AD_DC_Port_Usage|default ports port 389/tcp (LDAP) and 636/tcp (LDAPS)`.

* Disable the automatic start of your Samba PDC services and LDAP server (if any).

* Enable your Samba AD service to automatically start at boot time.

* If your passdb backend was smbpasswd or tdbsam, remove the domain groups from /etc/group. All groups that had a groupmapping were imported, including their members. You should also remove any Samba users from /etc/passwd, they are now stored in AD.

* If you used the internal DNS server, you will need to add a ``dns forwarder`` line to smb.conf, such as ``dns forwarder = 8.8.8.8``.

* Edit /etc/resolv.conf to use the DC's ipaddress for the first nameserver.

* Install winbind, if it is not already installed

.. warning:

   Once you have upgraded your NT4-style domain to an AD domain, be very selective about what lines you add to your new smb.conf . Never add any ``idmap config`` or ``vfs object`` lines.

=================
Continuing with the AD DC setup
=================

The „classicupdate“ process replaces the „provisioning“ step in the Samba AD DC HowTo. If the classicupgrade finished without problems, you have to continue with the Samba AD DC HowTo `Setting_up_Samba_as_an_Active_Directory_Domain_Controller#Testing_your_Samba_AD_DC|after the provisioning step`.

=================
Improving classicupgrade
=================

Because of complexity and the various setups, the classicupgrade doesn't catch all exceptions with a meaningful message yet.

Please open a [https://bugzilla.samba.org/ bug report] for every uncaught exception error (please first [https://bugzilla.samba.org/query.cgi search bugzilla], to see if it has already been reported).

Of course all other bugs, feature enhancements, etc. should be [https://bugzilla.samba.org/ reported], as well, to improve the migration process in future releases.

=================
classicupgrade FAQ
=================

What are the consequences of changing a SID/RID? 
------------------------

**Warning SID's are the only thing that Windows uses in it's backend to identify users, groups and machines. Changing a SID, without due consideration, may result in serious problems or damages!

Example 1: You have two accounts with the same SID. One of them is member of the local administrators group on a workstation. If you change the SID of this account, the group membership gets lost, because Windows had stored the account SID in the group and not the account name.

Example 2: You have two machine accounts with the same SID. If you change the SID of one account, then this computer is not part of the domain any more and logins are not possible. If you have duplicate SID's and at least one of them is on a machine account, the easiest way is to delete the machine account and rejoin the computer to the domain.

Error: User 'Administrator' in your existing directory has SID ..., expected it to be ...-500 
------------------------

The error says what's wrong: In your NT4-style domain backend, the RID of the domain administrator account isn't -500, what it should be (see [http:SSLLAASS:HHsupport.microsoft.com/kb/243330/en Windows well-known security identifiers]). Change it to 500 and start over. You can remove the account, too, as it will be automatically created during the AD provisioning. See `#What_are_the_consequences_of_changing_a_SID.2FRID.3F|What are the consequences of changing a SID/RID?`.

Not all attributes were copied when migrating from passwd backend = ldapsam 
------------------------

Sadly classicupdate currently does not migrate all attributes found in LDAP. You can follow [https://bugzilla.samba.org/show_bug.cgi?id=9008 bug report #9908] about the progress. But improvements would only take effect when doing the classicupgrade - not afterwards!

Workaround:

* Change the listen port of your NT4-style domain LDAP server to a different one than its default (389/tcp), if hosted on the same machine as your new Active Directory.

* Write a small script, that walks through all user accounts in your AD, then search the same user account in your old LDAP and retrieve the attributes you want to transfer. Add them to an LDIF file, which can be imported with 

 # ldbmodify -H path/to/sam.ldb LDIFfilename

----
`Category Directory`
`Category Domains`