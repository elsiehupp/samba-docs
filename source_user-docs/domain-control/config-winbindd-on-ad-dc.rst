Configuring Winbindd on a Samba AD DC
    <namespace>0</namespace>
<last_edited>2019-08-08T15:22:11Z</last_edited>
<last_editor>Hortimech</last_editor>

===============================

Introduction
===============================

The ``Winbindd`` service enables you to:
* Use domain users and groups in local commands, such as ``chown`` and ``chgrp``.
* Display domain users and groups in local command's output, such as ``ls``.

Configuring ``Winbindd`` on a Samba Active Directory (AD) domain controller (DC) is different than on a domain member. To configure the service on a domain member, see `Setting_up_Samba_as_a_Domain_Member|Setting up Samba as a Domain Member`.

The Difference Between the ``Winbind`` and ``Winbindd`` Service 
------------------------

Samba 4.0 and 4.1 used a version of ``Winbind`` built into the ``samba`` command. However, this implementation never worked correctly. For this reason, Samba 4.2 enabled the ``winbindd`` utility to be used on domain controllers (DC). If you run a version of Samba prior to 4.2, update to a supported version before using ``Winbindd``. For details, see `Updating_Samba|Updating Samba`.

Identity Mapping on a Samba Domain Controller 
------------------------

Identity Mapping works differently on a Samba domain controller (DC) than on a Unix domain member. For example, setting ``idmap config`` lines in the DC ``smb.conf`` file is not supported and will cause the ``samba`` service to fail. For details, see `Updating_Samba#Failure_To_Access_Shares_on_Domain_Controllers_If_idmap_config_Parameters_Set_in_the_smb.conf_File|Failure_To_Access_Shares_on_Domain_Controllers_If_idmap_config_Parameters_Set_in_the_smb.conf_File`.

On a Samba Active Directory DC, ``Winbindd`` will always read the user IDs (UID) and group IDs (GID) from any ``uidNumber`` and ``gidNumber`` attributes set in the AD user or group objects. The ``uidNumber`` and ``gidNumber`` attributes are not added automatically, so any users and groups that do not have a UID or GID assigned will have an ID generated locally on the DC and stored in the ``/usr/local/samba/private/idmap.ldb`` file.

.. note:

    If you set an ID in the AD object's properties after a local ID was generated, ``Winbindd`` wiil only use the value from the directory after the winbindd cache expires or you manually run ``net cache flush``. The operating system manages file ownerships using IDs. You must manually reset the permissions on files to enable the user to access the files using the new ID.

=================
Setting ``Winbindd`` Parameters in the smb.conf File
=================

To run ``Winbindd`` on a Samba Active Directory (AD) domain controller (DC), in most cases no configuration in the ``smb.conf`` file is required.

User and group IDs, are loaded from Active Directory (AD) or automatically generated locally. For details, see `#Identity_Mapping_on_a_Samba_Domain_Controller|Identity Mapping on a Samba Domain Controller`.

On a Samba DC, only the winbind template mode is supported. In this mode, all users get:
* The home directory path assigned, set in the ``template homedir`` parameter. The default value of this parameter is ``/home/%D/%U``.
* The shell assigned, set in the ``template shell`` parameter. The default value of this parameter is ``/bin/false``.

To assign the ``/bin/bash`` shell and the ``/home/%U`` path as home directory path to all domain users provided by ``Winbindd``:
* Add the following parameters to the ``[global]`` section of your ``smb.conf`` file:
 template shell = /bin/bash
 template homedir = /home/%U
* For details, see the ``smb.conf(5)`` man page.

* Restart the ``samba`` service.

{{Imbox
| type = warning
| text = On a Samba AD DC, not all of the ``Winbindd``-related parameters described in the ``smb.conf(5)`` man page are supported. Additionally, some of the parameters, such as ``idmap config``, will cause the ``samba`` service to fail. Currently, the man page does not highlight parameters supported on a DC. Thus it is suggested that you keep the defaults or only use the parameters described in this section.

=================
Libnss winbind Links
=================

{{:Libnss_winbind_Links}}

=================
Configuring the Name Service Switch
=================

To enable the name service switch (NSS) library to make domain users and groups available to the local system:

* Append the ``winbind`` entry to the following databases in the ``/etc/nsswitch.conf`` file:

 passwd: files <u>winbind</u>
 group:  files <u>winbind</u>

* * Keep the ``files`` entry as first source for both databases. This enables NSS to look up domain users and groups from the ``/etc/passwd`` and ``/etc/group`` files before querying the Winbind service.

* * Do not add the ``winbind`` entry to the NSS ``shadow`` database. This can cause the ``wbinfo`` utility fail.

* * If you compiled Samba, add symbolic links from the ``libnss_winbind`` library to the operating system's library path. For details, see `Libnss_winbind_Links|libnss_winbind Links`. If you used packages to install Samba, the link is usually created automatically.

.. note:

    Do not use the same user names in the local ``/etc/passwd`` file as in the domain.

=================
The ``winbindd`` Service
=================

Do not start the ``winbindd`` Service manually on a Samba Active Directory (AD) domain controller (DC). The service is started automatically as a sub-process of the ``samba`` process. To verify, enter:

 # ps axf
 ...
 2156 ?        Ss     0:00 /usr/local/samba/sbin/samba -D
 2158 ?        S      0:00  \_ /usr/local/samba/sbin/samba -D
 2172 ?        R      0:00      **\_ /usr/local/samba/sbin/winbindd -D --option=server role check:inhibit=yes --foreground**
 ...

=================
Testing the Winbindd Connectivity
=================

See `Setting_up_Samba_as_a_Domain_Member#Testing_the_Winbindd_Connectivity|Testing the Winbindd Connectivity`.

=================
Authenticating Domain Users Using PAM
=================

See `Authenticating_Domain_Users_Using_PAM|Authenticating Domain Users Using PAM`.

----
`Category:Active Directory`