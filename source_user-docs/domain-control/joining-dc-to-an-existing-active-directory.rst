Joining a Samba DC to an Existing Active Directory
    <namespace>0</namespace>
<last_edited>2020-02-12T12:07:23Z</last_edited>
<last_editor>Hortimech</last_editor>

===============================

Introduction
===============================

Running one domain controller (DC) is sufficient for a working Active Directory (AD) forest. However, for redundacy and load balancing reasons, you should add further DCs to your AD forest. Joining an additional Samba DC to an existing AD differs from provisioning the first DC in a forest. If you set up a new AD forest, see `Setting_up_Samba_as_an_Active_Directory_Domain_Controller|Setting up Samba as an Active Directory Domain Controller`.

{{Imbox
| type = warning
| text = Do not provision a Computer as a Samba AD DC, then try to join it to an existing AD domain. This will not work, you only need to run the ``samba-tool domain join`` command to join a Computer to the existing AD domain. 

{{Imbox
| type = warning
| text = If you are joining a Samba as a DC to an existing Windows AD domain that was provisioned as a Windows 2003 (or earlier) DC, you must ensure that it is running a domain integrated DNS server. This dns server must be configured with 2008 behaviour. 

.. note:

    An NT4 domain uses only one Primary Domain Controller (PDC) and optionally additional Backup Domain Controllers (BDC). In an AD forest, there is no difference between DCs, beside the `Flexible_Single-Master_Operations_(FSMO)_Roles|FSMO roles`. Use only the term "domain controller" or "DC" when you talk about AD to avoid any possibility of confusion.

=================
Preparing the Installation
=================

For details, see `Setting_up_Samba_as_an_Active_Directory_Domain_Controller#Preparing_the_Installation|Preparing the Installation` in the `Setting_up_Samba_as_an_Active_Directory_Domain_Controller|Setting up Samba as an Active Directory Domain Controller` documentation.

=================
Installing Samba
=================

For details, see `Installing_Samba|Installing Samba`.

.. note:

    Install a maintained Samba version. For details, see `Samba_Release_Planning|Samba Release Planning`.

=================
Preparing the Host for Joining the Domain
=================

Local DNS server 
------------------------

By default, the first Domain Controller (DC) in a forest runs a DNS server for Active Directory (AD)-based zones. For redundancy reasons it is recommended to run multiple DCs acting as a DNS server in a network. If you consider providing a DNS service on the new DC:

* For the ``BIND9_DLZ`` back end, see `BIND9_DLZ_DNS_Back_End|BIND9_DLZ DNS Back End`. Finish this task before you start the Samba DC service.
* For the internal DNS no further actions are required.

Configuring DNS 
------------------------

For details, see `Linux_and_Unix_DNS_Configuration|Linux and Unix DNS Configuration`.

.. note:

    The 'nameserver' you set in '/etc/resolv.conf' must be an AD DC, otherwise the join will not be able to find the KDC. 

Kerberos 
------------------------

Set the following settings in your Kerberos client configuration file ``/etc/krb5.conf``:

 [libdefaults]
     dns_lookup_realm = false
     dns_lookup_kdc = true
     default_realm = SAMDOM.EXAMPLE.COM

To verify the settings use the ``kinit`` command to request a Kerberos ticket for the domain administrator:

 # kinit administrator
 Password for administrator@SAMDOM.EXAMPLE.COM:

To list Kerberos tickets:

 # klist
 Ticket cache: FILE:/tmp/krb5cc_0
 Default principal: administrator@SAMDOM.EXAMPLE.COM

 Valid starting       Expires              Service principal
 24.09.2015 19:56:55  25.09.2015 05:56:55  krbtgt/SAMDOM.EXAMPLE.COM@SAMDOM.EXAMPLE.COM
 	renew until 25.09.2015 19:56:53

=================
Configuring Time Synchronisation
=================

Kerberos requires a synchronised time on all domain members. For further details and how to set up the ``ntpd`` service, see `Time_Synchronisation|Time Synchronisation`.

=================
Joining the Active Directory as a Domain Controller
=================

To join the domain ``samdom.example.com`` as a domain controller (DC) that additionally acts as a DNS server using the Samba internal DNS:

There are three authentication methods you can use, Username & Password or two kerberos methods (the kerberos methods depend on running ``kinit`` as an admin user).

Username & Password:
 # samba-tool domain join samdom.example.com DC -U"SAMDOM\administrator"

Or:
 # samba-tool domain join samdom.example.com DC -k yes

Or:
 # samba-tool domain join samdom.example.com DC --krb5-ccache=/tmp/krb5cc_0

Using any of the above, should result in output similar to this:

 Finding a writeable DC for domain 'samdom.example.com'
 Found DC dc1.samdom.example.com
 Password for [SAMDOM\administrator]:
 workgroup is SAMDOM
 realm is samdom.example.com
 Adding CN=DC2,OU=Domain Controllers,DC=samdom,DC=example,DC=com
 Adding CN=DC2,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com
 Adding CN=NTDS Settings,CN=DC2,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com
 Adding SPNs to CN=DC2,OU=Domain Controllers,DC=samdom,DC=example,DC=com
 Setting account password for DC2$
 Enabling account
 Calling bare provision
 Looking up IPv4 addresses
 Looking up IPv6 addresses
 No IPv6 address will be assigned
 Setting up share.ldb
 Setting up secrets.ldb
 Setting up the registry
 Setting up the privileges database
 Setting up idmap db
 Setting up SAM db
 Setting up sam.ldb partitions and settings
 Setting up sam.ldb rootDSE
 Pre-loading the Samba 4 and AD schema
 A Kerberos configuration suitable for Samba 4 has been generated at /usr/local/samba/private/krb5.conf
 Provision OK for domain DN DC=samdom,DC=example,DC=com
 Starting replication
 Schema-DN[CN=Schema,CN=Configuration,DC=samdom,DC=example,DC=com] objects[402/1550] linked_values[0/0]
 Schema-DN[CN=Schema,CN=Configuration,DC=samdom,DC=example,DC=com] objects[804/1550] linked_values[0/0]
 Schema-DN[CN=Schema,CN=Configuration,DC=samdom,DC=example,DC=com] objects[1206/1550] linked_values[0/0]
 Schema-DN[CN=Schema,CN=Configuration,DC=samdom,DC=example,DC=com] objects[1550/1550] linked_values[0/0]
 Analyze and apply schema objects
 Partition[CN=Configuration,DC=samdom,DC=example,DC=com] objects[402/1618] linked_values[0/0]
 Partition[CN=Configuration,DC=samdom,DC=example,DC=com] objects[804/1618] linked_values[0/0]
 Partition[CN=Configuration,DC=samdom,DC=example,DC=com] objects[1206/1618] linked_values[0/0]
 Partition[CN=Configuration,DC=samdom,DC=example,DC=com] objects[1608/1618] linked_values[0/0]
 Partition[CN=Configuration,DC=samdom,DC=example,DC=com] objects[1618/1618] linked_values[42/0]
 Replicating critical objects from the base DN of the domain
 Partition[DC=samdom,DC=example,DC=com] objects[100/100] linked_values[23/0]
 Partition[DC=samdom,DC=example,DC=com] objects[386/286] linked_values[23/0]
 Done with always replicated NC (base, config, schema)
 Replicating DC=DomainDnsZones,DC=samdom,DC=example,DC=com
 Partition[DC=DomainDnsZones,DC=samdom,DC=example,DC=com] objects[44/44] linked_values[0/0]
 Replicating DC=ForestDnsZones,DC=samdom,DC=example,DC=com
 Partition[DC=ForestDnsZones,DC=samdom,DC=example,DC=com] objects[19/19] linked_values[0/0]
 Committing SAM database
 Sending DsReplicaUpdateRefs for all the replicated partitions
 Setting isSynchronized and dsServiceName
 Setting up secrets database
 Joined domain SAMDOM (SID S-1-5-21-469703510-2364959079-1506205053) as a DC

See the ``samba-tool domain join --help`` command's output for further information.

Other parameters frequently used with the ``samba-tool domain join`` command:

* ``--dns-backend=NAMESERVER-BACKEND``: Use the supplied DNS server backend. Valid options are ``SAMBA_INTERNAL`` or ``BIND9_DLZ``, unless you want to use Bind9, there is no need to supply this option.
* : If you use the internal DNS server, you will not be asked for a forwarder and the one in /etc/resolv.conf will not be obtained automatically. You must supply one with ``--option="dns forwarder=forwarder_ipaddress"``.

* ``--site=SITE``: Directly join the host as DC to a specific `Active_Directory_Sites|Active Directory Site`.

* ``--option="interfaces=lo eth0" --option="bind interfaces only=yes"``: If your server has multiple network interfaces, use these options to bind Samba to the specified interfaces. This enables the ``samba-tool`` command to register the correct LAN IP address in the directory during the join.

.. note:

    If the other DCs are Samba DCs and were provisioned with ``--use-rfc2307``, you Should add ``--option='idmap_ldb:use rfc2307 = yes'`` to the join command

=================
Verifying the DNS Entries
=================

If you join a Samba DC that runs Samba 4.7 and later, ``samba-tool`` created all required DNS entries automatically. To manually create the records on an earlier version, see `Verifying_and_Creating_a_DC_DNS_Record|Verifying and Creating a DC DNS Record`.

=================
Configuring the BIND9_DLZ DNS Back End
=================

If you selected the ``BIND9_DLZ`` DNS back end during the domain join, set up the BIND configuration. For details, see `BIND9_DLZ_DNS_Back_End|BIND9_DLZ DNS Back End`.

=================
Built-in User & Group ID Mappings
=================
{{:SysVol replication (DFS-R)}}

To use a Sysvol Replication workaround, all domain controllers (DC) must use the same ID mappings for built-in users and groups.

By default, a Samba DC stores the user & group IDs in 'xidNumber' attributes in 'idmap.ldb'. Because of the way 'idmap.ldb' works, you cannot guarantee that each DC will use the same ID for a given user or group. To ensure that you do use the same IDs, you must: 

* Create a hot-backup of the ``/usr/local/samba/private/idmap.ldb`` file on the existing DC:

 # tdbbackup -s .bak /usr/local/samba/private/idmap.ldb

* This creates a backup file ``/usr/local/samba/private/idmap.ldb.bak``.

* Move the backup file to the ``/usr/local/samba/private/`` folder on the new joined DC and remove the ``.bak`` suffix to replace the existing file.

* Run ``net cache flush`` on the new DC.

* You will now need to sync Sysvol to the new DC.

* Reset the Sysvol folder's file system access control lists (ACL) on the new DC:

 # samba-tool ntacl sysvolreset

=================
Starting the Samba Service
=================

To start the ``samba`` Samba Active Directory (AD) domain controller (DC) service manually, enter:

 # samba

Samba does not provide System V init scripts, ``systemd``, ``upstart``, or other services configuration files.
* If you installed Samba using packages, use the script or service configuration file included in the package to start Samba.
* If you built Samba, see `Managing_the_Samba_AD_DC_Service|Managing the Samba AD DC Service`.

=================
Verifying Directory Replication
=================

After the domain controller (DC) has been started, the knowledge consistency checker (KCC) on the Samba DC creates replication agreements to other DCs in the Active Directory (AD) forest. It can take up to 15 minutes until the KCC creates the auto-generated replication connections.

For details about how to verify that the directory replication works correctly, see `Verifying the Directory Replication Statuses`.

.. note:

    To optimize replication latency and cost, the KCC in Samba 4.5 and later no longer creates a fully-meshed replication topology between all DCs. For further details, see `The Samba KCC`.

=================
Starting BIND
=================

Before you start the BIND daemon, verify that the DNS directory partitions have been successfully replicated:

 # samba-tool drs showrepl
 ...
 INBOUND NEIGHBORS =
===============================

------------------------

 ...
 DC=DomainDnsZones,DC=samdom,DC=example,DC=com
 	Default-First-Site-Name\DC1 via RPC
 		DSA object GUID: 4a6bd92a-6612-4b15-aa8c-9ec371e8994f
 		Last attempt @ Thu Sep 24 20:08:45 2015 CEST was successful
 		0 consecutive failure(s).
 		Last success @ Thu Sep 24 20:08:45 2015 CEST
 ...
 DC=ForestDnsZones,DC=samdom,DC=example,DC=com
 	Default-First-Site-Name\DC1 via RPC
 		DSA object GUID: 4a6bd92a-6612-4b15-aa8c-9ec371e8994f
 		Last attempt @ Thu Sep 24 20:08:45 2015 CEST was successful
 		0 consecutive failure(s).
 		Last success @ Thu Sep 24 20:08:45 2015 CEST

If the replication works correctly, start the BIND service. See your distribution's documentation for information how to start a service.

=================
Testing your Samba AD DC
=================

Verifying the File Server 
------------------------

For details, see `Setting_up_Samba_as_an_Active_Directory_Domain_Controller#Verifying_the_File_Server|Verifying the File Server` in the `Setting_up_Samba_as_an_Active_Directory_Domain_Controller|Setting up Samba as an Active Directory Domain Controller` documentation.

Testing the Local DNS Server 
------------------------

Skip this step if you selected ``--dns-backend=NONE`` during the join.

Query the local DNS server to resolve the domain name ``samdom.example.com``:

 # host -t A samdom.example.com localhost
 Using domain server:
 Name: localhost
 Address: 127.0.0.1#53
 Aliases:

 samdom.example.com has address 10.99.0.1
 samdom.example.com has address 10.99.0.2

The local DNS resolves the domain name to the IP addresses of all domain controllers (DC).

In case you receive no or a different result, review this documentation and check:
* the system log files,
* the Samba log files,
* the BIND log files, if the ``BIND9_DLZ`` is used.

Verifying Kerberos 
------------------------

For details, see `Setting_up_Samba_as_an_Active_Directory_Domain_Controller#Verifying_Kerberos|Verifying Kerberos` in the `Setting_up_Samba_as_an_Active_Directory_Domain_Controller|Setting up Samba as an Active Directory Domain Controller` documentation.

=================
DNS Configuration on Domain Controllers
=================

The DNS configuration on domain controllers (DC) is important, because if it is unable to locate other DCs the replication will fail.

Set the local IP of the DC as the primary name server. For example:

On the new joined DC, use the local ``10.99.0.2`` IP as primary ``nameserver`` entry:

 nameserver 10.99.0.2
 search samdom.example.com

=================
Configuring Winbindd on a Samba AD DC
=================

*Optional*. For details, see `Configuring_Winbindd_on_a_Samba_AD_DC|Configuring Winbindd on a Samba AD DC`.

=================
Using the Domain Controller as a File Server
=================

For details, see `Setting_up_Samba_as_an_Active_Directory_Domain_Controller#Using_the_Domain_Controller_as_a_File_Server|Using the Domain Controller as a File Server`.

=================
Sysvol Replication
=================

Samba currently does not automatically replicate Sysvol, you must use some other form of replication. For community supported workarounds, see `SysVol_replication_(DFS-R)|Sysvol Replication`.

.. note:

    If there are more than the default GPOs in Sysvol on the other DC(s), you must sync Sysvol to the new DC, ``samba-tool ntacl sysvolreset`` will throw an error if you do not.

=================
Testing the Directory Replication
=================

To test that the directory replication works correctly, add for example a user on an existing DC and verify that it shows up automatically on the newly joined DC.

Optionally use the ``ldapcmp`` utility to compare two directories. For details, see `Samba-tool_ldapcmp|samba-tool ldapcmp`.

=================
Troubleshooting
=================

For further details, see `Samba_AD_DC_Troubleshooting|Samba AD DC Troubleshooting`.

----
`Category:Active Directory`
`Category:Domain Control`