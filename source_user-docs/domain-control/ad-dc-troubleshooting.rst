Samba AD DC Troubleshooting
    <namespace>0</namespace>
<last_edited>2021-02-15T08:52:30Z</last_edited>
<last_editor>B3it</last_editor>

===============================

Introduction
===============================

This documentation helps you to troubleshoot problems users can encounter when running Samba as an Active Directory (AD) domain controller (DC).

=================
General
=================

Setting the Samba Log Level 
------------------------

For details, see `Setting_the_Samba_Log_Level|Setting the Samba Log Level`.

The ``net`` Command Fails to Connect to the ``127.0.0.1`` IP Address 
------------------------

For details, see `Troubleshooting_Samba_Domain_Members#The_net_Command_Fails_to_Connect_to_the_127.0.0.1_IP_Address|Troubleshooting Samba Domain Members - The net Command Fails to Connect to the 127.0.0.1 IP Address`.

=================
Process Management
=================

Verifying That Samba Is Running 
------------------------

Use the ``ps`` utility to verify that Samba processes are executed:

 # ps axf | egrep "samba|smbd|winbindd"
 ...
 917 ?        Ss     0:00 /usr/local/samba/sbin/samba -D
 923 ?        S      0:00  \_ /usr/local/samba/sbin/samba -D
 936 ?        Ss     0:00  |   \_ /usr/local/samba/sbin/smbd -D --option=server role check:inhibit=yes --foreground
 940 ?        S      0:00  |       \_ /usr/local/samba/sbin/smbd -D --option=server role check:inhibit=yes --foreground
 941 ?        S      0:00  |       \_ /usr/local/samba/sbin/smbd -D --option=server role check:inhibit=yes --foreground
 943 ?        S      0:00  |       \_ /usr/local/samba/sbin/smbd -D --option=server role check:inhibit=yes --foreground
 924 ?        S      0:00  \_ /usr/local/samba/sbin/samba -D
 925 ?        S      0:00  \_ /usr/local/samba/sbin/samba -D
 ...
 935 ?        Ss     0:00  |   \_ /usr/local/samba/sbin/winbindd -D --option=server role check:inhibit=yes --foreground
 939 ?        S      0:00  |       \_ /usr/local/samba/sbin/winbindd -D --option=server role check:inhibit=yes --foreground
 ...

.. note:

    Samba Domain Controller do not support network browsing, and thus no ``nmbd`` processes are listed.

All ``samba``, ``smbd``, and ``winbindd`` processes must be child processes of one ``samba`` process.

If you do not see a process structure as displayed:

* Verify your Samba log files to locate the problem. For a detailed output, increase the log level. For details, see `#Setting_the_Samba_Log_Level|Setting the Samba Log Level`

* Start Samba interactively and watch the output:

 # samba -i

=================
DNS
=================

DNS Back End-specific Troubleshooting 
------------------------

See:

* `Samba_Internal_DNS_Back_End#Troubleshooting|Samba INTERNAL_DNS Back End - Troubleshooting`
* `BIND9_DLZ_DNS_Back_End#Troubleshooting|BIND9_DLZ DNS Back End - Troubleshooting`

Issues with DNS during DC join 
------------------------

DNS rcode name error
------------------------

There is a bug adding DNS entries while joining a domain [https://bugzilla.samba.org/show_bug.cgi?id=13298 13298] - note that this should only affect Samba v4.7 and later.

.. code-block::

    Adding DNS A record XXX.XXX.XXX.XXX for IPv4 IP: XX.XX.XX.XX
Join failed - cleaning up
ldb_wrap open of secrets.ldb
Could not find machine account in secrets database: Failed to fetch machine account password for MYDOMAIN from both secrets.ldb (Could not find entry to match filter: '(&(flatname=MYDOMAIN)(objectclass=primaryDomain))' base: 'cn=Primary Domains': No such object: dsdb_search at ../../source4/dsdb/common/util.c:4733) and from /var/lib/samba/private/secrets.tdb: NT_STATUS_CANT_ACCESS_DOMAIN_INFO
Deleted CN=RID Set,CN=MYDC,OU=Domain Controllers,DC=mydomain,DC=local
Deleted CN=MYDC,OU=Domain Controllers,DC=mydomain,DC=local
Deleted CN=dns-MYDC,CN=Users,DC=mydomain,DC=local
Deleted CN=NTDS Settings,CN=MYDC,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=mydomain,DC=local
Deleted CN=MYDC,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=mydomain,DC=local
ERROR(runtime): uncaught exception - (9003, 'WERR_DNS_ERROR_RCODE_NAME_ERROR')
    ile "/usr/lib64/python2.7/site-packages/samba/netcmd/__init__.py", line 185, in _run
    return self.run(*args, **kwargs)
    ile "/usr/lib64/python2.7/site-packages/samba/netcmd/domain.py", line 699, in run
    backend_store=backend_store)
    ile "/usr/lib64/python2.7/site-packages/samba/join.py", line 1535, in join_DC
    ctx.do_join()
    ile "/usr/lib64/python2.7/site-packages/samba/join.py", line 1436, in do_join
    ctx.join_add_dns_records()
    ile "/usr/lib64/python2.7/site-packages/samba/join.py", line 1178, in join_add_dns_records
    dns_partition=domaindns_zone_dn)
    ile "/usr/lib64/python2.7/site-packages/samba/samdb.py", line 1069, in dns_lookup
    dns_partition=dns_partition)

DNS zone does not exist
------------------------

.. code-block::

    ERROR(runtime): uncaught exception - (9601, 'WERR_DNS_ERROR_ZONE_DOES_NOT_EXIST')
    ile "/usr/lib/python2.7/dist-packages/samba/netcmd/__init__.py", line 176, in _run
    return self.run(*args, **kwargs)
    ile "/usr/lib/python2.7/dist-packages/samba/netcmd/domain.py", line 661, in run
    machinepass=machinepass, use_ntvfs=use_ntvfs, dns_backend=dns_backend)
    ile "/usr/lib/python2.7/dist-packages/samba/join.py", line 1474, in join_DC
    ctx.do_join()
    ile "/usr/lib/python2.7/dist-packages/samba/join.py", line 1384, in do_join
    ctx.join_add_dns_records()
    ile "/usr/lib/python2.7/dist-packages/samba/join.py", line 1138, in join_add_dns_records
    None) 

Name or zone errors like above may happen for a number of different reasons. In particular, the name error has been much more common (particularly against Windows). If the domain has been migrated from Windows 2000 or 2003 (including R2 variants and possibly 2008 non-R2), the DNS zones may not have been migrated correctly. Legacy DNS zone locations are not supported in Samba, which only supports fully replicated AD DNS zones (ForestDnsZones, DomainDnsZones). Where an error occurs indicating zone may not exist, it may be the case that the standard AD zone has not been created (despite it appearing to serve records from that location). A full re-import of your DNS database via PowerShell is one way to ensure that DNS records are only in the modern locations.

Assuming that these errors are not the result of migration issues, and are the result of issues with the running server, there is a workaround available:

.. warning:

   Performing these steps out of order may cause replication issues due to some objects being created twice.

1. During ``samba-tool`` domain join, specify the ``--dns-backend=NONE`` command line option.

2. Perform a ``samba-tool`` drs replicate of the DC=ForestDnsZones and DC=DomainDnsZones partitions with the options ``--local --full-sync``.

3. Run ``samba_upgradedns`` against the new DC database.

4. Perform a ``samba-tool`` `dbcheck` with the ``--cross-ncs`` option to correct discrepancies in the creation of the partitions. 

Optionally, you can now run ``samba-tool`` ldapcmp in order to verify that the databases are consistent (noting attributes ``msDs-masteredBy``, ``msDS-NC-Replica-Locations``, ``msDS-hasMasterNCs`` have been changed).

Other Windows compatibility issues
------------------------

For some more detail in regards to issues with domains migrated from Windows 2003 R2 or earlier:
* `Windows_2012_Server_compatibility#Pre-2003_functional_level| Windows Server Compatibility`

=================
SELinux
=================

For details, see `Troubleshooting_SELinux_on_a_Samba_AD_DC|Troubleshooting SELinux on a Samba AD DC`.

=================
Updating
=================

If you have any problems with your Active Directory (AD) domain controller (DC) after updating Samba, see: `Updating_Samba#Notable_Enhancements_and_Changes|Notable Enhancements and Changes`.

----
`Category:Active Directory`