Demoting a Samba AD DC
    <namespace>0</namespace>
<last_edited>2019-07-31T02:53:38Z</last_edited>
<last_editor>Timbeale</last_editor>

===============================

Introduction
===============================

In certain situations, it is necessary that you permanently remove a domain controller (DC) from Active Directory (AD). While for a regular domain member, you only delete the machine account entry, you have to demote a DC, to remove it from AD.

If a DC is not demoted correctly, your AD can get unstable. For example:
* replication failures can occur.
* the remaining DCs can slow down due to time outs and failed replication attempts.
* log ins on domain members can fail or take longer.

=================
Demoting an Online Domain Controller
=================

If the domain controller (DC) to remove is still working correctly:

* Log in locally to the DC to demote.

* Verify that the DC does not own any flexible single master operations (FSMO) roles. See `Transferring_and_Seizing_FSMO_Roles#Displaying_the_Current_FSMO_Role_Owners|Displaying the Current FSMO Role Owners`.

* In case that the DC owns one or more FSMO roles, transfer them to a different DC. See `Transferring_and_Seizing_FSMO_Roles#Transferring_an_FSMO_Role|Transferring an FSMO Role`.

* Optionally, display the objectGUID of the DC. For example, for the ``DC2`` host:

 # ldbsearch -H /usr/local/samba/private/sam.ldb '(invocationId=*)' --cross-ncs objectguid | grep -A1 DC2
 dn: CN=NTDS Settings,CN=DC2,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com
 objectGUID: c14a774f-9732-4ec2-b9fa-2156c95c4e48

* If you want to verify that all DNS entries were deleted ater you demoted the DC, you need to know the host name, IP address, and the objectGUID of the DC.

* Demote the DC:

 # samba-tool domain demote -Uadministrator
 Using DC1.samdom.example.com as partner server for the demotion
 Password for [SAMDOM\administrator]:
 Deactivating inbound replication
 Asking partner server DC1.samdom.example.com to synchronize from us
 Changing userControl and container
 Removing Sysvol reference: CN=DC2,CN=Enterprise,CN=Microsoft System Volumes,CN=System,CN=Configuration,DC=samdom,DC=example,DC=com
 Removing Sysvol reference: CN=DC2,CN=samdom.example.com,CN=Microsoft System Volumes,CN=System,CN=Configuration,DC=samdom,DC=example,DC=com
 Removing Sysvol reference: CN=DC2,CN=Domain System Volumes (SYSVOL share),CN=File Replication Service,CN=System,DC=samdom,DC=example,DC=com
 Removing Sysvol reference: CN=DC2,CN=Topology,CN=Domain System Volume,CN=DFSR-GlobalSettings,CN=System,DC=samdom,DC=example,DC=com
 Demote successful

* Stop the ``samba`` service.

* If this DC ran a DNS service for the Active Directory (AD) zones:
* * stop the DNS service, if you used the ``BIND9_DLZ`` DNS back end.
* * verify that domain members and DCs do no longer use this host to resolve the AD DNS zones.

=================
Demoting an Offline Domain Controller
=================

In certain situations, such as hardware failures, it is necessary to remove a domain controller (DC) from the domain, that is no longer accessible. In this case, demote the DC using a remaining working Samba DC.

.. warning:

   Only run this procedure if the DC to demote is no longer connected to the AD and you cannot demote it as described in `#Demoting_an_Online_Domain_Controller|Demoting an Online Domain Controller`. This ensures that all changes, like password changes, are replicated onto another DC. Otherwise such changes would be lost. You can get a list of changes by using `Samba-tool ldapcmp`.

To remotely demote an offline DC:

* Log in to a working Samba DC in the Active Directory (AD) forest.

* Verify that Samba 4.4 or later is installed:

 # samba --version

.. warning:

   You cannot demote an offline remote DC from a DC that runs Samba 4.4 or earlier. Update to Samba 4.4.0 or later before you continue. For details, see `Updating Samba`.

* Verify that the remote DC to demote does not own any flexible single master operations (FSMO) role. See `Transferring_and_Seizing_FSMO_Roles#Displaying_the_Current_FSMO_Role_Owners|Displaying the Current FSMO Role Owners`.

* * In case that the DC to demote owns one or more FSMO roles, seize them to the local DC. See `Transferring_and_Seizing_FSMO_Roles#Transferring_and_Seizing_FSMO_Roles#Seizing_a_FSMO_Role|Seizing an FSMO Role`.

* Verify that the DC to demote is turned off.

* Optionally, display the objectGUID of the DC. For example, for the ``DC2`` host:

 # ldbsearch -H /usr/local/samba/private/sam.ldb '(invocationId=*)' --cross-ncs objectguid | grep -A1 DC2
 dn: CN=NTDS Settings,CN=DC2,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com
 objectGUID: c14a774f-9732-4ec2-b9fa-2156c95c4e48

* If you want to verify that all DNS entries were deleted ater you demoted the DC, you need to know the host name, IP address, and the objectGUID of the DC.

* Demote the remote DC. For example, to demote ``DC2``:

 # samba-tool domain demote --remove-other-dead-server=DC2
 Removing nTDSConnection: CN=04baf417-eb41-4f31-a5f1-c739f0e92b1b,CN=NTDS Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com
 Removing nTDSDSA: CN=NTDS Settings,CN=DC2,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com (and any children)
 Removing RID Set: CN=RID Set,CN=DC2,OU=Domain Controllers,DC=samdom,DC=example,DC=com
 Removing computer account: CN=DC2,OU=Domain Controllers,DC=samdom,DC=example,DC=com (and any child objects)
 Removing Samba-specific DNS service account: CN=dns-DC2,CN=Users,DC=samdom,DC=example,DC=com
 updating samdom.example.com keeping 3 values, removing 1 values
 updating DC=_kerberos._tcp.Default-First-Site-Name._sites,DC=samdom.example.com,CN=MicrosoftDNS,DC=DomainDnsZones,DC=samdom,DC=example,DC=com keeping 1 values, removing 1 values
 updating DC=_ldap._tcp.Default-First-Site-Name._sites,DC=samdom.example.com,CN=MicrosoftDNS,DC=DomainDnsZones,DC=samdom,DC=example,DC=com keeping 1 values, removing 1 values
 updating DC=_gc._tcp.Default-First-Site-Name._sites,DC=samdom.example.com,CN=MicrosoftDNS,DC=DomainDnsZones,DC=samdom,DC=example,DC=com keeping 1 values, removing 1 values
 updating DC=_kerberos._tcp,DC=samdom.example.com,CN=MicrosoftDNS,DC=DomainDnsZones,DC=samdom,DC=example,DC=com keeping 1 values, removing 1 values
 updating DC=_kerberos._udp,DC=samdom.example.com,CN=MicrosoftDNS,DC=DomainDnsZones,DC=samdom,DC=example,DC=com keeping 1 values, removing 1 values
 updating DC=_kpasswd._tcp,DC=samdom.example.com,CN=MicrosoftDNS,DC=DomainDnsZones,DC=samdom,DC=example,DC=com keeping 1 values, removing 1 values
 updating DC=_kpasswd._udp,DC=samdom.example.com,CN=MicrosoftDNS,DC=DomainDnsZones,DC=samdom,DC=example,DC=com keeping 1 values, removing 1 values
 updating DC=_ldap._tcp,DC=samdom.example.com,CN=MicrosoftDNS,DC=DomainDnsZones,DC=samdom,DC=example,DC=com keeping 1 values, removing 1 values
 updating DC=_gc._tcp,DC=samdom.example.com,CN=MicrosoftDNS,DC=DomainDnsZones,DC=samdom,DC=example,DC=com keeping 1 values, removing 1 values
 updating DC=_ldap._tcp.4d5258b9-0cd7-4d78-bdd7-99ebe6b19751.domains,DC=_msdcs.samdom.example.com,CN=MicrosoftDNS,DC=ForestDnsZones,DC=samdom,DC=example,DC=com keeping 1 values, removing 1 values
 updating DC=_kerberos._tcp.Default-First-Site-Name._sites.dc,DC=_msdcs.samdom.example.com,CN=MicrosoftDNS,DC=ForestDnsZones,DC=samdom,DC=example,DC=com keeping 1 values, removing 1 values
 updating DC=_ldap._tcp.Default-First-Site-Name._sites.dc,DC=_msdcs.samdom.example.com,CN=MicrosoftDNS,DC=ForestDnsZones,DC=samdom,DC=example,DC=com keeping 1 values, removing 1 values
 updating DC=_ldap._tcp.Default-First-Site-Name._sites.gc,DC=_msdcs.samdom.example.com,CN=MicrosoftDNS,DC=ForestDnsZones,DC=samdom,DC=example,DC=com keeping 1 values, removing 1 values
 updating DC=c14a774f-9732-4ec2-b9fa-2156c95c4e48,DC=_msdcs.samdom.example.com,CN=MicrosoftDNS,DC=ForestDnsZones,DC=samdom,DC=example,DC=com keeping 0 values, removing 1 values
 updating DC=_kerberos._tcp.dc,DC=_msdcs.samdom.example.com,CN=MicrosoftDNS,DC=ForestDnsZones,DC=samdom,DC=example,DC=com keeping 1 values, removing 1 values
 updating DC=_ldap._tcp.dc,DC=_msdcs.samdom.example.com,CN=MicrosoftDNS,DC=ForestDnsZones,DC=samdom,DC=example,DC=com keeping 1 values, removing 1 values
 updating DC=_ldap._tcp.gc,DC=_msdcs.samdom.example.com,CN=MicrosoftDNS,DC=ForestDnsZones,DC=samdom,DC=example,DC=com keeping 1 values, removing 1 values
 Removing Sysvol reference: CN=DC2,CN=Enterprise,CN=Microsoft System Volumes,CN=System,CN=Configuration,DC=samdom,DC=example,DC=com
 Removing Sysvol reference: CN=DC2,CN=samdom.example.com,CN=Microsoft System Volumes,CN=System,CN=Configuration,DC=samdom,DC=example,DC=com
 Removing Sysvol reference: CN=DC2,CN=Domain System Volumes (SYSVOL share),CN=File Replication Service,CN=System,DC=samdom,DC=example,DC=com
 Removing Sysvol reference: CN=DC2,CN=Topology,CN=Domain System Volume,CN=DFSR-GlobalSettings,CN=System,DC=samdom,DC=example,DC=com

* {{Imbox
| type = warning
| text = You must not reconnect a DC to the network, that was demoted remotely. Your AD can get inconsistent.

* If the demoted DC ran a DNS service for the Active Directory (AD) zones, verify that domain members and DCs no longer use this host to resolve the AD DNS zones.

=================
Verifying the Demotion
=================

To manually verify that the domain controller (DC) was successfully demoted:

.. warning:

   The steps described in this section, do not replace the official demote procedures described in the previous sections. The steps in this section are only to verify and to manually remove remaining entries, if the official demote process failed.

* Log in to a Windows domain member using an account that is member of the ``Domain Admins`` group, such as the AD domain Administrator account.

* Install the Remote Server Administration Tools (RSAT). For details, see `Installing RSAT`.

* Open the ``Active Directory Users and Computers`` application.

* * Navigate to the ``Domain Controllers`` entry and verify that the demoted DC was removed. For example:
* `Image:ADUC_Domain_Controllers.png`
* * If the entry is still listed, you can manually remove it:
* :* Right-click to the DC entry and select ``Delete``
* :* Click ``Yes`` to confirm.
* :* Select ``Delete this Domain Controller anyway. It is permanently offline and can no longer be removed using the removal wizard.`` and click ``OK``.
* :* If the DC is a global catalog server, click ``Yes`` to confirm.

* Open the ``Active Directory Sites and Services`` application and verify that the demoted DC is no longer listed in any Active Directory (AD) site entry. For example:
* `Image:ADSS_Domain_Controllers.png`
* * If the entry is still listed, you can manually remove it:
* :* Right-click to the DC entry and select ``Delete``
* :* Click ``Yes`` to confirm.

* Open the ``DNS`` application and verify that the DC's host name, IP address, and objectGUID is no longer used in any DNS entry in any AD DNS zone. For example:
* `Image:DNS_Domain_Controllers.png`
* * If entries are still listed, you can manually remove them:
* :* Right-click to the entry and select ``Delete``
* :* Click ``Yes`` to confirm.

----
`Category:Active Directory`