Time Synchronisation
    <namespace>0</namespace>
<last_edited>2021-05-02T19:04:29Z</last_edited>
<last_editor>Abartlet</last_editor>

===============================

Introduction
===============================

In an Active Directory (AD) you must have an accurate time synchronisation. For example, Kerberos requires correct time stamps to prevent replay attacks and the AD uses the time to resolve replication conflicts. The default maximum allowed time deviation in an AD is 5 minutes. If a domain member or domain controller (DC) has a higher or lower time difference, the access is denied. As a result, a user cannot access shares or query the directory.

Samba supports the ``ntpd`` from http://ntp.org and ``chrony`` from https://chrony.tuxfamily.org/ . The daemon synchronises the time with external sources and enables clients to retrieve the time from the server running the daemon.

**Recommended best practise**

 internet time server
           ^
           |
           |
   PDC Emulator DC
    ^         ^
    |         |
    |         | 
 Other DC <----Workstation  

From the above, you can see that only the PDC emulator DC gets its time from external time servers, all other DC's get their time from the PDC emulator, all other workstations get their time from any DC.
There is however a problem with this, Windows clients get their time from the PDC emulator DC and if this goes offline, they will not use the other DC's and the other DC's will be looking for the PDC emulator DC.
As a workaround for this, set the same external time servers on all DC's, then if the PDC emulator goes offline and cannot easily be restarted, transfer or seize the PDC emulator role to another DC. 

By default domain joined Windows clients synchronize their clock via NT5DS with AD-DC's.
The NT5DS protocol uses digital signatures. These can be provided by Samba if the time server runs on the same server, and is configured as described on this page (with options mssntp and ntpsigndsocket).
Alternatively you could configure all machines to do standard ntp, but NT5DS is recommended.

Note that authenticated time synchronisation with Windows 2000 clients is not supported.

Before deciding which time server software to install, You can see a comparison of ntp and chrony here https://chrony.tuxfamily.org/comparison.html

For example, the NTP modes table.  (last date checked 7 June 2018.)

* {| class="wikitable"
!
!chrony
!ntp
!openntpd
|-
|Broadcast server
|Yes
|Yes
|No
|-
|Broadcast client
|No
|Yes
|No
|-
|Multicast server
|No
|Yes
|No
|-
|Multicast client
|No
|Yes
|No
|-
|Manycast server
|No
|Yes
|No
|-
|Manycast client
|No
|Yes
|No
|}

.. note:

    You must set up a time server on all Samba AD DC's

=================
Configuring Time Synchronisation on a DC
=================

Requirements 
------------------------

* ntpd >= 4.2.6 from http://www.ntp.org, compiled with enabled signed ntp support (``--enable-ntp-signd`` when building NTP)

Or

* chrony >= 3.0 from https://chrony.tuxfamily.org, compiled with enabled signed ntp support (``--enable-ntp-signd`` when building chrony)

With ntpd 
------------------------

* Verify the socket permissions on your domain controller (DC). The time daemon must have read permissions in the ``ntp_signed`` directory. To list the permissions, enter:

 # ls -ld /usr/local/samba/var/lib/ntp_signd/
 drwxr-x--- 2 root ntp 4096  1. May 09:30 /usr/local/samba/var/lib/ntp_signd/

* To set the permissions, run:
 # chown root:ntp /usr/local/samba/var/lib/ntp_signd/
 # chmod 750 /usr/local/samba/var/lib/ntp_signd/

Typically, the ``ntpd`` daemon read its configuration from the ``/etc/ntpd.conf`` file.

The following is a minimum ``ntpd.conf`` file that synchronises the time with three external NTP server and enables clients to query the time using signed NTP requests:

 # Local clock. Note that is not the "localhost" address!
 server 127.127.1.0
 fudge  127.127.1.0 stratum 10

 # Where to retrieve the time from
 server 0.pool.ntp.org     iburst prefer
 server 1.pool.ntp.org     iburst prefer
 server 2.pool.ntp.org     iburst prefer

 driftfile       /var/lib/ntp/ntp.drift
 logfile         /var/log/ntp
 ntpsigndsocket  /usr/local/samba/var/lib/ntp_signd/

 # Access control
 # Default restriction: Allow clients only to query the time
 restrict default kod nomodify notrap nopeer mssntp

 # No restrictions for "localhost"
 restrict 127.0.0.1

 # Enable the time sources to only provide time to this host
 restrict 0.pool.ntp.org   mask 255.255.255.255    nomodify notrap nopeer noquery
 restrict 1.pool.ntp.org   mask 255.255.255.255    nomodify notrap nopeer noquery
 restrict 2.pool.ntp.org   mask 255.255.255.255    nomodify notrap nopeer noquery

If you are running the DC in a VM, you should consider adding ``tinker panic 0`` to the end of the ``ntp.conf``. This tells NTP not to panic and exit, no matter what the time offset is. This is recommended because 
virtual machines have no physical clock and can be paused at anytime and started back up hours later.
For further information see: https://www.redhat.com/en/blog/avoiding-clock-drift-vms 

For further information about the ``ntpd`` access control, see http://support.ntp.org/bin/view/Support/AccessRestrictions.

If you have SELinux enabled on your server, see `Time_Synchronisation_-_SELinux_Labeling_and_Policy|Time Synchronisation - SELinux Labeling and Policy`.

With chrony 
------------------------

* Verify the socket permissions on your domain controller (DC). The time daemon must have read permissions in the ``ntp_signed`` directory. To list the permissions, enter:

 # ls -ld /usr/local/samba/var/lib/ntp_signd/
 drwxr-x--- 2 root _chrony 4096  1. May 09:30 /usr/local/samba/var/lib/ntp_signd/

* To set the permissions, run:
 # chown root:_chrony /usr/local/samba/var/lib/ntp_signd/
 # chmod 750 /usr/local/samba/var/lib/ntp_signd/

Typically, the ``chrony`` daemon read its configuration from the ``/etc/chrony/chrony.conf`` file.

The following is a minimum ``chrony.conf`` file that synchronises the time with three external NTP servers and enables clients to query the time using signed NTP requests:

 # Welcome to the chrony configuration file. See chrony.conf(5) for more
 # information about usuable directives.

 # This directive specify the location of the file containing ID/key pairs for
 # NTP authentication.
 keyfile /etc/chrony/chrony.keys

 # This directive specify the file into which chronyd will store the rate
 # information.
 driftfile /var/lib/chrony/chrony.drift

 # Uncomment the following line to turn logging on.
 #log tracking measurements statistics

 # Log files location.
 logdir /var/log/chrony

 # Stop bad estimates upsetting machine clock.
 maxupdateskew 100.0

 # This directive tells 'chronyd' to parse the 'adjtime' file to find out if the
 # real-time clock keeps local time or UTC. It overrides the 'rtconutc' directive.
 hwclockfile /etc/adjtime

 # This directive enables kernel synchronisation (every 11 minutes) of the
 # real-time clock. Note that it can’t be used along with the 'rtcfile' directive.
 rtcsync

 # Step the system clock instead of slewing it if the adjustment is larger than
 # one second, but only in the first three clock updates.
 makestep 1 3

 bindcmdaddress 192.168.0.2 # ipaddress of this DC

 # The source, where we are receiving the time from
 server 0.pool.ntp.org     iburst
 server 1.pool.ntp.org     iburst
 server 2.pool.ntp.org     iburst

 allow 192.168.0.0/24 # dns netmask

 ntpsigndsocket  /usr/local/samba/var/lib/ntp_signd

=================
Configuring Time Synchronisation on a Unix Domain Member
=================

Requirements 
------------------------

* ntpd from http://www.ntp.org.

Or

* chrony >= 3.0 from https://chrony.tuxfamily.org

Or

* systemd-timesyncd >= a recent linux distro that supports and runs with systemd.

With ntpd 
------------------------

Typically, the ``ntpd`` daemon read its configuration from the ``/etc/ntpd.conf`` file.

The following is a minimum conf file that synchronises the time with the Samba Active Directory (AD) domain controllers (DC) ``DC1`` and ``DC2`` and does not provide time services for other hosts.

 # Local clock. Note that is not the "localhost" address!
 server 127.127.1.0
 fudge  127.127.1.0 stratum 10
     # Where to retrieve the time from
 server DC1.samdom.example.com     iburst prefer
 server DC2.samdom.example.com     iburst

 driftfile /var/lib/ntp/ntp.drift
 logfile   /var/log/ntp

 # Access control
 # Default restriction: Disallow everything
 restrict default ignore

 # No restrictions for "localhost"
 restrict 127.0.0.1

 # Enable the time sources only to only provide time to this host
 restrict DC1.samdom.example.com   mask 255.255.255.255    nomodify notrap nopeer noquery
 restrict DC2.samdom.example.com   mask 255.255.255.255    nomodify notrap nopeer noquery

If you are running the Unix Domain Member in a VM, you should consider adding ``tinker panic 0`` to the end of the ``ntp.conf``. This tells NTP not to panic and exit, no matter what the time offset is. This is recommended because virtual machines have no physical clock and can be paused at any time and started back up hours later.
For further information see: https://www.redhat.com/en/blog/avoiding-clock-drift-vms

For further information about the ``ntpd`` access control, see http://support.ntp.org/bin/view/Support/AccessRestrictions.

With chrony 
------------------------

Typically, the ``chrony`` daemon read its configuration from the ``/etc/chrony/chrony.conf`` file.

The following is a minimum conf file that synchronises the time with the Samba Active Directory (AD) domain controllers (DC) ``DC1`` and ``DC2`` and does not provide time services for other hosts.

 # Welcome to the chrony configuration file. See chrony.conf(5) for more
 # information about usuable directives.

 # This directive specify the location of the file containing ID/key pairs for
 # NTP authentication.
 keyfile /etc/chrony/chrony.keys

 # This directive specify the file into which chronyd will store the rate
 # information.
 driftfile /var/lib/chrony/chrony.drift

 # Uncomment the following line to turn logging on.
 #log tracking measurements statistics

 # Log files location.
 logdir /var/log/chrony

 # Stop bad estimates upsetting machine clock.
 maxupdateskew 100.0

 # This directive tells 'chronyd' to parse the 'adjtime' file to find out if the
 # real-time clock keeps local time or UTC. It overrides the 'rtconutc' directive.
 hwclockfile /etc/adjtime

 # This directive enables kernel synchronisation (every 11 minutes) of the
 # real-time clock. Note that it can’t be used along with the 'rtcfile' directive.
 rtcsync

 # Step the system clock instead of slewing it if the adjustment is larger than
 # one second, but only in the first three clock updates.
 makestep 1 3

 bindcmdaddress 192.168.0.6 # ipaddress of this Unix domain member

 # The source, where we are receiving the time from
 server DC1.samdom.example.com    iburst
 server DC2.samdom.example.com    iburst

With systemd-timesyncd 
------------------------

There are a few ways to setup systemd-timesyncd.

The ``systemd-timesynced`` daemon reads its configuration from the ``/etc/systemd/timesyncd.conf`` file,
or from your network configuration defined in systemd's .network file ``/etc/systemd/network/your.network``,
or by dhcp settings. 

Option 1: Using the ``/etc/systemd/timesyncd.conf`` file.
Enable the following. 
 [Time]
 NTP=DC1.samdom.example.com DC2.samdom.example.com
 FallbackNTP=the.same.ntp-server.as.your.dc.points.to one-extra.ntp-server.as.your.dc.points.to

In this example the fallback NTP servers are also used, this is not mandatory. 

Option 2: set your time servers in your network configuration (for example: ``/etc/systemd/network/20-wired-dev1.network``).
 #/etc/systemd/network/20-wired-dev1.network
 [Network]
 NTP=dc1.samdom.example.com
 NTP=dc2.samdom.example.com

 [Address]
 Address=192.168.0.200/24

 [Route]
 Destination=0.0.0.0/0
 Gateway=192.168.0.1

After the changes enable and start the time daemon.
 systemctl enable systemd-timesyncd
 systemctl start systemd-timesyncd

Check the service status with: 
 systemctl status systemd-timesyncd

Check the journaling logs with: 
 journalctl -u systemd-timesyncd

Check the time daemon with:
 timedatectl status

If required, get the list of timezones: 
 timedatectl list-timezones
and apply the new time-zone. 
 timedatectl set-timezones Europe/Amsterdam

Why use the systemd time daemon? It works fine for a member server and doesn't require the installation of any extra software.

.. note:

    You can find the documentation for ``systemd-timesynced`` here: https://www.freedesktop.org/software/systemd/man/systemd-timesyncd.service.html

=================
Configuring Time Synchronisation on a Windows Domain Member
=================

The following describes the basics of how to configure time synchronisation on a Windows domain member. For further details, see your Microsoft Windows documentation.

Default Time Source 
------------------------

Windows AD domain members will use any DC as their default time source. If you have set up ntp on the DC as described on this page, you usually do not need to reconfigure the clients. Alternative configuration options for the clients are described below.

For more information about the time synchronisation and hierarchy in an AD, see http://technet.microsoft.com/en-us/library/cc773013%28v=ws.10%29.aspx#w2k3tr_times_how_izcr.

Setting User Defined Time Sources and Options  
------------------------

To create a group policy object (GPO) to for setting a user defined NTP time source and options:

* Log in to a computer using an account that is allowed you to edit group policies, such as the AD domain ``Administrator`` account.

* Open the ``Group Policy Management Console``. If you are not having the Remote Server Administration Tools (RSAT) installed on this computer, see `Installing RSAT|Installing RSAT`.

* Right-click to your AD domain and select ``Create a GPO in this domain, and Link it here``.

* Enter a name for the GPO, such as ``Time Sources``. The new GPO is shown below the domain entry.

* Right-click to the newly-created GPO and select ``Edit`` to open the ``Group Policy Management Editor``.

* Navigate to the ``Computer Configuration`` &rarr; ``Policies`` &rarr; ``Administrative Templates`` &rarr; ``System`` &rarr; ``Windows Time Service`` &rarr; ``Time Providers`` entry, and double-click ``Configure Windows NTP Client`` to configure the policy:
* * Enable the policy and set the following options:
* :* Enter the fully-quallified domain name (FQDN) of the NTP server to the ``NtpServer`` field and and append the ``0x9`` flag. For example:
* ::`Image:GPO_Windows_NTP_Client_Options.png`
* :: To enter multiple server, separate the individual entries using a space.
* :* Keep the ``NT5DS`` type setting.
* :* Update the additional parameters, if necessary.
* * Click ``OK`` to save the settings.

* Navigate to the ``Computer Configuration`` &rarr; ``Policies`` &rarr; ``Administrative Templates`` &rarr; ``System`` &rarr; ``Windows Time Service`` &rarr; ``Time Providers`` entry, and double-click ``Enable Windows NTP Client`` to configure the policy:
* * Enable the policy.
* * Click ``OK`` to save the settings.

* Close the ``Group Policy Management Editor``.

* Close the ``Group Policy Management Console``.

Notes:

* The default Type NT5DS ignores the parameter NtpServer, and syncs with the DC.

* If ntpd on your DC is not configured for mssntp with ntpsigndsocket, use Type NTP.

* If a client will not be able to connect to the DC for a long time (for example a laptop), use Type AllSync and set NtpServer to "time.windows.com,0x9". This will cause the client to try both NT5DS to your DC, and NTP to NtpServer.