Verifying the Directory Replication Statuses
    <namespace>0</namespace>
<last_edited>2017-05-17T17:06:15Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

===============================

Introduction
===============================

Directory replication is important in an Active Directory (AD) forest with multiple domain controllers (DC) for fail-over and load balancing.

.. warning:

   To optimize replication latency and cost, the knowledge consistency checker (KCC) on Samba and Windows DCs do not create a full-meshed replication topology between all DCs. For further details, see `The Samba KCC`.

For replication agreements the KCCs auto-create, the following containers are replicated by default:
* ``DC=*Forest_Root_Domain*``
* ``CN=Configuration,DC=*Forest_Root_Domain*``
* ``CN=Schema,CN=Configuration,DC=*Forest_Root_Domain*``
* ``DC=ForestDnsZones,DC=*Forest_Root_Domain*``
* ``DC=DomainDnsZones,DC=*Forest_Root_Domain*``

Note that if you join a new DC, it can take up to 15 minutes until the KCCs create the connection objects and inbound and outbound replication starts for all containers.

=================
Displaying the Replication Statuses on a Samba DC
=================

The ``samba-tool drs showrepl`` command displays the inbound and outbound replication agreements with other DC in the AD forest. The output is reported from the viewpoint of the Samba DC, on which you run the command.

 # samba-tool drs showrepl
 Default-First-Site-Name\DC2
 DSA Options:01
 DSA object GUID:-9732-4ec2-b9fa-2156c95c4e48
 DSA invocationId:-6868-4dd9-9460-33dea4b6b87b

 INBOUND NEIGHBORS =
===============================

------------------------

 CN=Schema,CN=Configuration,DC=samdom,DC=example,DC=com
        Default-First-Site-Name\DC1 via RPC
                DSA object GUID:-6612-4b15-aa8c-9ec371e8994f
                Last attempt @ Sat May 13 02:7 :uccessful
                0 consecutive failure(s).
                Last success @ Sat May 13 02:7 :

 DC=DomainDnsZones,DC=samdom,DC=example,DC=com
        Default-First-Site-Name\DC1 via RPC
                DSA object GUID:-6612-4b15-aa8c-9ec371e8994f
                Last attempt @ Sat May 13 02:7 :uccessful
                0 consecutive failure(s).
                Last success @ Sat May 13 02:7 :

 CN=Configuration,DC=samdom,DC=example,DC=com
        Default-First-Site-Name\DC1 via RPC
                DSA object GUID:-6612-4b15-aa8c-9ec371e8994f
                Last attempt @ Sat May 13 02:7 :uccessful
                0 consecutive failure(s).
                Last success @ Sat May 13 02:7 :

 DC=ForestDnsZones,DC=samdom,DC=example,DC=com
        Default-First-Site-Name\DC1 via RPC
                DSA object GUID:-6612-4b15-aa8c-9ec371e8994f
                Last attempt @ Sat May 13 02:7 :uccessful
                0 consecutive failure(s).
                Last success @ Sat May 13 02:7 :

 DC=samdom,DC=example,DC=com
        Default-First-Site-Name\DC1 via RPC
                DSA object GUID:-6612-4b15-aa8c-9ec371e8994f
                Last attempt @ Sat May 13 02:7 :uccessful
                0 consecutive failure(s).
                Last success @ Sat May 13 02:7 :

 OUTBOUND NEIGHBORS =
===============================

------------------------

 CN=Schema,CN=Configuration,DC=samdom,DC=example,DC=com
        Default-First-Site-Name\DC1 via RPC
                DSA object GUID:-6612-4b15-aa8c-9ec371e8994f
                Last attempt @ NTTIME(0) was successful
                0 consecutive failure(s).
                Last success @ NTTIME(0)

 DC=DomainDnsZones,DC=samdom,DC=example,DC=com
        Default-First-Site-Name\DC1 via RPC
                DSA object GUID:-6612-4b15-aa8c-9ec371e8994f
                Last attempt @ NTTIME(0) was successful
                0 consecutive failure(s).
                Last success @ NTTIME(0)

 CN=Configuration,DC=samdom,DC=example,DC=com
        Default-First-Site-Name\DC1 via RPC
                DSA object GUID:-6612-4b15-aa8c-9ec371e8994f
                Last attempt @ NTTIME(0) was successful
                0 consecutive failure(s).
                Last success @ NTTIME(0)

 DC=ForestDnsZones,DC=samdom,DC=example,DC=com
        Default-First-Site-Name\DC1 via RPC
                DSA object GUID:-6612-4b15-aa8c-9ec371e8994f
                Last attempt @ NTTIME(0) was successful
                0 consecutive failure(s).
                Last success @ NTTIME(0)

 DC=samdom,DC=example,DC=com
        Default-First-Site-Name\DC1 via RPC
                DSA object GUID:-6612-4b15-aa8c-9ec371e8994f
                Last attempt @ NTTIME(0) was successful
                0 consecutive failure(s).
                Last success @ NTTIME(0)

 KCC CONNECTION OBJECTS =
===============================

------------------------

 Connection --
        Connection name:-1654-4a02-8e11-f0ea120b60cc
        Enabled        :
        Server DNS name :Tsamdom.example.com
        Server DN name  :Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com
                TransportType:
                options:01
 Warning:plicated for Connection!

For further details about the ``No NC replicated for Connection!`` warning, see `FAQ#What_does_Warning:plicated_for_Connection.21_Mean.3F|FAQ: What do:: No NC replicated:ction! Means`.

=================
Displaying the Replication Statuses on a Windows DC
=================

Inbound Replication 
------------------------

To display the inbound replication on a Windows DC:

* Open a command prompt.

* Use the ``repadmin`` utility to display the inbound connection statuses:

 > repadmin /showrepl

 Repadmin:command /showrepl against full DC localhost
 Default-First-Site-Name\Windows-DC
 DSA Options:
 Site Options:
 DSA object GUID:-61fe-4f6f-985a-ecbad14d89f4
 DSA invocationID:-eebe-4cca-8968-fcc8d0b20d97

 INBOUND NEIGHBORS =================================

 DC=samdom,DC=example,DC=com
     Default-First-Site-Name\Samba-DC via RPC
         DSA object GUID:-6612-4b15-aa8c-9ec371e8994f
         Last attempt @ 2017-05-13 00: s:DOOTT

 CN=Configuration,DC=samdom,DC=example,DC=com
     Default-First-Site-Name\Samba-DC via RPC
         DSA object GUID:-6612-4b15-aa8c-9ec371e8994f
         Last attempt @ 2017-05-13 00: s:DOOTT

 CN=Schema,CN=Configuration,DC=samdom,DC=example,DC=com
     Default-First-Site-Name\Samba-DC via RPC
         DSA object GUID:-6612-4b15-aa8c-9ec371e8994f
         Last attempt @ 2017-05-13 00: s:DOOTT

 DC=DomainDnsZones,DC=samdom,DC=example,DC=com
     Default-First-Site-Name\Samba-DC via RPC
         DSA object GUID:-6612-4b15-aa8c-9ec371e8994f
         Last attempt @ 2017-05-13 00: s:DOOTT

 DC=ForestDnsZones,DC=samdom,DC=example,DC=com
     Default-First-Site-Name\Samba-DC via RPC
         DSA object GUID:-6612-4b15-aa8c-9ec371e8994f
         Last attempt @ 2017-05-13 00: s:DOOTT

Outbound Replication 
------------------------

Windows does not support displaying outbound replication connection statuses. To work around the problem, you can display the statuses of the inbound connections on Samba DCs the Windows DC replicates to:

* Log in to a Samba DC.

* Search the AD for all replication partners of the Windows DC. For example, to display the replication partners of the DC named ``Windows-DC``:

 # ldbsearch -H /usr/local/samba/private/sam.ldb '(fromServer=*CN=<u>Windows-DC</u>*)' --cross-ncs dn
 # record 1
 dn:afb-cb5a-49ce-8f74-be348c471348,CN=NTDS Settings,CN=<u>Samba-DC</u>,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com

 # returned 1 records
 # 1 entries
 # 0 referrals

* revious example, one replication partner is returned (host name: <cod:-DC``). The host name of replication partner is part of the returned distinguished name (DN).

* Log on to every Samba DC retrieved in the previous step and use ``samba-tool`` to display the directory replication status. See `#Displaying_the_Replication_Statuses_on_a_Samba_DC|Displaying the Replication Statuses on a Samba DC`.

* hat each directory container to replicate is listed for the Windows DC in the ``INBOUND NEIGHBORS`` section on the Samba DC and the statuses are ``successful``.

----
`Category:rectory`