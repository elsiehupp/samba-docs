Active Directory Naming FAQ
    <namespace>0</namespace>
<last_edited>2021-01-20T22:47:20Z</last_edited>
<last_editor>Bjacke</last_editor>

===============================

Introduction
===============================

Picking an Active Directory domain name is one of the most important steps in setting up a domain. And one that is important to get right the first time, as changing it later is a non-trivial task. There have been religious debates on this issue, and MS recommendations have changed over time.

==========================

What is an Active Directory domain?
==========================

An `Terms_and_Abbreviations#Active_Directory_.28AD.29|Active Directory (AD)` domain is basically the same as an internet `Terms_and_Abbreviations#Domain|domain`. It "defines a realm of administrative autonomy, authority, and control" for a group of computers. Active Directory Domain names are controlled by the same set of rules and principles, that govern traditional `Terms_and_Abbreviations#Domain_Name_System_.28DNS.29|Domain Name Systems (DNS)`. Thus in order to understand Active Directory domain naming it is important to understand DNS.

====
===============================

How DNS Works
====
===============================

DNS is the system, used to translate names into other types of data for computer consumption, such as an IP address (an A record) or any of a number of other record types. An often used analogy is that of a phone book. A DNS server serves as a sort of computer phone book that can be used to rapidly translate computer names to IPs (or other sorts of data). However this phone book is of gigantic size (there are ~4 billion IPv4 address alone) and constantly changing so DNS uses a hierarchical system to determine which phone book, or name server, is responsible for which sets of addresses.

Virtually every time a domain name is resolved, be it by a web-browser or some other source, a DNS inquiry is made. Actual name resolution in Windows is a somewhat complex topic, but in general a Windows PC checks its local name, then its hosts file, then makes a DNS request (unless the name is in its cache). The DNS name servers then looks up the address and returns a response or forwards the request on to another server for resolution if it is a recursive DNS server. Active Directory DNS servers are typically recursive and serve all DNS requests for PCs within the domain. DNS names do not necessarily resolve to only a single address either, they can correspond to multiple other records and can even refer to other records recursively using records like a CNAME.

DNS requests go out primarily over UDP. With UDP there is no guarantee of message or response delivery, and since users expect name resolution to be fast and seamless, timeout for a DNS reply is short (3 seconds is the Windows, 5 seconds the Linux default). For the most part, DNS is fast and reliable, but it is important to remember that a lookup failure from time to time is not-unexpected.

DNS Hierarchy 
------------------------

A DNS name is segmented into different parts by dots. Each segment represent a different parts of the hierarchy, called a domain. The first segment from the right (the .com, .net, etc.) is known as the top-level domain (TLD), every domain beneath that level is known as a subdomain. These subdomains can themselves contain subdomains, up to 127 layers deep. So "example.com" is a subdomain of the .com domain, and can itself contain multiple subdomains like "samdom.example.com". A DNS name that resolves to a specific device or piece of data is known as a `Terms_and_Abbreviations#Fully_qualified_Domain_Name_.28FQDN.29|full qualified domain name (FQDN)`. Technically a FQDN should also specify the root zone which is a blank domain name specified by a dot at the end of the domain name, such as "www.samdom.example.com.", but in practice the root zone is often omitted.

Each DNS name server is responsible for different parts of this hierarchy, known as a zone. A zone comprises a DNS name, such as "samdom.example.com", and all the names beneath that level, such as www.samdom.example.com", "ftp.samdom.example.com", or "server.samdom.example.com". DNS servers give authoritative, or final, answers to request with the zones they are configured to be responsible for. More details on the process can be found on the [http://ikipedia.org/wiki/DNS/esol/anism Wikipedia DNS page].

Why This Matters 
------------------------

The domain name, you select for your Active Directory domain, is also going to be the primary domain, that the AD DNS server is authoritative for. All of your PCs in Active Directory are going to have a name within this domain. For Active Directory to work properly computers that are a part of it need all of these names to resolve correctly. This means it must serve as the DNS server for all the PCs that exists within your domain. Problems can arise when there is a DNS conflict, where two DNS servers resolve the same name to two different addresses. A DNS conflict is not the same as say an IP address conflict, it does not prevent network traffic from flowing, but when one happens you can often have the problem where traffic is flowing to addresses you don't intend, or names that you want resolved aren't resolving at all.

For example, if you named your AD domain "samdom.example.com", your AD DNS server would naturally be authoritative for all requests at or below the "samdom.example.com" level of the hierarchy. It would directly respond to all requests for names within that domain such as "workstation.samdom.example.com" or "server.samdom.example.com", or any other name configured within the DNS server. If you requested www.samba.org, that would pose no problem either, it would see that it is not authoritative for the samba.org domain and forward the request to the server that is responsible for the name, then return the answer to you.

But what if you also have an external website such as "www.samdom.example.com", probably handled by a different external DNS server? Well, when you request that name, the DNS server is going to see that it is responsible for the "samdom.example.com" domain, and look up a response for you. If it doesn't have one, it is not going to forward the request to any other server for resolution, because it believes it is the final authority for this domain. And so it will return you a "no such name exists response", or NXDOMAIN.

Security Implications 
------------------------

While technically not an aspect of domain naming per-se, the security implications of DNS are very important to consider. Your AD DNS server will contain a list of all PCs within your network, and while most DNS servers do not allow just anyone to request a full listing of domain names (also known as a zone transfer), allowing devices outside your domain to resolve names from this list still represents an unnecessary exposure of internal information and a possible security risk. In addition most AD DNS servers are recursive, and running a recursive DNS server on the internet has important security implications beyond the scope of this documentation.

Similarly, you should not send DNS requests for internal names outside of the internal network. Even if you trust the DNS server you are sending them to, DNS is not encrypted, and so potential for intercept exists along every router that passes the traffic, this represents a serious security risk. As an attacker who controls this traffic could direct your PCs traffic to whatever location he desired.

For these reasons, a secure AD DNS server should only listen to requests that come from within your network, and a different DNS server should handle DNS requests external to your network. In addition all of your workstations should be configured to only look to your AD DNS server for DNS, and not to any external DNS servers. This is known as [https://ikipedia.org/wiki/Spl/hori/ split-horizon DNS].

====
===============================

NetBIOS Names
====
===============================

Before Windows utilized DNS, it relied upon another naming system, `Terms_and_Abbreviations#NetBios|NetBIOS` (technically NetBIOS-NS), and the `Terms_and_Abbreviations#Windows_Internet_Naming_Service_.28WINS.29|Windows Internet Name Service (WINS)`. NetBIOS is similar to DNS in that it can serve as a directory service, but more limited as it has no provisions for a name hierarchy and names were limited to 15 characters. However NetBIOS does provide a means for peer-to-peer name resolution over the layer 2 broadcast domain (all PCs within the same subnet). Microsoft extended this with WINS to provide name resolution across layer 3 (routed) networks. If you have name resolution working in a network without DNS service, it is probably being handled by NetBIOS.

While the days of those systems are mostly behind us now, traces of this legacy system still linger all throughout Windows. For example some aspects of Windows networking, such as networking neighborhood and its descendants, are still reliant upon this service. In particular every AD Domain also has a NetBIOS name along with its traditional DNS name. And every computer in your domain will also have a NetBIOS name (even if you turn the NetBIOS names service off). Most of the time, this defaults to the first 15 characters of the PCs name. 

Why This Matters 
------------------------

Just as DNS names can conflict, NetBIOS names can also conflict. Most of the time this will not pose an issue. Windows queries NetBIOS as a last resort for name resolution, and without a WINS server in your network, NetBIOS names cannot cross layer 2 (the subnet). However, while Active Directory already prevents you from having PCs with duplicate names, it does not prevent you from having duplicate NetBIOS names, which generally would only happen if the first 15 digits of your computer name were identical. Such name conflicts should be avoided.

NetBIOS Domain Naming 
------------------------

Since NetBIOS has [https://OOTTmicrosoft.com/enDDAASS/8997 /AA/ possibilities] in what domain names are acceptable, there is less you can do to avoid potential naming conflicts. Typically and recommended is, to pick the first part off your domain name for the NetBIOS domain (note:  another name for 'workgroup'). For example if your domain name is "samdom.example.com", you might pick the NetBIOS name "SAMDOM". Whatever you use for your NetBIOS name, ensure it is just one word, no longer than 15 characters and without any punctuation, this includes periods '.' . This appears to be especially important with Windows 10 clients, there have been reports that they cannot join the domain if the NetBIOS domain name contains a period. 

===================

How Should I Name My Domain?
===================

Before we look at your options, let's look at some desirable features our domain name should have:

* The domain name should be globally unique. This ensures that no matter what the computer is configured to use for DNS resolution, the name will either resolve properly or will not return a domain (NXDOMAIN). There should never be a domain name conflict!

* The domain should be associative to your organization. The domain name should ideally be related to your organization, making it easy to remember.

* The domain should be in your control. A domain name that you control (by being the registered owner of it) helps to prevent malicious use. Domain name registration is cheap and desirable for any enterprise anyway.

* The domain name should still be a valid domain name, that way you can get 3rd party SSL certificates for it, if you desire.

* The FQDN for an Active Directory domain name is limited to 64 bytes, including the dots, an Active directory server name example : s4ad01.office.example.tld

* Whatever domain name you use, it should not be resolvable from the internet, it is not a good idea to have any AD domain computer connected directly to the internet. 

So with those criteria in mind, lets look at some of your options:

====================

Subdomain of a Domain You Own
====================

In this scenario you would name your domain in the fashion of "subdomain.domainyouown.tld" such as "samdom.example.com". **This is typically the best option you can select!** It also aligns with Microsoft's  current [https://OOTTmicrosoft.com/enDDAASS/ry/cc738121%/10%29DD/ best practices]. While the subdomain name can be anything you desire, keeping it short and simple is probably a good idea (e. g. "ad."). This style of name fulfills all the above criteria we laid out in a desirable domain name, most importantly::

* It will be globally unique. Since you control the domains registration on the net (and presumably its DNS entries), you can ensure that the domain you use internally does not resolve to anything externally.

* It will be a valid domain name for retrieving 3rd party SSL certificates.

Important Note 
------------------------

When naming your domain, Windows and samba-tool also generate a [https://OOTTmicrosoft.com/enDDAASS/ry/cc961556D/  /on] legacy NetBIOS domain name. By default this is the first 15 characters of the leftmost domain name. So if your domain is named "ad.example.com", then the default suggestion would be simply "AD". Selecting such a NetBIOS domain name would be a bad idea, as it would be quite likely to conflict with any other domains that had the same NetBIOS name. This would pose an issue if you ever had to set up a trust relationship between these two domains. Instead select a custom name based upon your domain name, for example you might select "EXAMPLE" for a domain named "ad.example.com".

Common Objections 
------------------------

My User Logins Does Not Match My Email 
------------------------

You are correct, that the part of the user name that follows the @ sign, the User Principle Name Suffix (UPN Suffix), defaults to the domain name, and so would be "subdomain.domain.tld" by default under this scheme. However, the UPN Suffix is arbitrary and configurable. You can [https://OOTTmicrosoft.com/enDDAASS/ry/cc772007D/  /e] it to be whatever you like, including your users email. It must be unique among all security principal objects within a directory forest. This means the prefix of a UPN can be reused, just not with the same suffix.

It has the following restrictions:

It must be the DNS name of a domain, but does not need to be the name of the domain that contains the user.
It must be the name of a domain in the current domain forest (which in Samba AD means the same thing), or an alternate name listed in the upnSuffixes attribute of the Partitions container within the Configuration container.

The Style of the Domain Name Is to Long
------------------------

The addition of the suffix can be as short as you like it (using just "ad" or "ds" is very common). However, typing the domain name can be avoided entirely by setting the DNS Suffix and DNS suffix search list. When these variables are set, clients will attempt to resolve single label domain names like just "server" as "server.dnssuffix.tld". This is even valid with certificates, so you can issue a certificate for an internal server that would be good for "<nowiki>https://t;/now/ot;S/nstead of "<nowiki>https://server.samdom.example.com/</nowiki>" /PAAC/ like. And if at some point you need to avoid the use of the DNS search suffix on a DNS inquiry, you can do that by specifying the FQDN remembering to include the final '.' for the root zone.

This Does Not Work Right with My External Domain
------------------------

This assumption is incorrect. The AD DNS server will only be authoritative for this single subdomain and the names beneath it. It is not responsible for any other domain names. Thus if your AD Domain Name is "samdom.example.com", and you ask it to resolve the name "www.example.com", it will recognize that is not authoritative for "www.example.com" and so will forward the request on to the external server that is authoritative for that domain.

My NetBIOS Name Can Conflict
------------------------

A valid concern. However the name suggested by samba-tool and in the Windows DC Promo wizard is only a suggestion. You can select any NetBIOS name you wish. Picking one based off of your domain name might be a good alternative, or pick any other associative but unique name for the NetBIOS domain name.

I have to Use Different Names to Resolve Host Names Internally and Externally
------------------------

Yes, since under this scheme no DNS service outside your network can resolve names within it, a different DNS name is necessary to resolve the same computer within your network as you might use to resolve it outside your network. For the most part this is a good thing, the internal and external address of computers generally are fundamentally different, and will have different IP address.

However in some situations you might find it inadvisable to add this extra complication into your configurations. For example, if you distribute mail settings for users to configure, the added complexity of having to use a different mail scheme externally and internally might be too much for them. On mobile devices this scheme might not even be workable. Thankfully there are a couple solutions to this problem:

* **Allow traffic for the external name to route out and back into your network.**
* EOften no extra configuration is necessary for this setup, traffic bound for external IP addresses generally can be treated the same as any other internet bound traffic, even if its eventual destination is back inside your network.

* **Create a DNS zone that only resolves the name want.**
* EThere is a clever trick you can do in AD DNS to only resolve a single host within a zone, and still have the rest of the hosts resolved normally by the external server. Create a DNS zone named "host.domain.tld", for example "mail.example.com". Inside the zone create a single A or CNAME record (CNAME is probably preferred) leaving the name of the record blank. As the dialog tells you, this name will be used to resolve the parent domain, in this case, "host.domain.tld".

* EThis will do exactly what you want. "host.domain.tld" will be resolved as you specify by your DNS server, while requests to other hosts under "domain.tld" will be resolved externally, since "host.domain.tld" and "domain.tld" correspond to different zones. If you use a CNAME as your parent record you can point this back to the record in your internal domain name.

===========

Using an Invalid TLD
===========

In this scenario you would name your domain in the format of "domain.invalid.tld" such as "SAMDOM.local". Using an invalid top-level domain (TLD) such as .local or .internal used to be a very common practice. In fact all versions of Microsoft's Small Business Servers were configured to use a domain in the form of "domain.local". Since the .local TLD is officially reserved by ICANN, you can also be assured that no external DNS server will resolve this domain. However this style of name has a few major issues:

* The .local TLD is used by some zeroconf systems, most importantly Apple's Bonjour service. Using them together will not work correctly.

* Invalid TLDs, such as .local or .internal, will soon be unable to get SSL certificates from any of the major certificate providers. The CA/ Forum has [https://wwwDDOOT/DOOTTcom/internal-n/htm?SSAID=314743 decided] that no certificates should be issued for these invalid domains starting November 1, 2015. In fact, you are now unable to purchase a certificate for these names if they expire after this date. This includes Subject Alternative Names (SAN) used within otherwise valid certificates (this is a very common configuration for Microsoft Exchange). While internal certificate authorities have no such restriction, having this option open to you is always a good thing.

* It is possible that the invalid TLD you are now using, could become a valid TLD in the future. While .local is reserved by ICANN, the TLD system is currently scheduled to undergo a vast [http://gtld.com/ /n] of the generic TLD (gTLD) it supports, from  22 to over a thousand new names. This trend is likely to continue.

For the same reason, names with other invalid TLDs should be avoided, including .internal and .lan.

======================

Using Your external Domain Name
======================

In this scenario, you simply reuse internally, your external domain name, in the format "domain.tld", for example "example.com". While this can be a valid option, it can also add unnecessary complication to your DNS system. As explained earlier, such a system introduces the potential for a domain name conflict, typically between a name that is either not present internally, or a name that resolves differently externally to internally. This can be resolved by duplicating the entries you have on the external server, on your internal server, but this may be impracticable if you have a lot of external DNS entries or if they change frequently. You could and probably should pick an internal naming scheme that never conflicts with your external naming scheme.

==================

Using a Generic Domain Name
==================

It has been suggested, for organisations that are subject to a lot of mergers and acquisitions, using a generic domain name such as "corp.local" might be a valid option. Since domain renaming and migration is often a difficult and costly operation, there may be some validity to this, but there is no guarantee that the selected domain name might not be chosen by another admin thinking along the same lines as yourself. This would make a domain merger much more difficult. In addition, while the generic name can be hidden from the users by use of custom UPN suffixes and DNS search suffixes, an old domain name with guaranteed uniqueness could also be hidden in the same fashion.

====================

Using a Different Domain Name
====================

In this scenario you use a different, unused domain name from your primary internet domain name. For example you might use a different TLD ("example.net" as opposed to "example.com") or an entirely different domain name together, but still registered normally with ICANN. The purported advantage of this scheme is the ability to resolve some domain names (for example, "mail.domain.tld") via the same name internally and externally.

While this is true as far as it goes, the same effect can be achieved in other schemes by doing some clever DNS tricks (described above). Furthermore, any time you resolve names differently externally to internally, there is the potential for an unwanted DNS naming conflict, so it is questionable if it is ever desirable to do this across the whole domain. This scheme also leaks a minor amount of sensitive information (the domain name) onto the net, and represents a minor additional cost (the cost of the domain registration), while offering only marginal advantages. It may however be a valid option for some organizations, such a scheme is often used by ISPs and other internet focused organizations.

=====
===============================

The Realm name
=====
===============================

This is quite simple, once you have selected the DNS domain, you must use this for the realm name, but in most cases it is expressed in uppercase. For example, the DNS domain ``samdom.example.com</ would be the Realm name ``SAMDOM.EXAMPLE.COM``/

----
`Category Directory`