Running a Samba AD DC with MIT Kerberos KDC
    <namespace>0</namespace>
<last_edited>2019-03-06T08:43:59Z</last_edited>
<last_editor>GlaDiaC</last_editor>

===============================

Introduction
===============================

On an Active Directory (AD) domain controller (DC), Samba uses an external application to provide Kerberos support. In version 4.6 and earlier, Samba only supported the [https://www.h5l.org Heimdal Kerberos] implementation for the Key Distribution Center (KDC). For this reason, vendors of operating systems that only support MIT Kerberos could not provide packages with AD DC-capabilities. On these operating systems you can build Samba or use 3rd-party packages with AD DC support to set up a DC, but Samba can not be fully integrated into operating systems that use MIT Kerberos.

Samba 4.7 and later supports building Samba with [https://web.mit.edu/kerberos/ MIT Kerberos]. Distributions, which previously did not provide AD DC-aware Samba packages because they use MIT Kerberos, are now able to provide such packages. For details about migrating a Samba DC, for example, from self-compiled to packages, see `Migrating a Samba Installation`.

Use this documentation, if you want to:
* Build Samba with MIT Kerberos back end and set up a new AD DC.
* Migrate a Heimdal Kerberos-based Samba DC installation to MIT Kerberos back end.

.. note:

    The KDC does not need to be consistent on all your DCs.

=================
Experimental Feature
=================

.. note:

    Using MIT Kerberos is still considered experimental.

Samba 4.7 and later versions have shipped with code to support building
the Samba AD DC using MIT Kerberos.  Since the time of the release a
number of issues, including security issues, have been found by real-world use.  
However sadly the Samba Team has not been able to resource
the resolution of these issues to a standard that we are happy with,
and so  Samba 4.9.3, 4.8.7 and 4.7.12 releases mark this mode more clearly as experimental.  

As an experimental feature, we will not be issuing security patches for
this feature, including for:

* [https://bugzilla.samba.org/show_bug.cgi?id=13571 S4U2Self crash with MIT KDC build]    

=================
Known Limitations of MIT Kerberos Support in Samba
=================

Samba DCs with MIT Kerberos KDC currently do not support:

* PKINIT support required for using smart cards
* Service for User to Self-service (S4U2self)
* Service for User to Proxy (S4U2proxy)
* Running as a Read only domain controller (RODC)
* Authentication Audit logging
* Computer GPO's are not applied, see  [https://bugzilla.samba.org/show_bug.cgi?id=13516 Bug 13516]

=================
Building Samba with MIT Kerberos Support
=================

To enable MIT Kerberos support when you build Samba:

* Install the MIT Kerberos 1.15.1 or later server and header files. For details, see `Package Dependencies Required to Build Samba`.

* Pass the ``--with-system-mitkrb5 --with-experimental-mit-ad-dc`` option to the ``configure`` script when you build Samba. For further details on building Samba, see `Build Samba from Source`.

=================
Verifying if Samba Has Been Built with MIT Kerberos Support
=================

To verify if Samba has been built with MIT Kerberos support, enter:

 # smbd -b | grep HAVE_LIBKADM5SRV_MIT
    HAVE_LIBKADM5SRV_MIT

If no output is displayed, Samba was compiled without MIT Kerberos support and uses Heimdal Kerberos.

=================
Configuring the MIT KDC on a new DC
=================

When you provision a new DC or join a DC to an existing AD, ``samba-tool`` automatically creates the ``/usr/local/samba/private/kdc.conf`` file. No further action is required.

The ``kdc.conf`` file is stored in Samba's ``private`` directory. To locate this directory:

 # smbd -b | grep "PRIVATE_DIR"
   PRIVATE_DIR: /usr/local/samba/private/

=================
Migrating a DC That Previously Used the Heimdal KDC
=================

If you previously ran a DC that used the Heimdal KDC and want to migrate the DC to use MIT Kerberos:
* Build Samba with MIT Kerberos support using the same installation directories. For details, see `#Building_Samba_with_MIT_Kerberos_Support|Building Samba with MIT Kerberos Support`.
* Install the MIT Kerberos-aware Samba over your existing installation.
* Manually create the ``kdc.conf`` file:
* * Locate the path to the Samba ``private`` directory:

 # smbd -b | grep "PRIVATE_DIR"
   PRIVATE_DIR: /usr/local/samba/private/

* : In a later step you will create the ``kdc.conf`` in this directory.

* * Locate the path to the Samba modules directory:
 # smbd -b | grep "MODULESDIR"
   MODULESDIR: /usr/local/samba/lib/

* : The ``samba.so`` Kerberos database module is stored in the ``krb5/plugins/kdb/`` subdirectory of the modules directory. In the previous example, the file is located in the ``/usr/local/samba/lib/krb5/plugins/kdb/`` directory. In the next step, set the ``db_module_dir`` parameter in the ``kdc.conf`` file to this directory.

* * Create the ``kdc.conf`` in the Samba ``private`` directory. For example, in ``/usr/local/samba/private/kdc.conf``.

 [kdcdefaults]
        kdc_ports = 88
        kdc_tcp_ports = 88
        kadmind_port = 464

 [realms]
        SAMDOM.EXAMPLE.COM = {
        }

         samdom.example.com = {
        }

        SAMDOM = {
 	}

 [dbmodules]
        # Set the following parameter to the directory
        # that contains the samba.so database module:
        db_module_dir = /usr/local/samba/lib/krb5/plugins/kdb/

        SAMDOM.EXAMPLE.COM = {
                db_library = samba
        }

        samdom.example.com = {
                db_library = samba
        }

        SAMDOM = {
                db_library = samba
        }

 [logging]
        kdc = FILE:/var/log/samba/mit_kdc.log
        admin_server = FILE:/var/log/samba/mit_kadmin.log

* :.. warning:

   Set the ``db_module_dir`` parameter to the directory that contains the ``samba.so`` database module.

=================
Verifying that Samba uses the MIT Kerberos KDC
=================

When you start the ``samba`` service, the process automatically starts the ``krb5kdc`` MIT Kerberos KDC.

.. warning:

   Do not start the ``krb5kdc`` KDC manually.

To verify that the ``krb5kdc`` is a subprocess of the ``samba`` process, use the ``ps`` utility:

    306 ?        Ss     0:00 samba -D
    307 ?        S      0:00  \_ samba -D
    315 ?        S      0:00  |   \_ samba -D
    319 ?        Ss     0:00  |       \_ /usr/local/samba/sbin/smbd -D --option=server role check:inhibit=yes --foreground
    ..                        |
    313 ?        S      0:00  \_ samba -D
    316 ?        S      0:00  |   \_ samba -D
    322 ?        S      0:00  |       \_ /usr/sbin/krb5kdc -n
    ..

=================
Debugging Samba With MIT Kerberos Support
=================

To debug Kerberos-related problems, see the following log files:
* Samba logs to the file set in the ``log file`` parameter in your ``smb.conf`` file. For further details about logging in Samba and how to increase the log level, see `Configuring Logging on a Samba Server`.

* The MIT KDC logs to the file set in the ``kdc`` and ``admin_server`` paramter in the ``kdc.conf`` file. To increase the log out, see the ``kdc.conf(5)`` man page.