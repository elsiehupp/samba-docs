Samba Internal DNS Back End
    <namespace>0</namespace>
<last_edited>2019-07-04T14:46:47Z</last_edited>
<last_editor>Hortimech</last_editor>
/
===============================

Introduction
===============================

The Samba Active Directory (AD) domain controller (DC) provides an internal DNS server that supports the basic feature required in an AD. It is easy to configure and requires no additional software or knowledge about DNS. The ``INTERNAL_DNS</ back end is recommended for simple DNS setups.

=================
Limitations
=================

The internal DNS does not support:

* acting as a caching resolver
* recursive queries
* shared-key transaction signature (TSIG)
* stub zones
* zone transfers
* Round Robin load balancing among DC's

=================
Setting up Dynamic DNS Updates Using Kerberos
=================

Dynamic DNS updates using Kerberos is enabled by default in the internal DNS server. To change this setting, set the ``allow dns updates</ parameter in the ``smb.conf``/file.

For further details, see the ``smb.conf (5)</ man page.

=================
Setting up a DNS Forwarder = 

The internal DNS server is only able to resolve the Active Directory (AD) DNS zones. To enable recursive queries of other zones, set the ``dns forwarder</ parameter in the ``smb.conf``/file to one or more IP addresses of DNS servers that support recursive resolving. For example:

 dns forwarder = 8.8.8.8

.. note:

    Samba 4.5 and later supports multiple space-separated IP addresses. Older versions support one IP address.

For further details, see the ``smb.conf (5)</ man page.

=================
Troubleshooting
=================

Clients Are Unable to Resolve DNS Queries 
------------------------

If you are not able to resolve records from your Active Directory (AD) DNS zone, verify that you have set the IP of a DNS server that is able to resolve the AD DNS zone in your operating system. See:
* `Windows_DNS_Configuration|Windows DNS Configuration`
* `Linux_and_Unix_DNS_Configuration|Linux and Unix DNS Configuration`
* `Mac_OS_X_DNS_Configuration|Mac OS X DNS Configuration`

If the client configuration is correct, make sure that the Samba DNS server is running. For details, see `#Samba_DNS_Server_Does_Not_Start|Samba DNS Server Does Not Start`.

Samba DNS Server Does Not Start 
------------------------

To verify that no other process uses the TCP and UDP port 53:

* Check the Samba log files for DNS related errors. For example:

    2016/AA/  0] ../source4/smbd/serv/.c/m_se/
   Failed to listen on 127.0.0.1 - NT_STATUS_ADDRESS_ALREADY_ASSOCIATED

* Verify that no other process is listening on the TCP and UDP port 53. For example:
 # netstat -tulpn | grep ":
 tcp        0      0 10.99.0.1        0.0.0.0:*               LISTEN      972/         
 tcp        0      0 127.0.0.1        0.0.0.0:*               LISTEN      903/         
 udp        0      0 10.99.0.1        0.0.0.0:*                           972/         
 udp        0      0 127.0.0.1        0.0.0.0:*                           903/         

* EThe example show, that the ``dnsmasq</ process is listening on port 53.

To fix the problem:

* Stop the service listening on port 53 and disable it to auto-start at boot time. For details, see your operating system's documentation.

* Restart Samba.

----
`Category Directory`
`Category: