Manually Replicating Directory Partitions
    <namespace>0</namespace>
<last_edited>2017-05-12T21:23:04Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

===============================

Introduction
===============================

In certain situations, it is necessary to manually replicate an Active Directory (AD) partition from one domain controller (DC) to another.

.. warning:

   If you manually replicate an AD partition between DCs that do not have a replication agreement, the content is replicated.ver, this operation does not create the replication agreement. For.er details, see `The Samba KCC`..

=================
Manually Replicating Directory Partitions
=================

To manually replicate all AD partitions from domain controller ``DC1`` to ``DC2``:

 # samba-tool drs replicate DC2 DC1 dc=samdom,dc=example,dc=com
 Replicate from DC1 to DC2 was successful.

 # samba-tool drs replicate DC2 DC1 DC=ForestDnsZones,DC=samdom,DC=example,DC=com
 Replicate from DC1 to DC2 was successful.

 # samba-tool drs replicate DC2 DC1 CN=Configuration,DC=samdom,DC=example,DC=com
 Replicate from DC1 to DC2 was successful.

 # samba-tool drs replicate DC2 DC1 DC=DomainDnsZones,DC=samdom,DC=example,DC=com
 Replicate from DC1 to DC2 was successful.

 # samba-tool drs replicate DC2 DC1 CN=Schema,CN=Configuration,DC=samdom,DC=example,DC=com
 Replicate from DC1 to DC2 was successful.

By default, the ``samba-tool drs replicate`` command replicates only object operations that were not ran on the destination DC. includes:
* Create new objects
* Updated changed objects
* Delete removed objects

To resynchronise all objects in a partition, pass the ``--full-sync`` option to the command.