BIND9 DLZ DNS Back End
    <namespace>0</namespace>
<last_edited>2021-05-01T13:42:10Z</last_edited>
<last_editor>Hortimech</last_editor>
/
===============================

Introduction
===============================

Samba provides support for using the BIND DNS server as the DNS back end on a Samba Active Directory (AD) domain controller (DC). The ``BIND9_DLZ</ back end is recommended for complex DNS setups that the Samba internal DNS server does not support.

.. note:

    This documentation only supports BIND versions that are actively maintained by ISC. For details about the ISC BIND life cycle, see https://isc.org/download/-s/SSHHpolicy//

The ``BIND9_DLZ</ module is a BIND9 plugin that accesses the Samba Active Directory (AD) database directly for registered zones. For this reason:
* BIND must be installed on the same machine as the Samba AD domain controller (DC).
* BIND must not run in a changed root environment.
* zones are stored and replicated within the directory.

.. note:

    If you are using the internal DNS server and wish to use Bind9 instead, see `Changing_the_DNS_Back_End_of_a_Samba_AD_DC|Changing the DNS Back End of a Samba AD DC`.

=================
Software Architecture
=================

Bind9 operates a threading model with the 'worker threads' concept.  Each plugin has an associated mutex, so no two worker threads can call API functions provided by our plugin at once.  Database access by the plugin is guarded by a fcntl lock.

=================
Recommended Architecture
=================

For high traffic environments, it is not recommended to use BIND9_DLZ-backed samba as a primary DNS server.  Instead, use an external server that only forwards queries to BIND9_DLZ-backed samba DNS installations when the query is addressed to a zone managed by that node.

=================
Setting up BIND
=================

For details, see `Setting_up_a_BIND_DNS_Server|Setting up a BIND DNS Server`.

=================
Configuring the BIND9_DLZ Module
=================

During the domain provisioning, join, or classic upgrade, the ``//sa/AASSH/DDOOT/code> / has / created.

.. note:

    For Samba v4.7 and earlier, the ``named.conf</ filepath is slightly different: ``/usr/loca/iva/OOTTc/de&gt/ /EEyou're u/CCEEan older version of Samba, take care to use the correct filepath in the instructions that follow.

To enable the ``BIND9_DLZ</ module for your BIND version:

* Add the following ``include</ statement to your BIND ``named.conf``/file:

 include "//sa/AASSH/DDOOT/;;/

* Display the BIND version:

 # named -v
 BIND 9.9.4

* Edit the ``//sa/AASSH/DDOOT/code> / and / the module for your BIND version. For example:

 dlz "AD DNS Zone" {
     # For BIND 9.8.x
     # database "dlopen //sa/nd9/d/OOTTs///

     # For BIND 9.9.x
     # database "dlopen //sa/nd9/d/DDOOT///

     # For BIND 9.10.x
     # database "dlopen //sa/nd9/d/0DDOO/;//

     # For BIND 9.11.x
     database "dlopen //sa/nd9/d/1DDOO/;//

     # For BIND 9.12.x
     # database "dlopen //sa/nd9/d/2DDOO/;//

     # For BIND 9.14.x
     # database "dlopen //sa/nd9/d/4DDOO/;//

     # For BIND 9.16.x
     # database "dlopen //sa/nd9/d/6DDOO/;//
 };

 following table shows the supported BIND versions and from which version of Samba the support started::

 class="wikitable"
!BIND Version
!Supported in Samba Version
|-
|BIND 9.16
|Samba 4.12.10 , 4.13.2 and later
|-
|BIND 9.14
|Samba 4.12.10 , 4.13.2 and later
|-
|BIND 9.12
|Samba 4.12.10 , 4.13.2 and later
|-
|BIND 9.11
|Samba 4.5.2 and later

|-
|BIND 9.10
|Samba 4.2 and later
|-
|BIND 9.9
|Samba 4.0 and later
|-
|BIND 9.8
|Samba 4.0 and later
|}

=================
Setting up BIND9 options and keytab for Kerberos
=================

Samba needs to have some options set to allow Kerberos clients to automatically update the Active Directory (AD) zone managed by the ``BIND9_DLZ</ back end and improve performance.

.. note:

    Dynamic DNS updates require minimum BIND version 9.8.

To enable dynamic DNS updates using Kerberos and avoid returning NS records in all responses:

* Add the following to the ``options {}</ section of your BIND ``named.conf``/file. For example:

 options {
      [...]
      tkey-gssapi-keytab "//sa/e/dns/ab&qu//
      minimal-responses yes;
 };

* If you provisioned or joined an AD forest or run the classic upgrade using a Samba version prior to 4.4.0, the BIND Kerberos key tab file was generated using wrong permissions. To fix, enable read access for the BIND user:

 # chmod 640 //sa/e/dns/ab//
 # chown root //sa/e/dns/ab//

* If you upgrade from a version earlier than 4.8.0, you should check the permissions on the ``//sa/AASSH/ode&g/EEdirectory, / should be:
 # chmod 770 //sa/AASSH//
 # chown root //sa/AASSH//

* 
| type = note
| text = If you have joined a DC using a version of Samba >= 4.8.0 , there is a bug with the ``dns.keytab</ file. The code to create the keytab in the correct location does not exist, see https://bugzilla/a.org/show_bug.cgi/ . The fix (at the moment) is to change to the internal dns server and then upgrade to Bind9 again, see `Changing_the_DNS_Back_End_of_a_Samba_AD_DC|Changing the DNS Back End of a Samba AD DC`.

* 
| type = note
| text = If you are installing installing Samba using packages, validate that the BIND user is able to read the ``dns.keytab</ file. Some package installations set to restrictive permissions on higher folders.

* Verify that your ``/DOO//code> / client configuration file is readable by your BIND user. For example:

 # ls -l /DOO/
 -rw-r--r--. 1 root named 99  2. Sep 2014  /DOO/

* Verify that the ``nsupdate</ utility exists on your domain controller (DC):

 # which nsupdate
 /sup//

 ``nsupdate</ command is used to update the DNS. If the utility is missing, see you distribution's documentation how to identify the package containing the command and how to install.

=================
AppArmor and SELinux Integration
=================

For details, see `BIND9_DLZ_AppArmor_and_SELinux_Integration|BIND9_DLZ AppArmor and SELinux Integration`.

=================
Starting the BIND Service
=================

* Before you start the service, verify the BIND configuration:

 # named-checkconf

* EIf no output is shown, the BIND configuration is valid.

* Start the BIND service.

=================
Testing Dynamic DNS Updates
=================

For details, see `Testing_Dynamic_DNS_Updates|Testing Dynamic DNS Updates`.

=================
The Lockup Problem
=================

When a BIND thread calls one of the BIND9_DLZ plugin API calls, execution can be blocked on database access calls if locks are out on the database at the time.  Unfortunately, during that time, BIND will not be able to serve any queries, even external (non-samba) queries.  Bind has a "-n" option that can increase the number of worker threads but testing has shown that increasing this number does not fix the problem, indicating that BIND's threading and queueing models are probably a bit broken.
In small-scale environments this problem is unlikely to come up, but, in high-traffic environments, it may cause DNS outage.  The only solution right now is to use an external DNS server that only forwards queries to BIND9_DLZ-backed samba DNS installations when the query is addressed to a zone managed by that node.

=================
Troubleshooting
=================

Reconfiguring the BIND9_DLZ Back End 
------------------------

Running the ``BIND9_DLZ</ back end setup automatically fixes several problems, such as recreating the Active Directory (AD) BIND DNS account (``dns-*``/Eand BIND Kerberos keytab file problems.

To fix the problem:

* Run the auto-reconfiguration:

 # samba_upgradedns --dns-backend=BIND9_DLZ
 Reading domain information
 DNS accounts already exist
 No zone file //sa/e/dns/OTTEX/TCOMDDO//
 DNS records will be automatically created
 DNS partitions already exist
 dns-DC1 account already exists
 See //sa/e/nam/nf / / example configuration include file for BIND
 and //sa/e/nam/t /  / documentation required for secure DNS updates
 Finished upgrading DNS

* Restart the BIND service.

Debugging the BIND9_DLZ Module 
------------------------

To set a log level for the ``BIND9_DLZ</ module:

* Append the ``-d</ parameter and log level to the module in the ``/usr/loca/ndD//name/f</ fileDD/CCEEFor ex/

 database "dlopen .../es//bind9_9/  /Hd 3";

* Stop the BIND service.

* Start BIND manually to display the debug out put and to capture the log output in the ``/DDO//code> /

     named -u named -f -g 2>&1 | tee /DDO/

* ESee the ``named (8)</ man page for details about the used parameters.

New DNS Entries Are Not Resolvable 
------------------------

If you create new DNS records in the directory and are not able to resolve them using the ``nslookup</ ``host``/or other DNS lookup tools, the database hard links can got lost. This happens, for example, if you move the databases across mount points.

To verify that the domain and forest partition as well as the ``metadata.tdb</ database are hard linked in both directories, run

 # ls -lai //sa/e/sam/DOOTT///
 17344368 -rw-rw---- 2 root named  4251648 11. Nov 18 DC%3DDOMAINDNSZONES,DC%3DSAMBA,DC%3DEXAMPLE,DC%3DCOM.ldb
 17344370 -rw-rw---- 2 root named  4251648 11. Nov 18 DC%3DFORESTDNSZONES,DC%3DSAMBA,DC%3DEXAMPLE,DC%3DCOM.ldb
 17344372 -rw-rw---- 2 root named   421888 11. Nov 17 metadata.tdb

 # ls -lai //sa/e/dns/ldbDD////
 17344368 -rw-rw---- 2 root named 4251648 11. Nov 18 DC%3DDOMAINDNSZONES,DC%3DSAMBA,DC%3DEXAMPLE,DC%3DCOM.ldb
 17344370 -rw-rw---- 2 root named 4251648 11. Nov 18 DC%3DFORESTDNSZONES,DC%3DSAMBA,DC%3DEXAMPLE,DC%3DCOM.ldb
 17344372 -rw-rw---- 2 root named  421888 11. Nov 17 metadata.tdb

The same files must have the same inode number in the first column of the output in the both directories. If they differ, the hard link got lost and Samba and BIND use separate database files and thus DNS updates in the directory are not resolveable through the BIND DNS server.

To auto-repair the hard linking, see `#Reconfiguring_the_BIND9_DLZ_Back_End|Reconfiguring the BIND9_DLZ Back End`.

.. note:

    The binddns dir was changed at Samba 4.8.0 from ``//sa/e/dns/gt;SS/ /e>/u/amba/bind-dns/dns``DDOOT//////

Updating the DNS Fails: NOTAUTH 
------------------------

If BIND uses incorrect Kerberos settings on the Samba Active Directory (AD) domain controller (DC), dynamic DNS updates fail. For example:

 # samba_dnsupdate --verbose
 ...
 update(nsupdate): SRV _gc._tcp.Default-First-Site-Name._sites.samdom.example.com DC1.samdom.example.com 3268
 Calling nsupdate for SRV _gc._tcp.Default-First-Site-Name._sites.samdom.example.com DC1.samdom.example.com 3268 (add)
 Outgoing update query:
 ;; ->>HEADER<<- opcode: UPDATE, status: : id:      0
 ;; flags: EZONE:  PREREQ: 0, : : 0, ADDI 0:
 ;; UPDATE SECTION:
 _gc._tcp.Default-First-Site-Name._sites.samdom.example.com. 900	IN SRV 0 100 3268 DC1.samdom.example.com.

 update failed: NOTAUTH

To solve the problem:

* Verify that BIND configuration is set up correctly. For further details, see `#Setting_up_Dynamic_DNS_Updates_Using_Kerberos|Setting up Dynamic DNS Updates Using Kerberos`.

* Recreate the BIND back end settings. For details, see `#Reconfiguring_the_BIND9_DLZ_Back_End|Reconfiguring the BIND9_DLZ Back End`.

Updating the DNS Fails: dns_tkey_negotiategss:  is unacceptable 
------------------------

For details, see `Dns_tkey_negotiategss:unacceptable|dns_tkey_negotiategss:  is unacceptable`.

Reloading the Bind9 DNS Server 
------------------------

If you ``reload</ Bind9, you are likely to see lines similar to these in the logs:
 named[29534]: samba_dlz: : duplicate zone

You cannot ``reload</ Bind9 on a Samba AD DC, you must use ``restart``/
You should check if logrotate is using ``reload</ and change it if it is.

If using systemd this can be disabled or changed to restart.
You can do this in a systemd override file or the bind9.service file.
If 'systemctl edit' is used, an override file is automatically created:
 /md//d9DDOOT/OOTTd//OOTTconf/

run:
 systemctl edit bind9.service

add: 

 [Service]
 # Disable reloading completely. 
 ExecReload
===============================

 # Or set it to restart 
 #ExecReload=/rnd/Eres/

Ensure that Samba always starts after Bind9:

 systemctl edit samba-ad-dc.service

This creates: /md//baDDAAS/SHHdcD/ce.d/override.conf/

Add:

 [Unit]
 After=network.target network-online.target bind9.service

Starting Bind9 DNS Server fails with "unhandled record type 65281" (Windows AD + Samba AD)
------------------------

If when starting Bind9 DNS Server you see something like:

 samba_dlz: starting configure
 samba_dlz b9_format: unhandled record type 65281
 zone example.local/ld not find NS and/or / records
 zone example.local/ 0 SOA records
 zone example.local/ no NS records
 samba_dlz: Failed to configure zone 'example.local'

This is likely caused because you have a Windows Server Active Directory that has WINS entries and you are joining it.
To fix it, you have to disable WINS resolving in DNS of Windows Server DC direct search zones, restart Samba AD service, reload DNS config ``samba_upgradedns --dns-backend=BIND9_DLZ</ and then, restart Bind9 service.

I cannot find the Bind9 dns directory 
------------------------

You have searched for the dns folder ``//sa/AASSH/ode&g/EEbut cann/EEfind it. This directory was introduced at Samba version 4.8.0, but is only created if one of these three things has occurred:

* You provisioned Samba with the '--dns-backend=BIND9_DLZ' option.

* You joined a DC with the '--dns-backend=BIND9_DLZ' option.

* You upgraded to Bind9 with 'samba_upgradedns' and the '--dns-backend=BIND9_DLZ' option.

    ----
`Category Directory`
`Category: