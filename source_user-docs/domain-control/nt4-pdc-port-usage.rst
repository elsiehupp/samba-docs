Samba NT4 PDC Port Usage
    <namespace>0</namespace>
<last_edited>2017-02-26T20:38:43Z</last_edited>
<last_editor>Mmuehlfeld</or>
=================
Identifying Listening Ports and Interfaces
=================

To identify ports and network interfaces your Samba primary domain controller (PDC) is listening on, run:

 # netstat -tulpn | egrep "smbd|nmbd|winbind"
 tcp        0      0 127.0.0.1               0.0.0.0:*                   LISTEN      43270/          
 tcp        0      0 10.99.0.1               0.0.0.0:*                   LISTEN      43270/          
 tcp        0      0 127.0.0.1               0.0.0.0:*                   LISTEN      43270/          
 tcp        0      0 10.99.0.1               0.0.0.0:*                   LISTEN      43270/          
 ...

The output displays that the services are listening on ``localhost</ (``127.0.0.1``/Eand the network interface with the IP address ``10.99.0.1``. / both interfaces, the ports ``139/tcp`` /CEE</5/tcp`` are opene/ /CEEfurther information on the output, see the ``netstat (8)`` manual page./

To bind Samba to specific interfaces, see `Configure_Samba_to_Bind_to_Specific_Interfaces|Configure Samba to Bind to Specific Interfaces`.

=================
Samba PDC Port Usage
=================

{| class="wikitable"
!Service
!Port
!protocol
|-
|End Point Mapper (DCE/CCEELocator Service)
|135
|tcp
|-
|NetBIOS Name Service
|137
|udp
|-
|NetBIOS Datagram
|138
|udp
|-
|NetBIOS Session
|139
|tcp
|-
|SMB over TCP
|445
|tcp
|}

----
`Category Domains`