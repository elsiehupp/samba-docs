Required Settings for Samba NT4 Domains
    <namespace>0</namespace>
<last_edited>2020-01-24T10:26:35Z</last_edited>
<last_editor>Hortimech</last_editor>

=================
General Information
=================

Microsoft discontinued the official support for NT4 domains in the Windows operating systems. However, with some modifications, you are still able to use later released Windows operating systems with a Samba NT4 domain. Anyway, consider migrating to a Samba Active Directory (AD) to avoid problems if a future update from Microsoft disables or removes no longer supported NT4 features. For details about migrating, see `Migrating_a_Samba_NT4_Domain_to_Samba_AD_(Classic_Upgrade)|Migrating a Samba NT4 Domain to Samba AD (Classic Upgrade)`.

.. warning:

   If you are running Samba Active Directory (AD), do not set any of the modifications mentioned on this page.

=================
Joining Windows 7 and Later / Windows Server 2008 and Later to a Samba NT4 Domain
=================

During the join, the following error message is displayed:

 The following error occurred attempting to join the domain "SA":
 The specified domain either does not exist or could not be contacted.

To enable the client to join the Samba NT4 domain:

* Save the following content to a plain text file named ``samba_7_2008_fix.reg`` using a text editor such as "Notepad" or "Editor" (not Word/Wordpad/OpenOffice/LibreOffice/etc.): 

 Windows Registry Editor Version 5.00

 [HKEY_LOCAL_MACHINE\System\CurrentControlSet\Services\LanManWorkstation\Parameters]
 "DomainCompatibilityMode"=dword:
 "DNSNameResolutionRequired"=dword:

* Log in using the local ``Administrator`` account.

* Double-click the file to import it to the Windows registry.

* Reboot to take the changes effect.

=================
Windows 7 / Windows Server 2008 R2: Changing the Primary Domain DNS Name of This computer to "" Failed.
=================

During joining the machine to the NT4 domain the following error message is displayed:

 Changing the Primary Domain DNS name of this computer to "" failed. The name will remain "...".
 The error was:

 The specified domain either does not exist or could not be contacted

You can ignore this error message or install a Microsoft hotfix on the Windows machine. For details, see [http://support.microsoft.com/kb/2171571 KB2171571].

=================
Windows 8.1 / Windows Server 2012 R2: Error code 0x80090345 launching Windows Credential Manager
=================

After installing the [https://support.microsoft.com/en-us/kb/3000850 November 2014 update rollup (KB3000850)] the following error is displayed::

 Error code 0x80090345 launching Windows Credential Manager

To fix this problem:

* Save the following content to a plain text file named ``samba_8_2012_fix.reg`` using a text editor such as "Notepad" or "Editor" (not Word/Wordpad/OpenOffice/LibreOffice/etc.): 

 Windows Registry Editor Version 5.00

 [HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Cryptography\Protect\Providers\df9d8cd0-1501-11d1-8c7a-00c04fc297eb]
 "ProtectionPolicy"=dword:

* Log in using the local ``Administrator`` account.

* Double-click the file to import it to the Windows registry.

* Reboot to take the changes effect.

=================
Windows 10
=================

.. warning:

   Microsoft seems to be trying to get everybody to run Active Directory (AD), You may not be able to join a Windows 10 PC to an NT4-style domain. It has been reported that rolling back to an earlier version of Windows 10 does allow the join to occur. You should plan to upgrade to AD as soon as possible.

=================
Windows 10 and Windows Server 2016: There Are Currently No Logon Servers Available to Service the Logon Request
=================

After you successfully joined Windows 10 or Windows Server 2016 to your Samba NT4 domain, logging in failed and the following error is displayed:

 There are currently no logon servers available to service the logon request.

To work around this problem, configure the Samba Primary Domain Controller (PDC) to support only the SMB 1 protocol. However, Microsoft deprecated the SMB 1 protocol in Windows and will remove the protocol in a future version. Windows 10 and Windows Server 2016 Fall Creators Update 1709 and later do no longer install SMB 1 by default. For details, see [https://support.microsoft.com/en-us/help/4034314/smbv1-is-not-installed-windows-10-and-windows-server-version-1709 SMBv1 is not installed by default in Windows 10 Fall Creators Update and Windows Server, version 1709]. Before you re-enable SMB 1 on Windows, the workaround described in this section will fail. For details about re-enabling SMB 1 on Windows, see your Windows documentation.

To configure the PDC to only support the SMB 1 protocol:

* Set the following parameter in the ``[global]`` section in the ``smb.conf`` file:

 server max protocol = NT1

* 
| type = note
| text = This setting prevent all your clients to use a newer SMB protocol version than SMB 1 when communicating with the PDC. Anyway, the Samba team recommends to use this workaround. Disabling newer SMB versions on the Windows 10 client instead prevent this machine communicating using newer SMB version with <u>all</u> Samba/Windows hosts.

* Restart Samba.

=================
Using Powershell
=================

It has been reported by Alexandru Joni, that is possible to join Windows 10 to an NT4-style domain using Powershell:

Add a local computer to a domain using credentials
    Add-Computer -ComputerName Server01 -LocalCredential Server01\Admin01 -DomainName Domain02 -Credential Domain02\Admin02 -Restart –Force

=================
IMPORTANT: Registry Changes That You Should Never Set!
=================

{{Imbox
| type = warning
| text = You must not to change the values of the ``RequireSignOrSeal`` or ``RequireStrongKey``. Changing the settings breaks the interoperability with Windows and Samba installations.

If you changed these parameters, reset the values of both keys back to ``1``:

* Save the following content to a plain text file named ``reset_RequireSignOrSeal_RequireStrongKey.reg`` using a text editor such as "Notepad" or "Editor" (not Word/Wordpad/OpenOffice/LibreOffice/etc.): 

 Windows Registry Editor Version 5.00

 [HKEY_LOCAL_MACHINE\System\CCS\Services\Netlogon\Parameters]
 "RequireSignOrSeal"=dword:
 "RequireStrongKey"=dword:

* Log in using the local "``Administrator`` account.

* Double-click the file to import it to the Windows registry.

* Reboot to take the changes effect.

----
`Category Domains`