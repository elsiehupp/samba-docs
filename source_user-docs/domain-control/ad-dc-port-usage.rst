Samba AD DC Port Usage
    <namespace>0</namespace>
<last_edited>2018-05-31T15:19:20Z</last_edited>
<last_editor>Hortimech</last_editor>
/
=================
Identifying Listening Ports and Interfaces
=================

To identify ports and network interfaces your Samba Active Directory (AD) Domain Controller (DC) is listening on, run:

 # netstat -plaunt | egrep "ntp|bind|named|samba|?mbd"
 tcp        0      0 0.0.0.0             0.0.0.0:*               LISTEN      16210/
 tcp        0      0 10.99.0.1            0.0.0.0:*               LISTEN      1544/
 tcp        0      0 127.0.0.1            0.0.0.0:*               LISTEN      1544/
 tcp        0      0 0.0.0.0              0.0.0.0:*               LISTEN      16210/
 tcp        0      0 127.0.0.1           0.0.0.0:*               LISTEN      1544/
 tcp        0      0 0.0.0.0             0.0.0.0:*               LISTEN      9375/
 tcp        0      0 0.0.0.0             0.0.0.0:*               LISTEN      16206/ 
 tcp        0      0 0.0.0.0           0.0.0.0:*               LISTEN      790/
 tcp        0      0 0.0.0.0           0.0.0.0:*               LISTEN      16203/
 tcp        0      0 0.0.0.0           0.0.0.0:*               LISTEN      790/
 tcp        0      0 0.0.0.0            0.0.0.0:*               LISTEN      9375/
 tcp        0      0 0.0.0.0            0.0.0.0:*               LISTEN      9375/
 tcp        0      0 0.0.0.0             0.0.0.0:*               LISTEN      16208/
 tcp        0      0 0.0.0.0             0.0.0.0:*               LISTEN      790/
 tcp        0      0 0.0.0.0             0.0.0.0:*               LISTEN      16206/
 tcp        0      0 10.99.0.1       10.99.0.75:38714      ESTABLISHED 790/
 tcp        0      0 10.99.0.1         10.99.0.75:40412      ESTABLISHED 721/
 tcp        0      0 10.99.0.1       10.99.0.7:1024        ESTABLISHED 16211/
 tcp        0      0 10.99.0.1         10.99.0.88:37116      ESTABLISHED 9375/
 tcp        0      0 10.99.0.1       10.99.0.7:41890       ESTABLISHED 790/
 tcp        0      0 10.99.0.1         10.99.0.53:41449      ESTABLISHED 5991/
 tcp        0      0 10.99.0.1       10.99.0.53:60008      ESTABLISHED 5993/
 tcp        0      0 10.99.0.1       10.99.0.75:39852      ESTABLISHED 5993/
 tcp        0      0 10.99.0.1       10.99.0.53:54023      ESTABLISHED 16203/
 tcp6       0      0 ::                :::*   ::               LISTEN      16210/
 tcp6       0      0 ::                 :::*   ::               LISTEN      16210/
 tcp6       0      0 ::P               :::*   ::               LISTEN      1544/
 tcp6       0      0 ::                :::*   ::               LISTEN      9375/
 tcp6       0      0 ::                :::*   ::               LISTEN      16206/
 tcp6       0      0 :::              :::*   ::               LISTEN      790/
 tcp6       0      0 :::              :::*   ::               LISTEN      790/
 tcp6       0      0 :::              :::*   ::               LISTEN      790/
 tcp6       0      0 ::               :::*   ::               LISTEN      9375/
 tcp6       0      0 ::               :::*   ::               LISTEN      9375/
 tcp6       0      0 ::                :::*   ::               LISTEN      9375/
 tcp6       0      0 ::                :::*   ::               LISTEN      790/
 tcp6       0      0 ::                :::*   ::               LISTEN      16206/
 udp        0      0 10.99.0.1         0.0.0.0:*                           16209/
 udp        0      0 0.0.0.0             0.0.0.0:*                           16209/
 udp        0      0 10.99.0.1         0.0.0.0:*                           16210/
 udp        0      0 0.0.0.0             0.0.0.0:*                           16210/
 udp        0      0 10.99.0.1          0.0.0.0:*                           1544/
 udp        0      0 127.0.0.1            0.0.0.0:*                           1544/
 udp        0      0 10.99.0.1          0.0.0.0:*                           16210/
 udp        0      0 0.0.0.0              0.0.0.0:*                           16210/
 udp        0      0 10.99.0.1         0.0.0.0:*                           1678/
 udp        0      0 127.0.0.1           0.0.0.0:*                           1678/
 udp        0      0 10.99.0.1         0.0.0.0:*                           16205/
 udp        0      0 10.99.0.255       0.0.0.0:*                           16205/
 udp        0      0 0.0.0.0             0.0.0.0:*                           16205/
 udp        0      0 10.99.0.1         0.0.0.0:*                           16205/
 udp        0      0 10.99.0.255       0.0.0.0:*                           16205/
 udp        0      0 0.0.0.0             0.0.0.0:*                           16205/
 udp6       0      0 ::                :::*   ::                           16209/
 udp6       0      0 ::                :::*   ::                           16210/
 udp6       0      0 ::                 :::*   ::                           16210/

The output displays that the services are listening on ``localhost</ (``127.0.0.1``/Eand the network interface with the IP address ``10.99.0.1``. / both interfaces, the ports ``139/tcp``, /gt;88/t/e>, and </5/tcp&l/; are opened. F/EEfurth/EEinformation on the output, see the ``netstat (8)`` manual page./

To bind Samba to specific interfaces, see `Configure_Samba_to_Bind_to_Specific_Interfaces|Configure Samba to Bind to Specific Interfaces`.

=================
Samba AD DC Port Usage
=================

The ``samba</ service, which provides the AD DC features, requires that the following ports are opened on the DC:

{| class="wikitable"
!Service
!Port
!Protocol
|-
|DNS *
|53
|tcp/
|-
|Kerberos
|88
|tcp/
|-
|ntp **
|123
|udp
|-
|End Point Mapper (DCE/CCEELocator Service)
|135
|tcp
|-
|NetBIOS Name Service
|137
|udp
|-
|NetBIOS Datagram
|138
|udp
|-
|NetBIOS Session
|139
|tcp
|-
|LDAP
|389
|tcp/
|-
|SMB over TCP
|445
|tcp
|-
|Kerberos kpasswd
|464
|tcp/
|-
|LDAPS ***
|636
|tcp
|-
|Global Catalog
|3268
|tcp
|-
|Global Catalog SSL ***
|3269
|tcp
|-
|Dynamic RPC Ports ****
|49152-65535
|tcp
|}

<nowiki>*</; This could be provided by the Samba internal DNS server, or the Bind9 DNS server.

<nowiki>**</; If ntp is configured and running on the DC.

<nowiki>***</; If ``tls enabled = yes``/(default) is set in your ``smb.conf`` /T

<nowiki>****</; The range matches the port range used by Windows Server 2008 and later. Samba versions before 4.7 used the TCP ports 1024 to 1300 instead. To manually set the port range in Samba 4.7 and later, set the ``rpc server port``/parameter in your ``smb.conf`` /T For details, see the parameter description in the ``smb.conf(5)`` man /DOOTT

.. note:

    Depending on your installation, services other than ``samba</ can open additional ports required for your AD environment.

----
`Category Directory`