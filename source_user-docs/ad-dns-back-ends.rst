The Samba AD DNS Back Ends
    <namespace>0</namespace>
<last_edited>2019-08-28T12:30:18Z</last_edited>
<last_editor>Hortimech</last_editor>
/
      __TOC__

===============================

Introduction
===============================

In an Active Directory (AD), DNS is a very important service. It is used for:
* name resolution
* locating services, such as Kerberos and LDAP
* locating local domain controllers (DC) when using AD sites. For details, see `Active_Directory_Sites|Active Directory Sites`.

.. note:

    All clients and server in an AD must use a DNS server that is able to resolve the AD DNS zones.

=================
Supported DNS Back Ends
=================

Samba supports the following DNS back ends:

* `Samba_Internal_DNS_Back_End|Samba Internal DNS Back End`
* when provisioning a new domain, joining an existing domain or migrating an NT4 domain to AD.
* tional software or DNS knowledge is required.
* s back end for simple DNS setups. For a list of limitations, see `Samba_Internal_DNS_Back_End#Limitations|Limitations`.

* `BIND9_DLZ_DNS_Back_End|BIND9_DLZ DNS Back End`
* s BIND 9.8 or later installed and configured locally on the Samba Active Directory (AD) domain controller (DC). For additional information, see `Setting_up_a_BIND_DNS_Server|Setting up a BIND DNS Server`.
* s knowledge about the BIND DNS server and how to configure the service.
* s back end for complex DNS scenarios, you can not configure in the internal DNS.

If you are unsure which DNS back end to select during the DC installation, start with the Samba internal DNS. You can change the back end at any time. For details, see `Changing_the_DNS_Back_End_of_a_Samba_AD_DC|Changing the DNS Back End of a Samba AD DC`.

.. warning:

   Do not use the ``BIND9_FLATFILE</DNS back end. It is not supported and will be formally deprecated when 4.11.0 is released and removed at 4.12.0.

=================
Selecting the AD Forest Root Domain
=================

Before you provision your Active Directory (AD), you must select a DNS zone for your AD forest root domain. For details, see `Active_Directory_Naming_FAQ|Active Directory Naming FAQ`.

{{Imbox
| type = warning
| text = Samba does not support renaming the AD forest root domain.

Best practices:

* Use a domain name you own.
* Use a subdomain of your domain, such as ``ad.example.com</DOOTT
* Do not use ``.local</domains. They can cause problems with Mac OS X and Zeroconf.

For details, see `Active_Directory_Naming_FAQ|Active Directory Naming FAQ`.

----
`Category:rectory`
`Category: