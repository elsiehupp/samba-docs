Samba 4.1 Features added/
    <namespace>0</namespace>
<last_edited>2019-09-17T22:04:59Z</last_edited>
<last_editor>Fraz</last_editor>

      Samba 4.1 is `Samba_Release_Planning#Discontinued_.28End_of_Life.29|**Discontinued (End of Life)**`.

Samba 4.1.22
------------------------

* Notes for Samba 4.1.22
* 16, 2015

===============================
This is a security release in order to address the following CVEs:

*  [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2015-7540 CVE-2015-7540] (Remote DoS in Samba (AD) LDAP server)
*  [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2015-3223 CVE-2015-3223] (Denial of service in Samba Active Directory server)
*  [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2015-5252 CVE-2015-5252] (Insufficient symlink verification in smbd)
*  [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2015-5299 CVE-2015-5299] (Missing access control check in shadow copy code)
*  [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2015-5296 CVE-2015-5296] (Samba client requesting encryption vulnerable to downgrade attack)
*  [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2015-8467 CVE-2015-8467] (Denial of service attack against Windows Active Directory server)
*  [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2015-5330 CVE-2015-5330] (Remote memory read in Samba LDAP server)

Please note that if building against a system libldb, the required version has been bumped to ldb-1.1.24.  This is needed to ensure we build against a system ldb library that contains the fixes for [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2015-5330 CVE-2015-5330] and [http:SSLLAASS:HHcve.mitre.org/cgi-bin/cvenameDDOO/=CVE-20/3223 CVE-2015-3223].

===============================
Details
===============================

------------------------

*  [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2015-7540 CVE-2015-7540]::
 versions of Samba from 4.0.0 to 4.1.21 inclusive are vulnerable to an anonymous memory exhaustion attack in the samba daemon LDAP server.

* malicious client can send packets that cause the LDAP server provided by the AD DC in the samba daemon process to consume unlimited memory and be terminated.

*  [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2015-3223 CVE-2015-3223]::
 versions of Samba from 4.0.0 to 4.3.2 inclusive (resp. all ldb versions up to 1.1.23 inclusive) are vulnerable to a denial of service attack in the samba daemon LDAP server.

* malicious client can send packets that cause the LDAP server in the samba daemon process to become unresponsive, preventing the server from servicing any other requests.

 flaw is not exploitable beyond causing the code to loop expending CPU resources.

*  [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2015-5252 CVE-2015-5252]::
 versions of Samba from 3.0.0 to 4.3.2 inclusive are vulnerable to a bug in symlink verification, which under certain circumstances could allow client access to files outside the exported share path.

 a Samba share is configured with a path that shares a common path prefix with another directory on the file system, the smbd daemon may allow the client to follow a symlink pointing to a file or directory in that other directory, even if the share parameter "wide links" is set to "no" (the default).

*  [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2015-5299 CVE-2015-5299]::
 versions of Samba from 3.2.0 to 4.3.2 inclusive are vulnerable to a missing access control check in the vfs_shadow_copy2 module. When looking for the shadow copy directory under the share path the current accessing user should have DIRECTORY_LIST access rights in order to view the current snapshots.

 was not being checked in the affected versions of Samba.

*  [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2015-5296 CVE-2015-5296]::
* of Samba from 3.2.0 to 4.3.2 inclusive do not ensure that signing is negotiated when creating an encrypted client connection to a server.

* this a man-in-the-middle attack could downgrade the connection and connect using the supplied credentials as an unsigned, unencrypted connection.

*  [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2015-8467 CVE-2015-8467]::
 operating as an AD DC, is sometimes operated in a domain with a mix of Samba and Windows Active Directory Domain Controllers.

 versions of Samba from 4.0.0 to 4.3.2 inclusive, when deployed as an AD DC in the same domain with Windows DCs, could be used to override the protection against the MS15-096 /ECVE-2015-2535 security issue in Windows.

 to MS16-096 it was possible to bypass the quota of machine accounts a non-administrative user could create.  Pure Samba domains are not impacted, as Samba does not implement the SeMachineAccountPrivilege functionality to allow non-administrator users to create new computer objects.

*  [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2015-5330 CVE-2015-5330]::
 versions of Samba from 4.0.0 to 4.3.2 inclusive (resp. all ldb versions up to 1.1.23 inclusive) are vulnerable to a remote memory read attack in the samba daemon LDAP server.

* malicious client can send packets that cause the LDAP server in the samba daemon process to return heap memory beyond the length of the requested value.

 memory may contain data that the client should not be allowed to see, allowing compromise of the server.

 memory may either be returned to the client in an error string, or stored in the database by a suitabily privileged user.  If untrusted users can create objects in your database, please confirm that all DN and name attributes are reasonable.

===============================
Changes since 4.1.21:

*  Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11552 BUG #11552]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgiDDAAS/name.cgi?/AASSHH2015-8467 CVE-2015-8467]: samdb: Match :-096 :viour for userAccountControl.
*  Jeremy Allison <jra@samba.org>
*  [https://DOOTTsamba.org/show_bug/id=9187 BUG #9187]: : E[http:/SSLLAAS:TTmitreDD/i-bin/cvenameDDOO/=CVE-20/7540 CVE-2015-7540]: Bogus L request cause samba to use all the memory and be ookilled.
* [https://bugzilla.samba.org/DOOTTcgi?id=11325 BUG #11325]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgiDDAAS/name.cgi?/AASSHH2015-3223 CVE-2015-3223]: Fix LDAP  search expression attack DoS.
* [https://bugzilla.samba.org/DOOTTcgi?id=11395 BUG #11395]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgiDDAAS/name.cgi?/AASSHH2015-5252 CVE-2015-5252]: Fix insufficient:symlink verification (file access outside the share).
* [https://bugzilla.samba.org/DOOTTcgi?id=11529 BUG #11529]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgiDDAAS/name.cgi?/AASSHH2015-5299 CVE-2015-5299]: s3-shadow-c Fix missing acce: Echeck on snapdir.
* Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* [https://bugzilla.samba.org/DOOTTcgi?id=11599 BUG #11599]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgiDDAAS/name.cgi?/AASSHH2015-5330 CVE-2015-5330]: Fix remote : memory exploit in LDB.
*  Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11536 BUG #11536]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgiDDAAS/name.cgi?/AASSHH2015-5296 CVE-2015-5296]: Add man  the middle protection when forcing smb encryption on the client side.

 https://samba.org/samba/hi/aDDAA/T1DDOOT/tml

Samba 4.1.21
------------------------

* Notes for Samba 4.1.21
* 13, 2015

===============================
This is the last bugfix release of the Samba 4.1 release series.
===============================

------------------------

There will very likely be security releases beyond this point only.

===============================
Changes since 4.1.20:

*   Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11488 BUG #11488]: AvoidSS:oting problems in user's DNs.

 https://samba.org/samba/hi/aDDAA/T1DDOOT/tml

Samba 4.1.20
------------------------

* Notes for Samba 4.1.20
* 1, 2015

===============================
This is the latest stable release of Samba 4.1.
===============================

------------------------

===============================
Changes since 4.1.19:

*   Michael Adam <obnox@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11366 BUG #11366]: docs:SS: rhaul  description of "smb encrypt" to include SMB3 encryption.
* [https://bugzilla.samba.org/DOOTTcgi?id=11372 BUG #11372]: smbd:SS:x SMB: functionality of "smb encrypt".
*   Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10823 BUG #10823]: s3: :indd:  TALLOC_FREES:f uninitialized groups variable.
* [https://bugzilla.samba.org/DOOTTcgi?id=11328 BUG #11328]: Use :urce group sids obtained from pac logon_info.
* [https://bugzilla.samba.org/DOOTTcgi?id=11339 BUG #11339]: s3: :: U: Eseparate : to track become_root()/unbecome/ state.
* [https://bugzilla.samba.org/DOOTTcgi?id=11342 BUG #11342]: s3: :: C: cras: in do_smb_load_module().
* [https://bugzilla.samba.org/DOOTTcgi?id=11359 BUG #11359]: lib: :lace:  strsep :on (missing on Solaris).
*   Christian Ambach <ambi@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11170 BUG #11170]: s3:para:Hloadparm : 'testparm --show-all-parameters'.
*   Ralph Boehme <slow@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11426 BUG #11426]: s3DDAAS Use tal array in share allowedusers.
*   Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11373 BUG #11373]: s3DDAAS: Reset p in smbXsrv_connection_init_tables failure paths.
*   Justin Maggard <jmaggard@netgear.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=11320 BUG #11320]: s3DDAAS: Respect :ME_GROUP flag in sid lookup.
*   Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11061 BUG #11061]: Fix :n via MS Remote Desktop.
* [https://bugzilla.samba.org/DOOTTcgi?id=11081 BUG #11081]: s3:winb make :   pass a valid server to rpccli_netlogon_sam_network_logon*().
*   Anubhav Rakshit <anubhav.rakshit@gmail.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=11361 BUG #11361]: s3:libs Fix  b: Ein conversion of ea list to ea array.
*   Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11403 BUG #11403]: s3DDAAS: Leave s: e() if dfree command is used.
* [https://bugzilla.samba.org/DOOTTcgi?id=11404 BUG #11404]: s3DDAAS: Fix aSS:ssible null pointer dereference.
*   Roel van Meer <roel@1afa.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=11427 BUG #11427]: s3DDAAS: Compare  maximum allowed length of a NetBIOS name.

 https://samba.org/samba/hi/aDDAA/T1DDOOT/tml

Samba 4.1.19
------------------------

* Notes for Samba 4.1.19
 23, 2015

===============================
This is the latest stable release of Samba 4.1.
===============================

------------------------

===============================
Changes since 4.1.18:

*   Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11068 BUG #11068]: s3: :acls: : we  a hex number as %x, not %u.
* [https://bugzilla.samba.org/DOOTTcgi?id=11249 BUG #11249]: Make :gled names work with acl_xattr.
* [https://bugzilla.samba.org/DOOTTcgi?id=11295 BUG #11295]: Excessi: Ecli_resolve_path() usage can slow down transmission.
*   Ralph Boehme <slow@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11244 BUG #11244]: ErrorSS:de path doesn't call END_PROFILE.
* [https://bugzilla.samba.org/DOOTTcgi?id=11277 BUG #11277]: s3:smb2: Add :   last command in compound requests.
*   Alexander Bokovoy <ab@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11284 BUG #11284]: s4: :li/:dap: /tinue process CLDAP until all addresses are used.
*   Evangelos Foutras <evangelos@foutrelis.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=8780 BUG #8780]: s4:libS:ls:  build  gnutls 3.4.
*   David Holder <david.holder@erion.co.uk>
* [https://bugzilla.samba.org/DOOTTcgi?id=11283 BUG #11283]: s3: : en DNS connections for ADS client.
*   Steve Howells <steve.howells@moscowfirst.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10924 BUG #10924]: s4DDOOT:HHfsmo.py: Fix fsm: transfer exception.
*   Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11141 BUG #11141]: s3:winb Make :   remove pending io requests before closing client sockets.
* [https://bugzilla.samba.org/DOOTTcgi?id=11182 BUG #11182]: Fix :c triggered by smbd_smb2_request_notify_done() -> smbXsrv_session_find_channel() in smbd.
*   Christof Schmitt <cs@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11313 BUG #11313]: idmap_r Fix 'wb --gid-to-sid' query.
*   Uri Simchoni <urisimchoni@gmail.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=11267 BUG #11267]: libads::Record  ticket endtime for sealed ldap connections.

 https://samba.org/samba/hi/aDDAA/T1DDOOT/tml

Samba 4.1.18
------------------------

* Notes for Samba 4.1.18
 12, 2015

===============================
This is the latest stable release of Samba 4.1.
===============================

------------------------

===============================
Changes since 4.1.17:

* Michael Adam <obnox@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=8905 BUG #8905]: s3:winb: Do stop:group: numeration when a group has no gid.
* [https://bugzilla.samba.org/DOOTTcgi?id=11058 BUG #11058]: cli_con:nd: don't s on host == NULL.
* [https://bugzilla.samba.org/DOOTTcgi?id=11117 BUG #11117]: vfs_glu manpage corrections.
* [https://bugzilla.samba.org/DOOTTcgi?id=11143 BUG #11143]: s3DDAAS:: Fix cha user group lookup of trusted domains.
* Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10016 BUG #10016]: Fix : authentication.
* [https://bugzilla.samba.org/DOOTTcgi?id=10888 BUG #10888]: s3: :nt : "client use spnego principal = yes" code checks wrong name.
* [https://bugzilla.samba.org/DOOTTcgi?id=11079 BUG #11079]: s3: : li:: If:reusing a : r struct, check every cli->timout miliseconds if it's still valid before use.
* [https://bugzilla.samba.org/DOOTTcgi?id=11094 BUG #11094]: s3: :lient: :o leavesSS:  file handle open.
* [https://bugzilla.samba.org/DOOTTcgi?id=11144 BUG #11144]: Fix :ry leak in SMB2 notify handling.
* [https://bugzilla.samba.org/DOOTTcgi?id=11173 BUG #11173]: s3: :li:  Ensure : correc finish a tevent req if the writev fails in the SMB1 case.
* [https://bugzilla.samba.org/DOOTTcgi?id=11177 BUG #11177]: s3: :mbclient: : missing :c stackframe.
* [https://bugzilla.samba.org/DOOTTcgi?id=11186 BUG #11186]: s3: :mbclient: : r getting :ribute server, ensure main srv pointer is still valid.
* [https://bugzilla.samba.org/DOOTTcgi?id=11187 BUG #11187]: s3: : OSS: 10.10.x fails validate negotiate request to 4.1.x.
* [https://bugzilla.samba.org/DOOTTcgi?id=11236 BUG #11236]: s4: : Re dcesrv_ function into setup and send steps.
* [https://bugzilla.samba.org/DOOTTcgi?id=11240 BUG #11240]: s3: :: I: fileSS:ze returned in the response of "FILE_SUPERSEDE Create".
* [https://bugzilla.samba.org/DOOTTcgi?id=11254 BUG #11254]: s3: :: D set :anged = True inside update_server_ttl().
* Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11100 BUG #11100]: debug:S: t clo:on-exec for the main log file FD.
* Ralph Boehme <slow@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11224 BUG #11224]: s3:smbd: Missing :nt_r:.
* [https://bugzilla.samba.org/DOOTTcgi?id=11243 BUG #11243]: vfs: :nel_flock : named streams.
*  Ira Cooper <ira@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11069 BUG #11069]: vfs_glu Add com to the pipe(2) code.
* [https://bugzilla.samba.org/DOOTTcgi?id=11115 BUG #11115]: smbd:SS:op us vfs_Chdir after SMB_VFS_DISCONNECT.
* Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10240 BUG #10240]: vfs: : glu manpage.
* David Disseldorp <ddiss@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10808 BUG #10808]: printin:Hcups: Pack re:ASSHHattributes with IPP_TAG_KEYWORD.
* [https://bugzilla.samba.org/DOOTTcgi?id=11018 BUG #11018]: smbd :'t find the GUID for a printer in the registry and fails to publish printers.
* [https://bugzilla.samba.org/DOOTTcgi?id=11059 BUG #11059]: libsmb::Provide : domain for encrypted session referrals.
* [https://bugzilla.samba.org/DOOTTcgi?id=11169 BUG #11169]: docsSSL:ap_rid: Remove : base_rid from example.
* [https://bugzilla.samba.org/DOOTTcgi?id=11210 BUG #11210]: spoolss: Purge t: Eprinter name cache on name change.
* Julien Kerihuel <j.kerihuel@openchange.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11225 BUG #11225]: s4:rpc_ AddSS:ltiplex :te to dcerpc flags and control over multiplex PFC flag in bind_ack and and dcesrv_alter replies.
* [https://bugzilla.samba.org/DOOTTcgi?id=11226 BUG #11226]: Fix :inate connection behavior for asynchronous endpoint with PUSH notification flavors.
* Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11041 BUG #11041]: smbd:SS:x CID:1063259 Uninitialized scalar variable.
* [https://bugzilla.samba.org/DOOTTcgi?id=11051 BUG #11051]: net: : 'ne: sam addgroupmem'.
* Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=9702 BUG #9702]: s3:smb2: prot againstS:nteger wrap with "smb2 max credits = 65535".
* [https://bugzilla.samba.org/DOOTTcgi?id=11144 BUG #11144]: Fix :ry leak in SMB2 notify handling.
* [https://bugzilla.samba.org/DOOTTcgi?id=11164 BUG #11164]: s4:auth:gensec_gssap: Elet gensec_gssap: return NT_STATUS_LOGON_FAILURE for unknown errors.
* Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10984 BUG #10984]: spoolss: Clear P: on GetPrinter error.
* [https://bugzilla.samba.org/DOOTTcgi?id=11008 BUG #11008]: s3DDAAS: Fix aut:n with long hostnames.
* [https://bugzilla.samba.org/DOOTTcgi?id=11037 BUG #11037]: s3DDAAS: Fix aSS:ssible segfault in kerberos_fetch_pac().
* [https://bugzilla.samba.org/DOOTTcgi?id=11058 BUG #11058]: utils:S:ix 'ne: time' segfault.
* [https://bugzilla.samba.org/DOOTTcgi?id=11066 BUG #11066]: s3DDAAS:pass: Fix mem leak in pam_sm_authenticate().
* [https://bugzilla.samba.org/DOOTTcgi?id=11127 BUG #11127]: docDDAA: Add 'sh reference to 'access based share enum'.
* [https://bugzilla.samba.org/DOOTTcgi?id=11180 BUG #11180]: s4DDAAS:_model: Do notS:lose random fds while forking.
* [https://bugzilla.samba.org/DOOTTcgi?id=11185 BUG #11185]: s3DDAAS: Fix 'fo user' with winbind default domain.
* Richard Sharpe <rsharpe@nutanix.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=11234 BUG #11234]: Fix :h in 'net ads dns gethostbyname' with an error in TALLOC_FREE if you enter invalid values.

 https://samba.org/samba/hi/aDDAA/T1DDOOT/tml

Samba 4.1.17
------------------------

* Notes for Samba 4.1.17
* 23, 2015

===============================
This is a security release in order to address [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2015-0240 CVE-2015-0240].
===============================

------------------------

* EUnexpected code execution in smbd

*  [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2015-0240 CVE-2015-0240]::
 versions of Samba from 3.5.0 to 4.2.0rc4 are vulnerable to an unexpected code execution vulnerability in the smbd file server daemon.

* malicious client could send packets that may set up the stack in such a way that the freeing of memory in a subsequent anonymous netlogon packet could allow execution of arbitrary code. This code would execute with root privileges.

===============================
Changes since 4.1.16:
*   Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11077 BUG #11077]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgiDDAAS/name.cgi?/AASSHH2015-0240 CVE-2015-0240]: talloc free  uninitialized stack pointer in netlogon server could lead to security vulnerability.
*   Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11077 BUG #11077]: CVEDDAA:AASSHH0240: s3-netlog Make sure  do not deference a NULL pointer.

 https://samba.org/samba/hi/aDDAA/T1DDOOT/tml

Samba 4.1.16
------------------------

* Notes for Samba 4.1.16
* 15, 2015

===============================
This is a security release in order to address [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2014-8143 CVE-2014-8143]
===============================

------------------------

This is a security release in order to address [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2014-8143 CVE-2014-8143] (Elevation of privilege to Active Directory Domain Controller).

*  [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2014-8143 CVE-2014-8143]::
* AD DC allows the administrator to delegate creation of user or computer accounts to specific users or groups.

* all released versions of Samba's AD DC did not implement the additional required check on the UF_SERVER_TRUST_ACCOUNT bit in the userAccountControl attributes.

===============================
Changes since 4.1.15:

*   Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10993 BUG #10993]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgiDDAAS/name.cgi?/AASSHH2014-8143 CVE-2014-8143]: dsdb-samldb: : for extend: Eaccess rights before we allow changes to userAccountControl.

 https://samba.org/samba/hi/aDDAA/T1DDOOT/tml

Samba 4.1.15
------------------------

* Notes for Samba 4.1.15
* 12, 2015

===============================
This is the latest stable release of Samba 4.1.
===============================

------------------------

===============================
Changes since 4.1.14:
*   Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10966 BUG #10966]: libcli::SMB2: P SMB2DDAASSH negprot fix to make us behave as a Windows client does.
* [https://bugzilla.samba.org/DOOTTcgi?id=10982 BUG #10982] s3: smbdSSL:ules: Fix *al calls to follow POSIX error return convention.
*   Christian Ambach <ambi@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=9629 BUG #9629]: Fix :iles tool.
*   Samuel Cabrero <scabrero@zentyal.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=11006 BUG #11006]: idl:drs ManageSS:l :le lengths of drsuapi_DsBindInfo.
*   Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=9056 BUG #9056]: pam_win Fix war:re implementation.
*   Guenter Kukkukk <linux@kukkukk.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10952 BUG #10952]B: s4DDAAS dnsserver:  enumerationS:f IPv4 and IPv6 addresses.
*   Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=9299 BUG #9299]BUG 9299: nsswitc: EFix son of linux nss_*.so.2 modules.
* [https://bugzilla.samba.org/DOOTTcgi?id=10949 BUG #10949]BUG 10949: s4:dsdb:rootdse: :nd extendedS:n values with the AS_SYSTEM control.
* [https://bugzilla.samba.org/DOOTTcgi?id=10958 BUG #10958]BUG 10958: s3:smb2: Allo: reauthenti without signing.
* [https://bugzilla.samba.org/DOOTTcgi?id=11006 BUG #11006]BUG 11006: Fix :ain join' by adding 'drsuapi.DsBindInfoFallBack' attribute 'supported_extensions'.
*   Matthieu Patou <mat@matws.net>
* [https://bugzilla.samba.org/DOOTTcgi?id=11006 BUG #11006]: Fix :ain join' by adding 'drsuapi.DsBindInfoFallBack' attribute 'supported_extensions'.
*   Christof Schmitt <cs@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11034 BUG #11034]: winbind: Retry L:l RPC in ping-dc after session expiration.
*   Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10279 BUG #10279]: s3DDAAS Do notS: quire a password with --use-ccache.
* [https://bugzilla.samba.org/DOOTTcgi?id=10960 BUG #10960]: s3DDAAS:nt: Return  if we listed the shares.
* [https://bugzilla.samba.org/DOOTTcgi?id=10961 BUG #10961]: s3DDAAS:us: Fix exi: code of profile output.

 https://samba.org/samba/hi/aDDAA/T1DDOOT/tml

Samba 4.1.14
------------------------

* Notes for Samba 4.1.14
* 1, 2014

===============================
This is the latest stable release of Samba 4.1.
===============================

------------------------

===============================
Changes since 4.1.13:
* Michael Adam <obnox@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10472 BUG #10472]: RevertS:uildtools/wafadmin/l.p/Eback/to upstream state.
*   Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10711 BUG #10711]: nmbd :ls to accept "--piddir" option.
* [https://bugzilla.samba.org/DOOTTcgi?id=10896 BUG #10896]: s3DDAAS: Fix net name truncation.
* [https://bugzilla.samba.org/DOOTTcgi?id=10904 BUG #10904]: s3: :mbclientDDAA:OOTT MacOSX 10 SMB2 server doesn't set  STATUS_NO_MORE_FILES when handed a non-wildcard path.
* [https://bugzilla.samba.org/DOOTTcgi?id=10920 BUG #10920]: s3: :: E NetBIOSSS:mes are only 15 characters stored.
*   Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10942 BUG #10942]: Cleanup:add_string_to_array and usage.
*   David Disseldorp <ddiss@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10898 BUG #10898]: spoolss: Fix han of bad EnumJobs levels.
* [https://bugzilla.samba.org/DOOTTcgi?id=10905 BUG #10905]: spoolss: Fix job: Ein level 3 EnumJobs response.
*   Amitay Isaacs <amitay@gmail.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10620 BUG #10620]: s4DDAAS Add sup for BIND 9.10.
*   Björn Jacke <bj@sernet.de>
* [https://bugzilla.samba.org/DOOTTcgi?id=10835 BUG #10835]: nss_win Add get:rship for FreeBSD.
*   Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10932 BUG #10932]: pdb_tdb: Fix aSS:LLOC/SAFE_FRE/Emixup.
*   Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10472 BUG #10472]: pidlSSL:ript: Remove :AASSHHwith-perl-* options.
* [https://bugzilla.samba.org/DOOTTcgi?id=10921 BUG #10921]: s3:smbd: Fix :AACC:on using "write cache size != 0".
*   Jose A. Rivera <jarrpa@redhat.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10889 BUG #10889]: vfs_glu Remove :ger fd" code and store the glfs pointers.
*   Matt Rogers <mrogers@redhat.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10933 BUG #10933]: s3DDAAS: Fix key array NULL termination.
*   Richard Sharpe <realrichardsharpe@gmail.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10880 BUG #10880]: S3: :ce3SSLLAASSH:ASSHHprocess.c::srv_send_smb() returns :: on the error path.

 http://samba.org/samba/hi/aDDAA/T1DDOOT/tml

Samba 4.1.13
------------------------

* Notes for Samba 4.1.13
* 20, 2014

===============================
This is the latest stable release of Samba 4.1.
===============================

------------------------

===============================
Changes since 4.1.12:

* Michael Adam <obnox@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10809 BUG #10809]: s3:smbd:: U: EaS:ore :l check.
* Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10717 BUG #10717]: s3: :indd:  NT  code sets struct winbind_domain->alt_name to be NULL. Ensure this is safe with modern AD-DCs.
* [https://bugzilla.samba.org/DOOTTcgi?id=10779 BUG #10779]: pthread Slightly :  jobs.
* [https://bugzilla.samba.org/DOOTTcgi?id=10809 BUG #10809]: s3: :: O logic :OTT
* [https://bugzilla.samba.org/DOOTTcgi?id=10830 BUG #10831]: s3: :: E the : nmbd process doesn't create zombies.
* [https://bugzilla.samba.org/DOOTTcgi?id=10831 BUG #10831]: s3: : Si handlingS:DAASSHH ensure smbrun and change password code save and restore existing SIGCHLD handlers.
* [https://bugzilla.samba.org/DOOTTcgi?id=10848 BUG #10848]: s3: :cli:  info : length check was reversed.
* Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=9984 BUG #9984]: s3DDAAS: Make su: Ewe do not overwrite precreated SPNs.
* Börn Jacke <bj@sernet.de>
* [https://bugzilla.samba.org/DOOTTcgi?id=10814 BUG #10814]: docs:SS:ntion :bility between kernel oplocks and streams_xattr.
* Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10735 BUG #10735]: Fix :rcpy.
* [https://bugzilla.samba.org/DOOTTcgi?id=10797 BUG #10797]: s3: :: s -: nsure share mode validation ignores internal opens (op_mid == 0).
* [https://bugzilla.samba.org/DOOTTcgi?id=10813 BUG #10813]: vfs_med:: Fix aSS:ash bug.
* [https://bugzilla.samba.org/DOOTTcgi?id=10860 BUG #10860]: registr: EDon't l dangling transactions.
* Christof Schmitt <cs@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10826 BUG #10826]: s3DDAAS:d: Use cor realm for trusted domains in idmap child.
* [https://bugzilla.samba.org/DOOTTcgi?id=10837 BUG #10837]: idmap_r Fix aSS:ash after connection problem to DC.
* [https://bugzilla.samba.org/DOOTTcgi?id=10838 BUG #10838]: s3DDAAS:d: Do notS:se domain SID from LookupSids for Sids2UnixIDs call.
* Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=9984 BUG #99846]: s3DDAAS: Add lib: t_machine_spns().
* [https://bugzilla.samba.org/DOOTTcgi?id=9985 BUG #9985]: s3DDAAS: Add all:machine account principals to the keytab.
* [https://bugzilla.samba.org/DOOTTcgi?id=10816 BUG #10816]: nmbd:SS:nd wa status to systemd.
* [https://bugzilla.samba.org/DOOTTcgi?id=10817 BUG #10817]: libcli::Fix aSS:gfault calling smbXcli_req_set_pending() on NULL.
* [https://bugzilla.samba.org/DOOTTcgi?id=10824 BUG #10824]: nsswitc: ESkip gr we were not able to map.

 http://samba.org/samba/hi/aDDAA/T1DDOOT/tml

Samba 4.1.12
------------------------

* Notes for Samba 4.1.12
* 8, 2014

===============================
This is the latest stable release of Samba 4.1.
===============================

------------------------

===============================
Major enhancements in Samba 4.1.12 include:

*  [https://DOOTTsamba.org/show_bug/id=3204 BUG #3204]New parameter "winbind request timeout" has been added. Please see smb.conf man page for details.
*  [https://DOOTTsamba.org/show_bug/id=10716 BUG #10716]Fix smbd crashes when filename contains non-ascii character.
*  [https://DOOTTsamba.org/show_bug/id=10749 BUG #10749]dnsserver: : updates of tombstoned dnsNode objects.

===============================
Changes since 4.1.11:

*   Michael Adam <obnox@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10369 BUG #10369]: build:S:ix con to honour '--without-dmapi'.
* [https://bugzilla.samba.org/DOOTTcgi?id=10737 BUG #10737]: s3:idma: EDon't :  : range config if range checking not requested.
* [https://bugzilla.samba.org/DOOTTcgi?id=10741 BUG #10741]: Fix :ping VFS gpfs offline bit.
*   Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=3204 BUG #3204]: s3: :indd:  new  connect, prune idle or hung connections older than "winbind request timeout". Add new parameter "winbind request timeout".
* [https://bugzilla.samba.org/DOOTTcgi?id=10640 BUG #10640]: lib: : nt:  TEVENT_SIG_: atomic.
* [https://bugzilla.samba.org/DOOTTcgi?id=10650 BUG #10650]: Make :ot;case sensitive = True" option working with "max protocol = SMB2" or higher in large directories.
* [https://bugzilla.samba.org/DOOTTcgi?id=10716 BUG #10716]: Fix : crashes when filename contains non-ascii character.
* [https://bugzilla.samba.org/DOOTTcgi?id=10728 BUG #10728]: 'net : ': Fix usa: Eand core dump.
* [https://bugzilla.samba.org/DOOTTcgi?id=10773 BUG #10773]: s3: :: P ACLs.:Remove incorrect check for SECINFO_PROTECTED_DACL in incoming security_information flags in posix_get_nt_acl_common().
* [https://bugzilla.samba.org/DOOTTcgi?id=10794 BUG #10794]: vfs_dir Fix anS:ff-by-one error that can cause uninitialized memory read.
*   Björn Baumbach <bb@sernet.de>
* [https://bugzilla.samba.org/DOOTTcgi?id=10543 BUG #10543]: s3: :rce : Epositive allocation_file_size for non-empty files.
*   Kai Blin <kai@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10466 BUG #10466]: provisi Correctly :on the SOA record minimum TTL.
*   David Disseldorp <ddiss@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10652 BUG #10652]: SambaSS: consuming a lot of CPU when re-reading printcap info.
* [https://bugzilla.samba.org/DOOTTcgi?id=10787 BUG #10787]: dosmode: Fix FSC:RSE request validation.
*   Amitay Isaacs <amitay@gmail.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10742 BUG #10742]: s4DDAAS dnsserver: : . : be specified for @ record.
*   Daniel Kobras <d.kobras@science-computing.de>
* [https://bugzilla.samba.org/DOOTTcgi?id=10731 BUG #10731]: sys_pol Fix tim arithmetic.
*   Ross Lagerwall <rosslagerwall@gmail.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10778 BUG #10778]: s3:libs Set  m: Echarge for SMB2 connections.
*   Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10716 BUG #10716]: lib: :ings: : strcase:
* [https://bugzilla.samba.org/DOOTTcgi?id=10758 BUG #10758]: lib: :ove  nstrcpy.
* [https://bugzilla.samba.org/DOOTTcgi?id=10782 BUG #10782]: smbd:SS:operly :ze mangle_hash.
*   Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=9831 BUG #9831]: s4:setu:Hdns_update_ make use  the new substitution variables.
* [https://bugzilla.samba.org/DOOTTcgi?id=10723 BUG #10723]: AllowSS:tr_ServerReqChallenge() and netr_ServerAuthenticate3() on different connections.
* [https://bugzilla.samba.org/DOOTTcgi?id=10749 BUG #10749]: s4DDAAS dnsserver: : updatesSS: tombstoned dnsNode objects.
* [https://bugzilla.samba.org/DOOTTcgi?id=10751 BUG #10751]: s4DDAAS dnsserver: : DNS_RANK_ recors when explicitly asked for.
* [https://bugzilla.samba.org/DOOTTcgi?id=10773 BUG #10773]: libcliS: curity: Add bet detection of SECINFO_[UN]PROTECTED_[D|S]ACL in get_sec_info().
*   Marc Muehlfeld <mmuehlfeld@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10761 BUG #10761]: docs:SS:x typ: Ein smb.conf (inherit acls).
*   Shirish Pargaonkar <spargaonkar@suse.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10755 BUG #10755]: samba:S: tain  sensitivity of cifs client.
*   Arvid Requate <requate@univention.de>
* [https://bugzilla.samba.org/DOOTTcgi?id=9570 BUG #9570]: passdb::Fix NT_:SUCH_GROUP.
*   Har Gagan Sahai <SHarGagan@novell.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10759 BUG #10759]: Fix  memory leak in cli_set_mntpoint().
*   Roel van Meer <roel@1afa.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10777 BUG #10777]: Don'tSS:scard result of checking grouptype.

 http://samba.org/samba/hi/aDDAA/T1DDOOT/tml

Samba 4.1.11
------------------------

* Notes for Samba 4.1.11
 1, 2014

===============================
This is a security release in order to address
===============================

------------------------

* [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2014-3560 CVE-2014-3560] (Remote code execution in nmbd).

* [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2014-3560 CVE-2014-3560]::
* :OOTT0.0 to 4.1.10 are affected by a remote code execution attack on unauthenticated nmbd NetBIOS name services.

* :us browser can send packets that may overwrite the heap of the target nmbd NetBIOS name services daemon. It may be possible to use this to generate a remote code execution vulnerability as the superuser (root).

===============================
Changes since 4.1.10:
*   Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10735 BUG 10735] : [http:S:SLLAASSHHcveDDO:OOTTorg/cgiDDAAS/name.cgi?/AASSHH2014-3560 CVE-2014-3560]: Fix unstrcpy :o length.

 http://samba.org/samba/hi/aDDAA/T1DDOOT/tml

Samba 4.1.10
------------------------

* Notes for Samba 4.1.10
 28, 2014

===============================
This is the latest stable release of Samba 4.1.
===============================

------------------------

* Backport ldb-1.1.17 + changes from master

===============================
Changes since 4.1.9:

*   Michael Adam <obnox@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10693 BUG #10693]: Backpor: ldb-1.1.17 + changes from master.
*   Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10587 BUG #10587]: s3: :mbclient: :k around : in SLES cifsd and Apple smbx SMB1 servers.
* [https://bugzilla.samba.org/DOOTTcgi?id=10653 BUG #10653]: SambaSS:n't start on a machine configured with only IPv4.
* [https://bugzilla.samba.org/DOOTTcgi?id=10671 BUG #10671]: s3: :: P file :cation on an open that fails with share mode violation.
* [https://bugzilla.samba.org/DOOTTcgi?id=10673 BUG #10673]: s3: :: F: Eleak  blocking lock records in the database.
* [https://bugzilla.samba.org/DOOTTcgi?id=10684 BUG #10684]: SMB1 :cking locks can fail notification on unlock, causing client timeout.
* [https://bugzilla.samba.org/DOOTTcgi?id=10685 BUG #10685]: s3: :: L fix :DAASSHHby one calculation in brl_pending_overlap().
* [https://bugzilla.samba.org/DOOTTcgi?id=10692 BUG #10692]: wbcCred:  fails if challenge_blob is not first.
*   Christian Ambach <ambi@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10693 BUG #10693]: libSSLL: Fix com warnings.
*  Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=8077 BUG #8077]: dbcheck: Add che: Eand test for various invalid userParameters values.
* [https://bugzilla.samba.org/DOOTTcgi?id=8499 BUG #8499]: SimpleS:se case results in "no talloc stackframe around, leaking memory" error.)
* [https://bugzilla.samba.org/DOOTTcgi?id=10130 BUG #10130]: dsdb:SS:ways  and return the userParameters as a array of LE 16-bit values.
* [https://bugzilla.samba.org/DOOTTcgi?id=10582 BUG #10582]: dsdb:SS:name :ta to rootdse_private_data in rootdse.
* [https://bugzilla.samba.org/DOOTTcgi?id=10627 BUG #10627]: rid_arr: Eused before status checked - segmentation fault due to null pointer dereference.
* [https://bugzilla.samba.org/DOOTTcgi?id=10693 BUG #10693]: ldb: :  th: successful ldb_transaction_start() message clearer.
* [https://bugzilla.samba.org/DOOTTcgi?id=10694 BUG #10694]: dsdb:SS:turn :JECT if a basedn is a deleted object.
* [https://bugzilla.samba.org/DOOTTcgi?id=10700 BUG #10700]: Backpor: access check related fixes from master.
*   Björn Baumbach <bb@sernet.de>
* [https://bugzilla.samba.org/DOOTTcgi?id=10674 BUG #10674]: sambaDD:: Add DDA:SHHsite parameter to provision command.
*   Howard Chu <hyc@symas.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10693 BUG #10693]: Fix : from improperly formed SUBSTRING/PRESENCE/filter.
*   Jeroen Dekkers <jeroen@dekkers.ch>
* E* [https://bugzilla.samba.org/DOOTTcgi?id=10693 BUG #10693]: ldb: : notS:uild libldb-cmdline when using system ldb.
*   Nadezhda Ivanova <nivanova@symas.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10693 BUG #10693]: s4DDAAS:p: Remove  of talloc_reference in ldb_map_outbound.c
*   Björn Jacke <bj@sernet.de>
* [https://bugzilla.samba.org/DOOTTcgi?id=3263 BUG #3263]: netSSLL: Make cl that net vampire is for NT4 domains only.
*   Abhidnya Joshi <achirmul@in.ibm.com>
* BUG s3:  missing braces in nfs4_acls.c.
*   Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10593 BUG #10593]: Fix :t;PANIC: assert  at ../source3//DOOTTc(/AACC/;.:
* [https://bugzilla.samba.org/DOOTTcgi?id=10633 BUG #10633]: msg_cha Fix aSS:0% CPU loop.
* [https://bugzilla.samba.org/DOOTTcgi?id=10671 BUG #10671]: s3: :: P file :cation on an open that fails with share mode violation.
* [https://bugzilla.samba.org/DOOTTcgi?id=10680 BUG #10680]: smbstat Fix anS:ninitialized variable.
* [https://bugzilla.samba.org/DOOTTcgi?id=10687 BUG #10687]: 'RW2'SS:btorture test fails when -N <numprocs> is set to 2 due to the invalid status check in the second client.
* [https://bugzilla.samba.org/DOOTTcgi?id=10693 BUG #10693]: ldb: : 113 Dereference null return value, fix CIDs 241329, 240798, 1034791, 1034792 1034910, 1034910).
* [https://bugzilla.samba.org/DOOTTcgi?id=10699 BUG #10699]: smbd:SS:oid d:SHHfree in get_print_db_byname.
* Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=8077 BUG #8077]: s4:dsdb:samldb: : allow :Parameters' to be modified over LDAP for now.
* [https://bugzilla.samba.org/DOOTTcgi?id=9752 BUG #9752]: s4:dsdb:repl_meta_da Make sure :UID can't be deleted.
* [https://bugzilla.samba.org/DOOTTcgi?id=10469 BUG #10469]: ldbDDAA: fix aSS:mory leak in ldif_canonicalise_objectCategory().
* [https://bugzilla.samba.org/DOOTTcgi?id=10294 BUG #10294]: s4:repl:: f: Earray : nt in replmd_process_linked_attribute().
* [https://bugzilla.samba.org/DOOTTcgi?id=10536 BUG #10536]: dbcheck Verify  fix broken dn values.
* [https://bugzilla.samba.org/DOOTTcgi?id=10692 BUG #10692]: wbcCred:  fails if challenge_blob is not first.
* [https://bugzilla.samba.org/DOOTTcgi?id=106i3 BUG #10693]: ldb:pyl Add :PAACC helper functions for LdbDn.
* [https://bugzilla.samba.org/DOOTTcgi?id=10694 BUG #10694]: s4:dsdb: xtended_dn_ Don't force :SEARCH_SHOW_RECYCLED.
* [https://bugzilla.samba.org/DOOTTcgi?id=10696 BUG #10696]: Backpor: autobuild/selftest/fixes from master.
* [https://bugzilla.samba.org/DOOTTcgi?id=10706 BUG #10706]: s3:smb2 let :2_sendfil:a() behave like send_file_readX().
* Matthieu Patou <mat@matws.net>
* [https://bugzilla.samba.org/DOOTTcgi?id=10693 BUG #10693]: pyldb:S: crement  counters on py_results and quiet warnings.
* [https://bugzilla.samba.org/DOOTTcgi?id=10698 BUG #10698]: Backpor: drs-crackname fixes from master.
* Pavel Reichl <pavel.reichl@redhat.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10693 BUG #10693]: ldb: : ofS:ULL pointer bugfix.
*   Garming Sam <garming@catalyst.net.nz>
* [https://bugzilla.samba.org/DOOTTcgi?id=10703 BUG #10703]: Backpor: provision fixes from master.
*   Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10693 BUG #10693]: ldb: : aSS:v variable to disable RTLD_DEEPBIND.

 http://samba.org/samba/hi/aDDAA/T1DDOOT/tml

Samba 4.1.9
------------------------

* Notes for Samba 4.1.9
 23, 2014

===============================
This is a security release in order to address
===============================

------------------------

* [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2014-0244 CVE-2014-0244]
 current released versions of Samba are vulnerable to a denial of service on the nmbd NetBIOS name services daemon. A malformed packet can cause the nmbd server to loop the CPU and prevent any further NetBIOS name service.
 flaw is not exploitable beyond causing the code to loop expending CPU resources.

* [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2014-3493 CVE-2014-3493]
* EAll current released versions of Samba are affected by a denial of service crash involving overwriting memory on an authenticated connection to the smbd file server.

===============================
Changes since 4.1.8:

*   Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10633 BUG #10633]BUG 10633: [http:S:SLLAASSHHcveDDO:OOTTorg/cgiDDAAS/name.cgi?/AASSHH2014-0244 CVE-2014-0244]: Fix nmbd : of service.
* [https://bugzilla.samba.org/DOOTTcgi?id=10654 BUG #10654]BUG 10654: [http:S:SLLAASSHHcveDDO:OOTTorg/cgiDDAAS/name.cgi?/AASSHH2014-3493 CVE-2014-3493]: Fix segmentation:fault in smbd_marshall_dir_entry()'s SMB_FIND_FILE_UNIX handler.

 http://samba.org/samba/hi/aDDAA/T1DDOOT/ml

Samba 4.1.8
------------------------

* Notes for Samba 4.1.8
 3, 2014

===============================
This is the latest stable release of Samba 4.1.
===============================

------------------------

Please note that this bug fix release also addresses two minor security issues
without being a dedicated security release:

* [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2014-0239 CVE-2014-0239]:  Don'tSS:ply to replies (bug #10609).
* [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2014-0178 CVE-2014-0178]: :d FSCTL_SRV_ENUMERATE_SNAPSHOTS response ([https:/SSLLAAS:a.samba./ug.cgi?id=10549 /CCEE#10549]).

For more details including security advisories and patches, please see

    ttp://samba.org/samba/hi/rityD//

===============================
Changes since 4.1.7:
*   Michael Adam <obnox@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10548 BUG #10548]: build:S:ix ord problems with lib-provided and internal RPATHs.
*   Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=3124 BUG #3124]: s3: :: F: E'xcopy :SSHHd' with samba shares.
* [https://bugzilla.samba.org/DOOTTcgi?id=10544 BUG #10544]: s3: :SLLAASSHHuti: EFix logic : set_namearray loops.
* [https://bugzilla.samba.org/DOOTTcgi?id=10564 BUG #10564]: Fix : order violation and file lost.
* [https://bugzilla.samba.org/DOOTTcgi?id=10577 BUG #10577]: Fix :card unlink to fail if we get an error rather than trying to continue.
*   Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10569 BUG #10569]: dsdb:SS: chec: Efor invalid renames in samldb, before repl_meta_data.
*   Björn Baumbach <bb@sernet.de>
* [https://bugzilla.samba.org/DOOTTcgi?id=10239 BUG #10239]: s3: :: R debug :ngs after reading config file.
* [https://bugzilla.samba.org/DOOTTcgi?id=10544 BUG #10544]: s3: :SLLAASSHHuti: Eset_namearray re across end of namelist
* [https://bugzilla.samba.org/DOOTTcgi?id=10556 BUG #10556]: libDDAA: Rename  to smb_memdup and fix all callers.
*   Kai Blin <kai@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10609 BUG #10609]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgiDDAAS/name.cgi?/AASSHH2014-0239 CVE-2014-0239]: dns: Don't : toSS:plies.
*   Alexander Bokovoy <ab@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10517 BUG #10517]: Use :_daemon() to communicate status of startup to systemd.
*   David Disseldorp <ddiss@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10590 BUG #10590]: byteord Do notS:ssume PowerPC is big-endian.
* [https://bugzilla.samba.org/DOOTTcgi?id=10612 BUG #10612]: printin: EFix pur: Eof all print jobs.
*   Benjamin Franzke <benjaminfranzke@googlemail.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10524 BUG #10524]: Fix :ng NetApps.
*   Abhidnya Joshi <achirmul@in.ibm.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10547 BUG #10547]: idmap_a Fix fai in reverse lookup if ID is from domain range index #0.
*   Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10472 BUG #10472]: scriptS:utobuild: Make us: of '--with-perl-{arch,lib}-install-dir'.
*   Noel Power <nopower@suse.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10554 BUG #10554]: Fix : of deleted memory in reply_writeclose()'.
*   Jose A. Rivera <jarrpa@redhat.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10151 BUG #10151]: ExtraSS:' in msg:for Waf Cross Compile Build System with Cross-answers command.
* [https://bugzilla.samba.org/DOOTTcgi?id=10348 BUG #10348]: Fix :y body in if-statement in continue_domain_open_lookup.
*   Christof Schmitt <christof.schmitt@us.ibm.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10549 BUG #10549]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgiDDAAS/name.cgi?/AASSHH2014-0178 CVE-2014-0178]: Malformed FSCTL_:ATE_SNAPSHOTS response.
*   Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10472 BUG #10472]: wafsamb: EFix the:installation on FreeBSD.

 http://samba.org/samba/hi/aDDAA/T1DDOOT/ml

Samba 4.1.7
------------------------

* Notes for Samba 4.1.7
 17, 2014
===============================
This is the latest stable release of Samba 4.1.
===============================

------------------------

===============================
Changes since 4.1.6:

*   Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=9878 BUG #9878]: Make :ot;force user" work as expected.
* [https://bugzilla.samba.org/DOOTTcgi?id=9942 BUG #9942]: Fix :lem with server taking too long to respond to a MSG_PRINTER_DRVUPGRADE message.
* [https://bugzilla.samba.org/DOOTTcgi?id=9993 BUG #9993]: s3DDAAS:g: Fix obv memory leak in printer_list_get_printer().
* [https://bugzilla.samba.org/DOOTTcgi?id=10344 BUG #10344]: Session on a signed connection with an outstanding notify request crashes smbd.
* [https://bugzilla.samba.org/DOOTTcgi?id=10431 BUG #10431]: Fix :US_NO_MEMORY response from Query File Posix Lock request.
* [https://bugzilla.samba.org/DOOTTcgi?id=10508 BUG #10508]: smbd:SS:rrectly  remote users into local groups.
* [https://bugzilla.samba.org/DOOTTcgi?id=10534 BUG #10534]: Cleanup:messages.tdb record after unclean smbd shutdown.
*   Christian Ambach <ambi@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=9911 BUG #9911]: Fix :d on AIX with IBM XL C/C++ /ext detection issues).
* [https://bugzilla.samba.org/DOOTTcgi?id=10308 BUG #10308]: Fix :ng Conversion Errors with Samba 4.1.0 Build on AIX 7.1.
*   Gregor Beck <gbeck@sernet.de>
* [https://bugzilla.samba.org/DOOTTcgi?id=10230 BUG #10230]: Make :b)smbclient work with NetApp.
* [https://bugzilla.samba.org/DOOTTcgi?id=10458 BUG #10458]: Fix :nfo -i' with one-way trust.
* s3:rpc_serv MinorSS:factoring of process_request_pdu().
*   Kai Blin <kai@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10471 BUG #10471]: Don'tSS:spond with NXDOMAIN to records that exist with another type.
*   Alexander Bokovoy <ab@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10504 BUG #10504]: lsaDDOO Define :orestTrustCollisionInfo and ForestTrustCollisionRecord as public structs.
*   Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10439 BUG #10439]: Increas: max netbios name components.
*   David Disseldorp <ddiss@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10188 BUG #10188]: doc: : &qu:: architecture" : r usage.
* [https://bugzilla.samba.org/DOOTTcgi?id=10484 BUG #10484]: Initial:FSRVP rpcclient requests fail with NT_STATUS_PIPE_NOT_AVAILABLE.
* [https://bugzilla.samba.org/DOOTTcgi?id=10521 BUG #10521]: rpcclie: EFSRVP request UNCs should include a trailing backslash.
*   Daniel Liberman <danielvl@gmail.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10387 BUG #10387]: 'net : search' on high latency networks can return a partial list with no error indication.
*   Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10200 BUG #10200]: Make :bclient' support DFS shares with SMB2/3./
* [https://bugzilla.samba.org/DOOTTcgi?id=10344 BUG #10344]: Session on a signed connection with an outstanding notify request crashes smbd.
* [https://bugzilla.samba.org/DOOTTcgi?id=10422 BUG #10422]: max : > 64kb leads to segmentation fault.
* [https://bugzilla.samba.org/DOOTTcgi?id=10444 BUG #10444]: smbd_se:ction_terminate("CTDB_SRVID_RELEASE_IP") panics from within ctdbd_migrate() with invalid lock_order.
* [https://bugzilla.samba.org/DOOTTcgi?id=10464 BUG #10464]: samba4S: rvices not binding on IPv6 addresses causing connection delays.
*   Garming Sam <garming@catalyst.net.nz>
* [https://bugzilla.samba.org/DOOTTcgi?id=10378 BUG #10378]: dfs: :ays  create_conn_struct with root privileges.
*   Andreas Schneider <asn@cryptomilk.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10467 BUG #10467]: s3DDAAS Fix str: vfs module on btrfs.
* [https://bugzilla.samba.org/DOOTTcgi?id=10472 BUG #10472]: pidl:SS:f sho have an option for the dir to install perl files and do not glob.
* [https://bugzilla.samba.org/DOOTTcgi?id=10474 BUG #10474]: s3DDAAS:d: Don't r spoolssd if epmd is not running.
* [https://bugzilla.samba.org/DOOTTcgi?id=10481 BUG #10481]: s3DDAAS:ver: Fix han of fragmented rpc requests.
*   Gustavo Zacarias <gustavo@zacarias.com.ar>
* [https://bugzilla.samba.org/DOOTTcgi?id=10506 BUG #10506]: Make :breadline' build with readline 6.3.

 http://samba.org/samba/hi/aDDAA/T1DDOOT/ml

Samba 4.1.6
------------------------

* Notes for Samba 4.1.6
 11, 2014

===============================
This is a security release in order to address
===============================

------------------------

*[http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2013-4496 CVE-2013-4496] (Password lockout not enforced for SAMR password changes) and [http:SSLLAASS:HHcve.mitre.org/cgi-bin/cvenameDDOO/=CVE-20/6442 CVE-2013-6442] (smbcacls can remove a file or directory ACL by mistake).

* [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2013-4496 CVE-2013-4496]:  versions 3.4.0 and above allow the administrator to implement locking out Samba accounts after a number of bad password attempts.
* :all released versions of Samba did not implement this check for password changes, such as are available over multiple SAMR and RAP interfaces, allowing password guessing attacks.

* [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2013-6442 CVE-2013-6442]:  versions 4.0.0 and above have a flaw in the smbcacls command. If smbcacls is used with the "-C|--chown name" or "-G|--chgrp name" command options it will remove the existing ACL on the object being modified, leaving the file or directory unprotected.

===============================
Changes since 4.1.5:

* Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10327 bug #10327]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgiDDAAS/name.cgi?/AASSHH2013-6442 CVE-2013-6442]: ensure we : lose an existing ACL when setting owner or group owner.
* Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10245 bug #10245]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgiDDAAS/name.cgi?/AASSHH2013-4496 CVE-2013-4496]: Enforce password:lockout for SAMR password changes.
* Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10245 bug #10245]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgiDDAAS/name.cgi?/AASSHH2013-4496 CVE-2013-4496]: Enforce password:lockout for SAMR password changes.

 http://samba.org/samba/hi/aDDAA/T1DDOOT/ml

Samba 4.1.5
------------------------

* Notes for Samba 4.1.5
* 21, 2014

===============================
This is the latest stable release of Samba 4.1.
===============================

------------------------

===============================
Major enhancements in Samba 4.1.5 include:

* [https://DOOTTsamba.org/show_bug/id=10358 bug #10358]Fix 100% CPU utilization in winbindd when trying to free memory in winbindd_reinit_after_fork.
* [https://DOOTTsamba.org/show_bug/id=10415 bug #10415]smbd:  memory overwrites.

===============================
Changes since 4.1.4:

* Michael Adam <obnox at samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10259 bug #10259]: Make :dow_copy2 module working with Windows 7.
* Jeremy Allison <jra at samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=2662 bug #2662]: Make :amped directory handling code 64bit clean.
* [https://bugzilla.samba.org/DOOTTcgi?id=10320 bug #10320]: s3: :asswd:  crashes  invalid input.
* [https://bugzilla.samba.org/DOOTTcgi?id=10358 bug #10358]: Fix : CPU utilization in winbindd when trying to free memory in winbindd_reinit_after_fork.
* [https://bugzilla.samba.org/DOOTTcgi?id=10406 bug #10406]: s3: :dirsort : : Allow dirsort : work when multiple simultaneous directories are open.
* [https://bugzilla.samba.org/DOOTTcgi?id=10429 bug #10429]: s3: :les: :fo: AsSS: have  VFS function SMB_VFS_LLISTXATTR we can't cope with a symlink when lp_posix_pathnames() is true.
* Alistair Leslie-Hughes <leslie_alistair at hotmail.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10087 bug #10087]: ntlm_au: Esometimes returns the wrong username to mod_ntlm_auth_winbind.
* Andrew Bartlett <abartlet at samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10418 bug #10418]: Fix :RNAL ERROR: Signal : Ein the kdc pid.
* Jeffrey Clark <dude at zaplabs.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10418 bug #10418]: Add :ort for Heimdal's unified krb5 and hdb plugin system.
* Niels de Vos <ndevos at redhat.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10384 bug #10384]: vfsSSLL:terfs: In case:atime is not passed, set it to the current atime.
* David Disseldorp <ddiss at samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10424 bug #10424]: vfs_btr Fix inc zero length server-side copy request handling.
* Volker Lendecke <vl at samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=2191 bug #2191]: s3DDAAS:: Improve :ce of wb_fill_pwent_sid2uid_done().
* [https://bugzilla.samba.org/DOOTTcgi?id=10415 bug #10415]: smbd:SS:x mem overwrites.
* [https://bugzilla.samba.org/DOOTTcgi?id=10436 bug #10436]: smbd:SS:x anS:ncient oplock bug.
* Stefan Metzmacher <metze at samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10442 bug #10442]: Fix :h bug in smb2_notify code.
*  Andreas Schneider <asn at samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10367 bug #10367]: Fix :ral memory leaks.
* Jelmer Vernooij <jelmer at samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10418 bug #10418]: Cope :h first element in hdb_method having a different name in different heimdal versions.

 http://samba.org/samba/hi/aDDAA/T1DDOOT/ml

Samba 4.1.4
------------------------

* Notes for Samba 4.1.4
* 10, 2014

===============================
This is the latest stable release of Samba 4.1.
===============================

------------------------

===============================
Major enhancements in Samba 4.1.4 include:

* [https://DOOTTsamba.org/show_bug/id=10284 bug #10284]Fix segfault in smbd.
* [https://DOOTTsamba.org/show_bug/id=10311 bug #10311]Fix SMB2 server panic when a smb2 brlock times out.

===============================
Changes since 4.1.3:

* Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=9870 bug #9870]: smbd:SS:low u on directory write times on open handles.
* [https://bugzilla.samba.org/DOOTTcgi?id=10260 bug #10260]: smbclie: Eshows no error if deleting a directory with del failed.
* [https://bugzilla.samba.org/DOOTTcgi?id=10297 bug #10297]: smbd:SS:x wri to a directory with -wx permissions on a share.
* [https://bugzilla.samba.org/DOOTTcgi?id=10305 bug #10305]: ldb: : ifS: st in ldb_comparison_fold().
* Christian Ambach <ambi@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10276 bug #10276]: Fix : build error on AIX with IBM XL C/C++DDOOT/
* [https://bugzilla.samba.org/DOOTTcgi?id=10280 bug #10280]: s3:winb fix : of uninitialized variables.
* Michele Baldessari <michele@acksyn.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10281 bug #10281]: Fix :s in man pages.
* Jan Brummer <jan.brummer@tabos.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10285 bug #10285]: s3DDAAS:d: Fix DEB: Estatement in winbind_msg_offline().
* Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10262 bug #10262]: s3DDAAS:oin: Use upp:case realm when composing default upn.
* [https://bugzilla.samba.org/DOOTTcgi?id=10281 bug #10281]: Fix :rous typos in man pages.
* David Disseldorp <ddiss@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10271 bug #10271]: Send :rect job-ID in print job notifications.
* Poornima Gurusiddaiah <pgurusid@redhat.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10337 bug #10337]: vfs_glu Enable  client log file.
* Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10250 bug #10250]: smbd:SS:x aSS:lloc hierarchy problem in msg_channel.
* [https://bugzilla.samba.org/DOOTTcgi?id=10284 bug #10284]: smbd:SS:x seg:TT
* [https://bugzilla.samba.org/DOOTTcgi?id=10297 bug #10297]: smbd:SS:x wri to a directory with -wx permissions on a share.
* [https://bugzilla.samba.org/DOOTTcgi?id=10311 bug #10311]: Fix : server panic when a smb2 brlock times out.
* Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10298 bug #10298]: ReduceS:mb2_server processing overhead.
* Arvid Requate <requate@univention.de>
* [https://bugzilla.samba.org/DOOTTcgi?id=10267 bug #10267]: Fix :ting via local printer drivers with Windows 8.
* Christof Schmitt <cs@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10310 bug #10310]: Fix : with SMB2 and locks.
* Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=2191 bug #2191]: Fix :LLAASSHH%g substitution in 'template homedir'.
* [https://bugzilla.samba.org/DOOTTcgi?id=10274 bug #10274]: Fix :ral issues and warnings from analyzer tools.
* [https://bugzilla.samba.org/DOOTTcgi?id=10286 bug #10286]: s3DDAAS Fix %GS:ubstitution for domain users in smbd.

 https://TTsamba.org/archive//SHHtech/-January/097145//

Samba 4.1.3
------------------------

* Notes for Samba 4.1.3
* 9, 2013

===============================
This is a security release in order to address
===============================

------------------------

*[http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2013-4408 CVE-2013-4408] (DCE-RPC fragment length field is incorrectly checked) and
*[http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2012-6150 CVE-2012-6150] (pam_winbind login without require_membership_of restrictions).

*[http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2013-4408 CVE-2013-4408]::
 versions 3.4.0 and above (versions 3.4.0 - 3.4.17, 3.5.0 - 3.5.22, 3.6.0 - 3.6.21, 4.0.0 - 4.0.12 and including 4.1.2) are vulnerable to buffer overrun exploits in the client processing of DCE-RPC packets. This is due to incorrect checking of the DCE-RPC fragment length in the client code.

 is a critical vulnerability as the DCE-RPC client code is part of the winbindd authentication and identity mapping daemon, which is commonly configured as part of many server installations (when joined to an Active Directory Domain). A malicious Active Directory Domain Controller or man-in-the-middle attacker impersonating an Active Directory Domain Controller could achieve root-level access by compromising the winbindd process.

 server versions 3.4.0 - 3.4.17 and versions 3.5.0 - 3.5.22 are also vulnerable to a denial of service attack (server crash) due to a similar error in the server code of those versions.

 server versions 3.6.0 and above (including all 3.6.x versions, all 4.0.x versions and 4.1.x) are not vulnerable to this problem.

 addition range checks were missing on arguments returned from calls to the DCE-RPC functions LookupSids (lsa and samr), LookupNames (lsa and samr) and LookupRids (samr) which could also cause similar problems.

 this was found during an internal audit of the Samba code there are no currently known exploits for this problem (as of December 9th 2013).

*[http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2012-6150 CVE-2012-6150]::
* allows for the further restriction of authenticated PAM logins using the require_membership_of parameter. System administrators may specify a list of SIDs or groups for which an authenticated user must be a member of. If an authenticated user does not belong to any of the entries, then login should fail. Invalid group name entries are ignored.

 versions 3.3.10, 3.4.3, 3.5.0 and later incorrectly allow login from authenticated users if the require_membership_of parameter specifies only invalid group names.

 is a vulnerability with low impact. All require_membership_of group names must be invalid for this bug to be encountered.

===============================
Changes since 4.1.2:

* Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10185 bug #10185] [http:/SSLLAAS:/OTTorg/cgi-bin/cv/Tcgi?name=CVED/3-4408 CVE-2013-4408]: Correctly : DCE-RPC fragment length field.
* Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10185 bug #10185] [http:/SSLLAAS:/OTTorg/cgi-bin/cv/Tcgi?name=CVED/3-4408 CVE-2013-4408]: Correctly : DCE-RPC fragment length field.
* Noel Power <noel.power@suse.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10300 bug #10300], [https:/SSLLAAS:/ba.org/show_bug.cgi/ bug #10306]: [http:/:cve.mitreD:LLAASSHHc/bin/cvename.cgi?name=CVE-20/6150 CVE-2012-6150]: Fail authentication  user isn't member of *any* require_membership_of specified groups.

Samba 4.1.2
------------------------

* Notes for Samba 4.1.2
* 22, 2013

This is is the latest stable release of Samba 4.1.

===============================
Changes since 4.1.1
===============================

------------------------

*   Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10187 bug #10187]: Missing:talloc_free can leak stackframe in error path.
* [https://bugzilla.samba.org/DOOTTcgi?id=10196 bug #10196]: RW : for a specific user is not overriding RW Allow for a group.
*   Anand Avati <avati@redhat.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10224 bug #10224]: vfs_glu Implement : mashalling/unmarsha/ of ACLs.
*   Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10052 bug #10052]: dfs_ser Use dsd:ne to catch 0 results as well as NO_SUCH_OBJECT errors.
*   Samuel Cabrero <scabrero@zentyal.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=9091 bug #9091]: s4DDAAS dlz_bind9: : dnsDDAASS: account disabled.
*   Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10264 bug #10264]: s3DDAAS:d: Fix cac: _validate_fn failure for NDR cache entries.
*   Christopher R. Hertel <crh@redhat.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10224 bug #10224]: vfs_glu Fix exc debug output from vfs_gluster_open().
*   Björn Jacke <bj@sernet.de>
* [https://bugzilla.samba.org/DOOTTcgi?id=10247 bug #10247]: xattr:S:ix lis EAs on *BSD for non-root users.
*   Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10190 bug #10190]: Fix : t used with constant zero length parameter.
* [https://bugzilla.samba.org/DOOTTcgi?id=10195 bug #10195]: nsswitc: EFix sho: Ewrites in winbind_write_sock.
*   Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10193 bug #10193]: s4:dsdb:rootdse: :rt 'dnsHostN instead of 'dNSHostName'.
* [https://bugzilla.samba.org/DOOTTcgi?id=10232 bug #10232]: libcliS:mb: Fix smb:*() against Windows 2008.
*   Susant Kumar Palai <spalai@redhat.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10224 bug #10224]: VFS :in was sending the actual size of the volume instead of the total number of block units because of which windows was getting the wrong volume capacity.
*   Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10194 bug #10194]: Make :line logon cache updating for cross child domain group membership.
* [https://bugzilla.samba.org/DOOTTcgi?id=10269 bug #10269]: util:SS:move  macros breaking strict aliasing.
* [https://bugzilla.samba.org/DOOTTcgi?id=10253 bug #10253]: Fix : build of vfs_glusterfs.

Samba 4.1.1
------------------------

* Notes for Samba 4.1.1
* 11, 2013

This is a security release in order to address [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2013-4475 CVE-2013-4475] (ACLs are not checked on opening an alternate data stream on a file or directory) and CVE-2013-4476 (Private key in key.pem world readable).

* [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2013-4475 CVE-2013-4475]::
* ESamba versions 3.2.0 and above (all versions of 3.2.x, 3.3.x, 3.4.x, 3.5.x, 3.6.x, 4.0.x and 4.1.x) do not check the underlying file or directory ACL when opening an alternate data stream.
* EAccording to the SMB1 and SMB2+ protocols the ACL on an underlying file or directory should control what access is allowed to alternate data streams that are associated with the file or directory.
* EBy default no version of Samba supports alternate data streams on files or directories.
* ESamba can be configured to support alternate data streams by loading either one of two virtual file system modues (VFS) vfs_streams_depot or vfs_streams_xattr supplied with Samba, so this bug only affects Samba servers configured this way.
* ETo determine if your server is vulnerable, check for the strings "streams_depot" or "streams_xattr" inside your smb.conf configuration file.

* [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2013-4476 CVE-2013-4476]::
* EIn setups which provide ldap(s) and/CEEhttps services, the private key for SSL/TLS /ption might be world readable. This typically happens in active directory domain controller setups.

===============================
Changes since 4.1.1:

* Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10234 bug #10234] + [https:/SSLLAAS:/ba.org/show_bug.cgi/ bug #10229]: [http:/:cve.mitreD:LLAASSHHc/bin/cvename.cgi?name=CVE-20/4475 CVE-2013-4475]: Fix access check:verification on stream files.
* Björn Baumbach <bb@sernet.de>
* [https://bugzilla.samba.org/DOOTTcgi?id=10234 bug #10234]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgiDDAAS/name.cgi?/AASSHH2013-4476 CVE-2013-4476]: Private key  key.pem world readable.

Samba 4.1.0 
------------------------

<onlyinclude>
* Notes for Samba 4.1.0
* 11, 2013

===============================
This is is the first stable release of Samba 4.1.
===============================

------------------------

Samba 4.1 will be the next version of the Samba suite and includes all the technology found in both the Samba4 series and the stable 3.x series. The primary additional features over Samba 3.6 are support for the Active Directory logon protocols used by Windows 2000 and above.

Major enhancements in Samba 4.1.0 include:

===============================
Client tools support SMB2/

Samba 4.1.0 contains the first release of our client tools and client library that work over the new protocols SMB2 or SMB3. Note that SMB3 only works either to a Samba server version 4.0.0 or above, or to a Windows Server running Windows 2012 or Windows 8.

The default protocol for smbclient and smbcacls is still SMB1 (the NT1 protocol dialect). An SMB2 or SMB3 connection can be selected in one of two ways. The easiest way to test the new protocol connection is to add the -mMAX_PROTOCOL command line switch to either smbclient or smbcacls.

For example, to connect using SMB3 with smbclient a user would type:

 smbclient //CCEEDD/r%password -mSMB3

Another example of connecting using SMB2 using smbcacls would be:

 smbcacls //CCEEDD/r%password -mSMB2 filename

Note that when connecting using SMB2 or SMB3 protocols the UNIX extensions are no longer available inside the smbclient command set. This is due to UNIX extensions not yet being defined for the SMB2 or SMB3 protocols.

The second way to select SMB2 or SMB3 connections is to set the "client max protocol" parameter in the [global] section of your smb.conf.

Setting this parameter will cause all client connections from Samba and its client tools to offer the requested max protocol to a server on every connection request.

For example, to cause all client tools (including winbindd, rpcclient, and the libsmbclient library) to attempt use SMB3 by default add the line:

 client max protocol = SMB3

to the [global] section of your smb.conf. This has not been as widely tested as the -mPROTOCOL options, but is intended to work correctly in the final release of 4.1.0.

* SLLAASSHHSMB2`

===============================
Encrypted transport==
===============================

------------------------

Although Samba servers have supported encrypted transport connections using the UNIX extensions for many years, selecting SMB3 transport allows encrypted transport connections to Windows servers that support SMB3, as well as Samba servers.

In order to enable this, add the "-e" option to the smbclient command line.

For example, to connect to a Windows 2012 server over SMB3 and select an encrypted transport you would use the following command line:

 smbclient //e DD/r%password -mSMB3 -e

===============================
Directory database replication (AD DC mode)==
===============================

------------------------

Directory replication has been reworked in order to improve the correctness and efficiency.

As a net effect of it, replication with other domain controllers with a heavily modified schema is now possible (ie. Windows 2012 DCs or other Windows DC with exchange installed) and replication didn't fail anymore in such environments.

===============================
Server-Side Copy Support==
===============================

------------------------

Samba 4.1.0 adds support for server-side copy operations via the SMB2 FSCTL_SRV_COPYCHUNK request. Clients making use of server-side copy support, such as Windows Server 2012, should experience considerable performance improvements for file copy operations, as file data need not traverse the network.

This feature is enabled by default on the smbd file server.

===============================
Btrfs Filesystem Integration==
===============================

------------------------

The Btrfs VFS module provided with Samba 4.1.0 further improves the performance of server-side copy operations on shares backed by a Btrfs filesystem. It does so by allowing multiple files to share the same on-disk extents, avoiding the unnecessary duplication of source and destination file data during a server-side copy operation.

This feature can be explicitly enabled on smbd shares backed by a Btrfs filesystem with the smb.conf parameter:

 vfs objects = btrfs

===============================
REMOVED COMPONENTS==
===============================

------------------------

The Samba Web Administration Tool (SWAT) has been removed. Details why SWAT has been removed can be found on the samba-technical mailing list:

 https://TTsamba.org/archive//SHHtech/-February/09057/l/

Changes=
===============================

------------------------

===============================
smb.conf changes==
===============================

------------------------

   Parameter Name			Description	Default
   --------------			-----------	-------
   acl allow execute always             New		False
   password level                       Removed
   set directory                        Removed
   use ntdb                             New		No

RUNNING Samba 4.1 as an AD DC=
===============================

------------------------

A short guide to setting up Samba as an AD DC can be found on the wiki:

* _up_Samba_as_an_Active_Directory_Domain_Controller`
</de>

COMMIT HIGHLIGHTS=
===============================

------------------------

*   Jeremy Allison <jra@samba.org>
* Add SMB2 and SMB3 support for client tools and client library.
* Add support for SMB3 Encrypted transport.
*   David Disseldorp <ddiss@samba.org>
* Add vfs_btrfs module.
* Add support for server-side copy operations via the SMB2 FSCTL_SRV_COPYCHUNK request.

===============================
CHANGES SINCE 4.1.0rc4==
===============================

------------------------

*  Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10178 bug #10178]: Fix : parsing failure.
*   Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10132 bug #10132]: pam_win Support  KEYRING ccache type.

===============================
CHANGES SINCE 4.1.0rc3==
===============================

------------------------

*   Michael Adam <obnox@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10134 bug #10134]: Add :t;acl allow execute always" parameter.
*   Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10139 bug #10139]: ValidSS:f8 filenames cause "invalid conversion error" messages.
* [https://bugzilla.samba.org/DOOTTcgi?id=10145 bug #10145]: SambaSS:B2 client code reads the wrong short name length in a directory listing reply.
* [https://bugzilla.samba.org/DOOTTcgi?id=10149 bug #10149]: cli_smb:ist_path() failed to close file on exit.
* [https://bugzilla.samba.org/DOOTTcgi?id=10150 bug #10150]: Not : OEM servers support the ALTNAME info level.
*   Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=8077 bug #8077]: dsdb:SS:nvert  full string from UTF16 to UTF8, including embedded NULLs.
* [https://bugzilla.samba.org/DOOTTcgi?id=9461 bug #9461]: pythonD:ba-tool fsmo: Do notS:ive an error on a successful role transfer.
* [https://bugzilla.samba.org/DOOTTcgi?id=10157 bug #10157]: Regress causes replication failure with Windows 2008R2 and deletes Deleted Objects.
*   Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10147 bug #10147]: BetterS:ocument potential implications of a globally used "valid users".
*   Korobkin <korobkin+samba@gmail.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10118 bug #10118]: RaiseSS:  level of a debug when unable to open a printer.
*   Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10008 bug #10008]: dbwrap_ Treat e records as non-existing.
* [https://bugzilla.samba.org/DOOTTcgi?id=10138 bug #10138]: smbd:SS:ways  up share modes after hard crash.
*   Daniel Liberman <danielvl@gmail.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10162 bug #10162]: Fix :X ACL mapping when setting DENY ACE's from Windows.
*   Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10144 bug #10144]: libcliS:mb: Use SMB: MID=0 for the initial Negprot.
* [https://bugzilla.samba.org/DOOTTcgi?id=10146 bug #10146]: libcliS:mb: Only ch the SMB2 session setup signature if required and valid.
*   Matthieu Patou <mat@matws.net>
* [https://bugzilla.samba.org/DOOTTcgi?id=10158 bug #10158]: Netbios:related samba process consumes 100% CPU.
*   Christof Schmitt <christof.schmitt@us.ibm.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10137 bug #10137]: vfs_sha: Display : versions correctly over SMB2.

===============================
CHANGES SINCE 4.1.0rc2==
===============================

------------------------

*   Michael Adam <obnox@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10107 bug #10107]: Fix :ind crashes on DC with trusted AD domains.
*   Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=5197 bug #5917]: Fix :ing on site with Read Only Domain Controller.
* [https://bugzilla.samba.org/DOOTTcgi?id=9974 bug #9974]: Add : and SMB3 support for smbclient.
* [https://bugzilla.samba.org/DOOTTcgi?id=10163 bug #10163]: Fix :ry leak in source3/lib/util/93D/
* [https://bugzilla.samba.org/DOOTTcgi?id=10121 bug #10121]: MasksSS:correctly applied to UNIX extension permission changes.
*   Christian Ambach <ambi@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=9911 bug #9911]: BuildSS:mba 4.0.x on AIX with IBM XL C/C++DDOOT/
*   Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=9091 bug #9091]: When :licating DNS for bind9_dlz we need to create the server-DNS account remotely.
*   Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=9615 bug #9615]: Winbind:unable to retrieve user information from AD.
* [https://bugzilla.samba.org/DOOTTcgi?id=9899 bug #9899]: winbind:mes() fails because of NT_STATUS_CANT_ACCESS_DOMAIN_INFO.
* [https://bugzilla.samba.org/DOOTTcgi?id=10107 bug #10107]: Fix :ind crashes on DC with trusted AD domains.
*   Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10086 bug #10086]: smbd:SS:x asy: Eecho handler forking.
* [https://bugzilla.samba.org/DOOTTcgi?id=10106 bug #10106]: HonourS:utput buffer length set by the client for SMB2 GetInfo requests.
* [https://bugzilla.samba.org/DOOTTcgi?id=10114 bug #10114]: HandleS:ropbox (write-only-directory) case correctly in pathname lookup.
*   Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10030 bug #10030]: ::1 :d ::ver on join.
*   Rusty Russell <rusty@rustcorp.com.au>
* [https://bugzilla.samba.org/DOOTTcgi?id=10000 bug #10000]: Add : pages for ntdb tools.
*   Karolin Seeger <kseeger@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=7364 bug #7364]: Add : page for vfs_syncops.
* [https://bugzilla.samba.org/DOOTTcgi?id=7490 bug #7490]: Add : page for vfs_linux_xfs_sgid.
* [https://bugzilla.samba.org/DOOTTcgi?id=10001 bug #10001]: Add : page for samba-regedit tool.
* [https://bugzilla.samba.org/DOOTTcgi?id=10076 bug #10076]: Fix :able list in vfs_crossrename man page.
*   Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10073 bug #10073]: Fix : ntation fault in 'net ads join'.
* [https://bugzilla.samba.org/DOOTTcgi?id=10082 bug #10082]: s3DDAAS:: Fix aSS:gfault passing NULL to a fstring argument.
*   Richard Sharpe <realrichardsharpe@gmail.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10097 bug #10097]: MacOSXS:0.9 will not follow path-based DFS referrals handed out by Samba.
*   Ralph Wuerthner <ralph.wuerthner@de.ibm.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10106 bug #10106]: HonourS:utput buffer length set by the client for SMB2 GetInfo requests.

===============================
CHANGES SINCE 4.1.0rc1==
===============================

------------------------

* 
*   Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=9992 bug #9992]: Windows: rror 0x800700FE when copying files with xattr names containing ":".:
* [https://bugzilla.samba.org/DOOTTcgi?id=10010 bug #10010]: Missing:integer wrap protection in EA list reading can cause server to loop with DOS (CVE-2013-4124).
* [https://bugzilla.samba.org/DOOTTcgi?id=10064 bug #10064]: LinuxSS:rnel oplock breaks can miss signals.
*   Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=9029 bug #9029]: Fix :ication with --domain-crictical-only to fill in backlinks.
* [https://bugzilla.samba.org/DOOTTcgi?id=9820 bug #9820]: Fix :h of winbind after "ls -l /usr/loca/r/l/l&quo////
* [https://bugzilla.samba.org/DOOTTcgi?id=10056 bug #10056]: dsdb :rovements.
*   Björn Baumbach <bb@sernet.de>
* [https://bugzilla.samba.org/DOOTTcgi?id=10003 bug #10003]: Fix :ault while reading incomplete session info.
*   Gregor Beck <gbeck@sernet.de>
* [https://bugzilla.samba.org/DOOTTcgi?id=9678 bug #9678]: Windows:8 Roaming profiles fail.
*   Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10043 bug #10043]: AllowSS: change the default location for Kerberos credential caches.
*   Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=10013 bug #10013]: Fix  100% loop at shutdown time (smbd).
*   Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=9820 bug #9820]: Fix :h of winbind after "ls -l /usr/loca/r/l/l&quo////
* [https://bugzilla.samba.org/DOOTTcgi?id=10015 bug #10015]: FixSSLL:ove debug options.
* [https://bugzilla.samba.org/DOOTTcgi?id=10042 bug #10042]: Fix :hes in socket_get_local_addr().
* [https://bugzilla.samba.org/DOOTTcgi?id=10056 bug #10056]: dsdb :rovements.
*Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=9994 bug #9994]: Do : delete an existing valid credential cache (s3-winbind).
* [https://bugzilla.samba.org/DOOTTcgi?id=10040 bug #10040]: RenameS: gedit to samba-regedit.
* [https://bugzilla.samba.org/DOOTTcgi?id=10041 bug #10041]: RemoveS:bsolete swat manpage and references.
* [https://bugzilla.samba.org/DOOTTcgi?id=10048 bug #10048]: nsswitc: EAdd OPT:  to avoid an error message.
* Alexander Werth <alexander.werth@de.ibm.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10045 bug #10045]: RemoveS: redundant inlined substitution of ACLs.
*   Ralph Wuerthner <ralphw@de.ibm.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=10064 bug #10064]: LinuxSS:rnel oplock breaks can miss signals.

----
`Category: Notes`