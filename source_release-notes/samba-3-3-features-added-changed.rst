Samba 3.3 Features added/changed
    <namespace>0</namespace>
<last_edited>2017-02-26T21:09:06Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

This release series is in the **security fixes only** mode.

Samba 3.3 turned into security fixes only mode 
------------------------

(**Updated 01-March-2010**)

Moving forward, any 3.3.x releases will be on a as needed basis
for **security issues only**.

Samba 3.3.16 
------------------------

* Release Notes for Samba 3.3.16
* July 26, 2011

===============================
This is a security release in order to address CVE-2011-2522 CVE-2011-2694
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-2522 CVE-2011-2522]:
* The Samba Web Administration Tool (SWAT) in Samba versions 3.0.x to 3.5.9 are affected by a cross-site request forgery.

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-2694 CVE-2011-2694]:
* The Samba Web Administration Tool (SWAT) in Samba versions 3.0.x to 3.5.9 are affected by a cross-site scripting vulnerability.

Please note that SWAT must be enabled in order for these
vulnerabilities to be exploitable. By default, SWAT
is *not* enabled on a Samba install.

 [http://www.samba.org/samba/history/samba-3.3.16.html Release Notes Samba 3.3.16]

Samba 3.3.15 
------------------------

* Release Notes for Samba 3.3.15
* February 28, 2011

===============================
This is a security release in order to address CVE-2011-0719.
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-0719 CVE-2011-0719 CVE-2011-0719]:
* All current released versions of Samba are vulnerable to a denial of service caused by memory corruption. Range checks on file descriptors being used in the FD_SET macro were not present allowing stack corruption. This can cause the Samba code to crash or to loop attempting to select on a bad file descriptor set.

----

(**Updated 28-February-2011**)

* Monday, February 28 - Samba 3.3.15 has been released to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-0719 CVE-2011-0719].
    http://www.samba.org/samba/history/samba-3.3.15.html Release Notes Samba 3.3.15]

Samba 3.3.14 
------------------------

* Release Notes for Samba 3.3.14
* September 14, 2010

===============================
This is a security release in order to address CVE-2010-3069.
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-2069 CVE-2010-2069  CVE-2010-3069]:
* All current released versions of Samba are vulnerable to a buffer overrun vulnerability. The sid_parse() function (and related dom_sid_parse() function in the source4 code) do not correctly check their input lengths when reading a binary representation of a Windows SID (Security ID). This allows a malicious client to send a sid that can overflow the stack variable that is being used to store the SID in the Samba smbd server.

(**Updated 14-September-2010**)

* Tuesday, September 14 - Samba 3.3.14 has been released to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-2069 CVE-2010-2069].
    http://www.samba.org/samba/history/samba-3.3.14.html Release Notes Samba 3.3.14]

Samba 3.3.13 
------------------------

* Release Notes for Samba 3.3.13
* June 16, 2010

===============================
This is a security release in order to address CVE-2010-2063.
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-2063 CVE-2010-2063 CVE-2010-2063]:
* In Samba 3.3.x and below, a buffer overrun is possible in chain_reply code.

(**Updated 16-June-2010**)

* Wednesday, June 16 - Samba 3.3.13 has been released to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-2063 CVE-2010-2063].
    http://www.samba.org/samba/history/samba-3.3.13.html Release Notes Samba 3.3.13]

Samba 3.3.12 
------------------------

* Release Notes for Samba 3.3.12
* March 8, 2010

===============================
This is a security release in order to address CVE-2010-0728.
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-0728 CVE-2010-0728 CVE-2010-0728]:
* In Samba releases 3.5.0, 3.4.6 and 3.3.11, new code was added to fix a problem with Linux asynchronous IO handling. This code introduced a bad security flaw on Linux platforms if the binaries were built on Linux platforms with libcap support. The flaw caused all smbd processes to inherit CAP_DAC_OVERRIDE capabilities, allowing all file system access to be allowed even when permissions should have denied access.

(**Updated 09-March-2010**)

* Monday, March 8 - Samba 3.3.12 has been released to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-0728 CVE-2010-0728].
    http://www.samba.org/samba/history/samba-3.3.12.html Release Notes Samba 3.3.12]

Samba 3.3.11 
------------------------

* Release Notes for Samba 3.3.11
* February 26, 2010

===============================
This is the latest bugfix release of the Samba 3.3 series.
===============================

------------------------

Major enhancements in Samba 3.3.11 include:=
===============================

------------------------

* "wide links" and "unix extensions" are incompatible [https://bugzilla.samba.org/show_bug.cgi?id=7104 bug #7104].
* Fix failing of smbd to respond to a read or a write caused by Linux asynchronous IO (aio) [https://bugzilla.samba.org/show_bug.cgi?id=7067 bug #7067].

(**Updated 26-February-2010**)

* Friday, February 26 - Samba 3.3.11 has been released
**Please note, that this will probably be the last bug fix release of the 3.3 series.**

Samba 3.3.10 
------------------------

* Release Notes for Samba 3.3.10
* January 14, 2010

===============================
This is the latest bugfix release of the Samba 3.3 series.
===============================

------------------------

Major enhancements in Samba 3.3.10 include:=
===============================

------------------------

* Fix changing of ACLs on writable file with "dos filemode=yes" [https://bugzilla.samba.org/show_bug.cgi?id=5202 bug #5202].
* Fix smbd crashes in dns_register_smbd_reply [https://bugzilla.samba.org/show_bug.cgi?id=6696 bug #6696].
* Fix Winbind crashes when queried from nss [https://bugzilla.samba.org/show_bug.cgi?id=6889 bug #6889].
* Fix Winbind crash when retrieving empty group members [https://bugzilla.samba.org/show_bug.cgi?id=7014 bug #7014].
* Fix interdomain trusts with Win2008R2 [https://bugzilla.samba.org/show_bug.cgi?id=6697 bug #6697].
(**Updated 14-January-2010**)

* Thursday, January 14 - Samba 3.3.10 has been released
    http://www.samba.org/samba/history/samba-3.3.10.html Release Notes Samba 3.3.10]

Samba 3.3.9 
------------------------

* Release Notes for Samba 3.3.9
* October, 15  2009

===============================
This is the latest bugfix release of the Samba 3.3 series.
===============================

------------------------

Major enhancements in Samba 3.3.9 include:=
===============================

------------------------

* Fix trust relationships to windows 2008 (2008 r2) [https://bugzilla.samba.org/show_bug.cgi?id=6711 bug #6711].
* Fix file corruption using smbclient with NT4 server [https://bugzilla.samba.org/show_bug.cgi?id=6606 bug #6606].
* Fix Windows 7 share access (which defaults to NTLMv2) [https://bugzilla.samba.org/show_bug.cgi?id=6680 bug #6680].
* Fix SAMR server for Winbind access [https://bugzilla.samba.org/show_bug.cgi?id=6504 bug #6504].
(**Updated 15-October-2009**)

* Thursday, October 15 - Samba 3.3.9  has been released
    http://www.samba.org/samba/history/samba-3.3.9.html Release Notes Samba 3.3.9]

Samba 3.3.8 
------------------------

* Release Notes for Samba 3.3.8
* October, 1  2009

===============================
This is a security release in order to address CVE-2009-2813, CVE-2009-2948 and CVE-2009-2906.
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-2813 CVE-2009-2813]:
* In all versions of Samba later than 3.0.11, connecting to the home share of a user will use the root of the filesystem as the home directory if this user is misconfigured to have an empty home directory in /etc/passwd.

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-2948 CVE-2009-2948]:
* If mount.cifs is installed as a setuid program, a user can pass it a credential or password path to which he or she does not have access and then use the --verbose option to view the first line of that file. All known Samba versions are affected.

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1888 CVE-2009-2906]:
* Specially crafted SMB requests on authenticated SMB connections can send smbd into a 100% CPU loop, causing a DoS on the Samba server.

(**Updated 1-October-2009**)

* Thursday, October 1 - Samba 3.3.8 has been issued as **Security Release** to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1888 CVE-2009-2906],
[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-2906 CVE-2009-2906] and
[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-2813 CVE-2009-2813].
    http://www.samba.org/samba/history/samba-3.3.8.html Release Notes Samba 3.3.8]

Samba 3.3.7 
------------------------

* Release Notes for Samba 3.3.7
* July, 29  2009

===============================
This is the latest bugfix release of the Samba 3.3 series.
===============================

------------------------

(**Updated 23-June-2009**)

* Wednesday, July 29 - Samba 3.3.7 has been released
    http://www.samba.org/samba/history/samba-3.3.7.html Release Notes Samba 3.3.7]

Samba 3.3.6 
------------------------

* Release Notes for Samba 3.3.6
* June, 23  2009

This is a security release in order to address CVE-2009-1888.

* [http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1888 CVE-2009-1888] ("Uninitialized read of a data value"):
* In Samba 3.0.31 to 3.3.5 (inclusive), an uninitialized read of a data value can potentially affect access control when "dos filemode" is set to "yes".

(**Updated 23-June-2009**)

* Tuesday, June 23 2009: Samba 3.3.6 **Security Release** has been released to address
[http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1888 CVE-2009-1888] ("Uninitialized read of a data value").
For more information, please see [http://samba.org/samba/history/security.html Samba Security page].
    http://samba.org/samba/security/CVE-2009-1888.html Security Advisory]
    http://www.samba.org/samba/history/samba-3.3.6.html Release Notes Samba 3.3.6]

Samba 3.3.5 
------------------------

* Release Notes for Samba 3.3.5
* June, 16  2009

===============================
This is the latest bugfix release of the Samba 3.3 series.
===============================

------------------------

Major enhancements in Samba 3.3.5 include:=
===============================

------------------------

* Fix SAMR and LSA checks [https://bugzilla.samba.org/show_bug.cgi?id=6089 bug #60689], [https://bugzilla.samba.org/show_bug.cgi?id=6289 bug #6289]
* Fix posix acls when setting an ACL without explicit ACE for the owner [https://bugzilla.samba.org/show_bug.cgi?id=2346 bug #2346].
* Fix joining of Win7 into Samba domain [https://bugzilla.samba.org/show_bug.cgi?id=6099 bug #6099].
* Fix joining of Win2000 SP4 clients [https://bugzilla.samba.org/show_bug.cgi?id=6301 bug #6301].
(**Updated 16-June-2009**)

* Tuesday, June 16 - Samba 3.3.5 has been released
    http://www.samba.org/samba/history/samba-3.3.5.html Release Notes Samba 3.3.5]

Samba 3.3.4 
------------------------

* Release Notes for Samba 3.3.4
* April, 29  2009

===============================
This is the latest bugfix release of the Samba 3.3 series.
===============================

------------------------

Major enhancements in Samba 3.3.4 include:=
===============================

------------------------

* Fix domain logins for WinXP clients pre SP3 [https://bugzilla.samba.org/show_bug.cgi?id=6263 bug #6263].
* Fix samr_OpenDomain access checks [https://bugzilla.samba.org/show_bug.cgi?id=6089 bug #6089].
* Fix usrmgr.exe creating a user [https://bugzilla.samba.org/show_bug.cgi?id=6243 bug #6243].
(**Updated 29-April-2009**)

* Wednesday, April 29 - Samba 3.3.4 has been released
    http://www.samba.org/samba/history/samba-3.3.4.html Release Notes Samba 3.3.4]

Samba 3.3.3 
------------------------

* Release Notes for Samba 3.3.3
* April, 1 2009

===============================
This is the latest bugfix release release of the Samba 3.3 series.
===============================

------------------------

Major enhancements in Samba 3.3.3 include:=
===============================

------------------------

* Migrating from 3.0.x to 3.3.x can fail to update passdb.tdb correctly [https://bugzilla.samba.org/show_bug.cgi?id=6195 bug #6195].
* Fix serving of files with colons to CIFS/VFS client [https://bugzilla.samba.org/show_bug.cgi?id=6196 bug #6196].
* Fix "map readonly" [https://bugzilla.samba.org/show_bug.cgi?id=6186 bug #6186].
(**Updated 01-April-2009**)

* Wednesday, April 1 - Samba 3.3.3 has been released
    http://www.samba.org/samba/history/samba-3.3.3.html Release Notes Samba 3.3.3]

Samba 3.3.2 
------------------------

* Release Notes for Samba 3.3.2
* March, 12 2009

===============================
This is the latest bugfix release release of the Samba 3.3 series.
===============================

------------------------

Major enhancements in Samba 3.3.2 include:=
===============================

------------------------

* Fix "force group" (bug #6155).
* Fix saving of files on Samba share using MS Office 2007 [https://bugzilla.samba.org/show_bug.cgi?id=6160 bug #6160].
* Fix guest authentication in setups with "security = share" and "guest ok = yes" when Winbind is running.
* Fix corruptions of source path in tar mode of smbclient [https://bugzilla.samba.org/show_bug.cgi?id=6161 bug #6161].

(**Updated 12-March-2009**)

* Thursday, March 12 - Samba 3.3.2 has been released
    http://www.samba.org/samba/history/samba-3.3.2.html Release Notes Samba 3.3.2]

Samba 3.3.1 
------------------------

* Release Notes for Samba 3.3.1
* February, 24 2009

===============================
This is the latest bugfix release release of the Samba 3.3 series.
===============================

------------------------

Major enhancements in Samba 3.3.1 include:=
===============================

------------------------

* Fix net ads join when "ldap ssl = start tls" [https://bugzilla.samba.org/show_bug.cgi?id=6073 bug #6073].
* Fix renaming/deleting of files using Windows clients [https://bugzilla.samba.org/show_bug.cgi?id=6082 bug #6082].
* Fix renaming/deleting a "not matching/resolving" symlink [https://bugzilla.samba.org/show_bug.cgi?id=6090 bug #6090].
* Fix remotely adding a share via the Windows MMC.
(**Updated 24-February-2009**)

* Tuesday, February 24 - Samba 3.3.1 has been released
    http://www.samba.org/samba/history/samba-3.3.1.html Release Notes Samba 3.3.1]

Samba 3.3.0 
------------------------

* Release Notes for Samba 3.3.0
* January, 27 2009

===============================
This is the first stable release of Samba 3.3.0.
===============================

------------------------

Major enhancements in Samba 3.3.0 include:=
===============================

------------------------

===============================
General changes:==
===============================

------------------------

* The passdb tdbsam version has been raised.

===============================
Configuration/installation:==
===============================

------------------------

* Splitting of library directory into library directory and separate modules directory.
* The default value of "ldap ssl" has been changed to "start tls".

===============================
File Serving:==
===============================

------------------------

* Extended Cluster support.
* New experimental VFS modules "vfs_acl_xattr" and "vfs_acl_tdb" to store NTFS ACLs on Samba file servers.

===============================
Winbind:==
===============================

------------------------

* Simplified idmap configuration.
* New idmap backends "adex" and "hash".
* Added new parameter "winbind reconnect delay".
* Added support for user and group aliasing.
* Added support for multiple domains to idmap_ad.

===============================
Administrative tools:==
===============================

------------------------

* The destination "all" of smbcontrol does now affect all running daemons including nmbd and winbindd.
* New 'net rpc vampire keytab' and 'net rpc vampire ldif' commands.
* The 'net' utility can now use kerberos for joining and authentication.
* The 'wbinfo' utility can now add, modify and remove identity mapping entries.

===============================
Libraries:==
===============================

------------------------

* NetApi library implements various new calls for User- and Group Account Management.
* libsmbclient does now determine case sensitivity based on file system attributes.

General changes=
===============================

------------------------

The passdb tdbsam version has been raised as among other things the RID counter has been moved from the winbindd_idmap.tdb to the passdb.tdb file to make "passdb backend = tdbsam" working in clustered environments.

Please note that an updated passdb.tdb file is _not_ compatible with Samba versions before 3.3.0! Please backup your passdb.tdb file if you use "passdb backend = tdbsam". That can be achieved by running

 'tdbbackup /etc/samba/passdb.tdb'

before the update.

Configure changes=
===============================

------------------------

The configure option "--with-libdir" has been removed. The library directory can still be specified by using the existing "--libdir" option. A new option "--with-modulesdir" has been added to allow the specification of a separate directory for the shared modules.

===============================
Configuration changes==
===============================

------------------------

The default value of "ldap ssl" has been changed to "start tls". This means, Samba will use the LDAPv3 StartTLS extended operation (RFC2830) for communicating with directory servers by default. If your directory servers do not support this extended operation, you will have to set "ldap ssl = no". Otherwise, Samba could not contact the directory servers
anymore!

===============================
Winbind idmap backend changes==
===============================

------------------------

The idmap configuration has changed with version 3.3 to something that allows a smoother upgrade path from pre-3.0.25 configurations that use "idmap backend". The reason for this change is that to many, also to Samba developers, the 3.0.25 style configuration with "idmap config" turned out to be very complex. Version 3.3 no longer deprecates the "idmap backend"
parameter, instead with "idmap backend" the default idmap backend is specified.

Accordingly, the "idmap config  : default = yes" setting is no longer being looked at.

The alloc backend defaults to the default backend, which should be able to allocate IDs. In the default distribution the tdb and ldap backends can allocate, the ad and rid backends can not. The idmap alloc range is now being set with the "old" parameters "idmap uid" and "idmap gid". 

The "idmap domains" parameter has been removed.

===============================
winbind reconnect delay==
===============================

------------------------

This is a new parameter which specifies the number of seconds the Winbind
daemon will wait between attempts to contact a Domain controller for a domain
that is determined to be down or not contactable.

===============================
Winbind's Name Aliasing==
===============================

------------------------

Name aliasing in Winbind is a feature that allows an administrator to map a fully qualified user or group name from a Windows domain to a convenient short name for Unix access.  This is similar to the username map functionality supported by smbd but is primary intended for clients and servers making use of Winbind's PAM and NSS libraries.

For example, the user "DOMAIN\fred" has been mapped to the Unix name "freddie".

   $ getent passwd "DOMAIN\fred"
   freddie:x:1000:1001:Fred Jones:/home/freddie:/bin/bash

   $ getent passwd freddie
   freddie:x:1000:1001:Fred Jones:/home/freddie:/bin/bash

The name aliasing support is provided by individual nss_info plugins. For example, the new "adex" plugin reads the uid attribute from Active Directory to make a short login name to the fully qualified name. 

While the new "hash" module utilizes a local file to map "short_name = QUALIFIED\name".  Both user and group name mapping is supported. Please refer to the "winbind nss info" option in smb.conf(5) and
to individual plugin man pages for further details.

===============================
idmap_hash==
===============================

------------------------

The idmap_hash plugin provides similar support as the idmap_rid module.  However, uids and gids are generated from the full domain SID using a hashing algorithm that maps the lower 19 bits from the user or group RID to bits 0 - 19 in the Unix id and hashes 96 bits from the domain SID to bits 20 - 30 in the Unix id.  The result is a 31 bit uid or gid that is consistent across machines and provides support for trusted domains.

Please refer to the idmap_hash(8) man page for more details.

===============================
idmap_adex==
===============================

------------------------

The adex idmap/nss_info plugin is an adaptation of the Likewise Enterprise plugin with support for OU based cells removed
(since the Windows pieces to manage the cells are not available).

This plugin supports

* The RFC2307 schema for users and groups.
* Connections to trusted domains
* Global catalog searches
* Cross forest trusts
* User and group aliases

Prerequisite: Add the following attributes to the Partial Attribute Set in global catalog:

* uidNumber
* uid
* gidNumber

A basic config using the current trunk code would look like:

 [global]
 	idmap backend = adex
 	idmap uid = 10000 - 29999
 	idmap gid = 10000 - 29999
 	winbind nss info = adex

 	winbind normalize names = yes
 	winbind refresh tickets = yes
 	template homedir = /home/%D/%U
 	template shell = /bin/bash

Please refer to the idmap_adex(8) man page for more details.

===============================
Libraries==
===============================

------------------------

libsmbclient will now treat file names case-sensitive by default if the filesystem we are connecting to supports case sensitivity. This change of behavior is considered a bug fix, as it was previously possible to accidentally overwrite a
file that had the same case-insensitive name but a different case-sensitive name as a previously-existing file, while creating a new file.

If it is not possible to detect if the filesystem supports case sensitivity, the user-specified option value will be used.

(**Updated 27-January-2009**)

* Tuesday, August 26 - Samba 3.3.0pre1 has been released
* Thursday, October 2 - Samba 3.3.0pre2 has been released
* Thursday, November 27 - Samba 3.3.0rc1 has been released
* Monday, December 15 - Samba 3.3.0rc2 has been released
* Tuesday, January 27 - Samba 3.3.0 has been released
    http://www.samba.org/samba/history/samba-3.3.0.html Release Notes Samba 3.3.0]

----
`Category:Release Notes`