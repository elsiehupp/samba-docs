Samba 3.2 Features added/changed
    <namespace>0</namespace>
<last_edited>2017-02-26T21:09:05Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

Samba 3.2 discontinued 
------------------------

(**Updated 01-March-2010**)

With the release of Samba 3.5.0, Samba 3.2 has been marked **discontinued**.

Samba 3.2 turned into mode 
------------------------

(**Updated 11-August-2009**)

Moving forward, any 3.2.x releases will be on a as needed basis
for **security issues only**.

Samba 3.2.15 
------------------------

* otes for Samba 3.2.15
* , 2009

===============================
This is a security release in order to address CVE-2009-2813, CVE-2009-2948 and CVE-2009-2906.
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-2813 CVE-2009-2813]::
* ersions of Samba later than 3.0.11, connecting to the home share of a user will use the root of the filesystem as the home directory if this user is misconfigured to have an empty home directory in /etc/passwd.

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-2948 CVE-2009-2948]::
* .cifs is installed as a setuid program, a user can pass it a credential or password path to which he or she does not have access and then use the --verbose option to view the first line of that file.
* n Samba versions are affected.

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1888 CVE-2009-2906]::
* y crafted SMB requests on authenticated SMB connections can send smbd into a 100% CPU loop, causing a DoS on the Samba server.

----

(**Updated 1-October-2009**)

* Thursday, October 1 - Samba 3.3.8 has been issued as **Security Release** to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1888 CVE-2009-2906],
[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-2906 CVE-2009-2906] and
[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-2813 CVE-2009-2813].
    http://www.samba.org/samba/history/samba-3.2.15.html Release Notes Samba 3.2.15]

Samba 3.2.14 
------------------------

* otes for Samba 3.2.14
* , 2009

===============================
This is the last maintenance release of the Samba 3.2 series.
===============================

------------------------

Please note that this is the last bugfix release of the Samba 3.2 series!

There will security releases on demand only. Please see http://wiki.samba.org/index.php/Samba3_Release_Planning for information
on current releases.

Major enhancements in 3.2.14 include:

* Fix SAMR access checks e.g. [https://bugzilla.samba.org/show_bug.cgi?id=6089 bugs #6089] and [https://bugzilla.samba.org/show_bug.cgi?id=6112 #6112].
* Fix 'force user' [https://bugzilla.samba.org/show_bug.cgi?id=6291 bug #6291].
* Improve Win7 support [https://bugzilla.samba.org/show_bug.cgi?id=6099 bug #6099].
* Fix posix ACLs when setting an ACL without explicit ACE for the owner [https://bugzilla.samba.org/show_bug.cgi?id=2346 bug #2346].

----

(**Updated 23-June-2009**)

* Wednesday, August 12 - Samba 3.2.14 has been released
**Please note that this is the last bug fix release of the 3.2 series!**
    http://www.samba.org/samba/history/samba-3.2.14.html Release Notes Samba 3.2.14]

Samba 3.2.13 
------------------------

* otes for Samba 3.2.13
* 2009

===============================
This is a security release in order to address CVE-2009-1886 and CVE-2009-1888.
===============================

------------------------

* [http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1886 CVE-2009-1886]: (":ng vulnerability in smbclient")
* 3.2.0 to 3.2.12 (inclusive), the smbclient commands dealing with file names treat user input as a format string to asprintf. With a maliciously crafted file name smbclient can be made to execute code triggered by the server.

* [http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1888 CVE-2009-1888]: (":zed read of a data value")
* 3.0.31 to 3.3.5 (inclusive), an uninitialized read of a data value can potentially affect access control when "dos filemode" is set to "yes".

----

(**Updated 23-June-2009**)

* Tuesday, June 23 2009:DOOTT2.13 **Security Release** has been released to address
[http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1886 CVE-2009-1886]
("Formatstring vulnerability in smbclient") and
[http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1888 CVE-2009-1888] ("Uninitialized read of a data value").
For more information, please see [http://samba.org/samba/history/security.html Samba Security page].
    http://samba.org/samba/security/CVE-2009-1886.html Security Advisory for CVE-2009-1886]
    http://samba.org/samba/security/CVE-2009-1888.html Security Advisory for CVE-2009-1888]

Samba 3.2.12 
------------------------

* otes for Samba 3.2.12
* 2009

===============================
This is a maintenance release of the Samba 3.2 series.
===============================

------------------------

Major enhancements in 3.2.12 include:

* Fix SAMR and LSA checks [https://bugzilla.samba.org/show_bug.cgi?id=6089 bug #6089], [https://bugzilla.samba.org/show_bug.cgi?id=6289 #6289]
* Fix posix acls when setting an ACL without explicit ACE for the owner [https://bugzilla.samba.org/show_bug.cgi?id=2346 bug #2346].
* Fix "force user" [https://bugzilla.samba.org/show_bug.cgi?id=6291 bug #6291].
* Fix Winbind crash [https://bugzilla.samba.org/show_bug.cgi?id=6279 bug #6279].
* Fix joining of Win7 into Samba domain [https://bugzilla.samba.org/show_bug.cgi?id=6099 bug #6099].

----

(**Updated 16-June-2009**)
* Tuesday, June 16 - Samba 3.2.12 has been released
    http://www.samba.org/samba/history/samba-3.2.12.html Release Notes Samba 3.2.12]

Samba 3.2.11 
------------------------

* otes for Samba 3.2.11
* 2009

===============================
This is a maintenance release of the Samba 3.2 series.
===============================

------------------------

Major enhancements in 3.2.11 include:

* Fix domain logins for WinXP clients pre SP3 [https://bugzilla.samba.org/show_bug.cgi?id=6263 bug #6263].
* Fix samr_OpenDomain access checks [https://bugzilla.samba.org/show_bug.cgi?id=6089 bug #6089].
* Fix smbd crash for close_on_completion.

----

(**Updated 17-April-2009**)
* Friday, April 17 - Samba 3.2.11 has been release to address [https://bugzilla.samba.org/show_bug.cgi?id=6263 BUG 6263] and [https://bugzilla.samba.org/show_bug.cgi?id=6089 BUG 6089].
    http://www.samba.org/samba/history/samba-3.2.11.html Release Notes Samba 3.2.11]

Samba 3.2.10 
------------------------

* otes for Samba 3.2.10
* 2009

===============================
This is a maintenance release of the Samba 3.2 series.
===============================

------------------------

In Samba 3.2.9, there is an issue while migrating passdb.tdb files from older Samba versions (e.g. 3.2.8). That causes panics of smbd child processes until the parent smbd is restarted once after converting the passdb.tdb file. This issue is fixed in Samba 3.2.10.

Sorry for the inconveniences!

(**Updated 1-April-2009**)
* Wednesday, April 1 - Samba 3.2.10 has been released due to update problems in Samba 3.2.9 (see [https://bugzilla.samba.org/show_bug.cgi?id=6195 BUG 6195] and the [http:SSLLAASS:HHwww.samba.org/samba/history/samba-3.2.10.html release notes] for more details).

Samba 3.2.9 
------------------------

* otes for Samba 3.2.9
* 2009

===============================
This is a maintenance release of the Samba 3.2 series.
===============================

------------------------

===============================
Major enhancements included in Samba 3.2.9 are:

* Migrating from 3.0.x to 3.3.x can fail to update passdb.tdb correctly [https://bugzilla.samba.org/show_bug.cgi?id=6195 bug #6195].
* Fix guest authentication in setups with "security = share" and "guest ok = yes" when Winbind is running.
* Fix corruptions of source path in tar mode of smbclient [https://bugzilla.samba.org/show_bug.cgi?id=6161 bug #6161].

----

(**Updated 31-March-2009**)
* Tuesday, March 31 - Samba 3.2.9 has been released
    http://www.samba.org/samba/history/samba-3.2.9.html Release Notes Samba 3.2.9]

Samba 3.2.8 
------------------------

* otes for Samba 3.2.8
* 2009

===============================
This is a bug fix release of the Samba 3.2 series.
===============================

------------------------

Major enhancements included in Samba 3.2.8 are:

* Correctly detect if the current DC is the closest one.
* Add saf_join_store() function to memorize the DC used at join time.
* ids problems caused by replication delays shortly after domain joins.

The original security announcement for this and past advisories can be found http://www.samba.org/samba/security/

(**Updated 03-February-2009**)
* Tuesday, February 03 - Samba 3.2.8 has been released
    http://www.samba.org/samba/history/samba-3.2.8.html Release Notes Samba 3.2.8]

Samba 3.2.7 
------------------------

* otes for Samba 3.2.7
* 05 2009

===============================
This is a security release in order to address CVE-2009-0022.
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-0022 CVE-2009-0022] ("Potential access to "/" in setups with registry shares enabled")
* 3.2.0 to 3.2.6, in setups with registry shares enabled, access to the root filesystem ("/") is granted when connecting to a share called "" (empty string) using old versions of smbclient (before 3.0.28).

----

(**Updated 05-January-2009**)
* Monday, January 05 - Samba 3.2.7 **Security Release** has been released to address [http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-0022 CVE-2009-0022] ("Potential access to "/" in setups with registry shares enabled")
    http://www.samba.org/samba/security/CVE-2009-0022.html Security advisory]

Samba 3.2.6 
------------------------

* otes for Samba 3.2.6
* 10, 2008

===============================
This is a bug fix release of the Samba 3.2 series.
===============================

------------------------

Major enhancements included in Samba 3.2.6 are:

* Fix Winbind crash bugs.
* Fix moving of readonly files.
* Fix "write list" in setups using "security = share".
* Fix access to cups-printers with cups 1.3.4.
* Fix timeouts in setups with large groups.
* Fix several bugs concerning Alternate Data Streams.
* Add new SMB traffic analyzer VFS module.

(**Updated 28-November-2008**)
* Wednesday, December 10 - Samba 3.2.6 has been released.
    http://www.samba.org/samba/history/samba-3.2.6.html Release Notes Samba 3.2.6]

Samba 3.2.5 
------------------------

* otes for Samba 3.2.5
* 27 2008

===============================
This is a security release in order to address CVE-2008-4314.
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2008-4314 CVE-2008-4314] ("Potential leak of arbitrary memory contents")
* :OTT29 to 3.2.4 can potentially leak arbitrary memory contents to malicious clients.

(**Updated 27-November-2008**)
* Thursday, November 27 - Samba 3.2.5 **Security Release** has been released to address [http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2008-4314 CVE-2008-4314] ("Potential leak of arbitrary memory contents").
    http://www.samba.org/samba/security/CVE-2008-4314.html Security advisory]

Samba 3.2.4 
------------------------

* otes for Samba 3.2.4
* 18, 2008

===============================
This is a bug fix release of the Samba 3.2 series.
===============================

------------------------

Major bug fixes included in Samba 3.2.4 are:

* Fix Winbind crashes.
* Fix changing of machine account passwords.
* Fix non guest connections to shares when "security = share" is used.
* Fix file write times.

----

(**Updated 18-September-2008**)

* Thursday, September 18 - Samba 3.2.4 has been released
    http://www.samba.org/samba/history/samba-3.2.4.html Release Notes Samba 3.2.4]

Samba 3.2.3 
------------------------

* otes for Samba 3.2.3
* 7 2008

This is a security release in order to address CVE-2008-3789.=
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2008-3789 CVE-2008-3789] ("Wrong permissions of group_mapping.ldb")
* group_mapping.ldb is created with the permissions 0666. That means everyone is able to edit this file and might map any SID to root.

----

(**Updated 27-August-2008**)

* Wednesday, August 27 - Samba 3.2.3 **Security Release** has been released to address CVE-2008-3789 ("Wrong permissions of group_mapping.ldb")       
    http://www.samba.org/samba/security/CVE-2008-3789.html Security advisory]

Samba 3.2.2 
------------------------

* otes for Samba 3.2.2
* 9 2008

===============================
This is a bug fix release of the Samba 3.2 series.
===============================

------------------------

Major bug fixes included in Samba 3.2.2 are:

* Fix removal of dead records in tdb files. This can lead to very large tdb files and to overflowing partitions as a consequence on systems running an nmbd daemon.
* Fix "force group" in setups using Winbind.
* Fix freezing Windows Explorer on WinXP while browsing Samba shares. This one led to timeouts during printing as well.
* Fix assigning of primary group memberships when authenticating via Winbind.
* Fix creation and installation of shared libraries.

----

(**Updated 18-August-2008**)

* Tuesday, August 19 - Planned release date for 3.2.2 (bugfix release)
    http://www.samba.org/samba/history/samba-3.2.2.html Release Notes Samba 3.2.2]

Samba 3.2.1 
------------------------

* otes for Samba 3.2.1
* 2008

===============================
This is the second stable release of Samba 3.2.
===============================

------------------------

Major bug fixes included in Samba 3.2.1 are:

* Race condition in Winbind leading to a crash.
* Regression in Winbindd offline mode.
* Flushing of smb.conf when creating a new share using SWAT.
* Setting of ACEs in setups with "dos filemode = yes".

----

(**Updated 20-July-2008**) 

* Tuesday, August 5 - 3.2.1 has been released.
    http://www.samba.org/samba/history/samba-3.2.1.html Release Notes Samba 3.2.1]

Samba 3.2.0 
------------------------

* otes for Samba 3.2.0
* 008

===============================
This is the first stable release of Samba 3.2.0.
===============================

------------------------

Please be aware that Samba is now distributed under the version 3
of the new GNU General Public License.  You may refer to the COPYING
file that accompanies these release notes for further licensing details.

===============================
Major enhancements in Samba 3.2.0 include:

File Serving:
* Use of IDL generated parsing layer for several DCE/RPC interfaces.
* Removal of the 1024 byte limit on pathnames and 256 byte limit on filename components to honor the MAX_PATH setting from the host OS.
* Introduction of a [http://www.samba.org/samba/docs/man/manpages-3/smb.conf.5.html#id2533226 registry] based configuration system.
* Improved CIFS Unix Extensions support.
* Experimental support for file serving clusters.
* Support for IPv6 in the server, and client tools and libraries.
* Support for storing alternate data streams in xattrs.
* Encrypted SMB transport in client tools and libraries, and server.
* Support for Vista clients authenticating via Kerberos.

Winbind and Active Directory Integration:
* Full support for Windows 2003 cross-forest, transitive trusts and one-way domain trusts.
* Support for userPrincipalName logons via pam_winbind and NSS lookups.
* Expansion of nested domain groups via NSS calls.
* Support for Active Directory LDAP Signing policy.
* New LGPL Winbind client library (libwbclient.so).
* Support for establishing interdomain trust relationships with Windows 2008.

Joining:
* New NetApi library for domain join related queries (libnetapi.so) and example GTK+ Domain join gui.
* New client and server support for remotely joining and unjoining Domains.
* Support for joining into Windows 2008 domains.

----

(**Updated 02-July-2008**) The following time based release schedule is currently in play for Samba 3.2.0:

* Friday, February 29 - Feature freeze for Samba 3.2.0pre2 in the v3-2-stable git branch.
* Tuesday, March 4 - Samba 3.2.0pre2 has been released.
* Friday, March 28 - Planned release date for 3.2.0pre3:oned** due to some critical bugs.
* Friday, April 18 - Planned release date for 3.2.0.:oned**
* Tuesday, April 22 - Feature freeze for Samba 3.2.0pre3 in the v3-2-stable git branch.
* Friday, April 25 - Samba 3.2.0pre3 has been released.
* Friday, May 23 - Samba 3.2.0rc1 has been released.
* Tuesday, June 10 - Samba 3.2.0rc2 has been released.
* Tuesday, July 1 - 3.2.0 final has been released.
    http://www.samba.org/samba/history/samba-3.2.0.html Release Notes Samba 3.2.0]

----
`Category:otes`