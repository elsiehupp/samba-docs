Samba 3.0 Features added/changed
    <namespace>0</namespace>
<last_edited>2017-02-26T21:09:04Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

==============================

.0.26a
===============================

------------------------

* Memory leaks in Winbind's IDMap manager.

===============================
3.0.25 {a/b/c}
===============================

------------------------

* 3.0.25c
** File sharing with Widows 9x clients.
** Winbind running out of file descriptors due to stalled  child processes.
** MS-DFS inter-operability issues.

* 3.0.25b
** Offline caching of files with Windows XP/Vista clients.
** Improper cleanup of expired or invalid byte range locks on files.
** Crashes is idmap_ldap and idmap_rid.

Changes to 'net idmap dump'=
===============================

------------------------

***A change in command line syntax and behavior was introduced in the 3.0.25 release series where the command <nowiki>$ net idmap dump /.../path/to/idmap.tdb</nowiki> would overwrite the tdb instead of dumping its contents to standard output as was the case in releases prior to Samba 3.0.25.  The changed has been reverted in 3.0.25b and the semantics from 3.0.24 and earlier releases have been restored.  

* 3.0.25a
** Missing supplementary Unix group membership when using "force group".
** Premature expiration of domain user passwords when using a Samba domain controller.
** Failure to open the Windows object picker against a server configured to use "security = domain".
** Authentication failures when using security = server.
Changes to MS-DFS Root Share Behavior=
===============================

------------------------

***Please be aware that the initial value for the "msdfs root" share parameter was changed in the 3.0.25 release series and that this option is now disabled by default.  Windows clients frequently require a reboot in order to clear any cached information about MS-DFS root shares on a server and you may experience failures accessing file services on Samba 3.0.25 servers until the client reboot is performed.  Alternately, you may explicitly re-enable the parameter in smb.conf.   Please refer to the smb.conf(5) man page for more details.

* 3.0.25
** Significant improvements in the winbind off-line logon support.
** Support for secure DDNS updates as part of the 'net ads join' process.
** Rewritten IdMap interface which allows for TTL based caching and per domain backends.
** New plug-in interface for the "winbind nss info" parameter.
** New file change notify subsystem which is able to make use of inotify on Linux.
** Support for passing Windows security descriptors to a VFS plug-in allowing for multiple Unix ACL implements to running side by side on the Same server.
** Improved compatibility with Windows Vista clients including improved read performance with Linux servers.
** Man pages for IdMap and VFS plug-ins.

===============================
3.0.23{a,b,c,d}
===============================

------------------------

* Stability fixes for winbindd
* Portability fixes on FreeBSD and Solaris operating systems.
* New "createupn" option to "net ads join"
* Rewritten Kerberos keytab generation when 'use kerberos keytab = yes'
* Improved 'make test'
* New offline mode in winbindd
* New Kerberos support for pam_winbind.so
* New handling of unmapped users and groups
* New non-root share management tools
* Improved support for local and BUILTIN groups
* Winbind IDMAP integration with RFC2307 schema objects supported by Windows 2003 R2
* Rewritten 'net ads join' to mimic Windows XP without requiring administrative rights to join a domain

===============================
3.0.21{a,b,c}
===============================

------------------------

* Complete NTLMv2 support by consolidating authentication mechanism used at the CIFS and RPC layers.
* The capability to manage Unix services using the Win32 Service Control API.
* The capability to view external Unix log files via the Microsoft Event Viewer.
* New libmsrpc share library for application developers.
* Rewrite of CIFS oplock implementation.
* Performance Counter external daemon.
* Winbindd auto-detection query methods when communicating with a domain controller.
* The ability to enumerate long share names in libsmbclient applications.
===============================
3.0.20{a,b}
===============================

------------------------

* Support for several new Win32 rpc pipes.
* Improved support for OS/2 clients.
* New 'net rpc service' tool for managing Win32 services.
* Capability to set the owner on new files and directory based on the parent's ownership.
* Experimental, asynchronous IO file serving support.
* Completed Support for Microsoft Print Migrator.
* New Winbind IDmap plugin (ad) for retrieving uid and gid from AD servers which maintain the SFU user and group attributes.
* Rewritten support for POSIX pathnames when utilizing the Linux CIFS fs client.
* New asynchronous winbindd.
* Support for Microsoft Print Migrator.
* New Windows NT registry file I/O library.
* New user right (SeTakeOwnershipPrivilege) added.
* New "net share migrate" options.

===============================
3.0.14a
===============================

------------------------

Release 3.0.14a is a pure bugfix release which fixed a "show stopper".

<b>Please note, the release policy has changed at this point.</b>

===============================
3.0.14
===============================

------------------------

===============================
3.0.13
===============================

------------------------

===============================
3.0.12
===============================

------------------------

* Performance enhancements when serving directories containing large number of files.
* MS-DFS support added to smbclient.
* More performance improvements when using Samba/OpenLDAP based DC's via the 'ldapsam: s' option.
* Support for the Novell NDS universal password when using the ldapsam passdb backend.
* New 'net rpc trustdom {add,del}' functionality to eventually replace 'smbpasswd {-a,-x} -i'.
* New libsmbclient functionality.

===============================
3.0.11
===============================

------------------------

* Winbindd performance improvements.
* More 'net rpc vampire' functionality.
* Support for the Windows privilege model to assign rights to specific SIDs.
* New administrative options to the 'net rpc' command.

===============================
3.0.10
===============================

------------------------

Release 3.0.10 is a fix for security issues described in CAN-2004-1154.
===============================
3.0.9
===============================

------------------------

Release 3.0.9 is a pure bigfix release which fixes printing problems from Windows 9x, roaming profile updates and unknown symbols for kde

----
`Category:otes`