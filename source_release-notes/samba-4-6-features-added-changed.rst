Samba 4.6 Features added/changed
    <namespace>0</namespace>
<last_edited>2019-09-17T21:51:15Z</last_edited>
<last_editor>Fraz</last_editor>

Samba 4.6 is `Samba_Release_Planning#Discontinued_.28End_of_Life.29|**Discontinued (End of Life)**`.

Samba 4.6.16
------------------------

* Release Notes for Samba 4.6.16
* August 14, 2018

===============================
This is a security release in order to address the following defects:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-10858 CVE-2018-10858] (Insufficient input validation on client directory listing in libsmbclient.)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-10919 CVE-2018-10919] (Confidential attribute disclosure from the AD LDAP server.)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-10858 CVE-2018-10858]: A malicious server could return a directory entry that could corrupt libsmbclient memory.
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-10919 CVE-2018-10919]: Missing access control checks allow discovery of confidential attribute values via authenticated LDAP search expressions.

===============================
Changes since 4.6.15:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13453 BUG #13453]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-10858 CVE-2018-10858]: libsmb: Harden smbc_readdir_internal() against returns from malicious servers.
*  Tim Beale <timbeale@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13434 BUG #13434]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-10919 CVE-2018-10919]: acl_read: Fix unauthorized attribute access via searches.

 https://www.samba.org/samba/history/samba-4.6.16.html

Samba 4.6.15
------------------------

* Release Notes for Samba 4.6.15
* April 13, 2018

===============================
This is the latest stable release of the Samba 4.6 release series.
===============================

------------------------

===============================
Changes since 4.6.14:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13244 BUG #13244]: s3: ldap: Ensure the ADS_STRUCT pointer doesn't get freed on error, we don't own it here.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13270 BUG #13270]: s3: smbd: Fix possible directory fd leak if the underlying OS doesn't support fdopendir().
* * [https://bugzilla.samba.org/show_bug.cgi?id=13375 BUG #13375]: s3: smbd: Unix extensions attempts to change wrong field in fchown call.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13277 BUG #13277]: build: fix libceph-common detection.
*  Poornima G <pgurusid@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13297 BUG #13297]: vfs_glusterfs: Fix the wrong pointer being sent in glfs_fsync_async.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13215 BUG #13215]: Fix smbd panic if the client-supplied channel sequence number wraps.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13240 BUG #13240]: samba: Only use async signal-safe functions in signal handler.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13197 BUG #13197]: SMB2 close/lock/logoff can generate NT_STATUS_NETWORK_SESSION_EXPIRED.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13206 BUG #13206]: Fix authentication with an empty string domain *.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13215 BUG #13215]: s3:smb2_server: correctly maintain request counters for compound requests.
*  Anton Nefedov
* * [https://bugzilla.samba.org/show_bug.cgi?id=13338 BUG #13338]: s3:smbd: Map nterror on smb2_flush errorpath.
*Dan Robertson <drobertson@tripwire.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13310 BUG #13310]: libsmb: Use smb2 tcon if conn_protocol >= SMB2_02.
*  Garming Sam <garming@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13031 BUG #13031]: subnet: Avoid a segfault when renaming subnet objects.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13315 BUG #13315]: s3:smbd: Do not crash if we fail to init the session table.

 https://www.samba.org/samba/history/samba-4.6.15.html

Samba 4.6.14
------------------------

* Release Notes for Samba 4.6.14
* March 13, 2018

===============================
This is a security release in order to address the following defects:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1050 CVE-2018-1050] (Denial of Service Attack on external print server.)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1057 CVE-2018-1057] (Authenticated users can change other users' password.)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1050 CVE-2018-1050]:
* All versions of Samba from 4.0.0 onwards are vulnerable to a denial of service attack when the RPC spoolss service is configured to be run as an external daemon. Missing input sanitization checks on some of the input parameters to spoolss RPC calls could cause the print spooler service to crash.

* There is no known vulnerability associated with this error, merely a denial of service. If the RPC spoolss service is left by default as an internal service, all a client can do is crash its own authenticated connection.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1057 CVE-2018-1057]:
* On a Samba 4 AD DC the LDAP server in all versions of Samba from 4.0.0 onwards incorrectly validates permissions to modify passwords over LDAP allowing authenticated users to change any other users' passwords, including administrative users.

Possible workarounds are described at a dedicated page in the Samba wiki:
* `CVE-2018-1057`

===============================
Changes since 4.6.13:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11343 BUG #11343]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1050 CVE-2018-1050]: Codenomicon crashes in spoolss server code.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13272 BUG #13272]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1057 CVE-2018-1057]: Unprivileged user can change any user (and admin) password.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13272 BUG #13272]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1057 CVE-2018-1057]: Unprivileged user can change any user (and admin) password.

 https://www.samba.org/samba/history/samba-4.6.14.html

Samba 4.6.13
------------------------

* Release Notes for Samba 4.6.13
* February 14, 2017

===============================
This is the latest stable release of the Samba 4.6 release series.
===============================

------------------------

===============================
Changes since 4.6.12:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13193 BUG #13193]: s3: smbd: Use identical logic to test for kernel oplocks on a share.
*  Love Hornquist Astrand <lha@h5l.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12986 BUG #12986]: Kerberos: PKINIT: Can't decode algorithm parameters in clientPublicValue.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13181 BUG #13181]: vfs_fruit: Fail to copy file with empty FinderInfo from Windows client to Samba share with fruit.
*  David Disseldorp <ddiss@suse.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13208 BUG #13208]: vfs_default: Use VFS statvfs macro in fs_capabilities.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13250 BUG #13250]: build: Fix ceph_statx check when configured with libcephfs_dir.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13188 BUG #13188]: ctdb-recovery-helper: Deregister message handler in error paths.
*  Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13189 BUG #13189]: smbd: Fix coredump on failing chdir during logoff.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12986 BUG #12986]: Kerberos: PKINIT: Can't decode algorithm parameters in clientPublicValue.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13132 BUG #13132]: s4:kdc: Only map SDB_ERR_NOT_FOUND_HERE to HDB_ERR_NOT_FOUND_HERE.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13195 BUG #13195]: g_lock: fix cleanup of stale entries in g_lock_trylock().
*  Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13176 BUG #13176]: Fix POSIX ACL support on hpux and possibly other big-endian OSs.

 https://www.samba.org/samba/history/samba-4.6.13.html

Samba 4.6.12
------------------------

* Release Notes for Samba 4.6.12
* December 20, 2017

===============================
This is the latest stable release of the Samba 4.6 release series.
===============================

------------------------

smbclient reparse point symlink parameters reversed=
===============================

------------------------

A bug in smbclient caused the 'symlink' command to reverse the meaning of the new name and link target parameters when creating a reparse point symlink against a Windows server.

This only affects using the smbclient 'symlink' command against a Windows server, not a Samba server using the UNIX extensions (the parameter order is correct in that case) so no existing user scripts that depend on creating symlinks on Samba servers need to change.

As this is a little used feature the ordering of these parameters has been reversed to match the parameter ordering of the UNIX extensions 'symlink' command. This means running 'symlink' against both Windows and Samba now uses the same paramter ordering in both cases. 

The usage message for this command has also been improved to remove confusion.

===============================
Changes since 4.6.11:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13140 BUG #13140]: s3: smbclient: Implement 'volume' command over SMB2.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13171 BUG #13171]: s3: libsmb: Fix valgrind read-after-free error in cli_smb2_close_fnum_recv().
* * [https://bugzilla.samba.org/show_bug.cgi?id=13172 BUG #13172]: s3: libsmb: Fix reversing of oldname/newname paths when creating a reparse point symlink on Windows from smbclient.
*  Timur I. Bakeyev <timur@iXsystems.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12934 BUG #12934]: Build man page for vfs_zfsacl.8 with Samba.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=6133 BUG #6133]: vfs_zfsacl: Fix compilation error.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13051 BUG #13051]: "smb encrypt" setting changes are not fully applied until full smbd restart.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13052 BUG #13052]: winbindd: Fix idmap_rid dependency on trusted domain list.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13155 BUG #13155]: vfs_fruit: Proper VFS-stackable conversion of FinderInfo.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13153 BUG #13153]: ctdb: sock_daemon leaks memory.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13154 BUG #13154]: TCP tickles not getting synchronised on CTDB restart.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13150 BUG #13150]: winbindd: Parent and child share a ctdb connection.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13179 BUG #13179]: pthreadpool: Fix starvation after fork.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13180 BUG #13180]: ctdb: Messaging initialisation for CTDB does not register unique ID.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13149 BUG #13149]: libnet_join: Fix 'net rpc oldjoin'.
*  Noel Power <noel.power@suse.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13166 BUG #13166]: s3:libads: net ads keytab list fails with "Key table name malformed".
*  Christof Schmitt <cs@samba.org>
 :* [https://bugzilla.samba.org/show_bug.cgi?id=13170 BUG #13170]: pthreadpool: Undo put_job when returning error.

 https://www.samba.org/samba/history/samba-4.6.12.html

Samba 4.6.11
------------------------

* Release Notes for Samba 4.6.11
* November 21, 2017

===============================
This is a security release
===============================

------------------------

* in order to address the following defects:
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-14746 CVE-2017-14746] Use-after-free vulnerability.
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-15275 CVE-2017-15275] Server heap memory information leak.

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-14746 CVE-2017-14746]:
* All versions of Samba from 4.0.0 onwards are vulnerable to a use after free vulnerability, where a malicious SMB1 request can be used to control the contents of heap memory via a deallocated heap pointer. It is possible this may be used to compromise the SMB server.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-15275 CVE-2017-15275]:
* All versions of Samba from 3.6.0 onwards are vulnerable to a heap memory information leak, where server allocated heap memory may be returned to the client without being cleared.

* There is no known vulnerability associated with this error, but uncleared heap memory may contain previously used data that may help an attacker compromise the server via other methods. Uncleared heap memory may potentially contain password hashes or other high-value data.

For more details and workarounds, please see the security advisories:

* https://www.samba.org/samba/security/CVE-2017-14746.html
* https://www.samba.org/samba/security/CVE-2017-15275.html

===============================
Changes since 4.6.10
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13041 BUG #13041]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-14746 CVE-2017-14746]: s3: smbd: Fix SMB1 use-after-free crash bug.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13077 BUG #13077]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-15275 CVE-2017-15275]: s3: smbd: Chain code can return uninitialized memory when talloc buffer is grown.

 https://www.samba.org/samba/history/samba-4.6.11.html

Samba 4.6.10
------------------------

* Release Notes for Samba 4.6.10
* November 15, 2017

===============================
This is an additional bugfix release to address a possible data corruption issue. Please update immediately!
===============================

------------------------

For details, please see
* [https://bugzilla.samba.org/show_bug.cgi?id=13130 BUG #13130]: smbd on disk file corruption bug under heavy threaded load.
Samba 4.6.0 and newer is affected by this issue.

===============================
Changes since 4.6.9:
===============================

------------------------

*  Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13091 BUG #13091]: vfs_glusterfs: Fix exporting subdirs with shadow_copy2.
*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13093 BUG #13093]: s3: smbclient: Ensure we call client_clean_name() before all operations on remote pathnames.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13121 BUG #13121]: Non-smbd processes using kernel oplocks can hang smbd.
*  Joe Guo <joeg@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13127 BUG #13127]: python: use communicate to fix Popen deadlock.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13130 BUG #13130]: smbd on disk file corruption bug under heavy threaded load.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13130 BUG #13130]: tevent: version 0.9.34.
*  Anoop C S <anoopcs@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13086 BUG #13086]: vfs_fruit: Replace closedir() by SMB_VFS_CLOSEDIR.
*  Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13047 BUG #13047]: smbd: Move check for SMB2 compound request to new function.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13047 BUG #13047]: s3:vfs_glusterfs: Fix a double free in vfs_gluster_getwd().
* * [https://bugzilla.samba.org/show_bug.cgi?id=13101 BUG #13101]: s4:pyparam: Fix resource leaks on error.
*  Ralph Wuerthner <ralph.wuerthner@de.ibm.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13118 BUG #13118]: s3: smbd: Fix delete-on-close after smb2_find.

 https://www.samba.org/samba/history/samba-4.6.10.html

Samba 4.6.9
------------------------

* Release Notes for Samba 4.6.9
* October 25, 2017

===============================
This is the latest stable release of the Samba 4.6 release series.
===============================

------------------------

===============================
Changes since 4.6.8:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12899 BUG #12899]: s3: libsmb: Reverse sense of 'clear all attributes', ignore attribute change in SMB2 to match SMB1.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12913 BUG #12913]: SMBC_setatr() initially uses an SMB1 call before falling back.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13003 BUG #13003]: Fix segfault on MacOS 10.12.3 clients caused by SMB_VFS_GET_COMPRESSION.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13069 BUG #13069]: sys_getwd() can leak memory or possibly return the wrong errno on older systems.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=6133 BUG #6133]: Cannot delete non-ACL files on Solaris/ZFS/NFSv4 ACL filesystem.
* * [https://bugzilla.samba.org/show_bug.cgi?id=7909 BUG #7909]: vfs_zfs_acl: Map SYNCHRONIZE acl permission statically.
* * [https://bugzilla.samba.org/show_bug.cgi?id=7933 BUG #7933]: vfs_fake_acls: Honor SEC_STD_WRITE_OWNER bit.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12791 BUG #12791]: Kernel oplocks still have issues with named streams.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12944 BUG #12944]: vfs_gpfs: Handle EACCES when fetching DOS attributes.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12991 BUG #12991]: s3/mdssvc: Missing assignment in sl_pack_float.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12995 BUG #12995]: Fix wrong Samba access checks when changing DOS attributes.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13065 BUG #13065]: net: Groupmap cleanup should not delete BUILTIN mappings.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13076 BUG #13076]: Enabling vfs_fruit results in loss of Finder tags and other xattrs.
*  Samuel Cabrero <scabrero@suse.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12993 BUG #12993]: s3: spoolss: Fix GUID string format on GetPrinter info.
*  David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12144 BUG #12144]: smbd/ioctl: Match WS2016 ReFS set compression behaviour.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13012 BUG #13012]: ctdb-daemon: Fix implementation of process_exists control.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13021 BUG #13021]: ctdb: GET_DB_SEQNUM control can cause ctdb to deadlock when databases are frozen.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13029 BUG #13029]: ctdb-daemon: Free up record data if a call request is deferred.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13036 BUG #13036]: ctdb-client: Initialize ctdb_ltdb_header completely for empty record.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13056 BUG #13056]: CTDB starts consuming memory if there are dead nodes in the cluster.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13070 BUG #13070]: ctdb-common: Ignore event scripts with multiple '.'s.
*  Lutz Justen <ljusten@google.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13046 BUG #13046]: libgpo: Sort the GPOs in the correct order.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12973 BUG #12973]: 'smbd' uses a lot of CPU on startup of a connection.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13018 BUG #13018]: charset: Fix str[n]casecmp_m() by comparing lower case values.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13079 BUG #13079]: Can't change password in Samba from a windows client if Samba runs on IPv6 only interface.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12903 BUG #12903]: Fix file change notification for renames.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13006 BUG #13006]: messaging: Avoid a socket leak after fork.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13090 BUG #13090]: vfs_catia: Fix a potential memleak.
*  Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12983 BUG #12983]: vfs_default: Fix passing of errno from async calls.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13032 BUG #13032]: vfs_streams_xattr: Fix segfault when running with log level 10.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12629 BUG #12629]: s3:utils: Do not report an invalid range for AD DC role.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12704 BUG #12704]: s3:libsmb: Print the kinit failed message with DBGLVL_NOTICE.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12956 BUG #12956]: s3:libads: Fix changing passwords with Kerberos.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12975 BUG #12975]: Fix changing the password with 'smbpasswd' as a local user on a domain member.

 https://www.samba.org/samba/history/samba-4.6.9.html

Samba 4.6.8
------------------------

* Release Notes for Samba 4.6.8
* September 20, 2017

===============================
This is a security release in order to address the following defects:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12150 CVE-2017-12150] SMB1/2/3 connections may not require signing where they should
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12151 CVE-2017-12151] SMB3 connections don't keep encryption across DFS redirects
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12163 CVE-2017-12163] Server memory information leak over SMB1

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12150 CVE-2017-12150]:
* A man in the middle attack may hijack client connections.
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12151 CVE-2017-12151]:
* A man in the middle attack can read and may alter confidential documents transferred via a client connection, which are reached via DFS redirect when the original connection used SMB3.
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12163 CVE-2017-12163]:
* Client with write access to a share can cause server memory contents to be written into a file or printer.

For more details and workarounds, please see the security advisories:

* https://www.samba.org/samba/security/CVE-2017-12150.html
* https://www.samba.org/samba/security/CVE-2017-12151.html
* https://www.samba.org/samba/security/CVE-2017-12163.html

===============================
Changes since 4.6.7:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12836 BUG #12836]: s3: smbd: Fix a read after free if a chained SMB1 call goes async.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13020 BUG #13020]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12163 CVE-2017-12163]: s3:smbd: Prevent client short SMB1 write from writing server memory to file.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12885 BUG #12885]: s3/smbd: Let non_widelink_open() chdir() to directories directly.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12996 BUG #12996]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12151 CVE-2017-12151]: Keep required encryption across SMB3 dfs redirects.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12997 BUG #12997]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12150 CVE-2017-12150]: Some code path don't enforce smb signing when they should.

 https://www.samba.org/samba/history/samba-4.6.8.html

Samba 4.6.7
------------------------

* Release Notes for Samba 4.6.7
* August 9, 2017

===============================
This is the latest stable release of the Samba 4.6 release series.
===============================

------------------------

===============================
Changes since 4.6.6:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12836 BUG #12836]: s3: smbd: Fix a read after free if a chained SMB1 call goes async.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11392 BUG #11392]: s4-cldap/netlogon: Match Windows 2012R2 and return NETLOGON_NT_VERSION_5 when version unspecified.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12885 BUG #12885]: s3/smbd: Let non_widelink_open() chdir() to directories directly.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12910 BUG #12910]: s3/notifyd: Ensure notifyd doesn't return from smbd_notifyd_init.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12840 BUG #12840]: vfs_fruit: Add fruit:model = <modelname> parametric option.
*  David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12911 BUG #12911]: vfs_ceph: Fix cephwrap_chdir().
*  Dustin L. Howett
* * [https://bugzilla.samba.org/show_bug.cgi?id=12720 BUG #12720]: idmap_ad: Retry query_user exactly once if we get TLDAP_SERVER_DOWN.
*  Thomas Jarosch <thomas.jarosch@intra2net.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12927 BUG #12927]: s3: libsmb: Fix use-after-free when accessing pointer *p.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12925 BUG #12925]: smbd: Fix a connection run-down race condition.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12782 BUG #12782]: winbindd changes the local password and gets NT_STATUS_WRONG_PASSWORD for the remote change.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12890 BUG #12890]: s3:smbd: consistently use talloc_tos() memory for rpc_pipe_open_interface().
*  Noel Power <noel.power@suse.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12937 BUG #12937]: smbcacls: Don't fail against a directory on Windows using SMB2.
*  Arvid Requate <requate@univention.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11392 BUG #11392]: s4-dsdb/netlogon: Allow missing ntver in cldap ping.
*  Garming Sam <garming@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12813 BUG #12813]: dnsserver: Stop dns_name_equal doing OOB read.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12886 BUG #12886]: s3:client: The smbspool krb5 wrapper needs negotiate for authentication.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12898 BUG #12898]: ctdb-common: Set close-on-exec when creating PID file.

 https://www.samba.org/samba/history/samba-4.6.7.html

Samba 4.6.6
------------------------

* Release Notes for Samba 4.6.6
* July 12, 2017

===============================
This is a security release in order to address the following defect:
===============================

------------------------

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-11103 CVE-2017-11103] (Orpheus' Lyre mutual authentication validation bypass)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-11103 CVE-2017-11103] (Heimdal):
* All versions of Samba from 4.0.0 onwards using embedded Heimdal Kerberos are vulnerable to a man-in-the-middle attack impersonating a trusted server, who may gain elevated access to the domain by returning malicious replication or authorization data.

* Samba binaries built against MIT Kerberos are not vulnerable.

===============================
Changes since 4.6.5
===============================

------------------------

*  Jeffrey Altman <jaltman@secure-endpoints.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12894 BUG #12894]: CVE-2017-11103: Orpheus' Lyre KDC-REP service name validation

 https://www.samba.org/samba/history/samba-4.6.6.html

Samba 4.6.5
------------------------

* Release Notes for Samba 4.6.5
* June 6, 2017

===============================
This is the latest stable release of the Samba 4.6 release series.
===============================

------------------------

===============================
Changes since 4.6.4:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12804 BUG #12804]: s3: VFS: Catia: Ensure path name is also converted.
*  Christian Ambach <ambi@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12765 BUG #12765]: s3:smbcacls add prompt for password.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12562 BUG #12562]: vfs_acl_xattr|tdb: Ensure create mask is at least 0666 if ignore_system_acls is set.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12702 BUG #12702]: Wrong sid->uid mapping for SIDs residing in sIDHistory.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12749 BUG #12749]: vfs_fruit: lp_case_sensitive() does not return a bool.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12766 BUG #12766]: s3/smbd: Update exclusive oplock optimisation to the lease area.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12798 BUG #12798]: s3/smbd: Fix exclusive lease optimisation.
*  Alexander Bokovoy <ab@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12751 BUG #12751]: Allow passing trusted domain password as plain-text to PASSDB layer.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12764 BUG #12764]: systemd: Fix detection of libsystemd.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12697 BUG #12697]: ctdb-readonly: Avoid a tight loop waiting for revoke to complete.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12770 BUG #12770]: ctdb-logging: Initialize DEBUGLEVEL before changing the value.
*  Shilpa Krishnareddy <skrishnareddy@panzura.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12756 BUG #12756]: notify: Fix ordering of events in notifyd.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12757 BUG #12757]: idmap_rfc2307: Lookup of more than two SIDs fails.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12767 BUG #12767]: samba-tool: Let 'samba-tool user syncpasswords' report deletions immediately.
*  Doug Nazar <nazard@nazar.ca>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12760 BUG #12760]: s3: smbd: inotify_map_mask_to_filter incorrectly indexes an array.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12687 BUG #12687]: vfs_expand_msdfs tries to open the remote address as a file path.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12802 BUG #12802]: 'ctdb nodestatus' incorrectly displays status for all nodes with wrong exit code.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12814 BUG #12814]: ctdb-common: Fix crash in logging initialisation.

 https://www.samba.org/samba/history/samba-4.6.5.html

Samba 4.6.4
------------------------

* Release Notes for Samba 4.6.4
* May 24, 2017

===============================
This is a security release in order to address the following defect:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CCVE-2017-7494 CVE-2017-7494] (Remote code execution from a writable share)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CCVE-2017-7494 CVE-2017-7494]:
* All versions of Samba from 3.5.0 onwards are vulnerable to a remote code execution vulnerability, allowing a malicious client to upload a shared library to a writable share, and then cause the server to load and execute it.

===============================
Mitigation from Redhat
===============================

------------------------

* https://access.redhat.com/security/cve/CVE-2017-7494
Any of the following:

# SELinux is enabled by default and our default policy prevents loading of modules from outside of samba's module directories and therefore blocks the exploit
# Mount the filessytem which is used by samba for its writeable share, using "noexec" option.
# Add the parameter:
* ::nt pipe support = no
* :to the [global] section of your smb.conf and restart smbd. This prevents clients from accessing any named pipe endpoints. Note this can disable some expected functionality for Windows clients.

===============================
Changes since 4.6.3:
===============================

------------------------

*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12780 BUG #12780]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CCVE-2017-7494 CVE-2017-7494]: Avoid remote code execution from a writable share.

 https://www.samba.org/samba/history/samba-4.6.4.html

Samba 4.6.3
------------------------

* Release Notes for Samba 4.6.3
* April 25, 2017

===============================
This is the latest stable release of the Samba 4.6 release series.
===============================

------------------------

===============================
Changes since 4.6.2:
===============================

------------------------

*  Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12743 BUG #12743]: s3:vfs:shadow_copy2: vfs_shadow_copy2 fails to list snapshots from shares with GlusterFS backend.
*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12559 BUG #12559]: Fix for Solaris C compiler.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12628 BUG #12628]: s3: locking: Update oplock optimization for the leases era.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12693 BUG #12693]: Make the Solaris C compiler happy.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12695 BUG #12695]: s3: libgpo: Allow skipping GPO objects that don't have the expected LDAP attributes.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12747 BUG #12747]: Fix buffer overflow caused by wrong use of getgroups.
*  Hanno Boeck <hanno@hboeck.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12746 BUG #12746]: lib: debug: Avoid negative array access.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12748 BUG #12748]: cleanupdb: Fix a memory read error.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=7537 BUG #7537]: streams_xattr and kernel oplocks results in NT_STATUS_NETWORK_BUSY.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11961 BUG #11961]: winbindd: idmap_autorid allocates ids for unknown SIDs from other backends.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12565 BUG #12565]: vfs_fruit: Resource fork open request with flags=O_CREAT|O_RDONLY.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12615 BUG #12615]: manpages/vfs_fruit: Document global options.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12624 BUG #12624]: lib/pthreadpool: Fix a memory leak.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12727 BUG #12727]: Lookup-domain for well-known SIDs on a DC.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12728 BUG #12728]: winbindd: Fix error handling in rpc_lookup_sids().
* * [https://bugzilla.samba.org/show_bug.cgi?id=12729 BUG #12729]: winbindd: Trigger possible passdb_dsdb initialisation.
*  Alexander Bokovoy <ab@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12611 BUG #12611]: credentials_krb5: use gss_acquire_cred for client-side GSSAPI use case.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12690 BUG #12690]: lib/crypto: Implement samba.crypto Python module for RC4.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12697 BUG #12697]: ctdb-readonly: Avoid a tight loop waiting for revoke to complete.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12723 BUG #12723]: ctdb_event monitor command crashes if event is not specified.
* * [https://bugzilla.samba.org/show_bug.cgi?id=27331 BUG #12733]: ctdb-docs: Fix documentation of "-n" option to 'ctdb tool'.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12558 BUG #12558]: smbd: Fix smb1 findfirst with DFS.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12610 BUG #12610]: smbd: Do an early exit on negprot failure.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12699 BUG #12699]: winbindd: Fix substitution for 'template homedir'.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12554 BUG #12554]: s4:kdc: Disable principal based autodetected referral detection.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12613 BUG #12613]: idmap_autorid: Allocate new domain range if the callers knows the sid is valid.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12724 BUG #12724]: LINKFLAGS_PYEMBED should not contain -L/some/path.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12725 BUG #12725]: PAM auth with WBFLAG_PAM_GET_PWD_POLICY returns wrong policy for trusted domain.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12731 BUG #12731]: rpcclient: Allow -U'OTHERDOMAIN\user' again.
*  Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12725 BUG #12725]: winbindd: Fix password policy for pam authentication.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12554 BUG #12554]: s3:gse: Correctly handle external trusts with MIT.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12611 BUG #12611]: auth/credentials: Always set the realm if we set the principal from the ccache.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12686 BUG #12686]: replace: Include sysmacros.h.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12687 BUG # 12687]: s3:vfs_expand_msdfs: Do not open the remote address as a file.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12704 BUG #12704]: s3:libsmb: Only print error message if kerberos use is forced.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12708 BUG #12708]: winbindd: Child process crashes when kerberos-authenticating  a user with wrong password.
*  Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12715 BUG #12715]: vfs_fruit: Office document opens as read-only on macOS due to CNID semantics.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12737 BUG #12737]: vfs_acl_xattr: Fix failure to get ACL on Linux if memory is fragmented.

 https://www.samba.org/samba/history/samba-4.6.3.html

Samba 4.6.2
------------------------

* Release Notes for Samba 4.6.2
* March 31, 2017

This is a bug fix release to address a regression introduced by the security fixes for CVE-2017-2619 (Symlink race allows access outside share definition).

Please see [https://bugzilla.samba.org/show_bug.cgi?id=12721 BUG #12721] for details.

===============================
Changes since 4.6.1:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12721 BUG #12721]: Fix regression with "follow symlinks = no".

 https://www.samba.org/samba/history/samba-4.6.2.html

Samba 4.6.1
------------------------

* Release Notes for Samba 4.6.1
* March 23, 2017

===============================
This is a security release in order to address the following defect: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-2619 CVE-2017-2619]
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-2619 CVE-2017-2619] (Symlink race allows access outside share definition)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-2619 CVE-2017-2619]:
* All versions of Samba prior to 4.6.1, 4.5.7, 4.4.11 are vulnerable to a malicious client using a symlink race to allow access to areas of the server file system not exported under the share definition.

* Samba uses the realpath() system call to ensure when a client requests access to a pathname that it is under the exported share path on the server file system.

* Clients that have write access to the exported part of the file system via SMB1 unix extensions or NFS to create symlinks can race the server by renaming a realpath() checked path and then creating a symlink. If the client wins the race it can cause the server to access the new symlink target after the exported share path check has been done. This new symlink target can point to anywhere on the server file system.

* This is a difficult race to win, but theoretically possible. Note that the proof of concept code supplied wins the race reliably only when the server is slowed down using the strace utility running on the server. Exploitation of this bug has not been seen in the wild.

===============================
Changes since 4.6.0:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12496 BUG #12496]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-2619 CVE-2017-2619]: Symlink race permits opening files outside share directory.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12496 BUG #12496]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-2619 CVE-2017-2619]: Symlink race permits opening files outside share directory.

 https://www.samba.org/samba/history/samba-4.6.1.html

Samba 4.6.0
------------------------

<onlyinclude>
* Release Notes for Samba 4.6.0
* March 7, 2017

===============================
Release Announcements
===============================

------------------------

This is the first stable release of Samba 4.6.

Please read the release notes carefully before upgrading.

===============================
UPGRADING
===============================

------------------------

ID Mapping=
===============================

------------------------

We discovered that the majority of users have an invalid or incorrect ID mapping configuration. We implemented checks in the 'testparm' tool to validate the ID mapping configuration. You should run it and check if it prints any warnings or errors after upgrading! If it does you should fix them. See the 'IDENTITY MAPPING CONSIDERATIONS' section in the smb.conf manpage. There are some ID mapping backends which are not allowed to be used for the default backend. Winbind will no longer start if an invalid backend is configured as the default backend.

To avoid problems in future we advise all users to run 'testparm' after changing the smb.conf file!

vfs_fruit option "fruit:resource" spelling correction=
===============================

------------------------

Due to a spelling error in the vfs_fruit option parsing for the "fruit:resource" option, users who have set this option in their smb.conf were still using the default setting "fruit:resource = file" as the parser was looking for the string "fruit:ressource" (two "s").

After upgrading to this Samba version 4.6, you MUST either remove the option from your smb.conf or set it to the default "fruit:resource = file", otherwise your macOS clients will not be able to access the resource fork data.

This version Samba 4.6 accepts both the correct and incorrect spelling, but the next Samba version 4.7 will not accept the wrong spelling.

Users who were using the wrong spelling "ressource" with two "s" can keep the setting, but are advised to switch to the correct spelling.

vfs_fruit Netatalk metadata xattr name on *BSD=
===============================

------------------------

Users on *BSD must rename the metadata xattr used by vfs_fruit when using the default setting "fruit:metadata = netatalk".

Due to a glitch in the Samba xattr API compatibility layer for FreeBSD and a mistake in vfs_fruit, vfs_fruit ended up using the wrong xattr name when configured with "fruit:metadata = netatalk" (default). Instead of the correct

    rg.netatalk.Metadata

it used

    etatalk.Metadata

Starting with Samba 4.6 vfs_fruit will use the correct "org.netatalk.Metadata" which means existing installations must rename this xattrs. For this purpose Samba now includes a new tool `mvxattr`. See below for further details.

===============================
NEW FEATURES/CHANGES
===============================

------------------------

Kerberos client encryption types=
===============================

------------------------

Some parts of Samba (most notably winbindd) perform Kerberos client operations based on a Samba-generated krb5.conf file. A new parameter, "kerberos encryption types" allows configuring the encryption types set in this file, thereby allowing the user to enforce strong or legacy encryption in Kerberos exchanges.

The default value of "all" is compatible with previous behavior, allowing all encryption algorithms to be negotiated. Setting the parameter to "strong" only allows AES-based algorithms to be negotiated. Setting the parameter to "legacy" allows only RC4-HMAC-MD5 - the legacy algorithm for Active Directory. This can solves some corner cases of mixed environments with Server 2003R2 and newer DCs.

Printing=
===============================

------------------------

Support for uploading printer drivers from newer Windows clients (Windows 10) has been added until our implementation of [MS-PAR] protocol is ready. Several issues with uploading different printing drivers have been addressed.

The OS Version for the printing server has been increased to announce Windows Server 2003 R2 SP2. If a driver needs a newer version then you should check the smb.conf manpage for details.

New option for owner inheritance=
===============================

------------------------

The "inherit owner" smb.conf parameter instructs smbd to set the owner of files to be the same as the parent directory's owner. Up until now, this parameter could be set to "yes" or "no". A new option, "unix only", enables this feature only for the UNIX owner of the file, not affecting the SID owner in the Windows NT ACL of the file. This can be used to emulate something very similar to folder quotas.

Multi-process Netlogon support=
===============================

------------------------

The Netlogon server in the Samba AD DC can now run as multiple processes.  The Netlogon server is a part of the AD DC that handles NTLM authentication on behalf of domain members, including file servers, NTLM-authenticated web servers and 802.1x gateways.  The previous restriction to running as a single process has been removed, and it will now run in the same process model as the rest of the 'samba' binary.

As part of this change, the NETLOGON service will now run on a distinct TCP port, rather than being shared with all other RPC services (LSA, SAMR, DRSUAPI etc).

New options for controlling TCP ports used for RPC services=
===============================

------------------------

The new 'rpc server port' option controls the default port used for RPC services other than Netlogon.  The Netlogon server honours instead the 'rpc server port:netlogon' option.  The default value for both these options is the first available port including or after 1024.

AD LDAP and replication performance improvements=
===============================

------------------------

Samba's LDB (the database holding the AD directory tree, as seen via LDAP) and our DRSUAPI replication code continues to improve, particularly in respect to the handling of large numbers of objects or linked attributes.

* We now respect an 'uptodateness vector' which will dramatically reduce the over-replication of links from new DCs.
* We have also made the parsing of on-disk linked attributes much more efficient.
* We rely on ldb 1.1.28.  This ldb version has improved memory handling for ldb search results, improving poorly indexed and unindexed search result processing speed by around 20%.

DNS improvements=
===============================

------------------------

The samba-tool dns subcommand is now much more robust and can delete records in a number of situations where it was not possible to do so in the past.

On the server side, DNS names are now more strictly validated.

CTDB changes=
===============================

------------------------

* "ctdb event" is a new top-level command for interacting with event scripts
* : "ctdb event status" replaces "ctdb scriptstatus" - the latter is maintained for backward compatibility but the output format has been   cleaned up
* : "ctdb event run" replaces "ctdb eventscript"
* : "ctdb event script enable" replaces "ctdb enablescript"
* : "ctdb event script disable" replaces "ctdb disablescript"

The new command "ctdb event script list" lists event scripts.

* CTDB's back-end for running event scripts has been replaced by a separate, long-running daemon ctdbd_eventd.
* Running ctdb interactively will log to stderr
* CTDB logs now include process id for each process
* CTDB tags log messages differently.  Changes include:
* : ctdb-recoverd: Messages from CTDB's recovery daemon
* : ctdb-recovery: Messages from CTDB database recovery
* : ctdb-eventd: Messages from CTDB's event daemon
* : ctdb-takeover: Messages from CTDB's public IP takeover subsystem

* The mapping between symbolic and numeric debug levels has changed
* : Configurations containing numeric debug levels should be updated. Symbolic debug levels are recommended.  See the DEBUG LEVEL section of ctdb(7) for details.

*  Tunable IPAllocAlgorithm replaces LCP2PublicIPs, DeterministicIPs
* :  See ctdb-tunables(7) for details.

* CTDB's configuration tunables should be consistently set across a cluster
* : This has always been the cases for most tunables but this fact is now documented.

* CTDB ships with recovery lock helper call-outs for etcd and Ceph RADOS
* :  To build/install these, use the
* :: "--enable-etcd-reclock" and
* :: "--enable-ceph-reclock" configure options.

winbind changes=
===============================

------------------------

winbind contains code that tries to emulate the group membership calculation that domain controllers do when a user logs in. This group membership calculation is a very complex process, in particular for domain trust relationship situations. Also, in many scenarios it is impossible for winbind to correctly do this calculation due to access restrictions in the domains: winbind using its machine account simply does not have the rights to ask for an arbitrary user's group memberships.

When a user logs in to a Samba server, the domain controller correctly calculates the user's group memberships authoritatively and makes the information available to the Samba server. This is the only reliable way Samba can get informed about the groups a user is member of.

Because of its flakiness, the fallback group membership code is unwished, and our code pathes try hard to only use of the group memberships calculated by the domain controller.

However, a lot of admins rely on the fallback behavior in order to support access for nfs access, ssh public key authentication and passwordless sudo.

That's the reason for changing this back between 4.6.0rc4 and 4.6.0 (See [https://bugzilla.samba.org/show_bug.cgi?id=12612 BUG #12612]).

The winbind change to simplify the calculation of supplementary groups to make it more reliable and predictable has been deferred to 4.7 or later.

This means that "id <username>" without the user having logged in previously stops showing any supplementary groups. Also, it will show "DOMAIN\Domain Users" as the primary group. Once the user has logged in, "id <username>" will correctly show the primary group and supplementary group list.

winbind primary group and nss info=
===============================

------------------------

With 4.6, it will be possible to optionally use the primary group as set in the "Unix Attributes" tab for the local unix token of a domain user.  Before 4.6, the Windows primary group was always chosen as primary group for the local unix token.

To activate the unix primary group, set

 idmap config <DOMAIN> : unix_primary_group = yes

Similarly, set

 idmap config <DOMAIN> : unix_nss_info = yes

to retrieve the home directory and login shell from the "Unix Attributes" of the user. This supersedes the "winbind nss info" parameter with a per-domain configuration option.

mvxattr=
===============================

------------------------

mvxattr is a simple utility to recursively rename extended attributes of all files and directories in a directory tree.

    sage: mvxattr -s STRING -d STRING PATH [PATH ...]
    -s, --from=STRING         xattr source name
    -d, --to=STRING           xattr destination name
    -l, --follow-symlinks     follow symlinks, the default is to ignore them
    -p, --print               print files where the xattr got renamed
    -v, --verbose             print files as they are checked
    -f, --force               force overwriting of destination xattr

    elp options:
    -?, --help                Show this help message
    --usage                   Display brief usage message

idmap_hash=
===============================

------------------------

The idmap_hash module is marked as deprecated with this release and will be removed in a future version. See the manpage of the module for details.

===============================
smb.conf changes
===============================

------------------------

    arameter Name                Description             Default
    -------------                -----------             -------
    erberos encryption types     New                     all
    nherit owner                 New option
    ruit:resource                Spelling correction
    sa over netlogon             New (deprecated)        no
    pc server port               New                     0
</onlyinclude>

===============================
KNOWN ISSUES
===============================

------------------------

 https://wiki.samba.org/index.php/Release_Planning_for_Samba_4.6#Release_blocking_bugs

===============================
CHANGES SINCE 4.6.0rc4==
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12592 BUG #12592]: Fix several issues found by covscan.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12608 BUG #12608]: s3: smbd: Restart reading the incoming SMB2 fd when the send queue is drained.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12427 BUG #12427]: vfs_fruit doesn't work with fruit:metadata=stream.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12526 BUG #12526]: vfs_fruit: Only veto AppleDouble files if "fruit:resource" is set to "file".
* * [https://bugzilla.samba.org/show_bug.cgi?id=12604 BUG #12604]: vfs_fruit: Enabling AAPL extensions must be a global switch.
*  Volker Lendecke <vl@samba.org>
* * BUG 12612: Re-enable token groups fallback.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9048 BUG #9048]: Samba4 ldap error codes.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12557 BUG #12557]: gensec:spnego: Add debug message for the failed principal.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12605 BUG #12605]: s3:winbindd: Fix endless forest trust scan.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12612 BUG #12612]: winbindd: Find the domain based on the sid within wb_lookupusergroups_send().
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12557 BUG #12557]: s3:librpc: Handle gss_min in gse_get_client_auth_token() correctly.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12582 BUG #12582]: idmap_hash: Add a deprecation message, improve the idmap_hash manpage.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12592 BUG #12592]: Fix several issues found by covscan.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12592 BUG #12592]: ctdb-logging: CID 1396883 Dereference null return value (NULL_RETURNS).

===============================
CHANGES SINCE 4.6.0rc3==
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12545 BUG #12545]: s3: rpc_server/mdssvc: Add attribute "kMDItemContentType".
* * [https://bugzilla.samba.org/show_bug.cgi?id=12572 BUG #12572]: s3: smbd: Don't loop infinitely on bad-symlink resolution.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12490 BUG #12490]: vfs_fruit: Correct Netatalk metadata xattr on FreeBSD.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12536 BUG #12536]: s3/smbd: Check for invalid access_mask smbd_calculate_access_mask().
* * [https://bugzilla.samba.org/show_bug.cgi?id=12591 BUG #12591]: vfs_streams_xattr: use fsp, not base_fsp.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12580 BUG #12580]: ctdb-common: Fix use-after-free error in comm_fd_handler().
* * [https://bugzilla.samba.org/show_bug.cgi?id=12595 BUG #12595]: build: Fix generation of CTDB manpages while creating tarball.
*  Bryan Mason <bmason@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12575 BUG #12575]: Modify smbspool_krb5_wrapper to just fall through to smbspool if  AUTH_INFO_REQUIRED is not set or is not "negotiate".
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11830 BUG #11830]: s3:winbindd: Try a NETLOGON connection with noauth over NCACN_NP against trusted domains.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12262 BUG #12262]: 'net ads testjoin' and smb access fails after winbindd changed the trust password.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12585 BUG #12585]: librpc/rpc: fix regression in NT_STATUS_RPC_ENUM_VALUE_OUT_OF_RANGE error mapping.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12586 BUG #12586]: netlogon_creds_cli_LogonSamLogon doesn't work without netr_LogonSamLogonEx.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12587 BUG #12587]: winbindd child segfaults on connect to an NT4 domain.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12588 BUG #12588]: s3:winbindd: Make sure cm_prepare_connection() only returns OK with a valid tree connect.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12598 BUG #12598]: winbindd (as member) requires kerberos against trusted ad domain, while it shouldn't.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12598 BUG #12598]: Backport pytalloc_GenericObject_reference() related changes to 4.6.
*  Garming Sam <garming@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12598 BUG #12598]: dbchecker: Stop ignoring linked cases where both objects are alive.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12571 BUG #12571]: s3-vfs: Only walk the directory once in open_and_sort_dir().
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12589 BUG #12589]: CTDB statd-callout does not cause grace period when CTDB_NFS_CALLOUT="".
* * [https://bugzilla.samba.org/show_bug.cgi?id=12595 BUG #12595]: ctdb-build: Fix RPM build.

===============================
CHANGES SINCE 4.6.0rc2==
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12499 BUG #12499]: s3: vfs: dirsort doesn't handle opendir of "." correctly.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12546 BUG #12546]: s3: VFS: vfs_streams_xattr.c: Make streams_xattr_open() store the same path as streams_xattr_recheck().
* * [https://bugzilla.samba.org/show_bug.cgi?id=12531 BUG #12531]: Make vfs_shadow_copy2 cope with server changing directories.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12543 BUG #12543]: samba-tool: Correct handling of default value for use_ntvfs and use_xattrs.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12573 BUG #12573]: Samba < 4.7 does not know about compatibleFeatures and requiredFeatures.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12577 BUG #12577]: 'samba-tool dbcheck' gives errors on one-way links after a rename.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12184 BUG #12184]: s3/rpc_server: Shared rpc modules loading.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12520 BUG #12520]: Ensure global "smb encrypt = off" is effective.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12524 BUG #12524]: s3/rpc_server: Move rpc_modules.c to its own subsystem.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12541 BUG #12541]: vfs_fruit: checks wrong AAPL config state and so always uses readdirattr.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12551 BUG #12551]: smbd: Fix "map acl inherit" = yes.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12398 BUG #12398]: Replication with DRSUAPI_DRS_CRITICAL_ONLY and DRSUAPI_DRS_GET_ANC results in WERR_DS_DRA_MISSING_PARENT S
* * [https://bugzilla.samba.org/show_bug.cgi?id=12540 BUG #12540]: s3:smbd: allow "server min protocol = SMB3_00" to go via "SMB 2.???" negprot.
*  John Mulligan <jmulligan@nasuni.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12542 BUG #12542]: docs: Improve description of "unix_primary_group" parameter in idmap_ad manpage.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12552 BUG #12552]: waf: Do not install the unit test binary for krb5samba.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12547 BUG #12547]: ctdb-build: Install CTDB tests correctly from toplevel.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12549 BUG #12549]: ctdb-common: ioctl(.. FIONREAD ..) returns an int value.
*  Garming Sam <garming@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12577 BUG #12577]: 'samba-tool dbcheck' gives errors on one-way links after a rename.
*  Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12529 BUG #12529]: waf: Backport finding of pkg-config.

===============================
CHANGES SINCE 4.5.0rc1==
===============================

------------------------

*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12469 BUG #12469]: CTDB lock helper getting stuck trying to lock a record.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12500 BUG #12500]: ctdb-common: Fix a bug in packet reading code for generic socket I/O.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12510 BUG #12510]: sock_daemon_test 4 crashes with SEGV.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12513 BUG #12513]: ctdb-daemon: Remove stale eventd socket.

*  BjÃ¶rn Jacke <bj@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12535 BUG #12535]: vfs_default: Unlock the right file in copy chunk.

*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12509 BUG #12509]: messaging: Fix dead but not cleaned-up-yet destination sockets.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12538 BUG #12538]: Backport winbind fixes.

*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12501 BUG #12460]: s3:winbindd: talloc_steal the extra_data in winbindd_list_users_recv().

*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12511 BUG #12511]: ctdb-takeover: Handle case where there are no RELEASE_IPs to send.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12512 BUG #12512]: ctdb-scripts: Fix remaining uses of "ctdb gratiousarp".
* * [https://bugzilla.samba.org/show_bug.cgi?id=12516 BUG #12516]: /etc/iproute2/rt_tables gets populated with multiple 'default' entries.

* https://www.samba.org/samba/history/samba-4.6.0.html

----
`Category:Release Notes`