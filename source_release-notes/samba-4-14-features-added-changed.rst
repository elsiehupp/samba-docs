Samba 4.14 Features added/
    <namespace>0</namespace>
<last_edited>2021-04-30T11:48:15Z</last_edited>
<last_editor>Fraz</or>
Samba 4.14 is `Samba_Release_Planning#Current_Stable_Release|**Current Stable Release**`.
Samba 4.14.4
------------------------

* Notes for Samba 4.14.4
 29, 2021

===============================
This is a security release in order to address the following defect:

* CVE-2021-20254: Negative idmap cache entries can cause incorrect group entries in the Samba file server process token.

===============================
Details
===============================

------------------------

* [https://samba.org/samba/se/DDAAS/ASSHH202/ml CVE-2021-20254]
 Samba smbd file server must map Windows group identities (SIDs) into unix group ids (gids). The code that performs this had a flaw that could allow it to read data beyond the end of the array in the case where a negative cache entry had been added to the mapping cache. This could cause the calling code to return those values into the process token that stores the group membership for a user.

 commonly this flaw caused the calling code to crash, but an alert user (Peter Eriksson, IT Department, Linköping University) found this flaw by noticing an unprivileged user was able to delete a file within a network share that they should have been disallowed access to.

* of the code paths has not allowed us to discover a way for a remote user to be able to trigger this flaw reproducibly or on demand, but this CVE has been issued out of an abundance of caution.

===============================
Changes since 4.14.3
===============================

------------------------

*  Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=14571 BUG #14571]: CVEDDAA:AASSHH20254: Fix buf overrun in sids_to_unixids().

   [https://samba.org/samba/hi/aDDAA/T14DDOO/tml Release Notes Samba 4.14.4].

Samba 4.14.3
------------------------

* Notes for Samba 4.14.3
 20, 2021

===============================
This is the latest stable release of the Samba 4.14 release series.
===============================

------------------------

===============================
Changes since 4.14.2
===============================

------------------------

*o  Trever L. Adams <trever.adams@gmail.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=14671 BUG #14671]: s3:modu:rusfilter:SS:cent :_VFS c break vfs_virusfilter_openat.
*  Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=14586 BUG #14586]: build:S:otice : Eflex is missing at configure time.
*  Ralph Boehme <slow@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=14672 BUG #14672]: Fix : panic when two clients open same file.
* [https://bugzilla.samba.org/DOOTTcgi?id=14675 BUG #14675]: Fix :ry leak in the RPC server.
* [https://bugzilla.samba.org/DOOTTcgi?id=14679 BUG #14679]: s3: :: f: Edeferred :mes.
*  Samuel Cabrero <scabrero@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=14675 BUG #14675]: s3DDAAS:winspool: Set the:per-request memory context.
*  Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=14675 BUG #14675]: Fix :ry leak in the RPC server.
*  Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=11899 BUG #11899]: third_p Update :pper to version 1.3.2.
* [https://bugzilla.samba.org/DOOTTcgi?id=14640 BUG #14640]: third_p Update :pper to version 1.3.3.
*  David Mulder <dmulder@suse.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=14665 BUG #14665]: sambaDD:date: Test th: Esysvol paths download in case-insensitive way.
*  Sachin Prabhu <sprabhu@redhat.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=14662 BUG #14662]: smbd:SS:sure  is preserved across fsp destructor.
*  Christof Schmitt <cs@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=14663 BUG #14663]: idmap_r and idmap_nss return wrong mapping for uid/gid /ict.
*  Martin Schwenke <martin@meltin.net>
* [https://bugzilla.samba.org/DOOTTcgi?id=14288 BUG #14288]: build:S:nly ad: -Wl,--as-needed when supported.

   [https://samba.org/samba/hi/aDDAA/T14DDOO/tml Release Notes Samba 4.14.3].

Samba 4.14.2
------------------------

* Notes for Samba 4.14.2
 24, 2021

===============================
This is a security release in order to address the following defects:

* [https://samba.org/samba/se/DDAAS/ASSHH278/ml CVE-2020-27840]:  corruption via crafted DN strings.
* [https://samba.org/samba/se/DDAAS/ASSHH202/ml CVE-2021-20277]:  of bounds read in AD DC LDAP server.

===============================
Details
===============================

------------------------

*  [https://samba.org/samba/se/DDAAS/ASSHH278/ml CVE-2020-27840]::
 anonymous attacker can crash the Samba AD DC LDAP server by sending easily crafted DNs as part of a bind request. More serious heap corruption is likely also possible.
*  [https://samba.org/samba/se/DDAAS/ASSHH202/ml CVE-2021-20277]::
* SHHcontrolled LDAP filter strings against the AD DC LDAP server may crash the LDAP server.

For more details, please refer to the security advisories.

===============================
Changes since 4.14.0
===============================

------------------------

*  Release with dependency on ldb version 2.3.0
*  Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=14634 BUG #14634]: [https::/wwwDDO:O/ba/security/CV/021DD/7.h/CEECVE-2021-20277]: Fix out  bounds read in ldb_handler_fold.
*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* [https://bugzilla.samba.org/DOOTTcgi?id=14634 BUG #14634]: [https::/wwwDDO:O/ba/security/CV/020DD/0.h/CEECVE-2020-27840]: Fix unauthentica remote heap corruption via bad DNs.
* [https://bugzilla.samba.org/DOOTTcgi?id=14634 BUG #14634]: [https::/wwwDDO:O/ba/security/CV/021DD/7.h/CEECVE-2021-20277]: Fix out  bounds read in ldb_handler_fold.

   [https://samba.org/samba/hi/aDDAA/T14DDOO/tml Release Notes Samba 4.14.2].

Samba 4.14.0
------------------------

<onlyinclude>
* Notes for Samba 4.14.0
 9, 2021
===============================
Release Announcements
===============================

------------------------

This is the first stable release of the Samba 4.14 release series. Please read the release notes carefully before upgrading.

===============================
New GPG key
===============================

------------------------

The GPG release key for Samba releases changed from:
 <nowiki>
pub   dsa1024/568B7EA 2007-02-04 [SC] [expires: 2021-02-05]
      Key fingerprint = 52FB C0B8 6D95 4B08 4332  4CDC 6F33 915B 6568 B7EA
uid                 [  full  ] Samba Distribution Verification Key <samba-bugs@samba.org>
sub   elg2048/A6DFB44 2007-02-04 [E] [expires: 2021-02-05]

to the following new key:

pub   rsa4096/680B620 2020-12-21 [SC] [expires: 2022-12-21]
      Key fingerprint = 81F5 E283 2BD2 545A 1897  B713 AA99 442F B680 B620
uid                 [ultimate] Samba Distribution Verification Key <samba-bugs@samba.org>
sub   rsa4096/BFD4002 2020-12-21 [E] [expires: 2022-12-21]
 </;
Starting from Jan 21th 2021, all Samba releases will be signed with the new key.

See also GPG_AA99442FB680B620_replaces_6F33915B6568B7EA.txt

===============================
NEW FEATURES/
===============================

Here is a copy of a clarification note added to the Samba code in the file: VFS-License-clarification.txt.=
===============================

------------------------

 <nowiki>
A clarification of our GNU GPL License enforcement boundary within the Samba
Virtual File System (VFS) layer.

Samba is licensed under the GNU GPL. All code committed to the Samba
project or that creates a "modified version" or software "based on" Samba must
be either licensed under the GNU GPL or a compatible license.

Samba has several plug-in interfaces where external code may be called
from Samba GNU GPL licensed code. The most important of these is the
Samba VFS layer.

Samba VFS modules are intimately connected by header files and API
definitions to the part of the Samba code that provides file services,
and as such, code that implements a plug-in Samba VFS module must be
licensed under the GNU GPL or a compatible license.
However, Samba VFS modules may themselves call third-party external
libraries that are not part of the Samba project and are externally
developed and maintained.

As long as these third-party external libraries do not use any of the
Samba internal structure, APIs or interface definitions created by the
Samba project (to the extent that they would be considered subject to the GNU
GPL), then the Samba Team will not consider such third-party external
libraries called from Samba VFS modules as "based on" and/CEEcreating a
"modified version" of the Samba code for the purposes of GNU GPL.
Accordingly, we do not require such libraries be licensed under the GNU GPL
or a GNU GPL compatible license.
</;
===============================
VFS
===============================

------------------------

The effort to modernize Samba's VFS interface has reached a major milestone with the next release Samba 4.14.

For details please refer to the documentation at source3/he_New_/xt or visit the `The_New_VFS`.

===============================
Printing
===============================

------------------------

Publishing printers in AD is more reliable and more printer features are added to the published information in AD. Samba now also supports Windows drivers for the ARM64 architecture.

===============================
Client Group Policy
===============================

------------------------

This release extends Samba to support Group Policy functionality for Winbind clients. Active Directory Administrators can set policies that apply Sudoers configuration, and cron jobs to run hourly, daily, weekly or monthly.

To enable the application of Group Policies on a client, set the global smb.conf option 'apply group policies' to 'yes'. Policies are applied on an interval of every 90 minutes, plus a random offset between 0 and 30 minutes.

Policies applied by Samba are 'non-tattooing', meaning that changes can be reverted by executing the `samba-gpupdate --unapply` command. Policies can be re-applied using the `samba-gpupdate --force` command. To view what policies have been or will be applied to a system, use the `samba-gpupdate --rsop` command.

Administration of Samba policy requires that a Samba ADMX template be uploaded to the SYSVOL share. The samba-tool command `samba-tool gpo admxload` is provided as a convenient method for adding this policy. Once uploaded, policies can be modified in the Group Policy Management Editor under Computer Configuration/Administ/ Templates. Alternatively, Samba policy may be managed using the `samba-tool gpo manage` command. This tool does not require the admx templates to be installed.

===============================
Python 3.6 or later required
===============================

------------------------

Samba's minimum runtime requirement for python was raised to Python 3.6 with samba 4.13.  Samba 4.14 raises this minimum version to Python 3.6 also to build Samba. It is no longer possible to build Samba (even just the file server) with Python versions 2.6 and 2.7.

As Python 2.7 has been End Of Life upstream since April 2020, Samba is dropping ALL Python 2.x support in this release.

===============================
Miscellaneous samba-tool changes
===============================

------------------------

The 'samba-tool' subcommands to manage AD objects (e.g. users, computers and groups) now consistently use the "add" command when adding a new object to the AD. The previous deprecation warnings when using the 'add' commands have been removed. For compatibility reasons, both the 'add' and 'create' commands can be used now.

Users, groups and contacts can now be renamed with the respective rename commands.

Locked users can be unlocked with the new 'samba-tool user unlock' command.

The 'samba-tool user list' and 'samba-tool group listmembers' commands provide additional options to hide expired and disabled user accounts (--hide-expired and --hide-disabled).

===============================
CTDB CHANGES
===============================

------------------------

* The NAT gateway and LVS features now uses the term "leader" to refer to the main node in a group through which traffic is routed and "follower" for other members of a group.  The command for determining the leader has changed to "ctdb natgw leader" (from "ctdb natgw master").  The configuration keyword for indicating that a node can not be the leader of a group has changed to "follower-only" (from "slave-only").  Identical changes were made for LVS.
* Remove "ctdb isnotrecmaster" command.  It isn't used by CTDB's scripts and can be checked by users with "ctdb pnn" and "ctdb recmaster".

===============================
smb.conf changes
===============================

------------------------

    arameter Name                     Description                Default
    -------------                     -----------                -------
    mb encrypt                        Removed
    sync dns timeout                  New                        10
    lient smb encrypt                 New                        default
    onor change notify privilege      New                        No
    mbd force process locks           New                        No
    erver smb encrypt                 New                        default</de>

===============================
CHANGES SINCE 4.14.0rc4
===============================

------------------------

*  Trever L. Adams <trever.adams@gmail.com>
* [https://bugzilla.samba.org/DOOTTcgi?id=14634 BUG #14634]: s3:modu:rusfilter:SS:cent :loc ch cause infinite start-up failure.
*  Peter Eriksson <pen@lysator.liu.se>
* [https://bugzilla.samba.org/DOOTTcgi?id=14648 BUG #14648]: s3: : nf:OTT  missing TALLOC_FREE(frame) in error path.
*:  Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=14636 BUG #14636]: g_lock::Fix uni: variable reads.

===============================
CHANGES SINCE 4.14.0rc3
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=13992 BUG #14604]: smbd:SS: conn:s_done() when forcing a connection closed force a full reload of services.
*  Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=13992 BUG #14593]: dbcheck: Check D Objects and reduce noise in reports about expired tombstones.
*  Ralph Boehme <slow@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=13992 BUG #14619]: vfs: :tore : specific POSIX sys_acl_set_file() functions.
* [https://bugzilla.samba.org/DOOTTcgi?id=13992 BUG #14620]: Fix : build on AIX.
* [https://bugzilla.samba.org/DOOTTcgi?id=13992 BUG #14629]: smbd:SS:n't o: _mode if neither a msdfs symlink nor get_dosmode is requested.
* [https://bugzilla.samba.org/DOOTTcgi?id=13992 BUG #14635]: Fix :ter driver upload.

===============================
CHANGES SINCE 4.14.0rc2
===============================

------------------------

*  Björn Jacke <bj@sernet.de>
* [https://bugzilla.samba.org/DOOTTcgi?id=13992 BUG #14624]: classic: Treat o: Enever expires value right.
*  Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=13992 BUG #13898]: s3:pysm fix   in py_smbd_create_file().
*  Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=13992 BUG #14625]: Fix : share mode double free crash.
*  Paul Wise <pabs3@bonedaddy.net>
* [https://bugzilla.samba.org/DOOTTcgi?id=13992 BUG #12505]: HEIMDAL: krb5_storage_fre should work.

===============================
CHANGES SINCE 4.14.0rc1
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=13992 BUG #13992]: Fix :A RPC share error.
*  Ralph Boehme <slow@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=14602 BUG #14602]: "w:ore domains&qu doesn't prevent user login from trusted domain.
* [https://bugzilla.samba.org/DOOTTcgi?id=14617 BUG #14617]: smbd : s to delete files with wrong permissions (uses guest instead of user from force user =).
*  Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=14539 BUG #14539]: s3:idma Relia returnS:D_TYPE_BOTH.
*  Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/DOOTTcgi?id=14627 BUG #14627]: s3:smbd: Fix :  :y access in posix_sys_acl_blob_get_fd().

===============================
KNOWN ISSUES
===============================

------------------------

`Release_Planning_for_Samba_4.14#Release_blocking_bugs`

    https://samba.org/samba/hi/aDDAA/T14DDOO/tml Release Notes Samba 4.14.0].

----
`Category: Notes`