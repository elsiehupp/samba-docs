Samba 4.12 Features added/changed
    <namespace>0</namespace>
<last_edited>2021-04-30T11:51:09Z</last_edited>
<last_editor>Fraz</last_editor>

Samba 4.12 is `Samba_Release_Planning#Security_Fixes_Only_Mode|**'Security Fixes_Only Mode*`.

Samba 4.12.15
------------------------

* Release Notes for Samba 4.12.15
* April 29, 2021

===============================
This is a security release in order to address the following defect:
===============================

------------------------

* CVE-2021-20254: Negative idmap cache entries can cause incorrect group entries in the Samba file server process token.

===============================
Details
===============================

------------------------

* [https://www.samba.org/samba/security/CVE-2021-20254.html CVE-2021-20254]
* The Samba smbd file server must map Windows group identities (SIDs) into unix group ids (gids). The code that performs this had a flaw that could allow it to read data beyond the end of the array in the case where a negative cache entry had been added to the mapping cache. This could cause the calling code to return those values into the process token that stores the group membership for a user.

* Most commonly this flaw caused the calling code to crash, but an alert user (Peter Eriksson, IT Department, Linköping University) found this flaw by noticing an unprivileged user was able to delete a file within a network share that they should have been disallowed access to.

* Analysis of the code paths has not allowed us to discover a way for a remote user to be able to trigger this flaw reproducibly or on demand, but this CVE has been issued out of an abundance of caution.

===============================
Changes since 4.12.14
===============================

------------------------

*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14571 BUG #14571]: CVE-2021-20254: Fix buffer overrun in sids_to_unixids().

   [https://www.samba.org/samba/history/samba-4.12.15.html Release Notes Samba 4.12.15].

Samba 4.12.14
------------------------

* Release Notes for Samba 4.12.14
* March 24, 2021

===============================
This is a security release in order to address the following defects:
===============================

------------------------

* [https://www.samba.org/samba/security/CVE-2020-27840.html CVE-2020-27840]: Heap corruption via crafted DN strings.
* [https://www.samba.org/samba/security/CVE-2021-20277.html CVE-2021-20277]: Out of bounds read in AD DC LDAP server.

===============================
Details
===============================

------------------------

*  [https://www.samba.org/samba/security/CVE-2020-27840.html CVE-2020-27840]:
* An anonymous attacker can crash the Samba AD DC LDAP server by sending easily crafted DNs as part of a bind request. More serious heap corruption is likely also possible.
*  [https://www.samba.org/samba/security/CVE-2021-20277.html CVE-2021-20277]:
* User-controlled LDAP filter strings against the AD DC LDAP server may crash the LDAP server.

For more details, please refer to the security advisories.

===============================
Changes since 4.12.12
===============================

------------------------

*  Release with dependency on ldb version 2.1.5.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14634 BUG #14634]: [https://www.samba.org/samba/security/CVE-2021-20277.html CVE-2021-20277]: Fix out of bounds read in ldb_handler_fold.
*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14634 BUG #14634]: [https://www.samba.org/samba/security/CVE-2020-27840.html CVE-2020-27840]: Fix unauthenticated remote heap corruption via bad DNs.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14634 BUG #14634]: [https://www.samba.org/samba/security/CVE-2021-20277.html CVE-2021-20277]: Fix out of bounds read in ldb_handler_fold.

   [https://www.samba.org/samba/history/samba-4.12.14.html Release Notes Samba 4.12.14].

Samba 4.12.12
------------------------

* Release Notes for Samba 4.12.12
* March 11, 2021

===============================
This is the latest stable release of the Samba 4.12 release series.
===============================

------------------------

Please note that this will be the last bugfix release of the Samba 4.12 release series. There will be Security Releases only beyond this point.

===============================
New GPG key
===============================

------------------------

The GPG release key for Samba releases changed from:
 <nowiki>
pub   dsa1024/6F33915B6568B7EA 2007-02-04 [SC] [expires: 2021-02-05]
      Key fingerprint = 52FB C0B8 6D95 4B08 4332  4CDC 6F33 915B 6568 B7EA
uid                 [  full  ] Samba Distribution Verification Key <samba-bugs@samba.org>
sub   elg2048/9C6ED163DA6DFB44 2007-02-04 [E] [expires: 2021-02-05]

to the following new key:

pub   rsa4096/AA99442FB680B620 2020-12-21 [SC] [expires: 2022-12-21]
      Key fingerprint = 81F5 E283 2BD2 545A 1897  B713 AA99 442F B680 B620
uid                 [ultimate] Samba Distribution Verification Key <samba-bugs@samba.org>
sub   rsa4096/97EF9386FBFD4002 2020-12-21 [E] [expires: 2022-12-21]
 </nowiki>
Starting from Jan 21th 2021, all Samba releases will be signed with the new key.

See also GPG_AA99442FB680B620_replaces_6F33915B6568B7EA.txt

===============================
Changes since 4.12.11
===============================

------------------------

*  Trever L. Adams <trever.adams@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14634 BUG #14634]: s3:modules:vfs_virusfilter: Recent talloc changes cause infinite start-up failure.
*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13992 BUG #13992]: SAMBA RPC share error.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14612 BUG #14612]: s3: smbd: Add call to conn_setup_case_options() to create_conn_struct_as_root().
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14602 BUG #14602]: s3/auth: Implement "winbind:ignore domains".
* * [https://bugzilla.samba.org/show_bug.cgi?id=14612 BUG #14612]: build: Remove smbd_conn private library.
*  Peter Eriksson <pen@lysator.liu.se>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14648 BUG #14648]: s3: VFS: nfs4_acls. Add missing TALLOC_FREE(frame) in error path.
*  Björn Jacke <bj@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14624 BUG #14624]: classicupgrade: Treat old never expires value right.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14636 BUG #14636]: g_lock: Fix uninitalized variable reads.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13898 BUG #13898]: s3:pysmbd: Fix fd leak in py_smbd_create_file().
* * [https://bugzilla.samba.org/show_bug.cgi?id=14607 BUG #14607]: Work around special SMB2 IOCTL response behavior of NetApp Ontap 7.3.7.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14625 BUG #14625]: Fix smbd share mode double free crash.
*  Paul Wise <pabs3@bonedaddy.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12505 BUG #12505]: HEIMDAL: krb5_storage_free(NULL) should work.

    https://www.samba.org/samba/history/samba-4.12.12.html Release Notes Samba 4.12.12].

Samba 4.12.11
------------------------

* Release Notes for Samba 4.12.11
* January 14, 2021

===============================
This is the latest stable release of the Samba 4.12 release series.
===============================

------------------------

===============================
Changes since 4.12.10
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14210 BUG #14210]: libcli: smb2: Never print length if smb2_signing_key_valid() fails for crypto blob.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14486 BUG #14486]: s3: modules: gluster. Fix the error I made in preventing talloc leaks from a function.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14515 BUG #14515]: s3: smbd: Don't overwrite contents of fsp->aio_requests[0] with NULL via TALLOC_FREE().
* * [https://bugzilla.samba.org/show_bug.cgi?id=14568 BUG #14568]: s3: spoolss: Make parameters in call to user_ok_token() match all other uses.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14590 BUG #14590]: s3: smbd: Quiet log messages from usershares for an unknown share.

*  Dimitry Andric <dimitry@andric.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14605 BUG #14605]: lib: Avoid declaring zero-length VLAs in various messaging functions.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14579 BUG #14579]: Do not create an empty DB when accessing a sam.ldb.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14248 BUG #14248]: samba process does not honor "max log size".
* * [https://bugzilla.samba.org/show_bug.cgi?id=14587 BUG #14587]: vfs_zfsacl: add missing inherited flag on hidden "magic" everyone@ ACE.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14596 BUG #14596]: vfs_fruit may close wrong backend fd.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14486 BUG #14486]: s3-vfs_glusterfs: always disable write-behind translator.
*  Arne Kreddig <arne@kreddig.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14606 BUG #14606]: vfs_virusfilter: Allocate separate memory for config char*.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14596 BUG #14596]: vfs_fruit may close wrong backend fd.
*  Anoop C S <anoopcs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14486 BUG #14486]: manpages/vfs_glusterfs: Mention silent skipping of write-behind translator.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14573 BUG #14573]: vfs_shadow_copy2: Preserve all open flags assuming ROFS.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14601 BUG #14601]: s3:lib: Create the cache path of user gencache recursively.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14594 BUG #14594]: Be more flexible with repository names in CentOS 8 test environments.
*  Jones Syue <jonessyue@qnap.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14514 BUG #14514]: interface: Fix if_index is not parsed correctly.

 [https://www.samba.org/samba/history/samba-4.12.11.html Release Notes Samba 4.12.11].

Samba 4.12.10
------------------------

* Release Notes for Samba 4.12.10
* November 05, 2020

===============================
This is the latest stable release of the Samba 4.12 release series.
===============================

------------------------

===============================
Major enhancements include:
===============================

------------------------

* [https://bugzilla.samba.org/show_bug.cgi?id=14537 BUG #14537]: ctdb-common: Avoid aliasing errors during code optimization.
* [https://bugzilla.samba.org/show_bug.cgi?id=14486 BUG #14486]: vfs_glusterfs: Avoid data corruption with the write-behind translator.

===============================
Details
===============================

------------------------

The GlusterFS write-behind performance translator, when used with Samba, could be a source of data corruption. The translator, while processing a write call, immediately returns success but continues writing the data to the server in the background. This can cause data corruption when two clients relying on Samba to provide data consistency are operating on the same file.

The write-behind translator is enabled by default on GlusterFS. The vfs_glusterfs plugin will check for the presence of the translator and refuse to connect if detected. Please disable the write-behind translator for the GlusterFS volume to allow the plugin to connect to the volume.

===============================
Changes since 4.12.9
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14486 BUG #14486]: s3: modules: vfs_glusterfs: Fix leak of char **lines onto mem_ctx on return.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14471 BUG #14471]: RN: vfs_zfsacl: Only grant DELETE_CHILD if ACL tag is special.
*  Alexander Bokovoy <ab@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14538 BUG #14538]: smb.conf.5: Add clarification how configuration changes reflected by Samba.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14486 BUG #14486]: s3-vfs_glusterfs: Refuse connection when write-behind xlator is present.
* * winexe:: Add configure option to control whether to build it (default: auto).
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14487 BUG #14487]: Latest version of Bind9 is now 9.20.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14537 BUG #14537]: ctdb-common: Avoid aliasing errors during code optimization.
*  Björn Jacke <bjacke@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14522 BUG #14522]: docs: Fix default value of spoolss:architecture.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14531 BUG #14531]: s4:dsdb:acl_read: Implement "List Object" mode feature.
*  Sachin Prabhu <sprabhu@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14486 BUG #14486]: docs-xml/manpages: Add warning about write-behind translator for vfs_glusterfs.
*  Khem Raj <raj.khem@gmail.com>
* * nsswitch/nsstest.c: Avoid nss function conflicts with glibc nss.h.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14513 BUG #14513]: ctdb disable/enable can still fail due to race condition.
*  Andrew Walker <awalker@ixsystems.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14471 BUG #14471]: RN: vfs_zfsacl: Only grant DELETE_CHILD if ACL tag is special.

 [https://www.samba.org/samba/history/samba-4.12.10.html Release Notes Samba 4.12.10].

Samba 4.12.9
------------------------

* Release Notes for Samba 4.12.9
* October 29, 2020

===============================
This is a security release in order to address the following defects:
===============================

------------------------

* [https://www.samba.org/samba/security/CVE-2020-14318.html CVE-2020-14318]: Missing handle permissions check in SMB1/2/3 ChangeNotify.
* [https://www.samba.org/samba/security/CVE-2020-14323.html CVE-2020-14323]: Unprivileged user can crash winbind.
* [https://www.samba.org/samba/security/CVE-2020-14383.html CVE-2020-14383]: An authenticated user can crash the DCE/RPC DNS with easily crafted records.

===============================
Details
===============================

------------------------

* [https://www.samba.org/samba/security/CVE-2020-14318.html CVE-2020-14318]:
* The SMB1/2/3 protocols have a concept of "ChangeNotify", where a client can request file name notification on a directory handle when a condition such as "new file creation" or "file size change" or "file timestamp update" occurs.

* A missing permissions check on a directory handle requesting ChangeNotify meant that a client with a directory handle open only for FILE_READ_ATTRIBUTES (minimal access rights) could be used to obtain change notify replies from the server. These replies contain information that should not be available to directory handles open for FILE_READ_ATTRIBUTE only.

* [https://www.samba.org/samba/security/CVE-2020-14323.html CVE-2020-14323]:
* winbind in version 3.6 and later implements a request to translate multiple Windows SIDs into names in one request. This was done for performance reasons: The Microsoft RPC call domain controllers offer to do this translation, so it was an obvious extension to also offer this batch operation on the winbind unix domain stream socket that is available to local processes on the Samba server.

* Due to improper input validation a hand-crafted packet can make winbind perform a NULL pointer dereference and thus crash.

* [https://www.samba.org/samba/security/CVE-2020-14383.html CVE-2020-14383]:
* Some DNS records (such as MX and NS records) usually contain data in the additional section. Samba's dnsserver RPC pipe (which is an administrative interface not used in the DNS server itself) made an error in handling the case where there are no records present: instead of noticing the lack of records, it dereferenced uninitialised memory, causing the RPC server to crash. This RPC server, which also serves protocols other than dnsserver, will be restarted after a short delay, but it is easy for an authenticated non-admin attacker to crash it again as soon as it returns. The Samba DNS server itself will continue to operate, but many RPC services will not.

For more details, please refer to the security advisories.
* `CVE-2020-14318`
* `CVE-2020-14323`
* `CVE-2020-14383`

===============================
Changes since 4.12.8
===============================

------------------------

o  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14434 BUG #14434]: [https://www.samba.org/samba/security/CVE-2020-14318.html CVE-2020-14318]: s3: smbd: Ensure change notifies can't get set unless the directory handle is open for SEC_DIR_LIST.
*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12795 BUG #12795]: [https://www.samba.org/samba/security/CVE-2020-14383.html CVE-2020-14383]: Remote crash after adding NS or MX records using 'samba-tool'.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14472 BUG #14472]: [https://www.samba.org/samba/security/CVE-2020-14383.html CVE-2020-14383]: Remote crash after adding MX records.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14436 BUG #14436]: [https://www.samba.org/samba/security/CVE-2020-14323.html CVE-2020-14323]: winbind: Fix invalid lookupsids DoS.

 [https://www.samba.org/samba/history/samba-4.12.9.html Release Notes Samba 4.12.9].

Samba 4.12.8
------------------------

* Release Notes for Samba 4.12.8
* October 07, 2020

===============================
This is the latest stable release of the Samba 4.12 release series.
===============================

------------------------

===============================
Changes since 4.12.7
===============================

------------------------

*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14318 BUG #14318]: docs: Add missing winexe manpage.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14465 BUG #14465]: idmap_ad does not deal properly with a RFC4511 section 4.4.1 response.
*  Laurent Menase <laurent.menase@hpe.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14388 BUG #14388]: winbind: Fix a memleak.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14465 BUG #14465]: idmap_ad does not deal properly with a RFC4511 section 4.4.1 response.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14482 BUG #14482]: Compilation of heimdal tree fails if libbsd is not installed.
*  Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14166 BUG #14166]: util: Allow symlinks in directory_create_or_exist.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14399 BUG #14399]: waf: Only use gnutls_aead_cipher_encryptv2() for GnuTLS > 3.6.14.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14467 BUG #14467]: s3:smbd: Fix %U substitutions if it contains a domain name.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14466 BUG #14466]: ctdb disable/enable can fail due to race condition.

 [https://www.samba.org/samba/history/samba-4.12.8.html Release Notes Samba 4.12.8].

Samba 4.12.7
------------------------

* Release Notes for Samba 4.12.7
* September 18, 2020

===============================
This is a **security** release in order to address the following defect:
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-1472 CVE-2020-1472]: Unauthenticated domain takeover via netlogon ("ZeroLogon").

The following applies to Samba used as domain controller only (most seriously the Active Directory DC, but also the classic/NT4-style DC).

Installations running Samba as a file server only are not directly affected by this flaw, though they may need configuration changes to continue to talk to domain controllers (see "file servers and domain members" below).

The netlogon protocol contains a flaw that allows an authentication bypass. This was reported and patched by Microsoft as [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-1472 CVE-2020-1472]. Since the bug is a protocol level flaw, and Samba implements the protocol, Samba is also vulnerable.

However, since version 4.8 (released in March 2018), the default behaviour of Samba has been to insist on a secure netlogon channel, which is a sufficient fix against the known exploits. This default is equivalent to having 'server schannel = yes' in the smb.conf.

Therefore versions 4.8 and above are not vulnerable unless they have the smb.conf lines 
 'server schannel = no' 
or 
 'server schannel = auto'.

Samba versions 4.7 and below are vulnerable unless they have 
 'server schannel = yes' 
in the smb.conf.

* Note: each domain controller needs the correct settings in its smb.conf.

Vendors supporting Samba 4.7 and below are advised to patch their installations and packages to add this line to the [global] section if their smb.conf file.

The 'server schannel = yes' smb.conf line is equivalent to Microsoft's 'FullSecureChannelProtection=1' registry key, the introduction of which we understand forms the core of Microsoft's fix.

Some domains employ third-party software that will not work with a 'server schannel = yes'. For these cases patches are available that allow specific machines to use insecure netlogon. For example, the following smb.conf:

   server schannel = yes
   server require schannel:triceratops$ = no
   server require schannel:greywacke$ = no

will allow only "triceratops$" and "greywacke$" to avoid schannel.

More details can be found here:
* ` CVE-2020-1472`

===============================
Changes since 4.12.6
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14497 BUG #14497]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-1472 CVE-2020-1472](ZeroLogon): s3:rpc_server/netlogon: Protect netr_ServerPasswordSet2 against unencrypted passwords.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14497 BUG #14497]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-1472 CVE-2020-1472](ZeroLogon): s3:rpc_server/netlogon: Support "server require schannel:WORKSTATION$ = no" about unsecure configurations.
*  Gary Lockyer <gary@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14497 BUG #14497]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-1472 CVE-2020-1472](ZeroLogon): s4 torture rpc: repeated bytes in client challenge.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14497 BUG #14497]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-1472 CVE-2020-1472](ZeroLogon): libcli/auth: Reject weak client challenges in netlogon_creds_server_init() "server require schannel:WORKSTATION$ = no".

 [https://www.samba.org/samba/history/samba-4.12.7.html Release Notes Samba 4.12.7].

Samba 4.12.6
------------------------

* Release Notes for Samba 4.12.6
* August 13, 2020

===============================
This is the latest stable release of the Samba 4.12 release series.
===============================

------------------------

===============================
Changes since 4.12.5
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14403 BUG #14403]: s3: libsmb: Fix SMB2 client rename bug to a Windows server.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14424 BUG #14424]: dsdb: Allow "password hash userPassword schemes = CryptSHA256" to work on RHEL7.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14450 BUG #14450]: dbcheck: Allow a dangling forward link outside our known NCs.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14426 BUG #14426]: lib/debug: Set the correct default backend loglevel to MAX_DEBUG_LEVEL.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14428 BUG #14428]: PANIC: Assert failed in get_lease_type().
*  Bjoern Jacke <bjacke@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14422 BUG #14422]: util: Fix build on AIX by fixing the order of replace.h include.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14355 BUG #14355]: srvsvc_NetFileEnum asserts with open files.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14354 BUG #14354]: KDC breaks with DES keys still in the database and msDS-SupportedEncryptionTypes 31 indicating support for it.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14427 BUG #14427]: s3:smbd: Make sure vfs_ChDir() always sets conn->cwd_fsp->fh->fd = AT_FDCWD.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14428 BUG #14428]: PANIC: Assert failed in get_lease_type().
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14358 BUG #14358]: docs: Fix documentation for require_membership_of of pam_winbind.conf.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14444 BUG #14444]: ctdb-scripts: Use nfsconf utility for variable values in CTDB NFS scripts.
*  Andrew Walker <awalker@ixsystems.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14425 BUG #14425]: s3:winbind:idmap_ad: Make failure to get attrnames for schema mode fatal.

 [https://www.samba.org/samba/history/samba-4.12.6.html Release Notes Samba 4.12.6].

Samba 4.12.5
------------------------

* Release Notes for Samba 4.12.5
* July 02, 2020

===============================
This is the latest stable release of the Samba 4.12 release series.
===============================

------------------------

===============================
Changes since 4.12.4
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14301 BUG #14301]: Fix smbd panic on force-close share during async io.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14374 BUG #14374]: Fix segfault when using SMBC_opendir_ctx() routine for share folder that contains incorrect symbols in any file name.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14391 BUG #14391]: Fix DFS links.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14310 BUG #14310]: Can't use DNS functionality after a Windows DC has been in domain.
*  Alexander Bokovoy <ab@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14413 BUG #14413]: ldapi search to FreeIPA crashes.
*  Isaac Boukris <iboukris@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14396 BUG #14396]: Add net-ads-join dnshostname=fqdn option.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14406 BUG #14406]: Fix adding msDS-AdditionalDnsHostName to keytab with Windows DC.
*  Björn Jacke <bj@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14386 BUG #14386]: docs-xml: Update list of posible VFS operations for vfs_full_audit.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14382 BUG #14382]: winbindd: Fix a use-after-free when winbind clients exit.
* Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14370 BUG #14370]: Client tools are not able to read gencache anymore.

 [https://www.samba.org/samba/history/samba-4.12.5.html Release Notes Samba 4.12.5].

Samba 4.12.4
------------------------

* Release Notes for Samba 4.12.4
* July 02, 2020

===============================
This is a **security release** in order to address the following defects:
===============================

------------------------

* [https://www.samba.org/samba/security/CVE-2020-10730.html CVE-2020-10730]: NULL pointer de-reference and use-after-free in Samba AD DC LDAP Server with ASQ, VLV and paged_results.
* [https://www.samba.org/samba/security/CVE-2020-10745.html CVE-2020-10745]: Parsing and packing of NBT and DNS packets can consume excessive CPU
* [https://www.samba.org/samba/security/CVE-2020-10760.html CVE-2020-10760]: LDAP Use-after-free in Samba AD DC Global Catalog with  paged_results and VLV.
* [https://www.samba.org/samba/security/CVE-2020-14303.html CVE-2020-14303]: Empty UDP packet DoS in Samba AD DC nbtd.

===============================
Details
===============================

------------------------

* [https://www.samba.org/samba/security/CVE-2020-10730.html CVE-2020-10730]:
* A client combining the 'ASQ' and 'VLV' LDAP controls can cause a NULL pointer de-reference and further combinations with the LDAP paged_results feature can give a use-after-free in Samba's AD DC LDAP server.
* [https://www.samba.org/samba/security/CVE-2020-10745.html CVE-2020-10745]:
* Parsing and packing of NBT and DNS packets can consume excessive CPU.
*  [https://www.samba.org/samba/security/CVE-2020-10760.html CVE-2020-10760]:
* The use of the paged_results or VLV controls against the Global Catalog LDAP server on the AD DC will cause a use-after-free.
*  [https://www.samba.org/samba/security/CVE-2020-14303.html CVE-2020-14303]:
* The AD DC NBT server in Samba 4.0 will enter a CPU spin and not process further requests once it receives an empty (zero-length) UDP packet to port 137.

For more details, please refer to the security advisories.

===============================
Changes since 4.12.3
===============================

------------------------

* Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14378 BUG #14378]: [https://www.samba.org/samba/security/CVE-2020-10745.html CVE-2020-10745]: Invalid DNS or NBT queries containing dots use several seconds of CPU each.
* Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14364 BUG #14364]: [https://www.samba.org/samba/security/CVE-2020-10730.html CVE-2020-10730]: NULL de-reference in AD DC LDAP server when ASQ and VLV combined.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14402 BUG #14402]: [https://www.samba.org/samba/security/CVE-2020-10760.html CVE-2020-10760]: Fix use-after-free in AD DC Global Catalog LDAP server with paged_result or VLV.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14417 BUG #14417]: [https://www.samba.org/samba/security/CVE-2020-14303.html CVE-2020-14303]: Fix endless loop from empty UDP packet sent to AD DC nbt_server.
*  Gary Lockyer <gary@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14364 BUG #14364]: [https://www.samba.org/samba/security/CVE-2020-10730.html CVE-2020-10730]: NULL de-reference in AD DC LDAP server when ASQ and VLV combined, ldb: Bump version to 1.5.8.

 [https://www.samba.org/samba/history/samba-4.12.4.html Release Notes Samba 4.12.4].

Samba 4.12.3
------------------------

* Release Notes for Samba 4.12.3
* May 19, 2020

===============================
This is the latest stable release of the Samba 4.12 release series.
===============================

------------------------

===============================
Changes since 4.12.2
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14301 BUG #14301]: Fix smbd panic on force-close share during async io.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14343 BUG #14343]: s3: vfs_full_audit: Add missing fcntl entry in vfs_op_names[] array.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14361 BUG #14361]: vfs_io_uring: Fix data corruption with Windows clients.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14372 BUG #14372]: Fix smbd crashes when MacOS Catalina connects if iconv initialization fails.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14150 BUG #14150]: Exporting from macOS Adobe Illustrator creates multiple copies.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14256 BUG #14256]: smbd does a chdir() twice per request.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14320 BUG #14320]: smbd mistakenly updates a file's write-time on close.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14350 BUG #14350]: vfs_shadow_copy2: implement case canonicalisation in shadow_copy2_get_real_filename().
* * [https://bugzilla.samba.org/show_bug.cgi?id=14375 BUG #14375]: Fix Windows 7 clients problem after upgrading samba file server.
*  Alexander Bokovoy <ab@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14359 BUG #14359]: s3: Pass DCE RPC handle type to create_policy_hnd.
*  Isaac Boukris <iboukris@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14155 BUG #14155]: Fix uxsuccess test with new MIT krb5 library 1.18.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14342 BUG #14342]: mit-kdc: Explicitly reject S4U requests.
*  Anoop C S <anoopcs@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14352 BUG #14352]: dbwrap_watch: Set rec->value_valid while returning nested share_mode_do_locked().
*  Amit Kumar <amitkuma@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14345 BUG #14345]: lib:util: Fix smbclient -l basename dir.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14336 BUG #14336]: s3:libads: Fix ads_get_upn().
* * [https://bugzilla.samba.org/show_bug.cgi?id=14348 BUG #14348]: ctdb: Fix a memleak.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14366 BUG #14366]: Malicous SMB1 server can crash libsmbclient.
*  Gary Lockyer <gary@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14330 BUG #14330]: ldb: Bump version to 2.1.3, LMDB databases can grow without bounds
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14361 BUG #14361]: vfs_io_uring: Fix data corruption with Windows clients.
*  Noel Power <noel.power@suse.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14344 BUG #14344]: s3/librpc/crypto: Fix double free with unresolved credential cache.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14358 BUG #14358]: docs-xml: Fix usernames in pam_winbind manpages.

 [https://www.samba.org/samba/history/samba-4.12.3.html Release Notes Samba 4.12.3].

Samba 4.12.2
------------------------

* Release Notes for Samba 4.12.2
* April 28, 2020

===============================
This is a **Security Release** in order to address the following defects:
===============================

------------------------

* [https://www.samba.org/samba/security/CVE-2020-10700.html CVE-2020-10700] Use-after-free in Samba AD DC LDAP Server with ASQ.
* [https://www.samba.org/samba/security/CVE-2020-10704.html CVE-2020-10704] LDAP Denial of Service (stack overflow) in Samba AD DC.

===============================
Details
===============================

------------------------

[https://www.samba.org/samba/security/CVE-2020-10700.html CVE-2020-10700]
* A client combining the 'ASQ' and 'Paged Results' LDAP controls can cause a use-after-free in Samba's AD DC LDAP server.
[https://www.samba.org/samba/security/CVE-2020-10704.html CVE-2020-10704]
* A deeply nested filter in an un-authenticated LDAP search can exhaust the LDAP server's stack memory causing a SIGSEGV.

For more details, please refer to the security advisories.

===============================
Changes since 4.12.1
===============================

------------------------

* Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14331 BUG #14331]: [https://www.samba.org/samba/security/CVE-2020-10700.html CVE-2020-10700]: Fix use-after-free in AD DC LDAP server when ASQ and paged_results combined.

* Gary Lockyer <gary@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=20454 BUG #20454]: [https://www.samba.org/samba/security/CVE-2020-10704.html CVE-2020-10704]: Fix LDAP Denial of Service (stack overflow) in Samba AD DC.

 [https://www.samba.org/samba/history/samba-4.12.2.html Release Notes Samba 4.12.2].

Samba 4.12.1
------------------------

* Release Notes for Samba 4.12.1
* April 07, 2020

===============================
This is the latest stable release of the Samba 4.12 release series.
===============================

------------------------

===============================
Changes since 4.12.0
===============================

------------------------

*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14242 BUG #14242]: nmblib: avoid undefined behaviour in handle_name_ptrs().
*  Björn Baumbach <bb@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14296 BUG #14296]: samba-tool group: Handle group names with special chars correctly.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14293 BUG #14293]: Add missing check for DMAPI offline status in async DOS attributes.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14295 BUG #14295]: Starting ctdb node that was powered off hard before results in recovery loop.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14307 BUG #14307]: smbd: Ignore set NTACL requests which contain S-1-5-88 NFS ACEs.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14316 BUG #14316]: vfs_recycle: Prevent flooding the log if we're called on non-existant paths.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14313 BUG #14313]: librpc: Fix IDL for svcctl_ChangeServiceConfigW.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14327 BUG #14327]: nsswitch: Fix use-after-free causing segfault in _pam_delete_cred.
*  Art M. Gallagher <repos@artmg.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13622 BUG #13622]: fruit:time machine max size is broken on arm.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14294 BUG #14294]: CTDB recovery corner cases can cause record resurrection and node banning.
*  Noel Power <noel.power@suse.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14332 BUG #14332]: s3/utils: Fix double free error with smbtree.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14294 BUG #14294]: CTDB recovery corner cases can cause record resurrection and node banning.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14295 BUG #14295]: Starting ctdb node that was powered off hard before results in recovery loop.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14324 BUG #14324]: CTDB recovery daemon can crash due to dereference of NULL pointer.

 https://www.samba.org/samba/history/samba-4.12.1.html

Samba 4.12.0
------------------------

<onlyinclude>
* Release Notes for Samba 4.12.0
* March 03, 2020
===============================
Release Announcements
===============================

------------------------

This is the first stable release of the Samba 4.12 release series. 

Please read the release notes carefully before upgrading.

===============================
NEW FEATURES/CHANGES
===============================

------------------------

Python 3.5 Required=
===============================

------------------------

Samba's minimum runtime requirement for python was raised to Python 3.4 with samba 4.11.  Samba 4.12 raises this minimum version to Python 3.5 both to access new features and because this is the oldest version we test with in our CI infrastructure.

* (Build time support for the file server with Python 2.6 has not changed)

Removing in-tree cryptography: GnuTLS 3.4.7 required=
===============================

------------------------

Samba is making efforts to remove in-tree cryptographic functionality, and to instead rely on externally maintained libraries.  To this end, Samba has chosen GnuTLS as our standard cryptographic provider.

Samba now requires GnuTLS 3.4.7 to be installed (including development headers at build time) for all configurations, not just the Samba AD DC.

Thanks to this work Samba no longer ships an in-tree DES implementation and on GnuTLS 3.6.5 or later Samba will include no in-tree cryptography other than the MD4 hash and that implemented in our copy of Heimdal.

Using GnuTLS for SMB3 encryption you will notice huge performance and copy speed improvements. Tests with the CIFS Kernel client from Linux Kernel 5.3 show a 3x speed improvement for writing and a 2.5x speed improvement for reads!

* NOTE WELL: The use of GnuTLS means that Samba will honour the system-wide 'FIPS mode' (a reference to the US FIPS-140 cryptographic standard) and so will not operate in many still common situations if this system-wide parameter is in effect, as many of our protocols rely on outdated cryptography.

A future Samba version will mitigate this to some extent where good cryptography effectively wraps bad cryptography, but for now that above applies.

zlib library is now required to build Samba=
===============================

------------------------

Samba no longer includes a local copy of zlib in our source tarball. By removing this we do not need to ship (even where we did not build) the old, broken zip encryption code found there.

New Spotlight backend for Elasticsearch=
===============================

------------------------

Support for the macOS specific Spotlight search protocol has been enhanced significantly. Starting with 4.12 Samba supports using Elasticsearch as search backend. Various new parameters have been added to configure this:

    potlight backend = noindex | elasticsearch | tracker
    lasticsearch:address = ADDRESS
    lasticsearch:port = PORT
    lasticsearch:use tls = BOOLEAN
    lasticsearch:index = INDEXNAME
    lasticsearch:mappings = PATH
    lasticsearch:max results = NUMBER

Samba also ships a Spotlight client command "mdfind" which can be used to search any SMB server that runs the Spotlight RPC service. See the manpage of mdfind for details.

Note that when upgrading existing installations that are using the previous default Spotlight backend Gnome Tracker must explicitly set "spotlight backend = tracker" as the new default is "noindex".

"net ads kerberos pac save" and "net eventlog export"=
===============================

------------------------

The "net ads kerberos pac save" and "net eventlog export" tools will no longer silently overwrite an existing file during data export.  If the filename given exits, an error will be shown.

Fuzzing=
===============================

------------------------

A large number of fuzz targets have been added to Samba, and Samba has been registered in Google's oss-fuzz cloud fuzzing service.  In particular, we now have good fuzzing coverage of our generated NDR parsing code.

A large number of issues have been found and fixed thanks to this effort.

'samba-tool' improvements add contacts as member to groups=
===============================

------------------------

Previously 'samba-tool group addmemers' can just add users, groups and computers as members to groups. But also contacts can be members of groups. Samba 4.12 adds the functionality to add contacts to groups. Since contacts have no sAMAccountName, it's possible that there are more than one contact with the same name in different organizational units. Therefore it's necessary to have an option to handle group members by their DN.

To get the DN of an object there is now the "--full-dn" option available for all necessary commands.

The MS Windows UI allows to search for specific types of group members when searching for new members for a group. This feature is included here with the new samba-tool group addmembers "--object-type=OBJECTYPE" option. The different types are selected accordingly to the Windows UI. The default samba-toole behaviour shouldn't be changed.

Allow filtering by OU or subtree in samba-tool=
===============================

------------------------

A new "--base-dn" and "--member-base-dn" option is added to relevant samba-tool user, group and ou management commands to allow operation on just one part of the AD tree, such as a single OU.

VFS=
===============================

------------------------

SMB_VFS_NTIMES=
===============================

------------------------

Samba now uses a sentinel value based on utimensat(2) UTIME_OMIT to denote to-be-ignored timestamp variables passed to the SMB_VFS_NTIMES() VFS function.

VFS modules can check whether any of the time values inside a struct smb_file_time is to be ignored by calling is_omit_timespec() on the value.

'io_uring' vfs module=
===============================

------------------------

The module makes use of the new io_uring infrastructure (intruduced in Linux 5.1), see https://lwn.net/Articles/776703/

Currently this implements SMB_VFS_{PREAD,PWRITE,FSYNC}_SEND/RECV and avoids the overhead of the userspace threadpool in the default vfs backend. See also vfs_io_uring(8).

In order to build the module you need the liburing userspace library and its developement headers installed, see https://git.kernel.dk/cgit/liburing/

At runtime you'll need a Linux kernel with version 5.1 or higher. Note that 5.4.14 and 5.4.15 have a regression that breaks the Samba module! The regression was fixed in Linux 5.4.16 again.

MS-DFS changes in the VFS=
===============================

------------------------

This release changes set getting and setting of MS-DFS redirects on the filesystem to go through two new VFS functions:

 SMB_VFS_CREATE_DFS_PATHAT()
 SMB_VFS_READ_DFS_PATHAT()

instead of smbd explicitly storing MS-DFS redirects inside symbolic links on the filesystem. The underlying default implementations of this has not changed, the redirects are still stored inside symbolic links on the filesystem, but moving the creation and reading of these links into the VFS as first-class functions now allows alternate methods of storing them (maybe in extended attributes) for OEMs who don't want to mis-use filesystem symbolic links in this way.

CTDB changes=
===============================

------------------------

* The ctdb_mutex_fcntl_helper periodically re-checks the lock file

* The re-check period is specified using a 2nd argument to this helper.  The default re-check period is 5s.

* If the file no longer exists or the inode number changes then the helper exits.  This triggers an election.

===============================
REMOVED FEATURES
===============================

------------------------

smb.conf "write cache size"=
===============================

------------------------

The smb.conf parameter "write cache size" has been removed.

Since the in-memory write caching code was written, our write path has changed significantly. In particular we have gained very flexible support for async I/O, with the new linux io_uring interface in development.  The old write cache concept which cached data in main memory followed by a blocking pwrite no longer gives any improvement on modern systems, and may make performance worse on memory-contrained systems, so this functionality should not be enabled in core smbd code.

In addition, it complicated the write code, which is a performance critical code path.

If required for specialist purposes, it can be recreated as a VFS module.

Retiring DES encryption types in Kerberos.=
===============================

------------------------

With this release, support for DES encryption types has been removed from Samba, and setting DES_ONLY flag for an account will cause Kerberos authentication to fail for that account (see RFC-6649).

Samba-DC: DES keys no longer saved in DB.=
===============================

------------------------

When a new password is set for an account, Samba DC will store random keys in DB instead of DES keys derived from the password.  If the account is being migrated to Windbows or to an older version of Samba in order to use DES keys, the password must be reset to make it work.

Heimdal-DC: removal of weak-crypto.=
===============================

------------------------

Following removal of DES encryption types from Samba, the embedded Heimdal build has been updated to not compile weak crypto code (HEIM_WEAK_CRYPTO).

vfs_netatalk: The netatalk VFS module has been removed.=
===============================

------------------------

The netatalk VFS module has been removed. It was unmaintained and is not needed any more.

BIND9_FLATFILE deprecated=
===============================

------------------------

The BIND9_FLATFILE DNS backend is deprecated in this release and will be removed in the future.  This was only practically useful on a single domain controller or under expert care and supervision.

This release removes the 'rndc command' smb.conf parameter, which supported this configuration by writing out a list of DCs permitted to make changes to the DNS Zone and nudging the 'named' server if a new DC was added to the domain.  Administrators using BIND9_FLATFILE will need to maintain this manually from now on.

===============================
smb.conf changes
===============================

------------------------

    arameter Name                     Description                Default
    -------------                     -----------                -------
    lasticsearch:address              New                        localhost
    lasticsearch:port                 New                        9200
    lasticsearch:use tls              New                        No
    lasticsearch:index                New                        _all
    lasticsearch:mappings             New                        DATADIR/elasticsearch_mappings.json
    lasticsearch:max results          New                        100
    fs4:acedup                        Changed default            merge
    ndc command                       Removed
    rite cache size                   Removed
    potlight backend		     New			noindex
</onlyinclude>

===============================
CHANGES SINCE 4.12.0rc4
===============================

------------------------

*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14258 BUG #14258]: dsdb: Correctly handle memory in objectclass_attrs.

===============================
CHANGES SINCE 4.12.0rc3
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14269 BUG #14269]: s3: DFS: Don't allow link deletion on a read-only share.
*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14284 BUG #14284]: pidl/wscript: configure should insist on Parse::Yapp::Driver.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14270 BUG #14270]: ldb: Fix search with scope ONE and small result sets. 
* * [https://bugzilla.samba.org/show_bug.cgi?id=14284 BUG #14284]: build: Do not check if system perl modules should be bundled.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14285 BUG #14285]: smbd fails to handle EINTR from open(2) properly.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14270 BUG #14270]: ldb: version 2.1.1.

===============================
CHANGES SINCE 4.12.0rc2
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14282 BUG #14282]: Set getting and setting of MS-DFS redirects on the filesystem to go through two new VFS functions SMB_VFS_CREATE_DFS_PATHAT() and SMB_VFS_READ_DFS_PATHAT().
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14255 BUG #14255]: bootstrap: Remove un-used dependency python3-crypto.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14247 BUG #14247]: Fix CID 1458418 and 1458420.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14281 BUG #14281]: lib: Fix a shutdown crash with "clustering = yes".
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14247 BUG #14247]: Winbind member (source3) fails local SAM auth with empty domain name.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14265 BUG #14265]: winbindd: Handle missing idmap in getgrgid().
* * [https://bugzilla.samba.org/show_bug.cgi?id=14271 BUG #14271]: Don't use forward declaration for GnuTLS typedefs.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14280 BUG #14280]: Add io_uring vfs module.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14250 BUG #14250]: libcli:smb: Improve check for gnutls_aead_cipher_(en|de)cryptv2.

===============================
CHANGES SINCE 4.12.0rc1
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14239 BUG #14239]: s3: lib: nmblib. Clean up and harden nmb packet processing.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14253 BUG #14253]: lib:util: Log mkdir error on correct debug levels.

===============================
KNOWN ISSUES
===============================

------------------------

`Release_Planning_for_Samba_4.12#Release_blocking_bugs`

    ttps://www.samba.org/samba/history/samba-4.12.0.html

----
`Category:Release Notes`