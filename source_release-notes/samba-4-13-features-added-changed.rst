Samba 4.13 Features added/changed
    <namespace>0</namespace>
<last_edited>2021-05-11T17:29:02Z</last_edited>
<last_editor>Fraz</last_editor>

Samba 4.13 is `Samba_Release_Planning#Maintenance_Mode|**Maintenance Mode**`.
Samba 4.13.9
------------------------

Release Notes for Samba 4.13.9
May 11, 2021

===============================
This is the latest stable release of the Samba 4.13 release series.
===============================

------------------------

===============================
Changes since 4.13.8
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14571 BUG #14571]: s3: smbd: SMB1 SMBsplwr doesn't send a reply packet on success.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14689 BUG #14689]: Add documentation for dsdb_group_audit and dsdb_group_json_audit to "log level", synchronise "log level" in smb.conf with the code.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14672 BUG #14672]: Fix smbd panic when two clients open same file.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14675 BUG #14675]: Fix memory leak in the RPC server. 
* * [https://bugzilla.samba.org/show_bug.cgi?id=14679 BUG #14679]: s3: smbd: Fix deferred renames.
*  Samuel Cabrero <scabrero@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14675 BUG #14675]: s3-iremotewinspool: Set the per-request memory context.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14675 BUG #14675]: rpc_server3: Fix a memleak for internal pipes.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11899 BUG #11899]: third_party: Update socket_wrapper to version 1.3.2.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14640 BUG #14640]: third_party: Update socket_wrapper to version 1.3.3.
*  Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14663 BUG #14663]: idmap_rfc2307 and idmap_nss return wrong mapping for uid/gid conflict.
*  Martin Schwenke <martin@meltin.net
* * [https://bugzilla.samba.org/show_bug.cgi?id=14288 BUG #14288] : Fix the build on OmniOS.

   [https://www.samba.org/samba/history/samba-4.13.9.html Release Notes Samba 4.13.9].

Samba 4.13.8
------------------------

* Release Notes for Samba 4.13-8
* April 29, 2021

===============================
This is a security release in order to address the following defect:
===============================

------------------------

* CVE-2021-20254: Negative idmap cache entries can cause incorrect group entries in the Samba file server process token.

===============================
Details
===============================

------------------------

* [https://www.samba.org/samba/security/CVE-2021-20254.html CVE-2021-20254]
* The Samba smbd file server must map Windows group identities (SIDs) into unix group ids (gids). The code that performs this had a flaw that could allow it to read data beyond the end of the array in the case where a negative cache entry had been added to the mapping cache. This could cause the calling code to return those values into the process token that stores the group membership for a user.

* Most commonly this flaw caused the calling code to crash, but an alert user (Peter Eriksson, IT Department, Linköping University) found this flaw by noticing an unprivileged user was able to delete a file within a network share that they should have been disallowed access to.

* Analysis of the code paths has not allowed us to discover a way for a remote user to be able to trigger this flaw reproducibly or on demand, but this CVE has been issued out of an abundance of caution.

===============================
Changes since 4.13.8
===============================

------------------------

*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14571 BUG #14571]: CVE-2021-20254: Fix buffer overrun in sids_to_unixids().

   [https://www.samba.org/samba/history/samba-4.13.8.html Release Notes Samba 4.13.8].

Samba 4.13.7
------------------------

* Release Notes for Samba 4.13.7
* March 24, 2021

===============================
This is a security release in order to address the following defects:
===============================

------------------------

* [https://www.samba.org/samba/security/CVE-2020-27840.html CVE-2020-27840]: Heap corruption via crafted DN strings.
* [https://www.samba.org/samba/security/CVE-2021-20277.html CVE-2021-20277]: Out of bounds read in AD DC LDAP server.

===============================
Details
===============================

------------------------

*  [https://www.samba.org/samba/security/CVE-2020-27840.html CVE-2020-27840]:
* An anonymous attacker can crash the Samba AD DC LDAP server by sending easily crafted DNs as part of a bind request. More serious heap corruption is likely also possible.
*  [https://www.samba.org/samba/security/CVE-2021-20277.html CVE-2021-20277]:
* User-controlled LDAP filter strings against the AD DC LDAP server may crash the LDAP server.

For more details, please refer to the security advisories.

===============================
Changes since 4.12.12
===============================

------------------------

*  Release with dependency on ldb version 2.2.1
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14634 BUG #14634]: [https://www.samba.org/samba/security/CVE-2021-20277.html CVE-2021-20277]: Fix out of bounds read in ldb_handler_fold.
*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14634 BUG #14634]: [https://www.samba.org/samba/security/CVE-2020-27840.html CVE-2020-27840]: Fix unauthenticated remote heap corruption via bad DNs.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14634 BUG #14634]: [https://www.samba.org/samba/security/CVE-2021-20277.html CVE-2021-20277]: Fix out of bounds read in ldb_handler_fold.

   [https://www.samba.org/samba/history/samba-4.13.7.html Release Notes Samba 4.13.7].

Samba 4.13.5
------------------------

* Release Notes for Samba 4.13.5
* March 09, 2021

===============================
This is the latest stable release of the Samba 4.13 release series.
===============================

------------------------

===============================
Changes since 4.13.4
===============================

------------------------

*  Trever L. Adams <trever.adams@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14634 BUG #14634]: s3:modules:vfs_virusfilter: Recent talloc changes cause infinite start-up failure.
*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13992 BUG #13992]: s3: libsmb: Add missing cli_tdis() in error path if encryption setup failed on temp proxy connection.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14604 BUG #14604]: smbd: In conn_force_tdis_done() when forcing a connection closed force a full reload of services.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14593 BUG #14593]: dbcheck: Check Deleted Objects and reduce noise in reports about expired tombstones.
*  Ralph Boehme <slow@samba.org
* * [https://bugzilla.samba.org/show_bug.cgi?id=14503 BUG #14503]: s3: Fix fcntl waf configure check.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14602 BUG #14602]: s3/auth: Implement "winbind:ignore domains".
* * [https://bugzilla.samba.org/show_bug.cgi?id=14617 BUG #14617]: smbd: Use fsp->conn->session_info for the initial delete-on-close token.
*  Peter Eriksson <pen@lysator.liu.se>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14648 BUG #14648]: s3: VFS: nfs4_acls. Add missing TALLOC_FREE(frame) in error path.
*  Björn Jacke <bj@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14624 BUG #14624]: classicupgrade: Treat old never expires value right.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14636 BUG #14636]: g_lock: Fix uninitalized variable reads.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13898 BUG #13898]: s3:pysmbd: Fix fd leak in py_smbd_create_file().
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14625 BUG #14625]: lib:util: Avoid free'ing our own pointer.
*  Paul Wise <pabs3@bonedaddy.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12505 BUG #12505]: HEIMDAL: krb5_storage_free(NULL) should work.

   [https://www.samba.org/samba/history/samba-4.13.5.html Release Notes Samba 4.13.5].

Samba 4.13.4
------------------------

* Release Notes for Samba 4.13.4
* January 26, 2021

===============================
This is the latest stable release of the Samba 4.13 release series.
===============================

------------------------

===============================
Changes since 4.13.3
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14607 BUG #14607]: Work around special SMB2 IOCTL response behavior of NetApp Ontap 7.3.7.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14210 BUG #14612]: Temporary DFS share setup doesn't set case parameters in the same way as a regular share definition does.
*  Dimitry Andric <dimitry@andric.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14605 BUG #14605]: lib: Avoid declaring zero-length VLAs in various messaging functions.
*  Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=14579 BUG #14579]: Do not create an empty DB when accessing a sam.ldb.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14596 BUG #14596]: vfs_fruit may close wrong backend fd.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14612 BUG #14612]: Temporary DFS share setup doesn't set case parameters in the same way as a regular share definition does.
*  Arne Kreddig <arne@kreddig.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14606 BUG #14606]: vfs_virusfilter: Allocate separate memory for config char*.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14596 BUG #14596]: vfs_fruit may close wrong backend fd.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14607 BUG #14607]: Work around special SMB2 IOCTL response behavior of NetApp Ontap 7.3.7.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14601 BUG #14601]: The cache directory for the user gencache should be created recursively.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14594 BUG #14594]: Be more flexible with repository names in CentOS 8 test environments.

    https://www.samba.org/samba/history/samba-4.13.4.html Release Notes Samba 4.13.4].

Samba 4.13.3
------------------------

* Release Notes for Samba 4.13.3
* December 15, 2020

===============================
This is the latest stable release of the Samba 4.13 release series.
===============================

------------------------

===============================
Changes since 4.13.2
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14210 BUG #14210]: libcli: smb2: Never print length if smb2_signing_key_valid() fails for crypto blob.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14486 BUG #14486]: s3: modules: gluster. Fix the error I made in preventing talloc leaks from a function. 
* * [https://bugzilla.samba.org/show_bug.cgi?id=14515 BUG #14515]: s3: smbd: Don't overwrite contents of fsp->aio_requests[0] with NULL via TALLOC_FREE().
* * [https://bugzilla.samba.org/show_bug.cgi?id=14568 BUG #14568]: s3: spoolss: Make parameters in call to user_ok_token() match all other uses.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14590 BUG #14590]: s3: smbd: Quiet log messages from usershares for an unknown share.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14248 BUG #14248]: samba process does not honor max log size.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14587 BUG #14587]: vfs_zfsacl: Add missing inherited flag on hidden "magic" everyone@ ACE.
*  Isaac Boukris <iboukris@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13124 BUG #13124]: s3-libads: Pass timeout to open_socket_out in ms.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14537 BUG #14486]: s3-vfs_glusterfs: Always disable write-behind translator.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14517 BUG #14517]: smbclient: Fix recursive mget.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14581 BUG #14581]: clitar: Use do_list()'s recursion in clitar.c.
*  Anoop C S <anoopcs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14486 BUG #14486]: manpages/vfs_glusterfs: Mention silent skipping of write-behind translator.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14573 BUG #14573]: vfs_shadow_copy2: Preserve all open flags assuming ROFS.
*  Jones Syue <jonessyue@qnap.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14514 BUG #14514]: interface: Fix if_index is not parsed correctly.

    https://www.samba.org/samba/history/samba-4.13.3.html Release Notes Samba 4.13.3].

Samba 4.13.2
------------------------

* Release Notes for Samba 4.13.2
* November 03, 2020

===============================
This is the latest stable release of the Samba 4.13 release series.
===============================

------------------------

===============================
Major enhancements include:
===============================

------------------------

* [https://bugzilla.samba.org/show_bug.cgi?id=14537 BUG #14537]: ctdb-common: Avoid aliasing errors during code optimization.
* [https://bugzilla.samba.org/show_bug.cgi?id=14486 BUG #14486]: vfs_glusterfs: Avoid data corruption with the write-behind translator.

===============================
Details
===============================

------------------------

The GlusterFS write-behind performance translator, when used with Samba, could be a source of data corruption. The translator, while processing a write call, immediately returns success but continues writing the data to the server in the background. This can cause data corruption when two clients relying on Samba to provide data consistency are operating on the same file.

The write-behind translator is enabled by default on GlusterFS. The vfs_glusterfs plugin will check for the presence of the translator and refuse to connect if detected. Please disable the write-behind translator for the GlusterFS volume to allow the plugin to connect to the volume.

===============================
Changes since 4.13.1
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14486 BUG #14486]: s3: modules: vfs_glusterfs: Fix leak of char **lines onto mem_ctx on return.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14471 BUG #14471]: RN: vfs_zfsacl: Only grant DELETE_CHILD if ACL tag is special.
*  Alexander Bokovoy <ab@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14538 BUG #14538]: smb.conf.5: Add clarification how configuration changes reflected by Samba.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14552 BUG #14552]: daemons: Report status to systemd even when running in foreground.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14553 BUG #14553]: DNS Resolver: Support both dnspython before and after 2.0.0.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14486 BUG #14486]: s3-vfs_glusterfs: Refuse connection when write-behind xlator is present.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14487 BUG #14487]: provision: Add support for BIND 9.16.x.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14537 BUG #14537]: ctdb-common: Avoid aliasing errors during code optimization.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14541 BUG #14541]: libndr: Avoid assigning duplicate versions to symbols.
*  Björn Jacke <bjacke@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14522 BUG #14522]: docs: Fix default value of spoolss:architecture.
*  Laurent Menase <laurent.menase@hpe.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14388 BUG #14388]: winbind: Fix a memleak.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14531 BUG #14531]: s4:dsdb:acl_read: Implement "List Object" mode feature.
*  Sachin Prabhu <sprabhu@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14486 BUG #14486]: docs-xml/manpages: Add warning about write-behind translator for vfs_glusterfs.
*  Khem Raj <raj.khem@gmail.com>
* * nsswitch/nsstest.c: Avoid nss function conflicts with glibc nss.h.
*  Anoop C S <anoopcs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14530 BUG #14530]: vfs_shadow_copy2: Avoid closing snapsdir twice.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14547 BUG #14547]: third_party: Update resolv_wrapper to version 1.1.7.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14550 BUG #14550]: examples:auth: Do not install example plugin.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14513 BUG #14513]: ctdb-recoverd: Drop unnecessary and broken code.
*  Andrew Walker <awalker@ixsystems.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14471 BUG #14471]: RN: vfs_zfsacl: Only grant DELETE_CHILD if ACL tag is special.

 [https://www.samba.org/samba/history/samba-4.13.2.html Release Notes Samba 4.13.2].

Samba 4.13.1
------------------------

* Release Notes for Samba 4.13.1
* October 29, 2020

===============================
This is a security release in order to address the following defects:
===============================

------------------------

* [https://www.samba.org/samba/security/CVE-2020-14318.html CVE-2020-14318]: Missing handle permissions check in SMB1/2/3 ChangeNotify.
* [https://www.samba.org/samba/security/CVE-2020-14323.html CVE-2020-14323]: Unprivileged user can crash winbind.
* [https://www.samba.org/samba/security/CVE-2020-14383.html CVE-2020-14383]: An authenticated user can crash the DCE/RPC DNS with easily crafted records.

===============================
Details
===============================

------------------------

* [https://www.samba.org/samba/security/CVE-2020-14318.html CVE-2020-14318]:
* The SMB1/2/3 protocols have a concept of "ChangeNotify", where a client can request file name notification on a directory handle when a condition such as "new file creation" or "file size change" or "file timestamp update" occurs.

* A missing permissions check on a directory handle requesting ChangeNotify meant that a client with a directory handle open only for FILE_READ_ATTRIBUTES (minimal access rights) could be used to obtain change notify replies from the server. These replies contain information that should not be available to directory handles open for FILE_READ_ATTRIBUTE only.

* [https://www.samba.org/samba/security/CVE-2020-14323.html CVE-2020-14323]:
* winbind in version 3.6 and later implements a request to translate multiple Windows SIDs into names in one request. This was done for performance reasons: The Microsoft RPC call domain controllers offer to do this translation, so it was an obvious extension to also offer this batch operation on the winbind unix domain stream socket that is available to local processes on the Samba server.

* Due to improper input validation a hand-crafted packet can make winbind perform a NULL pointer dereference and thus crash.

* [https://www.samba.org/samba/security/CVE-2020-14383.html CVE-2020-14383]:
* Some DNS records (such as MX and NS records) usually contain data in the additional section. Samba's dnsserver RPC pipe (which is an administrative interface not used in the DNS server itself) made an error in handling the case where there are no records present: instead of noticing the lack of records, it dereferenced uninitialised memory, causing the RPC server to crash. This RPC server, which also serves protocols other than dnsserver, will be restarted after a short delay, but it is easy for an authenticated non-admin attacker to crash it again as soon as it returns. The Samba DNS server itself will continue to operate, but many RPC services will not.

For more details, please refer to the security advisories.
* `CVE-2020-14318`
* `CVE-2020-14323`
* `CVE-2020-14383`

===============================
Changes since 4.13
===============================

------------------------

o  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14434 BUG #14434]: [https://www.samba.org/samba/security/CVE-2020-14318.html CVE-2020-14318]: s3: smbd: Ensure change notifies can't get set unless the directory handle is open for SEC_DIR_LIST.
*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12795 BUG #12795]: [https://www.samba.org/samba/security/CVE-2020-14383.html CVE-2020-14383]: Remote crash after adding NS or MX records using 'samba-tool'.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14472 BUG #14472]: [https://www.samba.org/samba/security/CVE-2020-14383.html CVE-2020-14383]: Remote crash after adding MX records.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14436 BUG #14436]: [https://www.samba.org/samba/security/CVE-2020-14323.html CVE-2020-14323]: winbind: Fix invalid lookupsids DoS.

 [https://www.samba.org/samba/history/samba-4.13.1.html Release Notes Samba 4.13.1].

Samba 4.13
------------------------

<onlyinclude>
* Release Notes for Samba 4.13
* September 22, 2020

===============================
Release Announcements
===============================

------------------------

This is the first stable release of the Samba 4.13 release series. Please read the release notes carefully before upgrading.

===============================
ZeroLogon
===============================

------------------------

Please avoid to set "server schannel = no" and "server schannel= auto" on all Samba domain controllers due to the wellknown ZeroLogon issue.

For details please see
* ` CVE-2020-1472`

===============================
NEW FEATURES/CHANGES
===============================

------------------------

Python 3.6 or later required=
===============================

------------------------

Samba's minimum runtime requirement for python was raised to Python 3.5 with samba 4.12.  Samba 4.13 raises this minimum version to Python 3.6 both to access new features and because this is the oldest version we test with in our CI infrastructure.

This is also the last release where it will be possible to build Samba (just the file server) with Python versions 2.6 and 2.7.

As Python 2.7 has been End Of Life upstream since April 2020, Samba is dropping ALL Python 2.x support in the NEXT release.

Samba 4.14 to be released in March 2021 will require Python 3.6 or later to build.

wide links functionality=
===============================

------------------------

For this release, the code implementing the insecure "wide links = yes" functionality has been moved out of the core smbd code and into a separate VFS module, vfs_widelinks. Currently this vfs module is implicitly loaded by smbd as the last but one module before vfs_default if "wide links = yes" is enabled on the share (note, the existing restrictions on enabling wide links around the SMB1 "unix extensions" and the "allow insecure wide links" parameters are still in force). The implicit loading was done to allow existing users of "wide links = yes" to keep this functionality without having to make a change to existing working smb.conf files.

Please note that the Samba developers recommend changing any Samba installations that currently use "wide links = yes" to use bind mounts as soon as possible, as "wide links = yes" is an inherently insecure configuration which we would like to remove from Samba. Moving the feature into a VFS module allows this to be done in a cleaner way in future.

A future release to be determined will remove this implicit linkage, causing administrators who need this functionality to have to explicitly add the vfs_widelinks module into the "vfs objects =" parameter lists. The release notes will be updated to note this change when it occurs.

NT4-like 'classic' Samba domain controllers=
===============================

------------------------

Samba 4.13 deprecates Samba's original domain controller mode.

Sites using Samba as a Domain Controller should upgrade from the NT4-like 'classic' Domain Controller to a Samba Active Directory DC to ensure full operation with modern windows clients.

SMBv1 only protocol options deprecated=
===============================

------------------------

A number of smb.conf parameters for less-secure authentication methods which are only possible over SMBv1 are deprecated in this release.

REMOVED FEATURES=
===============================

------------------------

The deprecated "ldap ssl ads" smb.conf option has been removed.

===============================
smb.conf changes
===============================

------------------------

    arameter Name                     Description                Default
    -------------                     -----------                -------
    dap ssl ads                       removed
    mb2 disable lock sequence checking				No
    omain logons                      Deprecated                 no
    aw NTLMv2 auth                    Deprecated                 no
    lient plaintext auth              Deprecated                 no
    lient NTLMv2 auth                 Deprecated                 yes
    lient lanman auth                 Deprecated                 no
    lient use spnego                  Deprecated                 yes
    erver schannel                    To be removed in 4.13.0
    erver require schannel:COMPUTER   Added
</onlyinclude>

===============================
CHANGES SINCE 4.13.0rc5
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14497 BUG #14497]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-1472 CVE-2020-1472](ZeroLogon): s3:rpc_server/netlogon: Protect netr_ServerPasswordSet2 against unencrypted passwords.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14497 BUG #14497]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-1472 CVE-2020-1472](ZeroLogon): s3:rpc_server/netlogon: Support "server require schannel:WORKSTATION$ = no" about unsecure configurations.
*  Gary Lockyer <gary@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14497 BUG #14497]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-1472 CVE-2020-1472](ZeroLogon): s4 torture rpc: repeated bytes in client challenge.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14497 BUG #14497]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-1472 CVE-2020-1472](ZeroLogon): libcli/auth: Reject weak client challenges in netlogon_creds_server_init() "server require schannel:WORKSTATION$ = no".
===============================
CHANGES SINCE 4.13.0rc4
===============================

------------------------

*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14399 BUG #14399]: waf: Only use gnutls_aead_cipher_encryptv2() for GnuTLS > 3.6.14.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14467 BUG #14467]: s3:smbd: Fix %U substitutions if it contains a domain name.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14479 BUG #14479]: The created krb5.conf for 'net ads join' doesn't have a domain entry.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14482 BUG #14482]: Fix build problem if libbsd-dev is not installed.

===============================
CHANGES SINCE 4.13.0rc3
===============================

------------------------

*  David Disseldorp <ddiss at samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14437 BUG #14437]: build: Toggle vfs_snapper using "--with-shared-modules".
*  Volker Lendecke <vl at samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14465 BUG #14465]: idmap_ad does not deal properly with a RFC4511 section 4.4.1 response.
*  Stefan Metzmacher <metze at samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14428 BUG #14428]: PANIC: Assert failed in get_lease_type().
* * [https://bugzilla.samba.org/show_bug.cgi?id=14465 BUG #14465]: idmap_ad does not deal properly with a RFC4511 section 4.4.1

===============================
CHANGES SINCE 4.13.0rc2
===============================

------------------------

*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14460 BUG #14460]: Deprecate domain logons, SMBv1 things.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14318 BUG #14318]: docs: Add missing winexe manpage.
*  Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14166 BUG #14166]: util: Allow symlinks in directory_create_or_exist.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14466 BUG #14466]: ctdb disable/enable can fail due to race condition.

===============================
CHANGES SINCE 4.13.0rc1
===============================

------------------------

*  Andrew Bartlett <abartlet at samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14450 BUG #14450]: dbcheck: Allow a dangling forward link outside our known NCs.
*  Isaac Boukris <iboukris at gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14462 BUG #14462]: Remove deprecated "ldap ssl ads" smb.conf option.
*  Volker Lendecke <vl at samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14435 BUG #14435]: winbind: Fix lookuprids cache problem.
*  Stefan Metzmacher <metze at samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14354 BUG #14354]: kdc:db-glue: Ignore KRB5_PROG_ETYPE_NOSUPP also for Primary:Kerberos.
*  Andreas Schneider <asn at samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14358 BUG #14358]: docs: Fix documentation for require_membership_of of pam_winbind.conf.
*  Martin Schwenke <martin at meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14444 BUG #14444]: ctdb-scripts: Use nfsconf as a last resort get nfsd thread count.

===============================
KNOWN ISSUES
===============================

------------------------

`Release_Planning_for_Samba_4.13#Release_blocking_bugs`

    https://www.samba.org/samba/history/samba-4.13.0.html Release Notes Samba 4.13.0].

----
`Category:Release Notes`