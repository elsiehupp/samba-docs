Samba 4.10 Features added/changed
    <namespace>0</namespace>
<last_edited>2020-09-22T20:37:38Z</last_edited>
<last_editor>Fraz</last_editor>

Samba 4.10 is `Samba_Release_Planning#Discontinued_.28End_of_Life.29|**Discontinued (End of Life)**`.

Samba 4.10.18
------------------------

* Release Notes for Samba 4.10.18
* September 18, 2020

===============================
This is a **security release** in order to address the following defect:
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-1472 CVE-2020-1472]: Unauthenticated domain takeover via netlogon ("ZeroLogon").

The following applies to Samba used as domain controller only (most seriously the Active Directory DC, but also the classic/NT4-style DC).

Installations running Samba as a file server only are not directly affected by this flaw, though they may need configuration changes to continue to talk to domain controllers (see "file servers and domain members" below).

The netlogon protocol contains a flaw that allows an authentication bypass. This was reported and patched by Microsoft as [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-1472 CVE-2020-1472]. Since the bug is a protocol level flaw, and Samba implements the protocol, Samba is also vulnerable.

However, since version 4.8 (released in March 2018), the default behaviour of Samba has been to insist on a secure netlogon channel, which is a sufficient fix against the known exploits. This default is equivalent to having 'server schannel = yes' in the smb.conf.

Therefore versions 4.8 and above are not vulnerable unless they have the smb.conf lines 
 'server schannel = no' 
or 
 'server schannel = auto'.

Samba versions 4.7 and below are vulnerable unless they have 
 'server schannel = yes' 
in the smb.conf.

* Note: each domain controller needs the correct settings in its smb.conf.

Vendors supporting Samba 4.7 and below are advised to patch their installations and packages to add this line to the [global] section if their smb.conf file.

The 'server schannel = yes' smb.conf line is equivalent to Microsoft's 'FullSecureChannelProtection=1' registry key, the introduction of which we understand forms the core of Microsoft's fix.

Some domains employ third-party software that will not work with a 'server schannel = yes'. For these cases patches are available that allow specific machines to use insecure netlogon. For example, the following smb.conf:

   server schannel = yes
   server require schannel:triceratops$ = no
   server require schannel:greywacke$ = no

will allow only "triceratops$" and "greywacke$" to avoid schannel.

More details can be found here:
* ` CVE-2020-1472`

===============================
Changes since 4.10.17
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14497 BUG #14497]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-1472 CVE-2020-1472](ZeroLogon): s3:rpc_server/netlogon: Protect netr_ServerPasswordSet2 against unencrypted passwords.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14497 BUG #14497]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-1472 CVE-2020-1472](ZeroLogon): s3:rpc_server/netlogon: Support "server require schannel:WORKSTATION$ = no" about unsecure configurations.
*  Gary Lockyer <gary@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14497 BUG #14497]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-1472 CVE-2020-1472](ZeroLogon): s4 torture rpc: repeated bytes in client challenge.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14497 BUG #14497]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-1472 CVE-2020-1472](ZeroLogon): libcli/auth: Reject weak client challenges in netlogon_creds_server_init() "server require schannel:WORKSTATION$ = no".

 [https://www.samba.org/samba/history/samba-4.10.18.html Release Notes Samba 4.10.18].

Samba 4.10.17
------------------------

* Release Notes for Samba 4.10.17
* July 02, 2020

===============================
This is a **security release** in order to address the following defects:
===============================

------------------------

* [https://www.samba.org/samba/security/CVE-2020-10730.html CVE-2020-10730]: NULL pointer de-reference and use-after-free in Samba AD DC LDAP Server with ASQ, VLV and paged_results.
* [https://www.samba.org/samba/security/CVE-2020-10745.html CVE-2020-10745]: Parsing and packing of NBT and DNS packets can consume excessive CPU
* [https://www.samba.org/samba/security/CVE-2020-10760.html CVE-2020-10760]: LDAP Use-after-free in Samba AD DC Global Catalog with  paged_results and VLV.
* [https://www.samba.org/samba/security/CVE-2020-14303.html CVE-2020-14303]: Empty UDP packet DoS in Samba AD DC nbtd.

===============================
Details
===============================

------------------------

* [https://www.samba.org/samba/security/CVE-2020-10730.html CVE-2020-10730]:
* A client combining the 'ASQ' and 'VLV' LDAP controls can cause a NULL pointer de-reference and further combinations with the LDAP paged_results feature can give a use-after-free in Samba's AD DC LDAP server.
* [https://www.samba.org/samba/security/CVE-2020-10745.html CVE-2020-10745]:
* Parsing and packing of NBT and DNS packets can consume excessive CPU.
*  [https://www.samba.org/samba/security/CVE-2020-10760.html CVE-2020-10760]:
* The use of the paged_results or VLV controls against the Global Catalog LDAP server on the AD DC will cause a use-after-free.
*  [https://www.samba.org/samba/security/CVE-2020-14303.html CVE-2020-14303]:
* The AD DC NBT server in Samba 4.0 will enter a CPU spin and not process further requests once it receives an empty (zero-length) UDP packet to port 137.

For more details, please refer to the security advisories.

===============================
Changes since 4.10.16
===============================

------------------------

* Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14378 BUG #14378]: [https://www.samba.org/samba/security/CVE-2020-10745.html CVE-2020-10745]: Invalid DNS or NBT queries containing dots use several seconds of CPU each.
* Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14364 BUG #14364]: [https://www.samba.org/samba/security/CVE-2020-10730.html CVE-2020-10730]: NULL de-reference in AD DC LDAP server when ASQ and VLV combined.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14402 BUG #14402]: [https://www.samba.org/samba/security/CVE-2020-10760.html CVE-2020-10760]: Fix use-after-free in AD DC Global Catalog LDAP server with paged_result or VLV.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14417 BUG #14417]: [https://www.samba.org/samba/security/CVE-2020-14303.html CVE-2020-14303]: Fix endless loop from empty UDP packet sent to AD DC nbt_server.
*  Gary Lockyer <gary@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14364 BUG #14364]: [https://www.samba.org/samba/security/CVE-2020-10730.html CVE-2020-10730]: NULL de-reference in AD DC LDAP server when ASQ and VLV combined, ldb: Bump version to 1.5.8.

    https://www.samba.org/samba/history/samba-4.10.17.html Release Notes Samba 4.10.17].

Samba 4.10.16
------------------------

* Release Notes for Samba 4.10.16
* May 25, 2020

===============================
This is the last stable release of the Samba 4.10 release series.
===============================

------------------------

===============================
Changes since 4.10.15
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * s3: lib: Paranoia around use of snprintf copying into a fixed-size buffer from a getenv() pointer.
*  Amit Kumar <amitkuma@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14345 BUG #14345]: lib:util: Fix smbclient -l basename dir.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14366 BUG #14366]: Malicous SMB1 server can crash libsmbclient.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14336 BUG #14336]: s3:libads: Fix ads_get_upn().
* * [https://bugzilla.samba.org/show_bug.cgi?id=14358 BUG #14358]: docs-xml: Fix usernames in pam_winbind manpages.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14370 BUG #14370]: Client tools are not able to read gencache anymore since 4.10.

 [https://www.samba.org/samba/history/samba-4.10.16.html Release Notes Samba 4.10.16].

Samba 4.10.15
------------------------

* Release Notes for Samba 4.10.15
* April 28, 2020

===============================
This is a **Security Release** in order to address the following defects:
===============================

------------------------

* [https://www.samba.org/samba/security/CVE-2020-10700.html CVE-2020-10700] Use-after-free in Samba AD DC LDAP Server with ASQ.
* [https://www.samba.org/samba/security/CVE-2020-10704.html CVE-2020-10704] LDAP Denial of Service (stack overflow) in Samba AD DC.

===============================
Details
===============================

------------------------

[https://www.samba.org/samba/security/CVE-2020-10700.html CVE-2020-10700]
* A client combining the 'ASQ' and 'Paged Results' LDAP controls can cause a use-after-free in Samba's AD DC LDAP server.
[https://www.samba.org/samba/security/CVE-2020-10704.html CVE-2020-10704]
* A deeply nested filter in an un-authenticated LDAP search can exhaust the LDAP server's stack memory causing a SIGSEGV.

For more details, please refer to the security advisories.

===============================
Changes since 4.10.14
===============================

------------------------

* Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14331 BUG #14331]: [https://www.samba.org/samba/security/CVE-2020-10700.html CVE-2020-10700]: Fix use-after-free in AD DC LDAP server when ASQ and paged_results combined.

* Gary Lockyer <gary@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=20454 BUG #20454]: [https://www.samba.org/samba/security/CVE-2020-10704.html CVE-2020-10704]: Fix LDAP Denial of Service (stack overflow) in Samba AD DC.

 [https://www.samba.org/samba/history/samba-4.10.15.html Release Notes Samba 4.10.15].

Samba 4.10.14
------------------------

* Release Notes for Samba 4.10.14
* March 26, 2020

===============================
This is the last bugfix release of the Samba 4.10 release series.
===============================

------------------------

There will be security releases only beyond this point.

===============================
Changes since 4.10.13
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14239 BUG #14239]: s3: lib: nmblib. Clean up and harden nmb packet processing.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14283 BUG #14283]: s3: VFS: full_audit. Use system session_info if called from a temporary share definition.
*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=20193 BUG #20193]: nmblib: Avoid undefined behaviour in handle_name_ptrs().
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14258 BUG #14258]: dsdb: Correctly handle memory in objectclass_attrs.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14247 BUG #14247]: auth: Fix CID 1458418 Null pointer dereferences (REVERSE_INULL), auth: Fix CID 1458420 Null pointer dereferences (REVERSE_INULL).
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14247 BUG #14247]: winbind member (source3) fails local SAM auth with empty domain name.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14265 BUG #14265]: winbindd: Handling missing idmap in getgrgid().
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14253 BUG #14253]: lib:util: Log mkdir error on correct debug levels.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14266 BUG #14266]: wafsamba: Do not use 'rU' as the 'U' is deprecated in Python 3.9.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14274 BUG #14274]: ctdb-tcp: Make error handling for outbound connection consistent.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14295 BUG #14295]: Starting ctdb node that was powered off hard before results in recovery loop.

 https://www.samba.org/samba/history/samba-4.10.14.html

Samba 4.10.13
------------------------

* Release Notes for Samba 4.10.13
* January 23, 2020

===============================
This is the latest stable release of the Samba 4.10 release series.
===============================

------------------------

===============================
Changes since 4.10.12
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14161 BUG #14161]: s3: libsmb: Ensure SMB1 cli_qpathinfo2() doesn't return an inode number.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14174 BUG #14174]: s3: utils: smbtree. Ensure we don't call cli_RNetShareEnum() on an SMB1 connection.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14176 BUG #14176]: s3: libsmb: Ensure return from net_share_enum_rpc() sets cli->raw_status on error.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14189 BUG #14189]: s3: smbd: SMB2 - Ensure we use the correct session_id if encrypting an interim response.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14205 BUG #14205]: s3: smbd: Only set xconn->smb1.negprot.done = true after supported_protocols[protocol].proto_reply_fn() succeeds.
*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14209 BUG #14209]: pygpo: Use correct method flags.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13925 BUG #13925]: s3: Remove now unneeded call to cmdline_messaging_context().
* * [https://bugzilla.samba.org/show_bug.cgi?id=14069 BUG #14069]: Incomplete conversion of former parametric options.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14070 BUG #14070]: Fix sync dosmode fallback in async dosmode codepath.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14171 BUG #14171]: vfs_fruit returns capped resource fork length.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13745 BUG #13745]: s3:printing: Fix %J substition.
*  Isaac Boukris <iboukris@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14116 BUG #14116]: libnet_join: Add SPNs for additional-dns-hostnames entries.
*  Torsten Fohrer <torsten.fohrer@sbe.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14209 BUG #14209]: Avoiding bad call flags with python 3.8, using METH_NOARGS instead of zero.
*  Björn Jacke <bjacke@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14122 BUG #14122]: docs-xml/winbindnssinfo: Clarify interaction with idmap_ad etc.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14175 BUG #14175]: ctdb-tcp: Close inflight connecting TCP sockets after fork.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14153 BUG #14153]: s4:dirsync: Fix interaction of dirsync and extended_dn controls.
*  Gary Lockyer <gary@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14199 BUG #14199]: upgradedns: Ensure lmdb lock files linked.
*  Anoop C S <anoopcs@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14182 BUG #14182]: s3: VFS: glusterfs: Reset nlinks for symlink entries during readdir.
*  Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14140 BUG #14140]: wscript: Remove checks for shm_open and shmget.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14101 BUG #14101]: libsmbclient: smbc_stat() doesn't return the correct st_mode and also the uid/gid is not filled (SMBv1).
* * [https://bugzilla.samba.org/show_bug.cgi?id=14168 BUG #14168]: replace: Only link libnsl and libsocket if required.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14219 BUG #14219]: librpc: Fix string length checking in ndr_pull_charset_to_null().
*  Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13856 BUG #13856]: heimdal-build: Avoid hard-coded /usr/include/heimdal in asn1_compile-generated code.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14175 BUG #14175]: ctdb-tcp: Drop tracking of file descriptor for incoming connections.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14227 BUG #14227]: ctdb-scripts: Strip square brackets when gathering connection info.

 https://www.samba.org/samba/history/samba-4.10.13.html

Samba 4.10.12
------------------------

* Release Notes for Samba 4.10.12
* January 21, 2020

===============================
This is a security release in order to address the following defects:
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14902 CVE-2019-14902]: Replication of ACLs set to inherit down a subtree on AD Directory not automatic.
* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14907 CVE-2019-14907]: Crash after failed character conversion at log level 3 or above.
* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-19344 CVE-2019-19344]: Use after free during DNS zone scavenging in Samba AD DC.
===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14902 CVE-2019-14902]:
* The implementation of ACL inheritance in the Samba AD DC was not complete, and so absent a 'full-sync' replication, ACLs could get out of sync between domain controllers. 
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14907 CVE-2019-14907]:
* When processing untrusted string input Samba can read past the end of the allocated buffer when printing a "Conversion error" message to the logs.
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-19344 CVE-2019-19344]:                                                                                
* During DNS zone scavenging (of expired dynamic entries) there is a read of memory after it has been freed.

For more details and workarounds, please refer to the security advisories.

===============================
Changes since 4.10.11:
===============================

------------------------

*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12497 BUG #12497]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14902 CVE-2019-14902]: Replication of ACLs down subtree on AD Directory not automatic.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14208 BUG #14208]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14907 CVE-2019-14907]: lib/util: Do not print the failed to convert string into the logs.
*  Gary Lockyer <gary@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14050 BUG #14050]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-19344 CVE-2019-19344]: kcc dns scavenging: Fix use after free in dns_tombstone_records_zone.

 https://www.samba.org/samba/history/samba-4.10.12.html

Samba Samba 4.10.11
------------------------

* Release Notes for Samba Samba 4.10.11
* December 10, 2019

===============================
This is a security release in order to address the following defects:
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14861 CVE-2019-14861]: Samba AD DC zone-named record Denial of Service in DNS management server (dnsserver).
* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14870 CVE-2019-14870]: DelegationNotAllowed not being enforced in protocol transition on Samba AD DC.

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14861 CVE-2019-14861]:
* An authenticated user can crash the DCE/RPC DNS management server by creating records with matching the zone name.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14870 CVE-2019-14870]:
* The DelegationNotAllowed Kerberos feature restriction was not being applied when processing protocol transition requests (S4U2Self), in the AD DC KDC.

For more details and workarounds, please refer to the security advisories.

===============================
Changes since Samba 4.10.10:
===============================

------------------------

*Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14138 BUG #14138]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14861 CVE-2019-14861]: Fix DNSServer RPC server crash.
*  Isaac Boukris <iboukris@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14187 BUG #14187]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14870 CVE-2019-14870]: DelegationNotAllowed not being enforced.

 https://www.samba.org/samba/history/samba-4.10.11.html

Samba 4.10.10
------------------------

* Release Notes for Samba 4.10.10
* October 29, 2019

===============================
This is a security release in order to address the following defects:
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-10218 CVE-2019-10218]: Client code can return filenames containing path separators.
* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14833 CVE-2019-14833]: Samba AD DC check password script does not receive the full password.
* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14847 CVE-2019-14847]: User with "get changes" permission can crash AD DC LDAP server via dirsync.

===============================
Details
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-10218 CVE-2019-10218]:
* Malicious servers can cause Samba client code to return filenames containing path separators to calling code.
* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14833 CVE-2019-14833]:
* When the password contains multi-byte (non-ASCII) characters, the check password script does not receive the full password string.

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14847 CVE-2019-14847]:
* Users with the "get changes" extended access right can crash the AD DC LDAP server by requesting an attribute using the range= syntax.

For more details and workarounds, please refer to the security advisories.

===============================
Changes since 4.10.9:
===============================

------------------------

* Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14007 BUG #14071]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-10197 CVE-2019-10197]CVE-2019-10218 - s3: libsmb: Protect SMB1 and SMB2 client code from evil server returned names.

*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12438 BUG #12438]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14833 CVE-2019-14833]: Use utf8 characters in the unacceptable password.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14040 BUG #14040]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14847 CVE-2019-14847] dsdb: Correct behaviour of ranged_results when combined with dirsync.
*  Björn Baumbach <bb@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12438 BUG #12438]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14833 CVE-2019-14833] dsdb: Send full password to check password script.

 https://www.samba.org/samba/history/samba-4.10.10.html

Samba 4.10.9
------------------------

* Release Notes for Samba 4.10.9
* October 17, 2019

===============================
Changes since 4.10.8:
===============================

------------------------

*  Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13972 BUG #13972]: Different Device Id for GlusterFS FUSE mount is causing data loss in CTDB cluster.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14141 BUG #14141]: winbind: Provide passwd struct for group sid with ID_TYPE_BOTH mapping (again).
*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14094 BUG #14094]: smbc_readdirplus() is incompatible with smbc_telldir() and smbc_lseekdir().
* * [https://bugzilla.samba.org/show_bug.cgi?id=14152 BUG #14152]: s3: smbclient: Stop an SMB2-connection from blundering into SMB1-specific calls.
*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13978 BUG #13978]: s4/scripting: MORE py3 compatible print functions.
*  Andrew Bartlett <abartlet@samba.org>
* * ldb: Release ldb 1.5.6
* * [https://bugzilla.samba.org/show_bug.cgi?id=13978 BUG #13978]: undoduididx: Add "or later" to warning about using tools from Samba 4.8.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13959 BUG #13959]: ldb_tdb fails to check error return when parsing pack formats.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14038 BUG #14038]: ctdb: Fix compilation on systems with glibc robust mutexes.
*  Isaac Boukris <iboukris@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11362 BUG #11362]: GPO security filtering based on the groups in Kerberos PAC (but primary group is missing).
* * [https://bugzilla.samba.org/show_bug.cgi?id=14106 BUG #14106]: Fix spnego fallback from kerberos to ntlmssp in smbd server.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14130 BUG #14130]: s3-winbindd: fix forest trusts with additional trust attributes.
*  Poornima G <pgurusid@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14098 BUG #14098]: vfs_glusterfs: Use pthreadpool for scheduling aio operations.
*  Aaron Haslett <aaronhaslett@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13977 BUG #13977]: ldb: baseinfo pack format check on init.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13978 BUG #13978]: ldb: ldbdump key and pack format version comments.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14140 BUG #14140]: Overlinking libreplace against librt and pthread against every binary or library causes issues.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14147 BUG #14147]: ctdb-vacuum: Process all records not deleted on a remote node.
*  Björn Jacke <bj@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14136 BUG #14136]: classicupgrade: Fix uncaught exception.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14139 BUG #14139]: fault.c: Improve fault_report message text pointing to our wiki.
*  Bryan Mason <bmason@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14128 BUG #14128]: s3:client:Use DEVICE_URI, instead of argv[0],for Device URI.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14055 BUG #14055]: We should send SMB2_NETNAME_NEGOTIATE_CONTEXT_ID negotiation context.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14124 BUG #14124]: 'pam_winbind' with 'krb5_auth' or 'wbinfo -K' doesn't work for users of trusted domains/forests principals" logic.
*  Anoop C S <anoopcs@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14093 BUG #14093]: vfs_glusterfs: Enable profiling for file system operations.
*  Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14032 BUG #14032]: vfs_gpfs: Implement special case for denying owner access to ACL.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13884 BUG #13884]: Joining Active Directory should not use SAMR to set the password.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14106 BUG #14106]: s3:libsmb: Do not check the SPNEGO neg token for KRB5.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14140 BUG #14140]: Overlinking libreplace against librt and pthread against every binary or library causes issues.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14155 BUG #14155]: 'kpasswd' fails when built with MIT Kerberos.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14084 BUG #14084]: CTDB replies can be lost before nodes are bidirectionally connected.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14087 BUG #14087]: "ctdb stop" command completes before databases are frozen.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14129 BUG #14129]: ctdb-tools: Stop deleted nodes from influencing ctdb nodestatus exit code.
*  Evgeny Sinelnikov <sin@altlinux.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14007 BUG #14007]: s3:ldap: Fix join with don't exists machine account.

 https://www.samba.org/samba/history/samba-4.10.9.html

Samba 4.10.8
------------------------

* Release Notes for Samba 4.10.8
* September 03, 2019

===============================
This is a security release in order to address the following defect:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-10197 CVE-2019-10197]: Combination of parameters and permissions can allow user to escape from the share path definition.

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-10197 CVE-2019-10197]:
* Under certain parameter configurations, when an SMB client accesses a network share and the user does not have permission to access the share root directory, it is possible for the user to escape from the share to see the complete '/' filesystem. Unix permission checks in the kernel are still enforced.

===============================
Changes since 4.10.7:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14035 BUG #14035]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-10197 CVE-2019-10197]: Permissions check deny can allow user to escape from the share.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14035 BUG #14035]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-10197 CVE-2019-10197]: Permissions check deny can allow user to escape from the share.

 https://www.samba.org/samba/history/samba-4.10.8.html

Samba 4.10.7
------------------------

* Release Notes for Samba 4.10.7
* August 22, 2019

===============================
This is the latest stable release of the Samba 4.10 release series.
===============================

------------------------

===============================
Changes since 4.10.6:
===============================

------------------------

*  Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14010 BUG #14010]: Unable to create or rename file/directory inside shares configured with vfs_glusterfs_fuse module.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13844 BUG #13844]: build: Allow build when '--disable-gnutls' is set.
*  Björn Baumbach <bb@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13973 BUG #13973]: samba-tool: Add 'import samba.drs_utils' to fsmo.py.
*  Tim Beale <timbeale@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14008 BUG #14008]: Fix 'Error 32 determining PSOs in system' message on old DB with FL upgrade.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14021 BUG #14021]: s4/libnet: Fix joining a Windows pre-2008R2 DC.
* * [https://bugzilla.samba.org/show_bug.cgi?id=14046 BUG #14046]: join: Use a specific attribute order for the DsAddEntry nTDSDSA object.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14015 BUG #14015]: vfs_catia: Pass stat info to synthetic_smb_fname().
*  Alexander Bokovoy <ab@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14091 BUG #14091]: lookup_name: Allow own domain lookup when flags == 0.
*  Gary Lockyer <gary@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13932 BUG #13932]: s4 librpc rpc pyrpc: Ensure tevent_context deleted last.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13915 BUG #13915]: DEBUGC and DEBUGADDC doesn't print into a class specific log file.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13949 BUG #13949]: Request to keep deprecated option "server schannel", VMWare Quickprep requires "auto".
* * [https://bugzilla.samba.org/show_bug.cgi?id=13967 BUG #13967]: dbcheck: Fallback to the default tombstoneLifetime of 180 days.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13969 BUG #13969]: dnsProperty fails to decode values from older Windows versions.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13973 BUG #13973]: samba-tool: Use only one LDAP modify for dns partition fsmo role transfer.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13960 BUG #13960]: third_party: Update waf to version 2.0.17.
*  Garming Sam <garming@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14051 BUG #14051]: netcmd: Allow 'drs replicate --local' to create partitions.
*  Rafael David Tinoco <rafaeldtinoco@ubuntu.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14017 #14017]: ctdb-config: Depend on /etc/ctdb/nodes file.

 https://www.samba.org/samba/history/samba-4.10.7.html

Samba 4.10.6
------------------------

* Release Notes for Samba 4.10.6
* July 8, 2019

===============================
This is the latest stable release of the Samba 4.10 release series.
===============================

------------------------

===============================
Changes since 4.10.5:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13956 BUG #13956]: s3: winbind: Fix crash when invoking winbind idmap scripts.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13964 BUG #13964]: smbd does not correctly parse arguments passed to dfree and quota scripts.
*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13965 BUG #13965]: samba-tool dns: use bytes for inet_ntop.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13828 BUG #13828]: samba-tool domain provision: Fix --interactive module in python3.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13893 BUG #13893]: ldb_kv: Skip @ records early in a search full scan.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13981 BUG #13981]: docs: Improve documentation of "lanman auth" and "ntlm auth" connection.
*  Björn Baumbach <bb@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=14002 BUG #14002]: python/ntacls: Use correct "state directory" smb.conf option instead of "state dir".
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13840 BUG #13840]: registry: Add a missing include.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13944 BUG #13944]: Fix SMB guest authentication.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13958 BUG #13958]: AppleDouble conversion breaks Resourceforks.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13968 BUG #13968]: vfs_fruit makes direct use of syscalls like mmap() and pread().
* * [https://bugzilla.samba.org/show_bug.cgi?id=13987 BUG #13987]: s3:mdssvc: Fix flex compilation error.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13872 BUG #13872]: s3/vfs_glusterfs[_fuse]: Avoid using NAME_MAX directly:
*  Aaron Haslett <aaronhaslett@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13799 BUG #13799]: dsdb:samdb: schemainfo update with relax control.
*  Aliaksei Karaliou <akaraliou@panasas.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13964 BUG #13964]: s3:util: Move static file_pload() function to lib/util.
*  Volker Lendecke <vl@samba.o*rg>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13957 BUG #13957]: smbd: Fix a panic.
*  Gary Lockyer <gary@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12478 BUG #12478]: ldap server: Generate correct referral schemes.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13941 BUG #13941]: s4 dsdb/repl_meta_data: fix use after free in dsdb_audit_add_ldb_value.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13942 BUG #13942]: s4 dsdb: Fix use after free in samldb_rename_search_base_callback.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12204 BUG #12204]: dsdb/repl: we need to replicate the whole schema before we can apply it.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12478 BUG #12478]: ldb: Release ldb 1.5.5
* * [https://bugzilla.samba.org/show_bug.cgi?id=13713 BUG #13713]: Schema replication fails if link crosses chunk boundary backwards.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13799 BUG #13799]: 'samba-tool domain schemaupgrade' uses relax control and skips the schemaInfo update provision.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13916 BUG #13916]: dsdb_audit: avoid printing "... remote host [Unknown] SID [(NULL SID)] ..."
* * [https://bugzilla.samba.org/show_bug.cgi?id=13917 BUG #13917]: python/ntacls: We only need security.SEC_STD_READ_CONTROL in order to get the ACL.
*  Shyamsunder Rathi <shyam.rathi@nutanix.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13947 BUG #13947]: s3:loadparm: Ensure to truncate FS Volume Label at multibyte boundary.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13939 BUG #13939]: Using Kerberos credentials to print using spoolss doesn't work.
*  Lukas Slebodnik <lslebodn@fedoraproject.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13998 BUG #13998]: wafsamba: Use native waf timer.
*  Rafael David Tinoco <rafaeldtinoco@ubuntu.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13984 BUG #13984]: ctdb-scripts: Fix tcp_tw_recycle existence check.

 https://www.samba.org/samba/history/samba-4.10.6.html

Samba 4.10.5
------------------------

* Release Notes for Samba 4.10.5
* June 19, 2019

===============================
This is a security release in order to address the following defects:
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-12435 CVE-2019-12435] (Samba AD DC Denial of Service in DNS management server (dnsserver))
* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-12436 CVE-2019-12436] (Samba AD DC LDAP server crash (paged searches))

===============================
Details
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-12435 CVE-2019-12435]:
* An authenticated user can crash the Samba AD DC's RPC server process via a NULL pointer dereference.

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-12436 CVE-2019-12436]:
* An user with read access to the directory can cause a NULL pointer dereference using the paged search control.

For more details and workarounds, please refer to the security advisories.

*[https://www.samba.org/samba/security/CVE-2019-12435.html CVE-2019-12435]
*[https://www.samba.org/samba/security/CVE-2019-12436.html CVE-2019-12436]

===============================
Changes since 4.10.4:
===============================

------------------------

*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13922 BUG #13922]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-12435 CVE-2019-12435] rpc/dns: Avoid NULL deference if zone not found in DnssrvOperation2.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13951 BUG #13951]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-12436 CVE-2019-12436] dsdb/paged_results: Ignore successful results without messages.

 https://www.samba.org/samba/history/samba-4.10.5.html

Samba 4.10.4
------------------------

* Release Notes for Samba 4.10.4
* May 22, 2019

===============================
This is the latest stable release of the Samba 4.10 release series.
===============================

------------------------

===============================
Changes since 4.10.3:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13938 BUG #13938]: s3: SMB1: Don't allow recvfile on stream fsp's.
*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13882 BUG #13882]: py/provision: Fix for Python 2.6.
*  Tim Beale <timbeale@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13873 BUG #13873]: netcmd: Fix 'passwordsettings --max-pwd-age' command.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13938 BUG #13938]: s3:smbd: Don't use recvfile on streams.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13861 BUG #13861]: s3-libnet_join: 'net ads join' to child domain fails when using "-U admin@forestroot".
*  David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13896 BUG #13896]: vfs_ceph: Explicitly enable libcephfs POSIX ACL support.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13940 BUG #13940]: vfs_ceph: Fix cephwrap_flistxattr() debug message.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13895 BUG #13895]: ctdb-common: Avoid race between fd and signal events.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13943 BUG #13943]: ctdb-common: Fix memory leak in run_proc.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13892 BUG #13892]: lib: Initialize getline() arguments.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13903 BUG #13903]: winbind: Fix overlapping id ranges.
*  Gary Lockyer <gary@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13902 BUG #13902]: lib util debug: Increase format buffer to 4KiB.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13927 BUG #13927]: nsswitch pam_winbind: Fix Asan use after free.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13929 BUG #13929]: s4 lib socket: Ensure address string owned by parent struct.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13936 BUG #13936]: s3 rpc_client: Fix Asan stack use after scope.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10097 BUG #10097]: s3:smbd: Handle IO_REPARSE_TAG_DFS in SMB_FIND_FILE_FULL_DIRECTORY_INFO.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10344 BUG #10344]: smb2_tcon: Avoid STATUS_PENDING completely on tdis.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12845 BUG #12845]: smb2_sesssetup: avoid STATUS_PENDING responses for session setup.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13698 BUG #13698]: smb2_tcon: Avoid STATUS_PENDING completely on tdis.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13796 BUG #13796]: smb2_sesssetup: avoid STATUS_PENDING responses for session setup.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13843 BUG #13843]: dbcheck: Fix the err_empty_attribute() check.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13858 BUG #13858]: vfs_snapper: Drop unneeded fstat handler.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13862 BUG #13862]: vfs_default: Fix vfswrap_offload_write_send() NT_STATUS_INVALID_VIEW_SIZE check.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13863 BUG #13863]: smb2_server: Grant all 8192 credits to clients.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13919 BUG #13919]: smbd: Implement SMB_FILE_NORMALIZED_NAME_INFORMATION handling.
*  Anoop C S <anoopcs@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13872 BUG #13872]: s3/vfs_glusterfs: Dynamically determine NAME_MAX.
*  Robert Sander <r.sander@heinlein-support.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13918 BUG #13918]: s3: modules: ceph: Use current working directory instead of share path.
*  Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13831 BUG #13831]: winbind: Use domain name from lsa query for sid_to_name cache entry.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13865 BUG #13865]: memcache: Increase size of default memcache to 512k.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13857 BUG #13857]: docs: Update smbclient manpage for "--max-protocol".
* * [https://bugzilla.samba.org/show_bug.cgi?id=13861 BUG #13861]: 'net ads join' to child domain fails when using "-U admin@forestroot".
* * [https://bugzilla.samba.org/show_bug.cgi?id=13937 BUG #13937]: s3:utils: If share is NULL in smbcacls, don't print it.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13939 BUG #13939]: s3:smbspool: Fix regression printing with Kerberos credentials.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13860 BUG #13860]: ctdb-scripts: CTDB restarts failed NFS RPC services by hand, which is incompatible with systemd.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13888 BUG #13888]: ctdb-daemon: Revert "We can not assume that just because we could complete a TCP handshake".
* * [https://bugzilla.samba.org/show_bug.cgi?id=13930 BUG #13930]: ctdb-daemon: Never use 0 as a client ID.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13943 BUG #13943]: ctdb-common: Fix memory leak.
*  Ralph Wuerthner <ralph.wuerthner@de.ibm.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13904 BUG #13904]: s3:debug: Enable logging for early startup failures.

 https://www.samba.org/samba/history/samba-4.10.4.html

Samba 4.10.3
------------------------

* Release Notes for Samba 4.10.3
* May 14, 2019

===============================
This is a security release in order to address the following defect:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16860 CVE-2018-16860] (Samba AD DC S4U2Self/S4U2Proxy unkeyed checksum)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16860 CVE-2018-16860]:
* The checksum validation in the S4U2Self handler in the embedded Heimdal KDC did not first confirm that the checksum was keyed, allowing replacement of the requested target (client) principal.

For more details and workarounds, please refer to the security advisory.

===============================
Changes since 4.10.2:
===============================

------------------------

*  Isaac Boukris <iboukris@gmail.com> 
* * [https://bugzilla.samba.org/show_bug.cgi?id=13685 BUG #13685]: CVE-2018-16860: Heimdal KDC: Reject PA-S4U2Self with unkeyed checksum.

 https://www.samba.org/samba/history/samba-4.10.3.html

Samba 4.10.2
------------------------

* Release Notes for Samba 4.10.2
* April 8, 2019

===============================
This is a security release in order to address the following defects:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3870 CVE-2019-3870] (World writable files in Samba AD DC private/ dir)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3880 CVE-2019-3880] (Save registry file outside share as unprivileged user)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3870 CVE-2019-3870]: During the provision of a new Active Directory DC, some files in the private/directory are created world-writable.
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3880 CVE-2019-3880]: Authenticated users with write permission can trigger a symlink traversal to write or detect files outside the Samba share.

For more details and workarounds, please refer to the security advisories. 
* [https://www.samba.org/samba/security/CVE-2019-3870.html CVE-2019-3870]
* [https://www.samba.org/samba/security/CVE-2019-3880.html CVE-2019-3880]

===============================
Changes since 4.10.1:
===============================

------------------------

*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13834 BUG #13834] : [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3870 CVE-2019-3870]: pysmbd: Ensure a zero umask is set for smbd.mkdir().
*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13851 BUG #13851]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3880 CVE-2019-3880]: rpc: winreg: Remove implementations of SaveKey/RestoreKey.

 https://www.samba.org/samba/history/samba-4.10.2.html

Samba 4.10.1
------------------------

* Release Notes for Samba 4.10.1
* April 3, 2019

===============================
This is the latest stable release of the Samba 4.10 release series.
===============================

------------------------

===============================
Changes since 4.10.0:
===============================

------------------------

*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13837 BUG #13837]: py/kcc_utils: py2.6 compatibility.
*  Philipp Gesang <philipp.gesang@intra2net.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13869 BUG #13869]: libcli: permit larger values of DataLength in SMB2_ENCRYPTION_CAPABILITIES of negotiate response.
*  Michael Hanselmann <public@hansmi.ch>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13840 BUG #13840]: regfio: Improve handling of malformed registry hive files.
*  Amitay Isaacs <amitay@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13789 BUG #13789]: ctdb-version: Simplify version string usage.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13859 BUG #13859]: lib: Make fd_load work for non-regular files.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13816 BUG #13816]: dbcheck in the middle of the tombstone garbage collection causes replication failures, dbcheck: add --selftest-check-expired-tombstones cmdline option.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13818 BUG #13818]: ndr_spoolss_buf: Fix out of scope use of stack variable in NDR_SPOOLSS_PUSH_ENUM_OUT().
*  Anoop C S <anoopcs@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13854 BUG #13854]: s4/messaging: Fix undefined reference in linking libMESSAGING-samba4.so.
*  Garming Sam <garming@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13836 BUG #13836]: acl_read: Fix regression for empty lists.
*  Michael Saxl <mike@mwsys.mine.bz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13841 BUG #13841]: s4:dlz make b9_has_soa check dc=@ node.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13832 BUG #13832]: s3:client: Fix printing via smbspool backend with kerberos auth.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13847 BUG #13847]: s4:librpc: Fix installation of Samba.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13848 BUG #13848]: s3:lib: Fix the debug message for adding cache entries.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13793 BUG #13793]: s3:utils: Add 'smbstatus -L --resolve-uids' to show username.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13848 BUG #13848]: s3:lib: Fix the debug message for adding cache entries.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13853 BUG #13853]: s3:waf: Fix the detection of makdev() macro on Linux.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13789 BUG #13789]: ctdb-build: Drop creation of .distversion in tarball.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13838 BUG #13838]: ctdb-packaging: Test package requires tcpdump, ctdb package should not own system library directory.

 https://www.samba.org/samba/history/samba-4.10.1.html

Samba 4.10.0
------------------------

<onlyinclude>
* Release Notes for Samba 4.10.0
* March 19, 2019

===============================
Release Announcements
===============================

------------------------

This is the first stable release of the Samba 4.10 release series. Please read the release notes carefully before upgrading.

===============================
NEW FEATURES/CHANGES
===============================

------------------------

GPO Improvements=
===============================

------------------------

A new 'samba-tool gpo backup' command has been added that can export a set of Group Policy Objects from a domain in a generalised XML format.

A corresponding 'samba-tool gpo restore' command has been added to rebuild the Group Policy Objects from the XML after generalization. (The administrator needs to correct the values of XML entities between the backup and restore to account for the change in domain).

* `GPO_Backup_and_Restore | GPO Backup and Restore`

KDC prefork=
===============================

------------------------

The KDC now supports the pre-fork process model and worker processes will be forked for the KDC when the pre-fork process model is selected for samba.

Prefork 'prefork children'=
===============================

------------------------

The default value for this smdb.conf parameter has been increased from 1 to 4.

Netlogon prefork=
===============================

------------------------

DCERPC now supports pre-forked NETLOGON processes. The netlogon processes are pre-forked when the prefork process model is selected for samba.

Offline domain backups=
===============================

------------------------

The 'samba-tool domain backup' command has been extended with a new 'offline' option. This safely creates a backup of the local DC's database directly from disk. The main benefits of an offline backup are it's quicker, it stores more database details (for forensic purposes), and the samba process does not have to be running when the backup is made. Refer to the samba-tool help for more details on using this command.

Group membership statistics=
===============================

------------------------

A new 'samba-tool group stats' command has been added. This provides summary information about how the users are spread across groups in your domain. The 'samba-tool group list --verbose' command has also been updated to include the number of users in each group.

Paged results LDAP control=
===============================

------------------------

The behaviour of the paged results control (1.2.840.113556.1.4.319, RFC2696) has been changed to more closely match Windows servers, to improve memory usage. Paged results may be used internally (or is requested by the user) by LDAP libraries or tools that deal with large result sizes, for example, when listing all the objects in the database.

Previously, results were returned as a snapshot of the database but now, some changes made to the set of results while paging may be reflected in the responses. If strict inter-record consistency is required in answers (which is not possible on Windows with large result sets), consider avoiding the paged results control or alternatively, it might be possible to enforce restrictions using the LDAP filter expression.

For further details see `Paged_Results`

Prefork process restart=
===============================

------------------------

The pre-fork process model now restarts failed processes. The delay between restart attempts is controlled by the "prefork backoff increment" (default = 10) and "prefork maximum backoff" (default = 120) smbd.conf parameters.  A linear back off strategy is used with "prefork backoff increment" added to the delay between restart attempts up until it reaches "prefork maximum backoff".

Using the default sequence the restart delays (in seconds) are:
* 0, 10, 20, ..., 120, 120, ...

Standard process model=
===============================

------------------------

When using the standard process model samba forks a new process to handle ldap and netlogon connections.  Samba now honours the 'max smbd processes' smb.conf parameter.  The default value of 0, indicates there is no limit.  The limit is applied individually to netlogon and ldap.  When the process limit is exceeded Samba drops new connections immediately.

===============================
python3 support
===============================

------------------------

This is the first release of Samba which has full support for Python 3. Samba 4.10 still has support for Python 2, however, Python 3 will be used by default, i.e. 'configure' & 'make' will execute using python3.

To build Samba with python2 you *must* set the 'PYTHON' environment variable for both the 'configure' and 'make' steps, i.e.
   'PYTHON=python2 ./configure'
   'PYTHON=python2 make'
This will override the python3 default.

Alternatively, it is possible to produce Samba Python bindings for both Python 2 and Python 3. To do so, specify '--extra-python=/usr/bin/python2' as part of the 'configure' command. Note that python3 will still be used as the default in this case.

* Note:Samba 4.10 supports Python 3.4 onwards.

===============================
Future Python support
===============================

------------------------

Samba 4.10 will be the last release that comes with full support for Python 2. Unfortunately, the Samba Team doesn't have the resources to support both Python 2 and Python 3 long-term.

Samba 4.11 will not have any runtime support for Python 2. This means if you use Python 2 bindings it is time to migrate to Python 3 now.

If you are building Samba using the '--disable-python' option (i.e. you're excluding all the run-time Python support), then this will continue to work on a system that supports either python2 or python3.

* Note:Samba 4.11 will most likely only support Python 3.6 onwards.

JSON logging=
===============================

------------------------

Authentication messages now contain the Windows Event Id "eventId" and logon type "logonType". The supported event codes and logon types are:
* Event codes:
* :4624  Successful logon
* :4625  Unsuccessful logon

* Logon Types:
* :2  Interactive
* :3  Network
* :8  NetworkCleartext

The version number for Authentication messages is now 1.1, changed from 1.0

Password change messages now contain the Windows Event Id "eventId", the supported event Id's are:
* 4723 Password changed
* 4724 Password reset

The version number for PasswordChange messages is now 1.1, changed from 1.0

Group membership change messages now contain the Windows Event Id "eventId", the supported event Id's are:
* 4728 A member was added to a security enabled global group
* 4729 A member was removed from a security enabled global group
* 4732 A member was added to a security enabled local group
* 4733 A member was removed from a security enabled local group
* 4746 A member was added to a security disabled local group
* 4747 A member was removed from a security disabled local group
* 4751 A member was added to a security disabled global group
* 4752 A member was removed from a security disabled global group
* 4756 A member was added to a security enabled universal group
* 4757 A member was removed from a security enabled universal group
* 4761 A member was added to a security disabled universal group
* 4762 A member was removed from a security disabled universal group

The version number for GroupChange messages is now 1.1, changed from 1.0. Also A GroupChange message is generated when a new user is created to log that the user has been added to their primary group.

The leading "JSON <message type>:" and source file  prefix of the JSON formatted log entries has been removed to make the parsing of the JSON log messages easier. JSON log entries now start with 2 spaces followed by an opening brace i.e. "  {"

SMBv2 samba-tool support=
===============================

------------------------

On previous releases, some samba-tool commands would not work against a remote DC that had SMBv1 disabled. SMBv2 support has now been added for samba-tool. The affected commands are 'samba-tool domain backup|rename' and the 'samba-tool gpo' set of commands. Refer also [https://bugzilla.samba.org/show_bug.cgi?id=13676 BUG #13676].

New glusterfs_fuse VFS module=
===============================

------------------------

The new vfs_glusterfs_fuse module improves performance when Samba accesses a glusterfs volume mounted via FUSE (Filesystem in Userspace as part of the Linux kernel). It achieves that by leveraging a mechanism to retrieve the appropriate case of filenames by querying a specific extended attribute in the filesystem. No extra configuration is required to use this module, only glusterfs_fuse needs to be set in the "vfs objects" parameter. Further details can be found in the vfs_glusterfs_fuse(8) manpage. This new vfs_glusterfs_fuse module does not replace the existing vfs_glusterfs module, it just provides an additional, alternative mechanism to access a Gluster volume.

===============================
REMOVED FEATURES
===============================

------------------------

MIT Kerberos build of the AD DC=
===============================

------------------------

While not removed, the MIT Kerberos build of the Samba AD DC is still considered experimental.  Because Samba will not issue security patches for this configuration, such builds now require the explicit configure option: --with-experimental-mit-ad-dc

For further details see
`Running_a_Samba_AD_DC_with_MIT_Kerberos_KDC`

samba_backup=
===============================

------------------------

The samba_backup script has been removed. This has now been replaced by the 'samba-tool domain backup offline' command.

SMB client Python bindings=
===============================

------------------------

The SMB client python bindings are now deprecated and will be removed in future Samba releases. This will only affects users that may have used the Samba Python bindings to write their own utilities, i.e. users with a custom Python script that includes the line 'from samba import smb'.

===============================
smb.conf changes
===============================

------------------------

    arameter Name                     Description                Default
    -------------                     -----------                -------
    refork backoff increment   Delay added to process restart    10 (seconds)
                              between attempts.
    refork maximum backoff     Maximum delay for process between 120 (seconds)
                              process restart attempts
    mbd search ask sharemode   Name changed, old name was
                              "smbd:search ask sharemode"
    mbd async dosmode          Name changed, old name was
                              "smbd:async dosmode"
    mbd max async dosmode      Name changed, old name was
                              "smbd:max async dosmode"
    mbd getinfo ask sharemode  New: similar to "smbd search ask yes
                              sharemode" but for SMB getinfo
</onlyinclude>
==============================

HANGES SINCE 4.10.0rc4
===============================

------------------------

*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13760 BUG #13760]: s4-server: Open and close a transaction on sam.ldb at startup.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13812 BUG #13812]: access_check_max_allowed() doesn't process "Owner Rights" ACEs.
*  Joe Guo <joeg@catalyst.net.nz>
* * s4/scripting/bin: Open unicode files with utf8 encoding and write unicode string.
*  Björn Jacke <bj@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13759 BUG #13759]: sambaundoguididx: Use the right escaped oder unescaped sam ldb files.
*  Volker Lendecke <vl@samba.org>
* * BUG 13813: Fix idmap cache pollution with S-1-22- IDs on winbind hickup.
*  Christof Schmitt <cs@samba.org>
* * passdb: Update ABI to 0.27.2.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13813 BUG #13813]: lib/winbind_util: Add winbind_xid_to_sid for --without-winbind.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13823 BUG #13823]: lib:util: Move debug message for mkdir failing to log level 1.

===============================
CHANGES SINCE 4.10.0rc3
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13803 BUG #13803]: SMB1 POSIX mkdir does case insensitive name lookup.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13802 BUG #13802]: Fix idmap xid2sid cache issue.
*  David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13807 BUG #13807]: vfs_ceph strict_allocate_ftruncate calls (local FS) ftruncate and fallocate.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13786 BUG #13786]: messages_dgm: Properly handle receiver re-initialization.
*  Gary Lockyer <gary@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13765 BUG #13765]: man pages: Document prefork process model.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13773 BUG #13773]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3824 CVE-2019-3824] ldb: wildcard_match end of data check.
*  Stefan Metzmacher <metze@samba.org>
* * tdb: Fix compatibility of wscript with older python.
* * tevent: version 0.9.39
* * [https://bugzilla.samba.org/show_bug.cgi?id=13773 BUG #13773]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3824 CVE-2019-3824] ldb: version 1.5.4
*  David Mulder <dmulder@suse.com>
* * Search for location of waf script.
*  Noel Power <noel.power@suse.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13777 BUG #13777]: buildtools/wafsamba: Avoid decode when using python2.
*  Jiří Šašek <jiri.sasek@oracle.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13704 BUG #13704]: notifyd: Fix SIGBUS on sparc.
*  Swen Schillig <swen@linux.ibm.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13791 BUG #13791]: ctdb: Buffer write beyond limits.
*  Lukas Slebodnik <lslebodn@fedoraproject.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13773 BUG #13773]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3824 CVE-2019-3824] ldb: Out of bound read in ldb_wildcard_compare.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13790 BUG #13790]: ctdb-config: Change example recovery lock setting to one that fails.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13800 BUG #13800]: Fix recovery lock bug.

===============================
CHANGES SINCE 4.10.0rc2
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13690 BUG #13690]: smbd: uid: Don't crash if 'force group' is added to an existing share connection.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13770 BUG #13770]: s3: VFS: vfs_fruit. Fix the NetAtalk deny mode compatibility code.
*  Andrew Bartlett <abartlet@samba.org>
* * ldb: Release ldb 1.5.3
* * [https://bugzilla.samba.org/show_bug.cgi?id=13762 BUG #13762]: Avoid inefficient one-level searches.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13772 BUG #13772]: The test api.py should not rely on order of entries in dict.
*  Tim Beale <timbeale@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13762 BUG #13762]: ldb: Avoid inefficient one-level searches.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13776 BUG #13776]: tldap: Avoid use after free errors.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13746 BUG #13746]: s3-smbd: Use fruit:model string for mDNS registration.
*  David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13766 BUG #13766]: printing: Check lp_load_printers() prior to pcap cache update.
*  Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13787 BUG #13787]: waf: Check for libnscd.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13770 BUG #13770]: s3:vfs: Correctly check if OFD locks should be enabled or not.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13778 BUG #13778]: Public ZERO_STRUCT() uses undefined C11 function memset_s().

===============================
CHANGES SINCE 4.10.0rc1
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13750 BUG #13750]: libcli: dns: Change internal DNS_REQUEST_TIMEOUT from 2 to 10 seconds.
*  Tim Beale <timbeale@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13676 BUG #13676]: samba-tool SMB/sysvol connections do not work if SMBv1 is disabled.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13747 BUG #13747]: join: Throw CommandError instead of Exception for simple errors.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13774 BUG #13774]: s3-vfs: Add glusterfs_fuse vfs module.
*Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13742 BUG #13742]: ctdb: Print locks latency in machinereadable stats.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13752 BUG #13752]: s4:server: Add support for 'smbcontrol samba shutdown'.
*  Anoop C S <anoopcs@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13330 BUG #13330]: vfs_glusterfs: Adapt to changes in libgfapi signatures.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13774 BUG #13774]: s3-vfs: Use ENOATTR in errno comparison for getxattr.
*  Justin Stephenson <jstephen@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13727 BUG #13727]: s3:libsmb: Honor disable_netbios option in smbsock_connect_send.

===============================
KNOWN ISSUES
===============================

------------------------

`Release_Planning_for_Samba_4.10#Release_blocking_bugs`

    ttps://www.samba.org/samba/history/samba-4.10.0.html

----
`Category:Release Notes`