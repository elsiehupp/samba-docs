Samba 3.6 Features added/changed
    <namespace>0</namespace>
<last_edited>2017-02-26T21:09:07Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

With the release of Samba 4.2.0, Samba 3.6 has been marked `Samba_Release_Planning#Discontinued|**discontinued**`.

Samba 3.6 is the **Discontinued** release series
------------------------

*[https://bugzilla.samba.org/show_bug.cgi?id=8595 3.6 release bug]
*[https://bugzilla.samba.org/showdependencygraph.cgi?id=8595&display=tree&rankdir=LR 3.6 release bug]  Dependency Graph

Samba 3.6.25
------------------------

* Release Notes for Samba 3.6.25
* February 23, 2015

===============================
This is a security release in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-0240 CVE-2015-0240].
===============================

------------------------

* Unexpected code execution in smbd

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-0240 CVE-2015-0240]:
* All versions of Samba from 3.5.0 to 4.2.0rc4 are vulnerable to an unexpected code execution vulnerability in the smbd file server daemon.

* A malicious client could send packets that may set up the stack in such a way that the freeing of memory in a subsequent anonymous netlogon packet could allow execution of arbitrary code. This code would execute with root privileges.

===============================
Changes since 3.6.24:
===============================

------------------------

*   Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11077 BUG #11077]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-0240 CVE-2015-0240]: talloc free on uninitialized stack pointer in netlogon server could lead to security vulnerability.
*   Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11077 BUG #11077]: CVE-2015-0240: s3-netlogon: Make sure we do not deference a NULL pointer.

 https://www.samba.org/samba/history/samba-3.6.25.html
Samba 3.6.24
------------------------

* Release Notes for Samba 3.6.24
* June 23, 2014

===============================
This is a security release in order to address
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0244 CVE-2014-0244]
* All current released versions of Samba are vulnerable to a denial of service on the nmbd NetBIOS name services daemon. A malformed packet can cause the nmbd server to loop the CPU and prevent any further NetBIOS name service.
* This flaw is not exploitable beyond causing the code to loop expending CPU resources.

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-3493 CVE-2014-3493]
* All current released versions of Samba are affected by a denial of service crash involving overwriting memory on an authenticated connection to the smbd file server.

===============================
Changes since 3.6.23:
===============================

------------------------

*   Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10245 BUG #10633]BUG 10633: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0244 CVE-2014-0244]: Fix nmbd denial of service.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10245 BUG #10654]BUG 10654: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-3493 CVE-2014-3493]: Fix segmentation fault in smbd_marshall_dir_entry()'s SMB_FIND_FILE_UNIX handler.

 http://www.samba.org/samba/history/samba-3.6.24.html

Samba 3.6.23
------------------------

* Release Notes for Samba 3.6.23
* March 11, 2014

===============================
This is a security release in order to address
===============================

------------------------

*[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4496 CVE-2013-4496] (Password lockout not enforced for SAMR password changes).

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4496 CVE-2013-4496]: Samba versions 3.4.0 and above allow the administrator to implement locking out Samba accounts after a number of bad password attempts.
* :However, all released versions of Samba did not implement this check for password changes, such as are available over multiple SAMR and RAP interfaces, allowing password guessing attacks.

===============================
Changes since 3.6.22:
===============================

------------------------

* Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10245 bug #10245]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4496 CVE-2013-4496]: Enforce password lockout for SAMR password changes.
* Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10245 bug #10245]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4496 CVE-2013-4496]: Enforce password lockout for SAMR password changes.

 http://www.samba.org/samba/history/samba-3.6.23.html

Samba 3.6.22
------------------------

* Release Notes for Samba 3.6.22
* December 9, 2013

===============================
This is a security release in order to address
===============================

------------------------

*[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4408 CVE-2013-4408] (DCE-RPC fragment length field is incorrectly checked) and
*[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-6150 CVE-2012-6150] (pam_winbind login without require_membership_of restrictions).

*[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4408 CVE-2013-4408]:
* Samba versions 3.4.0 and above (versions 3.4.0 - 3.4.17, 3.5.0 - 3.5.22, 3.6.0 - 3.6.21, 4.0.0 - 4.0.12 and including 4.1.2) are vulnerable to buffer overrun exploits in the client processing of DCE-RPC packets. This is due to incorrect checking of the DCE-RPC fragment length in the client code.

* This is a critical vulnerability as the DCE-RPC client code is part of the winbindd authentication and identity mapping daemon, which is commonly configured as part of many server installations (when joined to an Active Directory Domain). A malicious Active Directory Domain Controller or man-in-the-middle attacker impersonating an Active Directory Domain Controller could achieve root-level access by compromising the winbindd process.

* Samba server versions 3.4.0 - 3.4.17 and versions 3.5.0 - 3.5.22 are also vulnerable to a denial of service attack (server crash) due to a similar error in the server code of those versions.

* Samba server versions 3.6.0 and above (including all 3.6.x versions, all 4.0.x versions and 4.1.x) are not vulnerable to this problem.

* In addition range checks were missing on arguments returned from calls to the DCE-RPC functions LookupSids (lsa and samr), LookupNames (lsa and samr) and LookupRids (samr) which could also cause similar problems.

* As this was found during an internal audit of the Samba code there are no currently known exploits for this problem (as of December 9th 2013).

*[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-6150 CVE-2012-6150]:
* Winbind allows for the further restriction of authenticated PAM logins using the require_membership_of parameter. System administrators may specify a list of SIDs or groups for which an authenticated user must be a member of. If an authenticated user does not belong to any of the entries, then login should fail. Invalid group name entries are ignored.

* Samba versions 3.3.10, 3.4.3, 3.5.0 and later incorrectly allow login from authenticated users if the require_membership_of parameter specifies only invalid group names.

* This is a vulnerability with low impact. All require_membership_of group names must be invalid for this bug to be encountered.

===============================
Changes since 3.6.21:
===============================

------------------------

* Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10185 bug #10185] [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4408 CVE-2013-4408]: Correctly check DCE-RPC fragment length field.
* Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10185 bug #10185] [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4408 CVE-2013-4408]: Correctly check DCE-RPC fragment length field.
* Noel Power <noel.power@suse.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10300 bug #10300], [https://bugzilla.samba.org/show_bug.cgi?id=10306 bug #10306]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-6150 CVE-2012-6150]: Fail authentication if user isn't member of *any* require_membership_of specified groups.

Samba 3.6.21
------------------------

* Release Notes for Samba 3.6.21
* November 29, 2013
===============================
This is is the latest stable release of Samba 3.6.
===============================

------------------------

===============================
Changes since 3.6.20:
===============================

------------------------

*   Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10139 bug #10139]: Valid utf8 filenames cause "invalid conversion error" messages.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10267 bug #10167]: s3-smb2 server: smb2 breaks "smb encryption = mandatory".
* * [https://bugzilla.samba.org/show_bug.cgi?id=10187 bug #10187]: Missing talloc_free can leak stackframe in error path.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10247 bug #10247]: xattr: Fix listing EAs on *BSD for non-root users.
*   Korobkin <korobkin+samba@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10118 bug #10118]: Raise debug level for being unable to open a printer.
*   Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10195 bug #10195]: nsswitch: Fix short writes in winbind_write_sock.
*   Arvid Requate <requate@univention.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10267 bug #10267]: Fix Windows 8 printing via local printer drivers.
*   Andreas Schneider <asn@cryptomilk.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10194 bug #10194]: Make offline logon cache updating for cross child domain group membership.

Samba 3.6.20
------------------------

* Release Notes for Samba 3.6.20
* November 11, 2013

This is a security release in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4475 CVE-2013-4475] (ACLs are not checked on opening an alternate data stream on a file or directory) .

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4475 CVE-2013-4475]:
* Samba versions 3.2.0 and above (all versions of 3.2.x, 3.3.x, 3.4.x, 3.5.x, 3.6.x, 4.0.x and 4.1.x) do not check the underlying file or directory ACL when opening an alternate data stream.
* According to the SMB1 and SMB2+ protocols the ACL on an underlying file or directory should control what access is allowed to alternate data streams that are associated with the file or directory.
* By default no version of Samba supports alternate data streams on files or directories.
* Samba can be configured to support alternate data streams by loading either one of two virtual file system modues (VFS) vfs_streams_depot or vfs_streams_xattr supplied with Samba, so this bug only affects Samba servers configured this way.
* To determine if your server is vulnerable, check for the strings "streams_depot" or "streams_xattr" inside your smb.conf configuration file.

===============================
Changes since 3.6.19:
===============================

------------------------

* Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10234 bug #10234] + [https://bugzilla.samba.org/show_bug.cgi?id=10229 bug #10229]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4475 CVE-2013-4475]: Fix access check verification on stream files.

Samba 3.6.19
------------------------

* Release Notes for Samba 3.6.19
* September 25, 2013

===============================
This is is the latest maintenance release of Samba 3.6.
===============================

------------------------

Please note that this will probably be the last maintenance release of the Samba 3.6 release series. With the release of Samba 4.1.0, the 3.6 release series will be turned into the "security fixes only" mode.

Changes since 3.6.18:=
===============================

------------------------

* Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=5917 bug #5917]: Make Samba work on site with Read Only Domain Controller.
* Christian Ambach <ambi@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=8955 bug #8955]: NetrServerPasswordSet2 timeout is too short.
* Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9899 bug #9899]: Fix fallback to ncacn_np in cm_connect_lsat().
* * [https://bugzilla.samba.org/show_bug.cgi?id=9615 bug #9615]: Fix fallback to ncacn_np in cm_connect_lsat().
* * [https://bugzilla.samba.org/show_bug.cgi?id=10127 bug #10127]: Fix 'smbstatus' as non-root user.
* Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=8955 bug #8955]: Give machine password changes 10 minutes of time.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10106 bug #10106]: Honour output buffer length set by the client for SMB2 GetInforequests.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10114 bug #10114]: Handle Dropbox (write-only-directory) case correctly in pathname lookup.
* Karolin Seeger <kseeger@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10076 bug #10076]: Fix variable list in man vfs_crossrename.
* Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9994 bug #9994]: s3-winbind: Do not delete an existing valid credential cache.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10073 bug #10073]: 'net ads join': Fix segmentation fault in create_local_private_krb5_conf_for_domain.
*   Richard Sharpe <realrichardsharpe@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10097 bug #10097]: MacOSX 10.9 will not follow path-based DFS referrals handed out by Samba.

Samba 3.6.18
------------------------

* Release Notes for Samba 3.6.18
* August 14, 2013

===============================
This is is the latest stable release of Samba 3.6.
===============================

------------------------

Changes since 3.6.17:=
===============================

------------------------

*Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9777 bug #9777]: vfs_dirsort uses non-stackable calls, dirfd(), malloc instead of talloc and doesn't cope with directories being modified whilst reading.
*Gregor Beck <gbeck@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9678 bug #9678]: Windows 8 Roaming profiles fail.
* Alexander Bokovoy <ab@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9636 bug #9636]: Fix parsing linemarkers in preprocessor output.
* Björn Jacke <bj@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9880 bug #9880]: Use of wrong RFC2307 primary group field.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9983 bug #983]3: Fix output of syslog-facility check.
* Ralph Wuerthner <ralphw@de.ibm.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10064 bug #910064]: Linux kernel oplock breaks can miss signals.

Samba 3.6.17
------------------------

* Release Notes for Samba 3.6.17
* August 05, 2013

===============================
This is a security release in order to address
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4124 CVE-2013-4124] (Missing integer wrap protection in EA list reading can cause
server to loop with DOS).

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4124 CVE-2013-4124]:
* All current released versions of Samba are vulnerable to a denial of service on an authenticated or guest connection. A malformed packet can cause the smbd server to loop the CPU performing memory allocations and preventing any further service.

* A connection to a file share, or a local account is needed to exploit this problem, either authenticated or unauthenticated if guest connections are allowed.

* This flaw is not exploitable beyond causing the code to loop allocating memory, which may cause the machine to exceed memory limits.

Changes since 3.6.16:=
===============================

------------------------

*Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10010 bug #10010`: CVE-2013-4124: Missing integer wrap protection in EA list reading can cause server to loop with DOS.

Samba 3.6.16
------------------------

* Release Notes for Samba 3.6.16
* June 19, 2013

===============================
This is is the latest stable release of Samba 3.6.
===============================

------------------------

Major enhancements in Samba 3.6.16 include:=
===============================

------------------------

*  [https://bugzilla.samba.org/show_bug.cgi?id=9822 bug #9822]Fix crash bug during Win8 sync.
*  [https://bugzilla.samba.org/show_bug.cgi?id=9722 bug #9722]Properly handle Oplock breaks in compound requests.

Changes since 3.6.15:=
===============================

------------------------

* Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9881 bug #9881]: Link dbwrap_tool and dbwrap_torture against libtevent.
* Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9722 bug #9722]: Properly handle Oplock breaks in compound requests.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9822 bug #9822]: Fix crash bug during Win8 sync.
* Anand Avati <avati@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9927 bug #9927]: errno gets overwritten in call to check_parent_exists().
* David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=8997 bug #8997]: Change libreplace GPL source to LGPL.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9900 bug #9900]: is_printer_published GUID retrieval.
* Peng Haitao <penght@cn.fujitsu.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9941 bug #9941]: Fix a bug of drvupgrade of smbcontrol.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9868 bug #9868]: Don't know how to make LIBNDR_PREG_OBJ.
* SATOH Fumiyasu <fumiyas@osstech.co.jp>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9688 bug #9688]: Remove "experimental" label on "max protocol=SMB2" parameter.
* Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9881 bug #9881]: Check for system libtevent.

Samba 3.6.15 
------------------------

* Release Notes for Samba 3.6.15
* May 08, 2013

===============================
This is is the latest stable release of Samba 3.6.
===============================

------------------------

Major enhancements in Samba 3.6.15 include:=
===============================

------------------------

*  Fix crash bug in Winbind [https://bugzilla.samba.org/show_bug.cgi?id=9854 bug #9854].

Changes since 3.6.14:=
===============================

------------------------

*   Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9746 bug #9746]: Fix "guest ok", "force user" and "force group" for guest users.

*   David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9830 bug #9830]: Fix panic in nt_printer_publish_ads.

*   Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9854 bug #9854]: Fix crash bug in Winbind.

*   Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9817 bug #9817]: Fix 'map untrusted to domain' with NTLMv2.

Samba 3.6.14
------------------------

* Release Notes for Samba 3.6.14
* April 29, 2013

===============================
This is is the latest stable release of Samba 3.6.
===============================

------------------------

Major enhancements in Samba 3.6.14 include:=
===============================

------------------------

* Certain xattrs cause Windows error 0x800700FF [https://bugzilla.samba.org/show_bug.cgi?id=9130 bug #9130].

Changes since 3.6.13:=
===============================

------------------------

* Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9130 bug #9130]BUG 9130: Certain xattrs cause Windows error 0x800700FF.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9724 bug #9724]BUG 9724: Use is_encrypted_packet() function correctly inside server.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9733 bug #9733]: Fix 'smbcontrol close-share' is not working.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9747 bug #9747]: Make sure that we only propogate the INHERITED flag when we are allowed to.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9748 bug #9748]: Remove unneeded fstat system call from hot read path.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9811 bug #9811]: Fix bug in old create temp SMB request. Only use VFS functions.
* David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9650 bug #9645]: New or deleted CUPS printerqueues are not recognized by Samba.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9807 bug #9807]: wbinfo: Fix segfault in wbinfo_pam_logon.
* Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9727 bug #9727]: wkssvc: Fix NULL pointer dereference.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9736 bug #9736]: smbd: Tune "dir" a bit.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9775 bug #9775]: Fix segfault for "artificial" conn_structs.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9809 bug #9809]: RHEL SPEC: Package dbwrap_tool man page.
* Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9139 bug #9139]: Fix the username map optimization.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9699 bug #9699]: Fix adding case sensitive spn.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9723 bug #9723]: Add a tool to migrate latin1 printing tdbs to registry.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9735 bug #9735]: Fix Winbind separator in upn to username conversion.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9766 bug #9766]: Cache name_to_sid/sid_to_name correctly.

Note about upgrading from older versions:=
===============================

------------------------

It is still the case that there are printing tdbs (ntprinting.tdb, ntforms.tdb, ntdrivers.tdb) which are in latin1 or other encodings. When updating from Samba 3.5 or earlier to Samba 3.6 or 4.0 these tdbs need to be migrated to our new registry based printing management.  This means during the migration we also need to do charset conversion. This can only been done manually cause we don't know in which encoding the tdb is. You have to specify the correct code page for the conversion, see iconv -l and Wikipedia [1] for the available codepages. The mostly used one is Windows Latin1 which is CP1252.

We've extended the 'net printing dump' and 'net printing migrate' commands to define the encoding of the tdb. So you can correctly view the tdb with:

    et printing dump encoding=CP1252 /path/to/ntprinters.tdb

or migrate it with e.g.:

    et printing migrate encoding=CP1252 /path/to/ntprinters.tdb

If you migrate printers we suggest you do it in the following order.

# ntforms.tdb
# ntdrivers.tdb
# ntprinting.tdb

Don't forget to rename, move or delete these files in /var/lib/samba after the migration.

 [1] https://en.wikipedia.org/wiki/Code_page

Samba 3.6.13
------------------------

* Release Notes for Samba 3.6.13
* March 18, 2013

===============================
This is is the latest stable release of Samba 3.6.
===============================

------------------------

Major enhancements in Samba 3.6.13 include:=
===============================

------------------------

*  Fix two resource leaks in winbindd [https://bugzilla.samba.org/show_bug.cgi?id=9684 bug #9648].
*  Unlink after open causes smbd to panic [https://bugzilla.samba.org/show_bug.cgi?id=9571 bug #9571].

Changes since 3.6.12:=
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9519 bug #9519]: Samba returns unexpected error on SMB posix open.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9585 bug #9585]: Samba 3.6.x not correctly signing any but the last response in a compound request/response.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9586 bug #9586]: smbd[29175]: disk_free: sys_popen() failed" message logged in /var/log/messages many times.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9587 bug #9587]: Archive flag is always set on directories.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9588 bug #9588]: ACLs are not inherited to directories for DFS shares.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9637 bug #9637]: Renaming directories as guest user in security share mode doesn't work.
*  Björn Baumbach <bb@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9568 bug #9568]: Add dbwrap_tool.1 manual page.
*  Ira Cooper <ira@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9646 bug #9646]: Make SMB2_GETINFO multi-volume aware.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9474 bug #9474]: Downgrade v4 printer driver requests to v3.
*   David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9378 bug #9378]: Add extra attributes for AD printer publishing.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9658 bug #9658]: Fix initial large PAC sess setup response.
*  Björn Jacke <bj@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=7825 bug #7825]: Fix GNU ld version detection with old gcc releases.
*  Daniel Kobras <d.kobras@science-computing.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9039 bug #9039]: 'map untrusted to domain' treats WORKSTATION as bogus domain.
*  Guenter Kukkukk <kukks@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9701 bug #9701]: Fix vfs_catia module.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9541 bug #9541]: Add support for posix_openpt.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9625 bug #9625]: wbcAuthenticateEx gives unix times.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9574 bug #9574]: Fix a possible null pointer dereference in spoolss.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9684 bug #9684]: Fix two resource leaks in winbindd.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9686 bug #9686]: Fix a possible buffer overrun in pdb_smbpasswd.
*   Pavel Shilovsky <piastry@etersoft.ru>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9571 bug #9571]: Unlink after open causes smbd to panic.

 http://www.samba.org/samba/history/samba-3.6.13.html

Samba 3.6.12
------------------------

* Release Notes for Samba 3.6.12
* January 30, 2013

===============================
This is a security release in order to address
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-0213 CVE-2013-0213] Clickjacking issue in SWAT
* All current released versions of Samba are vulnerable to clickjacking in the Samba Web Administration Tool (SWAT). When the SWAT pages are integrated into a malicious web page via a frame or iframe and then overlaid by other content, an attacker could trick an administrator to potentially change Samba settings.

* In order to be vulnerable, SWAT must have been installed and enabled either as a standalone server launched from inetd or xinetd, or as a CGI plugin to Apache. If SWAT has not been installed or enabled (which is the default install state for Samba) this advisory can be ignored.

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-0214 CVE-2013-0214] Potential XSRF in SWAT
* All current released versions of Samba are vulnerable to a cross-site request forgery in the Samba Web Administration Tool (SWAT). By guessing a user's password and then tricking a user who is authenticated with SWAT into clicking a manipulated URL on a different web page, it is possible to manipulate SWAT.
* In order to be vulnerable, the attacker needs to know the victim's password. Additionally SWAT must have been installed and enabled either as a standalone server launched from inetd or xinetd, or as a CGI plugin to Apache. If SWAT has not been installed or enabled (which is the default install state for Samba) this advisory can be ignored.

Changes since 3.6.11:=
===============================

------------------------

* Kai Blin <kai@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9576 bug #9576] CVE-2013-0213: Fix clickjacking issue in SWAT.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9577 bug #9577] CVE-2013-0214: Fix potential XSRF in SWAT.

 http://www.samba.org/samba/history/samba-3.6.12.html

Samba 3.6.11
------------------------

* Release Notes for Samba 3.6.11
* January 21, 2013

===============================
This is is the latest stable release of Samba 3.6.
===============================

------------------------

Major enhancements in Samba 3.6.11 include:=
===============================

------------------------

* defer_open is triggered multiple times on the same request [https://bugzilla.samba.org/show_bug.cgi?id=9196 bug #9196].
* Fix SEGV wh_n using second vfs module [https://bugzilla.samba.org/show_bug.cgi?id=9471 bug #9471].

Changes since 3.6.10:=
===============================

------------------------

* Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9196 bug #9196] defer_open is triggered multiple times on the same request.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9550 bug #9550] Mask off signals the correct way from the signal handler.
* Björn Baumbach <bb@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9569 bug #9569] ntlm_auth.1: Fix format and make examples visible.
* Tsukasa Hamano <hamano@osstech.co.jp>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9471 bug #9471] Fix SEGV when using second vfs module.
* Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9548 bug #9548] Correctly detect O_DIRECT.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9546 bug #9546] Fix aio_suspend detection on FreeBSD.

 http://www.samba.org/samba/history/samba-3.6.11.html

Samba 3.6.10 
------------------------

* Release Notes for Samba 3.6.10
* December 10, 2012

===============================
This is is the latest stable release of Samba 3.6.
===============================

------------------------

Major enhancements in Samba 3.6.10 include:=
===============================

------------------------

* Respond correctly to FILE_STREAM_INFO requests [https://bugzilla.samba.org/show_bug.cgi?id=9460 bug #9460].
* Fix segfault when "default devmode" is disabled [https://bugzilla.samba.org/show_bug.cgi?id=9433 bug #9433].
* Fix segfaults in "log level = 10" on Solaris [https://bugzilla.samba.org/show_bug.cgi?id=9390 bug #9390].

 http://www.samba.org/samba/history/samba-3.6.10.html

Samba 3.6.9 
------------------------

* Release Notes for Samba 3.6.9
* Oct 29, 2012

===============================
This is is the latest stable release of Samba 3.6
===============================

------------------------

Major enhancements in Samba 3.6.9 include:=
===============================

------------------------

* When setting a non-default ACL, don't forget to apply masks to SMB_ACL_USER and SMB_ACL_GROUP entries [https://bugzilla.samba.org/show_bug.cgi?id=9236 bug #9236].
* Winbind can't fetch user or group info from AD via LDAP [https://bugzilla.samba.org/show_bug.cgi?id=9147 bug #9147].
* Fix segfault in smbd if user specified ports out for range [https://bugzilla.samba.org/show_bug.cgi?id=9218 bug #9218].

 http://www.samba.org/samba/history/samba-3.6.9.html

Samba 3.6.8 
------------------------

* Release Notes for Samba 3.6.8
* Sep 17, 2012

===============================
This is is the latest stable release of Samba 3.6
===============================

------------------------

Major enhancements in Samba 3.6.8 include:=
===============================

------------------------

* Fix crash bug in smbd caused by a blocking lock followed by close [https://bugzilla.samba.org/show_bug.cgi?id=9084 bug #9084].
* Fix Winbind panic if we couldn't find the domain [https://bugzilla.samba.org/show_bug.cgi?id=9135 bug #9135].

 http://www.samba.org/samba/history/samba-3.6.8.html

Samba 3.6.7 
------------------------

* Release Notes for Samba 3.6.7
* Aug 6, 2012

===============================
This is is the latest stable release of Samba 3.6
===============================

------------------------

Major enhancements in Samba 3.6.7 include:=
===============================

------------------------

*  Fix resolving our own "Domain Local" groups [https://bugzilla.samba.org/show_bug.cgi?id=9052 bug #9052].
*  Fix migrating printers while upgrading from 3.5.x [https://bugzilla.samba.org/show_bug.cgi?id=9026 bug #9026].

 http://www.samba.org/samba/history/samba-3.6.7.html

Samba 3.6.6 
------------------------

* Release Notes for Samba 3.6.6
* June 25, 2012

===============================
This is the latest stable release of Samba 3.6
===============================

------------------------

Major enhancements in Samba 3.6.6 include:=
===============================

------------------------

* Fix possible memory leaks in the Samba master process [https://bugzilla.samba.org/show_bug.cgi?id=8970 bug #8970].
* Fix uninitialized memory read in talloc_free().
* Fix joining of XP Pro workstations to 3.6 DCs [https://bugzilla.samba.org/show_bug.cgi?id=8373 bug #8373].

 http://www.samba.org/samba/history/samba-3.6.6.html

Samba 3.6.5 
------------------------

* Release Notes for Samba 3.6.5
* April 30, 2012

===============================
This is a security release in order to address CVE-2012-2111:
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-2111 CVE-2012-2111]:
* Incorrect permission checks when granting/removing privileges can compromise file server security.
* Samba 3.4.x to 3.6.4 are affected by a vulnerability that allows arbitrary users to modify privileges on a file server.

Samba 3.6.4 
------------------------

* Release Notes for Samba 3.6.4
* April 10, 2012

===============================
This is a security release in order to address CVE-2012-1182:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-1182 CVE-2012-1182]:
*  Samba 3.0.x to 3.6.3 are affected by a vulnerability that allows remote code execution as the "root" user.
* * Bug [https://bugzilla.samba.org/show_bug.cgi?id=8815 8815]: PIDL based autogenerated code allows overwriting beyond of allocated array (CVE-2012-1182).

Samba 3.6.3 
------------------------

* Release Notes for Samba 3.6.3
* January 29, 2012

===============================
This is a security release in order to address CVE-2012-0817.
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-0817 CVE-2012-0817]:
*   The Samba File Serving daemon (smbd) in Samba versions 3.6.0 to 3.6.2 is affected by a memory leak that can cause a server denial of service.

 http://www.samba.org/samba/history/samba-3.6.3.html

Samba 3.6.2 
------------------------

* Release Notes for Samba 3.6.2
* January 25, 2012

===============================
This is the latest stable release of Samba 3.6
===============================

------------------------

Major enhancements in Samba 3.6.2 include:=
===============================

------------------------

* Make Winbind receive user/group information [https://bugzilla.samba.org/show_bug.cgi?id=8371 bug #8371].
* Several SMB2 fixes.
* Release Bug 3.6.2 [https://bugzilla.samba.org/show_bug.cgi?id=8595 bug#8595].

 http://www.samba.org/samba/history/samba-3.6.1.html

Samba 3.6.1 
------------------------

* Release Notes for Samba 3.6.1
* October 20, 2011

===============================
This is the latest stable release of Samba 3.6
===============================

------------------------

Major enhancements in Samba 3.6.1 include:=
===============================

------------------------

*  Fix smbd crashes triggered by Windows XP clients [https://bugzilla.samba.org/show_bug.cgi?id=8384 bug #8384].
*  Fix a Winbind race leading to 100% CPU load [https://bugzilla.samba.org/show_bug.cgi?id=8409 bug #8409].
*  Several SMB2 fixes.
*  The VFS ACL modules are no longer experimental but production-ready.

 http://www.samba.org/samba/history/samba-3.6.1.html

Samba 3.6.0 
------------------------

* Release Notes for Samba 3.6.0
* August 9, 2011

===============================
This is the first release of Samba 3.6.0.
===============================

------------------------

Major enhancements in Samba 3.6.0 include:=
===============================

------------------------

===============================
Changed security defaults==
===============================

------------------------

Samba 3.6 has adopted a number of improved security defaults that will impact on existing users of Samba.

 client ntlmv2 auth = yes
 client use spnego principal = no
 send spnego principal = no

The impact of 'client ntlmv2 auth = yes' is that by default we will not use NTLM authentication as a client.  This applies to the Samba client tools such as smbclient and winbind, but does not change the separately released in-kernel CIFS client.  To re-enable the poorer NTLM encryption set '--option=clientusentlmv2auth=no' on your smbclient command line, or set 'client ntlmv2 auth = no' in your smb.conf

The impact of 'client use spnego principal = no' is that Samba will use CIFS/hostname to obtain a kerberos ticket, acting more like Windows when using Kerberos against a CIFS server in smbclient, winbind and other Samba client tools.  This will change which servers we will successfully negotiate kerberos connections to.  This is due to Samba no longer trusting a server-provided hint which is not available from Windows 2008 or later.  For correct operation with all clients, all aliases for a server should be recorded as a as a servicePrincipalName on the server's record in AD.  (For this reason, this behavior change and parameter was also made in Samba 3.5.9)

The impact of 'send spnego principal = no' is to match Windows 2008 and not to send this principal, making existing clients give more consistent behaviour (more likely to fall back to NTLM ) between Samba and
Windows 2008, and between Windows versions that did and no longer use this insecure hint.

===============================
SMB2 support==
===============================

------------------------

SMB2 support in 3.6.0 is fully functional (with one omission), and can be enabled by setting:

max protocol = SMB2

in the [global] section of your smb.conf and re-starting Samba. All features should work over SMB2 except the modification of user quotas using the Windows quota management tools.

As this is the first release containing what we consider to be a fully featured SMB2 protocol, we are not enabling this by default, but encourage users to enable SMB2 and test it. Once we have enough confirmation from Samba users and OEMs that SMB2 support is stable in wide user testing we will enable SMB2 by default in a future Samba release.

===============================
Internal Winbind passdb changes==
===============================

------------------------

Winbind has been changed to use the internal samr and lsa rpc pipe to get local user and group information instead of calling passdb functions. The reason is to use more of our infrastructure and test this infrastructure by using it. With this approach more code in Winbind is shared.

===============================
New Spoolss code==
===============================

------------------------

The spoolss and the old RAP printing code have been completely overhauled and refactored.

All calls from lanman/printing code has been changed to go through the spoolss RPC interfaces, this allows us to keep all checks in one place and avoid special cases in the main printing code. Printing code has been therefore confined within the spoolss code.

All the printing code, including the spoolss RPC interfaces has been changed to use the winreg RPC interfaces to store all data. All data has been migrated from custom, arbitrary TDB files to the registry interface. This transition allow us to present correct data to windows client accessing the server registry through the winreg RPC interfaces to query for printer data. Data is served out from a real registry implementation and therefore arguably 100% forward compatible.

Migration code from the previous TDB files formats is provided. This code is automatically invoked the first time the new code is run on the server. Although manual migration is also available using the 'net printer migrate' command.

These changes not only make all the spoolss code much more closer to "the spec", it also greatly improves our internal testing of both spoolss and winreg interfaces, and reduces overall code duplication.

As part of this work, new tests have been also added to increase coverage.

This code will also allow, in future, an easy transition to split out the spooling functions into a separate daemon for those OEMs that do not need printing functionality in their appliances, reducing the code footprint.

===============================
ID Mapping Changes==
===============================

------------------------

The id mapping configuration has been a source of much grief in the past. For this release, id mapping has been rewritten yet again with the goal of making the configuration more simple and more coherent while keeping the needed flexibility and even adding to the flexibility in some respects.

The major change that implies the configuration simplifications is at the heart of the id mapping system: The separation of the "idmap alloc system" that is responsible for the unix id counters in the tdb, tdb2 and ldap idmap backends from the id mapping code itself has been removed. The sids_to_unixids operation is now atomic and encapsulates (if needed) the action of allocating a unix id for a mapping that is to be created. Consequently all idmap alloc configuration parameters have vanished and it is hence now also not possible any more to specify an idmap alloc backend different from the idmap backend. Each idmap backend uses its own idmap unixid creation mechanism transparently.

As a consequence of the id mapping changes, the methods that are used for storing and deleting id mappings have been removed from the winbindd API. The "net idmap dump/restore" commands have been rewritten to not speak through winbindd any more but directly act on the databases. This is currently available for the tdb and tdb2 backends, the implementation for ldap still missing.

The allocate_id functionality is preserved for the unix id creator of the default idmap configuration is also used as the source of unix ids for the group mapping database and for the posix attributes in a ldapsam: ditposix setup.

As part of the changes, the default idmap configuration has been changed to be more coherent with the per-domain configuration. The parameters "idmap uid", "idmap gid" and "idmap range" are now deprecated in favour of the systematic "idmap config * : range" and "idmap config * : backend" parameters. The reason for this change is that the old options only provided an incomplete and hence deceiving backwards compatibility, which was a source of many problems with upgrades. By introducing this change in configuration, it should be brought to the conciousness of the users that even the simple id mapping is not working exactly as in Samba 3.0 versions any more.

===============================
`Endpoint_Mapper|Endpoint Mapper`==
===============================

------------------------

As Microsoft is more and more relying on endpoint mapper and we didn't have a complete implementation we decided to create an instance for Samba. The endpoint mapper is like a DNS server but for ports. If you want to talk to a
certain RPC service over TCP/IP, you just ask the endpoint mapper on which port it is running. Then you can connect to the service and make sure that it is running.

The code is deactivated by default, because it needs more testing and it doesn't scale yet. If you want to enable and test the endpoint mapper you can set "rpc_server: pmapper = daemon" in the smb.conf file.

===============================
Internal restructuring==
===============================

------------------------

Ongoing internal restructuring for better separation of internal subsystem to achieve a faster build, smaller binaries and cleaner dependencies for the samba3 waf build.

===============================
SMB Traffic Analyzer==
===============================

------------------------

Added the new SMB Traffic Analyzer (SMBTA) VFS module protocol 2 featuring encryption, multiple arguments, and easier parseability. A new tool 'smbta-util' has been created to control the encryption behaviour of SMBTA. For compatibility, SMBTA by default operates on version 1. There are programs consuming the data that the module sends.

More information can be found on http://holger123.wordpress.com/smb-traffic-analyzer/

===============================
NFS quota backend on Linux==
===============================

------------------------

A new nfs quota backend for Linux has been added that is based on the existing Solaris/FreeBSD implementation. This allows samba to communicate correct diskfree information for nfs imports that are re-exported as samba shares.

===============================
Changes
===============================

------------------------

smb.conf changes=
===============================

------------------------

   Parameter Name                      Description     Default
   --------------                      -----------     -------
   async smb echo handler	       New	       No
   [http://www.samba.org/samba/docs/man/manpages-3/smb.conf.5.html#CLIENTNTLMV2AUTH client ntlmv2 auth]           Changed Default Yes
   client use spnego principal	       New	       No
   ctdb locktime warn threshold	       New	       0
   idmap alloc backend		       Removed
   log writeable files on exit	       New	       No
   multicast dns register	       New	       Yes
   ncalrpc dir			       New
   send spnego principal	       New	       No
   smb2 max credits		       New	       128
   smb2 max read		       New	       1048576
   smb2 max trans		       New	       1048576
   smb2 max write		       New	       1048576
   username map cache time	       New	       0
   winbind max clients		       New	       200

* The variable substitutions for %i and %I no longer use IPv4 addresses mapped to IPv6, e.g. '::ffff:192.168.0.1', if the host has IPv6 enabled. Now %i and %I contain just '192.168.0.1'.

Commit Highlights=
===============================

------------------------

*Michael Adam <obnox@samba.org>
** ID Mapping changes.

*Jeremy Allison <jra@samba.org>
** Implement SMB2 support.

*Stefan Metzmacher <metze@samba.org>
** Implement SMB2 support.

*Andreas Schneider <asn@samba.org>
** Add an Endpoint Mapper daemon.

----
`Category:Release Notes`