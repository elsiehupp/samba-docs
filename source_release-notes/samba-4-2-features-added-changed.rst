Samba 4.2 Features added/changed
    <namespace>0</namespace>
<last_edited>2019-09-17T22:03:56Z</last_edited>
<last_editor>Fraz</last_editor>

Samba 4.2 is `Samba_Release_Planning#Discontinued_.28End_of_Life.29|**Discontinued (End of Life)**`.
Samba 4.2.14
------------------------

* Notes for Samba 4.2.14
 07, 2016

===============================
This is a security release in order to address the following defect:

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2119 CVE-2016-2119] (Client side SMB2/3 required signing can be downgraded)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2119 CVE-2016-2119]::
It's possible for an attacker to downgrade the required signing for an SMB2/3 client connection, by injecting the SMB2_SESSION_FLAG_IS_GUEST or SMB2_SESSION_FLAG_IS_NULL flags.

This means that the attacker can impersonate a server being connected to by Samba, and return malicious results.

The primary concern is with winbindd, as it uses DCERPC over SMB2 when talking to domain controllers as a member server, and trusted domains as a domain controller.  These DCE/RPC connections were intended to protected by the combination of "client ipc signing" and "client ipc max protocol" in their effective default settings ("mandatory" and "SMB3_11").

Additionally, management tools like net, samba-tool and rpcclient use DCERPC over SMB2/3 connections.

By default, other tools in Samba are unprotected, but rarely they are configured to use smb signing, via the "client signing" parameter (the default is "if_required").  Even more rarely the "client max protocol" is set to SMB2, rather than the NT1 default.

If both these conditions are met, then this issue would also apply to these other tools, including command line tools like smbcacls, smbcquota, smbclient, smbget and applications using libsmbclient.

===============================
Changes since 4.2.13:
*  Amitay Isaacs <amitay@gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11705 BUG #11705]: Fix : ts with htons(IPPROTO_RAW) and CVE-2015-8543 (Kernel).
* [https://bugzilla.samba.org/show_bug.cgi?id=11770 BUG #11770]: ctdbDDA:n: For AF_ socket types, protocol is in network order.
*  Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11860 BUG #11860]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2016-2119 CVE-2016-2119]: Fix client : SMB2 signing downgrade.
* [https://bugzilla.samba.org/show_bug.cgi?id=11948 BUG #11948]: TotalSS: rpc response payload more than 0x400000.

 https://www.samba.org/samba/history/samba-4.2.14.html

Samba 4.2.13
------------------------

* Notes for Samba 4.2.13
 17, 2016

===============================
This is a security release in order to address the following bug
===============================

------------------------

Although Samba 4.2 is in the security only mode, the Samba Team decided to ship this very last bug fix release to address some important issues.

===============================
Changes since 4.2.12:

*  Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=10618 BUG #10618]: s3: :: M the :ion of struct dom_sid tmp_sid to function level scope.
* [https://bugzilla.samba.org/show_bug.cgi?id=11959 BUG #11959]: s3: :: k -S:he done label can be jumped to with context == NULL.
*  Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11844 BUG #11844]: smbd:SS:x anS:ssert.
*  Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11910 BUG #11910]: s3:smbd: Fix :s :hentication if signing is mandatory.
* [https://bugzilla.samba.org/show_bug.cgi?id=11912 BUG #11912]: libcliS:uth: Let msr: return talloc'ed empty strings.
* [https://bugzilla.samba.org/show_bug.cgi?id=11914 BUG #11914]: s3:ntlm MakeSS:lm_auth_g:ssion_info() more complete.
* [https://bugzilla.samba.org/show_bug.cgi?id=11927 BUG #11927]: s3:rpcc MakeSS:   SMB_SIGNING_IPC_DEFAULT.

 https://www.samba.org/samba/history/samba-4.2.13.html

Samba 4.2.12
------------------------

* Notes for Samba 4.2.12
 02, 2016

===============================
This is the last bugfix release of Samba 4.2.
===============================

------------------------

This is the last bugfix release of Samba 4.2. Please note that there will be security releases only beyond this point!

This release fixes some regressions introduced by the last security fixes. Please see bug https://bugzilla.samba.org/show_bug.cgi?id=11849 for a list of
bugs addressing these regressions and more information.

===============================
Changes since 4.2.11:
*  Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=10489 BUG #10489]: s3: :: p: FixS:heck for :ting u:g:o entry on a fi:P:h no ACL support.
* [https://bugzilla.samba.org/show_bug.cgi?id=11703 BUG #11703]: s3: :: F: Etimestamp :nding inside SMB2 create.
* [https://bugzilla.samba.org/show_bug.cgi?id=11742 BUG #11742]: lib: : nt:  memory : when old signal action restored.
* [https://bugzilla.samba.org/show_bug.cgi?id=11771 BUG #11771]: lib: : nt:  memory : when old signal action restored.
*  Christian Ambach <ambi@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=6482 BUG #6482]: s3:util:Hsmbget: : recursive :nload.
*  Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11780 BUG #11780]: smbd:SS:ly ch dev/inode in open_directory, not the full stat().
* [https://bugzilla.samba.org/show_bug.cgi?id=11789 BUG #11789]: build:S:ark ex dependencies on pytalloc-util.
*  Ralph Boehme <slow@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11714 BUG #11714]: libSSLL:ket: Work ar sockets not supporting FIONREAD.
*  Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11789 BUG #11789]: libsmbS:ysmb: add pyt:SHHutil dependency to fix the build.
*  Berend De Schouwer <berend.de.schouwer@gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11643 BUG #11643]: docs:SS:d exa for domain logins to smbspool man page.
*  Nathan Huff <nhuff@acm.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11771 BUG #11771]: Fix :  handling for Solaris event ports.
*  Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11732 BUG #11732]: param:S:ix str: to accept ";" again.
* [https://bugzilla.samba.org/show_bug.cgi?id=11816 BUG #11816]: nwrap:S:ix the:build on Solaris.
* [https://bugzilla.samba.org/show_bug.cgi?id=11827 BUG #11827]: Fix : ak.
*  Justin Maggard <jmaggard10@gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11773 BUG #11773]: s3:smbd: Add :  :  arch detection for OSX.
*  Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11742 BUG #11742]: tevent::version :DOOTT28. Fix memory leak when old signal action restored.
* [https://bugzilla.samba.org/show_bug.cgi?id=11789 BUG #11789]: s3:wscr pylibsmb:depends:on pycredentials.
* [https://bugzilla.samba.org/show_bug.cgi?id=11841 BUG #11841]: Fix :TATUS_ACCESS_DENIED when accessing Windows public share.
* [https://bugzilla.samba.org/show_bug.cgi?id=11847 BUG #11847]: Only :idate MIC if "map to guest" is not being used.
* [https://bugzilla.samba.org/show_bug.cgi?id=11849 BUG #11849]: authSSL:mssp: Add ntl:nt,server}:force_old_spnego option : testing.
* [https://bugzilla.samba.org/show_bug.cgi?id=11850 BUG #11850]: NetAPPS:MB servers don't negotiate NTLM _SIGN.
* [https://bugzilla.samba.org/show_bug.cgi?id=11858 BUG #11858]: AllowSS:onymous smb connections.
* [https://bugzilla.samba.org/show_bug.cgi?id=11870 BUG #11870]: Fix :sasl_spnego_gensec_bind(KRB5).
* [https://bugzilla.samba.org/show_bug.cgi?id=11872 BUG #11872]: Fix :nfo -u' and 'net ads search'.
*  Jose A. Rivera <jarrpa@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11727 BUG #11727]: s3:smbd SkipSS:dund c to file_set_dosmode when creating a new file.
*  Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11690 BUG #11690]: docs:SS:d smb:_wrapper manpage.
*  Jorge Schrauwen <sjorge@blackdot.be>
* [https://bugzilla.samba.org/show_bug.cgi?id=11816 BUG #11816]: configu Don't c for inotify on illumos.
*  Martin Schwenke <martin@meltin.net>
* [https://bugzilla.samba.org/show_bug.cgi?id=11719 BUG #11719]: ctdbDDA:ts: Drop us: of "smbcontrol winbindd ip-dropped ...".
*  Uri Simchoni <uri@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11852 BUG #11852]: libads::Record  expiry for spnego sasl binds.
*  Hemanth Thummala <hemanth.thummala@nutanix.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11708 BUG #11708]: loadpar: EFix mem leak issue.
* [https://bugzilla.samba.org/show_bug.cgi?id=11740 BUG #11740]: Real :ory leak(buildup) issue in loadparm.
*  Jelmer Vernooij <jelmer@jelmer.uk>
* [https://bugzilla.samba.org/show_bug.cgi?id=11771 BUG #11771]: tevent::Only se: public headers field when installing as a public library.

 https://www.samba.org/samba/history/samba-4.2.12.html

Samba 4.2.11
------------------------

* Notes for Samba 4.2.11
 12, 2016

This is a security release containing one additional regression fix for the security release 4.2.10.

This fixes a regression that prevents things like 'net ads join' from working against a Windows 2003 domain.

===============================
Changes since 4.2.10:

*  Stefan Metzmacher <metze@samba.org>
*  [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016

Samba 4.2.10
------------------------

* Notes for Samba 4.2.10
 12, 2016

===============================
This is a security release in order to address the following CVEs:

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5370 CVE-2015-5370] (Multiple errors in DCE-RPC code)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2110 CVE-2016-2110] (Man in the middle attacks possible with NTLM )
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2111 CVE-2016-2111] (NETLOGON Spoofing Vulnerability)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2112 CVE-2016-2112] (LDAP client and server don't enforce integrity)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2113 CVE-2016-2113] (Missing TLS certificate validation)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2114 CVE-2016-2114] ("server signing = mandatory" not enforced)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2115 CVE-2016-2115] (SMB IPC traffic is not integrity protected)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2118 CVE-2016-2118] (SAMR and LSA man in the middle attacks possible)

The number of changes are rather huge for a security release, compared to typical security releases.

Given the number of problems and the fact that they are all related to man in the middle attacks we decided to fix them all at once instead of splitting them.

In order to prevent the man in the middle attacks it was required to change the (default) behavior for some protocols. Please see the "New smb.conf options" and "Behavior changes" sections below.

===============================
Details
===============================

------------------------

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5370 CVE-2015-5370]=
===============================

------------------------

Versions of Samba from 3.6.0 to 4.4.0 inclusive are vulnerable to denial of service attacks (crashes and high cpu consumption) in the DCE-RPC client and server implementations. In addition, errors in validation of the DCE-RPC packets can lead to a downgrade of a secure connection to an insecure one.

While we think it is unlikely, there's a nonzero chance for a remote code execution attack against the client components, which are used by smbd, winbindd and tools like net, rpcclient and others. This may gain root access to the attacker.

The above applies all possible server roles Samba can operate in.

Note that versions before 3.6.0 had completely different marshalling functions for the generic DCE-RPC layer. It's quite possible that that code has similar problems!

The downgrade of a secure connection to an insecure one may allow an attacker to take control of Active Directory object handles created on a connection created from an Administrator account and re-use them on the now non-privileged connection, compromising the security of the Samba AD-DC.

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2110 CVE-2016-2110]:====:

There are several man in the middle attacks possible with NTLM  authentication.

E.g. NTLM _NEGOTIATE_SIGN and NTLM _NEGOTIATE_SEAL can be cleared by a man in the middle.

This was by protocol design in earlier Windows versions.

Windows Server 2003 RTM and Vista RTM introduced a way to protect against the trivial downgrade.

See MsvAvFlags and flag 0x00000002 in
   https://msdn.microsoft.com/en-us/library/cc236646.aspx

This new feature also implies support for a mechlistMIC when used within SPNEGO, which may prevent downgrades from other SPNEGO mechs, e.g. Kerberos, if sign or seal is finally negotiated.

The Samba implementation doesn't enforce the existence of required flags, which were requested by the application layer, e.g. LDAP or SMB1 encryption (via the unix extensions). As a result a man in the middle can take over the connection. It is also possible to misguide client and/or server to send unencrypted traffic even if encryption was explicitly requested.

LDAP (with NTLM  authentication) is used as a client by various admin tools of the Samba project, e.g. "net", "samba-tool", "ldbsearch", "ldbedit", ...

As an active directory member server LDAP is also used by the winbindd service when connecting to domain controllers.

Samba also offers an LDAP server when running as active directory domain controller.

The NTLM  authentication used by the SMB1 encryption is protected by smb signing, see CVE-2015-5296.

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2111 CVE-2016-2111]:====:

It's basically the same as CVE-2015-0005 for Windows:

 NETLOGON service in Microsoft Windows Server 2003 SP2, Windows Server 2008 SP2 and R2 SP1, and Windows Server 2012 Gold and R2, when a Domain Controller is configured, allows remote attackers to spoof the computer name of a secure channel's endpoint, and obtain sensitive session information, by running a crafted application and leveraging the ability to sniff network traffic, aka "NETLOGON Spoofing Vulnerability".

The vulnerability in Samba is worse as it doesn't require credentials of a computer account in the domain.

This only applies to Samba running as classic primary domain controller, classic backup domain controller or active directory domain controller.

The security patches introduce a new option called "raw NTLMv2 auth" ("yes" or "no") for the [global] section in smb.conf. Samba (the smbd process) will reject client using raw NTLMv2 without using NTLM .

Note that this option also applies to Samba running as standalone server and member server.

You should also consider using "lanman auth = no" (which is already the default) and "ntlm auth = no". Have a look at the smb.conf manpage for further details, as they might impact compatibility with older clients. These also apply for all server roles.

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2112 CVE-2016-2112]:====:

Samba uses various LDAP client libraries, a builtin one and/or the system ldap libraries (typically openldap).

As active directory domain controller Samba also provides an LDAP server.

Samba takes care of doing SASL (GSS-SPNEGO) authentication with Kerberos or NTLM  for LDAP connections, including possible integrity (sign) and privacy (seal) protection.

Samba has support for an option called "client ldap sasl wrapping" since version 3.2.0. Its default value has changed from "plain" to "sign" with version 4.2.0.

Tools using the builtin LDAP client library do not obey the "client ldap sasl wrapping" option. This applies to tools like: "samba-tool", "ldbsearch", "ldbedit" and more. Some of them have command line options like "--sign" and "--encrypt". With the security update they will also obey the "client ldap sasl wrapping" option as default.

In all cases, even if explicitly request via "client ldap sasl wrapping", "--sign" or "--encrypt", the protection can be downgraded by a man in the middle.

The LDAP server doesn't have an option to enforce strong authentication yet. The security patches will introduce a new option called "ldap server require strong auth", possible values are "no", "allow_sasl_over_tls" and "yes".

As the default behavior was as "no" before, you may have to explicitly change this option until all clients have been adjusted to handle LDAP_STRONG_AUTH_REQUIRED errors. Windows clients and Samba member servers already use integrity protection.

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2113 CVE-2016-2113]:====:

Samba has support for TLS/SSL for some protocols: ldap and http, but currently certificates are not validated at all. While we have a "tls cafile" option, the configured certificate is not used to validate the server certificate.

This applies to ldaps:// connections triggered by tools like: :bsearch", "ldbedit" and more. Note that it only applies to the ldb tools when they are built as part of Samba or with Samba extensions installed, which means the Samba builtin LDAP client library is used.

It also applies to dcerpc client connections using ncacn_http (with https://), which are only used by the openchange project. Support for ncacn_http was introduced in version 4.2.0. 

The security patches will introduce a new option called "tls verify peer". Possible values are "no_check", "ca_only", "ca_and_name_if_available", "ca_and_name" and "as_strict_as_possible". 

If you use the self-signed certificates which are auto-generated by Samba, you won't have a crl file and need to explicitly set "tls verify peer = ca_and_name".

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2114 CVE-2016-2114]=
===============================

------------------------

Due to a regression introduced in Samba 4.0.0, an explicit "server signing = mandatory" in the [global] section of the smb.conf was not enforced for clients using the SMB1 protocol.

As a result it does not enforce smb signing and allows man in the middle attacks.

This problem applies to all possible server roles: standalone server, member server, classic primary domain controller, classic backup domain controller and active directory domain controller.

In addition, when Samba is configured with "server role = active directory domain controller" the effective default for the "server signing" option should be "mandatory".

During the early development of Samba 4 we had a new experimental file server located under source4/smb_server. But before the final 4.0.0 release we switched back to the file server under source3/smbd.

But the logic for the correct default of "server signing" was not ported correctly ported.

Note that the default for server roles other than active directory domain controller, is "off" because of performance reasons.

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2115 CVE-2016-2115]:====:

Samba has an option called "client signing", this is turned off by default for performance reasons on file transfers.

This option is also used when using DCERPC with ncacn_np.

In order to get integrity protection for ipc related communication by default the "client ipc signing" option is introduced. The effective default for this new option is "mandatory".

In order to be compatible with more SMB server implementations, the following additional options are introduced:
*   "client ipc min protocol" ("NT1" by default) and
*   "client ipc max protocol" (the highest support SMB2/3 dialect by default).
 options overwrite the "client min protocol" and "client max protocol" options, because the default for "client max protocol" is still "NT1". The reason for this is the fact that all SMB2/3 support SMB signing, while there are still SMB1 implementations which don't offer SMB signing by default (this includes Samba versions before 4.0.0). 

Note that winbindd (in versions 4.2.0 and higher) enforces SMB signing against active directory domain controllers despite of the "client signing" and "client ipc signing" options.

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2118 CVE-2016-2118] (a.k.a. BADLOCK):====:

The Security Account Manager Remote Protocol [MS-SAMR] and the Local Security Authority (Domain Policy) Remote Protocol [MS-LSAD] are both vulnerable to man in the middle attacks. Both are application level protocols based on the generic DCE 1.1 Remote Procedure Call (DCERPC) protocol.

These protocols are typically available on all Windows installations as well as every Samba server. They are used to maintain the Security Account Manager Database. This applies to all roles, e.g. standalone, domain member, domain controller.

Any authenticated DCERPC connection a client initiates against a server can be used by a man in the middle to impersonate the authenticated user against the SAMR or LSAD service on the server.

The client chosen application protocol, auth type (e.g. Kerberos or NTLM ) and auth level (NONE, CONNECT, PKT_INTEGRITY, PKT_PRIVACY) do not matter in this case. A man in the middle can change auth level to CONNECT (which means authentication without message protection) and take over the connection.

As a result, a man in the middle is able to get read/write access to the Security Account Manager Database, which reveals all password sand any other potential sensitive information.

Samba running as an active directory domain controller is additionally missing checks to enforce PKT_PRIVACY for the Directory Replication Service Remote Protocol [MS-DRSR] (drsuapi) and the BackupKey Remote Protocol [MS-BKRP] (backupkey). The Domain Name Service Server Management Protocol [MS-DNSP] (dnsserver) is not enforcing at least PKT_INTEGRITY.

===============================
New smb.conf options
===============================

------------------------

allow dcerpc auth level connect (G)=
===============================

------------------------

This option controls whether DCERPC services are allowed to be used with DCERPC_AUTH_LEVEL_CONNECT, which provides authentication, but no per message integrity nor privacy protection.

Some interfaces like samr, lsarpc and netlogon have a hard-coded default of no and epmapper, mgmt and rpcecho have a hard-coded default of yes.

The behavior can be overwritten per interface name (e.g. lsarpc, netlogon, samr, srvsvc, winreg, wkssvc ...) by using 'allow dcerpc auth level connect: = yes' as option.

This option yields precedence to the implementation specific restrictions. E.g. the drsuapi and backupkey protocols require DCERPC_AUTH_LEVEL_PRIVACY. The dnsserver protocol requires DCERPC_AUTH_LEVEL_INTEGRITY.

    Default: allow dcerpc auth level connect = no
    Example: allow dcerpc auth level connect = yes

client ipc signing (G)=
===============================

------------------------

This controls whether the client is allowed or required to use SMB signing for IPC$ connections as DCERPC transport. Possible values are auto, mandatory and disabled.

When set to mandatory or default, SMB signing is required. When set to auto, SMB signing is offered, but not enforced and if set to disabled, SMB signing is not offered either.

Connections from winbindd to Active Directory Domain Controllers always enforce signing.

    Default: client ipc signing = default

client ipc max protocol (G)=
===============================

------------------------

The value of the parameter (a string) is the highest protocol level that will be supported for IPC$ connections as DCERPC transport.

Normally this option should not be set as the automatic negotiation phase in the SMB protocol takes care of choosing the appropriate protocol.

The value default refers to the latest supported protocol, currently SMB3_11.

See client max protocol for a full list of available protocols. The values CORE, COREPLUS, LANMAN1, LANMAN2 are silently upgraded to NT1.

    Default: client ipc max protocol = default
    Example: client ipc max protocol = SMB2_10

client ipc min protocol (G)=
===============================

------------------------

This setting controls the minimum protocol version that the will be attempted to use for IPC$ connections as DCERPC transport.

Normally this option should not be set as the automatic negotiation phase in the SMB protocol takes care of choosing the appropriate protocol.

The value default refers to the higher value of NT1 and the effective value of "client min protocol".

See client max protocol for a full list of available protocols. The values CORE, COREPLUS, LANMAN1, LANMAN2 are silently upgraded to NT1.

    Default: client ipc min protocol = default
    Example: client ipc min protocol = SMB3_11

ldap server require strong auth (G)=
===============================

------------------------

The ldap server require strong auth defines whether the ldap server requires ldap traffic to be signed or signed and encrypted (sealed). Possible values are no, allow_sasl_over_tls and yes.

A value of no allows simple and sasl binds over all transports.

A value of allow_sasl_over_tls allows simple and sasl binds (without sign or seal) over TLS encrypted connections. Unencrypted connections only allow sasl binds with sign or seal.

A value of yes allows only simple binds over TLS encrypted connections. Unencrypted connections only allow sasl binds with sign or seal.

    Default: ldap server require strong auth = yes

raw NTLMv2 auth (G)=
===============================

------------------------

This parameter determines whether or not smbd(8) will allow SMB1 clients without extended security (without SPNEGO) to use NTLMv2 authentication.

If this option, lanman auth and ntlm auth are all disabled, then only clients with SPNEGO support will be permitted. That means NTLMv2 is only supported within NTLM .

    Default: raw NTLMv2 auth = no

tls verify peer (G)=
===============================

------------------------

This controls if and how strict the client will verify the peer's certificate and name. Possible values are (in increasing order): no_check, ca_only, ca_and_name_if_available, ca_and_name and as_strict_as_possible.

When set to no_check the certificate is not verified at all, which allows trivial man in the middle attacks.

When set to ca_only the certificate is verified to be signed from a ca specified in the "tls ca file" option. Setting "tls ca file" to a valid file is required. The certificate lifetime is also verified. If the "tls crl file" option is configured, the certificate is also verified against the ca crl.

When set to ca_and_name_if_available all checks from ca_only are performed. In addition, the peer hostname is verified against the certificate's name, if it is provided by the application layer and not given as an ip address string.

When set to ca_and_name all checks from ca_and_name_if_available are performed. In addition the peer hostname needs to be provided and even an ip address is checked against the certificate's name.

When set to as_strict_as_possible all checks from ca_and_name are performed. In addition the "tls crl file" needs to be configured. Future versions of Samba may implement additional checks.

    Default: tls verify peer = as_strict_as_possible

tls priority (G) (backported from Samba 4.3 to Samba 4.2)=
===============================

------------------------

This option can be set to a string describing the TLS protocols to be supported in the parts of Samba that use GnuTLS, specifically the AD DC.

The default turns off SSLv3, as this protocol is no longer considered secure after CVE-2014-3566 (otherwise known as POODLE) impacted SSLv3 use in HTTPS applications.

The valid options are described in the GNUTLS Priority-Strings documentation at http://gnutls.org/manual/html_node/Priority-Strings.html

    Default: tls priority = NORMAL:-:HHSSL3.0

===============================
Behavior changes
===============================

------------------------

*  The default auth level for authenticated binds has changed from DCERPC_AUTH_LEVEL_CONNECT to DCERPC_AUTH_LEVEL_INTEGRITY. That means ncacn_ip_tcp is now implicitly the same as ncacn_ip_tcp:server[s and offers a similar protection  as ncacn_np:server,  relies on smb signing.

*  The following constraints are applied to SMB1 connections:
** "client lanman auth = yes" is now consistently required for authenticated connections using the SMB1 LANMAN2 dialect.
** "client ntlmv2 auth = yes" and "client use spnego = yes" (both the default values), require extended security (SPNEGO) support from the server. That means NTLMv2 is only used within NTLM .

*  Tools like "samba-tool", "ldbsearch", "ldbedit" and more obey the default of "client ldap sasl wrapping = sign". Even with "client ldap sasl wrapping = plain" they will automatically upgrade to "sign" when getting LDAP_STRONG_AUTH_REQUIRED from the LDAP server.

===============================
Changes since 4.2.9:

*  Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11344 BUG #11344] - [http:/SSLLAAS:TTmitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5370 CVE-2015-5370]: Multiple : in DCE-RPC code.
* [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.
*  Christian Ambach <ambi@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.
*  Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.
*  Ralph Boehme <slow@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11644 BUG #11644] - [http:/SSLLAAS:TTmitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2112 CVE-2016-2112]: The LDA: client and server don't enforce integrity protection.
*  Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11749 BUG #11749] - [http:/SSLLAAS:TTmitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2111 CVE-2016-2111]: NETLOGON : Vulnerability.
* [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.
*  Björn Jacke <bj@sernet.de>
* [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.
o  Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.
o  Kamen Mazdrashki <kamenim@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.
o  Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11344 BUG #11344] - [http:/SSLLAAS:TTmitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5370 CVE-2015-5370]: Multiple : in DCE-RPC code.
* [https://bugzilla.samba.org/show_bug.cgi?id=11616 BUG #11616] - [http:/SSLLAAS:TTmitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2118 CVE-2016-2118]: SAMR an: LSA man in the middle attacks possible.
* [https://bugzilla.samba.org/show_bug.cgi?id=11644 BUG #11644] - [http:/SSLLAAS:TTmitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2112 CVE-2016-2112]: The LDA: client and server doesn't enforce integrity protection.
* [https://bugzilla.samba.org/show_bug.cgi?id=11687 BUG #11687] - [http:/SSLLAAS:TTmitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2114 CVE-2016-2114]: "server :ing = mandatory" not enforced.
* [https://bugzilla.samba.org/show_bug.cgi?id=11688 BUG #11688] - [http:/SSLLAAS:TTmitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2110 CVE-2016-2110]: Man inS:he middle attacks possible with NTLM .
* [https://bugzilla.samba.org/show_bug.cgi?id=11749 BUG #11749] - [http:/SSLLAAS:TTmitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2111 CVE-2016-2111]: NETLOGON : Vulnerability.
* [https://bugzilla.samba.org/show_bug.cgi?id=11752 BUG #11752] - [http:/SSLLAAS:TTmitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2113 CVE-2016-2113]: Missing  certificate validation allows man in the middle attacks.
* [https://bugzilla.samba.org/show_bug.cgi?id=11756 BUG #11756] - [http:/SSLLAAS:TTmitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2115 CVE-2016-2115]: SMB cli connections for IPC traffic are not integrity protected.
* [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.
*  Richard Sharpe <rsharpe@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.
*  Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.
*  Jelmer Vernooij <jelmer@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.

Samba 4.2.9
------------------------

Release Notes for Samba 4.2.9
March 8, 2016

===============================
This is a security release in order to address the following CVEs:

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-7560 CVE-2015-7560] (Incorrect ACL get/set allowed on symlink path)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-0771 CVE-2016-0771] (Out-of-bounds read in internal DNS server)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-7560 CVE-2015-7560]::
 versions of Samba from 3.2.0 to 4.4.0rc3 inclusive are vulnerable to a malicious client overwriting the ownership of ACLs using symlinks.

 authenticated malicious client can use SMB1 UNIX extensions to create a symlink to a file or directory, and then use non-UNIX SMB1 calls to overwrite the contents of the ACL on the file or directory linked to.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-0771 CVE-2016-0771]::
 versions of Samba from 4.0.0 to 4.4.0rc3 inclusive, when deployed as an AD DC and choose to run the internal DNS server, are vulnerable to an out-of-bounds read issue during DNS TXT record handling caused by users with permission to modify DNS records.

* malicious client can upload a specially constructed DNS TXT record, resulting in a remote denial-of-service attack. As long as the affected TXT record remains undisturbed in the Samba database, a targeted DNS query may continue to trigger this exploit.
 unlikely, the out-of-bounds read may bypass safety checks and allow leakage of memory from the server in the form of a DNS TXT reply.

 default only authenticated accounts can upload DNS records, as "allow dns updates = secure only" is the default. Any other value would allow anonymous clients to trigger this bug, which is a much higher risk.

===============================
Changes since 4.2.8:

*  Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11648 BUG #11648]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2015-7560 CVE-2015-7560]: Getting and :ng Windows ACLs on symlinks can change permissions on link target.

*  Garming Sam <garming@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=11128 BUG #11128], [https:/SSLLAAS:a.samba.org/show_bug.cgi?id=11686 BUG #11686] [http://cveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2016-0771 CVE-2016-0771]: Read of :ized memory DNS TXT handling.

*  Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11128 BUG #11128], [https:/SSLLAAS:a.samba.org/show_bug.cgi?id=11686 BUG #11686] [http://cveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2016-0771 CVE-2016-0771]: Read of :ized memory DNS TXT handling.

 https://www.samba.org/samba/history/samba-4.2.9.html

Samba 4.2.8
------------------------

* Notes for Samba 4.2.8
* 2, 2016

===============================
This is the latest stable release of Samba 4.2.
===============================

------------------------

===============================
Changes since 4.2.7:

*  Michael Adam <obnox@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11647 BUG #11647]: s3:smbd: Fix  c case of the symlink verification.
*  Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11624 BUG #11624]: s3: :mb: : initia the list head when keeping a list of primary followed by DFS connections.
* [https://bugzilla.samba.org/show_bug.cgi?id=11625 BUG #11625]: ReduceS:he memory footprint of empty string options.
*  Christian Ambach <ambi@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11400 BUG #11400]: s3:smbd:oplock: : kernel :k setting when releasing oplocks.
*  Ralph Boehme <slow@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11065 BUG #11065]: vfs_fru Fix ren directories with open files.
* [https://bugzilla.samba.org/show_bug.cgi?id=11347 BUG #11347]: Fix :S finder error 36 when copying folder to Samba.
* [https://bugzilla.samba.org/show_bug.cgi?id=11466 BUG #11466]: Fix :ing files with vfs_fruit when using vfs_streams_xattr without stream prefix and type suffix.
* [https://bugzilla.samba.org/show_bug.cgi?id=11645 BUG #11645]: smbd:SS:ke &q: dot files" option work with "store dos attributes = yes".
* [https://bugzilla.samba.org/show_bug.cgi?id=11684 BUG #11684]: s3:smbd: Ignore :alSS:location size for directory creation.
*Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11639 BUG #11639]: libSSLL:c_req: Do notS:nstall async_connect_send_test.
*  Karolin Seeger <kseeger@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11641 BUG #11641]: docs:SS:x typ: Ein man vfs_gpfs.
*  Uri Simchoni <uri@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11682 BUG #11682]: smbcacl: EFix uni: variable.

 https://www.samba.org/samba/history/samba-4.2.8.html

Samba 4.2.7
------------------------

* Notes for Samba 4.2.7
* 16, 2015

===============================
This is a security release in order to address the following CVEs:

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-3223 CVE-2015-3223] (Denial of service in Samba Active Directory server)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5252 CVE-2015-5252] (Insufficient symlink verification in smbd)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5299 CVE-2015-5299] (Missing access control check in shadow copy code)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5296 CVE-2015-5296] (Samba client requesting encryption vulnerable to downgrade attack)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-8467 CVE-2015-8467] (Denial of service attack against Windows Active Directory server)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5330 CVE-2015-5330] (Remote memory read in Samba LDAP server)

Please note that if building against a system libldb, the required version has been bumped to ldb-1.1.24.  This is needed to ensure we build against a system ldb library that contains the fixes for [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5330 CVE-2015-5330] and [http:SSLLAASS:HHcve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-3223 CVE-2015-3223].

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-3223 CVE-2015-3223]::
 versions of Samba from 4.0.0 to 4.3.2 inclusive (resp. all ldb versions up to 1.1.23 inclusive) are vulnerable to a denial of service attack in the samba daemon LDAP server.

* malicious client can send packets that cause the LDAP server in the samba daemon process to become unresponsive, preventing the server from servicing any other requests.

 flaw is not exploitable beyond causing the code to loop expending CPU resources.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5252 CVE-2015-5252]::
 versions of Samba from 3.0.0 to 4.3.2 inclusive are vulnerable to a bug in symlink verification, which under certain circumstances could allow client access to files outside the exported share path.

 a Samba share is configured with a path that shares a common path prefix with another directory on the file system, the smbd daemon may allow the client to follow a symlink pointing to a file or directory in that other directory, even if the share parameter "wide links" is set to "no" (the default).

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5299 CVE-2015-5299]::
 versions of Samba from 3.2.0 to 4.3.2 inclusive are vulnerable to a missing access control check in the vfs_shadow_copy2 module. When looking for the shadow copy directory under the share path the current accessing user should have DIRECTORY_LIST access rights in order to view the current snapshots.

 was not being checked in the affected versions of Samba.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5296 CVE-2015-5296]::
* of Samba from 3.2.0 to 4.3.2 inclusive do not ensure that signing is negotiated when creating an encrypted client connection to a server.

* this a man-in-the-middle attack could downgrade the connection and connect using the supplied credentials as an unsigned, unencrypted connection.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-8467 CVE-2015-8467]::
 operating as an AD DC, is sometimes operated in a domain with a mix of Samba and Windows Active Directory Domain Controllers.

 versions of Samba from 4.0.0 to 4.3.2 inclusive, when deployed as an AD DC in the same domain with Windows DCs, could be used to override the protection against the MS15-096 / CVE-2015-2535 security issue in Windows.

 to MS16-096 it was possible to bypass the quota of machine accounts a non-administrative user could create.  Pure Samba domains are not impacted, as Samba does not implement the SeMachineAccountPrivilege functionality to allow non-administrator users to create new computer objects.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5330 CVE-2015-5330]::
 versions of Samba from 4.0.0 to 4.3.2 inclusive (resp. all ldb versions up to 1.1.23 inclusive) are vulnerable to a remote memory read attack in the samba daemon LDAP server.

* malicious client can send packets that cause the LDAP server in the samba daemon process to return heap memory beyond the length of the requested value.

 memory may contain data that the client should not be allowed to see, allowing compromise of the server.

 memory may either be returned to the client in an error string, or stored in the database by a suitabily privileged user.  If untrusted users can create objects in your database, please confirm that all DN and name attributes are reasonable.

===============================
Changes since 4.2.6:

*  Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11552 BUG #11552]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2015-8467 CVE-2015-8467]: samdb: Match :-096 :viour for userAccountControl.
*  Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11325 BUG #11325]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2015-3223 CVE-2015-3223]: Fix LDAP  search expression attack DoS.
* [https://bugzilla.samba.org/show_bug.cgi?id=11395 BUG #11395]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2015-5252 CVE-2015-5252]: Fix insufficient:symlink verification (file access outside the share).
* [https://bugzilla.samba.org/show_bug.cgi?id=11529 BUG #11529]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2015-5299 CVE-2015-5299]: s3-shadow-c Fix missing acce: Echeck on snapdir.
* Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=11599 BUG #11599]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2015-5330 CVE-2015-5330]: Fix remote : memory exploit in LDB.
*  Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11536 BUG #11536]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2015-5296 CVE-2015-5296]: Add man  the middle protection when forcing smb encryption on the client side.

 https://www.samba.org/samba/history/samba-4.2.7.html

Samba 4.2.6
------------------------

* Notes for Samba 4.2.6
* 08, 2015

===============================
This is the latest stable release of Samba 4.2.
===============================

------------------------

===============================
Changes since 4.2.5:

* Michael Adam <obnox@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11365 BUG #11365]: ctdb:SS:rip t spaces from nodes file.
* [https://bugzilla.samba.org/show_bug.cgi?id=11577 BUG #11577]: ctdb:SS: n th: RO tracking db with perms 0600 instead of 0000.
* [https://bugzilla.samba.org/show_bug.cgi?id=11619 BUG #11619]: doc: : aSS:po in the smb.conf manpage.
* Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11452 BUG #11452]: s3DDAAS: Fix old:DOS client doing wildcard delete - gives a attribute type of zero.
* [https://bugzilla.samba.org/show_bug.cgi?id=11565 BUG #11565]: auth:SS:nsec:  a m leak.
* [https://bugzilla.samba.org/show_bug.cgi?id=11566 BUG #11566]: lib: :l: M non-: message a warning.
* [https://bugzilla.samba.org/show_bug.cgi?id=11589 BUG #11589]: s3: :: I: EA's  turned off on a share don't allow an SMB2 create containing them.
* [https://bugzilla.samba.org/show_bug.cgi?id=11615 BUG #11615]: s3: :: h:pen_below() :ls to enumerate open files below an open directory handle.
* Ralph Boehme <slow@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11564 BUG #11564]: async_r Fix non:ocking connect().
* Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11243 BUG #11243]: vfs_gpf: ERe-enable:share modes.
* [https://bugzilla.samba.org/show_bug.cgi?id=11570 BUG #11570]: smbd:SS:nd SM: Eoplock breaks unencrypted.
* YvanM <yvan.masson@openmailbox.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11584 BUG #11584]: manpage: Correct  typo error.
* Marc Muehlfeld <mmuehlfeld@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=9912 BUG #9912]: Changin: log level of two entries to from 1 to 3.
* Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11346 BUG #11346]: wafsamb: EAlso bu libraries with RELRO protection.
* [https://bugzilla.samba.org/show_bug.cgi?id=11563 BUG #11563]: nss_win: EDo notS:un into use after free issues when we access memory allocated on the globals and the global being reinitialized.
* Karolin Seeger <kseeger@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11619 BUG #11619]: docs:SS:x som: typos in the idmap config section of man 5 smb.conf.
* Noel Power <noel.power@suse.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11569 BUG #11569]: Fix :indd crashes with samlogon for trusted domain user.
* [https://bugzilla.samba.org/show_bug.cgi?id=11597 BUG #11597]: Backpor: some valgrind fixes from upstream master.

 https://www.samba.org/samba/history/samba-4.2.6.html

Samba 4.2.5
------------------------

* Notes for Samba 4.2.5
* 27, 2015

===============================
This is the latest stable release of Samba 4.2.
===============================

------------------------

===============================
Changes since 4.2.4:
*   Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=10252 BUG #10252]: s3: :: F: Eour :ASSHHbased enumeration on "hide unreadable"  to match Windows.
* [https://bugzilla.samba.org/show_bug.cgi?id=10634 BUG #10634]: smbd:SS:x fil: name buflen and padding in notify repsonse.
* [https://bugzilla.samba.org/show_bug.cgi?id=11486 BUG #11486]: s3: :: F: Emkdir  condition.
* [https://bugzilla.samba.org/show_bug.cgi?id=11522 BUG #11522]: s3: :: F: EopeningSSLLA:ing :stream files on t: Eroot share directory.
* [https://bugzilla.samba.org/show_bug.cgi?id=11535 BUG #11535]: s3: :: F: ENULL : bug introduced by previous 'raw' stream fix (bug #11522).
* [https://bugzilla.samba.org/show_bug.cgi?id=11555 BUG #11555]: s3: : lo:) lo for unqualified (no DOMAIN\ component) names is incorrect.
*   Ralph Boehme <slow@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11535 BUG #11535]: s3: :: F: Ea c in unix_convert().
* [https://bugzilla.samba.org/show_bug.cgi?id=11543 BUG #11543]: vfs_fru Return  of ad_pack in vfs_fruit.c.
* [https://bugzilla.samba.org/show_bug.cgi?id=11549 BUG #11549]: Fix : in smbstatus where the lease info is not printed.
* [https://bugzilla.samba.org/show_bug.cgi?id=11550 BUG #11550]: s3:smbs Add : am : to share_entry_forall().
* [https://bugzilla.samba.org/show_bug.cgi?id=11555 BUG #11555]: s3:lib::validate :inS:ame in lookup_wellknown_name().
*   Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11038 BUG #11038]: kerbero: EMake su: Ewe only use prompter type when available.
*   Björn Jacke <bj@sernet.de>
* [https://bugzilla.samba.org/show_bug.cgi?id=10365 BUG #10365]: nss_win Fix han: on Solaris on big groups.
* [https://bugzilla.samba.org/show_bug.cgi?id=11355 BUG #11355]: build:S:se asD:ded linker flag also on OpenBSD.
*   Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11038 BUG #11038]: winbind: Fix 100: loop.
* [https://bugzilla.samba.org/show_bug.cgi?id=11381 BUG #11381]: Fix  deadlock in tdb.
*   Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11316 BUG #11316]: s3:ctdb MakeS:ure  destroy tevent_fd before closing the socket.
* [https://bugzilla.samba.org/show_bug.cgi?id=11327 BUG #11327]: dcerpcD: accept  dcerpc_bind_nak pdus.
*   Har Gagan Sahai <SHarGagan@novell.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11509 BUG #11509]: s3: : Fi: a c when the dfs targets are disabled.
*   Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11502 BUG #11502]: pam_win Fix aSS:gfault if initialization fails.
*   Uri Simchoni <uri@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11528 BUG #11528]: net: : aSS:ash with 'net ads keytab create'.
* [https://bugzilla.samba.org/show_bug.cgi?id=11547 BUG #11547]: vfs_com Set the:fd on open before calling SMB_VFS_FSTAT.

 https://www.samba.org/samba/history/samba-4.2.5.html

Samba 4.2.4
------------------------

* Notes for Samba 4.2.4
* 8, 2015

===============================
This is the latest stable release of Samba 4.2.
===============================

------------------------

===============================
Changes since 4.2.3:
*   Michael Adam <obnox@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11372 BUG #11372]: smbd:SS:x SMB: functionality of "smb encrypt".
*   Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11359 BUG #11359]: lib: :lace:  strsep :on (missing on Solaris).
*   Ralph Boehme <slow@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11278 BUG #11278]: Fix :am names with colon with "fruit: ncoding = :quot;.
* [https://bugzilla.samba.org/show_bug.cgi?id=11317 BUG #11317]: vfs:fru ImplementS:opyfi: Estyle copy_chunk.
* [https://bugzilla.samba.org/show_bug.cgi?id=11426 BUG #11426]: s3DDAAS Use tal array in share allowedusers.
* [https://bugzilla.samba.org/show_bug.cgi?id=11467 BUG #11467]: vfs_fru Handling  empty resource fork.
*   Alexander Bokovoy <ab@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11265 BUG #11265]: authSSL:dentials: If cred have principal set, they are not anonymous anymore.
*   Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11373 BUG #11373]: s3DDAAS: Reset p in smbXsrv_connection_init_tables failure paths.
*   Amitay Isaacs <amitay@gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11398 BUG #11398]: ctdbDDA:n: Return  sequence number for CONTROL_GET_DB_SEQNUM.
* [https://bugzilla.samba.org/show_bug.cgi?id=11431 BUG #11431]: ctdbDDA:n: Improve  handling for running event scripts.
*   Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11316 BUG #11316]: lib: : run of open_socket_out().
* [https://bugzilla.samba.org/show_bug.cgi?id=11488 BUG #11488]: AvoidSS:oting problems in user's DNs.
*   Justin Maggard <jmaggard@netgear.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11320 BUG #11320]: s3DDAAS: Respect :ME_GROUP flag in sid lookup.
*   Roel van Meer <roel@1afa.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11427 BUG #11427]: s3DDAAS: Compare  maximum allowed length of a NetBIOS name.
*   Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11316 BUG #11316]: s3:lib::Fix :ACC: cases of open_socket_out_cleanup().
* [https://bugzilla.samba.org/show_bug.cgi?id=11454 BUG #11454]: Backpor: dcesrv_netr_DsRGetDCNameEx2 fixes.
*   Anubhav Rakshit <anubhav.rakshit@gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11361 BUG #11361]: s3:libs Fix  b: Ein conversion of ea list to ea array.
*   Arvid Requate <requate@univention.de>
* [https://bugzilla.samba.org/show_bug.cgi?id=11291 BUG #11291]: s4:rpc_:AASSHHnetlog Fix for :OTT
*   Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=9862 BUG #9862]: s3DDAAS: Fix &qu to guest = Bad uid".
* [https://bugzilla.samba.org/show_bug.cgi?id=11403 BUG #11403]: s3DDAAS: Leave s: e() if dfree command is used.
* [https://bugzilla.samba.org/show_bug.cgi?id=11404 BUG #11404]: s3DDAAS: Fix aSS:ssible null pointer dereference.
*   Martin Schwenke <martin@meltin.net>
* [https://bugzilla.samba.org/show_bug.cgi?id=11399 BUG #11399]: ctdbDDA:ts: Support :g of interestingly named VLANs on bonds.
* [https://bugzilla.samba.org/show_bug.cgi?id=11432 BUG #11432]: ctdbDDA:n: Check i: updates are in flight when releasing all IPs.
* [https://bugzilla.samba.org/show_bug.cgi?id=11435 BUG #11435]: ctdbDDA:: Fix bui of PCP PMDA module.
*   Wei Zhong <wweyeww@gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=10823 BUG #10823]: s3: :indd:  TALLOC_FREES:f uninitialized groups variable.

 https://www.samba.org/samba/history/samba-4.2.4.html

Samba 4.2.3
------------------------

* Notes for Samba 4.2.3
 14, 2015

===============================
This is the latest stable release of Samba 4.2.
===============================

------------------------

===============================
Changes since 4.2.2:

*   Michael Adam <obnox@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11366 BUG #11366]: docs:SS: rhaul  description of "smb encrypt" to include SMB encryption.
o   Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11068 BUG #11068]: s3: : ut Ensure : readSS: hex number as %x, not %u.
* [https://bugzilla.samba.org/show_bug.cgi?id=11295 BUG #11295]: Excessi: Ecli_resolve_path() usage can slow down transmission.
* [https://bugzilla.samba.org/show_bug.cgi?id=11328 BUG #11328]: winbind: Ewinbindd_raw_ker:n - ensure logon_info exists in PAC.
* [https://bugzilla.samba.org/show_bug.cgi?id=11339 BUG #11339]: s3: :: U: Eseparate : to track become_root()/unbecome_root() state.
* [https://bugzilla.samba.org/show_bug.cgi?id=11342 BUG #11342]: s3: :: C: cras: in do_smb_load_module().
*   Christian Ambach <ambi@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11170 BUG #11170]: s3:para:Hloadparm:SS:x 'testparm :ASSHH-show-all-parameters'.
*   Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=10991 BUG #10991]BUG 10991: winbind: ESync se:Tldb into secrets.tdb on startup.
*   Ralph Boehme <slow@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11277 BUG #11277]BUG 11277: s3:smb2: Add :   last command in compound requests.
* [https://bugzilla.samba.org/show_bug.cgi?id=11305 BUG #11305]BUG 11305: vfs_fru Add opt "veto_appledouble".
* [https://bugzilla.samba.org/show_bug.cgi?id=11323 BUG #11323]BUG 11323: smbdSSL:ns2: Add aSS: ful diagnostic for files with bad encoding.
* [https://bugzilla.samba.org/show_bug.cgi?id=11363 BUG #11363]BUG 11363: vfs_fru Check o and length for AFP_AfpInfo read requests.
* [https://bugzilla.samba.org/show_bug.cgi?id=11371 BUG #11371]BUG 11371: ncacn_h Fix GNU:
*   Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11245 BUG #11245]BUG 11245: s3DDAAS:ver: Fix rpc:pip_sockets() processing of interfaces.
*   Alexander Drozdov <al.drozdov@gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11331 BUG #11331]BUG 11331: tdb: :sion :DOOTT5: ABI change: :hainlock_read_nonblock() : been added.
*   Evangelos Foutras <evangelos@foutrelis.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=8780 BUG #8780]BUG 8780: s4:libS:ls:  build  gnutls 3.4.
*   David Holder <david.holder@erion.co.uk>
* [https://bugzilla.samba.org/show_bug.cgi?id=11281 BUG #11281]BUG 11281: Add : support to ADS client side LDAP connects.
* [https://bugzilla.samba.org/show_bug.cgi?id=11282 BUG #11282]BUG 11282: Add : support for determining FQDN during ADS join.
* [https://bugzilla.samba.org/show_bug.cgi?id=11283 BUG #11283]BUG 11283: s3: : en DNS connections for ADS client.
*   Steve Howells <steve.howells@moscowfirst.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=10924 BUG #10924]BUG 10924: s4DDOOT:HHfsmo.py: Fixed f transfer exception.
*   Amitay Isaacs <amitay@gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11293 BUG #11293]: Fix :lid write in ctdb_lock_context_destructor.
*   Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11218 BUG #11218]: smbd:SS:x aSS: -after-free.
* [https://bugzilla.samba.org/show_bug.cgi?id=11312 BUG #11312]: tstream: Make so: nonblocking.
* [https://bugzilla.samba.org/show_bug.cgi?id=11330 BUG #11330]: tevent::Fix CID:1035381 Unchecked return value.
* [https://bugzilla.samba.org/show_bug.cgi?id=11331 BUG #11331]: tdb: : CID:1034842 and 1034841 Resource leaks.
*   Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11061 BUG #11061]: LogonSS:a MS Remote Desktop hangs.
* [https://bugzilla.samba.org/show_bug.cgi?id=11141 BUG #11141]: tevent::Add aSS:te to tevent_add_fd().
* [https://bugzilla.samba.org/show_bug.cgi?id=11293 BUG #11293]: Fix :lid write in ctdb_lock_context_destructor.
* [https://bugzilla.samba.org/show_bug.cgi?id=11316 BUG #11316]: tevent_: Eneeds to be destroyed before closing the fd.
* [https://bugzilla.samba.org/show_bug.cgi?id=11319 BUG #11319]: BuildSS:ils on Solaris 11 with "â€˜PTHREAD_MUTEX_ROBUSTâ€™ undeclared".
* [https://bugzilla.samba.org/show_bug.cgi?id=11326 BUG #11326]: RobustS:utex support broken in 1.3.5.
* [https://bugzilla.samba.org/show_bug.cgi?id=11329 BUG #11329]: s3:smb2: Fix:memory : in the defer_rename case.
* [https://bugzilla.samba.org/show_bug.cgi?id=11330 BUG #11330]: Backpor: tevent-0.9.25.
* [https://bugzilla.samba.org/show_bug.cgi?id=11331 BUG #11331]: Backpor: tdb-1.3.6.
* [https://bugzilla.samba.org/show_bug.cgi?id=11367 BUG #11367]: s3:auth: FixS:alloc :m in connect_to_domain_password_server().
*   Marc Muehlfeld <mmuehlfeld@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11315 BUG #11315]: GroupSS: ation: Add msS: only when --nis-domain was given.
*   Matthieu Patou <mat@matws.net>
* [https://bugzilla.samba.org/show_bug.cgi?id=11356 BUG #11356]: pidl:SS:ke th: compilation of PIDL producing the same results if the content hasn't change.
*   Noel Power <noel.power@suse.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11328 BUG #11328]: Kerbero: auth info3 should contain resource group ids available from pac_logon.
*   Gordon Ross <gordon.w.ross@gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11330 BUG #11330]: lib: : nt:  compile : in Solaris ports backend.
*   Christof Schmitt <cs@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11313 BUG #11313]: idmap_r Fix wbi '--gid-to-sid' query.
* [https://bugzilla.samba.org/show_bug.cgi?id=11324 BUG #11324]: ChangeS:haresec output back to previous format.
*   Uri Simchoni <urisimchoni@gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11358 BUG #11358]: winbind: EDisconnect : process if request is cancelled at main process.
*   Petr Viktorin <pviktori@redhat.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11330 BUG #11330]: Backpor: tevent-0.9.25.
*   Youzhong Yang <yyang@mathworks.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11217 BUG #11217]: s3DDAAS:g: Remove  file after closing socket fd.

 https://www.samba.org/samba/history/samba-4.2.3.html

Samba 4.2.2
------------------------

* Notes for Samba 4.2.2
 27, 2015

===============================
This is the latest stable release of Samba 4.2.
===============================

------------------------

===============================
Changes since 4.2.1:

* Michael Adam <obnox@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11182 BUG #11182]: s3:smbX refactor:duplica: Ecode into smbXsrv_session_clear_and_logoff().
* [https://bugzilla.samba.org/show_bug.cgi?id=11260 BUG #11260]: gencach: Edon't f gencache_stabilize if there were records to delete.
* Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11186 BUG #11186]: s3: :mbclient: : r getting :ribute server, ensure main srv pointer is still valid.
* [https://bugzilla.samba.org/show_bug.cgi?id=11236 BUG #11236]: s4: : Re dcesrv_ function into setup and send steps.
* [https://bugzilla.samba.org/show_bug.cgi?id=11240 BUG #11240]: s3: :: I: fileSS:ze returned in the response of "FILE_SUPERSEDE Create".
* [https://bugzilla.samba.org/show_bug.cgi?id=11249 BUG #11249]: Mangled:names do not work with acl_xattr.
* [https://bugzilla.samba.org/show_bug.cgi?id=11254 BUG #11254]: nmbd :rites browse.dat when not required.
* Ralph Boehme <slow@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11213 BUG #11213]: vfs_fru add opt "nfs_aces" that controls the NFS ACEs stuff.
* [https://bugzilla.samba.org/show_bug.cgi?id=11224 BUG #11224]: s3:smbd: Add :  :t_req_nterror.
* [https://bugzilla.samba.org/show_bug.cgi?id=11243 BUG #11243]: vfs: :nel_flock : named streams.
* [https://bugzilla.samba.org/show_bug.cgi?id=11244 BUG #11244]: vfs_gpf: EError c path doesn't call END_PROFILE.
* Alexander Bokovoy <ab@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11284 BUG #11284]: s4: :li/:dap: continue process CLDAP until all addresses are used.
* David Disseldorp <ddiss@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11201 BUG #11201]: ctdb:SS: ck f: Etalloc_asprintf() failure.:w:
* [https://bugzilla.samba.org/show_bug.cgi?id=11210 BUG #11210]: spoolss: purge t: Eprinter name cache on name change.
* Amitay Isaacs <amitay@gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11204 BUG #11204]: CTDB :td-callout does not scale.
* Björn Jacke <bj@sernet.de>
* [https://bugzilla.samba.org/show_bug.cgi?id=11221 BUG #11221]: vfs_fru also ma: characters below 0x20.
*Rajesh Joseph <rjoseph@redhat.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11201 BUG #11201]: ctdb:SS:verity  for CID 1291643.
* Julien Kerihuel <j.kerihuel@openchange.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11225 BUG #11225]: Multipl RPC connections are not handled by DCERPC server.
* [https://bugzilla.samba.org/show_bug.cgi?id=11226 BUG #11226]: Fix :inate connection behavior for asynchronous endpoint with PUSH notification flavors.
*Led <ledest@gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11007 BUG #11007]: ctdbDDA:ts: Fix bas in ctdbd_wrapper script.
* Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11201 BUG #11201]: ctdb:SS:x CID: 1125615, 1125634, 1125613, 1288201 and 1125553.
* [https://bugzilla.samba.org/show_bug.cgi?id=11257 BUG #11257]: SMB2 :uld cancel pending NOTIFY calls with DELETE_PENDING if the directory is deleted.
* Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11141 BUG #11141]: s3:winb make :   remove pending io requests before closing client sockets.
* [https://bugzilla.samba.org/show_bug.cgi?id=11182 BUG #11182]: Fix :c triggered by smbd_smb2_request_notify_done() -> smbXsrv_session_find_channel() in smbd.
* Christof Schmitt <cs@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11237 BUG #11237]: 'shares output no longer matches input format.
* Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11200 BUG #11200]: waf: : sys detection.
* Martin Schwenke <martin@meltin.net>
* [https://bugzilla.samba.org/show_bug.cgi?id=11202 BUG #11202]: CTDB:SS:x por: issues.
* [https://bugzilla.samba.org/show_bug.cgi?id=11203 BUG #11203]: CTDB:SS:x som: IPv6-related issues.
* [https://bugzilla.samba.org/show_bug.cgi?id=11204 BUG #11204]: CTDB :td-callout does not scale.
* Richard Sharpe <rsharpe@nutanix.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11234 BUG #11234]: 'net : dns gethostbyname' crashes with an error in TALLOC_FREE if you enter invalid values.
* Uri Simchoni <urisimchoni@gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11267 BUG #11267]: libads::record  ticket endtime for sealed ldap connections.
* Lukas Slebodnik <lslebodn@redhat.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11033 BUG #11033]: libSSLL:: Include  macro in internal header files before samba_util.h.

 https://www.samba.org/samba/history/samba-4.2.2.html

Samba 4.2.1
------------------------

* Notes for Samba 4.2.1
 15, 2015

===============================
This is the latest stable release of Samba 4.2.
===============================

------------------------

===============================
Changes since 4.2.0:

*  Michael Adam <obnox@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=8905 BUG #8905]: s3:winb: Do stop:group: numeration when a group has no gid.
* [https://bugzilla.samba.org/show_bug.cgi?id=10476 BUG #10476]: build:w: Fix :   spaces instead of tabs.
* [https://bugzilla.samba.org/show_bug.cgi?id=11143 BUG #11143]: s3DDAAS:: Fix cac user group lookup of trusted domains.
* Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=10016 BUG #10016]: s3: : nt If :SP_NEGOTIATE_TARG isn't set, cope with servers that don't send the 2 unused fields.
* [https://bugzilla.samba.org/show_bug.cgi?id=10888 BUG #10888]: s3: :nt: : nt use:spnego principal = yes" code checks wrong name.
* [https://bugzilla.samba.org/show_bug.cgi?id=11079 BUG #11079]: s3: : li:: If:reusing a : r struct, check every cli->timout miliseconds if it's still valid before use.
* [https://bugzilla.samba.org/show_bug.cgi?id=11173 BUG #11173]: s3: :li:  Ensure : correc finish a tevent req if the writev fails in the SMB1 case.
* [https://bugzilla.samba.org/show_bug.cgi?id=11175 BUG #11175]: Fix : of winbindd zombie processes on Solaris platform.
* [https://bugzilla.samba.org/show_bug.cgi?id=11177 BUG #11177]: s3: :mbclient: : missing :c stackframe.
* Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11135 BUG #11135]: backupk Explicitly : to gnutls and gcrypt.
* [https://bugzilla.samba.org/show_bug.cgi?id=11174 BUG #11174]: backupk Use ndr:ct_blob_all().
* Ralph Boehme <slow@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11125 BUG #11125]: vfs_fru Enhance : of malformed AppleDouble files.
* Samuel Cabrero <samuelcabrero@kernevil.me>
* [https://bugzilla.samba.org/show_bug.cgi?id=9791 BUG #9791]: Initial dwFlags field of DNS_RPC_NODE structure.
* David Disseldorp <ddiss@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11169 BUG #11169]: docsSSL:ap_rid: Remove : base_rid from example.
* Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=10476 BUG #10476]: waf: : the:build on openbsd.
* Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11144 BUG #11144]: talloc::Version :DOOTT2.
* [https://bugzilla.samba.org/show_bug.cgi?id=11164 BUG #11164]: s4:auth:gensec_gssap: ELet gensec_gssap: return NT_STATUS_LOGON_FAILURE for unknown errors.
*  Matthew Newton <matthew-git@newtoncomputing.co.uk>
* [https://bugzilla.samba.org/show_bug.cgi?id=11149 BUG #11149]: UpdateS:ibwbclient version to 0.12.
*  Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11018 BUG #11018]: spoolss: Retrieve :d printer GUID if not in registry.
* [https://bugzilla.samba.org/show_bug.cgi?id=11135 BUG #11135]: replace: Remove :s check for gcrypt header.
* [https://bugzilla.samba.org/show_bug.cgi?id=11180 BUG #11180]: s4DDAAS:_model: Do notS:lose random fds while forking.
* [https://bugzilla.samba.org/show_bug.cgi?id=11185 BUG #11185]: s3DDAAS: Fix 'fo user' with winbind default domain.
* Christof Schmitt <cs@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11153 BUG #11153]: brlock::Use 0SS:stead of empty initializer list.
* Thomas Schulz <schulz@adi.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11092 BUG #11092]: lib: :pect:  the  on Solaris.
* [https://bugzilla.samba.org/show_bug.cgi?id=11140 BUG #11140]: libcliS:uth: Match D: of netlogon_creds_cli_context_tmp with implementation.
* Jelmer Vernooij <jelmer@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11137 BUG #11137]: Backpor: subunit changes.

 https://www.samba.org/samba/history/samba-4.2.1.html

Samba 4.2.0 
------------------------

<onlyinclude>
* Notes for Samba 4.2.0
 4, 2015

===============================
This is is the first stable release of Samba 4.2.
===============================

------------------------

Samba 4.2 will be the next version of the Samba suite.

===============================
IMPORTANT NOTE ABOUT THE SUPPORT END OF SAMBA 3
===============================

------------------------

With the final release of Samba 4.2, the last series of Samba 3 has been discontinued! People still running 3.6.x or earlier, should consider moving to a more recent and maintained version (4.0 - 4.2). One of the common misconceptions is that Samba 4.x automatically means "Active Directory only": This is wrong!

Acting as an Active Directory Domain Controller is just one of the enhancements included in Samba 4.0 and later. Version 4.0 was just the next release after the 3.6 series and contains all the features of the previous ones - including the NT4-style (classic) domain support. This means you can update a Samba 3.x NT4-style PDC to 4.x, just as you've updated in the past (e.g. from 3.4.x to 3.5.x). You don't have to move your NT4-style domain to an Active Directory!

And of course the possibility remains unchanged, to setup a new NT4-style PDC with Samba 4.x, like done in the past (e.g. with openLDAP backend). Active Directory support in Samba 4 is additional and does not replace any of these features. We do understand the difficulty presented by existing LDAP structures and for that reason there isn't a plan to decommission the classic PDC support. It remains tested by the continuous integration system.

The code that supports the classic Domain Controller is also the same code that supports the internal 'Domain' of standalone servers and Domain Member Servers. This means that we still use this code, even when not acting as an AD Domain Controller. It is also the basis for some of the features of FreeIPA and so it gets development attention from that direction as well.

===============================
UPGRADING
===============================

------------------------

Read the "Winbindd/Netlogon improvements" section (below) carefully!

===============================
NEW FEATURES
===============================

------------------------

Transparent File Compression=
===============================

------------------------

Samba 4.2.0 adds support for the manipulation of file and folder compression flags on the Btrfs filesystem. With the Btrfs Samba VFS module enabled, SMB2+ compression flags can be set remotely from the Windows Explorer File->Properties->Advanced dialog. Files flagged for compression are transparently compressed and uncompressed when accessed or modified.

Previous File Versions with Snapper=
===============================

------------------------

The newly added Snapper VFS module exposes snapshots managed by Snapper for use by Samba. This provides the ability for remote clients to access shadow-copies via Windows Explorer using the "previous versions" dialog.

Winbindd/Netlogon improvements=
===============================

------------------------

The whole concept of maintaining the netlogon secure channel to (other) domain controllers was rewritten in order to maintain global state in a netlogon_creds_cli.tdb. This is the proper fix for a large number of bugs:

    ttps://bugzilla.samba.org/show_bug.cgi?id=10860
    ttps://bugzilla.samba.org/show_bug.cgi?id=6563
    ttps://bugzilla.samba.org/show_bug.cgi?id=7944
    ttps://bugzilla.samba.org/show_bug.cgi?id=7945
    ttps://bugzilla.samba.org/show_bug.cgi?id=7568
    ttps://bugzilla.samba.org/show_bug.cgi?id=8599

In addition a strong session key is now required by default, which means that communication to older servers or clients might be rejected by default.

*For the client side we have the following new options:
* strong key" (yes by default), "reject md5 servers" (no by default). E.g. for Samba 3.0.37 you need "require strong key = no" and
for NT4 DCs you need "require strong key = no" and "client NTLMv2 auth = no",
*On the server side (as domain controller) we have the following new options: 
* nt4 crypto" (no by default), "reject md5 client" (no by default). E.g. in order to allow Samba < 3.0.27 or NT4 members to work you need "allow nt4 crypto = yes"

*winbindd does not list group memberships for display purposes (e.g. getent group <domain\<group>) anymore by default.
* : default is "winbind expand groups = 0" now, the reason for this is the same as for "winbind enum users = no" and "winbind enum groups = no". Providing this information is not always reliably possible, e.g. if there are trusted domains.

Please consult the [http://www.samba.org/samba/docs/man/manpages/smb.conf.5.html smb.conf] manpage for more details on these new options.

Winbindd use on the Samba AD DC=
===============================

------------------------

Winbindd is now used on the Samba AD DC by default, replacing the partial rewrite used for winbind operations in Samba 4.0 and 4.1.

This allows more code to be shared, more options to be honoured, and paves the way for support for trusted domains in the AD DC.

If required the old internal winbind can be activated by setting 'server services = +winbind -winbindd'.  Upgrading users with a server services parameter specified should ensure they change 'winbind' to 'winbindd' to obtain the new functionality.

The 'samba' binary still manages the starting of this service, there is no need to start the winbindd binary manually.

Winbind now requires secured connections=
===============================

------------------------

To improve protection against rogue domain controllers we now require that when we connect to an AD DC in our forest, that the connection be signed using SMB Signing.  Set 'client signing = off' in the smb.conf to disable.

Also and DCE/RPC pipes must be sealed, set 'require strong key = false' and 'winbind sealed pipes = false' to disable.

Finally, the default for 'client ldap sasl wrapping' has been set to 'sign', to ensure the integrity of LDAP connections.  Set 'client ldap
sasl wrapping = plain' to disable.

Larger IO sizes for SMB2/3 by default=
===============================

------------------------

The default values for "smb2 max read", "smb2 max write" and "smb2 max trans" have been changed to 8388608 (8MiB) in order to match the default of Windows 2012R2.

SMB2 leases=
===============================

------------------------

The SMB2 protocol allows clients to aggressively cache files locally above and beyond the caching allowed by SMB1 and SMB2 oplocks.

Called SMB2 leases, this can greatly reduce traffic on an SMB2 connection. Samba 4.2 now implements SMB2 leases.

It can be turned on by setting the parameter "smb2 leases = yes" in the [global] section of your smb.conf. This parameter is set to off by default until the SMB2 leasing code is declared fully stable.

Improved DCERPC man in the middle detection=
===============================

------------------------

The DCERPC header signing has been implemented in addition to the dcerpc_sec_verification_trailer protection.

Overhauled "net idmap" command=
===============================

------------------------

The command line interface of the "net idmap" command has been made systematic, and subcommands for reading and writing the autorid idmap database have been added. Note that the writing commands should be used with great care. See the net(8) manual page for details.

tdb improvements=
===============================

------------------------

The tdb library, our core mechanism to store Samba-specific data on disk and share it between processes, has been improved to support process shared robust mutexes on Linux. These mutexes are available on Linux and Solaris and significantly reduce the overhead involved with tdb. To enable mutexes for tdb, set

 dbwrap_tdb_mutexes: E= yes

in the [global] section of your smb.conf.

Tdb file space management has also been made more efficient. This will lead to smaller and less fragmented databases.

Messaging improvements=
===============================

------------------------

Our internal messaging subsystem, used for example for things like oplock break messages between smbds or setting a process debug level dynamically, has been rewritten to use unix domain datagram messages.

Clustering support=
===============================

------------------------

Samba's file server clustering component CTDB is now integrated in the Samba tree.  This avoids the confusion of compatibility of Samba and CTDB versions as existed previously.

To build the Samba file server with cluster support, use the configure command line option --with-cluster-support.  This will build clustered file server against the in-tree ctdb.  Building clustered samba with previous versions of CTDB is no longer supported.

Samba Registry Editor=
===============================

------------------------

The utitlity to browse the samba registry has been overhauled by our Google Summer of Code student Chris Davis. Now samba-regedit has a Midnight-Commander-like theme and UI experience. You can browse keys and edit the diffent value types. For a data value type a hexeditor has been implemented.

Bad Password Lockout in the AD DC=
===============================

------------------------

Samba's AD DC now implements bad password lockout (on a per-DC basis).

That is, incorrect password attempts are tracked, and accounts locked out if too many bad passwords are submitted.  There is also a grace period of 60 minutes on the previous password when used for NTLM authentication (matching Windows 2003 SP1: https://support2.microsoft.com/kb/906305).

The relevant settings can be seen using 'samba-tool domain passwordsettings show' (**the new settings being highlighted**):

Password informations for domain 'DC=samba,DC=example,DC=com'

* complexity: :
 plaintext passwords: :
* history length: :
* password length: :
* password age (days): :
* password age (days): :
* t lockout duration (mins): :
* t lockout threshold (attempts): :
* account lockout after (mins): :

These values can be set using 'samba-tool domain passwordsettings set'.

Correct defaults in the smb.conf manpages=
===============================

------------------------

The default values for smb.conf parameters are now correctly specified in the smb.conf manpage, even when they refer to build-time specified
paths.  Provided Samba is built on a system with the right tools (xsltproc in particular) required to generate our man pages, then these will be built with the exact same embedded paths as used by the configuration parser at runtime.  Additionally, the default values read from the smb.conf manpage are checked by our test suite to match the values seen in testparm and used by the running binaries.

Consistent behaviour between samba-tool testparm and testparm=
===============================

------------------------

With the exception of the registry backend, which remains only available in the file server, the behaviour of the smb.conf parser and the tools 'samba-tool testparm' and 'testparm' is now consistent, particularly with regard to default values.  Except with regard to registry shares, it is no longer needed to use one tool on the AD DC, and another on the file server.

VFS WORM module=
===============================

------------------------

A VFS module for basic WORM (Write once read many) support has been added. It allows an additional layer on top of a Samba share, that provides a basic set of WORM functionality on the client side, to control the writeability of files and folders.

As the module is simply an additional layer, share access and permissions work like expected - only WORM functionality is added on top. Removing the module from the share configuration, removes this layer again. The filesystem ACLs are not affected in any way from the module and treated as usual.

The module does not provide complete WORM functions, like some archiving products do! It is not audit-proof, because the WORM function is only available on the client side, when accessing a share through SMB! If the same folder is shared by other services like NFS, the access only depents on the underlaying filesystem ACLs. Equally if you access the content directly on the server.

For additional information, see
* `VFS/vfs_worm`

vfs_fruit, a VFS module for OS X clients=
===============================

------------------------

A new VFS module that provides enhanced compatibility with Apple SMB clients and interoperability with a Netatalk 3 AFP fileserver.

The module features enhanced performance with reliable named streams support, interoperability with special characters commonly used by OS X client (eg '*', '/'), integrated file locking and Mac metadata access with Netatalk 3 and enhanced performance by implementing Apple's SMB2 extension codenamed "AAPL".

The modules behaviour is fully configurable, please refer to the manpage vfs_fruit for further details.

smbclient archival improvements=
===============================

------------------------

Archive creation and extraction support in smbclient has been rewritten to use libarchive. This fixes a number of outstanding bugs in Samba's previous custom tar implementation and also adds support for the extraction of zipped archives.

smbclient archive support can be enabled or disabled at build time with corresponding --with[out]-libarchive configure parameters.

===============================
Changes
===============================

------------------------

smb.conf changes=
===============================

------------------------

   Parameter Name			Description	Default
   allow nt4 crypto                     New             no
   neutralize nt4 emulation             New             no
   reject md5 client                    New             no
   reject md5 servers                   New             no
   require strong key                   New             yes
   smb2 max read                        Changed default 8388608
   smb2 max write                       Changed default 8388608
   smb2 max trans                       Changed default 8388608
   winbind expand groups                Changed default 0
</onlyinclude>

CHANGES SINCE 4.2.0rc5=
===============================

------------------------

*   Michael Adam <obnox at samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11117 BUG #11117]: doc:man: rfs: :SPP: conf: section.
*   Jeremy Allison <jra at samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11118 BUG #11118]: tevent::Ignore : signal events in the same way the epoll backend does.
*   Andrew Bartlett <abartlet at samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11100 BUG #11100]: debug:S: t clo:on-exec for the main log file FD.
* [https://bugzilla.samba.org/show_bug.cgi?id=11097 BUG #11097]: Fix :.1 Credentials Manager issue after KB2992611 on Samba domain.
*   Ira Cooper <ira at samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=1115 BUG #1115]: smbd:SS:op us vfs_Chdir after SMB_VFS_DISCONNECT.
*   Günther Deschner <gd at samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11088 BUG #11088]: vfs: : aSS:ief vfs_ceph manpage.
*   David Disseldorp <ddiss at samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11118 BUG #11118]: tevent::version :DOOTT24.
*   Amitay Isaacs <amitay at gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11124 BUG #11124]: ctdbDDA: Do notS:se sys_write to write to client sockets.
*   Volker Lendecke <vl at samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11119 BUG #11119]: snprint: ETry toS:upport %j.
*   Garming Sam <garming at catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=11097 BUG #11097]: Fix :.1 Credentials Manager issue after KB2992611 on Samba domain.
*Andreas Schneider <asn at samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11127 BUG #11127]: docDDAA: Add 'sh reference to 'access based share enum'.

CHANGES SINCE 4.2.0rc4=
===============================

------------------------

*   Michael Adam <obnox@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11032 BUG #11032]: EnableS:utexes in gencache_notrans.tdb.
* [https://bugzilla.samba.org/show_bug.cgi?id=11058 BUG #11058]: cli_con:nd: Don't s on host == NULL.
*   Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=10849 BUG #10849]: s3: : s3: modules: Fix :ilation : ESolaris.
* [https://bugzilla.samba.org/show_bug.cgi?id=11044 BUG #11044]: Fix : ntication using Kerberos (not AD).
* [https://bugzilla.samba.org/show_bug.cgi?id=11077 BUG #11077]: CVEDDAA:AASSHH0240: s3: net Ensure:we don'tS:all talloc_free on an uninitialized pointer.
* [https://bugzilla.samba.org/show_bug.cgi?id=11094 BUG #11094]: s3: :lient: :o leavesSS:  file handle open.
* [https://bugzilla.samba.org/show_bug.cgi?id=11102 BUG #11102]: s3: :: l -S:osen paranoia check. Stat opens can grant leases.
* [https://bugzilla.samba.org/show_bug.cgi?id=11104 BUG #11104]: s3: :: S close.:If a file has delete on close, store the return info before deleting.
*   Ira Cooper <ira@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11069 BUG #11069]: vfs_glu Add com to the pipe(2) code.
*   Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11070 BUG #11070]: s3DDAAS Fix dev build of vfs_ceph module.
*   David Disseldorp <ddiss@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=10808 BUG #10808]: printin:Hcups: Pack re:ASSHHattributes with IPP_TAG_KEYWORD.
* [https://bugzilla.samba.org/show_bug.cgi?id=11055 BUG #11055]: vfs_sna Correctly : multi-byte DBus strings.
* [https://bugzilla.samba.org/show_bug.cgi?id=11059 BUG #11059]: libsmb::Provide : domain for encrypted session referrals.
*   Poornima G <pgurusid@redhat.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11069 BUG #11069]: vfs_glu Implement  support.
*   Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11032 BUG #11032]: EnableS:utexes in gencache_notrans.tdb.
*   Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=9299 BUG #9299]: nsswitc: EFix son of linux nss_*.so.2 modules.
* [https://bugzilla.samba.org/show_bug.cgi?id=9702 BUG #9702]: s3:smb2: prot againstS:nteger wrap with "smb2 max credits = 65535".
* [https://bugzilla.samba.org/show_bug.cgi?id=9810 BUG #9810]: Make :idate_ldb of String(Generalized-Time) accept millisecond format ".000Z".
* [https://bugzilla.samba.org/show_bug.cgi?id=10112 BUG #10112]: Use :SSHHR linker flag on Solaris, not -rpath.
*   Marc Muehlfeld <mmuehlfeld@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=10909 BUG #10909]: sambaDD:: Create  enabled users and unixHomeDirectory attribute.
*   Garming Sam <garming@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=11022 BUG #11022]: Make :repoint search show user documents.
*   Christof Schmitt <cs@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11032 BUG #11032]: EnableS:utexes in gencache_notrans.tdb.
*   Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11058 BUG #11058]: utils:S:ix 'ne: time' segfault.
* [https://bugzilla.samba.org/show_bug.cgi?id=11066 BUG #11066]: s3DDAAS:pass: Fix mem leak in pam_sm_authenticate().
* [https://bugzilla.samba.org/show_bug.cgi?id=11077 BUG #11077]: CVEDDAA:AASSHH0240: s3-netlog Make sure  do not deference a NULL pointer.
*   Raghavendra Talur <raghavendra.talur@gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11069 BUG #11069]B: vfsSSLL:terfs: Change  key to match gluster key.

CHANGES SINCE 4.2.0rc3=
===============================

------------------------

*   Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=10993 BUG #10892]: CVEDDAA:AASSHH8143: dsdb-saml Check for :d access rights before we allow changes to userAccountControl.
*   Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=10240 BUG #10240]: vfs: : glu manpage.
*   David Disseldorp <ddiss@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=10984 BUG #10984]: Fix :lss IDL response marshalling when returning error without clearing info.
*   Amitay Isaacs <amitay@gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11000 BUG #11000]: ctdbDDA:n: Use cor tdb flags when enabling robust mutex support.
*   Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11032 BUG #11032]: tdb_wra: EMake mu easier to use.
* [https://bugzilla.samba.org/show_bug.cgi?id=11039 BUG #11039]: vfs_fru Fix bas name conversion.
* [https://bugzilla.samba.org/show_bug.cgi?id=11040 BUG #11040]: vfs_fru mmap un FreeBSD needs PROT_READ.
* [https://bugzilla.samba.org/show_bug.cgi?id=11051 BUG #11051]: net: : sam:addgroupmem.
*   Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=10940 BUG #10940]: s3:pass fix :   pdb_set_pw_history().
* [https://bugzilla.samba.org/show_bug.cgi?id=11004 BUG #11004]: tdb: :sion :DOOTT4.
*   Christof Schmitt <cs@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11034 BUG #11034]: winbind: Retry a SESSION_EXPIRED error in ping-dc.
*   Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11008 BUG #11008]: s3DDAAS: Fix aut:n with long hostnames.
* [https://bugzilla.samba.org/show_bug.cgi?id=11026 BUG #11026]: nss_wra check f: Enss.h.
* [https://bugzilla.samba.org/show_bug.cgi?id=11033 BUG #11033]: libSSLL:: Avoid c: which alread defined consumer DEBUG macro.
* [https://bugzilla.samba.org/show_bug.cgi?id=11037 BUG #11037]: s3DDAAS: Fix aSS:ssible segfault in kerberos_fetch_pac().

CHANGES SINCE 4.2.0rc2=
===============================

------------------------

* Michael Adam <obnox@samba.org>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10892 BUG #10892]: Integrate  into top-level Samba build.
* Jeremy Allison <jra@samba.org>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10851 BUG #10851]: lib: ui: Fix:setgroups an: syscall detection on a system without native uid_wrapper library.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10896 BUG #10896]: s3-nmbd:S:ix netbios : truncation.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10904 BUG #10904]: Fix smb loops doing a directory listing against Mac OS X 10 server with a non-wildcard path.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10911 BUG #10911]: Add sup for SMB2 leases.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10920 BUG #10920]: s3: nmb: EEnsure :IOS n are only 15 characters stored.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10966 BUG #10966]: libcli:  Pure :AASSHHonly :rot fix to make us behave as a Windows client does.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10982 BUG #10982]: s3: smb: EFix :te* c to follow POSIX error return convention.
* Christian Ambach <ambi@samba.org>
* :SLLAAS:a.samba.org/show_bug.cgi?id=9629 BUG #9629]: Make 'p: work again.
* Björn Baumbach <bb@sernet.de>
* :SLLAAS:a.samba.org/show_bug.cgi?id=11014 BUG #11014]: ctdb-buil: EFix build : xsltproc.
* Ralph Boehme <slow@samba.org>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10834 BUG #10834]: Don't b vfs_snapper on FreeBSD.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10971 BUG #10971]: vfs_streams_xatt: ECheck stream :.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10983 BUG #10983]: vfs_fruit:  support : AAPL.
* :SLLAAS:a.samba.org/show_bug.cgi?id=11005 BUG #11005]: vfs_streams_xatt: EAdd missing : to SMB_VFS_NEXT_CONNECT.
* Günther Deschner <gd@samba.org>
* :SLLAAS:a.samba.org/show_bug.cgi?id=9056 BUG #9056]: pam_winbind: : warn_pwd_exp implementation.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10942 BUG #10942]: Cleanup :g_to_array and usage.
* David Disseldorp <ddiss@samba.org>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10898 BUG #10898]: spoolss:  handling : bad EnumJobs levels.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10905 BUG #10905]: Fix pri: Ejob enumeration.
* Amitay Isaacs <amitay@gmail.com>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10620 BUG #10620]: s4-dns:SS:d support : BIND 9.10.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10892 BUG #10892]: Integrate  into top-level Samba build.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10996 BUG #10996]: Fix IPv: support in CTDB.
* :SLLAAS:a.samba.org/show_bug.cgi?id=11014 BUG #11014]: packaging: :  CTDB : pages in the tarball.
* Björn Jacke <bj@sernet.de>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10835 BUG #10835]: nss_winbind: : getgroupmemb for FreeBSD.
* Guenter Kukkukk <linux@kukkukk.com>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10952 BUG #10952]: Fix 'sa:Htool dns serverinfo <server>' for IPv6.
* Volker Lendecke <vl@samba.org>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10932 BUG #10932]: pdb_tdb:  a T:ASSHHSAFE_FREE mixup.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10942 BUG #10942]: dbwrap_ctdb: : on  flags to tdb_open.
* Justin Maggard <jmaggard10@gmail.com>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10852 BUG #10852]: winbind3:  pwent :  substitution.
* Kamen Mazdrashki <kamenim@samba.org>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10975 BUG #10975]: ldb: ve 1.1:
* Stefan Metzmacher <metze@samba.org>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10781 BUG #10781]: tdb: ve 1.3:
* :SLLAAS:a.samba.org/show_bug.cgi?id=10911 BUG #10911]: Add sup for SMB2 leases.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10921 BUG #10921]: s3:smbd:  file :rupt using "write cache size != 0".
* :SLLAAS:a.samba.org/show_bug.cgi?id=10949 BUG #10949]: Fix Roo search with extended dn control.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10958 BUG #10958]: libcli/ only force :g of smb2 session setups when binding a new session.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10975 BUG #10975]: ldb: ve 1.1:
* :SLLAAS:a.samba.org/show_bug.cgi?id=11016 BUG #11016]: pdb_get_trustedd fails with non valid UTF16 random passwords.
* Marc Muehlfeld <mmuehlfeld@samba.org>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10895 BUG #10895]: samba-too: group add: Add option :SHH-nis-domain' and '--gid'.
* Noel Power <noel.power@suse.com>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10918 BUG #10918]: btrfs:  leak : directory handle.
* Matt Rogers <mrogers@redhat.com>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10933 BUG #10933]: s3-keytab: fix keytab : NULL termination.
* Garming Sam <garming@catalyst.net.nz>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10355 BUG #10355]: pdb: Fi: build : with shared modules.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10720 BUG #10720]: idmap:  the :t id type to *id_to_sid methods.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10864 BUG #10864]: Fix tes to show hidden share defaults.
* Andreas Schneider <asn@samba.org>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10279 BUG #10279]: Make 's: use cached creds.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10960 BUG #10960]: s3-smbcli Return successSS: we listed the shares.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10961 BUG #10961]: s3-smbsta Fix exit  of profile output.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10965 BUG #10965]: socket_wrapper:S:dd missing :type check for eventfd.
* Martin Schwenke <martin@meltin.net>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10892 BUG #10892]: Integrate  into top-level Samba build.
* :SLLAAS:a.samba.org/show_bug.cgi?id=10996 BUG #10996]: Fix IPv: support in CTDB.

CHANGES SINCE 4.2.0rc1=
===============================

------------------------

* Jeremy Allison <jra@samba.org>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10848 BUG #10848]: s3: smb querySS:fo retur: length check was reversed.
* Björn Baumbach <bb@sernet.de>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10862 BUG #10862]: build: : Enot  'texpect' binary anymore.
* Chris Davis <cd.rattan@gmail.com>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10859 BUG #10859]: Improve :SSHHregedit.
* Jakub Hrozek <jakub.hrozek@gmail.com>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10861 BUG #10861]: Fix bui: Eof socket_wrapper on systems without SO_PROTOCOL.
* Volker Lendecke <vl@samba.org>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10860 BUG #10860]: registry: : leave :ing transactions.
* Stefan Metzmacher <metze@samba.org>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10866 BUG #10866]: libcli/ Fix smb2cli_vali:iate_info with min=PROTOCOL_NT1 max=PROTOCOL_SMB2_02.
* Christof Schmitt <cs@samba.org>
* :SLLAAS:a.samba.org/show_bug.cgi?id=10837 BUG #10837]: idmap_rfc2307:SS:x a c after connection problem to DC.

===============================
KNOWN ISSUES
===============================

------------------------

 https://download.samba.org/pub/samba/rc/WHATSNEW-4.2.0rc2.txt

Samba 4.2 release blocker
------------------------

* [https://bugzilla.samba.org/show_bug.cgi?id=10077 BUG #10077]:  4.2 can't be released
*: [https://bugzilla.samba.org/showdependencytree.cgi?id=10077&hide_resolved=1 Dependecy tree]

----
`Category: Notes`