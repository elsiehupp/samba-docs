Samba 3.5 Features added/changed
    <namespace>0</namespace>
<last_edited>2017-02-26T21:09:06Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

Samba 3.5 is in the **security fixes only** mode 
------------------------

Samba 3.5 is in the **security fixes only** mode, which means there will be bug fix and security releases
for this series.

Samba 3.5.22
------------------------

* Release Notes for Samba 3.5.22
* August 05, 2013

===============================
This is a security release in order to address
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4124 CVE-2013-4124] (Missing integer wrap protection in EA list reading can cause
server to loop with DOS).

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4124 CVE-2013-4124]:
* All current released versions of Samba are vulnerable to a denial of service on an authenticated or guest connection. A malformed packet can cause the smbd server to loop the CPU performing memory allocations and preventing any further service.

* A connection to a file share, or a local account is needed to exploit this problem, either authenticated or unauthenticated if guest connections are allowed.

* This flaw is not exploitable beyond causing the code to loop allocating memory, which may cause the machine to exceed memory limits.

Changes since 3.6.16:=
===============================

------------------------

*Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10010 bug #10010`: CVE-2013-4124: Missing integer wrap protection in EA list reading can cause server to loop with DOS.

Samba 3.5.21
------------------------

* Release Notes for Samba 3.5.21
* January 30, 2013

===============================
This is a security release in order to addressCVE-2013-0213 CVE-2013-0213.
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-0213 CVE-2013-0213] Clickjacking issue in SWAT
* All current released versions of Samba are vulnerable to clickjacking in the Samba Web Administration Tool (SWAT). When the SWAT pages are integrated into a malicious web page via a frame or iframe and then overlaid by other content, an attacker could trick an administrator to potentially change Samba settings.

* In order to be vulnerable, SWAT must have been installed and enabled either as a standalone server launched from inetd or xinetd, or as a CGI plugin to Apache. If SWAT has not been installed or enabled (which is the default install state for Samba) this advisory can be ignored.

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-0214 CVE-2013-0214] Potential XSRF in SWAT
* All current released versions of Samba are vulnerable to a cross-site request forgery in the Samba Web Administration Tool (SWAT). By guessing a user's password and then tricking a user who is authenticated with SWAT into clicking a manipulated URL on a different web page, it is possible to manipulate SWAT.

* In order to be vulnerable, the attacker needs to know the victim's password. Additionally SWAT must have been installed and enabled either as a standalone server launched from inetd or xinetd, or as a CGI plugin to Apache. If SWAT has not been installed or enabled (which is the default install state for Samba) this advisory can be ignored.

Changes since 3.5.20:=
===============================

------------------------

* Kai Blin <kai@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9576 bug #9576] CVE-2013-0213: Fix clickjacking issue in SWAT.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9577 bug #9577] CVE-2013-0214: Fix potential XSRF in SWAT.

Samba 3.5.20
------------------------

* Release Notes for Samba 3.5.20
* December 17, 2012

===============================
This is the latest stable release of Samba 3.5.
===============================

------------------------

Major enhancements in Samba 3.5.20 include:=
===============================

------------------------

* [https://bugzilla.samba.org/show_bug.cgi?id=9390 bug #9390]Fix segfaults in log level = 10 on Solaris.
* [https://bugzilla.samba.org/show_bug.cgi?id=9236 bug #9236]Apply ACL masks correctly when setting ACLs.

===============================
Changes since 3.5.19:
===============================

------------------------

* Jeremy Allison <jra@samba.org>
* * BUG 7781: Samba transforms ShareName to lowercase (sharename) when adding new share via MMC.
* * BUG 9236: Apply ACL masks correctly when setting ACLs.
* * BUG 9455: munmap called for an address location not mapped by Samba.
* Björn Baumbach <bb@sernet.de>
* * BUG 9345: Fix usage of <smbconfoption> tag.
* Stefan Metzmacher <metze@samba.org>
* * BUG 9390: Fix segfaults in log level = 10 on Solaris.
* * BUG 9402: Fix dns updates against BIND9 (used in a Samba4 domain).

 http://www.samba.org/samba/history/samba-3.5.20.html

Samba 3.5.19 
------------------------

* Release Notes for Samba 3.5.19
* November 05, 2012

===============================
This is the latest stable release of Samba 3.5.
===============================

------------------------

Major enhancements in Samba 3.5.19 include:=
===============================

------------------------

* [https://bugzilla.samba.org/show_bug.cgi?id=9016 bug #9016]Connection to outbound trusted domain goes offline
* [https://bugzilla.samba.org/show_bug.cgi?id=9236 bug #9236]ACL masks incorrectly applied when setting ACLs.
* [https://bugzilla.samba.org/show_bug.cgi?id=9218 bug #9218]Samba panics if a user specifies an invalid port number.

Changes since 3.5.18:=
===============================

------------------------

* Jeremy Allison <jra at samba.org>
* * BUG 9016: Connection to outbound trusted domain goes offline.
* * BUG 9117: smbclient can't connect to a Windows 7 server using NTLMv2.
* * BUG 9213: Bad ASN.1 NegTokenInit packet can cause invalid free.
* * BUG 9236: ACL masks incorrectly applied when setting ACLs.
*   Andrew Bartlett <abartlet at samba.org>
* * BUG 8788: libsmb: Initialise ticket to ensure we do not free invalid memory.
*   Björn Jacke <bj at sernet.de>
* * BUG 8344: autoconf: Fix --with(out)-sendfile-support option handling.
* * BUG 8732: Fix compile of krb5 locator on Solaris.
* * BUG 9172: Add quota support for gfs2.
*   Matthieu Patou <mat at matws.net>
* * BUG 9259: lib-addns: Ensure that allocated buffer are pre set to 0.
*  Andreas Schneider <asn at samba.org>
* * BUG 9218: Samba panics if a user specifies an invalid port number.

 http://www.samba.org/samba/history/samba-3.5.19.html

Samba 3.5.18 
------------------------

* Release Notes for Samba 3.5.18
* September 24, 2012

===============================
This is the latest stable release of Samba 3.5.
===============================

------------------------

Major enhancements in Samba 3.5.18 include:=
===============================

------------------------

*  [https://bugzilla.samba.org/show_bug.cgi?id=9084 bug #9084]Fix a smbd crash in reply_lockingX_error.
*  [https://bugzilla.samba.org/show_bug.cgi?id=9104 bug #9104]Fix Winbind crashes caused by mis-identified idle clients (bug #9104).
*  [https://bugzilla.samba.org/show_bug.cgi?id=9013 bug #9013]Desktop Managers (xdm, gdm, lightdm...) crash with SIGSEGV in _pam_winbind_change_pwd() when password is expiring (bug #9013).

Changes since 3.5.17:=
===============================

------------------------

* Michael Adam <obnox at samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=7788 bug #7788]: Clarify the idmap_rid manpage.

*   Jeremy Allison <jra at samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9098 bug #9098]: Winbind does not refresh Kerberos tickets.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9147 bug #9147]: Winbind can't fetch user or group info from AD via LDAP.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9150 bug #9150]: Valid open requests can cause smbd assert due to incorrect oplock handling on delete requests.
*   Neil R. Goldberg <ngoldber at mitre.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9100 bug #9100]: Winbind doesn't return "Domain Local" groups from own domain.
*   Hargagan <shargagan at novell.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9085 bug #9085]: NMB registration for a duplicate workstation fails with registration refuse.
*   Björn Jacke <bj at sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=7814 bug #7814]: Fix build of sysquote_xfs.
* * [https://bugzilla.samba.org/show_bug.cgi?id=8402 bug #8402]: Winbind log spammed with idmap messages.
*   Volker Lendecke <vl at samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9084 bug #9084]: Fix a smbd crash in reply_lockingX_error.
*   Herb Lewis <hlewis at panasas.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9104 bug #9104]: Fix Winbind crashes caused by mis-identified idle clients.
*   Luca Lorenzetto <lorenzetto-luca at ubuntu-it.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9013 bug #9013]: Desktop Managers (xdm, gdm, lightdm...) crash with SIGSEGV in _pam_winbind_change_pwd() when password is expiring.

 http://www.samba.org/samba/history/samba-3.5.18.html

Samba 3.5.17 
------------------------

* Release Notes for Samba 3.5.17
* August 12, 2012

===============================
This is the latest stable release of Samba 3.5.
===============================

------------------------

Changes since 3.5.16:=
===============================

------------------------

* [https://bugzilla.samba.org/show_bug.cgi?id=9034 bug #9034]: Fix typo in set_re_uid() call when USE_SETRESUID selected in configure.
* [https://bugzilla.samba.org/show_bug.cgi?id=8996 bug #6996]: Fix build without ads support.
* [https://bugzilla.samba.org/show_bug.cgi?id=9011 bug #9011]: Second part of a fix for bug #9011 (Build on HP-UX broken).
* [https://bugzilla.samba.org/show_bug.cgi?id=9022 bug #9022]: Make vfs_gpfs less verbose in get/set_xattr functions.

 http://www.samba.org/samba/history/samba-3.5.17.html

Samba 3.5.16 
------------------------

* Release Notes for Samba 3.5.16
* July 2, 2012

===============================
This is the latest stable release of Samba 3.5.
===============================

------------------------

Major enhancements in Samba 3.5.16 include:=
===============================

------------------------

* Fix possible memory leaks in the Samba master process [https://bugzilla.samba.org/show_bug.cgi?id=8970 bug #8970].
* Fix uninitialized memory read in talloc_free().
* Fix smbd crash with unknown user [https://bugzilla.samba.org/show_bug.cgi?id=8314 bug #8314].

 http://www.samba.org/samba/history/samba-3.5.16.html

Samba 3.5.15 
------------------------

* Release Notes for Samba 3.5.15
* April 30, 2012

===============================
This is a security release in order to address CVE-2012-2111:
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-2111 CVE-2012-2111]:
* Incorrect permission checks when granting/removing privileges can compromise file server security.
* Samba 3.4.x to 3.6.4 are affected by a vulnerability that allows arbitrary users to modify privileges on a file server.

Samba 3.5.14 
------------------------

* Release Notes for Samba 3.5.14
* April 10, 2012

===============================
This is a security release in order to address CVE-2012-1182:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-1182 CVE-2012-1182]:
*  Samba 3.0.x to 3.6.3 are affected by a vulnerability that allows remote code execution as the "root" user.
* * Bug [https://bugzilla.samba.org/show_bug.cgi?id=8815 8815]: PIDL based autogenerated code allows overwriting beyond of allocated array (CVE-2012-1182).

Samba 3.5.13 
------------------------

* Release Notes for Samba 3.5.13
* March 12, 2012

===============================
This is the latest stable release of Samba 3.5.
===============================

------------------------

Major enhancements in Samba 3.5.13 include:=
===============================

------------------------

* Fix a crash bug in cldap_socket_recv_dgram() [https://bugzilla.samba.org/show_bug.cgi?id=8593 bug #8593].
* Fully observe password change settings [https://bugzilla.samba.org/show_bug.cgi?id=8561 bug #8561].
* Fix NT ACL issue [https://bugzilla.samba.org/show_bug.cgi?id=8673 bug #8673].
* Fix segfault in Winbind if we can't map the last user [https://bugzilla.samba.org/show_bug.cgi?id=8678 bug #8678].

 http://www.samba.org/samba/history/samba-3.5.13.html

Samba 3.5.12 
------------------------

* Release Notes for Samba 3.5.12
* November 2, 2011

===============================
This is the latest stable release of Samba 3.5.
===============================

------------------------

Major enhancements in Samba 3.5.12 include:=
===============================

------------------------

*  Fix race condition in Winbind [https://bugzilla.samba.org/show_bug.cgi?id=7844 bug #7844].
*  The VFS ACL modules are no longer experimental but production-ready.

 http://www.samba.org/samba/history/samba-3.5.12.html

Samba 3.5.11 
------------------------

* Release Notes for Samba 3.5.11
* August 4, 2011

===============================
This is the latest stable release of Samba 3.5.
===============================

------------------------

Major enhancements in Samba 3.5.11 include:=
===============================

------------------------

* Fix access to Samba shares when Windows security patch KB2536276 is installed [https://bugzilla.samba.org/show_bug.cgi?id=8238 bug #8238].
* Fix Winbind panics if verify_idpool() fails [https://bugzilla.samba.org/show_bug.cgi?id=8253 bug #8253].

 http://www.samba.org/samba/history/samba-3.5.11.html

Samba 3.5.10 
------------------------

* Release Notes for Samba 3.5.10
* July 26, 2011

===============================
This is a security release in order to address CVE-2011-2522 CVE-2011-2694
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-2522 CVE-2011-2522]:
* The Samba Web Administration Tool (SWAT) in Samba versions 3.0.x to 3.5.9 are affected by a cross-site request forgery.

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-2694 CVE-2011-2694]:
* The Samba Web Administration Tool (SWAT) in Samba versions 3.0.x to 3.5.9 are affected by a cross-site scripting vulnerability.

Please note that SWAT must be enabled in order for these
vulnerabilities to be exploitable. By default, SWAT
is *not* enabled on a Samba install.

 [http://www.samba.org/samba/history/samba-3.5.10.html Release Notes Samba 3.5.10]

Samba 3.5.9 
------------------------

* Release Notes for Samba 3.5.9:
* June 14, 2011:

===============================
This is the latest stable release of Samba 3.5.
===============================

------------------------

Major enhancements in Samba 3.5.9 include:=
===============================

------------------------

*  Sgid bit lost on folder rename [https://bugzilla.samba.org/show_bug.cgi?id=7996 bug #7996].
*  ACL can get lost when files are being renamed [https://bugzilla.samba.org/show_bug.cgi?id=7987 bug #7987].
*  Respect "allow trusted domains = no" in Winbind [https://bugzilla.samba.org/show_bug.cgi?id=6966 bug #6966].
*  Samba now follows Windows behaviour as a kerberos client, requesting a CIFS/ ticket [https://bugzilla.samba.org/show_bug.cgi?id=7893 bug #7893].
*  Kerberos authentication fails when security blobsize is greater than 16 kB [https://bugzilla.samba.org/show_bug.cgi?id=6911 bug #6911].

New Kerberos behaviour=
===============================

------------------------

A new parameter 'client use spnego principal' defaults to 'no' and means Samba will use CIFS/hostname to obtain a kerberos ticket, acting more like Windows when using Kerberos against a CIFS server in smbclient, Winbind and other Samba client tools.  This will change which servers we will successfully negotiate Kerberos connections to. This is due to Samba no longer trusting a server-provided hint which is not available from Windows 2008 or later.  For correct operation with all clients, all aliases for a server should be recorded as a as a servicePrincipalName on the server's record in AD.

----
(**Updated 14-June-2011**)

* Tuesday, June 14 - Samba 3.5.9 has been released
    http://www.samba.org/samba/history/samba-3.5.9.html Release Notes Samba 3.5.9]

Samba 3.5.8 
------------------------

* Release Notes for Samba 3.5.8:
* March 7, 2011:

===============================
This is the latest stable release of Samba 3.5.
===============================

------------------------

Major enhancements in Samba 3.5.8 include:=
===============================

------------------------

*  Fix Winbind crash bug when no DC is available [https://bugzilla.samba.org/show_bug.cgi?id=7730 bug #7730].
*  Fix finding users on domain members [https://bugzilla.samba.org/show_bug.cgi?id=7743 bug #7743].
*  Fix memory leaks in Winbind [https://bugzilla.samba.org/show_bug.cgi?id=7879 bug #7879].
*  Fix printing with Windows 7 clients [https://bugzilla.samba.org/show_bug.cgi?id=7567 bug #7567].

(**Updated 07-March-2011**)

* Monday, March 7 - Samba 3.5.8 has been released
    http://www.samba.org/samba/history/samba-3.5.8.html Release Notes Samba 3.5.8]

Samba 3.5.7 
------------------------

* Release Notes for Samba 3.5.7:
* February 28, 2011:

===============================
This is a security release in order to address CVE-2011-0719.
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-0719 CVE-2011-0719]:
* All current released versions of Samba are vulnerable to a denial of service caused by memory corruption. Range checks on file descriptors being used in the FD_SET macro were not present allowing stack corruption. This can cause the Samba code to crash or to loop attempting to select on a bad file descriptor set.

----
(**Updated 28-February-2011**)

* Monday, February 28 - Samba 3.5.7 has been released to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-0719 CVE-2011-0719].
    http://www.samba.org/samba/history/samba-3.5.7.html Release Notes Samba 3.5.7]

Samba 3.5.6 
------------------------

* Release Notes for Samba 3.5.6:
* October 8, 2010:

===============================
This is the latest stable release of Samba 3.5.
===============================

------------------------

Major enhancements in Samba 3.5.6 include:=
===============================

------------------------

* Fix smbd panic on invalid NetBIOS session request [https://bugzilla.samba.org/show_bug.cgi?id=7698 bug #7698].
* Fix smbd crash caused by "%D" in "printer admin" [https://bugzilla.samba.org/show_bug.cgi?id=7541 bug #7541)].
* Fix crash bug with invalid SPNEGO token [https://bugzilla.samba.org/show_bug.cgi?id=7694 bug #7694].
* Fix Winbind internal error [https://bugzilla.samba.org/show_bug.cgi?id=7636 bug #7636].

----
(**Updated 08-October-2010**)

* Friday, October 8 - Samba 3.5.6 has been released
    http://www.samba.org/samba/history/samba-3.5.6.html Release Notes Samba 3.5.6]

Samba 3.5.5 
------------------------

* Release Notes for Samba 3.5.5:
* September 14, 2010:

===============================
This is a security release in order to address CVE-2010-3069.
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-3069 CVE-2010-3069]:
*   All current released versions of Samba are vulnerable to a buffer overrun vulnerability. The sid_parse() function (and related dom_sid_parse() function in the source4 code) do not correctly check their input lengths when reading a binary representation of a Windows SID (Security ID). This allows a malicious client to send a sid that can overflow the stack variable that is being used to store the SID in the Samba smbd server.

----
(**Updated 14-September-2010**)

* Tuesday, September 14 - Samba 3.5.5 has been released to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-3069 CVE-2010-3069].
    http://www.samba.org/samba/history/samba-3.5.5.html Release Notes Samba 3.5.5]

Samba 3.5.4 
------------------------

* Release Notes for Samba 3.5.4:
* June 23, 2010:

===============================
This is the latest stable release of Samba 3.5.
===============================

------------------------

Major enhancements in Samba 3.5.4 include:=
===============================

------------------------

* Fix smbd crash when sambaLMPassword and sambaNTPassword entries missing from ldap [https://bugzilla.samba.org/show_bug.cgi?id=7448 bug #7448].
* Fix init_sam_from_ldap storing group in sid2uid cache [https://bugzilla.samba.org/show_bug.cgi?id=7507 bug #7507].
----
(**Updated 23-June-2010**)

* Wednesday, June 23 - Samba 3.5.4 has been released
    http://www.samba.org/samba/history/samba-3.5.4.html Release Notes Samba 3.5.4]

Samba 3.5.3 
------------------------

* Release Notes for Samba 3.5.3:
===============================
This is the latest stable release of Samba 3.5.
===============================

------------------------

Major enhancements in Samba 3.5.3 include:=
===============================

------------------------

* Fix MS-DFS functionality [https://bugzilla.samba.org/show_bug.cgi?id=7339 bug #7339].
* Fix a Winbind crash when scanning trusts [https://bugzilla.samba.org/show_bug.cgi?id=7389 bug #7389].
* Fix problems with SIGCHLD handling in Winbind [https://bugzilla.samba.org/show_bug.cgi?id=7317 bug #7317].
----
(**Updated 19-May-2010**)

* Wednesday, May 19 - Samba 3.5.3 has been released
    http://www.samba.org/samba/history/samba-3.5.3.html Release Notes Samba 3.5.3]

Samba 3.5.2 
------------------------

* Release Notes for Samba 3.5.2:
* April 7, 2010:

===============================
This is the latest stable release of Samba 3.5.
===============================

------------------------

Major enhancements in Samba 3.5.2 include:=
===============================

------------------------

* Fix smbd segfaults in _netr_SamLogon for clients sending null domain [https://bugzilla.samba.org/show_bug.cgi?id=7237 bug #7237].
* Fix smbd segfaults in "waiting for connections" message [https://bugzilla.samba.org/show_bug.cgi?id=7251 bug #7251].
* Fix an uninitialized variable read in smbd [https://bugzilla.samba.org/show_bug.cgi?id=7254 bug #7254].
* Fix a memleak in Winbind [https://bugzilla.samba.org/show_bug.cgi?id=7278 bug #7278].
* Fix Winbind reconnection to it's own domain [https://bugzilla.samba.org/show_bug.cgi?id=7295 bug #7295].
----
(**Updated 07-April-2010**)

* Wednesday, April 7 - Samba 3.5.2 has been released
    http://www.samba.org/samba/history/samba-3.5.2.html Release Notes Samba 3.5.2]

Samba 3.5.1 
------------------------

* Release Notes for Samba 3.5.1:
* March 8, 2010:

===============================
This is a security release in order to address CVE-2010-0728.
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-0728 CVE-2010-0728]:
*  In Samba releases 3.5.0, 3.4.6 and 3.3.11, new code was added to fix a problem with Linux asynchronous IO handling. This code introduced a bad security flaw on Linux platforms if the binaries were built on Linux platforms with libcap support. The flaw caused all smbd processes to inherit CAP_DAC_OVERRIDE capabilities, allowing all file system access to be allowed even when permissions should have denied access.

----
(**Updated 09-March-2010**)

* Monday, March 8 - Samba 3.5.1 has been released to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-0728 CVE-2010-0728].
    http://www.samba.org/samba/history/samba-3.5.1.html Release Notes Samba 3.5.1]

Samba 3.5.0 
------------------------

* Release Notes for Samba 3.5.0:
* March 1, 2010:

===============================
This is the first stable release of Samba 3.5.
===============================

------------------------

Major enhancements in Samba 3.5.0 include:=
===============================

------------------------

===============================
General changes:==
===============================

------------------------

* Add support for full Windows timestamp resolution
* The Using Samba HTML book has been removed.
* 'net', 'smbclient' and libsmbclient can use credentials cached by Winbind.
* The default value of "wide links" has been changed to "no".

===============================
Protocol changes:==
===============================

------------------------

* Experimental implementation of SMB2

===============================
Printing Changes:==
===============================

------------------------

* Add encryption support for connections to a CUPS server

===============================
Winbind changes:==
===============================

------------------------

* Major refactoring
* Asynchronous

===============================
VFS modules:==
===============================

------------------------

* New vfs_scannedonly module has been added.

General changes:=
===============================

------------------------

Support for full Windows timestamp resolution has been added. This effectively makes us use Windows' full 100ns timestamp resolution if supported by the kernel (2.6.22 and higher) and the glibc (2.6 and higher).

The Using Samba HTML book has been removed from the Samba tarball. It is still available at http://www.samba.org/samba/docs/using_samba/toc.html.

Samba client tools like 'net', 'smbclient' and libsmbclient can use the user credentials cached by Winbind at logon time. This is very useful e.g. when connecting to a Samba server using Nautilus without re-entering username and password. This feature is enabled by default and can be disabled per application by setting the LIBSMBCLIENT_NO_CCACHE environment variable.

The default value of "wide links" has been changed to "no" to avoid an insecure default configuration ("wide links = yes" and "unix extensions = yes"). For more details, please see http://www.samba.org/samba/news/symlink_attack.html.

Protocol changes=
===============================

------------------------

An EXPERIMENTAL implementation of the SMB2 protocol has been added. SMB2 can be enabled by setting "max protocol = smb2". SMB2 is a new implementation of the SMB protocol used by Windows Vista and higher.

Printing Changes=
===============================

------------------------

A new parameter "cups encrypt" has been added to control whether connections to CUPS servers will be encrypted or not. The default is to use unencrypted connections.

Winbind changes=
===============================

------------------------

The Winbind daemon has been refactored internally to be asynchronous. The new Winbind will not be blocked by running 'getent group' or 'getent passwd'.

VFS modules=
===============================

------------------------

A new VFS module "scannedonly" has been added. This is a filter that talks to an antivirus-engine and stores whether a file is clean or not. Users do only see clean files on their filesystem.

===============================
Changes
===============================

------------------------

smb.conf changes=
===============================

------------------------

   Parameter Name                      Description     Default
   --------------                      -----------     -------
   create krb5 conf		       New	       yes
   ctdb timeout			       New	       0
   cups encrypt			       New	       no
   debug hires timestamp	       Changed Default yes
   ldap deref			       New	       auto
   ldap follow referral		       New	       auto
   nmbd bind explicit broadcast	       New	       no
   wide links			       Changed Default no

New configure options=
===============================

------------------------

 --enable-external-libtdb	Enable external tdb
 --enable-netapi			Turn on netapi support
 --enable-pthreadpool		Enable pthreads pool helper support
 --with-cifsumount		Include umount.cifs (Linux only) support
 --with-codepagedir=DIR		Where to put codepages

----
(**Updated 01-March-2010**)

* Monday, March 1 - Samba 3.5.0 has been released
    http://www.samba.org/samba/history/samba-3.5.0.html Release Notes Samba 3.5.0]

----
`Category:Release Notes`