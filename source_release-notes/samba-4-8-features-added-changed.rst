Samba 4.8 Features added/changed
    <namespace>0</namespace>
<last_edited>2019-09-17T21:43:14Z</last_edited>
<last_editor>Fraz</last_editor>

Samba 4.8 is `Samba_Release_Planning#Discontinued_.28End_of_Life.29|**Discontinued (End of Life)**`.

Samba 4.8.12
------------------------

* Release Notes for Samba 4.8.12
* May 14, 2019

===============================
This is a security release in order to address the following defect:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16860 CVE-2018-16860] (Samba AD DC S4U2Self/S4U2Proxy unkeyed checksum)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16860 CVE-2018-16860]:
* The checksum validation in the S4U2Self handler in the embedded Heimdal KDC did not first confirm that the checksum was keyed, allowing replacement of the requested target (client) principal.

For more details and workarounds, please refer to the security advisory.

===============================
Changes since 4.8.11:
===============================

------------------------

*  Isaac Boukris <iboukris@gmail.com> 
* * [https://bugzilla.samba.org/show_bug.cgi?id=13685 BUG #13685]: CVE-2018-16860: Heimdal KDC: Reject PA-S4U2Self with unkeyed checksum.

 https://www.samba.org/samba/history/samba-4.8.12.html

Samba 4.8.11
------------------------

* Release Notes for Samba 4.8.11
* April 8, 2019

===============================
This is a security release in order to address the following defects:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3880 CVE-2019-3880] (Save registry file outside share as unprivileged user)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3880 CVE-2019-3880]: Authenticated users with write permission can trigger a symlink traversal to write or detect files outside the Samba share.

For more details and workarounds, please refer to the security advisorie. 
* [https://www.samba.org/samba/security/CVE-2019-3880.html CVE-2019-3880]

===============================
Changes since 4.8.10:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13851 BUG #13851]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3880 CVE-2019-3880]: rpc: winreg: Remove implementations of SaveKey/RestoreKey.

 https://www.samba.org/samba/history/samba-4.9.6.html

Samba 4.8.10
------------------------

* Release Notes for Samba 4.8.10
* April 4, 2019

===============================
This is the latest stable release of the Samba 4.8 release series.
===============================

------------------------

Please note that this will very likely be the last bugfix release of the Samba 4.8 release series. There will be security releases beyond this point only.

===============================
Changes since 4.8.9:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13690 BUG #13690]: smbd: uid: Don't crash if 'force group' is added to an existing share connection.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13770 BUG #13770]: s3: VFS: vfs_fruit. Fix the NetAtalk deny mode compatibility code.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13803 BUG #13803]: SMB1 POSIX mkdir does case insensitive name lookup.
*  Tim Beale <timbeale@catalyst.net.nz>
* * ldb: Bump ldb version to 1.3.7.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13686 BUG #13686]: 'samba-tool user syscpasswords' fails on a domain with many DCs.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13762 BUG #13762]: Performance regression in LDB one-level searches.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13776 BUG #13776]: tldap: Avoid use after free errors.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13802 BUG #13802]: Fix idmap xid2sid cache churn.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13812 BUG #13812]: access_check_max_allowed() doesn't process "Owner Rights" ACEs.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13746 BUG #13746]: s3-smbd: use fruit:model string for mDNS registration.
*  David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13766 BUG #13766]: Printcap still processed with "load printers" disabled.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13807 BUG #13807]: vfs_ceph strict_allocate_ftruncate calls (local FS) ftruncate and fallocate.
*  Joe Guo <joeg@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13728 BUG #13728]: netcmd/user: python[3]-gpgme unsupported and replaced by python[3]-gpg.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13520 BUG #13520]: Fix portability issues on freebsd.
*  Björn Jacke <bj@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13759 BUG #13759]: sambaundoguididx: Use the right escaped oder unescaped sam ldb files.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13786 BUG #13786]: messages_dgm: Properly handle receiver re-initialization.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13813 BUG #13813]: Fix idmap cache pollution with S-1-22- IDs on winbind hickup.
*  Gary Lockyer <gary@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13773 BUG #13773]: CVE-2019-3824 ldb: Release ldb 1.3.8, ldb: Out of bound read in ldb_wildcard_compare.
*  Marcos Mello <marcosfrm@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11568 BUG #11568]: Send status to systemd on daemon start.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13816 BUG #13816]: dbcheck in the middle of the tombstone garbage collection causes replication failures.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13818 BUG #13818]: An out of scope usage of a stack variable may cause corruption in EnumPrinter*.
*  Noel Power <noel.power@suse.com>
* * python/samba: Extra ndr_unpack needs bytes function.
*  Jiří Šašek <jiri.sasek@oracle.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13704 BUG #13704]: notifyd: Fix SIGBUS on sparc.
*  Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13787 BUG #13787]: waf: Check for libnscd.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13813 BUG #13813]: lib/winbind_util: Add winbind_xid_to_sid for --without-winbind.
* * passdb: Update ABI to 0.27.2.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13770 BUG #13770]: s3:vfs: Correctly check if OFD locks should be enabled or not.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13823 BUG #13823]: lib:util: Move debug message for mkdir failing to log level 1.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13832 BUG #13832]: Fix printing via smbspool backend with kerberos auth.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13847 BUG #13847]: s4:librpc: Fix installation of Samba.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13848 BUG #13848]: s3:lib: Fix the debug message for adding cache entries.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13848 BUG #13848]: s3:waf: Fix the detection of makdev() macro on Linux.
*  Zhu Shangzhong <zhu.shangzhong@zte.com.cn>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13839 BUG #13839]: ctdb: Initialize addr struct to zero before reparsing as IPV4.

 https://www.samba.org/samba/history/samba-4.8.10.html

Samba 4.8.9
------------------------

* Release Notes for Samba 4.8.9
* February 7, 2019

===============================
This is the latest stable release of the Samba 4.8 release series.
===============================

------------------------

Changes since 4.8.8:=
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11495 BUG #11495]: s3: lib: nmbname: Ensure we limit the NetBIOS name correctly. CID: 1433607.
*  Christian Ambach <ambi@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13199 BUG #13199]: s3:utils/smbget: Fix recursive download with empty source directories.
*  Tim Beale <timbeale@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13736 BUG #13736]: s3:libsmb: cli_smb2_list() can sometimes fail initially on a connection.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13747 BUG #13747]: join: Throw CommandError instead of Exception for simple errors.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13688 BUG #13688]: Windows 2016 fails to restore previous version of a file from a shadow_copy2 snapshot.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13455 BUG #13455]: Restoring previous version of stream with vfs_shadow_copy2 fails with NT_STATUS_OBJECT_NAME_INVALID.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13736 BUG #13736]: s3: libsmb: Use smb2cli_conn_max_trans_size() in cli_smb2_list().
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13708 BUG #13708]: s3-vfs: Prevent NULL pointer dereference in vfs_glusterfs.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13720 BUG #13720] : s3-smbd: Avoid assuming fsp is always intact after close_file call.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13725 BUG #13725]: s3-vfs-fruit,s3-vfs-streams_xattr: Add close call.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13774 BUG #13774]: s3-vfs: Add glusterfs_fuse vfs module.
*  Aaron Haslett <aaronhaslett@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13738 BUG #13738]: dns: Changing onelevel search for wildcard to subtree.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13722 BUG #13722]: s3:auth_winbind: Ignore a missing winbindd as NT4 PDC/BDC without trusts.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13723 BUG #13723]: s3:auth_winbind: Return NT_STATUS_NO_LOGON_SERVERS if winbindd is not available.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13752 BUG #13752]: s4:messaging: Add support 'smbcontrol <pid> debug/debuglevel'.
*  Anoop C S <anoopcs@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13330 BUG #13330]: vfs_glusterfs: Adapt to changes in libgfapi signatures.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13774 BUG #13774]: s3-vfs: Use ENOATTR in errno comparison for getxattr.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13717 BUG #13717]: lib/util: Count a trailing line that doesn't end in a newline.
*  Ralph Wuerthner <ralph.wuerthner@de.ibm.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13741 BUG #13741]: vfs_fileid: Fix get_connectpath_ino.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13744 BUG #13744]: vfs_fileid: Fix fsname_norootdir algorithm.

 https://www.samba.org/samba/history/samba-4.8.9.html

Samba 4.8.8
------------------------

* Release Notes for Samba 4.8.8
* December 13, 2018

===============================
This is the latest stable release of the Samba 4.8 release series.
===============================

------------------------

===============================
Major bug fixes include:
===============================

------------------------

* dns: Fix CNAME loop prevention using counter regression ([https://bugzilla.samba.org/show_bug.cgi?id=13600 BUG #13600]BUG 13600: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-14629 CVE-2018-14629]).

===============================
Changes since 4.8.7:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13633 BUG #13633]: s3: smbd: Prevent valgrind errors in smbtorture3 POSIX test.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13418 BUG #13418]: dsdb: Add comments explaining the limitations of our current backlink behaviour.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13495 BUG #13495]: dbcheck: Use symbolic control name for DSDB_CONTROL_DBCHECK_FIX_DUPLICATE_LINKS.
*  Tim Beale <timbeale@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13495 BUG #13495]: dbchecker: Fixing up incorrect DNs wasn't working.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9175 BUG #9175]: libcli/smb: Don't overwrite status code.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12164 BUG #12164]: 'wbinfo --group-info' 'NT AUTHORITY\System' does not work.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13175 BUG #13175]: Fix accessing ZFS snapshot directories over SMB.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13642 BUG #13642]: vfs_fruit should be able to cleanup AppleDouble files.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13465 BUG #13465]: testparm crashes with PANIC: Messaging not initialized on SLES 12 SP3.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13646 BUG #13646]: File saving issues with vfs_fruit on samba >= 4.8.5.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13649 BUG #13649]: Enabling vfs_fruit looses FinderInfo.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13661 BUG #13661]: Session setup reauth fails to sign response.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13667 BUG #13667]: Cancelling of SMB2 aio reads and writes returns wrong error NT_STATUS_INTERNAL_ERROR.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13677 BUG #13677]: Fix copy with vfs_fruit if AFP_AfpInfo stream file size > 60bytes.
*  Isaac Boukris <iboukris@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13571 BUG #13571]: CVE-2018-16853: Fix S4U2Self crash with MIT KDC build.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13641 BUG #13641]: Fix CTDB recovery record resurrection from inactive nodes and simplify vacuuming.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13659 BUG #13659]: Fix bugs in CTDB event handling.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13465 BUG #13465]: examples: Fix the smb2mount build.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13662 BUG #13662]: winbindd_cache: Fix timeout calculation for sid<->name cache.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13418 BUG #13418]: Extended DN SID component missing for member after switching group membership.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13600 BUG #13600]: CVE-2018-14629 dns: Fix CNAME loop prevention using counter regression.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13624 BUG #13624]: STATUS_SESSION_EXPIRED error is returned unencrypted, if the request was encrypted.
*  Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13465 BUG #13465]: testparm crashes with PANIC: Messaging not initialized on SLES 12 SP3.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13673 BUG #13673]: smbd: Fix DELETE_ON_CLOSE behaviour on files with READ_ONLY attribute.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13571 BUG #13571]: CVE-2018-16853: Fix S4U2Self crash with MIT KDC build.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13679 BUG #13679]: Fix a segfault in pyglue.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13670 BUG #13670]: ctdb-recovery: Ban a node that causes recovery failure.

 https://www.samba.org/samba/history/samba-4.8.8.html

Samba 4.8.7
------------------------

* Release Notes for Samba 4.8.7
* November 27, 2018

===============================
This is a security release in order to address the following defects:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-14629 CVE-2018-14629] Unprivileged adding of CNAME record causing loop in AD Internal DNS server
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16841 CVE-2018-16841] Double-free in Samba AD DC KDC with PKINIT
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16851 CVE-2018-16851] NULL pointer de-reference in Samba AD DC LDAP server
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16853 CVE-2018-16853] Samba AD DC S4U2Self crash in experimental MIT Kerberos configuration (unsupported)

===============================
Details
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-14629 CVE-2018-14629]:
* :All versions of Samba from 4.0.0 onwards are vulnerable to infinite query recursion caused by CNAME loops. Any dns record can be added via ldap by an unprivileged user using the ldbadd tool, so this is a security issue.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16841 CVE-2018-16841]:
* :When configured to accept smart-card authentication, Samba's KDC will call talloc_free() twice on the same memory if the principal in a validly signed certificate does not match the principal in the AS-REQ.

* :This is only possible after authentication with a trusted certificate.

* :talloc is robust against further corruption from a double-free with talloc_free() and directly calls abort(), terminating the KDC process.

* :There is no further vulnerability associated with this issue, merely a denial of service.

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16851 CVE-2018-16851]:
* :During the processing of an LDAP search before Samba's AD DC returns the LDAP entries to the client, the entries are cached in a single memory object with a maximum size of 256MB. When this size is reached, the Samba process providing the LDAP service will follow the NULL pointer, terminating the process.

* :There is no further vulnerability associated with this issue, merely a denial of service.

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16853 CVE-2018-16853]:
* :A user in a Samba AD domain can crash the KDC when Samba is built in the non-default MIT Kerberos configuration.

* :With this advisory we clarify that the MIT Kerberos build of the Samba AD DC is considered experimental.  Therefore the Samba Team will not issue security patches for this configuration.

For more details and workarounds, please refer to the security advisories.

===============================
Changes since 4.8.6:
===============================

------------------------

*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13628 BUG #13628]BUG 13628: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16841 CVE-2018-16841]: heimdal: Fix segfault on PKINIT with mis-matching principal.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13678 BUG #13678]BUG 13678: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16853 CVE-2018-16853]: build: The Samba AD DC, when build with MIT Kerberos is experimental
*  Aaron Haslett <aaronhaslett@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13600 BUG #13600]BUG 13600: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-14629 CVE-2018-14629]: dns: CNAME loop prevention using counter.
*  Garming Sam <garming@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13674 BUG #13674]BUG 13674: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16851 CVE-2018-16851]: ldap_server: Check ret before manipulating blob.

 https://www.samba.org/samba/history/samba-4.8.7.html

Samba 4.8.6
------------------------

* Release Notes for Samba 4.8.6
* October 9, 2018

===============================
This is the latest stable release of the Samba 4.8 release series.
===============================

------------------------

===============================
Changes since 4.8.5:
===============================

------------------------

*  Paulo Alcantara <paulo@paulo.ac>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13578 BUG #13578]: s3: util: Do not take over stderr when there is no log file.
*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13585 BUG #13585]: s3: smbd: Ensure get_real_filename() copes with empty pathnames.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13441 BUG #13441]: vfs_fruit: delete 0 byte size streams if AAPL is enabled.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13549 BUG #13549]: s3:smbd: Durable Reconnect fails because cookie.allow_reconnect is not set.
*  Alexander Bokovoy <ab@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13539 BUG #13539]: krb5-samba: Interdomain trust uses different salt principal.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13362 BUG #13362]: Fix possible memory leak in the Samba process.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13441 BUG #13441]: vfs_fruit: Don't unlink the main file.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13602 BUG #13602]: smbd: Fix a memleak in async search ask sharemode.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11517 BUG #11517]: Fix Samba GPO issue when Trust is enabled.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13539 BUG #13539]: samba-tool: Add virtualKerberosSalt attribute to 'user getpassword/syncpasswords'.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13606 BUG #13606]: wafsamba: Fix 'make -j<jobs>'.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13592 BUG #13592]: ctdbd logs an error until it can successfully connect to eventd.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13617 BUG #13617]: Fix race conditions in CTDB recovery lock.

 https://www.samba.org/samba/history/samba-4.8.6.html

Samba 4.8.5
------------------------

* Release Notes for Samba 4.8.5
* August 24, 2018

===============================
This is the latest stable release of the Samba 4.8 release series.
===============================

------------------------

===============================
Changes since 4.8.4:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13474 BUG #13474]: python: pysmbd: Additional error path leak fix.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13511 BUG #13511]: libsmbclient: Initialize written value before use.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13519 BUG #13519]: ldb: Refuse to build Samba against a newer minor version of ldb.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13527 BUG #13527]: s3: libsmbclient: Fix cli_splice() fallback when reading less than a complete file.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13537 BUG #13537]: Using "sendfile = yes" with SMB2 can cause CPU spin.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13575 BUG #13575]: ldb: Release LDB 1.3.6.
*  Bailey Berro <baileyberro@chromium.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13511 BUG #13511]: libsmbclient: Initialize written in cli_splice_fallback().
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13318 BUG #13318]: Durable Handles reconnect fails in a cluster when the cluster fs uses different device ids.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13351 BUG #13351]: s3: smbd: Always set vuid in check_user_ok().
* * [https://bugzilla.samba.org/show_bug.cgi?id=13441 BUG #13441]: vfs_fruit: Delete 0 byte size streams if AAPL is enabled.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13451 BUG #13451]: Fail renaming file if that file has open streams.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13505 BUG #13505]: lib: smb_threads: Fix access before init bug.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13535 BUG #13535]: s3: smbd: Fix path check in smbd_smb2_create_durable_lease_check().
*  Alexander Bokovoy <ab@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13538 BUG #13538]: samba-tool trust: Support discovery via netr_GetDcName.
*  Samuel Cabrero <scabrero@suse.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13540 BUG #13540]: ctdb_mutex_ceph_rados_helper: Set SIGINT signal handler.
*  David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13506 BUG #13506]: vfs_ceph: Don't lie about flock support.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13540 BUG #13540]: Fix deadlock with ctdb_mutex_ceph_rados_helper.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13493 BUG #13493]: ctdb: Fix build on FreeBSD and AIX.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13553 BUG #13553]: libsmb: Fix CID 1438243 (Unchecked return value), CID 1438244 (Unsigned compared against 0), CID 1438245 (Dereference before null check), CID 1438246 (Unchecked return value).
* * [https://bugzilla.samba.org/show_bug.cgi?id=13584 BUG #13584]: vfs_fruit: Fix a panic if fruit_access_check detects a locking conflict.
*  Gary Lockyer <gary@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13536 BUG #13536]: The current position in the dns name was not advanced past the '.' character.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13308 BUG #13308]: samba-tool domain trust: Fix trust compatibility to Windows Server 1709 and FreeIPA.
*  Oleksandr Natalenko <oleksandr@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13559 BUG #13559]: systemd: Only start smb when network interfaces are up.
*  Noel Power <noel.power@suse.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13553 BUG #13553]: Fix quotas with SMB2.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13563 BUG #13563]: s3/smbd: Ensure quota code is only called when quota support detected.
*  Anoop C S <anoopcs@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13204 BUG #13204]: s3/libsmb: Explicitly set delete_on_close token for rmdir.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13489 BUG #13489]: krb5_plugin: Install plugins to krb5 modules dir.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13503 BUG #13503]: s3:winbind: Do not lookup local system accounts in AD.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13499 BUG #13499]: Don't use CTDB_BROADCAST_VNNMAP.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13500 BUG #13500]: ctdb-daemon: Only consider client ID for local database attach.
*  Justin Stephenson <jstephen@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13485 BUG #13485]: s3:client: Add "--quiet" option to smbclient.
*  Ralph Wuerthner <ralph.wuerthner@de.ibm.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13568 BUG #13568]: s3: vfs: time_audit: Fix handling of token_blob in smb_time_audit_offload_read_recv().

 https://www.samba.org/samba/history/samba-4.8.5.html

Samba 4.8.4
------------------------

* Release Notes for Samba 4.8.4
* August 14, 2018

===============================
This is a security release in order to address the following defects:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1139 CVE-2018-1139] (Weak authentication protocol allowed.)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1140 CVE-2018-1140] (Denial of Service Attack on DNS and LDAP server.)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-10858 CVE-2018-10858] (Insufficient input validation on client directory listing in libsmbclient.)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-10918 CVE-2018-10918] (Denial of Service Attack on AD DC DRSUAPI server.)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-10919 CVE-2018-10919] (Confidential attribute disclosure from the AD LDAP server.)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1139 CVE-2018-1139]: Vulnerability that allows authentication via NTLMv1 even if disabled.
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1140 CVE-2018-1140]: Missing null pointer checks may crash the Samba AD DC, both over DNS and LDAP.
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-10858 CVE-2018-10858]: A malicious server could return a directory entry that could corrupt libsmbclient memory.
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-10918 CVE-2018-10918]: Missing null pointer checks may crash the Samba AD DC, over the authenticated DRSUAPI RPC service.
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-10919 CVE-2018-10919]: Missing access control checks allow discovery of confidential attribute values via authenticated LDAP search expressions.

===============================
Changes since 4.8.3:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13453 BUG #13453]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-10858 CVE-2018-10858]: libsmb: Harden smbc_readdir_internal() against returns from malicious servers.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13374 BUG #13374]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1140 CVE-2018-1140]: ldbsearch '(distinguishedName=abc)' and DNS query with escapes crashes, ldb: Release LDB 1.3.5 for CVE-2018-1140
* * [https://bugzilla.samba.org/show_bug.cgi?id=13552 BUG #13552]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-10918 CVE-2018-10918]: cracknames: Fix DoS (NULL pointer de-ref) when not servicePrincipalName is set on a user.
*  Tim Beale <timbeale@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13434 BUG #13434]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-10919 CVE-2018-10919]: acl_read: Fix unauthorized attribute access via searches.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13360 BUG #13360]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1139 CVE-2018-1139] libcli/auth: Do not allow ntlmv1 over SMB1 when it is disabled via "ntlm auth".
*  Andrej Gessel <Andrej.Gessel@janztec.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13374 BUG #13374]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1140 CVE-2018-1140] Add NULL check for ldb_dn_get_casefold() in ltdb_index_dn_attr().

 https://www.samba.org/samba/history/samba-4.8.4.html

Samba 4.8.3
------------------------

* Release Notes for Samba 4.8.3
* June 26, 2018

===============================
This is the latest stable release of the Samba 4.8 release series.
===============================

------------------------

===============================
Changes since 4.8.2:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13428 BUG #13428]: s3: smbd: Fix SMB2-FLUSH against directories.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13457 BUG #13457]: s3: smbd: printing: Re-implement delete-on-close semantics for print files missing since 3.5.x.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13474 BUG #13474]: python: Fix talloc frame use in make_simple_acl().
*  Jeffrey Altman <jaltman@secure-endpoints.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11573 BUG #11573]: heimdal: lib/krb5: Do not fail set_config_files due to parse error.
*  Andrew Bartlett <abartlet@samba.org>
* * ldb: version 1.3.4
* * [https://bugzilla.samba.org/show_bug.cgi?id=13448 BUG #13448]: ldb: One-level search was incorrectly falling back to full DB scan.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13452 BUG #13452]: ldb: Save a copy of the index result before calling the callbacks.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13454 BUG #13454]: No Backtrace given by Samba's AD DC by default.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13471 BUG #13471]: ldb_tdb: Use mem_ctx and so avoid leak onto long-term memory on duplicated add.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13432 BUG #13432]: s3:smbd: Fix interaction between chown and SD flags.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13437 BUG #13437]: Fix building Samba with gcc 8.1.
*  Andrej Gessel <Andrej.Gessel@janztec.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13475 BUG #13475]: Fix several mem leaks in ldb_index ldb_search ldb_tdb.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13331 BUG #13331]: libgpo: Fix the build --without-ads.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13369 BUG #13369]: Looking up the user using the UPN results in user name with the REALM instead of the DOMAIN.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13427 BUG #13427]: Fix broken server side GENSEC_FEATURE_LDAP_STYLE handling (NTLM  NTLM2 packet check failed due to invalid signature!).
*  Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13446 BUG #13446]: smbd: Flush dfree memcache on service reload.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13478 BUG #13478]: krb5_wrap: Fix keep_old_entries logic for older Kerberos libraries.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13369 BUG #13369]: Looking up the user using the UPN results in user name with the REALM instead of the DOMAIN.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13437 BUG #13437]: Fix building Samba with gcc 8.1.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13440 BUG #13440]: s3:utils: Do not segfault on error in DoDNSUpdate().
* * [https://bugzilla.samba.org/show_bug.cgi?id=13480 BUG #13480]: krb5_plugin: Add winbind localauth plugin for MIT Kerberos.
*  Lukas Slebodnik <lslebodn@fedoraproject.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13459 BUG #13459]: ldb: Fix memory leak on module context.

 https://www.samba.org/samba/history/samba-4.8.3.html

Samba 4.8.2
------------------------

* Release Notes for Samba 4.8.2
* May 16, 2018

===============================
This is the latest stable release of the Samba 4.8 release series.
===============================

------------------------

===============================
Major bug fixes include:
===============================

------------------------

* After update to 4.8.0 DC failed with "Failed to find our own NTDS Settings objectGUID" ([https://bugzilla.samba.org/show_bug.cgi?id=13335 BUG #13335]).

===============================
Changes since 4.8.1:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13380 BUG #13380]: s3: smbd: Generic fix for incorrect reporting of stream dos attributes on a directory.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13412 BUG #13412]: ceph: VFS: Add asynchronous fsync to ceph module, fake using synchronous call.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13419 BUG #13419]: s3: libsmbclient: Fix hard-coded connection error return of ETIMEDOUT.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13306 BUG #13306]: ldb: Release ldb 1.3.3:
* :* Fix failure to upgrade to the GUID index DB format.
* :* Add tests for GUID index behaviour.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13420 BUG #13420]: s4-lsa: Fix use-after-free in LSA server.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13430 BUG #13430]: winbindd: Do re-connect if the RPC call fails in the passdb case.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13416 BUG #13416]: s3:cleanupd: Sends MSG_SMB_UNLOCK twice to interested peers.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13414 BUG #13414]: s3:cleanupd: Use MSG_SMB_BRL_VALIDATE to signal cleanupd unclean process shutdown.
*  David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13425 BUG #13425]: vfs_ceph: add fake async pwrite/pread send/recv hooks.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13411 BUG #13411]: ctdb-client: Remove ununsed functions from old client code.
*  Björn Jacke <bjacke@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13395 BUG #13395]: printing: Return the same error code as windows does on upload failures.
*  Gary Lockyer <gary@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13335 BUG #13335]: After update to 4.8.0 DC failed with "Failed to find our own NTDS Settings objectGUID".
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13400 BUG #13400]: nsswitch: Fix memory leak in winbind_open_pipe_sock() when the privileged pipe is not accessable.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13420 BUG #13420]: s4:lsa_lookup: remove TALLOC_FREE(state) after all dcesrv_lsa_Lookup{Names,Sids}_base_map() calls.
*  Vandana Rungta <vrungta@amazon.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13424 BUG #13424]: s3: VFS: Fix memory leak in vfs_ceph.
*  Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13407 BUG #13407]: rpc_server: Fix NetSessEnum with stale sessions.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13417 BUG #13417]: s3:smbspool: Fix cmdline argument handling.

 https://www.samba.org/samba/history/samba-4.8.2.html

Samba 4.8.1
------------------------

* Release Notes for Samba 4.8.1
* April 26, 2018

===============================
This is the latest stable release of the Samba 4.8 release series.
===============================

------------------------

===============================
Changes since 4.8.0:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13244 BUG #13244]: s3: ldap: Ensure the ADS_STRUCT pointer doesn't get freed on error, we don't own it here.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13270 BUG #13270]: s3: smbd: Fix possible directory fd leak if the underlying OS doesn't support fdopendir().
* * [https://bugzilla.samba.org/show_bug.cgi?id=13319 BUG #13319]: Round-tripping ACL get/set through vfs_fruit will increase the number of ACE entries without limit.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13347 BUG #13347]: s3: smbd: SMB2: Add DBGC_SMB2_CREDITS class to specifically debug credit issues.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13358 BUG #13358]: s3: smbd: Files or directories can't be opened DELETE_ON_CLOSE without delete access.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13372 BUG #13372]: s3: smbd: Fix memory leak in vfswrap_getwd().
* * [https://bugzilla.samba.org/show_bug.cgi?id=13375 BUG #13375]: s3: smbd: Unix extensions attempts to change wrong field in fchown call.
*  Björn Baumbach <bb@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13337 BUG #13337]: ms_schema/samba-tool visualize: Fix python2.6 incompatibility.
*  Timur I. Bakeyev <timur@iXsystems.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13352 BUG #13352]: Fix invocation of gnutls_aead_cipher_encrypt().
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13328 BUG #13328]: Windows 10 cannot logon on Samba NT4 domain.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13332 BUG #13332]: winbindd: Recover loss of netlogon secure channel in case the peer DC is rebooted.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13363 BUG #13363]: s3:smbd: Don't use the directory cache for SMB2/3.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13356 BUG #13356]: ctdb-client: Fix bugs in client code.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13359 BUG #13359]: ctdb-scripts: Drop "net serverid wipe" from 50.samba event script.
*  Lutz Justen <ljusten@google.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13368 BUG #13368]: s3: lib: messages: Don't use the result of sec_init() before calling sec_init().
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13273 BUG #13273]: libads: Fix the build '--without-ads'.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13332 BUG #13332]: winbind: Keep "force_reauth" in invalidate_cm_connection, add 'smbcontrol disconnect-dc'.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13343 BUG #13343]: vfs_virusfilter: Fix CIDs 1428738-1428740.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13367 BUG #13367]: dsdb: Fix CID 1034966 Uninitialized scalar variable.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13370 BUG #13370]: rpc_server: Fix core dump in dfsgetinfo.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13382 BUG #13382]: smbclient: Fix notify.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13215 BUG #13215]: Fix smbd panic if the client-supplied channel sequence number wraps.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13328 BUG #13328]: Windows 10 cannot logon on Samba NT4 domain.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13342 BUG #13342]: lib/util: Remove unused '#include <sys/syscall.h>' from tests/tfork.c.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13343 BUG #13343]:	Fix build errors with cc from developerstudio 12.5 on Solaris.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13344 BUG #13344]: Fix the picky-developer build on FreeBSD 11.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13345 BUG #13345]: s3:modules: Fix the build of vfs_aixacl2.c.
*  Anton Nefedov
* * [https://bugzilla.samba.org/show_bug.cgi?id=13338 BUG #13338]: s3:smbd: map nterror on smb2_flush errorpath.
*  Noel Power <noel.power@suse.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13341 BUG #13341]: lib:replace: Fix linking when libtirpc-devel overwrites system headers.
*  Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13312 BUG #13312]: winbindd: 'wbinfo --name-to-sid' returns misleading result on invalid query.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13376 BUG #13376]: s3:passdb: Do not return OK if we don't have pinfo set up.
*  Eric Vannier <evannier@google.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13302 BUG #13302]: Allow AESNI to be used on all processor supporting AESNI.

 https://www.samba.org/samba/history/samba-4.8.1.html

Samba 4.8.0
------------------------

<onlyinclude>
* Release Notes for Samba 4.8.0
* March 13, 2018

===============================
Release Announcements
===============================

------------------------

This is the first stable release of the Samba 4.8 release series.
Please read the release notes carefully before upgrading.

===============================
UPGRADING
===============================

------------------------

New GUID Index mode in sam.ldb for the AD DC=
===============================

------------------------

Users who upgrade a Samba AD DC in-place will experience a short delay in the first startup of Samba while the sam.ldb is re-indexed.

Unlike in previous releases a transparent downgrade is not possible. If you wish to downgrade such a DB to a Samba 4.7 or earlier version, please run the source4/scripting/bin/sambaundoguididx script first.

Domain member setups require winbindd=
===============================

------------------------

Setups with "security = domain" or "security = ads" require a running 'winbindd' now. The fallback that smbd directly contacts domain controllers is gone.

smbclient reparse point symlink parameters reversed=
===============================

------------------------

See the more detailed description below.

Changed trusted domains listing with wbinfo -m --verbose=
===============================

------------------------

See the more detailed description below.

===============================
NEW FEATURES/CHANGES
===============================

------------------------

New GUID Index mode in sam.ldb for the AD DC=
===============================

------------------------

The new layout used for sam.ldb is GUID, rather than DN oriented. This provides Samba's Active Directory Domain Controller with a faster database, particularly at larger scale.

The underlying DB is still TDB, simply the choice of key has changed.

The new mode is not optional, so no configuration is required.  Older Samba versions cannot read the new database (see the upgrade note above).

KDC GPO application=
===============================

------------------------

Adds Group Policy support for the Samba kdc. Applies password policies (minimum/maximum password age, minimum password length, and password complexity) and kerberos policies (user/service ticket lifetime and renew lifetime).

Adds the samba_gpoupdate script for applying and unapplying policy. Can be applied automatically by setting

 'apply group policies = yes'.

Time Machine Support with vfs_fruit=
===============================

------------------------

Samba can be configured as a Time Machine target for Apple Mac devices through the vfs_fruit module. When enabling a share for Time Machine support the relevant Avahi records to support discovery will be published for installations that have been built against the Avahi client library.

Shares can be designated as a Time Machine share with the following setting:

    fruit:time machine = yes'

Support for lower casing the MDNS Name=
===============================

------------------------

Allows the server name that is advertised through MDNS to be set to the hostname rather than the Samba NETBIOS name. This allows an administrator to make Samba registered MDNS records match the case of the hostname rather than being in all capitals.

This can be set with the following settings:

    mdns name = mdns'

Encrypted secrets=
===============================

------------------------

Attributes deemed to be sensitive are now encrypted on disk. The sensitive values are currently:
	pekList
	msDS-ExecuteScriptPassword
	currentValue
	dBCSPwd
	initialAuthIncoming
	initialAuthOutgoing
	lmPwdHistory
	ntPwdHistory
	priorValue
	supplementalCredentials
	trustAuthIncoming
	trustAuthOutgoing
	unicodePwd
	clearTextPassword

This encryption is enabled by default on a new provision or join, it can be disabled at provision or join time with the new option '--plaintext-secrets'.

However, an in-place upgrade will not encrypt the database.

Once encrypted, it is not possible to do an in-place downgrade (eg to 4.7) of the database. To obtain an unencrypted copy of the database a new DC join should be performed, specifying the '--plaintext-secrets' option.

The key file "encrypted_secrets.key" is created in the same directory as the database and should NEVER be disclosed.  It is included by the samba_backup script.

Active Directory replication visualisation=
===============================

------------------------

To work out what is happening in a replication graph, it is sometimes helpful to use visualisations. We introduce a samba-tool subcommand to write Graphviz dot output and generate text-based heatmaps of the distance in hops between DCs.

There are two subcommands, two graphical modes, and (roughly) two modes of operation with respect to the location of authority.

*'samba-tool visualize ntdsconn' looks at NTDS Connections.
*'samba-tool visualize reps' looks at repsTo and repsFrom objects.

In '--distance' mode (default), the distances between DCs are shown in a matrix in the terminal. With '--color=yes', this is depicted as a heatmap. With '--utf8' it is a lttle prettier.

In '--dot' mode, Graphviz dot output is generated. When viewed using dot or xdot, this shows the network as a graph with DCs as vertices and connections edges. Certain types of degenerate edges are shown in different colours or line-styles.

smbclient reparse point symlink parameters reversed=
===============================

------------------------

A bug in smbclient caused the 'symlink' command to reverse the meaning of the new name and link target parameters when creating a reparse point symlink against a Windows server. As this is a little used feature the ordering of these parameters has been reversed to match the parameter ordering of the UNIX extensions 'symlink' command. The usage message for this command has also been improved to remove confusion.

Winbind changes=
===============================

------------------------

The dependency to global list of trusted domains within the winbindd processes has been reduced a lot.

The construction of that global list is not reliable and often incomplete in complex trust setups. In most situations the list is not needed any more for winbindd to operate correctly. E.g. for plain file serving via SMB using a simple idmap setup with autorid, tdb or ad. However some more complex setups require the list, e.g. if you specify idmap backends for specific domains. Some pam_winbind setups may also require the global list.

If you have a setup that doesn't require the global list, you should set "winbind scan trusted domains = no".

Improved support for trusted domains (as AD DC)=
===============================

------------------------

The support for trusted domains/forests has improved a lot.

External domain trusts, as well a transitive forest trusts, are supported in both directions (inbound and outbound) for Kerberos and NTLM authentication now.

The LSA LookupNames and LookupSids implementations support resolving names and sids from trusts domains/forest now. This is important in order to allow Samba based domain members to make use of the trust.

However there are currently still a few limitations:

* It's not possible to add users/groups of a trusted domainvinto domain groups. So group memberships are not expanded on trust boundaries.
* :See https://bugzilla.samba.org/show_bug.cgi?id=13300
* Both sides of the trust need to fully trust each other!
* No SID filtering rules are applied at all!
* This means DCs of domain A can grant domain admin rights in domain B.
* Selective (CROSS_ORIGANIZATION) authentication is not supported. It's possible to create such a trust, but the KDC and winbindd ignore them.

Changed trusted domains listing with wbinfo -m --verbose=
===============================

------------------------

The trust properties printed by wbinfo -m --verbose have been changed to correctly reflect the view of the system where wbinfo is executed.

The trust type field in particular can show additional values that correctly reflect the type of the trust: "Local" for the local SAM and BUILTIN, "Workstation" for a workstation trust to the primary domain, "RWDC" for the SAM on a AD DC, "RODC" for the SAM on a read-only DC, "PDC" for the SAM on a NT4-style DC, "Forest" for a AD forest trust and "External" for quarantined, external or NT4-style trusts.

Indirect trusts are shown as "Routed" including the routing domain.

Example, on a AD DC (SDOM1):

 Domain Name DNS Domain          Trust Type  Transitive  In   Out
 BUILTIN                         Local
 SDOM1       sdom1.site          RWDC
 WDOM3       wdom3.site          Forest      Yes         No   Yes
 WDOM2       wdom2.site          Forest      Yes         Yes  Yes
 SUBDOM31    subdom31.wdom3.site Routed (via WDOM3)
 SUBDOM21    subdom21.wdom2.site Routed (via WDOM2)

Same setup, on a member of WDOM2:

 Domain Name DNS Domain          Trust Type  Transitive  In   Out
 BUILTIN                         Local
 TITAN                           Local
 WDOM2       wdom2.site          Workstation Yes         No   Yes
 WDOM1       wdom1.site          Routed (via WDOM2)
 WDOM3       wdom3.site          Routed (via WDOM2)
 SUBDOM21    subdom21.wdom2.site Routed (via WDOM2)
 SDOM1       sdom1.site          Routed (via WDOM2)
 SUBDOM11    subdom11.wdom1.site Routed (via WDOM2)

The list of trusts may be incomplete and additional domains may appear as "Routed" if a user of an unknown domain is successfully authenticated.

VirusFilter VFS module=
===============================

------------------------

This new module integrates with Sophos, F-Secure and ClamAV anti-virus software to provide scanning and filtering of files on a Samba share.

===============================
REMOVED FEATURES
===============================

------------------------

'net serverid' commands removed=
===============================

------------------------

The two commands 'net serverid list' and 'net serverid wipe' have been removed, because the file serverid.tdb is not used anymore.

'net serverid list' can be replaced by listing all files in the subdirectory "msg.lock" of Samba's "lock directory". The unique id listed by 'net serverid list' is stored in every process' lockfile in "msg.lock".

'net serverid wipe' is not necessary anymore. It was meant primarily for clustered environments, where the serverid.tdb file was not properly cleaned up after single node crashes. Nowadays smbd and winbind take care of cleaning up the msg.lock and msg.sock directories automatically.

NT4-style replication based net commands removed=
===============================

------------------------

The following commands and sub-commands have been removed from the "net" utility:

*net rpc samdump
*net rpc vampire ldif

Also, replicating from a real NT4 domain with "net rpc vampire" and "net rpc vampire keytab" has been removed.

The NT4-based commands were accidentally broken in 2013, and nobody noticed the breakage. So instead of fixing them including tests (which would have meant writing a server for the protocols, which we don't have) we decided to remove them.

For the same reason, the "samsync", "samdeltas" and "database_redo" commands have been removed from rpcclient.

"net rpc vampire keytab" from Active Directory domains continues to be supported.

vfs_aio_linux module removed=
===============================

------------------------

The current Linux kernel aio does not match what Samba would do. Shipping code that uses it leads people to false assumptions. Samba implements async I/O based on threads by default, there is no special module required to see benefits of read and write request being sent do the disk in parallel.

===============================
smb.conf changes
===============================

------------------------

    arameter Name                     Description             Default
    -------------                     -----------             -------
    pply group policies               New                     no
    uth methods                       Removed
    inddns dir                        New
    lient schannel                    Default changed/        yes
                                     Deprecated
    po update command                 New
    dap ssl ads                       Deprecated
    ap untrusted to domain            Removed
    plock contention limit            Removed
    refork children                   New                     1
    dns name                          New                     netbios
    ruit:time machine                 New                     false
    rofile acls                       Removed
    se spnego                         Removed
    erver schannel                    Default changed/        yes
                                     Deprecated
    nicode                            Deprecated
    inbind scan trusted domains       New                     yes
    inbind trusted domains only       Removed
</onlyinclude>

===============================
KNOWN ISSUES
===============================

------------------------

* `Release_Planning_for_Samba_4.8#Release_blocking_bugs`

===============================
CHANGES SINCE 4.8.0rc4
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11343 BUG #11343]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1050 CVE-2018-1050]: Codenomicon crashes in spoolss server code.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13272 BUG #13272]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1057 CVE-2018-1057]: Unprivileged user can change any user (and admin) password.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13272 BUG #13272]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1057 CVE-2018-1057]: Unprivileged user can change any user (and admin) password.
*  Dan Robertson <drobertson@tripwire.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13310 BUG #13310] : libsmb: Use smb2 tcon if conn_protocol >= SMB2_02.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13315 BUG #13315]: s3:smbd: Do not crash if we fail to init the session table.

===============================
CHANGES SINCE 4.8.0rc3
===============================

------------------------

*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13287 BUG #13287]: Fix numerous trust related bugs in winbindd and s4 LSA RPC server.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13296 BUG #13296]: vfs_fruit: Use off_t, not size_t for TM size calculations.
*  Alexander Bokovoy <ab@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13304 BUG #13304]: mit-kdb: Support MIT Kerberos 1.16 KDB API changes.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13277 BUG #13277]: build: Fix libceph-common detection.
*  Poornima G <pgurusid@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13297 BUG #13297]: vfs_glusterfs: Fix the wrong pointer being sent in glfs_fsync_async.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13305 BUG #13305]: vfs_fileid: Fix the 32-bit build.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13206 BUG #13206]: Unable to authenticate with an empty string domain *.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13276 BUG #13276]: configure aborts without libnettle/gnutls.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13278 BUG #13278]: winbindd (on an AD DC) should only use netlogon/lsa against trusted domains.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13287 BUG #13287]: Fix numerous trust related bugs in winbindd and s4 LSA RPC server.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13290 BUG #13290]: A disconnecting winbind client can cause a problem in  the winbind parent child communication.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13291 BUG #13291]: tevent: version 0.9.36.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13292 BUG #13292]: winbind requests could get stuck in the queue of a busy child, while later requests could get served fine by other children.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13293 BUG #13293]: Minimize the lifetime of winbindd_cli_state->{pw,gr}ent_state.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13294 BUG #13294]: Avoid using fstrcpy(domain->dcname,...) on a char *.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13295 BUG #13295]: winbind parent should find the dc of a foreign domain via the primary domain.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13299 BUG #13299]: Disable support for CROSS_ORGANIZATION domains.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13306 BUG #13306]: ldb: version 1.3.2.
*  Sachin Prabhu <sprabhu@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13303 BUG #13303]: vfs_glusterfs: Add fallocate support for vfs_glusterfs.
*  Garming Sam <garming@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13031 BUG #13031]: subnet: Avoid a segfault when renaming subnet objects.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13269 BUG #13269]: RODC may skip objects during replication due to naming conflicts.

===============================
CHANGES SINCE 4.8.0rc2
===============================

------------------------

*  Trever L. Adams <trever.adams@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13246 BUG #13246]: Backport Samba VirusFilter.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13228 BUG #13228]: dbcheck: Add support for restoring missing forward links.
*  Güther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13221 BUG #13221]: python: fix the build with python3.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13228 BUG #13228]: dbcheck: Add support for restoring missing forward links.

===============================
CHANGES SINCE 4.8.0rc1
===============================

------------------------

*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13227 BUG #13227]: packaging: Fix default systemd-dir path.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13238 BUG #13238]: build: Deal with recent glibc sunrpc header removal.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13228 BUG #13228]: repl_meta_data: fix linked attribute corruption on databases with unsorted links on expunge.
*  Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13217 BUG #13217]: s3/smbd: Remove file system sharemode before calling unlink.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13209 BUG #13209]: Small improvements in winbindd for the resource cleanup in error cases.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13238 BUG #13238]: Make Samba work with tirpc and libnsl2.

 https://download.samba.org/pub/samba/rc/samba-4.8.0rc3.WHATSNEW.txt

----
`Category:Release Notes`