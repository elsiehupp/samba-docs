Samba 4.3 Features added/changed
    <namespace>0</namespace>
<last_edited>2019-09-17T22:02:56Z</last_edited>
<last_editor>Fraz</last_editor>

Samba 4.3 is `Samba_Release_Planning#Discontinued_.28End_of_Life.29|**Discontinued (End of Life)**`.

Samba 4.3.13
------------------------

* Release Notes for Samba 4.3.13
* December 19, 2016

===============================
This is a security release in order to address the following defects:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2123 CVE-2016-2123] (Samba NDR Parsing ndr_pull_dnsp_name Heap-based Buffer Overflow Remote Code Execution Vulnerability).
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2125 CVE-2016-2125] (Unconditional privilege delegation to Kerberos servers in trusted realms).
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2126 CVE-2016-2126] (Flaws in Kerberos PAC validation can trigger privilege elevation).

Details=
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2123 CVE-2016-2123]:
* The Samba routine ndr_pull_dnsp_name contains an integer wrap problem, leading to an attacker-controlled memory overwrite. ndr_pull_dnsp_name parses data from the Samba Active Directory ldb database.  Any user who can write to the dnsRecord attribute over LDAP can trigger this memory corruption.

* By default, all authenticated LDAP users can write to the dnsRecord attribute on new DNS objects. This makes the defect a remote privilege escalation.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2125 CVE-2016-2125]
* Samba client code always requests a forwardable ticket when using Kerberos authentication. This means the target server, which must be in the current or trusted domain/realm, is given a valid general purpose Kerberos "Ticket Granting Ticket" (TGT), which can be used to fully impersonate the authenticated user or service.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2126 CVE-2016-2126]
* A remote, authenticated, attacker can cause the winbindd process to crash using a legitimate Kerberos ticket due to incorrect handling of the arcfour-hmac-md5 PAC checksum.

* A local service with access to the winbindd privileged pipe can cause winbindd to cache elevated access permissions.

===============================
Changes since 4.3.13:
===============================

------------------------

*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12409 BUG #12409]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2123 CVE-2016-2123]: Fix DNS vuln ZDI-CAN-3995.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12445 BUG #12445]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2125 CVE-2016-2125]: Don't send delegated credentials to all servers.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12446 BUG #12446]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2126 CVE-2016-2126]: auth/kerberos: Only allow known checksum types in check_pac_checksum().

 https://www.samba.org/samba/history/samba-4.3.13.html

Samba 4.3.12
------------------------

* Release Notes for Samba 4.3.12
* November 3, 2016

===============================
This is the last bug-fix release of Samba 4.3. There will be only security updates beyond this point.
===============================

------------------------

Major enhancements in Samba 4.3.12 include:=
===============================

------------------------

*  Let winbindd discard expired kerberos tickets when built against (internal) heimdal ([https://bugzilla.samba.org/show_bug.cgi?id=12369 BUG #12369]).
*  REGRESSION: smbd segfaults on startup, tevent context being freed ([https://bugzilla.samba.org/show_bug.cgi?id=12283 BUG #12283]).
===============================
Changes since 4.3.11:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11838 BUG #11838]:  s4: ldb: Ignore case of "range" in sscanf as we've already checked for its presence.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12021 BUG #12021]: Fix smbd crash (Signal 4) on File Delete.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12135 BUG #12135]: libgpo: Correctly use the 'server' parameter after parsing it out of the GPO path.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12139 BUG #12139]: s3: oplock: Fix race condition when closing an oplocked file.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12272 BUG #12272]: Fix messaging subsystem crash.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12283 BUG #12283]: REGRESSION: smbd segfaults on startup, tevent context being freed.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12025 BUG #12025]: param: Correct the defaults for "dcerpc endpoint services".
* * [https://bugzilla.samba.org/show_bug.cgi?id=12026 BUG #12026]: build: Always build eventlog6.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12154 BUG #12154]: ldb-samba: Add "secret" as a value to hide in LDIF files.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12178 BUG #12178]: dbcheck: Abandon dbcheck if we get an error during a transaction.
*  Björn Baumbach <bb@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=8618 BUG #8618]: s3-printing: Fix migrate printer code.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11801 BUG #11801]: Fix crash in mdssvc with older glib2 versions.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11961 BUG #11961]: idmap_autorid allocates ids for unknown SIDs from other backends.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12005 BUG #12005]: smbd: Ignore ctdb tombstone records in fetch_share_mode_unlocked_parser().
* * [https://bugzilla.samba.org/show_bug.cgi?id=12016 BUG #12016]: cleanupd terminates main smbd on exit.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12028 BUG #12028]: vfs_acl_xattr: Objects without NT ACL xattr.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12105 BUG #12105]: async_req: Make async_connect_send() "reentrant".
* * [https://bugzilla.samba.org/show_bug.cgi?id=12177 BUG #12177]: vfs_acl_common: Fix unexpected synthesized default ACL from vfs_acl_xattr.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12181 BUG #12181]: vfs_acl_xattr|tdb: Enforced settings when "ignore system acls = yes".
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12285 BUG #12285]: s3-spoolss: Fix winreg_printer_ver_to_qword.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11770 BUG #11770]: Reset TCP Connections during IP failover.
*  Volker Lendecke <vl@samba.org>
* * glusterfs: Avoid tevent_internal.h.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11994 BUG #11994]: gensec/spnego: Work around missing server mechListMIC in SMB servers.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12268 BUG #12268]: smbd: Reset O_NONBLOCK on open files.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12374 BUG #12374]: spoolss: Fix caching of printername->sharename.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12045 BUG #12045]: gencache: Bail out of stabilize if we can not get the allrecord lock.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12007 BUG #12007]: libads: Ensure the right ccache is used during spnego bind.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12129 BUG #12129]: samba-tool/ldapcmp: Ignore differences of whenChanged.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12283 BUG #12283]: REGRESSION: smbd segfaults on startup, tevent context being freed.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12369 BUG #12369]: Let winbindd discard expired kerberos tickets when built against(internal) heimdal.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12106 BUG #12106]: ctdb-scripts: Fix regression in updateip code.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11860 BUG #11860]: ctdb-daemon: Fix several Coverity IDs.
*  Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12006 BUG #12006]: auth: Fix a memory leak in gssapi_get_session_key().
* * [https://bugzilla.samba.org/show_bug.cgi?id=11860 BUG #12149]: smbd: Allow reading files based on FILE_EXECUTE access right.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12172 BUG #12172]: Fix access of snapshot folders via SMB1.

 https://www.samba.org/samba/history/samba-4.3.12.html

Samba 4.3.11
------------------------

* Release Notes for Samba 4.3.11
* July 07, 2016

===============================
This is a security release in order to address the following defect:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2119 CVE-2016-2119] (Client side SMB2/3 required signing can be downgraded)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2119 CVE-2016-2119]:
It's possible for an attacker to downgrade the required signing for an SMB2/3 client connection, by injecting the SMB2_SESSION_FLAG_IS_GUEST or SMB2_SESSION_FLAG_IS_NULL flags.

This means that the attacker can impersonate a server being connected to by Samba, and return malicious results.

The primary concern is with winbindd, as it uses DCERPC over SMB2 when talking to domain controllers as a member server, and trusted domains as a domain controller.  These DCE/RPC connections were intended to protected by the combination of "client ipc signing" and "client ipc max protocol" in their effective default settings ("mandatory" and "SMB3_11").

Additionally, management tools like net, samba-tool and rpcclient use DCERPC over SMB2/3 connections.

By default, other tools in Samba are unprotected, but rarely they are configured to use smb signing, via the "client signing" parameter (the default is "if_required").  Even more rarely the "client max protocol" is set to SMB2, rather than the NT1 default.

If both these conditions are met, then this issue would also apply to these other tools, including command line tools like smbcacls, smbcquota, smbclient, smbget and applications using libsmbclient.

===============================
Changes since 4.3.11:
===============================

------------------------

*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11860 BUG #11860]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2119 CVE-2016-2119]: Fix client side SMB2 signing downgrade.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11948 BUG #11948]: Total dcerpc response payload more than 0x400000.

 https://www.samba.org/samba/history/samba-4.3.11.html

Samba 4.3.10
------------------------

* Release Notes for Samba 4.3.10
* June 15, 2016

===============================
This is the latest stable release of Samba 4.3.
===============================

------------------------

===============================
Changes since 4.3.9:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10618 BUG #10618]: Do not ignore supplementary groups.
*  Christian Ambach <ambi@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10796 BUG #10796]: s3:rpcclient: Make '--pw-nt-hash' option work.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11354 BUG #11354]: s3:libsmb/clifile: Use correct value for MaxParameterCount for setting EAs.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11438 BUG #11438]: s3:smbd/service disable case-sensitivity for SMB2/3 connections.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=1703 BUG #1703]: s3:libnet:libnet_join: Add netbios aliases as SPNs.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11721 BUG #11721]: vfs_fruit: Add an option that allows disabling POSIX rename behaviour.
*  Alexander Bokovoy <ab@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11936 BUG #11936]: s3-smbd: Support systemd 230.
*  Jérémie Courrèges-Anglas <jca@wxcvbn.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11864 BUG #11864]: Provide fallback code for non-portable clearenv(3).
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11864 BUG #11864]: s3:client:smbspool_krb5_wrapper: fix the non clearenv build.
*  Robin McCorkell <robin@mccorkell.me.uk>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11276 BUG #11276]: Correctly set cli->raw_status for libsmbclient in SMB2 code.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11910 BUG #11910]: s3:smbd: Fix anonymous authentication if signing is mandatory.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11912 BUG #11912]: libcli/auth: Let msrpc_parse() return talloc'ed empty strings.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11914 BUG #11914]: s3:ntlm_auth: Make ntlm_auth_generate_session_info() more complete.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11927 BUG #11927]: s3:rpcclient: Make use of SMB_SIGNING_IPC_DEFAULT.
*  Luca Olivetti <luca@wetron.es>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11530 BUG #11530]: pdb: Fix segfault in pdb_ldap for missing gecos.
*  Rowland Penny <rpenny@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11613 BUG #11613]: samba-tool: Allow 'samba-tool fsmo' to cope with empty or missing fsmo roles.
*  Anoop C S <anoopcs@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11907 BUG #11907]: packaging: Set default limit for core file size in service files.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11922 BUG #11922]: s3-net: Convert the key_name to UTF8 during migration.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11935 BUG #11935]: s3-smbspool: Log to stderr.
*  Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11900 BUG #11900]: heimdal: Encode/decode kvno as signed integer.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11931 BUG #11931]: s3-quotas: Fix sysquotas_4B quota fetching for BSD.
*  Raghavendra Talur <rtalur@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11907 BUG #11907]: init: Set core file size to unlimited by default.
*  Hemanth Thummala <hemanth.thummala@nutanix.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11934 BUG #11934]: Fix memory leak in share mode locking.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11844 BUG #11844]: smbd: Fix an assert.
*  Lorinczy Zsigmond <lzsiga@freemail.c3.hu>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11947 BUG #11947]: lib: replace: snprintf - Fix length calculation for hex/octal 64-bit values.

 https://www.samba.org/samba/history/samba-4.3.10.html

Samba 4.3.9
------------------------

* Release Notes for Samba 4.3.9
* May 2, 2016

===============================
This is the latest stable release of Samba 4.3.
===============================

------------------------

This release fixes some regressions introduced by the last security fixes. Please see bug https://bugzilla.samba.org/show_bug.cgi?id=11849 for a list of bugs addressing these regressions and more information.

===============================
Changes since 4.3.8:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11742 BUG #11742]: lib: tevent: Fix memory leak when old signal action restored.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11771 BUG #11771]: lib: tevent: Fix memory leak when old signal action restored.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11822 BUG #11822]: s3: libsmb: Fix error where short name length was read as 2 bytes, should be 1.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11780 BUG #11780]: smbd: Only check dev/inode in open_directory, not the full stat().
* * [https://bugzilla.samba.org/show_bug.cgi?id=11789 BUG #11789]: pydsdb: Fix returning of ldb.MessageElement.
*  Berend De Schouwer <berend.de.schouwer@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11643 BUG #11643]: docs: Add example for domain logins to smbspool man page.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11789 BUG #11789]: libsmb/pysmb: Add pytalloc-util dependency to fix the build.
*  Alberto Maria Fiaschi <alberto.fiaschi@estar.toscana.it>
* * [https://bugzilla.samba.org/show_bug.cgi?id=8093 BUG #8093]: access based share enum: Handle permission set in configuration files.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11816 BUG #11816]: nwrap: Fix the build on Solaris.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11827 BUG #11827]: vfs_catia: Fix memleak.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11878 BUG #11878]: smbd: Avoid large reads beyond EOF.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11622 BUG #11622]: libcli/smb: Make sure we have a body size of 0x31 before dereferencing an ioctl response.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11623 BUG #11623]: libcli/smb: Fix BUFFER_OVERFLOW handling in tstream_smbXcli_np.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11755 BUG #11755]: s3:libads: Setup the msDS-SupportedEncryptionTypes attribute on ldap_add.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11771 BUG #11771]: tevent: Version 0.9.28. Fix memory leak when old signal action restored.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11782 BUG #11782]: s3:winbindd: Don't include two '\0' at the end of the domain list.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11789 BUG #11789]: s3:wscript: pylibsmb depends on pycredentials.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11841 BUG #11841]: Fix NT_STATUS_ACCESS_DENIED when accessing Windows public share.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11847 BUG #11847]: Only validate MIC if "map to guest" is not being used.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11849 BUG #11849]: auth/ntlmssp: Add ntlmssp_{client,server}:force_old_spnego option for testing.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11850 BUG #11850]: NetAPP SMB servers don't negotiate NTLM _SIGN.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11858 BUG #11858]: Allow anonymous smb connections.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11870 BUG #11870]: Fix ads_sasl_spnego_gensec_bind(KRB5).
* * [https://bugzilla.samba.org/show_bug.cgi?id=11872 BUG #11872]: Fix 'wbinfo -u' and 'net ads search'.
*  Noel Power <noel.power@suse.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11738 BUG #11738]: libcli: Fix debug message, print sid string for new_ace trustee.
*  Garming Sam <garming@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11789 BUG #11789]: build: Mark explicit dependencies on pytalloc-util.
*  Partha Sarathi <partha@exablox.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11819 BUG #11819]: Fix the smb2_setinfo to handle FS info types and FSQUOTA infolevel.
*  Jorge Schrauwen <sjorge@blackdot.be>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11816 BUG #11816]: configure: Don't check for inotify on illumos.
*  Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11691 BUG #11691]: winbindd: Return trust parameters when listing trusts.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11753 BUG #11753]: smbd: Ignore SVHDX create context.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11763 BUG #11763]: passdb: Add linefeed to debug message.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11788 BUG #11788]: build: Fix disk-free quota support on Solaris 10.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11798 BUG #11798]: build: Fix build when '--without-quota' specified.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11806 BUG #11806]: vfs_acl_common: Avoid setting POSIX ACLs if "ignore system acls" is set.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11852 BUG #11852]: libads: Record session expiry for spnego sasl binds.
*  Hemanth Thummala <hemanth.thummala@nutanix.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11740 BUG #11740]: Real memory leak(buildup) issue in loadparm.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11840 BUG #11840]: Mask general purpose signals for notifyd.

 https://www.samba.org/samba/history/samba-4.3.9.html

Samba 4.3.8
------------------------

* Release Notes for Samba 4.3.8
* April 12, 2016

This is a security release containing one additional regression fix for the security release 4.2.10.

This fixes a regression that prevents things like 'net ads join' from working against a Windows 2003 domain.

===============================
Changes since 4.3.7:
===============================

------------------------

*  Stefan Metzmacher <metze@samba.org>
* *  [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016

Samba 4.3.7
------------------------

* Release Notes for Samba 4.3.7
* April 12, 2016

===============================
This is a security release in order to address the following CVEs:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5370 CVE-2015-5370] (Multiple errors in DCE-RPC code)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2110 CVE-2016-2110] (Man in the middle attacks possible with NTLM )
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2111 CVE-2016-2111] (NETLOGON Spoofing Vulnerability)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2112 CVE-2016-2112] (LDAP client and server don't enforce integrity)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2113 CVE-2016-2113] (Missing TLS certificate validation)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2114 CVE-2016-2114] ("server signing = mandatory" not enforced)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2115 CVE-2016-2115] (SMB IPC traffic is not integrity protected)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2118 CVE-2016-2118] (SAMR and LSA man in the middle attacks possible)

The number of changes are rather huge for a security release, compared to typical security releases.

Given the number of problems and the fact that they are all related to man in the middle attacks we decided to fix them all at once instead of splitting them.

In order to prevent the man in the middle attacks it was required to change the (default) behavior for some protocols. Please see the "New smb.conf options" and "Behavior changes" sections below.

===============================
Details
===============================

------------------------

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5370 CVE-2015-5370]=
===============================

------------------------

Versions of Samba from 3.6.0 to 4.4.0 inclusive are vulnerable to denial of service attacks (crashes and high cpu consumption) in the DCE-RPC client and server implementations. In addition, errors in validation of the DCE-RPC packets can lead to a downgrade of a secure connection to an insecure one.

While we think it is unlikely, there's a nonzero chance for a remote code execution attack against the client components, which are used by smbd, winbindd and tools like net, rpcclient and others. This may gain root access to the attacker.

The above applies all possible server roles Samba can operate in.

Note that versions before 3.6.0 had completely different marshalling functions for the generic DCE-RPC layer. It's quite possible that that code has similar problems!

The downgrade of a secure connection to an insecure one may allow an attacker to take control of Active Directory object handles created on a connection created from an Administrator account and re-use them on the now non-privileged connection, compromising the security of the Samba AD-DC.

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2110 CVE-2016-2110]:=
===============================

------------------------

There are several man in the middle attacks possible with NTLM  authentication.

E.g. NTLM _NEGOTIATE_SIGN and NTLM _NEGOTIATE_SEAL can be cleared by a man in the middle.

This was by protocol design in earlier Windows versions.

Windows Server 2003 RTM and Vista RTM introduced a way to protect against the trivial downgrade.

See MsvAvFlags and flag 0x00000002 in
   https://msdn.microsoft.com/en-us/library/cc236646.aspx

This new feature also implies support for a mechlistMIC when used within SPNEGO, which may prevent downgrades from other SPNEGO mechs, e.g. Kerberos, if sign or seal is finally negotiated.

The Samba implementation doesn't enforce the existence of required flags, which were requested by the application layer, e.g. LDAP or SMB1 encryption (via the unix extensions). As a result a man in the middle can take over the connection. It is also possible to misguide client and/or server to send unencrypted traffic even if encryption was explicitly requested.

LDAP (with NTLM  authentication) is used as a client by various admin tools of the Samba project, e.g. "net", "samba-tool", "ldbsearch", "ldbedit", ...

As an active directory member server LDAP is also used by the winbindd service when connecting to domain controllers.

Samba also offers an LDAP server when running as active directory domain controller.

The NTLM  authentication used by the SMB1 encryption is protected by smb signing, see CVE-2015-5296.

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2111 CVE-2016-2111]:=
===============================

------------------------

It's basically the same as CVE-2015-0005 for Windows:

* The NETLOGON service in Microsoft Windows Server 2003 SP2, Windows Server 2008 SP2 and R2 SP1, and Windows Server 2012 Gold and R2, when a Domain Controller is configured, allows remote attackers to spoof the computer name of a secure channel's endpoint, and obtain sensitive session information, by running a crafted application and leveraging the ability to sniff network traffic, aka "NETLOGON Spoofing Vulnerability".

The vulnerability in Samba is worse as it doesn't require credentials of a computer account in the domain.

This only applies to Samba running as classic primary domain controller, classic backup domain controller or active directory domain controller.

The security patches introduce a new option called "raw NTLMv2 auth" ("yes" or "no") for the [global] section in smb.conf. Samba (the smbd process) will reject client using raw NTLMv2 without using NTLM .

Note that this option also applies to Samba running as standalone server and member server.

You should also consider using "lanman auth = no" (which is already the default) and "ntlm auth = no". Have a look at the smb.conf manpage for further details, as they might impact compatibility with older clients. These also apply for all server roles.

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2112 CVE-2016-2112]:=
===============================

------------------------

Samba uses various LDAP client libraries, a builtin one and/or the system ldap libraries (typically openldap).

As active directory domain controller Samba also provides an LDAP server.

Samba takes care of doing SASL (GSS-SPNEGO) authentication with Kerberos or NTLM  for LDAP connections, including possible integrity (sign) and privacy (seal) protection.

Samba has support for an option called "client ldap sasl wrapping" since version 3.2.0. Its default value has changed from "plain" to "sign" with version 4.2.0.

Tools using the builtin LDAP client library do not obey the "client ldap sasl wrapping" option. This applies to tools like: "samba-tool", "ldbsearch", "ldbedit" and more. Some of them have command line options like "--sign" and "--encrypt". With the security update they will also obey the "client ldap sasl wrapping" option as default.

In all cases, even if explicitly request via "client ldap sasl wrapping", "--sign" or "--encrypt", the protection can be downgraded by a man in the middle.

The LDAP server doesn't have an option to enforce strong authentication yet. The security patches will introduce a new option called "ldap server require strong auth", possible values are "no", "allow_sasl_over_tls" and "yes".

As the default behavior was as "no" before, you may have to explicitly change this option until all clients have been adjusted to handle LDAP_STRONG_AUTH_REQUIRED errors. Windows clients and Samba member servers already use integrity protection.

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2113 CVE-2016-2113]:=
===============================

------------------------

Samba has support for TLS/SSL for some protocols: ldap and http, but currently certificates are not validated at all. While we have a "tls cafile" option, the configured certificate is not used to validate the server certificate.

This applies to ldaps:// connections triggered by tools like: "ldbsearch", "ldbedit" and more. Note that it only applies to the ldb tools when they are built as part of Samba or with Samba extensions installed, which means the Samba builtin LDAP client library is used.

It also applies to dcerpc client connections using ncacn_http (with https://), which are only used by the openchange project. Support for ncacn_http was introduced in version 4.2.0. 

The security patches will introduce a new option called "tls verify peer". Possible values are "no_check", "ca_only", "ca_and_name_if_available", "ca_and_name" and "as_strict_as_possible". 

If you use the self-signed certificates which are auto-generated by Samba, you won't have a crl file and need to explicitly set "tls verify peer = ca_and_name".

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2114 CVE-2016-2114]=
===============================

------------------------

Due to a regression introduced in Samba 4.0.0, an explicit "server signing = mandatory" in the [global] section of the smb.conf was not enforced for clients using the SMB1 protocol.

As a result it does not enforce smb signing and allows man in the middle attacks.

This problem applies to all possible server roles: standalone server, member server, classic primary domain controller, classic backup domain controller and active directory domain controller.

In addition, when Samba is configured with "server role = active directory domain controller" the effective default for the "server signing" option should be "mandatory".

During the early development of Samba 4 we had a new experimental file server located under source4/smb_server. But before the final 4.0.0 release we switched back to the file server under source3/smbd.

But the logic for the correct default of "server signing" was not ported correctly ported.

Note that the default for server roles other than active directory domain controller, is "off" because of performance reasons.

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2115 CVE-2016-2115]:=
===============================

------------------------

Samba has an option called "client signing", this is turned off by default for performance reasons on file transfers.

This option is also used when using DCERPC with ncacn_np.

In order to get integrity protection for ipc related communication by default the "client ipc signing" option is introduced. The effective default for this new option is "mandatory".

In order to be compatible with more SMB server implementations, the following additional options are introduced:
*   "client ipc min protocol" ("NT1" by default) and
*   "client ipc max protocol" (the highest support SMB2/3 dialect by default).
* These options overwrite the "client min protocol" and "client max protocol" options, because the default for "client max protocol" is still "NT1". The reason for this is the fact that all SMB2/3 support SMB signing, while there are still SMB1 implementations which don't offer SMB signing by default (this includes Samba versions before 4.0.0). 

Note that winbindd (in versions 4.2.0 and higher) enforces SMB signing against active directory domain controllers despite of the "client signing" and "client ipc signing" options.

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2118 CVE-2016-2118] (a.k.a. BADLOCK):=
===============================

------------------------

The Security Account Manager Remote Protocol [MS-SAMR] and the Local Security Authority (Domain Policy) Remote Protocol [MS-LSAD] are both vulnerable to man in the middle attacks. Both are application level protocols based on the generic DCE 1.1 Remote Procedure Call (DCERPC) protocol.

These protocols are typically available on all Windows installations as well as every Samba server. They are used to maintain the Security Account Manager Database. This applies to all roles, e.g. standalone, domain member, domain controller.

Any authenticated DCERPC connection a client initiates against a server can be used by a man in the middle to impersonate the authenticated user against the SAMR or LSAD service on the server.

The client chosen application protocol, auth type (e.g. Kerberos or NTLM ) and auth level (NONE, CONNECT, PKT_INTEGRITY, PKT_PRIVACY) do not matter in this case. A man in the middle can change auth level to CONNECT (which means authentication without message protection) and take over the connection.

As a result, a man in the middle is able to get read/write access to the Security Account Manager Database, which reveals all password sand any other potential sensitive information.

Samba running as an active directory domain controller is additionally missing checks to enforce PKT_PRIVACY for the Directory Replication Service Remote Protocol [MS-DRSR] (drsuapi) and the BackupKey Remote Protocol [MS-BKRP] (backupkey). The Domain Name Service Server Management Protocol [MS-DNSP] (dnsserver) is not enforcing at least PKT_INTEGRITY.

===============================
New smb.conf options
===============================

------------------------

allow dcerpc auth level connect (G)=
===============================

------------------------

This option controls whether DCERPC services are allowed to be used with DCERPC_AUTH_LEVEL_CONNECT, which provides authentication, but no per message integrity nor privacy protection.

Some interfaces like samr, lsarpc and netlogon have a hard-coded default of no and epmapper, mgmt and rpcecho have a hard-coded default of yes.

The behavior can be overwritten per interface name (e.g. lsarpc, netlogon, samr, srvsvc, winreg, wkssvc ...) by using 'allow dcerpc auth level connect:interface = yes' as option.

This option yields precedence to the implementation specific restrictions. E.g. the drsuapi and backupkey protocols require DCERPC_AUTH_LEVEL_PRIVACY. The dnsserver protocol requires DCERPC_AUTH_LEVEL_INTEGRITY.

    Default: allow dcerpc auth level connect = no
    Example: allow dcerpc auth level connect = yes

client ipc signing (G)=
===============================

------------------------

This controls whether the client is allowed or required to use SMB signing for IPC$ connections as DCERPC transport. Possible values are auto, mandatory and disabled.

When set to mandatory or default, SMB signing is required. When set to auto, SMB signing is offered, but not enforced and if set to disabled, SMB signing is not offered either.

Connections from winbindd to Active Directory Domain Controllers always enforce signing.

    Default: client ipc signing = default

client ipc max protocol (G)=
===============================

------------------------

The value of the parameter (a string) is the highest protocol level that will be supported for IPC$ connections as DCERPC transport.

Normally this option should not be set as the automatic negotiation phase in the SMB protocol takes care of choosing the appropriate protocol.

The value default refers to the latest supported protocol, currently SMB3_11.

See client max protocol for a full list of available protocols. The values CORE, COREPLUS, LANMAN1, LANMAN2 are silently upgraded to NT1.

    Default: client ipc max protocol = default
    Example: client ipc max protocol = SMB2_10

client ipc min protocol (G)=
===============================

------------------------

This setting controls the minimum protocol version that the will be attempted to use for IPC$ connections as DCERPC transport.

Normally this option should not be set as the automatic negotiation phase in the SMB protocol takes care of choosing the appropriate protocol.

The value default refers to the higher value of NT1 and the effective value of "client min protocol".

See client max protocol for a full list of available protocols. The values CORE, COREPLUS, LANMAN1, LANMAN2 are silently upgraded to NT1.

    Default: client ipc min protocol = default
    Example: client ipc min protocol = SMB3_11

ldap server require strong auth (G)=
===============================

------------------------

The ldap server require strong auth defines whether the ldap server requires ldap traffic to be signed or signed and encrypted (sealed). Possible values are no, allow_sasl_over_tls and yes.

A value of no allows simple and sasl binds over all transports.

A value of allow_sasl_over_tls allows simple and sasl binds (without sign or seal) over TLS encrypted connections. Unencrypted connections only allow sasl binds with sign or seal.

A value of yes allows only simple binds over TLS encrypted connections. Unencrypted connections only allow sasl binds with sign or seal.

    Default: ldap server require strong auth = yes

raw NTLMv2 auth (G)=
===============================

------------------------

This parameter determines whether or not smbd(8) will allow SMB1 clients without extended security (without SPNEGO) to use NTLMv2 authentication.

If this option, lanman auth and ntlm auth are all disabled, then only clients with SPNEGO support will be permitted. That means NTLMv2 is only supported within NTLM .

    Default: raw NTLMv2 auth = no

tls verify peer (G)=
===============================

------------------------

This controls if and how strict the client will verify the peer's certificate and name. Possible values are (in increasing order): no_check, ca_only, ca_and_name_if_available, ca_and_name and as_strict_as_possible.

When set to no_check the certificate is not verified at all, which allows trivial man in the middle attacks.

When set to ca_only the certificate is verified to be signed from a ca specified in the "tls ca file" option. Setting "tls ca file" to a valid file is required. The certificate lifetime is also verified. If the "tls crl file" option is configured, the certificate is also verified against the ca crl.

When set to ca_and_name_if_available all checks from ca_only are performed. In addition, the peer hostname is verified against the certificate's name, if it is provided by the application layer and not given as an ip address string.

When set to ca_and_name all checks from ca_and_name_if_available are performed. In addition the peer hostname needs to be provided and even an ip address is checked against the certificate's name.

When set to as_strict_as_possible all checks from ca_and_name are performed. In addition the "tls crl file" needs to be configured. Future versions of Samba may implement additional checks.

    Default: tls verify peer = as_strict_as_possible

tls priority (G) (backported from Samba 4.3 to Samba 4.2)=
===============================

------------------------

This option can be set to a string describing the TLS protocols to be supported in the parts of Samba that use GnuTLS, specifically the AD DC.

The default turns off SSLv3, as this protocol is no longer considered secure after CVE-2014-3566 (otherwise known as POODLE) impacted SSLv3 use in HTTPS applications.

The valid options are described in the GNUTLS Priority-Strings documentation at http://gnutls.org/manual/html_node/Priority-Strings.html

    Default: tls priority = NORMAL:-VERS-SSL3.0

===============================
Behavior changes
===============================

------------------------

*  The default auth level for authenticated binds has changed from DCERPC_AUTH_LEVEL_CONNECT to DCERPC_AUTH_LEVEL_INTEGRITY. That means ncacn_ip_tcp:server is now implicitly the same as ncacn_ip_tcp:server[sign] and offers a similar protection  as ncacn_np:server, which relies on smb signing.

*  The following constraints are applied to SMB1 connections:
** "client lanman auth = yes" is now consistently required for authenticated connections using the SMB1 LANMAN2 dialect.
** "client ntlmv2 auth = yes" and "client use spnego = yes" (both the default values), require extended security (SPNEGO) support from the server. That means NTLMv2 is only used within NTLM .

*  Tools like "samba-tool", "ldbsearch", "ldbedit" and more obey the default of "client ldap sasl wrapping = sign". Even with "client ldap sasl wrapping = plain" they will automatically upgrade to "sign" when getting LDAP_STRONG_AUTH_REQUIRED from the LDAP server.

===============================
Changes since 4.3.6:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11344 BUG #11344] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5370 CVE-2015-5370]: Multiple errors in DCE-RPC code.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.
*  Christian Ambach <ambi@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11644 BUG #11644] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2112 CVE-2016-2112]: The LDAP client and server don't enforce integrity protection.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11749 BUG #11749] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2111 CVE-2016-2111]: NETLOGON Spoofing Vulnerability.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.
*  Björn Jacke <bj@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.
o  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.
o  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11344 BUG #11344] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5370 CVE-2015-5370]: Multiple errors in DCE-RPC code.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11616 BUG #11616] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2118 CVE-2016-2118]: SAMR and LSA man in the middle attacks possible.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11644 BUG #11644] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2112 CVE-2016-2112]: The LDAP client and server doesn't enforce integrity protection.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11687 BUG #11687] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2114 CVE-2016-2114]: "server signing = mandatory" not enforced.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11688 BUG #11688] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2110 CVE-2016-2110]: Man in the middle attacks possible with NTLM .
* * [https://bugzilla.samba.org/show_bug.cgi?id=11749 BUG #11749] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2111 CVE-2016-2111]: NETLOGON Spoofing Vulnerability.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11752 BUG #11752] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2113 CVE-2016-2113]: Missing TLS certificate validation allows man in the middle attacks.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11756 BUG #11756] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2115 CVE-2016-2115]: SMB client connections for IPC traffic are not integrity protected.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.
*  Richard Sharpe <rsharpe@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.

* https://www.samba.org/samba/history/samba-4.3.8.html

Samba 4.3.6
------------------------

Release Notes for Samba 4.3.6
March 8, 2016

===============================
This is a security release in order to address the following CVEs:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-7560 CVE-2015-7560] (Incorrect ACL get/set allowed on symlink path)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-0771 CVE-2016-0771] (Out-of-bounds read in internal DNS server)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-7560 CVE-2015-7560]:
* All versions of Samba from 3.2.0 to 4.4.0rc3 inclusive are vulnerable to a malicious client overwriting the ownership of ACLs using symlinks.

* An authenticated malicious client can use SMB1 UNIX extensions to create a symlink to a file or directory, and then use non-UNIX SMB1 calls to overwrite the contents of the ACL on the file or directory linked to.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-0771 CVE-2016-0771]:
* All versions of Samba from 4.0.0 to 4.4.0rc3 inclusive, when deployed as an AD DC and choose to run the internal DNS server, are vulnerable to an out-of-bounds read issue during DNS TXT record handling caused by users with permission to modify DNS records.

* A malicious client can upload a specially constructed DNS TXT record, resulting in a remote denial-of-service attack. As long as the affected TXT record remains undisturbed in the Samba database, a targeted DNS query may continue to trigger this exploit.
* While unlikely, the out-of-bounds read may bypass safety checks and allow leakage of memory from the server in the form of a DNS TXT reply.

* By default only authenticated accounts can upload DNS records, as "allow dns updates = secure only" is the default. Any other value would allow anonymous clients to trigger this bug, which is a much higher risk.

===============================
Changes since 4.3.5:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11648 BUG #11648]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-7560 CVE-2015-7560]: Getting and setting Windows ACLs on symlinks can change permissions on link target.

*  Garming Sam <garming@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11128 BUG #11128], [https://bugzilla.samba.org/show_bug.cgi?id=11686 BUG #11686] [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-0771 CVE-2016-0771]: Read of uninitialized memory DNS TXT handling.

*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11128 BUG #11128], [https://bugzilla.samba.org/show_bug.cgi?id=11686 BUG #11686] [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-0771 CVE-2016-0771]: Read of uninitialized memory DNS TXT handling.

 https://www.samba.org/samba/history/samba-4.3.6.html

Samba 4.3.5
------------------------

* Release Notes for Samba 4.3.5
* February 23, 2016
===============================
This is the latest stable release of Samba 4.3.
===============================

------------------------

===============================
Changes since 4.3.4:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10489 BUG #10489]: s3: smbd: posix_acls: Fix check for setting u:g:o entry on a filesystem with no ACL support.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11703 BUG #11703]: s3: smbd: Fix timestamp rounding inside SMB2 create.
*  Christian Ambach <ambi@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=6482 BUG #6482]: s3:utils/smbget: Fix recursive download.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11400 BUG #11400]: s3:smbd/oplock: Obey kernel oplock setting when releasing oplocks.
*  Alexander Bokovoy <ab@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11693 BUG #11693]: s3-parm: Clean up defaults when removing global parameters.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11684 BUG #11684]: s3:smbd: Ignore initial allocation size for directory creation.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11714 BUG #11714]: lib/tsocket: Work around sockets not supporting FIONREAD.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11705 BUG #11705]: ctdb: Remove error messages after kernel security update (CVE-2015-8543).
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11732 BUG #11732]: param: Fix str_list_v3 to accept ";" again.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11699 BUG #11699]: Use M2Crypto.RC4.RC4 on platforms without Crypto.Cipher.ARC4.
*  Jose A. Rivera <jarrpa@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11727 BUG #11727]: s3:smbd:open: Skip redundant call to file_set_dosmode when creating a new file.
*  Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11670 BUG #11670]: winbindd: Handle expired sessions correctly.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11690 BUG #11690]: s3-client: Add a KRB5 wrapper for smbspool.
*  Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11580 BUG #11580]: vfs_shadow_copy2: Fix case where snapshots are outside the share.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11662 BUG #11662]: smbclient: Query disk usage relative to current directory.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11681 BUG #11681]: smbd: Show correct disk size for different quota and dfree block sizes.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11682 BUG #11682]: smbcacls: Fix uninitialized variable.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11719 BUG #11719]: ctdb-scripts: Drop use of "smbcontrol winbindd ip-dropped ...".
*  Hemanth Thummala <hemanth.thummala@nutanix.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11708 BUG #11708]: loadparm: Fix memory leak issue.

 https://www.samba.org/samba/history/samba-4.3.5.html

Samba 4.3.4
------------------------

* Release Notes for Samba 4.3.4
* January 12, 2016

===============================
This is the latest stable release of Samba 4.3.
===============================

------------------------

===============================
Changes since 4.3.3
===============================

------------------------

* Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11619 BUG #11619]: doc: Fix a typo in the smb.conf manpage, explanation of idmap config.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11647 BUG #11647]: s3:smbd: Fix a corner case of the symlink verification.

* Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11624 BUG #11624]: s3: libsmb: Correctly initialize the list head when keeping a list of primary followed by DFS connections.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11625 BUG #11625]: Reduce the memory footprint of empty string options.

* Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11659 BUG #11659]: Update lastLogon and lastLogonTimestamp.
* Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11065 BUG #11065]: vfs_fruit: Enable POSIX directory rename semantics.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11466 BUG #11466]: Copying files with vfs_fruit fails when using vfs_streams_xattr without stream prefix and type suffix.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11645 BUG #11645]: smbd: Make "hide dot files" option work with "store dos attributes = yes".
* Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11639 BUG #11639]: lib/async_req: Do not install async_connect_send_test.
o  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11394 BUG #11394]: Crash: Bad talloc magic value - access after free.
* Rowland Penny <repenny241155@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11613 BUG #11613]: samba-tool: Fix uncaught exception if no fSMORoleOwner attribute is given.
* Karolin Seeger <kseeger@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11619 BUG #11619]: docs: Fix some typos in the idmap backend section.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11641 BUG #11641]: docs: Fix typos in man vfs_gpfs.
* Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11649 BUG #11649]: smbd: Do not disable "store dos attributes" on-the-fly.

 https://www.samba.org/samba/history/samba-4.3.4.html

Samba 4.3.3
------------------------

* Release Notes for Samba 4.3.3
* December 16, 2015

===============================
This is a security release in order to address the following CVEs:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-3223 CVE-2015-3223] (Denial of service in Samba Active Directory server)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5252 CVE-2015-5252] (Insufficient symlink verification in smbd)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5299 CVE-2015-5299] (Missing access control check in shadow copy code)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5296 CVE-2015-5296] (Samba client requesting encryption vulnerable to downgrade attack)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-8467 CVE-2015-8467] (Denial of service attack against Windows Active Directory server)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5330 CVE-2015-5330] (Remote memory read in Samba LDAP server)

Please note that if building against a system libldb, the required version has been bumped to ldb-1.1.24.  This is needed to ensure we build against a system ldb library that contains the fixes for [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5330 CVE-2015-5330] and [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-3223 CVE-2015-3223].

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-3223 CVE-2015-3223]:
* All versions of Samba from 4.0.0 to 4.3.2 inclusive (resp. all ldb versions up to 1.1.23 inclusive) are vulnerable to a denial of service attack in the samba daemon LDAP server.

* A malicious client can send packets that cause the LDAP server in the samba daemon process to become unresponsive, preventing the server from servicing any other requests.

* This flaw is not exploitable beyond causing the code to loop expending CPU resources.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5252 CVE-2015-5252]:
* All versions of Samba from 3.0.0 to 4.3.2 inclusive are vulnerable to a bug in symlink verification, which under certain circumstances could allow client access to files outside the exported share path.

* If a Samba share is configured with a path that shares a common path prefix with another directory on the file system, the smbd daemon may allow the client to follow a symlink pointing to a file or directory in that other directory, even if the share parameter "wide links" is set to "no" (the default).

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5299 CVE-2015-5299]:
* All versions of Samba from 3.2.0 to 4.3.2 inclusive are vulnerable to a missing access control check in the vfs_shadow_copy2 module. When looking for the shadow copy directory under the share path the current accessing user should have DIRECTORY_LIST access rights in order to view the current snapshots.

* This was not being checked in the affected versions of Samba.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5296 CVE-2015-5296]:
* Versions of Samba from 3.2.0 to 4.3.2 inclusive do not ensure that signing is negotiated when creating an encrypted client connection to a server.

* Without this a man-in-the-middle attack could downgrade the connection and connect using the supplied credentials as an unsigned, unencrypted connection.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-8467 CVE-2015-8467]:
* Samba, operating as an AD DC, is sometimes operated in a domain with a mix of Samba and Windows Active Directory Domain Controllers.

* All versions of Samba from 4.0.0 to 4.3.2 inclusive, when deployed as an AD DC in the same domain with Windows DCs, could be used to override the protection against the MS15-096 / CVE-2015-2535 security issue in Windows.

* Prior to MS16-096 it was possible to bypass the quota of machine accounts a non-administrative user could create.  Pure Samba domains are not impacted, as Samba does not implement the SeMachineAccountPrivilege functionality to allow non-administrator users to create new computer objects.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5330 CVE-2015-5330]:
* All versions of Samba from 4.0.0 to 4.3.2 inclusive (resp. all ldb versions up to 1.1.23 inclusive) are vulnerable to a remote memory read attack in the samba daemon LDAP server.

* A malicious client can send packets that cause the LDAP server in the samba daemon process to return heap memory beyond the length of the requested value.

* This memory may contain data that the client should not be allowed to see, allowing compromise of the server.

* The memory may either be returned to the client in an error string, or stored in the database by a suitabily privileged user.  If untrusted users can create objects in your database, please confirm that all DN and name attributes are reasonable.

===============================
Changes since 4.3.3:
===============================

------------------------

*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11552 BUG #11552]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-8467 CVE-2015-8467]: samdb: Match MS15-096 behaviour for userAccountControl.
*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11325 BUG #11325]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-3223 CVE-2015-3223]: Fix LDAP \00 search expression attack DoS.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11395 BUG #11395]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5252 CVE-2015-5252]: Fix insufficient symlink verification (file access outside the share).
* * [https://bugzilla.samba.org/show_bug.cgi?id=11529 BUG #11529]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5299 CVE-2015-5299]: s3-shadow-copy2: Fix missing access check on snapdir.
* Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11599 BUG #11599]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5330 CVE-2015-5330]: Fix remote read memory exploit in LDB.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11536 BUG #11536]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5296 CVE-2015-5296]: Add man in the middle protection when forcing smb encryption on the client side.

 https://www.samba.org/samba/history/samba-4.3.3.html

Samba 4.3.2
------------------------

* Release Notes for Samba 4.3.2
* December 01, 2015

===============================
This is the latest stable release of Samba 4.3.
===============================

------------------------

===============================
Changes since 4.3.1:
===============================

------------------------

*Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11577 BUG #11577]: ctdb: Open the RO tracking db with perms 0600 instead of 0000.
*Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11452 BUG #10252]: s3-smbd: Fix old DOS client doing wildcard delete - gives an attribute type of zero.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11565 BUG #11565]: auth: gensec: Fix a memory leak.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11566 BUG #11566]: lib: util: Make non-critical message a warning.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11589 BUG #11589]: s3: smbd: If EAs are turned off on a share don't allow an SMB2 create containing them.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11615 BUG #11615]: s3: smbd: have_file_open_below() fails to enumerate open files below an open directory handle.
*   Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11562 BUG #11562]: s4:lib/messaging: Use correct path for names.tdb.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11564 BUG #11564]: async_req: Fix non-blocking connect().
*   Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11243 BUG #11243]: vfs_gpfs: Re-enable share modes.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11570 BUG #11570]: smbd: Send SMB2 oplock breaks unencrypted.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11612 BUG #11612]: winbind: Fix crash on invalid idmap configs.
*   YvanM <yvan.masson@openmailbox.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11584 BUG #11584]: manpage: Correct small typo error.
*Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11327 BUG #11327]: dcerpc.idl: Accept invalid dcerpc_bind_nak pdus.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11581 BUG #11581]: s3:smb2_server: Make the logic of SMB2_CANCEL DLIST_REMOVE() clearer.
*Marc Muehlfeld <mmuehlfeld@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9912 BUG #9912]: Changing log level of two entries to DBG_NOTICE.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11581 BUG #11581]: s3-smbd: Fix use after issue in smbd_smb2_request_dispatch().
*Noel Power <noel.power@suse.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11569 BUG #11569]: Fix winbindd crashes with samlogon for trusted domain user.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11597 BUG #11597]: Backport some valgrind fixes from upstream master.
*Andreas Schneider <asn@samba.org
* * [https://bugzilla.samba.org/show_bug.cgi?id=11563 BUG #11563]: Fix segfault of 'net ads (join|leave) -S INVALID' with nss_wins.
*Tom Schulz <schulz@adi.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11511 BUG #11511]: Add libreplace dependency to texpect, fixes a linking error on Solaris.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11512 BUG #11512]: s4: Fix linking of 'smbtorture' on Solaris.
*Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11608 BUG #11608]: auth: Consistent handling of well-known alias as primary gid.

 https://www.samba.org/samba/history/samba-4.3.2.html

Samba 4.3.1
------------------------

* Release Notes for Samba 4.3.1
* October 20, 2015

===============================
This is the latest stable release of Samba 4.3.
===============================

------------------------

===============================
Changes since 4.3.0:
===============================

------------------------

*   Jeremy Allison <jra@samba.org>
* *  [https://bugzilla.samba.org/show_bug.cgi?id=10252 BUG #10252]: s3: smbd: Fix our access-based enumeration on "hide unreadable"  to match Windows.
* *  [https://bugzilla.samba.org/show_bug.cgi?id=10634 BUG #10634]: smbd: Fix file name buflen and padding in notify repsonse.
* *  [https://bugzilla.samba.org/show_bug.cgi?id=11486 BUG #11486]: s3: smbd: Fix mkdir race condition.
* *  [https://bugzilla.samba.org/show_bug.cgi?id=11522 BUG #11522]: s3: smbd: Fix opening/creating :stream files on the root share directory.
* *  [https://bugzilla.samba.org/show_bug.cgi?id=11535 BUG #11535]: s3: smbd: Fix NULL pointer bug introduced by previous 'raw'
* *  [https://bugzilla.samba.org/show_bug.cgi?id=11522 BUG #11522]:stream fix .
* *  [https://bugzilla.samba.org/show_bug.cgi?id=11555 BUG #11555]: s3: lsa: lookup_name() logic for unqualified (no DOMAIN\component) names is incorrect.
*   Ralph Boehme <slow@samba.org>
* *  [https://bugzilla.samba.org/show_bug.cgi?id=11535 BUG #11535]: s3: smbd: Fix a crash in unix_convert().
* *  [https://bugzilla.samba.org/show_bug.cgi?id=11543 BUG #11543]: vfs_fruit: Return value of ad_pack in vfs_fruit.c.
* *  [https://bugzilla.samba.org/show_bug.cgi?id=11549 BUG #11549]: s3:locking: Initialize lease pointer in share_mode_traverse_fn().
* *  [https://bugzilla.samba.org/show_bug.cgi?id=11550 BUG #11550]: s3:smbstatus: Add stream name to share_entry_forall().
* *  [https://bugzilla.samba.org/show_bug.cgi?id=11555 BUG #11555]: s3:lib: Validate domain name in lookup_wellknown_name().
*   Günther Deschner <gd@samba.org>
* *  [https://bugzilla.samba.org/show_bug.cgi?id=11038 BUG #11038]: kerberos: Make sure we only use prompter type when available.
*   Volker Lendecke <vl@samba.org>
* *  [https://bugzilla.samba.org/show_bug.cgi?id=11038 BUG #11038]: winbind: Fix 100% loop.
* *  [https://bugzilla.samba.org/show_bug.cgi?id=11053 BUG #11053]: source3/lib/msghdr.c: Fix compiling error on Solaris.
*   Stefan Metzmacher <metze@samba.org>
* *  [https://bugzilla.samba.org/show_bug.cgi?id=11316 BUG #11316]: s3:ctdbd_conn: make sure we destroy tevent_fd before closing the socket.
* *  [https://bugzilla.samba.org/show_bug.cgi?id=11515 BUG #11515]: s4:lib/messaging: Use 'msg.lock' and 'msg.sock' for messaging related subdirs.
* *  [https://bugzilla.samba.org/show_bug.cgi?id=11526 BUG #11526]: lib/param: Fix hiding of FLAG_SYNONYM values.
*   Björn Jacke <bj@sernet.de>
* *  [https://bugzilla.samba.org/show_bug.cgi?id=10973 BUG #10973]: nss_winbind: Fix hang on Solaris on big groups.
* *  [https://bugzilla.samba.org/show_bug.cgi?id=11355 BUG #11355]: build: Use as-needed linker flag also on OpenBSD.
*  Har Gagan Sahai <SHarGagan@novell.com>
* *  [https://bugzilla.samba.org/show_bug.cgi?id= 11509 BUG #11509]: s3: dfs: Fix a crash when the dfs targets are disabled.
*   Andreas Schneider <asn@samba.org>
* *  [https://bugzilla.samba.org/show_bug.cgi?id= 11502 BUG #11502]: pam_winbind: Fix a segfault if initialization fails.
*   Uri Simchoni <uri@samba.org>
* *  [https://bugzilla.samba.org/show_bug.cgi?id= 11528 BUG #11528]: net: Fix a crash with 'net ads keytab create'.
* *  [https://bugzilla.samba.org/show_bug.cgi?id= 11547 BUG #11547]: vfs_commit: set the fd on open before calling SMB_VFS_FSTAT.

 https://www.samba.org/samba/history/samba-4.3.1.html

Samba 4.3.0 
------------------------

<onlyinclude>
* Release Notes for Samba 4.3.0
* September 8, 2015
===============================
This is the first stable release of Samba 4.3.
===============================

------------------------

===============================
UPGRADING
===============================

------------------------

Read the "New FileChangeNotify subsystem" and "smb.conf changes" sections (below).

===============================
NEW FEATURES
===============================

------------------------

Logging=
===============================

------------------------

The logging code now supports logging to multiple backends.  In addition to the previously available syslog and file backends, the backends for logging to the systemd-journal, lttng and gpfs have been added. Please consult the section for the 'logging' parameter in the smb.conf manpage for details.

Spotlight=
===============================

------------------------

Support for Apple's Spotlight has been added by integrating with Gnome Tracker.

For detailed instructions how to build and setup Samba for Spotlight, please see `Spotlight|here`.

New FileChangeNotify subsystem=
===============================

------------------------

Samba now contains a new subsystem to do FileChangeNotify. The previous system used a central database, notify_index.tdb, to store all notification requests. In particular in a cluster this turned out to be a major bottleneck, because some hot records need to be bounced back and forth between nodes on every change event like a new created file.

The new FileChangeNotify subsystem works with a central daemon per node. Every FileChangeNotify request and every event are handled by an
asynchronous message from smbd to the notify daemon. The notify daemon maintains a database of all FileChangeNotify requests in memory and
will distribute the notify events accordingly. This database is asynchronously distributed in the cluster by the notify daemons.

The notify daemon is supposed to scale a lot better than the previous implementation. The functional advantage is cross-node kernel change
notify: Files created via NFS will be seen by SMB clients on other nodes per FileChangeNotify, despite the fact that popular cluster file
systems do not offer cross-node inotify.

Two changes to the configuration were required for this new subsystem: 
* :The parameters "change notify" and "kernel change notify" are not per-share anymore but must be set globally. So it is no longer possible to enable or disable notify per share, the notify daemon has no notion of a share, it only works on absolute paths.

New SMB profiling code=
===============================

------------------------

The code for SMB (SMB1, SMB2 and SMB3) profiling uses a tdb instead of sysv IPC shared memory. This avoids performance problems and NUMA effects. The profile stats are a bit more detailed than before.

Improved DCERPC man in the middle detection for kerberos=
===============================

------------------------

The gssapi based kerberos backends for gensec have support for DCERPC header signing when using DCERPC_AUTH_LEVEL_PRIVACY.

SMB signing required in winbindd by default=
===============================

------------------------

The effective value for "client signing" is required by default for winbindd, if the primary domain uses active directory.

Experimental NTDB was removed=
===============================

------------------------

The experimental NTDB library introduced in Samba 4.0 has been removed again.

Improved support for trusted domains (as AD DC)=
===============================

------------------------

The support for trusted domains/forests has improved a lot.

samba-tool got "domain trust" subcommands to manage trusts:

    reate      - Create a domain or forest trust.
    elete      - Delete a domain trust.
    ist        - List domain trusts.
    amespaces  - Manage forest trust namespaces.
    how        - Show trusted domain details.
    alidate    - Validate a domain trust.

External trusts between individual domains work in both ways (inbound and outbound). The same applies to root domains of a forest trust. The transitive routing into the other forest is fully functional for kerberos, but not yet supported for NTLM .

While a lot of things are working fine, there are currently a few limitations:

* Both sides of the trust need to fully trust each other!
* No SID filtering rules are applied at all!
* This means DCs of domain A can grant domain admin rights in domain B.
* It's not possible to add users/groups of a trusted domain into domain groups.

SMB 3.1.1 supported=
===============================

------------------------

Both client and server have support for SMB 3.1.1 now.

This is the dialect introduced with Windows 10, it improves the secure negotiation of SMB dialects and features.

There's also a new optinal encryption algorithm aes-gcm-128, but for now this is only selected as fallback and aes-ccm-128 is preferred because of the better performance. This might change in future versions when hardware encryption will be supported.
* See [https://bugzilla.samba.org/show_bug.cgi?id=11451 BUG #11451]

New smbclient subcommands=
===============================

------------------------

* Query a directory for change notifications: notify <dir name>
* Server side copy: scopy <source filename> <destination filename>

New rpcclient subcommands=
===============================

------------------------

    etshareenumall 	- Enumerate all shares
    etsharegetinfo 	- Get Share Info
    etsharesetinfo 	- Set Share Info
    etsharesetdfsflags	- Set DFS flags
    etfileenum		- Enumerate open files
    etnamevalidate	- Validate sharename
    etfilegetsec		- Get File security
    etsessdel		- Delete Session
    etsessenum		- Enumerate Sessions
    etdiskenum		- Enumerate Disks
    etconnenum		- Enumerate Connections
    etshareadd		- Add share
    etsharedel		- Delete share

New modules=
===============================

------------------------

    dmap_script 		- see 'man 8 idmap_script'
    fs_unityed_media	- see 'man 8 vfs_unityed_media'
    fs_shell_snap	- see 'man 8 vfs_shell_snap'

New sparsely connected replia graph (Improved KCC)=
===============================

------------------------

The Knowledge Consistency Checker (KCC) maintains a replication graph for DCs across an AD network. The existing Samba KCC uses a fully connected graph, so that each DC replicates from all the others, which does not scale well with large networks. In 4.3 there is an experimental new KCC that creates a sparsely connected replication graph and closely follows Microsoft's specification. It is turned off by default. To use the new KCC, set "kccsrv:samba_kcc=true" in smb.conf and let us know how it goes. You should consider doing this if you are making a large new network. For small networks there is little benefit and you can always switch over at a later date.

Configurable TLS protocol support, with better defaults=
===============================

------------------------

The "tls priority" option can be used to change the supported TLS protocols. The default is to disable SSLv3, which is no longer considered secure.

Samba-tool now supports all 7 `Flexible_Single-Master_Operations_(FSMO)_Roles|FSMO` roles=
===============================

------------------------

Previously "samba-tool fsmo" could only show, transfer or seize the five well-known FSMO roles:

* Schema Master
* Domain Naming Master
* RID Master
* PDC Emulator
* Infrastructure Master

It can now also show, transfer or seize the DNS infrastructure roles:

* DomainDnsZones Infrastructure Master
* ForestDnsZones Infrastructure Master

CTDB logging changes=
===============================

------------------------

The destination for CTDB logging is now set via a single new configuration variable CTDB_LOGGING.  This replaces CTDB_LOGFILE and CTDB_SYSLOG, which have both been removed.  See ctdbd.conf(5) for details of CTDB_LOGGING.

CTDB no longer runs a separate logging daemon.

CTDB NFS support changes=
===============================

------------------------

CTDB's NFS service management has been combined into a single 60.nfs event script.  This updated 60.nfs script now uses a call-out to interact with different NFS implementations.  See the CTDB_NFS_CALLOUT option in the ctdbd.conf(5) manual page for details.  A default call-out is provided to interact with the Linux kernel NFS implementation.  The 60.ganesha event script has been removed - a sample call-out is provided for NFS Ganesha, based on this script.

The method of configuring NFS RPC checks has been improved.  See
ctdb/config/nfs-checks.d/README for details.

Improved Cross-Compiling Support==
===============================

------------------------

A new "hybrid" build configuration mode is added to improve cross-compilation support.

A common challenge in cross-compilation is that of obtaining the results of tests that have to run on the target, during the configuration phase of the build. The Samba build system already supports the following means to do so:

* Executing configure tests using the --cross-execute parameter
* Obtaining the results from an answers file using the --cross-answers parameter

The first method has the drawback of inaccurate results if the tests are run using an emulator, or a need to be connected to a running target while building, if the tests are to be run on an actual target. The second method presents a challenge of figuring out the test results.

The new hybrid mode runs the tests and records the result in an answer file. To activate this mode, use both --cross-execute and --cross-answers in the same configure invocation. This mode can be activated once against a running target, and then the generated answers file can be used in subsequent builds.

Also supplied is an example script that can be used as the cross-execute program. This script copies the test to a running target and runs the test on the target, obtaining the result. The obtained results are more accurate than running the test with an emulator, because they reflect the exact kernel and system libraries that exist on the target.

Improved Sparse File Support=
===============================

------------------------

------------------------

----
Support for the FSCTL_SET_ZERO_DATA and FSCTL_QUERY_ALLOCATED_RANGES SMB2 requests has been added to the smbd file server.

This allows for clients to deallocate (hole punch) regions within a sparse file, and check which portions of a file are allocated.

===============================
Changes
===============================

------------------------

smb.conf changes=
===============================

------------------------

    arameter Name		Description		Default
    ogging			New			(empty)
    sdfs shuffle referrals	New			no
    mbd profiling level		New			off
    potlight			New			no
    ls priority			New 			NORMAL:-VERS-SSL3.0
    se ntdb			Removed
    hange notify			Changed to [global]
    ernel change notify		Changed to [global]
    lient max protocol		Changed	default		SMB3_11
    erver max protocol		Changed default		SMB3_11

Removed modules=
===============================

------------------------

vfs_notify_fam - see section 'New FileChangeNotify subsystem'.
</onlyinclude>

KNOWN ISSUES=
===============================

------------------------

Currently none.

CHANGES SINCE 4.3.0rc4=
===============================

------------------------

*   Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10973 BUG #10973]: No objectClass found in replPropertyMetaData on ordinary objects (non-deleted)
* * [https://bugzilla.samba.org/show_bug.cgi?id=11429 BUG #11429]: Python bindings don't check integer types
* * [https://bugzilla.samba.org/show_bug.cgi?id=11430 BUG #11430]: Python bindings don't check array sizes
*   Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11467 BUG #11467]: Handling of 0 byte resource fork stream
*   Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11488 BUG #11488]: AD samr GetGroupsForUser fails for users with "()" in their name
*   Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11429 BUG #11429]: Python bindings don't check integer types
*   Matthieu Patou <mat@matws.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10973 BUG #10973]: No objectClass found in replPropertyMetaData on ordinary objects (non-deleted)

CHANGES SINCE 4.3.0rc3=
===============================

------------------------

*   Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11444 BUG #11444]: Crash in notify_remove caused by change notify = no
*   Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11411 BUG #11411]: smbtorture does not build when configured --with-system-mitkrb5
*   Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11455 BUG #11455]: fix recursion problem in rep_strtoll in lib/replace/replace.c
* * [https://bugzilla.samba.org/show_bug.cgi?id=11464 BUG #11464]: xid2sid gives inconsistent results
* * [https://bugzilla.samba.org/show_bug.cgi?id=11465 BUG #11465]: ctdb: Fix the build on FreeBSD 10.1
*   Roel van Meer <roel@1afa.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11427 BUG #11427]: nmbd incorrectly matches netbios names as own name
*   Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11451 BUG #11451]: Poor SMB3 encryption performance with AES-GCM
* * [https://bugzilla.samba.org/show_bug.cgi?id=11458 BUG #11458]: --bundled-libraries=!ldb,!pyldb,!pyldb-util doesn't disable ldb build and install
*   Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9862 BUG #9862]: Samba "map to guest = Bad uid" doesn't work

CHANGES SINCE 4.3.0rc2=
===============================

------------------------

*   Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11436 BUG #11436]: samba-tool uncaught exception error
* * [https://bugzilla.samba.org/show_bug.cgi?id=10493 BUG #10493]: revert LDAP extended rule 1.2.840.113556.1.4.1941 LDAP_MATCHING_RULE_IN_CHAIN changes
*   Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11278 BUG #11278]: Stream names with colon don't work with fruit: ncoding = native
* * [https://bugzilla.samba.org/show_bug.cgi?id=11426 BUG #11426]: net share allowedusers crashes
*   Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11432 BUG #11432]: Fix crash in nested ctdb banning
* * [https://bugzilla.samba.org/show_bug.cgi?id=11434 BUG #11434]: Cannot build ctdbpmda
* * [https://bugzilla.samba.org/show_bug.cgi?id=11431 BUG #11431]: CTDB's eventscript error handling is broken
o   Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11451 BUG #11451]: Poor SMB3 encryption performance with AES-GCM (part1)
* * [https://bugzilla.samba.org/show_bug.cgi?id=11316 BUG #11316]: tevent_fd needs to be destroyed before closing the fd
o   Arvid Requate <requate@univention.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11291 BUG #11291]: NetApp joined to a Samba/ADDC cannot resolve SIDs
o   Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11432 BUG #11432]: Fix crash in nested ctdb banning

CHANGES SINCE 4.3.0rc1=
===============================

------------------------

*   Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11359 BUG #11359]: strsep is not available on Solaris
*   Björn Baumbach <bb@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11421 BUG #11421]: Build with GPFS support is broken
*   Justin Maggard <jmaggard@netgear.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11320 BUG #11320]: "force group" with local group not working
*   Martin Schwenke <martin@meltin.net
* * [https://bugzilla.samba.org/show_bug.cgi?id=11424 BUG #11424]: Build broken with --disable-python

 https://www.samba.org/samba/history/samba-4.3.0.html

----
`Category:Release Notes`