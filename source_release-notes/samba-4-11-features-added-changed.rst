Samba 4.11 Features added/
    <namespace>0</namespace>
<last_edited>2021-03-10T14:14:03Z</last_edited>
<last_editor>Fraz</last_editor>

      Samba 4.11 is `Samba_Release_Planning#Discontinued_.28End_of_Life.29|**Discontinued (End of Life)**`.

Samba 4.11.17
------------------------

Release Notes for Samba 4.11.17
December 03, 2020

===============================
This is an extraordinary release of the Samba 4.11 release series to fix a regression introduced with Samba 4.11.16.
===============================

------------------------

===============================
Changes since 4.11.16
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14486]: s3: vfs_glusterfs: Fix the error in preventing talloc leaks from a function.
* Günther Deschner <gd@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14486]: s3-vfs_glusterfs: Always disable write-behind translator.
* Anoop C S <anoopcs@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14486]: manpages/vfs_glusterfs: Me/ silent skipping of write-behind translator.

 [https://TTorg/samba/history/sam/4DDOO/17DDOOT/ Release Notes Samba 4.11.17].

Samba 4.11.16
------------------------

* Release Notes for Samba 4.11.16
* November 04, 2020

===============================
This is an extraordinary release of the Samba 4.11 release series to address the following issues:
===============================

------------------------

* [https://a.org/show_bug.cgi/ BUG #14537]: ctdb-common: Avoid aliasing errors during code optimization.
* [https://a.org/show_bug.cgi/ BUG #14486]: vfs_glusterfs: Avoid data corruption with the write-behind translator.

===============================
Details
===============================

------------------------

The GlusterFS write-behind performance translator, when used with Samba, could be a source of data corruption. The translator, while processing a write call, immediately returns success but continues writing the data to the server in the background. This can cause data corruption when two clients relying on Samba to provide data consistency are operating on the same file.

The write-behind translator is enabled by default on GlusterFS. The vfs_glusterfs plugin will check for the presence of the translator and refuse to connect if detected. Please disable the write-behind translator for the GlusterFS volume to allow the plugin to connect to the volume.

===============================
Changes since 4.11.15
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14486]: s3: modules: vfs_glusterfs: Fix leak of char **lines onto mem_ctx on return.
*  Günther Deschner <gd@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14486]: s3-vfs_glusterfs: Refuse connection when write-behind xlator is present.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://a.org/show_bug.cgi/ BUG #14537]: ctdb-common: Avoid aliasing errors during code optimization.
*  Sachin Prabhu <sprabhu@redhat.com>
* * [https://a.org/show_bug.cgi/ BUG #14486]: docs-xml/manpages: Add /ing about write-behind translator for vfs_glusterfs.

    https://TTorg/samba/history/sam/4DDOO/16DDOOT/ Release Notes Samba 4.11.16].

Samba 4.11.15
------------------------

* Release Notes for Samba 4.11.15
* October 29, 2020

===============================
This is a security release in order to address the following defects:
===============================

------------------------

* [https://TTorg/samba/security/CV/020DD/8.h/CEECVE-2020-14318]: Missing handle permissions check in SMB1/2/3 ChangeNotify.//
* [https://TTorg/samba/security/CV/020DD/3.h/CEECVE-2020-14323]: Unprivileged user can crash winbind.
* [https://TTorg/samba/security/CV/020DD/3.h/CEECVE-2020-14383]: An authenticated user can crash the DCE/RPC DNS with easi/EEcrafted records.

===============================
Details
===============================

------------------------

* [https://TTorg/samba/security/CV/020DD/8.h/CEECVE-2020-14318]:
* The SMB1/C/ls have a concept of "ChangeNotify", where a client can request file name notification on a directory handle when a condition such as "new file creation" or "file size change" or "file timestamp update" occurs.

* A missing permissions check on a directory handle requesting ChangeNotify meant that a client with a directory handle open only for FILE_READ_ATTRIBUTES (minimal access rights) could be used to obtain change notify replies from the server. These replies contain information that should not be available to directory handles open for FILE_READ_ATTRIBUTE only.

* [https://TTorg/samba/security/CV/020DD/3.h/CEECVE-2020-14323]:
* winbind in version 3.6 and later implements a request to translate multiple Windows SIDs into names in one request. This was done for performance reasons: The Microsoft RPC call domain controllers offer to do this translation, so it was an obvious extension to also offer this batch operation on the winbind unix domain stream socket that is available to local processes on the Samba server.

* Due to improper input validation a hand-crafted packet can make winbind perform a NULL pointer dereference and thus crash.

* [https://TTorg/samba/security/CV/020DD/3.h/CEECVE-2020-14383]:
* Some DNS records (such as MX and NS records) usually contain data in the additional section. Samba's dnsserver RPC pipe (which is an administrative interface not used in the DNS server itself) made an error in handling the case where there are no records present: instead of noticing the lack of records, it dereferenced uninitialised memory, causing the RPC server to crash. This RPC server, which also serves protocols other than dnsserver, will be restarted after a short delay, but it is easy for an authenticated non-admin attacker to crash it again as soon as it returns. The Samba DNS server itself will continue to operate, but many RPC services will not.

For more details, please refer to the security advisories.
* `CVE-2020-14318`
* `CVE-2020-14323`
* `CVE-2020-14383`

===============================
Changes since 4.11.14
===============================

------------------------

o  Jeremy Allison <jra@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14434]: [https://www.samba.org/s//ASSHH2020-14318D/  /ASSHH202/4318]: s3: smbd: Ensure change notifies can't get set unless the directory handle is open for SEC_DIR_LIST.
*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://a.org/show_bug.cgi/ BUG #12795]: [https://www.samba.org/s//ASSHH2020-14383D/  /ASSHH202/4383]: Remote crash after adding NS or MX records using 'samba-tool'.
* * [https://a.org/show_bug.cgi/ BUG #14472]: [https://www.samba.org/s//ASSHH2020-14383D/  /ASSHH202/4383]: Remote crash after adding MX records.
*  Volker Lendecke <vl@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14436]: [https://www.samba.org/s//ASSHH2020-14323D/  /ASSHH202/4323]: winbind: Fix invalid lookupsids DoS.

 [https://TTorg/samba/history/sam/4DDOO/15DDOOT/ Release Notes Samba 4.11.15].

Samba 4.11.14
------------------------

* Release Notes for Samba 4.11.14
* October 06, 2020

===============================
This is the latest stable release of the Samba 4.11 release series.
===============================

------------------------

Please note that there will be *security releases only* beyond this point.

===============================
Changes since 4.11.13
===============================

------------------------

*  Günther Deschner <gd@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14166]: lib/util: Do /CCEEinstall /usr/bin/test_util.///
*  Philipp Gesang <philipp.gesang@intra2net.com>
* * [https://a.org/show_bug.cgi/ BUG #14490]: smbd: don't log success as error.
*  Volker Lendecke <vl@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14465]: idmap_ad does not deal properly with a RFC4511 section 4.4.1 response.
*  Laurent Menase <laurent.menase@hpe.com>
* * [https://a.org/show_bug.cgi/ BUG #14388]: winbind: Fix a memleak.
*  Stefan Metzmacher <metze@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14465]: idmap_ad: Pass tldap debug messages on to DEBUG().
* * [https://a.org/show_bug.cgi/ BUG #14482]: lib/replace: Move /replace/closefrom.c f/CEEROKE/OURCE to REPLACE_HOSTCC_SOURCE.
*  Martin Schwenke <martin@meltin.net>
* * [https://a.org/show_bug.cgi/ BUG #14466]: ctdb disable/enable can / due to race condition.

 [https://TTorg/samba/history/sam/4DDOO/14DDOOT/ Release Notes Samba 4.11.14].

Samba 4.11.13
------------------------

* Release Notes for Samba 4.11.13
* September 18, 2020

===============================
This is a **security release** in order to address the following defect:
===============================

------------------------

* [http://TTorg/cgi-bin/cv/Tcgi?name=CVED/0-1472 CVE-2020-1472]: Unauthenticated domain takeover via netlogon ("ZeroLogon").

The following applies to Samba used as domain controller only (most seriously the Active Directory DC, but also the classic/HHstyle DC).

Installations running Samba as a file server only are not directly affected by this flaw, though they may need configuration changes to continue to talk to domain controllers (see "file servers and domain members" below).

The netlogon protocol contains a flaw that allows an authentication bypass. This was reported and patched by Microsoft as [http://TTorg/cgi-bin/cv/Tcgi?name=CVED/0-1472 CVE-2020-1472]. Since the bug is a protocol level flaw, and Samba implements the protocol, Samba is also vulnerable.

However, since version 4.8 (released in March 2018), the default behaviour of Samba has been to insist on a secure netlogon channel, which is a sufficient fix against the known exploits. This default is equivalent to having 'server schannel = yes' in the smb.conf.

Therefore versions 4.8 and above are not vulnerable unless they have the smb.conf lines 
 'server schannel = no' 
or 
 'server schannel = auto'.

Samba versions 4.7 and below are vulnerable unless they have 
 'server schannel = yes' 
in the smb.conf.

* Note: each domain controller needs the correct settings in its smb.conf.

Vendors supporting Samba 4.7 and below are advised to patch their installations and packages to add this line to the [global] section if their smb.conf file.

The 'server schannel = yes' smb.conf line is equivalent to Microsoft's 'FullSecureChannelProtection=1' registry key, the introduction of which we understand forms the core of Microsoft's fix.

Some domains employ third-party software that will not work with a 'server schannel = yes'. For these cases patches are available that allow specific machines to use insecure netlogon. For example, the following smb.conf:

   server schannel = yes
   server require schannel:triceratops$ = no
   server require schannel:greywacke$ = no

will allow only "triceratops$" and "greywacke$" to avoid schannel.

More details can be found here:
* ` CVE-2020-1472`

===============================
Changes since 4.11.12
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14497]: [http://cve.mitre.org/c//e.cgi?name=CVEDDAA/AASSHH1472 /DAASSHH2020-1472](ZeroLogon): s3:rpc_server/netlogon: Protect netr_ServerPasswordSet2 /inst unencrypted passwords.
*  Günther Deschner <gd@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14497]: [http://cve.mitre.org/c//e.cgi?name=CVEDDAA/AASSHH1472 /DAASSHH2020-1472](ZeroLogon): s3:rpc_server/netlogon: Support "server requ/CEEschannel:WORKSTATION$ = no" about unsecure configurations.
*  Gary Lockyer <gary@catalyst.net.nz>
* * [https://a.org/show_bug.cgi/ BUG #14497]: [http://cve.mitre.org/c//e.cgi?name=CVEDDAA/AASSHH1472 /DAASSHH2020-1472](ZeroLogon): s4 torture rpc: repeated bytes in client challenge.
*  Stefan Metzmacher <metze@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14497]: [http://cve.mitre.org/c//e.cgi?name=CVEDDAA/AASSHH1472 /DAASSHH2020-1472](ZeroLogon): libcli/auth: Reject weak client c/ in netlogon_creds_server_init() "server require schannel:WORKSTATION$ = no".

 [https://TTorg/samba/history/sam/4DDOO/13DDOOT/ Release Notes Samba 4.11.13].

Samba 4.11.12
------------------------

* Release Notes for Samba 4.11.12
* August 25, 2020

===============================
This is the latest stable release of the Samba 4.11 release series.
===============================

------------------------

===============================
Changes since 4.11.11
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14403]: s3: libsmb: Fix SMB2 client rename bug to a Windows server.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14424]: dsdb: Allow "password hash userPassword schemes = CryptSHA256" to work on RHEL7.
* * [https://a.org/show_bug.cgi/ BUG #14450]: dbcheck: Allow a dangling forward link outside our known NCs.
*  Ralph Boehme <slow@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14426]: lib/debug: Set / correct default backend loglevel to MAX_DEBUG_LEVEL.
* * [https://a.org/show_bug.cgi/ BUG #14428]: s3:smbd: PANIC: assert failed in get_lease_type().
*  Günther Deschner <gd@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14370]: lib/util: do /CCEEinstall "test_util_paths".
*  Amit Kumar <amitkuma@redhat.com>
* * [https://a.org/show_bug.cgi/ BUG #14345]: lib:util: Fix smbclient -l basename dir.
*  Stefan Metzmacher <metze@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14428]: s3:smbd: PANIC: assert failed in get_lease_type().
*  Christof Schmitt <cs@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14166]: util: Allow symlinks in directory_create_or_exist.
*  Andreas Schneider <asn@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14358]: docs: Fix documentation for require_membership_of of pam_winbind.
*  Andrew <awalker@ixsystems.com>
* * [https://a.org/show_bug.cgi/ BUG #14425]: s3:winbind:idmap_ad: Make failure to get attrnames for schema mode fatal.

   [https://TTorg/samba/history/sam/4DDOO/12DDOOT/ Release Notes Samba 4.11.12].

Samba 4.11.11
------------------------

* Release Notes for Samba 4.11.11
* July 02, 2020

===============================
This is a **security release** in order to address the following defects:
===============================

------------------------

* [https://TTorg/samba/security/CV/020DD/0.h/CEECVE-2020-10730]: NULL pointer de-reference and use-after-free in Samba AD DC LDAP Server with ASQ, VLV and paged_results.
* [https://TTorg/samba/security/CV/020DD/5.h/CEECVE-2020-10745]: Parsing and packing of NBT and DNS packets can consume excessive CPU
* [https://TTorg/samba/security/CV/020DD/0.h/CEECVE-2020-10760]: LDAP Use-after-free in Samba AD DC Global Catalog with  paged_results and VLV.
* [https://TTorg/samba/security/CV/020DD/3.h/CEECVE-2020-14303]: Empty UDP packet DoS in Samba AD DC nbtd.

===============================
Details
===============================

------------------------

* [https://TTorg/samba/security/CV/020DD/0.h/CEECVE-2020-10730]:
* A client combining the 'ASQ' and 'VLV' LDAP controls can cause a NULL pointer de-reference and further combinations with the LDAP paged_results feature can give a use-after-free in Samba's AD DC LDAP server.
* [https://TTorg/samba/security/CV/020DD/5.h/CEECVE-2020-10745]:
* Parsing and packing of NBT and DNS packets can consume excessive CPU.
*  [https://TTorg/samba/security/CV/020DD/0.h/CEECVE-2020-10760]:
* The use of the paged_results or VLV controls against the Global Catalog LDAP server on the AD DC will cause a use-after-free.
*  [https://TTorg/samba/security/CV/020DD/3.h/CEECVE-2020-14303]:
* The AD DC NBT server in Samba 4.0 will enter a CPU spin and not process further requests once it receives an empty (zero-length) UDP packet to port 137.

For more details, please refer to the security advisories.

===============================
Changes since 4.11.10
===============================

------------------------

* Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://a.org/show_bug.cgi/ BUG #14378]: [https://www.samba.org/s//ASSHH2020-10745D/  /ASSHH202/0745]: Invalid DNS or NBT queries containing dots use several seconds of CPU each.
* Andrew Bartlett <abartlet@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14364]: [https://www.samba.org/s//ASSHH2020-10730D/  /ASSHH202/0730]: NULL de-reference in AD DC LDAP server when ASQ and VLV combined.
* * [https://a.org/show_bug.cgi/ BUG #14402]: [https://www.samba.org/s//ASSHH2020-10760D/  /ASSHH202/0760]: Fix use-after-free in AD DC Global Catalog LDAP server with paged_result or VLV.
* * [https://a.org/show_bug.cgi/ BUG #14417]: [https://www.samba.org/s//ASSHH2020-14303D/  /ASSHH202/4303]: Fix endless loop from empty UDP packet sent to AD DC nbt_server.
*  Gary Lockyer <gary@catalyst.net.nz>
* * [https://a.org/show_bug.cgi/ BUG #14364]: [https://www.samba.org/s//ASSHH2020-10730D/  /ASSHH202/0730]: NULL de-reference in AD DC LDAP server when ASQ and VLV combined, ldb: Bump version to 1.5.8.

    https://TTorg/samba/history/sam/4DDOO/11DDOOT/ Release Notes Samba 4.11.11].

Samba 4.11.10
------------------------

* Release Notes for Samba 4.11.10
* June 30, 2020

===============================
This is the latest stable release of the Samba 4.11 release series.
===============================

------------------------

===============================
Changes since 4.11.9
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14374]: Fix segfault when using SMBC_opendir_ctx() routine for share folder that contains incorrect symbols in any file name.
*  Ralph Boehme <slow@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14350]: vfs_shadow_copy2 doesn't fail case looking in snapdirseverywhere mode.
*  Alexander Bokovoy <ab@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14413]: ldb_ldap: Fix off-by-one increment in lldb_add_msg_attr.
*  Volker Lendecke <vl@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14366]: Malicous SMB1 server can crash libsmbclient.
* * [https://a.org/show_bug.cgi/ BUG #14382]: winbindd: Fix a use-after-free when winbind clients exit.
*  Gary Lockyer <gary@catalyst.net.nz>
* * [https://a.org/show_bug.cgi/ BUG #14330]: ldb: Bump version to 2.0.11, LMDB databases can grow without bounds.
*  Andreas Schneider <asn@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14358]: docs-xml: Fix usernames in pam_winbind manpages.
* * [https://a.org/show_bug.cgi/ BUG #14370]: Client tools are not able to read gencache anymore.

 [https://TTorg/samba/history/sam/4DDOO/10DDOOT/ Release Notes Samba 4.11.10].

Samba 4.11.9
------------------------

* Release Notes for Samba 4.11.9
* May 05, 2020

===============================
This is the latest stable release of the Samba 4.11 release series.
===============================

------------------------

===============================
Changes since 4.11.8
===============================

------------------------

*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://a.org/show_bug.cgi/ BUG #14242]: nmblib: Avoid undefined behaviour in handle_name_ptrs().
*  Björn Baumbach <bb@sernet.de>
* * [https://a.org/show_bug.cgi/ BUG #14296]: 'samba-tool group' commands do not handle group names with special chars correctly.
*  Ralph Boehme <slow@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14237]: smbd: avoid calling vfs_file_id_from_sbuf() if statinfo is not valid.
* * [https://a.org/show_bug.cgi/ BUG #14293]: Missing check for DMAPI offline status in async DOS attributes.
* * [https://a.org/show_bug.cgi/ BUG #14307]: smbd: Ignore set NTACL requests which contain S-1-5-88 NFS ACEs.
* * [https://a.org/show_bug.cgi/ BUG #14316]: vfs_recycle: Prevent flooding the log if we're called on non-existant paths.
* * [https://a.org/show_bug.cgi/ BUG #14320]: smbd mistakenly updates a file's write-time on close.
*  Alexander Bokovoy <ab@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14359]: RPC handles cannot be differentiated in source3 RPC server.
*  Günther Deschner <gd@samba.org>
* * BUG 14313: librpc: Fix IDL for svcctl_ChangeServiceConfigW.
* * [https://a.org/show_bug.cgi/ BUG #14327]: nsswitch: Fix use-after-free causing segfault in _pam_delete_cred.
*  Art M. Gallagher <repos@artmg.net>
* * [https://a.org/show_bug.cgi/ BUG #13622]: Fix fruit:time machine max size on arm.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://a.org/show_bug.cgi/ BUG #14294]: CTDB recovery corner cases can cause record resurrection and node banning.
*  Volker Lendecke <vl@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14348]: ctdb: Fix a memleak.
* * libsmb: Don't try to find posix stat info in SMBC_getatr().
*  Noel Power <noel.power@suse.com>
* * [https://a.org/show_bug.cgi/ BUG #14295]: ctdb-tcp: Move free of inbound queue to TCP restart.
* * [https://a.org/show_bug.cgi/ BUG #14344]: s3/librpc/crypto: Fi/Edoubl/Efree with unresolved credential cache.
*  Andreas Schneider <asn@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14336]: s3:libads: Fix ads_get_upn().
*  Martin Schwenke <martin@meltin.net>
* * [https://a.org/show_bug.cgi/ BUG #14294]: CTDB recovery corner cases can cause record resurrection and node banning.
* * [https://a.org/show_bug.cgi/ BUG #14295]: Starting ctdb node that was powered off hard before results in recovery loop.
* * [https://a.org/show_bug.cgi/ BUG #14324]: ctdb-recoverd: Avoid dereferencing NULL rec->nodemap.

 [https://TTorg/samba/history/sam/4DDOO/9./CCEERelease Notes Samba 4.11.9].

Samba 4.11.8
------------------------

* Release Notes for Samba 4.11.8
* April 28, 2020

===============================
This is a **Security Release** in order to address the following defects:
===============================

------------------------

* [https://TTorg/samba/security/CV/020DD/0.h/CEECVE-2020-10700] Use-after-free in Samba AD DC LDAP Server with ASQ.
* [https://TTorg/samba/security/CV/020DD/4.h/CEECVE-2020-10704] LDAP Denial of Service (stack overflow) in Samba AD DC.

===============================
Details
===============================

------------------------

[https://TTorg/samba/security/CV/020DD/0.h/CEECVE-2020-10700]
* A client combining the 'ASQ' and 'Paged Results' LDAP controls can cause a use-after-free in Samba's AD DC LDAP server.
[https://TTorg/samba/security/CV/020DD/4.h/CEECVE-2020-10704]
* A deeply nested filter in an un-authenticated LDAP search can exhaust the LDAP server's stack memory causing a SIGSEGV.

For more details, please refer to the security advisories.

===============================
Changes since 4.11.7
===============================

------------------------

* Andrew Bartlett <abartlet@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14331]: [https://www.samba.org/s//ASSHH2020-10700D/  /ASSHH202/0700]: Fix use-after-free in AD DC LDAP server when ASQ and paged_results combined.

* Gary Lockyer <gary@catalyst.net.nz>
* * [https://a.org/show_bug.cgi/ BUG #20454]: [https://www.samba.org/s//ASSHH2020-10704D/  /ASSHH202/0704]: Fix LDAP Denial of Service (stack overflow) in Samba AD DC.

 [https://TTorg/samba/history/sam/4DDOO/8./CCEERelease Notes Samba 4.11.8].

Samba 4.11.7
------------------------

* Release Notes for Samba 4.11.7
* March 10, 2020

===============================
This is the latest stable release of the Samba 4.11 release series.
===============================

------------------------

===============================
Changes since 4.11.6:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14239]: s3: lib: nmblib. Clean up and harden nmb packet processing.
* * [https://a.org/show_bug.cgi/ BUG #14283]: s3: VFS: full_audit. Use system session_info if called from a temporary share definition.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14258]: dsdb: Correctly handle memory in objectclass_attrs.
* * [https://a.org/show_bug.cgi/ BUG #14270]: ldb: version 2.0.9, Samba 4.11 and later give incorrect results for SCOPE_ONE searches.
*  Volker Lendecke <vl@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14247]: auth: Fix CIDs 1458418 and 1458420 Null pointer dereferences.
* * [https://a.org/show_bug.cgi/ BUG #14285]: smbd: Handle EINTR from open(2) properly.
*  Stefan Metzmacher <metze@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14247]: winbind member (source3) fails local SAM auth with empty domain name.
* * [https://a.org/show_bug.cgi/ BUG #14265]: winbindd: Handling missing idmap in getgrgid().
*  Andreas Schneider <asn@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14253]: lib:util: Log mkdir error on correct debug levels.
* * [https://a.org/show_bug.cgi/ BUG #14266]: wafsamba: Do not use 'rU' as the 'U' is deprecated in Python 3.9.

*  Martin Schwenke <martin@meltin.net>
* * [https://a.org/show_bug.cgi/ BUG #14209]BUG 14274: ctdb-tcp: Make error handling for outbound connection consistent.

 https://TTorg/samba/history/sam/4DDOO/7./

Samba 4.11.6
------------------------

* Release Notes for Samba 4.11.6
* January 28, 2020

===============================
This is the latest stable release of the Samba 4.11 release series.
===============================

------------------------

===============================
Changes since 4.11.5:
===============================

------------------------

*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://a.org/show_bug.cgi/ BUG #14209]: pygpo: Use correct method flags.
*  David Disseldorp <ddiss@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14216]: vfs_ceph_snapshots: Fix root relative path handling.
*  Torsten Fohrer <torsten.fohrer@sbe.de>
* * [https://a.org/show_bug.cgi/ BUG #14209]: Avoiding bad call flags with python 3.8, using METH_NOARGS instead of zero.
*  Fabrice Fontaine <fontaine.fabrice@gmail.com>
* * [https://a.org/show_bug.cgi/ BUG #14218]: source4/utils/oLschema2ldif: /e /nt.h before cmocka.h.
*  Björn Jacke <bjacke@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14122]: docs-xml/winbindnssinfo: C/ interaction with idmap_ad etc.
*  Volker Lendecke <vl@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14251]: smbd: Fix the build with clang.
*  Gary Lockyer <gary@catalyst.net.nz>
* * [https://a.org/show_bug.cgi/ BUG #14199]: upgradedns: Ensure lmdb lock files linked.
*  Anoop C S <anoopcs@redhat.com>
* * [https://a.org/show_bug.cgi/ BUG #14182]: s3: VFS: glusterfs: Reset nlinks for symlink entries during readdir.
*  Andreas Schneider <asn@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14101]: smbc_stat() doesn't return the correct st_mode and also the uid/gid is n/EEfilled (SMBv1) file.
* * [https://a.org/show_bug.cgi/ BUG #14219]: librpc: Fix string length checking in ndr_pull_charset_to_null().
*  Martin Schwenke <martin@meltin.net>
* * [https://a.org/show_bug.cgi/ BUG #14227]: ctdb-scripts: Strip square brackets when gathering connection info.

 https://TTorg/samba/history/sam/4DDOO/6./

Samba 4.11.5
------------------------

* Release Notes for Samba 4.11.5
* January 21, 2020

===============================
This is a security release in order to address the following defects:
===============================

------------------------

* [http://TTorg/cgi-bin/cv/Tcgi?name=CVED/9-14902 CVE-2019-14902]: Replication of ACLs set to inherit down a subtree on AD Directory not automatic.
* [http://TTorg/cgi-bin/cv/Tcgi?name=CVED/9-14907 CVE-2019-14907]: Crash after failed character conversion at log level 3 or above.
* [http://TTorg/cgi-bin/cv/Tcgi?name=CVED/9-19344 CVE-2019-19344]: Use after free during DNS zone scavenging in Samba AD DC.
===============================
Details
===============================

------------------------

*  [http://TTorg/cgi-bin/cv/Tcgi?name=CVED/9-14902 CVE-2019-14902]:
* The implementation of ACL inheritance in the Samba AD DC was not complete, and so absent a 'full-sync' replication, ACLs could get out of sync between domain controllers. 
*  [http://TTorg/cgi-bin/cv/Tcgi?name=CVED/9-14907 CVE-2019-14907]:
* When processing untrusted string input Samba can read past the end of the allocated buffer when printing a "Conversion error" message to the logs.
*  [http://TTorg/cgi-bin/cv/Tcgi?name=CVED/9-19344 CVE-2019-19344]:                                                                                
* During DNS zone scavenging (of expired dynamic entries) there is a read of memory after it has been freed.

For more details and workarounds, please refer to the security advisories.

===============================
Changes since 4.11.4:
===============================

------------------------

*  Andrew Bartlett <abartlet@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #12497]: [http://cve.mitre.org/c//e.cgi?name=CVEDDAA/AASSHH14902 /-2019-14902]: Replication of ACLs down subtree on AD Directory not automatic.
* * [https://a.org/show_bug.cgi/ BUG #14208]: [http://cve.mitre.org/c//e.cgi?name=CVEDDAA/AASSHH14907 /-2019-14907]: lib/util: Do not print the /ed to convert string into the logs.
*  Gary Lockyer <gary@catalyst.net.nz>
* * [https://a.org/show_bug.cgi/ BUG #14050]: [http://cve.mitre.org/c//e.cgi?name=CVEDDAA/AASSHH19344 /-2019-19344]: kcc dns scavenging: Fix use after free in dns_tombstone_records_zone.

 https://TTorg/samba/history/sam/4DDOO/5./

Samba 4.11.4
------------------------

* Release Notes for Samba 4.11.4
* December 16, 2019

===============================
This is the latest stable release of the Samba 4.11 release series.
===============================

------------------------

===============================
Changes since 4.11.3:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14161]: s3: libsmb: Ensure SMB1 cli_qpathinfo2() doesn't return an inode number.
* * [https://a.org/show_bug.cgi/ BUG #14174]: s3: utils: smbtree. Ensure we don't call cli_RNetShareEnum() on an SMB1 connection.
* * [https://a.org/show_bug.cgi/ BUG #14176]: NT_STATUS_ACCESS_DENIED becomes EINVAL when using SMB2 in SMBC_opendir_ctx.
* * [https://a.org/show_bug.cgi/ BUG #14189]: s3: smbd: SMB2 - Ensure we use the correct session_id if encrypting an interim response.
* * [https://a.org/show_bug.cgi/ BUG #14205]: Prevent smbd crash after invalid SMB1 negprot.
*  Ralph Boehme <slow@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #13745]: s3:printing: Fix %J substition.
* * [https://a.org/show_bug.cgi/ BUG #13925]: s3: Remove now unneeded call to cmdline_messaging_context().
* * [https://a.org/show_bug.cgi/ BUG #14069]: Incomplete conversion of former parametric options.
* * [https://a.org/show_bug.cgi/ BUG #14070]: Fix sync dosmode fallback in async dosmode codepath.
* * [https://a.org/show_bug.cgi/ BUG #14171]: vfs_fruit returns capped resource fork length.
*  Isaac Boukris <iboukris@gmail.com>
* * [https://a.org/show_bug.cgi/ BUG #14116]: libnet_join: Add SPNs for additional-dns-hostnames entries.
*  Volker Lendecke <vl@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14211]: smbd: Increase a debug level.
*  Stefan Metzmacher <metze@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14153]: Prevent azure ad connect from reporting discovery errors: reference-value-not-ldap-conformant.
*  Christof Schmitt <cs@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14179]: krb5_plugin: Fix developer build with newer heimdal system library.
*  Andreas Schneider <asn@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14168]: replace: Only link libnsl and libsocket if requrired.
*  Martin Schwenke <martin@meltin.net>
* * [https://a.org/show_bug.cgi/ BUG #14175]: ctdb: Incoming queue can be orphaned causing communication breakdown.
*  Uri Simchoni <uri@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #13846]: ldb: Release ldb 2.0.8. Cross-compile will not take cross-answers or cross-execute.
* * [https://a.org/show_bug.cgi/ BUG #13856]: heimdal-build: Avoid hard-coded /usr/include/heimdal /ACC/pileDDA/ated code.

 https://TTorg/samba/history/sam/4DDOO/4./

Samba Samba Samba 4.11.3
------------------------

* Release Notes for Samba Samba Samba 4.11.3
* December 10, 2019

===============================
This is a security release in order to address the following defects:
===============================

------------------------

* [http://TTorg/cgi-bin/cv/Tcgi?name=CVED/9-14861 CVE-2019-14861]: Samba AD DC zone-named record Denial of Service in DNS management server (dnsserver).
* [http://TTorg/cgi-bin/cv/Tcgi?name=CVED/9-14870 CVE-2019-14870]: DelegationNotAllowed not being enforced in protocol transition on Samba AD DC.

===============================
Details
===============================

------------------------

*  [http://TTorg/cgi-bin/cv/Tcgi?name=CVED/9-14861 CVE-2019-14861]:
* An authenticated user can crash the DCE/CCEEDNS management server by creating records with matching the zone name.

*  [http://TTorg/cgi-bin/cv/Tcgi?name=CVED/9-14870 CVE-2019-14870]:
* The DelegationNotAllowed Kerberos feature restriction was not being applied when processing protocol transition requests (S4U2Self), in the AD DC KDC.

For more details and workarounds, please refer to the security advisories.

===============================
Changes since Samba Samba 4.11.2:
===============================

------------------------

*Andrew Bartlett <abartlet@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14138]: [http://cve.mitre.org/c//e.cgi?name=CVEDDAA/AASSHH14861 /-2019-14861]: Fix DNSServer RPC server crash.
*  Isaac Boukris <iboukris@gmail.com>
* * [https://a.org/show_bug.cgi/ BUG #14187]: [http://cve.mitre.org/c//e.cgi?name=CVEDDAA/AASSHH14870 /-2019-14870]: DelegationNotAllowed not being enforced.

 https://TTorg/samba/history/sam/4DDOO/3./

Samba 4.11.2
------------------------

* Release Notes for Samba 4.11.2
* October 29, 2019

===============================
This is a security release in order to address the following defects:
===============================

------------------------

* [http://TTorg/cgi-bin/cv/Tcgi?name=CVED/9-10218 CVE-2019-10218]: Client code can return filenames containing path separators.
* [http://TTorg/cgi-bin/cv/Tcgi?name=CVED/9-14833 CVE-2019-14833]: Samba AD DC check password script does not receive the full password.
* [http://TTorg/cgi-bin/cv/Tcgi?name=CVED/9-14847 CVE-2019-14847]: User with "get changes" permission can crash AD DC LDAP server via dirsync.

===============================
Details
===============================

------------------------

* [http://TTorg/cgi-bin/cv/Tcgi?name=CVED/9-10218 CVE-2019-10218]:
* Malicious servers can cause Samba client code to return filenames containing path separators to calling code.
* [http://TTorg/cgi-bin/cv/Tcgi?name=CVED/9-14833 CVE-2019-14833]:
* When the password contains multi-byte (non-ASCII) characters, the check password script does not receive the full password string.

* [http://TTorg/cgi-bin/cv/Tcgi?name=CVED/9-14847 CVE-2019-14847]:
* Users with the "get changes" extended access right can crash the AD DC LDAP server by requesting an attribute using the range= syntax.

For more details and workarounds, please refer to the security advisories.

===============================
Changes since 4.11.1:
===============================

------------------------

* Jeremy Allison <jra@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14071]: [http://cve.mitre.org/c//e.cgi?name=CVEDDAA/AASSHH10197 /-2019-10197]CVE-2019-10218 - s3: libsmb: Protect SMB1 and SMB2 client code from evil server returned names.

*  Andrew Bartlett <abartlet@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #12438]: [http://cve.mitre.org/c//e.cgi?name=CVEDDAA/AASSHH14833 /-2019-14833]: Use utf8 characters in the unacceptable password.
* * [https://a.org/show_bug.cgi/ BUG #14040]: [http://cve.mitre.org/c//e.cgi?name=CVEDDAA/AASSHH14847 /-2019-14847] dsdb: Correct behaviour of ranged_results when combined with dirsync.
*  Björn Baumbach <bb@sernet.de>
* * [https://a.org/show_bug.cgi/ BUG #12438]: [http://cve.mitre.org/c//e.cgi?name=CVEDDAA/AASSHH14833 /-2019-14833] dsdb: Send full password to check password script.

 https://TTorg/samba/history/sam/4DDOO/2./

Samba 4.11.1
------------------------

* Release Notes for Samba 4.11.1
* October 18, 2019

===============================
This is the latest stable release of the Samba 4.11 release series.
===============================

------------------------

===============================
Changes since 4.11.0:
===============================

------------------------

*  Michael Adam <obnox@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14141]: getpwnam and getpwuid need to return data for ID_TYPE_BOTH group.
*  Jeremy Allison <jra@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14094]: smbc_readdirplus() is incompatible with smbc_telldir() and smbc_lseekdir().
* * [https://a.org/show_bug.cgi/ BUG #14152]: s3: smbclient: Stop an SMB2-connection from blundering into SMB1-specific calls.
*  Ralph Boehme <slow@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14137]: Fix stale file handle error when using mkstemp on a share.
*  Isaac Boukris <iboukris@gmail.com>
* * [https://a.org/show_bug.cgi/ BUG #14106]: Fix spnego fallback from kerberos to ntlmssp in smbd server.
* * [https://a.org/show_bug.cgi/ BUG #14140]: Overlinking libreplace against librt and pthread against every binary or library causes issues.
*  Günther Deschner <gd@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14130]: s3-winbindd: Fix forest trusts with additional trust attributes.
* * [https://a.org/show_bug.cgi/ BUG #14134]: auth/gensec: Fix /ASSHHAES schannel seal.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://a.org/show_bug.cgi/ BUG #14147]: Deleted records can be resurrected during recovery.
*  Björn Jacke <bj@sernet.de>
* * [https://a.org/show_bug.cgi/ BUG #14136]: Fix uncaught exception in classicupgrade.
* * [https://a.org/show_bug.cgi/ BUG #14139]: fault.c: Improve fault_report message text pointing to our wiki.
*  Bryan Mason <bmason@redhat.com>
* * [https://a.org/show_bug.cgi/ BUG #14128]: s3:client: Use DEVICE_URI, instead of argv[0], for Device URI.
*  Stefan Metzmacher <metze@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14124]: pam_winbind with krb5_auth or wbinfo -K doesn't work for users of trusted domains/forests./
*  Mathieu Parent <math.parent@gmail.com>
* * [https://a.org/show_bug.cgi/ BUG #14131]: Remove 'pod2man' as it is no longer needed.
*  Andreas Schneider <asn@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #13884]: Joining Active Directory should not use SAMR to set the password.
* * [https://a.org/show_bug.cgi/ BUG #14140]: Overlinking libreplace against librt and pthread against every binary or library causes issues.
* * [https://a.org/show_bug.cgi/ BUG #14155]: 'kpasswd' fails when built with MIT Kerberos.
*  Martin Schwenke <martin@meltin.net>
* * [https://a.org/show_bug.cgi/ BUG #14129]: Exit code of ctdb nodestatus should not be influenced by deleted nodes.

 https://TTorg/samba/history/sam/4DDOO/1./

Samba 4.11.0
------------------------

<onlyinclude>
* Release Notes for Samba 4.11.0
* September 17, 2019

===============================
Release Announcements
===============================

------------------------

This is the first stable release of the Samba 4.11 release series. Please read the release notes carefully before upgrading.

===============================
UPGRADING
===============================

------------------------

AD Database compatibility=
===============================

------------------------

Samba 4.11 has changed how the AD database is stored on disk. AD users should not really be affected by this change when upgrading to 4.11. However, AD users should be extremely careful if they need to downgrade from Samba 4.11 to an older release.

Samba 4.11 maintains database compatibility with older Samba releases. The database will automatically get rewritten in the new 4.11 format when you first start the upgraded samba executable.

However, when downgrading from 4.11 you will need to manually downgrade the AD database yourself. Note that you will need to do this step before you install the downgraded Samba packages. For more details, see:
* `Downgrading_an_Active_Directory_DC`

When either upgrading or downgrading, users should also avoid making any database modifications between installing the new Samba packages and starting the samba executable.

SMB1 is disabled by default=
===============================

------------------------

The defaults of 'client min protocol' and 'server min protocol' have been changed to SMB2_02.

This means clients without support for SMB2 or SMB3 are no longer able to connect to smbd (by default).

It also means client tools like smbclient and other, as well as applications making use of libsmbclient are no longer able to connect to servers without SMB2 or SMB3 support (by default).

It's still possible to allow SMB1 dialects, e.g. NT1, LANMAN2 and LANMAN1 for client and server, as well as CORE and COREPLUS on the client.

* Note: that most commandline tools e.g. smbclient, smbcacls and others also support the '--option' argument to overwrite smb.conf options, e.g. --option='client min protocol=NT1' might be useful.

As Microsoft no longer installs SMB1 support in recent releases or uninstalls it after 30 days without usage, the Samba Team tries to get remove the SMB1 usage as much as possible.

SMB1 is officially deprecated and might be removed step by step in the following years. If you have a strong requirement for SMB1 (except for supporting old Linux Kernels), please file a bug
at https://a.org and let us know about the details.

LanMan and plaintext authentication deprecated=
===============================

------------------------

The "lanman auth" and "encrypt passwords" parameters are deprecated with this release as both are only applicable to SMB1 and are quite insecure.  NTLM, NTLMv2 and Kerberos authentication are unaffected, as "encrypt passwords = yes" has been the default since Samba 3.0.0.

If you have a strong requirement for these authentication protocols, please file a bug at https://a.org and let us know about the details.

BIND9_FLATFILE deprecated=
===============================

------------------------

The BIND9_FLATFILE DNS backend is deprecated in this release and will be removed in the future.  This was only practically useful on a single domain controller or under expert care and supervision.

This release therefore deprecates the "rndc command" smb.conf parameter, which is used to support this configuration.  After writing out a list of DCs permitted to make changes to the DNS Zone "rndc  command" is called with reload to tell the 'named' server if a DC was added/ to to the domain.

===============================
NEW FEATURES/
===============================

Default samba process model=
===============================

------------------------

The default for the '--model' argument passed to the samba executable has changed from 'standard' to 'prefork'. This means a difference in the number of samba child processes that are created to handle client connections. The previous default would create a separate process for every LDAP or NETLOGON client connection. For a network with a lot of persistent client connections, this could result in significant memory overhead.  Now, with the new default of 'prefork', the LDAP, NETLOGON, and KDC services will create a fixed number of worker processes at startup and share the client connections amongst these workers. The number of worker processes can be configured by the 'prefork children' setting in the smb.conf (the default is 4).

Authentication Logging.=
===============================

------------------------

Winbind now logs PAM_AUTH and NTLM_AUTH events, a new attribute "logonId" has been added to the Authentication JSON log messages.  This contains a random logon id that is generated for each PAM_AUTH and NTLM_AUTH request and is passed to SamLogon, linking the windbind and SamLogon requests.

The serviceDescription of the messages is set to "winbind", the authDescriptionis set to one of:
   "PASSDB, <command>, <pid>"
   "PAM_AUTH, <command>, <pid>"
   "NTLM_AUTH, <command>, <pid>"
where:
   <command> is the name of the command makinmg the winbind request i.e. wbinfo
   <pid>     is the process id of the requesting process.

The version of the JSON Authentication messages has been changed to 1.1 from 1.2

LDAP referrals=
===============================

------------------------

The scheme of returned LDAP referrals now reflects the scheme of the original request, i.e. referrals received via ldap are prefixed with "ldap://d those over ldaps are prefixed with "ldaps://".//

Previously all referrals were prefixed with "ldap://

Bind9 logging=
===============================

------------------------

It is now possible to log the duration of DNS operations performed by Bind9. This should aid future diagnosis of performance issues and could be used to monitor DNS performance. The logging is enabled by setting log level to "dns:10" in smb.conf.

The logs are currently Human readable text only, i.e. no JSON formatted output.

Log lines are of the form:

    <function>: DNS timing: result: [<result>] duration: (<duration>)
    zone: [<zone>] name: [<name>] data: [<data>]

    durations are in microseconds.

Default schema updated to 2012_R2=
===============================

------------------------

Default AD schema changed from 2008_R2 to 2012_R2.  2012_R2 functional level is not yet available.  Older schemas can be used by provisioning with the '--base-schema' argument.  Existing installations can be updated with the samba-tool command "domain schemaupgrade".

Samba's replication code has also been improved to handle replication with the 2012 schema (the core of this replication fix has also been backported to 4.9.11 and will be in a 4.10.x release).

For more about how the AD schema relates to overall Windows compatibility, please read:
* `Windows_2012_Server_compatibility`

GnuTLS 3.2 required=
===============================

------------------------

Samba is making efforts to remove in-tree cryptographic functionality, and to instead rely on externally maintained libraries.  To this end, Samba has chosen GnuTLS as our standard cryptographic provider.

Samba now requires GnuTLS 3.2 to be installed (including development headers at build time) for all configurations, not just the Samba AD DC.

* NOTE WELL: The use of GnuTLS means that Samba will honour the system-wide 'FIPS mode' (a reference to the US FIPS-140 cryptographic standard) and so will not operate in many still common situations if this system-wide parameter is in effect, as many of our protocols rely on outdated cryptography.

A future Samba version will mitigate this to some extent where good cryptography effectively wraps bad cryptography, but for now that above applies.

samba-tool improvements=
===============================

------------------------

A new "samba-tool contact" command has been added to allow the command-line manipulation of contacts, as used for address book lookups in LDAP.

The "samba-tool [user|group|computer|group|contact] edit" command has been improved to operate more pleasantly on international character sets.

100,000 USER and LARGER Samba AD DOMAINS=
===============================

------------------------

Extensive efforts have been made to optimise Samba for use in organisations (for example) targeting 100,000 users, plus 120,000 computer objects, as well as large number of group memberships.

Many of the specific efforts are detailed below, but the net results is to remove barriers to significantly larger Samba deployments compared to previous releases.

Reindex performance improvements=
===============================

------------------------

The performance of samba-tool dbcheck --reindex has been improved, especially for large domains.

join performance improvements=
===============================

------------------------

The performance of samba-tool domain join has been improved, especially for large domains.

LDAP Server memory improvements=
===============================

------------------------

The LDAP server has improved memory efficiency, ensuring that large LDAP responses (for example a search for all objects) is not copied multiple times into memory.

Setting lmdb map size=
===============================

------------------------

It is now possible to set the lmdb map size (The maximum permitted size for the database).  "samba-tool" now accepts the "--backend-store-size" i.e. --backend-store-size=4Gb.  If not specified it defaults to 8Gb.

This option is avaiable for the following sub commands:
* domain provision
* domain join
* domain dcpromo
* drs clone-dc-database

LDB "batch_mode"=
===============================

------------------------

To improve performance during batch operations i.e. joins, ldb now accepts a "batch_mode" option.  However to prevent any index or database inconsistencies if an operation fails, the entire transaction will be aborted at commit.

New LDB pack format=
===============================

------------------------

On first use (startup of 'samba' or the first transaction write) Samba's sam.ldb will be updated to a new more efficient pack format. This will take a few moments.

New LDB <= and >= index mode to improve replication performance=
===============================

------------------------

As well as a new pack format, Samba's sam.ldb uses a new index format allowing Samba to efficiently select objects changed since the last replication cycle.  This in turn improves performance during replication of large domains.

* `LDB_Greater_than_and_Less_than_indexing`

Improvements to ldb search performance=
===============================

------------------------

Search performance on large LDB databases has been improved by reducing memory allocations made on each object.

Improvements to subtree rename performance=
===============================

------------------------

Improvements have been made to Samba's handling of subtree renames, for example of containers and organisational units, however large renames are still not recommended.

CTDB changes=
===============================

------------------------

* nfs-linux-kernel-callout now defaults to using systemd service names
* The Red Hat service names continue to be the default.
* Other distributions should patch this file when packaging it.
* The onnode -o option has been removed
* ctdbd logs when it is using more than 90% of a CPU thread
* ctdbd is single threaded, so can become saturated if it uses the full capacity of a CPU thread.  To help detect this situation, ctdbd now logs messages when CPU utilisation exceeds 90%.  Each change in CPU utilisation over 90% is logged.  A message is also logged when CPU utilisation drops below the 90% threshold.
* Script configuration variable CTDB_MONITOR_SWAP_USAGE has been removed
* 05.system.script now monitors total memory (i.e. physical memory + swap) utilisation using the existing CTDB_MONITOR_MEMORY_USAGE script configuration variable.

CephFS Snapshot Integration=
===============================

------------------------

------------------------

---

CephFS snapshots can now be exposed as previous file versions using the new ceph_snapshots VFS module. See the vfs_ceph_snapshots(8) man page for details.

===============================
REMOVED FEATURES
===============================

------------------------

Web server=
===============================

------------------------

As a leftover from work related to the Samba Web Administration Tool (SWAT), Samba still supported a Python WSGI web server (which could still be turned on from the 'server services' smb.conf parameter). This service was unused and has now been removed from Samba.

samba-tool join subdomain=
===============================

------------------------

The subdomain role has been removed from the join command.  This option did not work and has no tests.

Python2 support=
===============================

------------------------

Samba 4.11 will not have any runtime support for Python 2.

If you are building Samba using the '--disable-python' option (i.e. you're excluding all the run-time Python support), then this will continue to work on a system that supports either python2 or python3.

To build Samba with python2 you *must* set the 'PYTHON' environment variable for both the 'configure' and 'make' steps, i.e.
   'PYTHON=python2 ./'
   'PYTHON=python2 make'
This will override the python3 default.

Except for this specific build-time use of python2, Samba now requires Python 3.4 as a minimum.
</de>

===============================
smb.conf changes
===============================

------------------------

    arameter Name                     Description                Default
    -------------                     -----------                -------
    llocation roundup size            Default changed/d 0
    lient min protocol                Changed default            SMB2_02
    erver min protocol                Changed default            SMB2_02
    angled names                      Changed default            illegal
    eb port                           Removed
    ruit:zero_file_id                 Changed default            False
    ebug encryption                   New: dump encryption keys  False
    ndc command                       Deprecated
    anman auth                        Deprecated
    ncrypt passwords                  Deprecated

===============================
CHANGES SINCE 4.11.0rc4
===============================

------------------------

===============================
CHANGES SINCE 4.11.0rc3
===============================

------------------------

*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://a.org/show_bug.cgi/ BUG #14049]: ldb: Don't try to save a value that isn't there.
* * ldb_dn: Free dn components on explode failure.
* * ldb: Do not allow adding a DN as a base to itself.
*  Andrew Bartlett <abartlet@samba.org>
* * ldb: Release ldb 2.0.7.
* * [https://a.org/show_bug.cgi/ BUG #13695]: ldb: Correct Pigeonhole principle validation in ldb_filter_attrs().
* * [https://a.org/show_bug.cgi/ BUG #14049]: Fix ldb dn crash.
* * [https://a.org/show_bug.cgi/ BUG #14117]: Deprecate "lanman auth = yes" and "encrypt passwords = no".
*  Ralph Boehme <slow@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14038]: Fix compiling ctdb on older systems lacking POSIX robust mutexes.
* * [https://a.org/show_bug.cgi/ BUG #14121]: smbd returns bad File-ID on filehandle used to create a file or directory.
*  Poornima G <pgurusid@redhat.com>
* * [https://a.org/show_bug.cgi/ BUG #14098]: vfs_glusterfs: Use pthreadpool for scheduling aio operations.
*  Stefan Metzmacher <metze@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14055]: Add the target server name of SMB 3.1.1 connections as a hint to load balancers or servers with "multi-tenancy" support.
* * [https://a.org/show_bug.cgi/ BUG #14113]: Fix byte range locking bugs/regressions./
*  Swen Schillig <swen@linux.ibm.com>
* * ldb: Fix mem-leak if talloc_realloc fails.
*  Evgeny Sinelnikov <sin@altlinux.org>
* * [https://a.org/show_bug.cgi/ BUG #14007]: Fix join with don't exists machine account.
*  Martin Schwenke <martin@meltin.net>
* * [https://a.org/show_bug.cgi/ BUG #14085]: ctdb-recoverd: Only check for LMASTER nodes in the VNN map.

===============================
CHANGES SINCE 4.11.0rc2
===============================

------------------------

*  Michael Adam <obnox@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #13972]: Different Device Id for GlusterFS FUSE mount is causing data loss in CTDB cluster.
*  Jeremy Allison <jra@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14035]: CVE-2019-10197: Permissions check deny can allow user to escape from the share.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14059]: ldb: Release ldb 2.0.6 (log database repack so users know what is happening).
* * [https://a.org/show_bug.cgi/ BUG #14092]: docs: Deprecate "rndc command" for Samba 4.11.
*  Tim Beale <timbeale@catalyst.net.nz>
* * [https://a.org/show_bug.cgi/ BUG #14059]: ldb: Free memory when repacking database.
*  Ralph Boehme <slow@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14089]: vfs_default: Use correct flag in vfswrap_fs_file_id.
* * [https://a.org/show_bug.cgi/ BUG #14090]: vfs_glusterfs: Initialize st_ex_file_id, st_ex_itime and st_ex_iflags.
*  Anoop C S <anoopcs@redhat.com>
* * [https://a.org/show_bug.cgi/ BUG #14093]: vfs_glusterfs: Enable profiling for file system operations.
*  Aaron Haslett <aaronhaslett@catalyst.net.nz>
* * [https://a.org/show_bug.cgi/ BUG #14059]: Backport sambadowngradedatabase for v4.11.
*  Stefan Metzmacher <metze@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14035]: CVE-2019-10197: Permissions check deny can allow user to escape from the share.
*  Christof Schmitt <cs@samba.org>
* * [https://a.org/show_bug.cgi/ BUG #14032]: vfs_gpfs: Implement special case for denying owner access to ACL.
*  Martin Schwenke <martin@meltin.net>
* * [https://a.org/show_bug.cgi/ BUG #14084]: Avoid marking a node as connected before it can receive packets.
* * [https://a.org/show_bug.cgi/ BUG #14086]: Fix onnode test failure with ShellCheck >= 0.4.7.
* * [https://a.org/show_bug.cgi/ BUG #14087]: ctdb-daemon: Stop "ctdb stop" from completing before freezing databases.

===============================
KNOWN ISSUES
===============================

------------------------

* `Release_Planning_for_Samba_4.11#Release_blocking_bugs Release blocking bugs`

 https://a.org/pub/samba/rc/samb/DDO/T0rc3/SN/t

----
`Category:Release Notes`