Samba 4.5 Features added/changed
    <namespace>0</namespace>
<last_edited>2019-09-17T21:57:25Z</last_edited>
<last_editor>Fraz</last_editor>


Samba 4.5 is `Samba_Release_Planning#Discontinued_.28End_of_Life.29|**Discontinued (End of Life)**`.

Samba 4.5.16
------------------------

* Release Notes for Samba 4.5.16
* March 13, 2018

===============================
This is a security release in order to address the following defects:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1050 CVE-2018-1050] (Denial of Service Attack on external print server.)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1057 CVE-2018-1057] (Authenticated users can change other users' password.)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1050 CVE-2018-1050]:
* All versions of Samba from 4.0.0 onwards are vulnerable to a denial of service attack when the RPC spoolss service is configured to be run as an external daemon. Missing input sanitization checks on some of the input parameters to spoolss RPC calls could cause the print spooler service to crash.

* There is no known vulnerability associated with this error, merely a denial of service. If the RPC spoolss service is left by default as an internal service, all a client can do is crash its own authenticated connection.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1057 CVE-2018-1057]:
* On a Samba 4 AD DC the LDAP server in all versions of Samba from 4.0.0 onwards incorrectly validates permissions to modify passwords over LDAP allowing authenticated users to change any other users' passwords, including administrative users.

Possible workarounds are described at a dedicated page in the Samba wiki:
* `CVE-2018-1057`

===============================
Changes since 4.5.15:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11343 BUG #11343]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1050 CVE-2018-1050]: Codenomicon crashes in spoolss server code.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13272 BUG #13272]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1057 CVE-2018-1057]: Unprivileged user can change any user (and admin) password.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13272 BUG #13272]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1057 CVE-2018-1057]: Unprivileged user can change any user (and admin) password.

 https://www.samba.org/samba/history/samba-4.5.16.html

Samba 4.5.15
------------------------

* Release Notes for Samba 4.5.15
* November 21, 2017

===============================
This is a security release
===============================

------------------------

* in order to address the following defects:
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-14746 CVE-2017-14746] Use-after-free vulnerability.
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-15275 CVE-2017-15275] Server heap memory information leak.

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-14746 CVE-2017-14746]:
* All versions of Samba from 4.0.0 onwards are vulnerable to a use after free vulnerability, where a malicious SMB1 request can be used to control the contents of heap memory via a deallocated heap pointer. It is possible this may be used to compromise the SMB server.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-15275 CVE-2017-15275]:
* All versions of Samba from 3.6.0 onwards are vulnerable to a heap memory information leak, where server allocated heap memory may be returned to the client without being cleared.

* There is no known vulnerability associated with this error, but uncleared heap memory may contain previously used data that may help an attacker compromise the server via other methods. Uncleared heap memory may potentially contain password hashes or other high-value data.

For more details and workarounds, please see the security advisories:

* https://www.samba.org/samba/security/CVE-2017-14746.html
* https://www.samba.org/samba/security/CVE-2017-15275.html

===============================
Changes since 4.5.14
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=13041 BUG #13041]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-14746 CVE-2017-14746]: s3: smbd: Fix SMB1 use-after-free crash bug.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13077 BUG #13077]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-15275 CVE-2017-15275]: s3: smbd: Chain code can return uninitialized memory when talloc buffer is grown.

 https://www.samba.org/samba/history/samba-4.5.15.html

Samba 4.5.14
------------------------

* Release Notes for Samba 4.5.14
* September 20, 2017

===============================
This is a security release in order to address the following defects:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12150 CVE-2017-12150] SMB1/2/3 connections may not require signing where they should
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12151 CVE-2017-12151] SMB3 connections don't keep encryption across DFS redirects
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12163 CVE-2017-12163] Server memory information leak over SMB1

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12150 CVE-2017-12150]:
* A man in the middle attack may hijack client connections.
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12151 CVE-2017-12151]:
* A man in the middle attack can read and may alter confidential documents transferred via a client connection, which are reached via DFS redirect when the original connection used SMB3.
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12163 CVE-2017-12163]:
* Client with write access to a share can cause server memory contents to be written into a file or printer.

For more details and workarounds, please see the security advisories:

* https://www.samba.org/samba/security/CVE-2017-12150.html
* https://www.samba.org/samba/security/CVE-2017-12151.html
* https://www.samba.org/samba/security/CVE-2017-12163.html

===============================
Changes since 4.5.13:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12836 BUG #12836]: s3: smbd: Fix a read after free if a chained SMB1 call goes async.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13020 BUG #13020]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12163 CVE-2017-12163]: s3:smbd: Prevent client short SMB1 write from writing server memory to file.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12885 BUG #12885]: s3/smbd: Let non_widelink_open() chdir() to directories directly.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12996 BUG #12996]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12151 CVE-2017-12151]: Keep required encryption across SMB3 dfs redirects.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12997 BUG #12997]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12150 CVE-2017-12150]: Some code path don't enforce smb signing when they should.

 https://www.samba.org/samba/history/samba-4.5.14.html

Samba 4.5.13
------------------------

* Release Notes for Samba 4.5.13
* August 31, 2017

===============================
This is the latest stable release of the Samba 4.5 release series.
===============================

------------------------

===============================
Changes since 4.5.12:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12836 BUG #12836]: s3: smbd: Fix a read after free if a chained SMB1 call goes async.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12899 BUG #12899]: 'smbclient setmode' no longer works to clear attribute bits due to dialect upgrade.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12913 BUG #12913]: SMBC_setatr() initially uses an SMB1 call before falling back.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12791 BUG #12791]: Fix kernel oplock issues with named streams.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12897 BUG #12897]: vfs_fruit: Don't use MS NFS ACEs with Windows clients.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12910 BUG #12910]: s3/notifyd: Ensure notifyd doesn't return from smbd_notifyd_init.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12944 BUG #12944]: vfs_gpfs: handle EACCES when fetching DOS attributes from xattr.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12885 BUG #12885]: Let non_widelink_open() chdir() to directories directly.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12840 BUG #12840]: vfs_fruit: Add fruit:model = <modelname> parametric option.
*  David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12911 BUG #12911]: vfs_ceph: fix cephwrap_chdir().
*  Thomas Jarosch <thomas.jarosch@intra2net.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12927 BUG #12927]: s3: libsmb: Fix use-after-free when accessing pointer *p.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12782 BUG #12782]: winbindd changes the local password and gets NT_STATUS_WRONG_PASSWORD for the remote change.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12890 BUG #12890]: s3:smbd: consistently use talloc_tos() memory for rpc_pipe_open_interface().

 https://www.samba.org/samba/history/samba-4.5.13.html

Samba 4.5.12
------------------------

* Release Notes for Samba 4.5.12
* July 12, 2017

===============================
This is a security release in order to address the following defect:
===============================

------------------------

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-11103 CVE-2017-11103] (Orpheus' Lyre mutual authentication validation bypass)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-11103 CVE-2017-11103] (Heimdal):
* All versions of Samba from 4.0.0 onwards using embedded Heimdal Kerberos are vulnerable to a man-in-the-middle attack impersonating a trusted server, who may gain elevated access to the domain by returning malicious replication or authorization data.

* Samba binaries built against MIT Kerberos are not vulnerable.

===============================
Changes since 4.5.11
===============================

------------------------

*  Jeffrey Altman <jaltman@secure-endpoints.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12894 BUG #12894]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-11103 CVE-2017-11103]: Orpheus' Lyre KDC-REP service name validation

 https://www.samba.org/samba/history/samba-4.5.12.html

Samba 4.5.11
------------------------

* Release Notes for Samba 4.5.11
* July 6, 2017

===============================
This is the latest stable release of the Samba 4.5 release series.
===============================

------------------------

===============================
Changes since 4.5.10:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12793 BUG #12793]: s3: smbd: Fix open_files.idl to correctly ignore share_mode_lease *lease in share_mode_entry.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12804 BUG #12804]: s3: VFS: Catia: Ensure path name is also converted.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12818 BUG #12818]: s3: smbd: When deleting an fsp pointer ensure we don't keep any references to it around.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12831 BUG #12831]: s3: libsmb: Correctly save and restore connection tcon in 'smbclient', 'smbcacls' and 'smbtorture3'.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12798 BUG #12798]: s3/smbd: Fix exclusive lease optimisation.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12856 BUG #12856]: ctdb-scripts: Don't send empty argument string to logger.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12857 BUG #12857]: ctdb-recovery: Do not run local ip verification when in recovery.
*  Daniel Kobras <d.kobras@science-computing.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12860 BUG #12860]: s3: smbd: Fix regression with non-wide symlinks to directories over SMB3.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12768 BUG #12768]: samba-tool: Fix log message of 'samba-tool user syncpasswords'.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12772 BUG #12772]: s3:smbd: unimplement FSCTL_VALIDATE_NEGOTIATE_INFO with "server max protocol = SMB2_02".
* * [https://bugzilla.samba.org/show_bug.cgi?id=12788 BUG #12788]: auth/spnego: Fix gensec_update_ev() argument order for  the SPNEGO_FALLBACK case.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12832 BUG #12832]: s3:smb2_create: Avoid reusing the 'tevent_req' within smbd_smb2_create_send().
* * [https://bugzilla.samba.org/show_bug.cgi?id=12844 BUG #12844]: Related requests with TreeConnect fail with NETWORK_NAME_DELETED.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12845 BUG #12845]: Related requests with SessionSetup fail with INTERNAL_ERROR.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12859 BUG #12859]: ldb: protect Samba < 4.7 against incompatible ldb versions and require ldb < 1.2.0.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12862 BUG #12862]: auth/ntlmssp: Enforce NTLM _NEGOTIATE_NTLM2 for the NTLMv2 client case.
*  Michael Saxl <mike@mwsys.mine.bz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10490 BUG #10490]: s3:gse_krb5: Fix a possible crash in fill_mem_keytab_from_system_keytab().
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12808 BUG #12808]: libcli:smb2: Gracefully handle not supported for FSCTL_VALIDATE_NEGOTIATE_INFO.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12802 BUG #12802]: 'ctdb nodestatus' incorrectly displays status for all nodes with wrong exit code.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12837 BUG #12837]: ctdb-scripts: NFS call-out failures should cause event failure.
*  Richard Sharpe <richard.sharpe@primarydata.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=15852 BUG #15852]: There are valid paths where conn->lsa_pipe_tcp->transport is NULL.

 https://www.samba.org/samba/history/samba-4.5.11.html

Samba 4.5.10
------------------------

* Release Notes for Samba 4.5.10
* May 24, 2017

===============================
This is a security release in order to address the following defect:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CCVE-2017-7494 CVE-2017-7494] (Remote code execution from a writable share)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CCVE-2017-7494 CVE-2017-7494]:
* All versions of Samba from 3.5.0 onwards are vulnerable to a remote code execution vulnerability, allowing a malicious client to upload a shared library to a writable share, and then cause the server to load and execute it.

===============================
Mitigation from Redhat
===============================

------------------------

* https://access.redhat.com/security/cve/CVE-2017-7494

Any of the following:

# SELinux is enabled by default and our default policy prevents loading of modules from outside of samba's module directories and therefore blocks the exploit
# Mount the filessytem which is used by samba for its writeable share, using "noexec" option.
# Add the parameter:
* ::nt pipe support = no
* :to the [global] section of your smb.conf and restart smbd. This prevents clients from accessing any named pipe endpoints. Note this can disable some expected functionality for Windows clients.

===============================
Changes since 4.5.9:
===============================

------------------------

*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12780 BUG #12780]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CCVE-2017-7494 CVE-2017-7494]: Avoid remote code execution from a writable share.

 https://www.samba.org/samba/history/samba-4.5.10.html

Samba 4.5.9
------------------------

* Release Notes for Samba 4.5.9
* May 18, 2017

===============================
This is the latest stable release of the Samba 4.5 release series.
===============================

------------------------

===============================
Changes since 4.5.8:
===============================

------------------------

*  Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12743 BUG #12743]: vfs_shadow_copy2 fails to list snapshots from shares with GlusterFS backend.
*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12747 BUG #12747]: Wrong use of getgroups causes buffer overflow.
*  Hanno Böck <hanno@hboeck.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12746 BUG #12746]: lib: debug: Avoid negative array access.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12748 BUG #12748]: cleanupdb: Fix a memory read error.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11961 BUG #11961]: idmap_autorid allocates ids for unknown SIDs from other backends.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12562 BUG #12562]: vfs_acl_common should force "create mask = 0777".
* * [https://bugzilla.samba.org/show_bug.cgi?id=12565 BUG #12565]: vfs_fruit: resource fork open request with flags=O_CREAT|O_RDONLY.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12727 BUG #12727]: Lookup-domain for well-known SIDs on a DC.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12728 BUG #12728]: winbindd: Fix error handling in rpc_lookup_sids().
* * [https://bugzilla.samba.org/show_bug.cgi?id=12729 BUG #12729]: winbindd: Trigger possible passdb_dsdb initialisation.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12749 BUG #12749]: Can't case-rename files with vfs_fruit.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12766 BUG #12766]: s3/smbd: Update exclusive oplock optimisation to the lease area.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12733 BUG #12733]: ctdb-docs: Fix documentation of "-n" option to 'ctdb tool'.
*  Shilpa Krishnareddy <skrishnareddy@panzura.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12756 BUG #12756]: notify: Fix ordering of events in notifyd.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12276 BUG #12276]: lib: Fix CID 1373623 Dereference after null check.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12558 BUG #12558]: smbd: Fix smb1 findfirst with DFS.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12757 BUG #12757]: idmap_rfc2307: Fix lookup of more than two SIDs.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12767 BUG #12767]: samba-tool: Let 'samba-tool user syncpasswords' report deletions immediately.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12725 BUG #12725]: pam_winbind: no longer use wbcUserPasswordPolicyInfo when authenticating.
*  Doug Nazar <nazard@nazar.ca>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12760 BUG #12760]: s3: smbd: inotify_map_mask_to_filter incorrectly indexes an array.
*  Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12725 BUG #12725]: winbindd: Fix password policy for pam authentication.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12277 BUG #12277]: waf: Explicitly link libreplace against libnss_wins.so.
*  Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12737 BUG #12737]: vfs_acl_xattr - failure to get ACL on Linux if memory is fragmented.

 https://www.samba.org/samba/history/samba-4.5.9.html

Samba 4.5.8
------------------------

* Release Notes for Samba 4.5.8
* March 31, 2017

This is a bug fix release to address a regression introduced by the security fixes for CVE-2017-2619 (Symlink race allows access outside share definition).

Please see [https://bugzilla.samba.org/show_bug.cgi?id=12721 BUG #12721] for details.

===============================
Changes since 4.5.7:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12721 BUG #12721]: Fix regression with "follow symlinks = no".

 https://www.samba.org/samba/history/samba-4.5.8.html

Samba 4.5.7
------------------------

* Release Notes for Samba 4.5.7
* March 23, 2017

===============================
This is a security release in order to address the following defect: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-2619 CVE-2017-2619]
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-2619 CVE-2017-2619] (Symlink race allows access outside share definition)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-2619 CVE-2017-2619]:
* All versions of Samba prior to 4.6.1, 4.5.7, 4.4.11 are vulnerable to a malicious client using a symlink race to allow access to areas of the server file system not exported under the share definition.

* Samba uses the realpath() system call to ensure when a client requests access to a pathname that it is under the exported share path on the server file system.

* Clients that have write access to the exported part of the file system via SMB1 unix extensions or NFS to create symlinks can race the server by renaming a realpath() checked path and then creating a symlink. If the client wins the race it can cause the server to access the new symlink target after the exported share path check has been done. This new symlink target can point to anywhere on the server file system.

* This is a difficult race to win, but theoretically possible. Note that the proof of concept code supplied wins the race reliably only when the server is slowed down using the strace utility running on the server. Exploitation of this bug has not been seen in the wild.

===============================
Changes since 4.5.6:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12496 BUG #12496]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-2619 CVE-2017-2619]: Symlink race permits opening files outside share directory.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12496 BUG #12496]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-2619 CVE-2017-2619]: Symlink race permits opening files outside share directory.

 https://www.samba.org/samba/history/samba-4.5.7.html

Samba 4.5.6
------------------------

* Release Notes for Samba 4.5.6
* March 9, 2017

===============================
This is the latest stable release of the Samba 4.5 release series.
===============================

------------------------

===============================
Changes since 4.5.5:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12499 BUG #12499]: s3: vfs: dirsort doesn't handle opendir of "." correctly.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12531 BUG #12531]: vfs_shadow_copy2 doesn't cope with server changing directories.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12546 BUG #12546]: vfs_streams_xattr doesn't cope with server changing directories.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12572 BUG #12572]: s3: smbd: Don't loop infinitely on bad-symlink resolution.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12608 BUG #12608]: s3: smbd: Restart reading the incoming SMB2 fd when the send queue is drained.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12573 BUG #12573]: Samba < 4.7 does not know about compatibleFeatures and requiredFeatures.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12184 BUG #12184]: s3/rpc_server: Shared rpc modules loading.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12427 BUG #12427]: vfs_fruit doesn't work with fruit:metadata=stream.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12520 BUG #12520]: Ensure global "smb encrypt = off" is effective.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12524 BUG #12524]: s3/rpc_server: move rpc_modules.c to its own subsystem.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12536 BUG #12536]: s3/smbd: check for invalid access_mask smbd_calculate_access_mask().
* * [https://bugzilla.samba.org/show_bug.cgi?id=12541 BUG #12541]: vfs_fruit: checks wrong AAPL config state and so always uses readdirattr.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12545 BUG #12545]: s3/rpc_server/mdssvc: add attribute "kMDItemContentType".
* * [https://bugzilla.samba.org/show_bug.cgi?id=12591 BUG #12591]: vfs_streams_xattr: use fsp, not base_fsp.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12604 BUG #12604]: vfs_fruit: Enabling AAPL extensions must be a global switch.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12469 BUG #12469]: ctdb-tests: Use replace headers instead of system headers.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12547 BUG #12547]: ctdb-build: Install CTDB tests correctly from toplevel.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12580 BUG #12580]: ctdb-common: Fix use-after-free error in comm_fd_handler().
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12551 BUG #12551]: smbd: Fix "map acl inherit" = yes.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9048 BUG #9048]: s4:ldap_server: Match Windows in the error messages of failing LDAP Bind requests.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11830 BUG #11830]: s3:winbindd: Try a NETLOGON connection with noauth over NCACN_NP against trusted domains.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12262 BUG #12262]: 'net ads testjoin' and smb access fails after winbindd changed the trust password.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12399 BUG #12399]: s4:repl_meta_data: Normalize rdn attribute name via the schema.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12540 BUG #12540]: s3:smbd: Allow "server min protocol = SMB3_00" to go via "SMB 2.???" negprot.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12581 BUG #12581]: smbclient fails on bad endianess when listing shares from Solaris kernel SMB server on SPARC.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12585 BUG #12585]: librpc/rpc: fix regression in NT_STATUS_RPC_ENUM_VALUE_OUT_OF_RANGE error mapping.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12586 BUG #12586]: libcli/auth: Use the correct creds value against servers without LogonSamLogonEx.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12587 BUG #12587]: winbindd child segfaults on connect to an NT4 domain.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12588 BUG #12588]: cm_prepare_connection may return NT_STATUS_OK without a valid connection.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12598 BUG #12598]: winbindd (as member) requires Kerberos against trusted ad domain, while it shouldn't.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12605 BUG #12605]: s3:winbindd: Fix endless forest trust scan.
*  Garming Sam <garming@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12577 BUG #12577]: dbcheck-links: Test that dbcheck against one-way links does not error.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12600 BUG #12600]: dbchecker: Stop ignoring linked cases where both objects are alive.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12571 BUG #12571]: s3-vfs: Only walk the directory once in open_and_sort_dir().
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12589 BUG #12589]: ctdb-scripts: Initialise CTDB_NFS_CALLOUT in statd-callout.
*  Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12529 BUG #12529]: waf: backport finding of pkg-config.

 https://www.samba.org/samba/history/samba-4.5.6.html

Samba 4.5.5
------------------------

* Release Notes for Samba 4.5.5
* January 30, 2017

===============================
This is the latest stable release of the Samba 4.5 release series.
===============================

------------------------

Samba 4.5.4 erroneously included a rewrite of the vfs_fruit module. This patchset will be reverted with this release, because it needs to pass the review process first. If you are using the vfs_fruit module, please do not use Samba 4.5.4.

===============================
Changes since 4.5.4:
===============================

------------------------

*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12469 BUG #12469]: ctdb-locking: Explicitly unlock record/db in lock helper.
*  Björn Jacke <bj@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12535 BUG #12535]: vfs_default: Unlock the right file in copy chunk.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12512 BUG #12512]: ctdb-scripts: Fix remaining uses of "ctdb gratiousarp".
* * [https://bugzilla.samba.org/show_bug.cgi?id=12516 BUG #12516]: /etc/iproute2/rt_tables gets populated with multiple 'default' entries.

 https://www.samba.org/samba/history/samba-4.5.5.html

Samba 4.5.4== 
* Release Notes for Samba 4.5.4
* January 18, 2017

===============================
This is the latest stable release of the Samba 4.5 release series.
===============================

------------------------

===============================
Changes since 4.5.3:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12460 BUG #12460]: rename_internals_fsp missing ACL permission-check on destination  folder.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12466 BUG #12466]: lib: security: se_access_check() incorrectly processes owner rights (S-1-3-4) DENY ace entries.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12467 BUG #12467]: s3: ntlm_auth: Don't corrupt the output stream with debug messages.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12479 BUG #12479]: s3: libsmb: Add cli_smb2_ftruncate(), plumb into cli_ftruncate().
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12396 BUG #12396]: s3/smbd: Remove a misleading error message.
* *[https://bugzilla.samba.org/show_bug.cgi?id=12412 BUG #12412]: vfs_fruit: Fix "fruit:resource" option spelling, but not behaviour.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12485 BUG #12485]: ctdbd_conn: Fix a resource leak.
*  David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12144 BUG #12144]: smbd/ioctl: match WS2016 ReFS set compression behaviour.
*  Björn Jacke <bj@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=2210 BUG #2210]: pam: Map more NT password errors to PAM errors.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12484 BUG #12484]: winbindd: Use idmap cache in xids2sids.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12509 BUG #12509]: messaging: Fix dead but not cleaned-up-yet destination sockets.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12480 BUG #12480]: kinit succeeded but ads_sasl_spnego_gensec_bind(KRB5) failed: An internal error occurred (with MIT krb5).
*  Andreas Schneider <asn@samba.org>
* *[https://bugzilla.samba.org/show_bug.cgi?id=12183 BUG #12183]: printing: Fix building with CUPS version older than 1.7.
* *[https://bugzilla.samba.org/show_bug.cgi?id=12441 BUG #12441]: s3:libads: Include system /etc/krb5.conf if we use MIT Kerberos.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12470 BUG #12470]: Fix ctdb ip bugs.

 https://www.samba.org/samba/history/samba-4.5.4.html

Samba 4.5.3
------------------------

* Release Notes for Samba 4.5.3
* December 19, 2016

===============================
This is a security release in order to address the following defects:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2123 CVE-2016-2123] (Samba NDR Parsing ndr_pull_dnsp_name Heap-based Buffer Overflow Remote Code Execution Vulnerability).
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2125 CVE-2016-2125] (Unconditional privilege delegation to Kerberos servers in trusted realms).
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2126 CVE-2016-2126] (Flaws in Kerberos PAC validation can trigger privilege elevation).

Details=
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2123 CVE-2016-2123]:
* The Samba routine ndr_pull_dnsp_name contains an integer wrap problem, leading to an attacker-controlled memory overwrite. ndr_pull_dnsp_name parses data from the Samba Active Directory ldb database.  Any user who can write to the dnsRecord attribute over LDAP can trigger this memory corruption.

* By default, all authenticated LDAP users can write to the dnsRecord attribute on new DNS objects. This makes the defect a remote privilege escalation.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2125 CVE-2016-2125]
* Samba client code always requests a forwardable ticket when using Kerberos authentication. This means the target server, which must be in the current or trusted domain/realm, is given a valid general purpose Kerberos "Ticket Granting Ticket" (TGT), which can be used to fully impersonate the authenticated user or service.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2126 CVE-2016-2126]
* A remote, authenticated, attacker can cause the winbindd process to crash using a legitimate Kerberos ticket due to incorrect handling of the arcfour-hmac-md5 PAC checksum.

* A local service with access to the winbindd privileged pipe can cause winbindd to cache elevated access permissions.

===============================
Changes since 4.5.2:
===============================

------------------------

*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12409 BUG #12409]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2123 CVE-2016-2123]: Fix DNS vuln ZDI-CAN-3995.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12445 BUG #12445]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2125 CVE-2016-2125]: Don't send delegated credentials to all servers.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12446 BUG #12446]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2126 CVE-2016-2126]: auth/kerberos: Only allow known checksum types in check_pac_checksum().

 https://www.samba.org/samba/history/samba-4.5.3.html

Samba 4.5.2
------------------------

* Release Notes for Samba 4.5.2
* December 07, 2016

===============================
This is the latest stable release of the Samba 4.5 release series.
===============================

------------------------

===============================
Changes since 4.5.1:
===============================

------------------------

*  Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12404 BUG #12404]: vfs:glusterfs: Preallocate result for glfs_realpath.
*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12384 BUG #12384]: s3: vfs: Remove files/directories after the streams are deleted.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12387 BUG #12387]: s3: vfs_streams_depot: Use conn->connectpath not conn->cwd.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12436 BUG #12436]: s3/smbd: Fix the last resort check that sets the file type attribute.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9954 BUG #9954]: dsdb: Create RID Set as SYSTEM.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12297 BUG #12297]: dbcheck: Correct message for orphaned backlinks.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12395 BUG #12395]: build: Fix build with perl on debian sid.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12398 BUG #12398]: Fix errors in extended operations (like allocating a RID Set).
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11197 BUG #11197]: spoolss: Use correct values for secdesc and devmode pointers.
*  Clive Ferreira <cliveferreira@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12394 BUG #12394]: objectclass_attrs: Only abort on a missing attribute when an attribute is both MUST and replicated.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12366 BUG #12366]: provision,dlz-bind: Add support for BIND 9.11.x.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12392 BUG #12392]: ctdb-locking: Reset real-time priority in lock helper.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12407 BUG #12407]: ctdb-scripts: Fix calculation of CTDB_BASE.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12434 BUG #12434]: ctdb-recovery: Avoid NULL dereference in failure case.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10297 BUG #10297]: s3:smbd: Only pass UCF_PREP_CREATEFILE to filename_convert() if we may create a new file.
*  Mathieu Parent <math.parent@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12371 BUG #12371]: ctdb-scripts: Fix Debian init in samba eventscript.
*  Garming Sam <garming@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9954 BUG #9954]: samba_tool/fsmo: Allocate RID Set when seizing RID manager.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10882 BUG #10882]: s4-auth: Don't check for NULL saltPrincipal if it doesn't need it.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12297 BUG #12297]: upgradeprovision: Remove objectCategory from constructed attrs.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12385 BUG #12385]: collect_tombstones: Allow links to recycled objects to be deleted.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12183 BUG #12183]: s3-printing: Correctly encode CUPS printer URIs.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12195 BUG #12195]: s3-printing: Allow printer names longer than 16 chars.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12269 BUG #12269]: nss_wins: Fix errno values for HOST_NOT_FOUND.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12405 BUG #12405]: s3-winbind: Do not return NO_MEMORY if we have an empty user list.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12415 BUG #12415]: s3:spoolss: Add support for COPY_FROM_DIRECTORY in AddPrinterDriverEx.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12104 BUG #12104]: ctdb-packaging: Move CTDB tests to /usr/local/share/ctdb/tests/.
*  Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12375 BUG #12375]: smbd: In ntlm auth, do not map empty domain in case of \user@realm.
*  Ralph Wuerthner <ralph.wuerthner@de.ibm.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12372 BUG #12372]: ctdb-conn: Add missing variable initialization.

 https://www.samba.org/samba/history/samba-4.5.2.html

Samba 4.5.1
------------------------

* Release Notes for Samba 4.5.1
* October 26, 2016

===============================
This is the latest stable release of the Samba 4.5 release series.
===============================

------------------------

Major enhancements in Samba 4.5.1 include:

*  Let winbindd discard expired kerberos tickets when built against (internal) heimdal ([https://bugzilla.samba.org/show_bug.cgi?id=12115 BUG #12115]BUG #12369).
*  REGRESSION: smbd segfaults on startup, tevent context being freed ([https://bugzilla.samba.org/show_bug.cgi?id=12115 BUG #12115]BUG #12283).

===============================
Changes since 4.5.0:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11259 BUG #11259]: smbd contacts a domain controller for each session.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12272 BUG #12272]: Fix messaging subsystem crash.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12283 BUG #12283]: REGRESSION: smbd segfaults on startup, tevent context being freed.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12381 BUG #12381]: s3: cldap: cldap_multi_netlogon_send() fails with one bad IPv6 address.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12383 BUG #12383]: s3: libsmb: Fix cut and paste error using the wrong structure type.
*  Christian Ambach <ambi@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9945 BUG #9945]: Setting specific logger levels in smb.conf makes 'samba-tool drs showrepl' crash.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12382 BUG #12382]: Tombstone expunge does not remove old links.
*  Björn Baumbach <bb@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=8618 BUG #8618]: s3-printing: Fix migrate printer code.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12256 BUG #12256]: s3/smbd: In call_trans2qfilepathinfo call lstat when dealing with posix pathnames.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12261 BUG #12261]: s3/smbd: Set FILE_ATTRIBUTE_DIRECTORY as necessary.
* Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12285 BUG #12285]: "DriverVersion" registry backend parsing incorrect in spoolss.
*  David Disseldorp <ddiss@samba.org>
* * BUG 12144: smbd/ioctl: Match WS2016 ReFS get compression behaviour.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12259 BUG #12259]: ctdb-protocol: Fix marshalling for GET_DB_SEQNUM control request.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12275 BUG #12275]: ctdb-recovery-helper: Add missing initialisation of ban_credits.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12287 BUG #12287]: CTDB PID file handling is too weak.
* Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12045 BUG #12045]: gencache: Bail out of stabilize if we can not get the allrecord lock.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12268 BUG #12268]: smbd: Reset O_NONBLOCK on open files.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12283 BUG #12283]: glusterfs: Avoid tevent_internal.h.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12291 BUG #12291]: source3/lib/msghdr.c, line 208: syntax error before or at: ;.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12374 BUG #12374]: spoolss: Fix caching of printername->sharename.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12283 BUG #12283]: REGRESSION: smbd segfaults on startup, tevent context being freed.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12369 BUG #12369]: Let winbindd discard expired kerberos tickets when built against (internal) heimdal.
*  Noel Power <noel.power@suse.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12298 BUG #12298]: s3/winbindd: Fix using default domain with user@domain.com format.
*  Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12295 BUG #12295]: winbind: Fix passing idmap failure from wb_sids2xids back to callers.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12269 BUG #12269]: nss_wins has incorrect function definitions for gethostbyname*.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12276 BUG #12276]: s3-lib: Fix %G substitution in AD member environment.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12364 BUG #12364]: s3-utils: Fix loading smb.conf in smbcquotas.
*  Garming Sam <garming@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12286 BUG #12286]: kcc: Don't check schedule if None.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12382 BUG #12382]: Tombstone expunge does not remove old links.
*  Anoop C S <anoopcs@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12377 BUG #12377]: vfs_glusterfs: Fix a memory leak in connect path.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12254 BUG #12254]: CTDB IP takeover does not complete if there are no public addresses configured.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12255 BUG #12255]: ctdb-packaging: Fix systemd network dependency.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12287 BUG #12287]: CTDB PID file handling is too weak.
*  Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12270 BUG #12270]: smbcquotas: Fix error message listing quotas.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12273 BUG #12273]: s3-sysquotas: Correctly restore path when finding mount point.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12288 BUG #12115]: cliquota: Fix param count when setting fs quota.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12289 BUG #12289]: smbd: Free talloc context if no quota records are available.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12307 BUG #12307]: ntquotas: Support "freeing" an empty quota list.

 https://www.samba.org/samba/history/samba-4.5.1.html

Samba 4.5.0
------------------------

<onlyinclude>
* Release Notes for Samba 4.5.0
* September 7, 2016
===============================
Release Announcements
===============================

------------------------

This is the first stable release of the Samba 4.5 release series.

===============================
UPGRADING
===============================

------------------------

NTLMv1 authentication disabled by default=
===============================

------------------------

In order to improve security we have changed the default value for the "ntlm auth" option from "yes" to "no". This may have impact on very old clients which doesn't support NTLMv2 yet.

The primary user of NTLMv1 is MSCHAPv2 for VPNs and 802.1x.

By default, Samba will only allow NTLMv2 via NTLM  now, as we have the following default "lanman auth = no", "ntlm auth = no" and "raw NTLMv2 auth = no".

===============================
NEW FEATURES/CHANGES
===============================

------------------------

Support for LDAP_SERVER_NOTIFICATION_OID=
===============================

------------------------

The ldap server has support for the LDAP_SERVER_NOTIFICATION_OID control. This can be used to monitor the Active Directory database for changes.

KCC improvements for sparse network replication=
===============================

------------------------

The Samba KCC will now be the default knowledge consistency checker in Samba AD. Instead of using full mesh replication between every DC, the KCC will set up connections to optimize replication latency and cost (using site links to calculate the routes). This change should allow larger domains to function significantly better in terms of replication traffic and the time spent performing DRS replication.

VLV - Virtual List View=
===============================

------------------------

The VLV Control allows applications to page the LDAP directory in the way you might expect a live phone book application to operate, without first downloading the entire directory.

DRS Replication for the AD DC=
===============================

------------------------

DRS Replication in Samba 4.5 is now much more efficient in handling linked attributes, particularly in large domains with over 1000 group memberships or other links.

Replication is also much more reliable in the handling of tree
renames, such as the rename of an organizational unit containing many
users.  Extensive tests have been added to ensure this code remains
reliable, particularly in the case of conflicts between objects added
with the same name on different servers.

Schema updates are also handled much more reliably.

samba-tool drs replicate with new options=
===============================

------------------------

'samba-tool drs replicate' got two new options:

*The option '--local-online' will do the DsReplicaSync() via IRPC to the local dreplsrv service.
*The option '--async-op' will add DRSUAPI_DRS_ASYNC_OP to the DsReplicaSync(), which won't wait for the replication result.

replPropertyMetaData Changes=
===============================

------------------------

During the development of the DRS replication, tests showed that Samba stores the replPropertyMetaData object incorrectly. To address this, be aware that 'dbcheck' will now detect and offer to fix all objects in the domain for this error.

For further information and instructions how to fix the problem, see https://wiki.samba.org/index.php/Updating_Samba#Fixing_replPropertyMetaData_Attributes

Linked attributes on deleted objects=
===============================

------------------------

In Active Directory, an object that has been tombstoned or recycled has no linked attributes.  However, Samba incorrectly maintained such links, slowing replication and run-time performance. 'dbcheck' now offers to remove such links, and they are no longer kept after the object is tombstoned or recycled.

Improved AD DC performance=
===============================

------------------------

Many other improvements have been made to our LDAP database layer in the AD DC, to improve performance, both during 'samba-tool domain provision' and at runtime.

Other dbcheck improvements=
===============================

------------------------

* 'samba-tool dbcheck' can now find and fix a missing or corrupted 'deleted objects' container.
* [https://bugzilla.samba.org/show_bug.cgi?id=11433 BUG #11433]: samba-dbcheck no longer offers to resort auxiliary class values in objectClass as these were then re-sorted at the next dbcheck indefinitely.

Tombstone Reanimation=
===============================

------------------------

Samba now supports tombstone reanimation, a feature in the AD DC allowing tombstones, that is objects which have been deleted, to be restored with the original SID and GUID still in place.

Multiple DNS Forwarders on the AD DC=
===============================

------------------------

Previously, the Samba internal DNS server supported only one DNS forwarder. The "dns forwarder" option has been enhanced and now supports a space-separated list of multiple DNS server IP addresses. As a result, Samba is now able to fall back to alternative DNS servers. In case that a DNS query to the first server timed out, it is sent to the next DNS server listed in the option.

Password quality plugin support in the AD DC=
===============================

------------------------

The check password script now operates correctly in the AD DC

pwdLastSet is now correctly honoured=
===============================

------------------------

[https://bugzilla.samba.org/show_bug.cgi?id=9654 BUG #9654]: The pwdLastSet attribute is now correctly handled (this previously permitted passwords that expire next).

net ads dns unregister=
===============================

------------------------

It is now possible to remove the DNS entries created with 'net ads register' with the matching 'net ads unregister' command.

samba-tool improvements=
===============================

------------------------

Running 'samba-tool' on the command line should now be a lot snappier. The tool now only loads the code specific to the subcommand that you wish to run.

SMB 2.1 Leases enabled by default==
===============================

------------------------

Leasing is an SMB 2.1 (and higher) feature which allows clients to aggressively cache files locally above and beyond the caching allowed by SMB 1 oplocks. This feature was disabled in previous releases, but the SMB2 leasing code is now considered mature and stable enough to be enabled by default.

Open File Description (OFD) Locks=
===============================

------------------------

On systems that support them (currently only Linux), the fileserver now uses Open File Description (OFD) locks instead of POSIX locks to implement client byte range locks. As these locks are associated with a specific file descriptor on a file this allows more efficient use when multiple descriptors having file locks are opened onto the same file. An internal tunable "smbd:force process locks = true" may be used to turn off OFD locks if there appear to be problems with them.

Password sync as Active Directory domain controller=
===============================

------------------------

The new commands 'samba-tool user getpassword' and 'samba-tool user syncpasswords' provide access and syncing of various password fields.

If compiled with GPGME support (--with-gpgme) it's possible to store cleartext passwords in a PGP/OpenGPG encrypted form by configuring the new "password hash gpg key ids" option. This requires gpgme devel and python packages to be installed (e.g. libgpgme11-dev and python-gpgme on Debian/Ubuntu).

Python crypto requirements=
===============================

------------------------

Some samba-tool subcommands require python-crypto and/or python-m2crypto packages to be installed.

SmartCard/PKINIT improvements=
===============================

------------------------

'samba-tool user create' accepts --smartcard-required and 'samba-tool user setpassword' accepts "--smartcard-required" and "--clear-smartcard-required".

Specifying "--smartcard-required" results in the UF_SMARTCARD_REQUIRED flags being set in the userAccountControl attribute.
At the same time, the account password is reset to a random NTHASH value.

Interactive password logons are rejected, if the UF_SMARTCARD_REQUIRED bit is set in the userAccountControl attribute of a user.

When doing a PKINIT based Kerberos logon the KDC adds the required PAC_CREDENTIAL_INFO element to the authorization data. That means the NTHASH is shared between the PKINIT based client and the domain controller, which allows the client to do NTLM based authentication on behalf of the user. It also allows an offline logon using a smartcard to work on Windows clients.

CTDB changes=
===============================

------------------------

* New improved 'ctdb tool'
* :'ctdb tool' has been completely rewritten using new client API.
* :Usage messages are much improved.
* Sample CTDB configuration file is installed as ctdbd.conf.
* The use of real-time scheduling when taking locks has been narrowed to limit potential performance impacts on nodes.
* CTDB_RECOVERY_LOCK now supports specification of an external helper to take and hold the recovery lock.
* See the RECOVERY LOCK section in ctdb(7) for details.  Documentation for writing helpers is provided in doc/cluster_mutex_helper.txt.
* "ctdb natgwlist" has been replaced by a top level "ctdb natgw" command that has "master", "list" and "status" subcommands.
* The 'onnode' command no longer supports the "recmaster", "lvs" and "natgw" node specifications.
* Faster resetting of TCP connections to public IP addresses during failover.
* Tunables MaxRedirectCount, ReclockPingPeriod, DeferredRebalanceOnNodeAdd are now obsolete/ignored.
* "ctdb listvars" now lists all variables, including the first one.
* "ctdb xpnn", "ctdb rebalanceip" and "ctdb rebalancenode" have been removed.
* These are not needed because "ctdb reloadips" should do the correct rebalancing.
* Output for the following commands has been simplified:
* :ctdb getdbseqnum
* :ctdb getdebug
* :ctdb getmonmode
* :ctdb getpid
* :ctdb getreclock
* :ctdb getpid
* :ctdb pnn
* These now simply print the requested output with no preamble.  This means that scripts no longer need to strip part of the output.
* "ctdb getreclock" now prints nothing when the recovery lock is not set.
* Output for the following commands has been improved:
* ctdb setdebug
* ctdb uptime
* 'ctdb process-exists' has been updated to only take a PID argument.
* The PNN can be specified with -n <PNN>.  Output also cleaned up.
* LVS support has been reworked - related commands and configuration variables have changed
* 'ctdb lvsmaster' and 'ctdb lvs' have been replaced by a top level
* 'ctdb lvs' command that has "master", "list" and "status" subcommands.
* See the LVS sections in ctdb(7) and ctdbd.conf(5) for details, including configuration changes.
* Improved sample NFS Ganesha call-out

New shadow_copy2 options=
===============================

------------------------

shadow:snapprefix
* With growing number of snapshots file-systems need some mechanism to differentiate one set of snapshots from other, e.g. monthly, weekly, manual, special events, etc. Therefore, these file-systems provide different ways to tag snapshots, e.g. provide a configurable way to name snapshots, which is not just based on time.  With only shadow:format it is very difficult to filter these snapshots. With this optional parameter, one can specify a variable prefix component for names of the snapshot directories in the file-system. If this parameter is set, together with the shadow:format and shadow:delimiter parameters it determines the possible names of snapshot directories in the file-system. The option only supports Basic Regular Expression (BRE).

shadow:delimiter
* This optional parameter is used as a delimiter between "shadow:snapprefix" and "shadow:format" This parameter is used only when "shadow:snapprefix" is set.
* :Default: shadow:delimiter = "_GMT"

===============================
REMOVED FEATURES
===============================

------------------------

"only user" and "username" parameters=
===============================

------------------------

These two parameters have long been deprecated and superseded by "valid users" and "invalid users".
</onlyinclude>

===============================
smb.conf changes
===============================

------------------------

    arameter Name                Description             Default
    -------------                -----------             -------
    ccsrv:samba_kcc              Changed default         yes
    tlm auth                     Changed default         no
    nly user                     Removed
    assword hash gpg key ids     New
    hadow:snapprefix             New
    hadow:delimiter              New                     _GMT
    mb2 leases                   Changed default         yes
    sername                      Removed

===============================
KNOWN ISSUES
===============================

------------------------

While a lot of schema replication bugs were fixed in this release [https://bugzilla.samba.org/show_bug.cgi?id=12204 BUG #12204] - Samba fails to replicate schema 69 is still open.
The replication fails if more than 133 schema objects are added at the same time.

More open bugs are listed at: `Release_Planning_for_Samba_4.5#All_bugs`

===============================
CHANGES SINCE 4.5.0rc3
===============================

------------------------

* Björn Baumbach <bb@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12194 BUG #12194]: idmap_script: fix missing "IDTOSID" argument in scripts command line.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12178 BUG #12178]: samba-tool dbcheck fails to fix replPropertyMetaData.
* Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12177 BUG #12177]: Unexpected synthesized default ACL from vfs_acl_xattr.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12181 BUG #12181]: vfs_acl_common not setting filesystem permissions anymore.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12184 BUG #12184]: Loading shared RPC modules failed.
* Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12245 BUG #12245]: fix _spoolss_GetPrinterDataEx by moving the keyname length check.
* Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11994 BUG #11994]: smbclient fails to connect to Azure or Apple share spnego fails with no mechListMIC.
* Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12180 BUG #12180]: CTDB crashes running eventscripts.

===============================
CHANGES SINCE 4.5.0rc2
===============================

------------------------

*  Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12155 BUG #12155]: Some idmap backends don't perform range checks for the result of sids_to_xids.
*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12115 BUG #12115]: Endless loop on drsuapi pull replication after schema changes.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12135 BUG #12135]: net ads gpo refresh can crash with null pointer deref..
* * [https://bugzilla.samba.org/show_bug.cgi?id=12139 BUG #12139]: Race between break oplock and check for share_mode.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12150 BUG #12150]: SMB2 snapshot query fails on DFS shares..
* * [https://bugzilla.samba.org/show_bug.cgi?id=12165 BUG #12165]: smbclient allinfo doesn't correctly return 'previous version' info over SMB1.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12166 BUG #12166]: smbclient allinfo doesn't correctly return 'previous version' info over SMB2.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12174 BUG #12174]: error: 'conn' undeclared.
* Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12143 BUG #12143]: misnamed attribute in samba_kcc causes exception in unusual circumstances.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12187 BUG #12187]: Backport changes for partial attribute set calculation for 4.5.
* Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12107 BUG #12107]: backport backupkey tests.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12115 BUG #12115]: Endless loop on drsuapi pull replication after schema changes.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12128 BUG #12128]: Correctly resolve replicated schema changes regarding linked attributes.

* Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12137 BUG #12137]: Fix printf format non-liternal warnings and printf format errors.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12138 BUG #12138]: Fix uninitialized timeout in ctdb_pmda.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12151 BUG #12151]: Drop resurrected ctdb commands in new ctdb tool.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12152 BUG #12152]: Fix ctdb addip; implementation to match ctdb delip.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12163 BUG #12163]: Fix missing arguments and format elements in format strings.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12168 BUG #12168]: Fix format-nonliteral warnings.
* Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12108 BUG #12108]: Backport selftest/autobuild fixes to v4-5-test.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12114 BUG #12114]: In memory schema updated on non schema master.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12115 BUG #12115]: Endless loop on drsuapi pull replication after schema changes.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12128 BUG #12128]: Correctly resolve replicated schema changes regarding linked attributes.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12129 BUG #12129]: let samba-tool ldapcmp ignore whenChanged.
* Garming Sam <garming@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12187 BUG #12187]: Backport changes for partial attribute set calculation for 4.5.
* Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12175 BUG #12175]: smbget always prompts for a username.
* Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12150 BUG #12150]: SMB2 snapshot query fails on DFS shares..
* Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12157 BUG #12157]: Coverity and related fixes.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12158 BUG #12158]: CTDB release IP fixes.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12161 BUG #12161]: Fix CTDB cumulative takeover timeout.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12170 BUG #12170]: CTDB test runs can kill each other's ctdbd daemons.
* Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12145 BUG #12145]: smbd: if inherit owner is enabled, the free disk on a folder should take the owner's quota into account.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12149 BUG #12149]: smbd: cannot load a Windows device driver from a Samba share via SMB2.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12172 BUG #12172]: a snapshot folder cannot be accessed via SMB1.

===============================
CHANGES SINCE 4.5.0rc1
===============================

------------------------

* Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12005 BUG #12005]: parse_share_modes() chokes on ctdb tombstone record from ltdb.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12105 BUG #12105]: smbclient connection to not reachable IP eats 100% CPU.
* Ira Cooper <ira@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12133 BUG #12133]: source3/wscript: Add support for disabling vfs_cephfs.
* Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12121 BUG #12121]: ctdb-tools: Fix numerous Coverity IDs and other issues.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12122 BUG #12122]: If a transaction fails, it should be canceled and transaction handle should be freed.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12134 BUG #12134]: dbwrap: Fix structure initialization.

* Marc Muehlfeld <mmuehlfeld@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12023 BUG #12023]: man: Fix wrong option for parameter "ldap ssl" in smb.conf man page.
* Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12104 BUG #12104]: ctdb-waf: Move ctdb tests to libexec directory.
* Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12104 BUG #12104]: ctdb-packaging: Move ctdb tests to libexec directory.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12109 BUG #12109]: Fixes several CTDB tests.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12110 BUG #12110]: Fix numerous Coverity IDs.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12113 BUG #12113]: ctdb-mutex: Avoid corner case where helper is already reparented to init.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12123 BUG #12123]: Fix ctdb tickle command and update documentation.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12125 BUG #12125]: CTDB overwrites working configuration due to packaging change.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12126 BUG #12126]: Fix broken CTDB log messages.

 https://download.samba.org/pub/samba/rc/samba-4.5.0rc1.WHATSNEW.txt

----
`Category:Release Notes`