Samba 4.0 Features added/changed
    <namespace>0</namespace>
<last_edited>2019-09-17T22:05:48Z</last_edited>
<last_editor>Fraz</last_editor>

Samba 4.0 is in the `Samba_Release_Planning#Discontinued_.28End_of_Life.29|**Discontinued (End of Life)**`.

Samba 4.0.26
------------------------

* Release Notes for Samba 4.0.26
* May 6, 2015

===============================
This is the last bug-fix release of the Samba 4.0 release series.
===============================

------------------------

There will be security releases only starting from now.

===============================
Changes since 4.0.25:
===============================

------------------------

*   Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10982 BUG #10982]: s3: smbd: Fix *allocate* calls to follow POSIX error return convention.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9629 BUG #9629]: s3: smbclient: Allinfo leaves the file handle open.
*   Christian Ambach <ambi@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9629 BUG #9629]: Fix 'profiles' tool.
*   Ira Cooper <ira@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11115 BUG #11115]: smbd: Stop using vfs_Chdir after SMB_VFS_DISCONNECT.
*   David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10808 BUG #10808]: printing/cups: Pack requested-attributes with IPP_TAG_KEYWORD.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11059 BUG #11059]: libsmb: Provide authinfo domain for encrypted session referrals.
*   Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11041 BUG #11041]: smbd: Fix CID 1063259 Uninitialized scalar variable.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11051 BUG #11051]: net: Fix 'net sam addgroupmem'.
*   Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9299 BUG #9299]: nsswitch: Fix soname of linux nss_*.so.2 modules.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9702 BUG #9702]: s3:smb2_server: Protect against integer wrap with "smb2 max credits = 65535".
* * [https://bugzilla.samba.org/show_bug.cgi?id=10958 BUG #10958]: s4:dsdb/rootdse: Expand extended dn values with the AS_SYSTEM control.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10958 BUG #10958]: libcli/smb: Nnly force signing of smb2 session setups when binding a new session.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11144 BUG #11144]: Fix memory leak in SMB2 notify handling.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11164 BUG #11164]: s4:auth/gensec_gssapi: Let gensec_gssapi_update() return NT_STATUS_LOGON_FAILURE for unknown errors.
*   Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11034 BUG #11034]: winbind: Retry after SESSION_EXPIRED error in ping-dc.

 https://www.samba.org/samba/history/samba-4.0.26.html

Samba 4.0.25
------------------------

* Release Notes for Samba 4.0.25
* February 23, 2015

===============================
This is a security release in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-0240 CVE-2015-0240].
===============================

------------------------

* Unexpected code execution in smbd

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-0240 CVE-2015-0240]:
* All versions of Samba from 3.5.0 to 4.2.0rc4 are vulnerable to an unexpected code execution vulnerability in the smbd file server daemon.

* A malicious client could send packets that may set up the stack in such a way that the freeing of memory in a subsequent anonymous netlogon packet could allow execution of arbitrary code. This code would execute with root privileges.

===============================
Changes since 4.0.24:
===============================

------------------------

*   Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11077 BUG #11077]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-0240 CVE-2015-0240]: talloc free on uninitialized stack pointer in netlogon server could lead to security vulnerability.
*   Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11077 BUG #11077]: CVE-2015-0240: s3-netlogon: Make sure we do not deference a NULL pointer.

 https://www.samba.org/samba/history/samba-4.0.25.html

Samba 4.0.24
------------------------

* Release Notes for Samba 4.0.24
* January 15, 2015

===============================
This is a security release in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-8143 CVE-2014-8143]
===============================

------------------------

This is a security release in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-8143 CVE-2014-8143] (Elevation of privilege to Active Directory Domain Controller).

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-8143 CVE-2014-8143]:
* Samba's AD DC allows the administrator to delegate creation of user or computer accounts to specific users or groups.

* However, all released versions of Samba's AD DC did not implement the additional required check on the UF_SERVER_TRUST_ACCOUNT bit in the userAccountControl attributes.

===============================
Changes since 4.0.23:
===============================

------------------------

*   Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10993 BUG #10993]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-8143 CVE-2014-8143]: dsdb-samldb: Check for extended access rights before we allow changes to userAccountControl.

 https://www.samba.org/samba/history/samba-4.0.24.html

Samba 4.0.23
------------------------

* Release Notes for Samba 4.0.23
* December 08, 2014

===============================
This is the latest stable release of Samba 4.0.
===============================

------------------------

===============================
Changes since 4.0.22:
===============================

------------------------

*Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10472 BUG #10472]: Revert buildtools/wafadmin/Tools/perl.py back to upstream state.
* Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10711 BUG #10711]: s3:daemons: Ensure nmbd and winbindd are consistent in command line processing by adding POPT_COMMON_DYNCONFIG.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10779 BUG #10779]: pthreadpool: Slightly serialize jobs.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10830 BUG #10830]: s3:nmbd: Ensure the main nmbd process doesn't create zombies.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10831 BUG #10831]: SIGCLD Signal handler not correctly reinstalled on old library code use - smbrun etc.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10848 BUG #10848]: s3:smb2cli: Query info return length check was reversed.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10896 BUG #10896]: s3:nmbd: Fix netbios name truncation.
* Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9984 BUG #9984]: s3-libnet: Make sure we do not overwrite precreated SPNs.
* David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10898 BUG #10898]: spoolss: Fix handling of bad EnumJobs levels.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10905 BUG #10905]: spoolss: Fix print job enumeration.
* Björn Jacke <bj@sernet.de>
* * BUG docs: Mention incompatibility between kernel oplocks and streams_xattr.
* Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10860 BUG #10860]: registry: Don't leave dangling transactions.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10932 BUG #10932]: pdb_tdb: Fix a TALLOC/SAFE_FREE mixup.
* Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10472 BUG #10472]: Revert buildtools/wafadmin/Tools/perl.py back to upstream state.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10921 BUG #10921]: s3:smbd: Fix file corruption using "write cache size != 0".
* Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10838 BUG #10838]: s3-winbindd: Do not use domain SID from LookupSids for Sids2UnixIDs call.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9984 BUG #9984]: s3-libnet: Add libnet_join_get_machine_spns().
* * [https://bugzilla.samba.org/show_bug.cgi?id=9985 BUG #9985]: s3-libads: Add all machine account principals to the keytab.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10472 BUG #10472]: wafsamba: If perl can't provide defaults, define them.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10824 BUG #10824]: nsswitch: Skip groups we were not able to map.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10829 BUG #10829]: s3-libads: Improve service principle guessing.
* Richard Sharpe <realrichardsharpe@gmail.com>
* * BUG: source3/smbd/process.c::srv_send_smb() returns true on the error path.

 http://www.samba.org/samba/history/samba-4.0.23.html

Samba 4.0.22
------------------------

* Release Notes for Samba 4.0.22
* September 15, 2014

===============================
This is the latest stable release of Samba 4.0.
===============================

------------------------

===============================
Major enhancements in Samba 4.0.22 include:
===============================

------------------------

*  [https://bugzilla.samba.org/show_bug.cgi?id=3204 BUG #3204]New parameter "winbind request timeout" has been added. Please see smb.conf man page for details.

===============================
Changes since 4.0.21:
===============================

------------------------

*   Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10369 BUG #10369]: build: Fix configure to honour '--without-dmapi'.
*   Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=3204 BUG #3204]: s3: winbindd: On new client connect, prune idle or hung connections older than "winbind request timeout". Add new parameter "winbind request timeout".
* * [https://bugzilla.samba.org/show_bug.cgi?id=10640 BUG #10640]: lib: tevent: make TEVENT_SIG_INCREMENT atomic.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10650 BUG #10650]: Make "case sensitive = True" option working with "max protocol = SMB2" or higher in large directories.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10728 BUG #10728]: 'net time': Fix usage and core dump.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10773 BUG #10773]: s3: smbd: POSIX ACLs. Remove incorrect check for SECINFO_PROTECTED_DACL in incoming security_information flags in posix_get_nt_acl_common().
* * [https://bugzilla.samba.org/show_bug.cgi?id=10794 BUG #10794]: vfs_dirsort: Fix an off-by-one error that can cause uninitialized memory read.
*   Björn Baumbach <bb@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10543 BUG #10543]: s3: Enforce a positive allocation_file_size for non-empty files.
*   David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10652 BUG #10652]: Samba 4 consuming a lot of CPU when re-reading printcap info.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10787 BUG #10787]: dosmode: Fix FSCTL_SET_SPARSE request validation.
*   Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10742 BUG #10742]: s4-rpc: dnsserver: Allow . to be specified for @ record.
*   Daniel Kobras <d.kobras@science-computing.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10731 BUG #10731]: sys_poll_intr: Fix timeout arithmetic.
*   Ross Lagerwall <rosslagerwall@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10778 BUG #10778]: s3:libsmb: Set a max charge for SMB2 connections.
*   Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10758 BUG #10758]: lib: Remove unused nstrcpy.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10782 BUG #10782]: smbd: Properly initialize mangle_hash.
*   Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10773 BUG #10773]: libcli/security: Add better detection of SECINFO_[UN]PROTECTED_[D|S]ACL in get_sec_info().
*   Marc Muehlfeld <mmuehlfeld@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10761 BUG #10761]: docs: Fix typos in smb.conf (inherit acls).
*   Shirish Pargaonkar <spargaonkar@suse.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10755 BUG #10755]: samba: Retain case sensitivity of cifs client.
*   Arvid Requate <requate@univention.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9570 BUG #9570]: passdb: Fix NT_STATUS_NO_SUCH_GROUP.
*   Har Gagan Sahai <SHarGagan@novell.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10759 BUG #10759]: Fix a memory leak in cli_set_mntpoint().
*   Roel van Meer <roel@1afa.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10777 BUG #10777]: Don't discard result of checking grouptype.

 http://www.samba.org/samba/history/samba-4.0.22.html

Samba 4.0.21
------------------------

* Release Notes for Samba 4.0.21
* August 1, 2014

===============================
This is a security release in order to address
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-3560 CVE-2014-3560] (Remote code execution in nmbd).

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-3560 CVE-2014-3560]:
* :Samba 4.0.0 to 4.1.10 are affected by a remote code execution attack on unauthenticated nmbd NetBIOS name services.

* :A malicious browser can send packets that may overwrite the heap of the target nmbd NetBIOS name services daemon. It may be possible to use this to generate a remote code execution vulnerability as the superuser (root).

===============================
Changes since 4.0.20:
===============================

------------------------

*   Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10735 BUG 10735] : [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-3560 CVE-2014-3560]: Fix unstrcpy macro length.

 http://www.samba.org/samba/history/samba-4.0.21.html

Samba 4.0.20
------------------------

* Release Notes for Samba 4.0.20
* July 30, 2014

===============================
This is the latest stable release of the Samba 4.0 release series.
===============================

------------------------

===============================
Changes since 4.0.19:
===============================

------------------------

*   Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=3124 BUG #3124]: s3: smb2: Fix 'xcopy /d' with samba shares.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10653 BUG #10653]: Samba won't start on a machine configured with only IPv4.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10673 BUG #10673]: s3: SMB2: Fix leak of blocking lock records in the database.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10684 BUG #10684]: SMB1 blocking locks can fail notification on unlock, causing client timeout.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10685 BUG #10685]: s3: smbd: Locking, fix off-by one calculation in brl_pending_overlap().
* * [https://bugzilla.samba.org/show_bug.cgi?id=10692 BUG #10692]: wbcCredentialCache fails if challenge_blob is not first.
*   Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10627 BUG #10627]: rid_array used before status checked - segmentation fault due to null pointer dereference.
*   David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10612 BUG #10612]: printing: Fix purge of all print jobs.
*   Björn Jacke <bj@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=3263 BUG #3263]: net/doc: Make clear that net vampire is for NT4 domains only.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10657 BUG #10657]: autobuild: Delete $NSS_MODULES in "make clean".
*   Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10663 BUG #10663]: msg_channel: Fix a 100% CPU loop.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10633 BUG #10633]: smbstatus: Fix an uninitialized variable.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10633 BUG #10633]: 'RW2' smbtorture test fails when -N <numprocs> is set to 2 due to the invalid status check in the second client.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10699 BUG #10699]: smbd: Avoid double-free in get_print_db_byname.
*   Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10469 BUG #10469]: ldb-samba: fix a memory leak in ldif_canonicalise_objectCategory().
* * [https://bugzilla.samba.org/show_bug.cgi?id=10692 BUG #10692]: wbcCredentialCache fails if challenge_blob is not first.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10696 BUG #10696]: Backport autobuild/selftest fixes from master.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10706 BUG #10706]: s3:smb2_read: let smb2_sendfile_send_data() behave like send_file_readX().

 http://www.samba.org/samba/history/samba-4.0.20.html

Samba 4.0.19
------------------------

* Release Notes for Samba 4.0.19
* June 23, 2014

===============================
This is a security release in order to address
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0244 CVE-2014-0244]
* All current released versions of Samba are vulnerable to a denial of service on the nmbd NetBIOS name services daemon. A malformed packet can cause the nmbd server to loop the CPU and prevent any further NetBIOS name service.
* This flaw is not exploitable beyond causing the code to loop expending CPU resources.

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-3493 CVE-2014-3493]
* All current released versions of Samba are affected by a denial of service crash involving overwriting memory on an authenticated connection to the smbd file server.

===============================
Changes since 4.0.18:
===============================

------------------------

*   Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10633 BUG #10633]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0244 CVE-2014-0244]: Fix nmbd denial of service.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10654 BUG #10654]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-3493 CVE-2014-3493]: Fix segmentation fault in smbd_marshall_dir_entry()'s SMB_FIND_FILE_UNIX handler.

 http://www.samba.org/samba/history/samba-4.0.19.html

Samba 4.0.18
------------------------

* Release Notes for Samba 4.0.18
* May 27, 2014

===============================
This is the latest stable release of Samba 4.0.
===============================

------------------------

Please note that this bug fix release also addresses two minor security issues without being a dedicated security release:
* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0239 CVE-2014-0239]: dns: Don't reply to replies (bug #10609).
* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0178 CVE-2014-0178]: Malformed FSCTL_SRV_ENUMERATE_SNAPSHOTS response ([https://bugzilla.samba.org/show_bug.cgi?id=10549 BUG #10549]).

For more details including security advisories and patches, please see

*http://www.samba.org/samba/history/security.html

===============================
Changes since 4.0.17:
===============================

------------------------

* Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10548 BUG #10548]: build: Fix ordering problems with lib-provided and internal RPATHs.
* Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10577 BUG #10577]: SMB1 wildcard unlink fail can leave a retry record on the open retry queue.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10564 BUG #10564]: Fix lock order violation and file lost.
* Björn Baumbach <bb@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10239 BUG #10239]: s3-nmbd: Reset debug settings after reading config file.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10544 BUG #10544]: s3-lib/util: set_namearray reads across end of namelist string.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10556 BUG #10556]: lib-util: Rename memdup to smb_memdup and fix all callers.
* Kai Blin <kai@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10609 BUG #10609]: CVE-2014-0239: dns: Don't reply to replies.
* David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10590 BUG #10590]: byteorder: Do not assume PowerPC is big-endian.
* Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10472 BUG #10472]: script/autobuild: Make use of '--with-perl-{arch,lib}-install-dir'.
* Noel Power <nopower@suse.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10554 BUG #10554]: Fix read of deleted memory in reply_writeclose()'.
* Jose A. Rivera <jarrpa@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10151 BUG #10151]: Extra ':' in msg for Waf Cross Compile Build System with Cross-answers command.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10348 BUG #10348]: Fix empty body in if-statement in continue_domain_open_lookup.
* Christof Schmitt <christof.schmitt@us.ibm.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10549 BUG #10549]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0178 CVE-2014-0178]: Malformed FSCTL_SRV_ENUMERATE_SNAPSHOTS response.
* Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10472 BUG #10472]: wafsamba: Fix the installation on FreeBSD.

 http://www.samba.org/samba/history/samba-4.0.18.html

Samba 4.0.17
------------------------

* Release Notes for Samba 4.0.17
* April 15, 2014
===============================
This is the latest stable release of Samba 4.0.
===============================

------------------------

===============================
Changes since 4.0.16:
===============================

------------------------

*   Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9878 bug #9878]: Make "force user" work as expected.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9942 bug #9942]: Fix problem with server taking too long to respond to a MSG_PRINTER_DRVUPGRADE message.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9993 bug #9993]: s3-printing: Fix obvious memory leak in printer_list_get_printer().
* * [https://bugzilla.samba.org/show_bug.cgi?id=10344 bug #10344]: SessionLogoff on a signed connection with an outstanding notify request crashes smbd.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10431 bug #10431]: Fix STATUS_NO_MEMORY response from Query File Posix Lock request.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10508 bug #10508]: smbd: Correctly add remote users into local groups.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10534 bug #10534]: Cleanup messages.tdb record after unclean smbd shutdown.
*   Christian Ambach <ambi@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9911 bug #9911]: Fix build on AIX with IBM XL C/C++ (gettext detection issues).
* * [https://bugzilla.samba.org/show_bug.cgi?id=10308 bug #10308]: Fix String Conversion Errors with Samba 4.1.0 Build on AIX 7.1.
*   Andrew Bartlett <abartlet@samba.org>
* * smbd: Split create_conn_struct into a fn that does not change the working dir.
*   Gregor Beck <gbeck@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10458 bug #10458]: Fix 'wbinfo -i' with one-way trust.
* * s3:rpc_server: Minor refactoring of process_request_pdu().
*   Kai Blin <kai@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10471 bug #10471]: Don't respond with NXDOMAIN to records that exist with another type.
*   Alexander Bokovoy <ab@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10504 bug #10504]: lsa.idl: Define lsa.ForestTrustCollisionInfo and ForestTrustCollisionRecord as public structs.
*   Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10439 bug #10439]: Increase max netbios name components.
*   David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10188 bug #10188]: doc: Add "spoolss: architecture" parameter usage.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10484 bug #10484]: Initial FSRVP rpcclient requests fail with NT_STATUS_PIPE_NOT_AVAILABLE.
*   Daniel Liberman <danielvl@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10387 bug #10387]: 'net ads search' on high latency networks can return a partial list with no error indication.
*   Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10344 bug #10344]: SessionLogoff on a signed connection with an outstanding notify request crashes smbd.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10422 bug #10422]: max xmit > 64kb leads to segmentation fault.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10444 bug #10444]: smbd_server_connection_terminate("CTDB_SRVID_RELEASE_IP") panics from within ctdbd_migrate() with invalid lock_order.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10464 bug #10464]: samba4 services not binding on IPv6 addresses causing connection delays.
* * tevent: Fix crash bug in tevent_queue_immediate_trigger().
*   Garming Sam <garming@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10378 bug #10378]: dfs: Always call create_conn_struct with root privileges.
*   Andreas Schneider <asn@cryptomilk.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10472 bug #10472]: pidl: waf should have an option for the dir to install perl files and do not glob.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10474 bug #10474]: s3-spoolssd: Don't register spoolssd if epmd is not running.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10481 bug #10481]: s3-rpc_server: Fix handling of fragmented rpc requests.
*   Gustavo Zacarias <gustavo@zacarias.com.ar>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10506 bug #10506]: Make 'smbreadline' build with readline 6.3.

 http://www.samba.org/samba/history/samba-4.0.17.html

Samba 4.0.16
------------------------

* Release Notes for Samba 4.0.16
* March 11, 2014

===============================
This is a security release in order to address
===============================

------------------------

*[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4496 CVE-2013-4496] (Password lockout not enforced for SAMR password changes) and [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-6442 CVE-2013-6442] (smbcacls can remove a file or directory ACL by mistake).

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4496 CVE-2013-4496]: Samba versions 3.4.0 and above allow the administrator to implement locking out Samba accounts after a number of bad password attempts.
* :However, all released versions of Samba did not implement this check for password changes, such as are available over multiple SAMR and RAP interfaces, allowing password guessing attacks.

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-6442 CVE-2013-6442]: Samba versions 4.0.0 and above have a flaw in the smbcacls command. If smbcacls is used with the "-C|--chown name" or "-G|--chgrp name" command options it will remove the existing ACL on the object being modified, leaving the file or directory unprotected.

===============================
Changes since 4.0.15:
===============================

------------------------

* Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10327 bug #10327]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-6442 CVE-2013-6442]: ensure we don't lose an existing ACL when setting owner or group owner.
* Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10245 bug #10245]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4496 CVE-2013-4496]: Enforce password lockout for SAMR password changes.
* Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10245 bug #10245]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4496 CVE-2013-4496]: Enforce password lockout for SAMR password changes.

 http://www.samba.org/samba/history/samba-4.0.16.html

Samba 4.0.15
------------------------

* February 18, 2014

===============================
This is the latest stable release of Samba 4.0.
===============================

------------------------

Changes since 4.0.14:=
===============================

------------------------

* Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10259 bug #10259]: Make shadow_copy2 module working with Windows 7.
* Alistair Leslie-Hughes <leslie_alistair@hotmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10087 bug #10087]: ntlm_auth sometimes returns the wrong username to mod_ntlm_auth_winbind.
* Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=2662 bug #2662]: Make revamped directory handling code 64bit clean.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10358 bug #10358]: Fix 100% CPU utilization in winbindd when trying to free memory in winbindd_reinit_after_fork.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10429 bug #10429]: s3: modules: streaminfo: As we have no VFS function SMB_VFS_LLISTXATTR we can't cope with a symlink when lp_posix_pathnames() is true.
* Christian Ambach <ambi@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10280 bug #10280]: s3:winbindd: Fix use of uninitialized variables.
* Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10418 bug #10418]: Fix INTERNAL ERROR: Signal 11 in the kdc pid.
* Jeffrey Clark <dude@zaplabs.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10418 bug #10418]: Add support for Heimdal's unified krb5 and hdb plugin system.
* Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=2191 bug #2191]: s3-winbind: Improve performance of wb_fill_pwent_sid2uid_done().
* * [https://bugzilla.samba.org/show_bug.cgi?id=10415 bug #10415]: smbd: Fix memory overwrites.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10436 bug #10436]: smbd: Fix an ancient oplock bug.
* Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10442 bug #10442]: Fix crash bug in smb2_notify code.
* Jelmer Vernooij <jelmer@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10418 bug #10418]: Cope with first element in hdb_method having a different name in different heimdal versions.

 http://www.samba.org/samba/history/samba-4.0.15.html

Samba 4.0.14
------------------------

* Release Notes for Samba 4.0.14
* January 7, 2014

===============================
This is is the latest stable release of Samba 4.0.
===============================

------------------------

Major enhancements in Samba 4.0.14 include:=
===============================

------------------------

*  [https://bugzilla.samba.org/show_bug.cgi?id=10284 bug #10284]:Fix segfault in smbd.
*  [https://bugzilla.samba.org/show_bug.cgi?id=10311 bug #10311]:Fix SMB2 server panic when a smb2 brlock times out.

Changes since 4.0.13:=
===============================

------------------------

* Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9870 bug #9870]: smbd: Allow updates on directory write times on open handles.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10305 bug #10305]: ldb: Fix bad if test in ldb_comparison_fold().
* * [https://bugzilla.samba.org/show_bug.cgi?id=10320 bug #10320]: s3:smbpasswd: Fix crashes on invalid input.
* David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10271 bug #10271]: Send correct job-ID in print job notifications.
* Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10250 bug #10250]: smbd: Fix a talloc hierarchy problem in msg_channel.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10284 bug #10284]: smbd: Fix segfault.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10297 bug #10297]: smbd: Fix writing to a directory with -wx permissions on a share.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10311 bug #10311]: Fix SMB2 server panic when a smb2 brlock times out.
*   Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10298 bug #10298]: Reduce smb2_server processing overhead.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10330 bug #10330]: s3:configure: Require tevent >= 0.9.18 as external library.
*   Arvid Requate <requate@univention.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10267 bug #10267]: spoolss: Accept XPS_PASS datatype used by Windows 8.
* Christof Schmitt <cs@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10310 bug #10310]: Fix AIO with SMB2 and locks.
*   Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=2191 bug #2191]: Fix substution of %G/%g in 'template * homedir'.

Samba 4.0.13
------------------------

* Release Notes for Samba 4.0.13
* December 9, 2013

===============================
This is a security release in order to address
===============================

------------------------

*[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4408 CVE-2013-4408] (DCE-RPC fragment length field is incorrectly checked) and
*[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-6150 CVE-2012-6150] (pam_winbind login without require_membership_of restrictions).

*[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4408 CVE-2013-4408]:
* Samba versions 3.4.0 and above (versions 3.4.0 - 3.4.17, 3.5.0 - 3.5.22, 3.6.0 - 3.6.21, 4.0.0 - 4.0.12 and including 4.1.2) are vulnerable to buffer overrun exploits in the client processing of DCE-RPC packets. This is due to incorrect checking of the DCE-RPC fragment length in the client code.

* This is a critical vulnerability as the DCE-RPC client code is part of the winbindd authentication and identity mapping daemon, which is commonly configured as part of many server installations (when joined to an Active Directory Domain). A malicious Active Directory Domain Controller or man-in-the-middle attacker impersonating an Active Directory Domain Controller could achieve root-level access by compromising the winbindd process.

* Samba server versions 3.4.0 - 3.4.17 and versions 3.5.0 - 3.5.22 are also vulnerable to a denial of service attack (server crash) due to a similar error in the server code of those versions.

* Samba server versions 3.6.0 and above (including all 3.6.x versions, all 4.0.x versions and 4.1.x) are not vulnerable to this problem.

* In addition range checks were missing on arguments returned from calls to the DCE-RPC functions LookupSids (lsa and samr), LookupNames (lsa and samr) and LookupRids (samr) which could also cause similar problems.

* As this was found during an internal audit of the Samba code there are no currently known exploits for this problem (as of December 9th 2013).

*[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-6150 CVE-2012-6150]:
* Winbind allows for the further restriction of authenticated PAM logins using the require_membership_of parameter. System administrators may specify a list of SIDs or groups for which an authenticated user must be a member of. If an authenticated user does not belong to any of the entries, then login should fail. Invalid group name entries are ignored.

* Samba versions 3.3.10, 3.4.3, 3.5.0 and later incorrectly allow login from authenticated users if the require_membership_of parameter specifies only invalid group names.

* This is a vulnerability with low impact. All require_membership_of group names must be invalid for this bug to be encountered.

===============================
Changes since 4.0.12:
===============================

------------------------

* Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10185 bug #10185] [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4408 CVE-2013-4408]: Correctly check DCE-RPC fragment length field.
* Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10185 bug #10185] [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4408 CVE-2013-4408]: Correctly check DCE-RPC fragment length field.
* Noel Power <noel.power@suse.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10300 bug #10300], [https://bugzilla.samba.org/show_bug.cgi?id=10306 bug #10306]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-6150 CVE-2012-6150]: Fail authentication if user isn't member of *any* require_membership_of specified groups.

Samba 4.0.12
------------------------

* Release Notes for Samba 4.0.12
* November 19, 2013

This is is the latest stable release of Samba 4.0.

===============================
Major enhancements in Samba 4.0.12 include:
===============================

------------------------

*  RW Deny for a specific user is not overriding RW Allow for a group (bug #10196)

===============================
Changes since 4.0.11:
===============================

------------------------

*   Jeremy Allison <jra@samba.org>
*   Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10187 bug #10187]: Missing talloc_free can leak stackframe in error path.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10196 bug #10196]: RW Deny for a specific user is not overriding RW Allow for a group.
*   Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10052 bug #10052]: dfs_server: Use dsdb_search_one to catch 0 results as well as NO_SUCH_OBJECT errors.
*   Samuel Cabrero <scabrero@zentyal.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9091 bug #9091]: s4-dns: dlz_bind9: Create dns-HOSTNAME account disabled.
*   Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10264 bug #10264]: s3-winbindd: Fix cache_traverse_validate_fn failure for NDR cache entries.
*   Björn Jacke <bj@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10247 bug #10247]: xattr: Fix listing EAs on *BSD for non-root users.
*   Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10195 bug #10195]: nsswitch: Fix short writes in winbind_write_sock.
*   Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9905 bug #9905]: ldap_server: Register name and pid at startup.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10193 bug #10193]: s4:dsdb/rootdse: report 'dnsHostName' instead of 'dNSHostName'.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10232 bug #10232]: libcli/smb: Fix smb2cli_ioctl*() against Windows 2008.
*   Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10194 bug #10194]: Make offline logon cache updating for cross child domain group membership.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10269 bug #10269]: util: Remove 32bit macros breaking strict aliasing.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10253 bug #10253]: Fix the build of vfs_glusterfs.

Samba 4.0.11
------------------------

* Release Notes for Samba 4.0.11
* November 11, 2013

This is a security release in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4475 CVE-2013-4475] (ACLs are not checked on opening an alternate data stream on a file or directory) and CVE-2013-4476 (Private key in key.pem world readable).

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4475 CVE-2013-4475]:
* Samba versions 3.2.0 and above (all versions of 3.2.x, 3.3.x, 3.4.x, 3.5.x, 3.6.x, 4.0.x and 4.1.x) do not check the underlying file or directory ACL when opening an alternate data stream.
* According to the SMB1 and SMB2+ protocols the ACL on an underlying file or directory should control what access is allowed to alternate data streams that are associated with the file or directory.
* By default no version of Samba supports alternate data streams on files or directories.
* Samba can be configured to support alternate data streams by loading either one of two virtual file system modues (VFS) vfs_streams_depot or vfs_streams_xattr supplied with Samba, so this bug only affects Samba servers configured this way.
* To determine if your server is vulnerable, check for the strings "streams_depot" or "streams_xattr" inside your smb.conf configuration file.

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4476 CVE-2013-4476]:
* In setups which provide ldap(s) and/or https services, the private key for SSL/TLS encryption might be world readable. This typically happens in active directory domain controller setups.

===============================
Changes since 4.0.10:
===============================

------------------------

* Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10234 bug #10234] + [https://bugzilla.samba.org/show_bug.cgi?id=10229 bug #10229]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4475 CVE-2013-4475]: Fix access check verification on stream files.
* Björn Baumbach <bb@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10234 bug #10234]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4476 CVE-2013-4476]: Private key in key.pem world readable.

Samba 4.0.10
------------------------

* Release Notes for Samba 4.0.10
* October 8, 2013

===============================
This is is the latest stable release of Samba 4.0.
===============================

------------------------

Major enhancements in Samba 4.0.10 include:=
===============================

------------------------

* [https://bugzilla.samba.org/show_bug.cgi?id=10158 bug #10158]NetBIOS related samba process consumes 100% CPU.
* [https://bugzilla.samba.org/show_bug.cgi?id=10138 bug #10138]smbd: Clean up share modes after hard crash.
* [https://bugzilla.samba.org/show_bug.cgi?id=10162 bug #10162]Fix POSIX ACL mapping when setting DENY ACE's from Windows.

To ease upgrades from Samba 3.6 and older, a new parameter called "acl allow execute always" has been introduced as a temporary workaround. Please see the smb.conf man page for details.

Changes since 4.0.9:=
===============================

------------------------

* Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10134 bug #10134]: Ease file server upgrades from 3.6 and earlier with "acl allow execute always".
* * [https://bugzilla.samba.org/show_bug.cgi?id=10169 bug #10169]: Fix build error in scavenger.c.
*   Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=5917 bug #5917]: Make Samba work on site with Read Only Domain Controller.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9166 bug #9166]: Starting smbd or nmbd with stdin from /dev/null results in "EOF on stdin".
* * [https://bugzilla.samba.org/show_bug.cgi?id=10063 bug #10063]: source3/lib/util.c:1493 leaking memory w/ pam_winbind.so / winbind.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10121 bug #10121]: Masks incorrectly applied to UNIX extension permission changes.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10139 bug #10139]: Valid utf8 filenames cause "invalid conversion error" messages.
*   Christian Ambach <ambi@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9911 bug #9911] - Build Samba 4.0.x on AIX with IBM XL C/C++.
o   Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=8077 bug #8077]: dsdb: Convert the full string from UTF16 to UTF8, including embedded NULLs.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9091 bug #9091]: When replicating DNS for bind9_dlz we need to create the server-DNS account remotely.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9461 bug #9461]: python-samba-tool fsmo: Do not give an error on a successful role transfer.
*   Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9615 bug #9615]: s3-winbindd: fix fallback to ncacn_np in cm_connect_lsat().
* * [https://bugzilla.samba.org/show_bug.cgi?id=9899 bug #9899]: s3-winbindd: fix fallback to ncacn_np in cm_connect_lsat().
* * [https://bugzilla.samba.org/show_bug.cgi?id=10147 bug #10147]: Better document potential implications of a globally used "valid users".
*   Korobkin <korobkin+samba@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10118 bug #10118]: Samba is chatty about being unable to open a printer.
*   Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9599 bug #9599]: samba-tool/dns: Pass on additional flags when creating zones.
*   Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10086 bug #10086]: smbd: Fix async echo handler forking.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10106 bug #10106]: Honour output buffer length set by the client for SMB2 GetInfo requests.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10114 bug #10114]: Dropbox (write-only-directory) case isn't handled correctly in pathname lookup.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10138 bug #10138]: smbd: Clean up share modes after hard crash.
*   Daniel Liberman <danielvl@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10162 bug #10162]: Fix POSIX ACL mapping when setting DENY ACE's from Windows.
*   Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9802 bug #9802]: Move gencache.tdb to /var/cache/samba.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10030 bug #10030]: ::1 added to nameserver on join.
*   Matthieu Patou <mat@matws.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10158 bug #10158]: NetBIOS related samba process consumes 100% CPU.
*   Christof Schmitt <christof.schmitt@us.ibm.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10137 bug #10137]: vfs_shadow_copy2 does not display previous versions correctly over SMB2.
*   Karolin Seeger <kseeger@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10076 bug #10076]: docs: Fix variable list in man vfs_crossrename.
*   Richard Sharpe <realrichardsharpe@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10097 bug #10097]: MacOSX 10.9 will not follow path-based DFS referrals handed out by Samba.
*   Ralph Wuerthner <ralph.wuerthner@de.ibm.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10106 bug #10106]: Honour output buffer length set by the client for SMB2 GetInfo requests.

Samba 4.0.9
------------------------

* Release Notes for Samba 4.0.9
* August 20, 2013

===============================
This is is the latest stable release of Samba 4.0.
===============================

------------------------

Major enhancements in Samba 4.0.9 include:=
===============================

------------------------

* [https://bugzilla.samba.org/show_bug.cgi?id=9820 bug #9820` Fix crash of Winbind after "ls -l /usr/local/samba/var/locks/sysvol".
* [https://bugzilla.samba.org/show_bug.cgi?id=10003 bug #10003` Fix segmentation fault while reading incomplete session info.
* [https://bugzilla.samba.org/show_bug.cgi?id=10013 bug #10013` smbd: Fix a 100% loop at shutdown time.

Changes since 4.0.8:=
===============================

------------------------

* Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9930 bug #9930]: smbd: Cleanup disonnected durable handles.
* Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9992 bug #9992]: Fix Windows error 0x800700FE when copying files with xattr names containing ":".
* * [https://bugzilla.samba.org/show_bug.cgi?id=10064 bug #10064]: Linux kernel oplock breaks can miss signals.
* Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9820 bug #9820]: Fix crash of Winbind after "ls -l /usr/local/samba/var/locks/sysvol".
* * [https://bugzilla.samba.org/show_bug.cgi?id=10014 bug #10014]: Fix excessive RID allocation.
* Björn Baumbach <bb@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10003 bug #10003]: s3-lib: Fix segmentation fault while reading incomplete session info.
* Gregor Beck <gbeck@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9678 bug #9678]: Windows 8 Roaming profiles fail.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9930 bug #9930]: smbd: Cleanup disonnected durable handles.
* Kai Blin <kai@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10015 bug #10015]: Add debugclass for DNS server.
* Alexander Bokovoy <ab@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9779 bug #9779]: Add UPN enumeration to passdb internal API.
* Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10043 bug #10043]: Allow to change the default location for Kerberos credential caches.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10073 bug #10073]: net ads join: Fix segmentation fault in create_local_private_krb5_conf_for_domain.
* Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10013 bug #10013]: smbd: Fix a 100% loop at shutdown time.
* Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9820 bug #9820]: Fix crash of Winbind after "ls -l /usr/local/samba/var/locks/sysvol".
* * [https://bugzilla.samba.org/show_bug.cgi?id=10003 bug #10003]: s3-lib: Fix segmentation fault while reading incomplete session info.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10015 bug #10015]: Fix/improve debug options.
* Christof Schmitt <christof.schmitt@us.ibm.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9970 bug #9970]: vfs_streams_xattr: Do not attempt to write empty attribute twice.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9994 bug #9994]: s3-winbind: Do not delete an existing valid credential cache.
* * [https://bugzilla.samba.org/show_bug.cgi?id=10073 bug #10073]: net ads join: Fix segmentation fault in create_local_private_krb5_conf_for_domain.
* Ralph Wuerthner <ralphw@de.ibm.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10064 bug #10064]: Linux kernel oplock breaks can miss signals.

Samba 4.0.8
------------------------

* Release Notes for Samba 4.0.8
* August 05, 2013

===============================
This is a security release in order to address
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4124 CVE-2013-4124] (Missing integer wrap protection in EA list reading can cause
server to loop with DOS).

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4124 CVE-2013-4124]:
* All current released versions of Samba are vulnerable to a denial of service on an authenticated or guest connection. A malformed packet can cause the smbd server to loop the CPU performing memory allocations and preventing any further service.

* A connection to a file share, or a local account is needed to exploit this problem, either authenticated or unauthenticated if guest connections are allowed.

* This flaw is not exploitable beyond causing the code to loop allocating memory, which may cause the machine to exceed memory limits.

Changes since 3.6.16:=
===============================

------------------------

*Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10010 bug #10010`: CVE-2013-4124: Missing integer wrap protection in EA list reading can cause server to loop with DOS.

Samba 4.0.7
------------------------

* Release Notes for Samba 4.0.7
* July 2, 2013

===============================
This is is the latest stable release of Samba 4.0.
===============================

------------------------

Major enhancements in Samba 4.0.7 include:=
===============================

------------------------

* [https://bugzilla.samba.org/show_bug.cgi?id=9794 bug #9794]Fix a core dump with invalid lock order while opening/editing or copying MS files.
* [https://bugzilla.samba.org/show_bug.cgi?id=9867 bug #9867]Fix crash bug from search of mail= .
* [https://bugzilla.samba.org/show_bug.cgi?id=9832 bug #9832]winbind4: talloc use after free.

Changes since 4.0.6:=
===============================

------------------------

* Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9909 bug #9909]: build: Add missing new line to replaced python shebang line.
* Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9794 bug #9794]: Fix a core dump with invalid lock order while opening/editing or copying MS files.
* Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9465 bug #9465]: s3-rpc_server: Ensure we are root when starting and using gensec.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9906 bug #9906]: Doc fixes for 4.0.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9907 bug #9907]: Build fixes for 4.0 found during autoconf or debian packaging work.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9967 bug #9967]: Fix crash bug from search of mail=.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9968 bug #9968]: Fix build with system Heimdal of samba4kgetcred.
* Björn Baumbach <bb@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9947 bug #9947]: Check for netbios aliases in ad_get_referrals.
* Kai Blin <kai@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9485 bug #9485]: Add support for MX queries.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9559 bug #9559]: dns: Delete dnsNode objects when they are empty.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9632 bug #9632]: dns: Support larger queries when asking forwarder.
* David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=8997 bug #8997]: Change libreplace GPL source to LGPL.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9900 bug #9900]: is_printer_published GUID retrieval.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9910 bug #9910]: PIE builds not supported.
* Peng Haitao <penght@cn.fujitsu.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9941 bug #9941]: Fix a bug of drvupgrade of smbcontrol.
* Björn Jacke <bj@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9880 bug #9880]: Use of wrong RFC2307 primary group field.
* Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9832 bug #9832]: winbind4: talloc use after free.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9953 bug #9953]: Fix tevent_poll on 32-bit machines (Coverity ID 989236).
* Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9805 bug #9805]: s3:lib/server_mutex: Open mutex.tdb with CLEAR_IF_FIRST.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9929 bug #9929]: s4:winbind: Don't leak libnet_context into the main event context.
* Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9881 bug #9881]: Check for system libtevent.
* Michael Wood <esiotrot@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9964 bug #9964]: docs: Avoid mentioning a possibly misleading option.
* Vadim Zhukov <persgray@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9888 bug #9888]: More generic check for OpenBSD platform.

Samba 4.0.6
------------------------

* Release Notes for Samba 4.0.6
* May 21, 2013

===============================
This is is the latest stable release of Samba 4.0.
===============================

------------------------

Major enhancements in Samba 4.0.6 include:=
===============================

------------------------

* [https://bugzilla.samba.org/show_bug.cgi?id=9822 bug #9822] Fix crash during Win8 sync.
* [https://bugzilla.samba.org/show_bug.cgi?id=9834 bug #9834] Fix segfault when loging in with wrong password from w2k8r2.

Changes since 4.0.5:=
===============================

------------------------

* Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9412 bug #9412]: SMB2 server doesn't support recvfile.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9722 bug #9722]: Properly handle oplock breaks in compound requests.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9777 bug #9777]: vfs_dirsort uses non-stackable calls, dirfd(), malloc instead of talloc and doesn't cope with directories being modified whilst reading.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9811 bug #9811]: Old DOS SMB CTEMP request uses a non-VFS function to access the filesystem.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9822 bug #9822]: Fix crash during Win8 sync.
* Anand Avati <avati@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9833 bug #9833]: Function called in unix_convert() path can overwrite errno.
* Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9785 bug #9785]: Use specified python for runtime installation of Samba.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9834 bug #9834]: Fix segfault when loging in with wrong password from w2k8r2.
* Alexander Bokovoy <ab@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9767 bug #9767]: Fix 'net ads join' when called via stdin.
* David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9807 bug #9807]: wbinfo: Fix segfault in wbinfo_pam_logon.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9830 bug #9830]: Fix panic in nt_printer_publish_ads.
* Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9775 bug #9775]: Fix segfault for "artificial" conn_structs in vfs_fake_perms.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9809 bug #9809]: Package new dbwrap_tool man page.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9824 bug #9824]: SMB signing and the async echo responder don't work together.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9832 bug #9832]: talloc use after free in winbind4.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9854 bug #9854]: Fix NULL pointer dereference in Winbind.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9868 bug #9868]: Fix making LIBNDR_PREG_OBJ.
* Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9545 bug #9545]: Fix the build of vfs_notify_fam.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9803 bug #9803]: Change '--with-dmapi' to 'default=auto' to match the autoconf build.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9804 bug #9804]: wafsamba: Display the default value in help for SAMBA3_ADD_OPTION.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9382 bug #9382]: Add support for PFC_FLAG_OBJECT_UUID when parsing packets.
* Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9139 bug #9139]: Fix the username map optimization.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9699 bug #9699]: Fix adding case sensitive spn.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9766 bug #9766]: Cache name_to_sid/sid_to_name correctly.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9817 bug #9817]: Fix 'map untrusted to domain' with NTLMv2.
* Richard Sharpe <realrichardsharpe@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9722 bug #9722]:  Properly handle oplock breaks in compound requests.
* Ralph Wuerthner <ralph.wuerthner@de.ibm.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9782 bug #9782]: Fix panic when running 'smbtorture smb.base'.

Samba 4.0.5
------------------------

* Release Notes for Samba 4.0.5
* April 9, 2013

===============================
This is is the latest stable release of Samba 4.0.
===============================

------------------------

Major enhancements in Samba 4.0.5 include:=
===============================

------------------------

*Fix large reads/writes from some Linux clients [https://bugzilla.samba.org/show_bug.cgi?id=9706 bug #9706].
*Add 'samba-tool dbcheck --reset-well-known-acls' [https://bugzilla.samba.org/show_bug.cgi?id=9740 bug #9740] and [https://bugzilla.samba.org/show_bug.cgi?id=9267 bug #9267].

Changes since 4.0.4:=
===============================

------------------------

* Michael Adam <obnox@samba.org>
* * BUG 9617: libnss-winbindd does not provide pass struct for groups mapped with ID_TYPE_BOTH and vice versa.
* * BUG 9653: idmap_autorid: Fix freeing of non-talloced memory.
* * BUG 9711: s4:winbindd: Do not drop the workgroup name in the getgrnam, getgrent and getgrgid calls.
* Jeremy Allison <jra@samba.org>
* * BUG 9130: Certain xattrs cause Windows error 0x800700FF.
* * BUG 9519: Samba returns unexpected error on SMB posix open.
* * BUG 9642: Fix the build of vfs_afsacl.
* * BUG 9695: Backport tevent changes to bring library to version 0.9.18.
* * BUG 9706: Fix large reads/writes from some Linux clients.
* * BUG 9724: is_encrypted_packet() function incorrectly used inside server.
* * BUG 9733: Fix 'smbcontrol close-share'.
* * BUG 9748: Remove unneeded fstat system call from hot read path.
* * BUG 9760: Fix incorrect parsing of SMB2 command codes.
* Christian Ambach <ambi@samba.org>
* * BUG 9643: Fix the build with --fake-kaserver.
* * BUG 9644: Fix compile of source3/lib/afs.c.
* * BUG 9669: Fix crash in 'net rpc join' against a Samba 3.0.33 PDC.
* Timur Bakeyev <timur@FreeBSD.org>
* * BUG 9666: Fix filtering of link-local addresses.
* Andrew Bartlett <abartlet@samba.org>
* * BUG 9663: 'make test' hangs.
* * BUG 9697: DsReplicaGetInfo fails due to sendto() EMSGSIZE error on UNIX domain socket.
* * BUG 9703: Fix build on solaris8: Do not force a specific perl on pod2man.
* * BUG 9717: Set LD_LIBRARY_PATH in install_with_python.sh.
* * BUG 9718: s4-idmap: Remove requirement that posixAccount or posixGroup be set for rfc2307.
* * BUG 9719: Allow forcing an override of an old @MODULES record.
* * BUG 9720: Do not print the admin password during 'samba-tool classicupgrade'.
* * BUG 9721: Make samba_upgradedns more robust (do not guess addresses when just changing roles).
* * BUG 9725: upgradeprovision and 'samba-tool dbcheck' patches for 4.0.NEXT.
* * BUG 9728: DO NOT install samba_upgradeprovision in 4.0.x.
* * BUG 9739: PIDL: Build fixes for hosts without CPP (Solaris 11).
* * BUG 9740: Add 'samba-tool dbcheck --reset-well-known-acls'.
* * BUG 9267: Can't delegate adding computers to domain.
* Alexander Bokovoy <ab@samba.org>
* * BUG 9636: PIDL: Fix parsing linemarkers in preprocessor output.
* * BUG 9639: Rename internal subsystem pdb_ldap to pdb_ldapsam.
* Ira Cooper <ira@samba.org>
* * BUG 9646: Make SMB2_GETINFO multi-volume aware.
* David Disseldorp <ddiss@samba.org>
* * BUG 9633: Recursive mget should continue on EPERM.
* Landon Fuller <landonf@bikemonkey.org>
* * BUG 9656: Work around FreeBSD's getaddrinfo() underscore issue.
* * BUG 9696: Remove incomplete samba_dnsupdate IPv6 link-local address check.
* * BUG 9697: Handle EMSGSIZE on UNIX domain sockets.
* Björn Jacke <bj@sernet.de>
* * BUG 7825: Fix GNU ld version detection with old gcc releases.
* Daniel Kobras <d.kobras@science-computing.de>
* * BUG 9039: Never try to map global SAM name.
* Guenter Kukkukk <kukks@samba.org>
* * BUG 9701: Fix vfs_catia and update documentation.
* Volker Lendecke <vl@samba.org>
* * BUG 9695: Backport tevent changes to bring library to version 0.9.18.
* * BUG 9727: Fix NULL pointer dereference.
* * BUG 9736: Change to smbd/dir.c code gives significant performance increases on large directory listings.
* Stefan Metzmacher <metze@samba.org>
* * BUG 9557: Fix build on AIX.
* * BUG 9625: Reauth-capable client fails to access shares on Windows member.
* * BUG 9695: Backport tevent changes to bring library to version 0.9.18.
* * BUG 9706: Parameter is incorrect on Android.
* Andreas Schneider <asn@samba.org>
* * BUG 9664: Fix correct linking of libreplace with cmdline-credentials.
* * BUG 9683: Fix several resource (fd) leaks.
* * BUG 9685: Fix a memory leak in spoolss rpc server.
* * BUG 9686: Fix a possible buffer overrun in pdb_smbpasswd.
* * BUG 9687: Fix several possible null pointer dereferences.
* * BUG 9723: Add a tool to migrate latin1 printing tdbs to registry.
* * BUG 9735: Fix Winbind separator in upn to username conversion.
* * BUG 9758: Don't leak the epm_Map policy handle.
* Richard Sharpe <rsharpe@samba.org>
* * BUG 9674: Samba denies owner Read Control when there is a DENY entry while W2K08 does not.
* * BUG 9689: Make sure that domain joins work correctly when the DC disallows NTLM auth.
* * BUG 9704: Fix nss_winbind name on FreeBSD.
* * BUG 9747: Make sure that we only propogate the INHERITED flag when we are allowed to.

Note about upgrading from older versions:=
===============================

------------------------

It is still the case that there are printing tdbs (ntprinting.tdb, ntforms.tdb, ntdrivers.tdb) which are in latin1 or other encodings. When updating from Samba 3.5 or earlier to Samba 3.6 or 4.0 these tdbs need to be migrated to our new registry based printing management.  This means during the migration we also need to do charset conversion. This can only been done manually cause we don't
know in which encoding the tdb is. You have to specify the correct code page for the conversion, see iconv -l and Wikipedia [1] for the available codepages. The mostly used one is Windows Latin1 which is CP1252.

We've extended the 'net printing dump' and 'net printing migrate' commands to define the encoding of the tdb. So you can correctly view the tdb with:

    et printing dump encoding=CP1252 /path/to/ntprinters.tdb

or migrate it with e.g.:

    et printing migrate encoding=CP1252 /path/to/ntprinters.tdb

If you migrate printers we suggest you do it in the following order.

* ntforms.tdb
* ntdrivers.tdb
* ntprinting.tdb

Don't forget to rename, move or delete these files in /var/lib/samba after the
migration.

[1] https://en.wikipedia.org/wiki/Code_page

 http://www.samba.org/samba/history/samba-4.0.5.html

Samba 4.0.4
------------------------

* Release Notes for Samba 4.0.4
* March 19, 2013

===============================
This is a security release in order to address CVE-2013-1863
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-1863 CVE-2013-1863] World-writeable files may be created in additional shares on a Samba 4.0 AD DC
* Administrators of the Samba 4.0 Active Directory Domain Controller might unexpectedly find files created world-writeable if additional CIFS file shares are created on the AD DC.
* Samba versions 4.0.0rc6 - 4.0.3 (inclusive) are affected by this defect.

 http://www.samba.org/samba/history/samba-4.0.4.html

Changes since 4.0.3:=
===============================

------------------------

*   Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9709 bug #9709]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-1863 CVE-2013-1863]: Remove forced set of 'create mask' to 0777.

Samba 4.0.3 
------------------------

* Release Notes for Samba 4.0.3
* February 05, 2013

===============================
This is is the latest stable release of Samba 4.0.
===============================

------------------------

Major enhancements in Samba 4.0.3 include:=
===============================

------------------------

*  check_password_quality: Handle non-ASCII characters properly [https://bugzilla.samba.org/show_bug.cgi?id=9105 bug #9105].
*  Fix ACL problem with delegation of privileges and deletion of accounts over LDAP interface [https://bugzilla.samba.org/show_bug.cgi?id=8909 bug #8909].
*  Fix 'smbd' panic triggered by unlink after open [https://bugzilla.samba.org/show_bug.cgi?id=9571 bug #9571].
*  smbd: Fix memleak in the async echo handler [https://bugzilla.samba.org/show_bug.cgi?id=9549 bug #9549].

Known issues:=
===============================

------------------------

*  For more details concerning the ACL problem with delegation of privileges and deletion of accounts over LDAP interface (bugs #8909 and #9267) regarding upgrades from older 4.0.x versions, please see

* :`Updating_Samba#Notable_Enhancements_and_Changes`

* which will be filled with details once we have worked out an upgrade strategy.

Changes since 4.0.2:=
===============================

------------------------

*   Michael Adam <obnox@samba.org>
* * BUG 9568: Document the command line options in dbwrap_tool(1).
*   Jeremy Allison <jra@samba.org>
* * BUG 9196: defer_open is triggered multiple times on the same request.
* * BUG 9518: conn->share_access appears not be be reset between users.
* * BUG 9550: sigprocmask does not work on FreeBSD to stop further signals in a signal handler.
* * BUG 9572: Fix file corruption during SMB1 read by Mac OSX 10.8.2 clients.
* * BUG 9586: smbd[29175]: disk_free: sys_popen() failed" message logged in /var/log/message many times.
* * BUG 9587: Archive flag is always set on directories.
* * BUG 9588: ACLs are not inherited to directories for DFS shares.
*   Andrew Bartlett <abartlet@samba.org>
* * BUG 8909: Fix ACL problem with delegation of privileges and deletion of accounts over LDAP interface.
* * BUG 9461: FSMO seize of naming role fails: NT_STATUS_IO_TIMEOUT.
* * BUG 9564: Fix compilation of Solaris ACL module.
* * BUG 9581: gensec: Allow login without a PAC by default.
* * BUG 9596: Linked attribute handling should be by GUID.
* * BUG 9598: Use pid,task_id as cluster_id in process_single just like process_prefork.
* * BUG 9609: ldb: Ensure to decrement the transaction_active whenever we delete a transaction.
* * BUG 9609: Add 'ldbdump' tool.
* * BUG 9609: ldb: Remove no-longer-existing ltdb_unpack_data_free from ldb_tdb.h.
* * BUG 9609: ldb: Change ltdb_unpack_data to take an ldb_context.
* * BUG 9610: dsdb: Make secrets_tdb_sync cope with -H secrets.ldb.
*   Björn Baumbach <bb@sernet.de>
* * BUG 9512: wafsamba: Use additional xml catalog file.
* * BUG 9517: samba_dnsupdate: Set KRB5_CONFIG for nsupdate command.
* * BUG 9552: smb.conf(5): Update list of available protocols.
* * BUG 9568: Add dbwrap_tool.1 manual page.
* * BUG 9569: ntlm_auth(1): Fix format and make examples visible.
*   Ira Cooper <ira@samba.org>
* * BUG 9575: Duplicate flags defined in the winbindd protocol.
*   Günther Deschner <gd@samba.org>
* * BUG 9474: Downgrade v4 printer driver requests to v3.
* * BUG 9595: s3-winbind: Fix the build of idmap_ldap.
*   David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9378 Bug #9378] Add extra attributes for AD printer publishing.
*   Stephen Gallagher <sgallagh@redhat.com>
* * BUG 9609: ldb: Move doxygen comments for ldb_connect to the right place.
*   Volker Lendecke <vl@samba.org>
* * BUG 9541: Make use of posix_openpt.
* * BUG 9544: Fix build of vfs_commit and plug in async pwrite support.
* * BUG 9546: Fix aio_suspend detection on FreeBSD.
* * BUG 9548: Correctly detect O_DIRECT.
* * BUG 9549: smbd: Fix memleak in the async echo handler.
*   Stefan Metzmacher <metze@samba.org>
* * BUG 8909: Fix ACL problem with delegation of privileges and deletion of accounts over LDAP interface.
* * BUG 9105: check_password_quality: Handle non-ASCII characters properly.
* * BUG 9481: samba_upgradeprovision: fix the nTSecurityDescriptor on more containers.
* * BUG 9499: s3:smb2_negprot: set the 'remote_proto' value.
* * BUG 9508: s4:drsuapi: Make sure we report the meta data from the cycle start.
* * BUG 9540: terminate the irpc_servers_byname() result with server_id_set_disconnected().
* * BUG 9598: Fix timeouts of some IRPC calls.
* * BUG 9609: Fix a warning by converting from TDB_DATA to struct ldb_val.
*   Matthieu Patou <mat@matws.net>
* * BUG 8909: Add documentation.
* * BUG 9565: Adding additional Samba 4.0 DC to W2k8 srv AD domain (in win200 functional level) produces dbcheck errors.
*   Arvid Requate <requate@univention.de>
* * BUG 9555: s4-resolve: Fix parsing of IPv6/AAAA in dns_lookup.
*   Rusty Russell <rusty@rustcorp.com.au>
* * BUG 9609: tdb: Add '-e' option to tdbdump (and document it).
* * BUG 9609: tdb: 'tdbdump' should log errors, and fail in that case.
* * BUG 9609: tdb: Add tdb_rescue() to allow an emergency best-effort dump.
*   Samba-JP oota <ribbon@samba.gr.jp>
* * BUG 9528: Remove superfluous bracket in samba.8.xml.
* * BUG 9530: Fix typo in vfs_tsmsm.8.xml.
*   Andreas Schneider <asn@samba.org>
* * BUG 9574: Fix a possible null pointer dereference in spoolss.
*   Karolin Seeger <kseeger@samba.org>
* * BUG 9591: Correct meta data in ldb manpages.
*   Pavel Shilovsky <piastry@etersoft.ru>
* * BUG 9571: Fix 'smbd' panic triggered by unlink after open.
*   Andrew Tridgell <tridge@samba.org>
* * BUG 9609: ldb: Fix callers for ldb_pack_data() and ldb_unpack_data().
* * BUG 9609: ldb: move ldb_pack.c into common.
*   Jelmer Vernooij <jelmer@samba.org>
* * BUG 9503: waf assumes that pythonX.Y-config is a Python script.

 http://www.samba.org/samba/history/samba-4.0.3.html

Samba 4.0.2
------------------------

* Release Notes for Samba 4.0.2
* January 30, 2013

===============================
This is a security release in order to address CVE-2013-0213 CVE-2013-0213.
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-0213 CVE-2013-0213] Clickjacking issue in SWAT
* All current released versions of Samba are vulnerable to clickjacking in the Samba Web Administration Tool (SWAT). When the SWAT pages are integrated into a malicious web page via a frame or iframe and then overlaid by other content, an attacker could trick an administrator to potentially change Samba settings.

* In order to be vulnerable, SWAT must have been installed and enabled either as a standalone server launched from inetd or xinetd, or as a CGI plugin to Apache. If SWAT has not been installed or enabled (which is the default install state for Samba) this advisory can be ignored.

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-0214 CVE-2013-0214] Potential XSRF in SWAT
* All current released versions of Samba are vulnerable to a cross-site request forgery in the Samba Web Administration Tool (SWAT). By guessing a user's password and then tricking a user who is authenticated with SWAT into clicking a manipulated URL on a different web page, it is possible to manipulate SWAT.

* In order to be vulnerable, the attacker needs to know the victim's password. Additionally SWAT must have been installed and enabled either as a standalone server launched from inetd or xinetd, or as a CGI plugin to Apache. If SWAT has not been installed or enabled (which is the default install state for Samba) this advisory can be ignored.

Changes since 4.0.1:=
===============================

------------------------

* Kai Blin <kai@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9576 bug #9576] CVE-2013-0213: Fix clickjacking issue in SWAT.
* * [https://bugzilla.samba.org/show_bug.cgi?id=9577 bug #9577] CVE-2013-0214: Fix potential XSRF in SWAT.

Samba 4.0.1 
------------------------

* Release Notes for Samba 4.0.1
* January 15, 2013

===============================
This is a security release in order to address CVE-2013-0172.
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-1182 CVE-2013-0172]:
*  Samba 4.0.0 as an AD DC may provide authenticated users with write access to LDAP directory objects.
*  In AD, Access Control Entries can be assigned based on the objectClass of the object.  If a user or a group the user is a member of has any access based on the objectClass, then that user has write access to that object.

*  Additionally, if a user has write access to any attribute on the object, they may have access to write to all attributes.

*  An important mitigation is that anonymous access is totally disabled by default.  The second important mitigation is that normal users are typically only given the problematic per-objectClass right via the "pre-windows 2000 compatible access" group, and Samba 4.0.0 incorrectly does not make "authenticated users" part of this group.

===============================
Changes since 4.0.0:
===============================

------------------------

* Andrew Bartlett <abartlet at samba.org>
* * Bug [https://bugzilla.samba.org/show_bug.cgi?id=9554 9554] - CVE-2013-0172 - Samba 4.0 as an AD DC may provide authenticated users with write access to LDAP directory objects.

 http://www.samba.org/samba/history/samba-4.0.1.html

Samba 4.0 
------------------------

<onlyinclude>
* Release Notes for Samba 4.0
* December 11, 2012

===============================
Release Announcements
===============================

------------------------

===============================
This is is the first stable release of Samba 4.0.
===============================

------------------------

This release contains the best of all of Samba's technology parts, both a file server (that you can reasonably expect to upgrade existing Samba 3.x releases to) and the AD domain controller work previously known as 'Samba4'.

===============================
Major enhancements in Samba 4.0.0 include:==
===============================

------------------------

==Active Directory services===
===============================

------------------------

Samba 4.0 supports the server-side of the Active Directory logon environment used by Windows 2000 and later, so we can do full domain join and domain logon operations with these clients.

Our Domain Controller (DC) implementation includes our own built-in LDAP server and Kerberos Key Distribution Center (KDC) as well as the Samba3-like logon services provided over CIFS.  We correctly generate the infamous Kerberos PAC, and include it with the Kerberos  tickets we issue.

When running an AD DC, you only need to run 'samba' (not smbd/nmbd/winbindd), as the required services are co-coordinated by this master binary. The tool to administer the Active Directory services is called 'samba-tool'.

A short guide to setting up Samba 4 as an AD DC can be found on the wiki:
* `Setting_up_Samba_as_an_Active_Directory_Domain_Controller`

==File Services===
===============================

------------------------

Samba 4.0.0 ships with two distinct file servers.  We now use the file server from the Samba 3.x series 'smbd' for all file serving by default.

Samba 4.0 also ships with the 'NTVFS' file server. This file server is what was used prior to the beta2 release of Samba 4.0, and is tuned to match the requirements of an AD domain controller. We continue to support this, not only to provide continuity to installations that have deployed it as part of an AD DC, but also as a running example of the NT-FSA architecture we expect to move smbd to in the longer term.

For pure file server work, the binaries users would expect from that series (smbd, nmbd, winbindd, smbpasswd) continue to be available.

==DNS===
===============================

------------------------

As DNS is an integral part of Active Directory, we also provide two DNS solutions, a simple internal DNS server for 'out of the box' configurations and a more elaborate BIND plugin using the BIND DLZ mechanism in versions 9.8 and 9.9. During the provision, you can select which backend to use. With the internal backend, your DNS server is good to go. If you chose the BIND_DLZ backend, a configuration file will be generated for bind to make it use this plugin, as well as a file explaining how to set up bind.

==NTP===
===============================

------------------------

To provide accurate timestamps to Windows clients, we integrate with the NTP project to provide secured NTP replies.  To use you need to start ntpd and configure it with the 'restrict ... ms-sntp' and ntpsigndsocket options.

==Python Scripting Interface===
===============================

------------------------

A new scripting interface has been added to Samba 4, allowing Python programs to interface to Samba's internals, and many tools and internal workings of the DC code is now implemented in python.

==Known Issues===
===============================

------------------------

* Replication of DNS data from one AD server to another may not work. The DNS data used by the internal DNS server and bind9_dlz is stored in an application partition in our directory. The replication of this partition is not yet reliable.
* Replication may fail on FreeBSD due to getaddrinfo() rejecting names containing _.  A workaround will be in a future release.
* samba_upgradeprovision should not be run when upgrading to this release from a recent release.  No important database format changes have been made since alpha16.
* Installation on systems without a system iconv (and developer headers at compile time) is known to cause errors when dealing with non-ASCII characters.
* Domain member support in the 'samba' binary is in its infancy, and is not comparable to the support found in winbindd.  As such, do not use the 'samba' binary (provided for the AD server) on a member server.
* There is no NetBIOS browsing support (network neighbourhood) available for the AD domain controller.  (Support in nmbd and smbd for classic domains and member/standalone servers is unchanged).
* Clock Synchronisation is critical.  Many 'wrong password' errors are actually due to Kerberos objecting to a clock skew between client and server.  (The NTP work in the previous alphas are partly to assist with this problem).
* The DRS replication code may fail.  Please contact the team if you experience issues with DRS replication, as we have fixed many issues here in response to feedback from our production users.
* Linux inotify will now only be supported on systems where glibc also supports it (for details, please refer to [https://bugzilla.samba.org/show_bug.cgi?id=8850 bug #8850]).

==Upgrading===
===============================

------------------------

Users upgrading from Samba 3.x domain controllers and wanting to use Samba 4.0 as an AD DC should use the 'samba-tool domain classicupgrade' command.  See the wiki for more details `Migrating_a_Samba_NT4_Domain_to_Samba_AD_(Classic_Upgrade)|Migrating a Samba NT4 Domain to Samba AD (Classic Upgrade)`.

Users upgrading from Samba 4.0 alpha and beta releases since alpha15 should run 'samba-tool dbcheck --cross-ncs --fix' before re-starting Samba.  Users upgrading from earlier alpha releases should contact the team for advice.

Users upgrading an AD DC from any previous release should run 'samba-tool ntacl sysvolreset' to re-sync ACLs on the sysvol share with those matching the GPOs in LDAP and the defaults from an initial provision.  This will set an underlying POSIX ACL if required (eg not using the NTVFS file server).

If you used the BIND9_FLATFILE or BIND9_DLZ features, you'll have to add '-dns' to the 'server services' option, as the internal dns server (SAMBA_INTERNAL) is the default now.

==Supported features===
===============================

------------------------

A whitepaper of currently (un-)supported features is available on the wiki:

`Samba_4.0_Whitepaper`

==smb.conf changes===
===============================

------------------------

   Parameter Name    		Description
   --------------			-----------
   acl compatibility			Removed
   allow dns updates			New
   announce as				Removed
   announce version			Removed
   cldap port				New
   client max protocol			New
   client min protocol			New
   client signing			Changed default
   dcerpc endpoint servers		New
   dgram port				New
   directory security mask		Removed
   display charset			Removed
   dns forwarder			New
   dns update command			New
   force security mode			Removed
   force directory security mode	Removed
   homedir map				Changed default
   kernel oplocks			Changed default
   kernel share modes			New
   kpasswd port				New
   krb5 port				New
   nbt client socket address		New
   nbt port				New
   nsupdate command			New
   ntp signd socket directory		New
   ntvfs handler			New
   paranoid server security		Removed
   pid directory			New
   printer admin			Removed
   rndc command 			New
   rpc big endian			New
   samba kcc command			New
   security mask			Removed
   send spnego principal		Removed
   server max protocol			New
   server min protocol			New
   server role				New
   server services			New
   server signing			Changed default
   share backend			New
   share modes				Removed
   smb2 max read			Changed default
   smb2 max write			Changed default
   smb2 max trans			Changed default
   socket address			Removed
   spn update command			New
   time offset				Removed
   tls cafile				New
   tls certfile				New
   tls crlfile				New
   tls dh params file			New
   tls enabled				New
   tls keyfile				New
   unicode				New
   web port				New
   winbindd privileged socket directory	New
   winbind sealed pipes			New
   winbindd socket directory		New
</onlyinclude>

==CHANGES SINCE 4.0.0rc6===
===============================

------------------------

* Michael Adam <obnox at samba.org>
* * BUG 9414: Honor password complexity settings.
* * BUG 9456: developer-build: Fix panic when acl_xattr fails with access denied.
* * BUG 9457: Fix "map username script" with "security=ads" and Winbind.

* Jeremy Allison <jra at samba.org>
* * BUG 9462: Users can not be given write permissions any more by default.
* Andrew Bartlett <abartlet at samba.org>
* * BUG 9415: Install SWAT *.msg files with waf.
* Alexander Bokovoy <ab at samba.org>
* * BUG 9479: Support FIPS mode when building Samba.
* Günther Deschner <gd at samba.org>
* * BUG 9438: Fix netr_ServerPasswordSet2, netr_LogonSamLogon with netlogon AES.
* Tsukasa Hamano <hamano at osstech.co.jp>
* * BUG 9471: Fix SEGV when using second vfs module.
* Stefan Metzmacher <metze at samba.org>
* * BUG 9414: Honor password complexity settings.
* * BUG 9470: Fix MMC crashes.
* * BUG 9481: Fix ACL on "cn=partitions,cn=configuration".
* Andreas Schneider <asn at samba.org>
* * BUG 9459: Install manpages only if we install the target.
* Richard Sharpe <realrichardsharpe at gmail.com>
* * BUG 9460: Respond correctly to FILE_STREAM_INFO requests.

----
`Category:Release Notes`