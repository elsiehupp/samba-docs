Samba 4.4 Features added/changed
    <namespace>0</namespace>
<last_edited>2019-09-17T22:01:59Z</last_edited>
<last_editor>Fraz</last_editor>


Samba 4.4 is `Samba_Release_Planning#Discontinued_.28End_of_Life.29|**Discontinued (End of Life)**`.

Samba 4.4.16
------------------------

* Release Notes for Samba 4.4.16
* September 20, 2017

===============================
This is a security release in order to address the following defects:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12150 CVE-2017-12150] SMB1/2/3 connections may not require signing where they should
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12151 CVE-2017-12151] SMB3 connections don't keep encryption across DFS redirects
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12163 CVE-2017-12163] Server memory information leak over SMB1

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12150 CVE-2017-12150]:
* A man in the middle attack may hijack client connections.
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12151 CVE-2017-12151]:
* A man in the middle attack can read and may alter confidential documents transferred via a client connection, which are reached via DFS redirect when the original connection used SMB3.
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12163 CVE-2017-12163]:
* Client with write access to a share can cause server memory contents to be written into a file or printer.

For more details and workarounds, please see the security advisories:

* https://www.samba.org/samba/security/CVE-2017-12150.html
* https://www.samba.org/samba/security/CVE-2017-12151.html
* https://www.samba.org/samba/security/CVE-2017-12163.html

===============================
Changes since 4.4.15:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12836 BUG #12836]: s3: smbd: Fix a read after free if a chained SMB1 call goes async.
* * [https://bugzilla.samba.org/show_bug.cgi?id=13020 BUG #13020]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12163 CVE-2017-12163]: s3:smbd: Prevent client short SMB1 write from writing server memory to file.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12885 BUG #12885]: s3/smbd: Let non_widelink_open() chdir() to directories directly.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12996 BUG #12996]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12151 CVE-2017-12151]: Keep required encryption across SMB3 dfs redirects.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12997 BUG #12997]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12150 CVE-2017-12150]: Some code path don't enforce smb signing when they should.

 https://www.samba.org/samba/history/samba-4.4.16.html

Samba 4.4.15
------------------------

* Release Notes for Samba 4.4.15
* July 12, 2017

===============================
This is a security release in order to address the following defect:
===============================

------------------------

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-11103 CVE-2017-11103] (Orpheus' Lyre mutual authentication validation bypass)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-11103 CVE-2017-11103] (Heimdal):
* All versions of Samba from 4.0.0 onwards using embedded Heimdal Kerberos are vulnerable to a man-in-the-middle attack impersonating a trusted server, who may gain elevated access to the domain by returning malicious replication or authorization data.

* Samba binaries built against MIT Kerberos are not vulnerable.

===============================
Changes since 4.4.14
===============================

------------------------

*  Jeffrey Altman <jaltman@secure-endpoints.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12894 BUG #12894]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-11103 CVE-2017-11103]: Orpheus' Lyre KDC-REP service name validation

 https://www.samba.org/samba/history/samba-4.4.15.html

Samba 4.4.14
------------------------

* Release Notes for Samba 4.4.14
* May 24, 2017

===============================
This is a security release in order to address the following defect:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CCVE-2017-7494 CVE-2017-7494] (Remote code execution from a writable share)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CCVE-2017-7494 CVE-2017-7494]:
* All versions of Samba from 3.5.0 onwards are vulnerable to a remote code execution vulnerability, allowing a malicious client to upload a shared library to a writable share, and then cause the server to load and execute it.

===============================
Mitigation from Redhat
===============================

------------------------

* https://access.redhat.com/security/cve/CVE-2017-7494

Any of the following:

# SELinux is enabled by default and our default policy prevents loading of modules from outside of samba's module directories and therefore blocks the exploit
# Mount the filessytem which is used by samba for its writeable share, using "noexec" option.
# Add the parameter:
* ::nt pipe support = no
* :to the [global] section of your smb.conf and restart smbd. This prevents clients from accessing any named pipe endpoints. Note this can disable some expected functionality for Windows clients.

===============================
Changes since 4.4.13:
===============================

------------------------

*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12780 BUG #12780]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CCVE-2017-7494 CVE-2017-7494]: Avoid remote code execution from a writable share.

 https://www.samba.org/samba/history/samba-4.4.14.html

Samba 4.4.13
------------------------

* Release Notes for Samba 4.4.13
* March 31, 2017

This is a bug fix release to address a regression introduced by the security fixes for CVE-2017-2619 (Symlink race allows access outside share definition).

Please see [https://bugzilla.samba.org/show_bug.cgi?id=12721 BUG #12721] for details.

===============================
Changes since 4.4.12:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12721 BUG #12721]: Fix regression with "follow symlinks = no".

 https://www.samba.org/samba/history/samba-4.4.13.html

Samba 4.4.12
------------------------

* Release Notes for Samba 4.4.12
* March 23, 2017

===============================
This is a security release in order to address the following defect: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-2619 CVE-2017-2619]
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-2619 CVE-2017-2619] (Symlink race allows access outside share definition)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-2619 CVE-2017-2619]:
* All versions of Samba prior to 4.6.1, 4.5.7, 4.4.11 are vulnerable to a malicious client using a symlink race to allow access to areas of the server file system not exported under the share definition.

* Samba uses the realpath() system call to ensure when a client requests access to a pathname that it is under the exported share path on the server file system.

* Clients that have write access to the exported part of the file system via SMB1 unix extensions or NFS to create symlinks can race the server by renaming a realpath() checked path and then creating a symlink. If the client wins the race it can cause the server to access the new symlink target after the exported share path check has been done. This new symlink target can point to anywhere on the server file system.

* This is a difficult race to win, but theoretically possible. Note that the proof of concept code supplied wins the race reliably only when the server is slowed down using the strace utility running on the server. Exploitation of this bug has not been seen in the wild.

===============================
Changes since 4.4.11:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12496 BUG #12496]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-2619 CVE-2017-2619]: Symlink race permits opening files outside share directory.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12496 BUG #12496]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-2619 CVE-2017-2619]: Symlink race permits opening files outside share directory.

 https://www.samba.org/samba/history/samba-4.4.12.html

Samba 4.4.11
------------------------

* Release Notes for Samba 4.4.11
* March 16, 2017

This is the latest stable release of Samba 4.4. Please note that this will very likely be the last maintenance release of the Samba 4.4 release branch.

===============================
Changes since 4.4.10:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12608 BUG #12608]: s3: smbd: Restart reading the incoming SMB2 fd when the send queue is drained.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=7537 BUG #7537]: s3/smbd: Fix deferred open with streams and kernel oplocks.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12604 BUG #12604]: vfs_fruit: Enabling AAPL extensions must be a global switch.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12615 BUG #12615]: manpages/vfs_fruit: Document global options.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12610 BUG #12610]: smbd: Do an early exit on negprot failure.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12605 BUG #12605]: Winbindd endless looping in forest trust scan 
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12686 BUG #12686]: Fix build with newer glibc.

 https://www.samba.org/samba/history/samba-4.4.11.html

Samba 4.4.10
------------------------

* Release Notes for Samba 4.4.10
* March 1, 2017

===============================
This is the latest stable release of Samba 4.4. Please note that this will likely be the last maintenance release of the Samba 4.4 release branch.
===============================

------------------------

===============================
Major enhancements in Samba 4.4.10 include:
===============================

------------------------

*  Domain join broken under certain circumstances after winbindd changed the trust password ([https://bugzilla.samba.org/show_bug.cgi?id=12262 BUG #12262]).

A new parameter "include system krb5 conf" has been added ([https://bugzilla.samba.org/show_bug.cgi?id=12441 BUG #12441]). Please see the man page for details.

===============================
Changes since 4.4.9:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12479 BUG #12479]: s3: libsmb: Add cli_smb2_ftruncate(), plumb into cli_ftruncate().
* * [https://bugzilla.samba.org/show_bug.cgi?id=12499 BUG #12499]: s3: vfs: dirsort doesn't handle opendir of "." correctly.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12572 BUG #12572]: s3: smbd: Don't loop infinitely on bad-symlink resolution.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12531 BUG #12531]: Make vfs_shadow_copy2 cope with server changing directories.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12546 BUG #12546]: s3: VFS: vfs_streams_xattr.c: Make streams_xattr_open() store the same path as streams_xattr_recheck().
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12536 BUG #12536]: s3/smbd: Check for invalid access_mask smbd_calculate_access_mask().
* * [https://bugzilla.samba.org/show_bug.cgi?id=12541 BUG #12541]: vfs_fruit: checks wrong AAPL config state and so always uses readdirattr.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12545 BUG #12545]: s3/rpc_server/mdssvc: Add attribute "kMDItemContentType".
* * [https://bugzilla.samba.org/show_bug.cgi?id=12591 BUG #12591]: vfs_streams_xattr: Use fsp, not base_fsp.
*  David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12144 BUG #12144]: smbd/ioctl: Match WS2016 ReFS set compression behaviour.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12580 BUG #12580]: ctdb-common: Fix use-after-free error in comm_fd_handler().
*  Björn Jacke <bj@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=2210 BUG #2210]: pam: Map more NT password errors to PAM errors.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12535 BUG #12535]: vfs_default: Unlock the right file in copy chunk.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12509 BUG #12509]: messaging: Fix dead but not cleaned-up-yet destination sockets.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12551 BUG #12551]: smbd: Fix "map acl inherit" = yes.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11830 BUG #11830]: Domain member cannot resolve trusted domains' users.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12262 BUG #12262]: Domain join broken under certain circumstances after winbindd changed the trust password.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12480 BUG #12480]: 'kinit' succeeded, but ads_sasl_spnego_gensec_bind(KRB5) failed: An internal error occurred (with MIT krb5).
* * [https://bugzilla.samba.org/show_bug.cgi?id=12540 BUG #12540]: s3:smbd: allow "server min protocol = SMB3_00" to go via "SMB 2.???" negprot.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12581 BUG #12581]: 'smbclient' fails on bad endianess when listing shares from Solaris kernel SMB server on SPARC.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12585 BUG #12585]: librpc/rpc: fix regression in NT_STATUS_RPC_ENUM_VALUE_OUT_OF_RANGE error mapping.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12586 BUG #12586]: netlogon_creds_cli_LogonSamLogon doesn't work without netr_LogonSamLogonEx.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12587 BUG #12587]: Fix winbindd child segfaults on connect to an NT4 domain.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12588 BUG #12588]: cm_prepare_connection may return NT_STATUS_OK without a valid connection.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12598 BUG #12598]: winbindd (as member) requires kerberos against trusted ad domain, while it shouldn't.
* Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12441 BUG #12441]: s3:libads: Include system /etc/krb5.conf if we use MIT Kerberos.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12571 BUG #12571]: s3-vfs: Only walk the directory once in open_and_sort_dir().
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12589 BUG #12589]: CTDB statd-callout does not cause grace period when CTDB_NFS_CALLOUT="".
*  Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12529 BUG #12529]: waf: Backport finding of pkg-config.

 https://www.samba.org/samba/history/samba-4.4.10.html

Samba 4.4.9
------------------------

* Release Notes for Samba 4.4.9
* January 2, 2017

===============================
This is the latest stable release of Samba 4.4.
===============================

------------------------

===============================
Changes since 4.4.8:
===============================

------------------------

*  Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12404 BUG #12404]: vfs:glusterfs: Preallocate result for glfs_realpath.
*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12299 BUG #12299]: Fix unitialized variable warnings in smbd open.c and close.c.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12387 BUG #12387]: s3: vfs: streams_depot. Use conn->connectpath not conn->cwd.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12436 BUG #12436]: s3/smbd: Fix the last resort check that sets the file type attribute.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12395 BUG #12395]: build: Fix build with perl on debian sid.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12412 BUG #12412]: Fix typo in vfs_fruit: fruit:ressource -> fruit:resource.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11197 BUG #11197]: spoolss: Use correct values for secdesc and devmode pointers.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12366 BUG #12366]: provision: Add support for BIND 9.11.x.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12392 BUG #12392]: ctdb-locking: Reset real-time priority in lock helper.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12434 BUG #12434]: ctdb-recovery: Avoid NULL dereference in failure case.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10297 BUG #10297]: s3:smbd: Only pass UCF_PREP_CREATEFILE to filename_convert() if we may create a new file.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12471 BUG #12471]: Fix build with MIT Kerberos.
*  Mathieu Parent <math.parent@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12371 BUG #12371]: ctdb-scripts: Fix Debian init in Samba eventscript.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12183 BUG #12183]: s3-printing: Correctly encode CUPS printer URIs.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12195 BUG #12195]: s3-printing: Allow printer names longer than 16 chars.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12269 BUG #12269]: nss_wins: Fix errno values for HOST_NOT_FOUND.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12405 BUG #12405]: s3-winbind: Do not return NO_MEMORY if we have an empty user list.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12415 BUG #12415]: s3:spoolss: Add support for COPY_FROM_DIRECTORY in AddPrinterDriverEx.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12104 BUG #12104]: ctdb-packaging: Move CTDB tests to /usr/local/share/ctdb/tests/.
*  Ralph Wuerthner <ralph.wuerthner@de.ibm.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12372 BUG #12372]: ctdb-conn: Add missing variable initialization.

 https://www.samba.org/samba/history/samba-4.4.9.html

Samba 4.4.8
------------------------

* Release Notes for Samba 4.4.8
* December 19, 2016

===============================
This is a security release in order to address the following defects:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2123 CVE-2016-2123] (Samba NDR Parsing ndr_pull_dnsp_name Heap-based Buffer Overflow Remote Code Execution Vulnerability).
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2125 CVE-2016-2125] (Unconditional privilege delegation to Kerberos servers in trusted realms).
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2126 CVE-2016-2126] (Flaws in Kerberos PAC validation can trigger privilege elevation).

Details=
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2123 CVE-2016-2123]:
* The Samba routine ndr_pull_dnsp_name contains an integer wrap problem, leading to an attacker-controlled memory overwrite. ndr_pull_dnsp_name parses data from the Samba Active Directory ldb database.  Any user who can write to the dnsRecord attribute over LDAP can trigger this memory corruption.

* By default, all authenticated LDAP users can write to the dnsRecord attribute on new DNS objects. This makes the defect a remote privilege escalation.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2125 CVE-2016-2125]
* Samba client code always requests a forwardable ticket when using Kerberos authentication. This means the target server, which must be in the current or trusted domain/realm, is given a valid general purpose Kerberos "Ticket Granting Ticket" (TGT), which can be used to fully impersonate the authenticated user or service.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2126 CVE-2016-2126]
* A remote, authenticated, attacker can cause the winbindd process to crash using a legitimate Kerberos ticket due to incorrect handling of the arcfour-hmac-md5 PAC checksum.

* A local service with access to the winbindd privileged pipe can cause winbindd to cache elevated access permissions.

===============================
Changes since 4.4.8:
===============================

------------------------

*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12409 BUG #12409]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2123 CVE-2016-2123]: Fix DNS vuln ZDI-CAN-3995.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12445 BUG #12445]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2125 CVE-2016-2125]: Don't send delegated credentials to all servers.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12446 BUG #12446]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2126 CVE-2016-2126]: auth/kerberos: Only allow known checksum types in check_pac_checksum().

 https://www.samba.org/samba/history/samba-4.4.8.html

Samba 4.4.7
------------------------

* Release Notes for Samba 4.4.7
* October 26, 2016

===============================
This is the latest stable release of Samba 4.4.
===============================

------------------------

Major enhancements in Samba 4.4.7 include:

*  Let winbindd discard expired kerberos tickets when built against (internal) heimdal ([https://bugzilla.samba.org/show_bug.cgi?id=12369 BUG #12369]).
*  REGRESSION: smbd segfaults on startup, tevent context being freed ([https://bugzilla.samba.org/show_bug.cgi?id=12283 BUG #12283]).

===============================
Changes since 4.4.6:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11259 BUG #11259]: smbd contacts a domain controller for each session.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12283 BUG #12283]: REGRESSION: smbd segfaults on startup, tevent context being freed.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12291 BUG #12291]: source3/lib/msghdr: Fix syntax error before or at: ;.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12381 BUG #12381]: s3: cldap: cldap_multi_netlogon_send() fails with one bad IPv6 address.
*  Christian Ambach <ambi@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=9945 BUG #9945]: Setting specific logger levels in smb.conf makes 'samba-tool drs showrepl' crash.
*  Björn Baumbach <bb@sernet.de>
* * [https://bugzilla.samba.org/show_bug.cgi?id=8618 BUG #8618]: s3-printing: Fix migrate printer code.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12261 BUG #12261]: s3/smbd: Set FILE_ATTRIBUTE_DIRECTORY as necessary.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12285 BUG #12285]: "DriverVersion" registry backend parsing incorrect in spoolss.
*  David Disseldorp <ddiss@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12144 BUG #12144]: smbd/ioctl: Match WS2016 ReFS get compression behaviour.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12287 BUG #12287]: CTDB PID file handling is too weak.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12045 BUG #12045]: gencache: Bail out of stabilize if we can not get the allrecord lock.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12283 BUG #12283]: glusterfs: Avoid tevent_internal.h.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12374 BUG #12374]: spoolss: Fix caching of printername->sharename.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12283 BUG #12283]: REGRESSION: smbd segfaults on startup, tevent context being freed.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12369 BUG #12369]: Let winbindd discard expired kerberos tickets when built against (internal) heimdal.
*  Noel Power <noel.power@suse.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12298 BUG #12298]: s3/winbindd: Using default domain with user@domain.com format fails.
*  Jose A. Rivera <jarrpa@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12362 BUG #12362]: ctdb-scripts: Avoid dividing by zero in memory calculation.
*  Anoop C S <anoopcs@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12377 BUG #12377]: vfs_glusterfs: Fix a memory leak in connect path.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12269 BUG #12269]: nss_wins has incorrect function definitions for gethostbyname*.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12276 BUG #12276]: s3-lib: Fix %G substitution in AD member environment.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12364 BUG #12364]: s3-utils: Fix loading smb.conf in smbcquotas.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12287 BUG #12287]: CTDB PID file handling is too weak.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12362 BUG #12362]: ctdb-scripts: Fix incorrect variable reference.

 https://www.samba.org/samba/history/samba-4.4.7.html

Samba 4.4.6
------------------------

* Release Notes for Samba 4.4.6
* September 22, 2016

===============================
This is the latest stable release of Samba 4.4.
===============================

------------------------

===============================
Changes since 4.4.5:
===============================

------------------------

*  Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11977 BUG #11977]: libnet: Ignore realm setting for domain security joins to AD domains if 'winbind rpc only = true'.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12155 BUG #12155]: idmap: Centrally check that unix IDs returned by the idmap backends are in range.

o  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11838 BUG #11838]:  s4: ldb: Ignore case of "range" in sscanf as we've already checked for its presence.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11845 BUG #11845]: Incorrect bytecount in ReadAndX smb1 response.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11955 BUG #11955]: lib: Fix uninitialized read in msghdr_copy.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11959 BUG #11959]: s3: krb5: keytab - The done label can be jumped to with context == NULL.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11986 BUG #11986]: s3: libsmb: Correctly trim a trailing \\ character in cli_smb2_create_fnum_send() when passing a pathname to SMB2 create.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12021 BUG #12021]: Fix smbd crash (Signal 4) on File Delete.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12135 BUG #12135]: libgpo: Correctly use the 'server' parameter after parsing it out of the GPO path.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12139 BUG #12139]: s3: oplock: Fix race condition when closing an oplocked file.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12272 BUG #12272]: Fix messaging subsystem crash.
*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11750 BUG #11750]: gcc6 fails to build internal heimdal.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11991 BUG #11991]: build: Build less of Samba when building '--without-ntvfs-fileserver'.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12026 BUG #12026]: build: Always build eventlog6. This is not a duplicate of eventlog.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12154 BUG #12154]: ldb-samba: Add "secret" as a value to hide in LDIF files.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12178 BUG #12178]: dbcheck: Abandon dbcheck if we get an error during a transaction.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10008 BUG #10008]: dbwrap_ctdb: Treat empty records in ltdb as non-existing.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11520 BUG #11520]: Fix DNS secure updates.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11961 BUG #11961]: idmap_autorid allocates ids for unknown SIDs from other backends.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11992 BUG #11992]: s3/smbd: Only use stored dos attributes for open_match_attributes() check.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12005 BUG #12005]: smbd: Ignore ctdb tombstone records in fetch_share_mode_unlocked_parser().
* * [https://bugzilla.samba.org/show_bug.cgi?id=12016 BUG #12016]: cleanupd terminates main smbd on exit.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12028 BUG #12028]: vfs_acl_xattr: Objects without NT ACL xattr.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12105 BUG #12105]: async_req: Make async_connect_send() "reentrant".
* * [https://bugzilla.samba.org/show_bug.cgi?id=12177 BUG #12177]: vfs_acl_common: Fix unexpected synthesized default ACL from vfs_acl_xattr.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12181 BUG #12181]: vfs_acl_xattr|tdb: Enforced settings when "ignore system acls = yes".
*  Alexander Bokovoy <ab@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11975 BUG #11975]: libnet_join: use sitename if it was set by pre-join detection.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11977 BUG #11977]: s3-libnet: Print error string even on successful completion of libnetjoin.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11940 BUG #11940]: CTDB fails to recover large database.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11941 BUG #11941]: CTDB does not ban misbehaving nodes during recovery.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11946 BUG #11946] : Samba and CTDB packages both have tevent-unix-util dependency.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11956 BUG #11956]: ctdb-recoverd: Avoid duplicate recoverd event in parallel recovery.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12158 BUG #12158]: CTDB release IP fixes.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12259 BUG #12259]: ctdb-protocol: Fix marshalling for GET_DB_SEQNUM control request.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12271 BUG #12271]: CTDB recovery does not terminate if no node is banned due to failure.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12275 BUG #12275]12275: ctdb-recovery-helper: Add missing initialisation of ban_credits.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12268 BUG #12268]: smbd: Reset O_NONBLOCK on open files.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11948 BUG #11948]: dcerpc.idl: Remove unused DCERPC_NCACN_PAYLOAD_MAX_SIZE.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11982 BUG #11982]: Invalid auth_pad_length is not ignored for BIND_* and ALTER_* pdus.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11994 BUG #11994]: gensec/spnego: Work around missing server mechListMIC in SMB servers.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12007 BUG #12007]: libads: Ensure the right ccache is used during spnego bind.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12018 BUG #12018]: python/remove_dc: Handle dnsNode objects without dnsRecord attribute.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12129 BUG #12129]: samba-tool/ldapcmp: Ignore differences of whenChanged.
*  Marc Muehlfeld <mmuehlfeld@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12023 BUG #12023]: man: Wrong option for parameter ldap ssl in smb.conf man page.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11936 BUG #11936]: libutil: Support systemd 230.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11999 BUG #11999]: s3-winbind: Fix memory leak with each cached credential login.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12104 BUG #12104]: ctdb-waf: Move ctdb tests to libexec directory.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12175 BUG #12175]: s3-util: Fix asking for username and password in smbget.
*  Martin Schwenke <martin@meltin.net>
* * BUG 12104: ctdb-packaging: Move ctdb tests to libexec directory.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12110 BUG #12110]: ctdb-daemon: Fix several Coverity IDs.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12158 BUG #12158]: CTDB release IP fixes.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12161 BUG #12161]: Fix CTDB cumulative takeover timeout.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12180 BUG #12180]: Fix CTDB crashes running eventscripts.
*  Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=12006 BUG #12006]: auth: Fix a memory leak in gssapi_get_session_key().
* * [https://bugzilla.samba.org/show_bug.cgi?id=12145 BUG #12145]: smbd: If inherit owner is enabled, the free disk on a folder should take the owner's quota into account.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12149 BUG #12149]: smbd: Allow reading files based on FILE_EXECUTE access right.
* * [https://bugzilla.samba.org/show_bug.cgi?id=12172 BUG #12172]: Fix access of snapshot folders via SMB1.
*Lorinczy Zsigmond <lzsiga@freemail.c3.hu>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11947 BUG #11947]: lib: replace: snprintf: Fix length calculation for hex/octal 64-bit values.

 https://www.samba.org/samba/history/samba-4.4.6.html

Samba 4.4.5
------------------------

* Release Notes for Samba 4.4.5
* July 07, 2016

===============================
This is a security release in order to address the following defect:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2119 CVE-2016-2119] (Client side SMB2/3 required signing can be downgraded)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2119 CVE-2016-2119]:
It's possible for an attacker to downgrade the required signing for an SMB2/3 client connection, by injecting the SMB2_SESSION_FLAG_IS_GUEST or SMB2_SESSION_FLAG_IS_NULL flags.

This means that the attacker can impersonate a server being connected to by Samba, and return malicious results.

The primary concern is with winbindd, as it uses DCERPC over SMB2 when talking to domain controllers as a member server, and trusted domains as a domain controller.  These DCE/RPC connections were intended to protected by the combination of "client ipc signing" and "client ipc max protocol" in their effective default settings ("mandatory" and "SMB3_11").

Additionally, management tools like net, samba-tool and rpcclient use DCERPC over SMB2/3 connections.

By default, other tools in Samba are unprotected, but rarely they are configured to use smb signing, via the "client signing" parameter (the default is "if_required").  Even more rarely the "client max protocol" is set to SMB2, rather than the NT1 default.

If both these conditions are met, then this issue would also apply to these other tools, including command line tools like smbcacls, smbcquota, smbclient, smbget and applications using libsmbclient.

===============================
Changes since 4.4.4:
===============================

------------------------

*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11860 BUG #11860]: [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2119 CVE-2016-2119]: Fix client side SMB2 signing downgrade.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11948 BUG #11948]: Total dcerpc response payload more than 0x400000.

 https://www.samba.org/samba/history/samba-4.4.5.html

Samba 4.4.4
------------------------

* Release Notes for Samba 4.4.4
* June 7, 2016

===============================
This is the latest stable release of Samba 4.4.
===============================

------------------------

===============================
Changes since 4.4.3:
===============================

------------------------

*  Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11809 BUG #11809]BUG 11809: SMB3 multichannel: Add implementation of missing channel sequence number verification.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11919 BUG #11919]BUG 11919: smbd:close: Only remove kernel share modes if they had been taken at open.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11930 BUG #11930]BUG 11930: notifyd: Prevent NULL deref segfault in notifyd_peer_destructor.
*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10618 BUG #10618]BUG 10618: s3: auth: Move the declaration of struct dom_sid tmp_sid to function level scope.
*  Christian Ambach <ambi@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=10796 BUG #10796]: s3:rpcclient: Make '--pw-nt-hash' option work.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11354 BUG #11354]: s3:libsmb/clifile: Use correct value for MaxParameterCount for setting EAs.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11438 BUG #11438]: Fix case sensitivity issues over SMB2 or above.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=1703 BUG #1703]: s3:libnet:libnet_join: Add netbios aliases as SPNs.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11721 BUG #11721]: vfs_fruit: Add an option that allows disabling POSIX rename behaviour.
*  Alexander Bokovoy <ab@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11936 BUG #11936]: s3-smbd: Support systemd 230.
*  Ira Cooper <ira@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11907 BUG #11907]: source3: Honor the core soft limit of the OS.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11809 BUG #11809]: SMB3 multichannel: Add implementation of missing channel sequence number verification.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11864 BUG #11864]: s3:client:smbspool_krb5_wrapper: Fix the non clearenv build.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11906 BUG #11906]: s3-kerberos: Avoid entering a password change dialogue also when using MIT.
*  Robin Hack <hack.robin@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11890 BUG #11890]: ldb-samba/ldb_matching_rules: Fix CID 1349424 - Uninitialized pointer read.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11844 BUG #11844]: dbwrap_ctdb: Fix ENOENT->NT_STATUS_NOT_FOUND.
*  Robin McCorkell <robin@mccorkell.me.uk>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11276 BUG #11276]: Correctly set cli->raw_status for libsmbclient in SMB2 code.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11910 BUG #11910]: s3:smbd: Fix anonymous authentication if signing is mandatory.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11912 BUG #11912]: libcli/auth: Let msrpc_parse() return talloc'ed empty strings.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11914 BUG #11914]: Fix NTLM Authentication issue with squid.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11927 BUG #11927]: s3:rpcclient: make use of SMB_SIGNING_IPC_DEFAULT.
*  Luca Olivetti <luca@wetron.es>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11530 BUG #11530]: pdb: Fix segfault in pdb_ldap for missing gecos.
*  Rowland Penny <rpenny@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11613 BUG #11613]: Allow 'samba-tool fsmo' to cope with empty or missing fsmo roles.
*  Anoop C S <anoopcs@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11907 BUG #11907]: packaging: Set default limit for core file size in service files.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11922 BUG #11922]: s3-net: Convert the key_name to UTF8 during migration.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11935 BUG #11935]: s3-smbspool: Log to stderr.
*  Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11900 BUG #11900]: heimdal: Encode/decode kvno as signed integer.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11931 BUG #11931]: s3-quotas: Fix sysquotas_4B quota fetching for BSD.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11937 BUG #11937]: smbd: dfree: Ignore quota if not enforced.
*  Raghavendra Talur <rtalur@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11907 BUG #11907]: init: Set core file size to unlimited by default.
*  Hemanth Thummala <hemanth.thummala@nutanix.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11934 BUG #11934]: Fix memory leak in share mode locking.

 https://www.samba.org/samba/history/samba-4.4.4.html

Samba 4.4.3
------------------------

* Release Notes for Samba 4.4.3
* May 2, 2016

===============================
This is the latest stable release of Samba 4.4.
===============================

------------------------

This release fixes some regressions introduced by the last security fixes. Please see bug https://bugzilla.samba.org/show_bug.cgi?id=11849 for a list of bugs addressing these regressions and more information.

===============================
Changes since 4.4.2:
===============================

------------------------

*  Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11786 BUG #11786]: idmap_hash: Only allow the hash module for default idmap config.
*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11822 BUG #11822]: s3: libsmb: Fix error where short name length was read as 2 bytes, should be 1.
*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11789 BUG #11789]: Fix returning of ldb.MessageElement.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11855 BUG #11855]: cleanupd: Restart as needed.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11786 BUG #11786]: s3:winbindd:idmap: check loadparm in domain_has_idmap_config() helper as well.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11789 BUG #11789]: libsmb/pysmb: Add pytalloc-util dependency to fix the build.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11786 BUG #11786]: winbind: Fix CID 1357100: Unchecked return value.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11816 BUG #11816]: nwrap: Fix the build on Solaris.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11827 BUG #11827]: vfs_catia: Fix memleak. 
* * [https://bugzilla.samba.org/show_bug.cgi?id=11878 BUG #11878]: smbd: Avoid large reads beyond EOF.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11789 BUG #11789]: s3:wscript: pylibsmb depends on pycredentials.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11841 BUG #11841]: Fix NT_STATUS_ACCESS_DENIED when accessing Windows public share.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11847 BUG #11847]: Only validate MIC if "map to guest" is not being used.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11849 BUG #11849]: auth/ntlmssp: Add ntlmssp_{client,server}:force_old_spnego option for testing.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11850 BUG #11850]: NetAPP SMB servers don't negotiate NTLM _SIGN.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11858 BUG #11858]: Allow anonymous smb connections.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11870 BUG #11870]: Fix ads_sasl_spnego_gensec_bind(KRB5).
* * [https://bugzilla.samba.org/show_bug.cgi?id=11872 BUG #11872]: Fix 'wbinfo -u' and 'net ads search'.
*  Tom Mortensen <tomm@lime-technology.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11875 BUG #11875]: nss_wins: Fix the hostent setup.
*  Garming Sam <garming@catalyst.net.nz>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11789 BUG #11789]: build: Mark explicit dependencies on pytalloc-util.
*  Partha Sarathi <partha@exablox.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11819 BUG #11819]: Fix the smb2_setinfo to handle FS info types and FSQUOTA infolevel.
*  Jorge Schrauwen <sjorge@blackdot.be>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11816 BUG #11816]: configure: Don't check for inotify on illumos.
*  Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11806 BUG #11806]: vfs_acl_common: Avoid setting POSIX ACLs if "ignore system acls" is set.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11815 BUG #11815]: smbcquotas: print "NO LIMIT" only if returned quota value is 0.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11852 BUG #11852]: libads: Record session expiry for spnego sasl binds.
*  Hemanth Thummala <hemanth.thummala@nutanix.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11840 BUG #11840]: Mask general purpose signals for notifyd.

 https://www.samba.org/samba/history/samba-4.4.3.html

Samba 4.4.2
------------------------

* Release Notes for Samba 4.4.2
* April 12, 2016

This is a security release containing one additional regression fix for the security release 4.2.10.

This fixes a regression that prevents things like 'net ads join' from working against a Windows 2003 domain.

===============================
Changes since 4.4.1:
===============================

------------------------

*  Stefan Metzmacher <metze@samba.org>
* *  [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016

Samba 4.4.1
------------------------

* Release Notes for Samba 4.4.1
* April 12, 2016

===============================
This is a security release in order to address the following CVEs:
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5370 CVE-2015-5370] (Multiple errors in DCE-RPC code)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2110 CVE-2016-2110] (Man in the middle attacks possible with NTLM )
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2111 CVE-2016-2111] (NETLOGON Spoofing Vulnerability)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2112 CVE-2016-2112] (LDAP client and server don't enforce integrity)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2113 CVE-2016-2113] (Missing TLS certificate validation)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2114 CVE-2016-2114] ("server signing = mandatory" not enforced)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2115 CVE-2016-2115] (SMB IPC traffic is not integrity protected)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2118 CVE-2016-2118] (SAMR and LSA man in the middle attacks possible)

The number of changes are rather huge for a security release, compared to typical security releases.

Given the number of problems and the fact that they are all related to man in the middle attacks we decided to fix them all at once instead of splitting them.

In order to prevent the man in the middle attacks it was required to change the (default) behavior for some protocols. Please see the "New smb.conf options" and "Behavior changes" sections below.

===============================
Details
===============================

------------------------

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5370 CVE-2015-5370]=
===============================

------------------------

Versions of Samba from 3.6.0 to 4.4.0 inclusive are vulnerable to denial of service attacks (crashes and high cpu consumption) in the DCE-RPC client and server implementations. In addition, errors in validation of the DCE-RPC packets can lead to a downgrade of a secure connection to an insecure one.

While we think it is unlikely, there's a nonzero chance for a remote code execution attack against the client components, which are used by smbd, winbindd and tools like net, rpcclient and others. This may gain root access to the attacker.

The above applies all possible server roles Samba can operate in.

Note that versions before 3.6.0 had completely different marshalling functions for the generic DCE-RPC layer. It's quite possible that that code has similar problems!

The downgrade of a secure connection to an insecure one may allow an attacker to take control of Active Directory object handles created on a connection created from an Administrator account and re-use them on the now non-privileged connection, compromising the security of the Samba AD-DC.

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2110 CVE-2016-2110]:=
===============================

------------------------

There are several man in the middle attacks possible with NTLM  authentication.

E.g. NTLM _NEGOTIATE_SIGN and NTLM _NEGOTIATE_SEAL can be cleared by a man in the middle.

This was by protocol design in earlier Windows versions.

Windows Server 2003 RTM and Vista RTM introduced a way to protect against the trivial downgrade.

See MsvAvFlags and flag 0x00000002 in
   https://msdn.microsoft.com/en-us/library/cc236646.aspx

This new feature also implies support for a mechlistMIC when used within SPNEGO, which may prevent downgrades from other SPNEGO mechs, e.g. Kerberos, if sign or seal is finally negotiated.

The Samba implementation doesn't enforce the existence of required flags, which were requested by the application layer, e.g. LDAP or SMB1 encryption (via the unix extensions). As a result a man in the middle can take over the connection. It is also possible to misguide client and/or server to send unencrypted traffic even if encryption was explicitly requested.

LDAP (with NTLM  authentication) is used as a client by various admin tools of the Samba project, e.g. "net", "samba-tool", "ldbsearch", "ldbedit", ...

As an active directory member server LDAP is also used by the winbindd service when connecting to domain controllers.

Samba also offers an LDAP server when running as active directory domain controller.

The NTLM  authentication used by the SMB1 encryption is protected by smb signing, see CVE-2015-5296.

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2111 CVE-2016-2111]:=
===============================

------------------------

It's basically the same as CVE-2015-0005 for Windows:

* The NETLOGON service in Microsoft Windows Server 2003 SP2, Windows Server 2008 SP2 and R2 SP1, and Windows Server 2012 Gold and R2, when a Domain Controller is configured, allows remote attackers to spoof the computer name of a secure channel's endpoint, and obtain sensitive session information, by running a crafted application and leveraging the ability to sniff network traffic, aka "NETLOGON Spoofing Vulnerability".

The vulnerability in Samba is worse as it doesn't require credentials of a computer account in the domain.

This only applies to Samba running as classic primary domain controller, classic backup domain controller or active directory domain controller.

The security patches introduce a new option called "raw NTLMv2 auth" ("yes" or "no") for the [global] section in smb.conf. Samba (the smbd process) will reject client using raw NTLMv2 without using NTLM .

Note that this option also applies to Samba running as standalone server and member server.

You should also consider using "lanman auth = no" (which is already the default) and "ntlm auth = no". Have a look at the smb.conf manpage for further details, as they might impact compatibility with older clients. These also apply for all server roles.

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2112 CVE-2016-2112]:=
===============================

------------------------

Samba uses various LDAP client libraries, a builtin one and/or the system ldap libraries (typically openldap).

As active directory domain controller Samba also provides an LDAP server.

Samba takes care of doing SASL (GSS-SPNEGO) authentication with Kerberos or NTLM  for LDAP connections, including possible integrity (sign) and privacy (seal) protection.

Samba has support for an option called "client ldap sasl wrapping" since version 3.2.0. Its default value has changed from "plain" to "sign" with version 4.2.0.

Tools using the builtin LDAP client library do not obey the "client ldap sasl wrapping" option. This applies to tools like: "samba-tool", "ldbsearch", "ldbedit" and more. Some of them have command line options like "--sign" and "--encrypt". With the security update they will also obey the "client ldap sasl wrapping" option as default.

In all cases, even if explicitly request via "client ldap sasl wrapping", "--sign" or "--encrypt", the protection can be downgraded by a man in the middle.

The LDAP server doesn't have an option to enforce strong authentication yet. The security patches will introduce a new option called "ldap server require strong auth", possible values are "no", "allow_sasl_over_tls" and "yes".

As the default behavior was as "no" before, you may have to explicitly change this option until all clients have been adjusted to handle LDAP_STRONG_AUTH_REQUIRED errors. Windows clients and Samba member servers already use integrity protection.

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2113 CVE-2016-2113]:=
===============================

------------------------

Samba has support for TLS/SSL for some protocols: ldap and http, but currently certificates are not validated at all. While we have a "tls cafile" option, the configured certificate is not used to validate the server certificate.

This applies to ldaps:// connections triggered by tools like: "ldbsearch", "ldbedit" and more. Note that it only applies to the ldb tools when they are built as part of Samba or with Samba extensions installed, which means the Samba builtin LDAP client library is used.

It also applies to dcerpc client connections using ncacn_http (with https://), which are only used by the openchange project. Support for ncacn_http was introduced in version 4.2.0. 

The security patches will introduce a new option called "tls verify peer". Possible values are "no_check", "ca_only", "ca_and_name_if_available", "ca_and_name" and "as_strict_as_possible". 

If you use the self-signed certificates which are auto-generated by Samba, you won't have a crl file and need to explicitly set "tls verify peer = ca_and_name".

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2114 CVE-2016-2114]=
===============================

------------------------

Due to a regression introduced in Samba 4.0.0, an explicit "server signing = mandatory" in the [global] section of the smb.conf was not enforced for clients using the SMB1 protocol.

As a result it does not enforce smb signing and allows man in the middle attacks.

This problem applies to all possible server roles: standalone server, member server, classic primary domain controller, classic backup domain controller and active directory domain controller.

In addition, when Samba is configured with "server role = active directory domain controller" the effective default for the "server signing" option should be "mandatory".

During the early development of Samba 4 we had a new experimental file server located under source4/smb_server. But before the final 4.0.0 release we switched back to the file server under source3/smbd.

But the logic for the correct default of "server signing" was not ported correctly ported.

Note that the default for server roles other than active directory domain controller, is "off" because of performance reasons.

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2115 CVE-2016-2115]:=
===============================

------------------------

Samba has an option called "client signing", this is turned off by default for performance reasons on file transfers.

This option is also used when using DCERPC with ncacn_np.

In order to get integrity protection for ipc related communication by default the "client ipc signing" option is introduced. The effective default for this new option is "mandatory".

In order to be compatible with more SMB server implementations, the following additional options are introduced:
*   "client ipc min protocol" ("NT1" by default) and
*   "client ipc max protocol" (the highest support SMB2/3 dialect by default).
* These options overwrite the "client min protocol" and "client max protocol" options, because the default for "client max protocol" is still "NT1". The reason for this is the fact that all SMB2/3 support SMB signing, while there are still SMB1 implementations which don't offer SMB signing by default (this includes Samba versions before 4.0.0). 

Note that winbindd (in versions 4.2.0 and higher) enforces SMB signing against active directory domain controllers despite of the "client signing" and "client ipc signing" options.

[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2118 CVE-2016-2118] (a.k.a. BADLOCK):=
===============================

------------------------

The Security Account Manager Remote Protocol [MS-SAMR] and the Local Security Authority (Domain Policy) Remote Protocol [MS-LSAD] are both vulnerable to man in the middle attacks. Both are application level protocols based on the generic DCE 1.1 Remote Procedure Call (DCERPC) protocol.

These protocols are typically available on all Windows installations as well as every Samba server. They are used to maintain the Security Account Manager Database. This applies to all roles, e.g. standalone, domain member, domain controller.

Any authenticated DCERPC connection a client initiates against a server can be used by a man in the middle to impersonate the authenticated user against the SAMR or LSAD service on the server.

The client chosen application protocol, auth type (e.g. Kerberos or NTLM ) and auth level (NONE, CONNECT, PKT_INTEGRITY, PKT_PRIVACY) do not matter in this case. A man in the middle can change auth level to CONNECT (which means authentication without message protection) and take over the connection.

As a result, a man in the middle is able to get read/write access to the Security Account Manager Database, which reveals all password sand any other potential sensitive information.

Samba running as an active directory domain controller is additionally missing checks to enforce PKT_PRIVACY for the Directory Replication Service Remote Protocol [MS-DRSR] (drsuapi) and the BackupKey Remote Protocol [MS-BKRP] (backupkey). The Domain Name Service Server Management Protocol [MS-DNSP] (dnsserver) is not enforcing at least PKT_INTEGRITY.

===============================
New smb.conf options
===============================

------------------------

allow dcerpc auth level connect (G)=
===============================

------------------------

This option controls whether DCERPC services are allowed to be used with DCERPC_AUTH_LEVEL_CONNECT, which provides authentication, but no per message integrity nor privacy protection.

Some interfaces like samr, lsarpc and netlogon have a hard-coded default of no and epmapper, mgmt and rpcecho have a hard-coded default of yes.

The behavior can be overwritten per interface name (e.g. lsarpc, netlogon, samr, srvsvc, winreg, wkssvc ...) by using 'allow dcerpc auth level connect:interface = yes' as option.

This option yields precedence to the implementation specific restrictions. E.g. the drsuapi and backupkey protocols require DCERPC_AUTH_LEVEL_PRIVACY. The dnsserver protocol requires DCERPC_AUTH_LEVEL_INTEGRITY.

    Default: allow dcerpc auth level connect = no
    Example: allow dcerpc auth level connect = yes

client ipc signing (G)=
===============================

------------------------

This controls whether the client is allowed or required to use SMB signing for IPC$ connections as DCERPC transport. Possible values are auto, mandatory and disabled.

When set to mandatory or default, SMB signing is required. When set to auto, SMB signing is offered, but not enforced and if set to disabled, SMB signing is not offered either.

Connections from winbindd to Active Directory Domain Controllers always enforce signing.

    Default: client ipc signing = default

client ipc max protocol (G)=
===============================

------------------------

The value of the parameter (a string) is the highest protocol level that will be supported for IPC$ connections as DCERPC transport.

Normally this option should not be set as the automatic negotiation phase in the SMB protocol takes care of choosing the appropriate protocol.

The value default refers to the latest supported protocol, currently SMB3_11.

See client max protocol for a full list of available protocols. The values CORE, COREPLUS, LANMAN1, LANMAN2 are silently upgraded to NT1.

    Default: client ipc max protocol = default
    Example: client ipc max protocol = SMB2_10

client ipc min protocol (G)=
===============================

------------------------

This setting controls the minimum protocol version that the will be attempted to use for IPC$ connections as DCERPC transport.

Normally this option should not be set as the automatic negotiation phase in the SMB protocol takes care of choosing the appropriate protocol.

The value default refers to the higher value of NT1 and the effective value of "client min protocol".

See client max protocol for a full list of available protocols. The values CORE, COREPLUS, LANMAN1, LANMAN2 are silently upgraded to NT1.

    Default: client ipc min protocol = default
    Example: client ipc min protocol = SMB3_11

ldap server require strong auth (G)=
===============================

------------------------

The ldap server require strong auth defines whether the ldap server requires ldap traffic to be signed or signed and encrypted (sealed). Possible values are no, allow_sasl_over_tls and yes.

A value of no allows simple and sasl binds over all transports.

A value of allow_sasl_over_tls allows simple and sasl binds (without sign or seal) over TLS encrypted connections. Unencrypted connections only allow sasl binds with sign or seal.

A value of yes allows only simple binds over TLS encrypted connections. Unencrypted connections only allow sasl binds with sign or seal.

    Default: ldap server require strong auth = yes

raw NTLMv2 auth (G)=
===============================

------------------------

This parameter determines whether or not smbd(8) will allow SMB1 clients without extended security (without SPNEGO) to use NTLMv2 authentication.

If this option, lanman auth and ntlm auth are all disabled, then only clients with SPNEGO support will be permitted. That means NTLMv2 is only supported within NTLM .

    Default: raw NTLMv2 auth = no

tls verify peer (G)=
===============================

------------------------

This controls if and how strict the client will verify the peer's certificate and name. Possible values are (in increasing order): no_check, ca_only, ca_and_name_if_available, ca_and_name and as_strict_as_possible.

When set to no_check the certificate is not verified at all, which allows trivial man in the middle attacks.

When set to ca_only the certificate is verified to be signed from a ca specified in the "tls ca file" option. Setting "tls ca file" to a valid file is required. The certificate lifetime is also verified. If the "tls crl file" option is configured, the certificate is also verified against the ca crl.

When set to ca_and_name_if_available all checks from ca_only are performed. In addition, the peer hostname is verified against the certificate's name, if it is provided by the application layer and not given as an ip address string.

When set to ca_and_name all checks from ca_and_name_if_available are performed. In addition the peer hostname needs to be provided and even an ip address is checked against the certificate's name.

When set to as_strict_as_possible all checks from ca_and_name are performed. In addition the "tls crl file" needs to be configured. Future versions of Samba may implement additional checks.

    Default: tls verify peer = as_strict_as_possible

tls priority (G) (backported from Samba 4.3 to Samba 4.2)=
===============================

------------------------

This option can be set to a string describing the TLS protocols to be supported in the parts of Samba that use GnuTLS, specifically the AD DC.

The default turns off SSLv3, as this protocol is no longer considered secure after CVE-2014-3566 (otherwise known as POODLE) impacted SSLv3 use in HTTPS applications.

The valid options are described in the GNUTLS Priority-Strings documentation at http://gnutls.org/manual/html_node/Priority-Strings.html

    Default: tls priority = NORMAL:-VERS-SSL3.0

===============================
Behavior changes
===============================

------------------------

*  The default auth level for authenticated binds has changed from DCERPC_AUTH_LEVEL_CONNECT to DCERPC_AUTH_LEVEL_INTEGRITY. That means ncacn_ip_tcp:server is now implicitly the same as ncacn_ip_tcp:server[sign] and offers a similar protection  as ncacn_np:server, which relies on smb signing.

*  The following constraints are applied to SMB1 connections:
** "client lanman auth = yes" is now consistently required for authenticated connections using the SMB1 LANMAN2 dialect.
** "client ntlmv2 auth = yes" and "client use spnego = yes" (both the default values), require extended security (SPNEGO) support from the server. That means NTLMv2 is only used within NTLM .

*  Tools like "samba-tool", "ldbsearch", "ldbedit" and more obey the default of "client ldap sasl wrapping = sign". Even with "client ldap sasl wrapping = plain" they will automatically upgrade to "sign" when getting LDAP_STRONG_AUTH_REQUIRED from the LDAP server.

===============================
Changes since 4.4.0:
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11344 BUG #11344] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5370 CVE-2015-5370]: Multiple errors in DCE-RPC code.
*  Christian Ambach <ambi@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11644 BUG #11644] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2112 CVE-2016-2112]: The LDAP client and server don't enforce integrity protection.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11749 BUG #11749] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2111 CVE-2016-2111]: NETLOGON Spoofing Vulnerability.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.
o  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.
o  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11344 BUG #11344] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-5370 CVE-2015-5370]: Multiple errors in DCE-RPC code.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11616 BUG #11616] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2118 CVE-2016-2118]: SAMR and LSA man in the middle attacks possible.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11644 BUG #11644] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2112 CVE-2016-2112]: The LDAP client and server doesn't enforce integrity protection.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11687 BUG #11687] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2114 CVE-2016-2114]: "server signing = mandatory" not enforced.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11688 BUG #11688] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2110 CVE-2016-2110]: Man in the middle attacks possible with NTLM .
* * [https://bugzilla.samba.org/show_bug.cgi?id=11749 BUG #11749] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2111 CVE-2016-2111]: NETLOGON Spoofing Vulnerability.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11752 BUG #11752] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2113 CVE-2016-2113]: Missing TLS certificate validation allows man in the middle attacks.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11756 BUG #11756] - [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2115 CVE-2016-2115]: SMB client connections for IPC traffic are not integrity protected.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11804 BUG #11804] - prerequisite backports for the security release on April 12th, 2016.

* https://www.samba.org/samba/history/samba-4.4.2.html

Samba 4.4.0
------------------------

<onlyinclude>
* Release Notes for Samba 4.4.0
* March  22,  2016

===============================
This is the first stable release of the Samba 4.4 release series.
===============================

------------------------

===============================
UPGRADING
===============================

------------------------

Nothing special.

===============================
NEW FEATURES/CHANGES
===============================

------------------------

Asynchronous flush requests=
===============================

------------------------

Flush requests from SMB2/3 clients are handled asynchronously and do not block the processing of other requests. Note that 'strict sync' has to be set to 'yes' for Samba to honor flush requests from SMB clients.

s3: smbd=
===============================

------------------------

Remove '--with-aio-support' configure option. We no longer would ever prefer POSIX-RT aio, use pthread_aio instead.

samba-tool sites=
===============================

------------------------

The 'samba-tool sites' subcommand can now be run against another server by specifying an LDB URL using the '-H' option and not against the local database only (which is still the default when no URL is given).

samba-tool domain demote=
===============================

------------------------

Add '--remove-other-dead-server' option to 'samba-tool domain demote' subcommand. The new version of this tool now can remove another DC that is itself offline.  The '--remove-other-dead-server' removes as many references to the DC as possible.

samba-tool drs clone-dc-database=
===============================

------------------------

Replicate an initial clone of domain, but do not join it. This is developed for debugging purposes, but not for setting up another DC.

pdbedit=
===============================

------------------------

Add '--set-nt-hash' option to pdbedit to update user password from nt-hash hexstring. 'pdbedit -vw' shows also password hashes.

smbstatus=
===============================

------------------------

'smbstatus' was enhanced to show the state of signing and encryption for sessions and shares.

smbget=
===============================

------------------------

The -u and -p options for user and password were replaced by the -U option that accepts username[%password] as in many other tools of the Samba suite. Similary, smbgetrc files do not accept username and password options any more, only a single "user" option which also accepts user%password combinations.

s4-rpc_server=
===============================

------------------------

Add a GnuTLS based backupkey implementation.

ntlm_auth=
===============================

------------------------

Using the '--offline-logon' enables ntlm_auth to use cached passwords when the DC is offline.

Allow '--password' force a local password check for ntlm-server-1 mode.

vfs_offline=
===============================

------------------------

A new VFS module called vfs_offline has been added to mark all files in the share as offline. It can be useful for shares mounted on top of a remote file system (either through a samba VFS module or via FUSE).

KCC=
===============================

------------------------

The Samba KCC has been improved, but is still disabled by default.

DNS=
===============================

------------------------

There were several improvements concerning the Samba DNS server.

Active Directory=
===============================

------------------------

There were some improvements in the Active Directory area.

WINS nsswitch module=
===============================

------------------------

The WINS nsswitch module has been rewritten to address memory issues and to simplify the code. The module now uses libwbclient to do WINS queries. This means that winbind needs to be running in order to resolve WINS names using the nss_wins module. This does not affect smbd.

CTDB changes=
===============================

------------------------

* CTDB now uses a newly implemented parallel database recovery scheme that avoids deadlocks with smbd.
* In certain circumstances CTDB and smbd could deadlock. The new recovery implementation avoid this.  It also provides improved recovery performance.
* All files are now installed into and referred to by the paths configured at build time. Therefore, CTDB will now work properly when installed into the default location at /usr/local.
* Public CTDB header files are no longer installed, since Samba and CTDB are built from within the same source tree.
* CTDB_DBDIR can now be set to tmpfs[:<tmpfs-options>]
* This will cause volatile TDBs to be located in a tmpfs. This can help to avoid performance problems associated with contention on the disk where volatile TDBs are usually stored. See ctdbd.conf(5) for more details.
* Configuration variable CTDB_NATGW_SLAVE_ONLY is no longer used.
* Instead, nodes should be annotated with the "slave-only" option in the CTDB NAT gateway nodes file.  This file must be consistent across nodes in a NAT gateway group.  See ctdbd.conf(5) for more details.

* New event script 05.system allows various system resources to be monitored
* This can be helpful for explaining poor performance or unexpected behaviour. New configuration variables are CTDB_MONITOR_FILESYSTEM_USAGE, CTDB_MONITOR_MEMORY_USAGE and CTDB_MONITOR_SWAP_USAGE.  Default values cause warnings to be logged.  See the SYSTEM RESOURCE MONITORING CONFIGURATION in ctdbd.conf(5) for more information.
* The memory, swap and filesystem usage monitoring previously found in 00.ctdb and 40.fs_use is no longer available.  Therefore, configuration variables CTDB_CHECK_FS_USE, CTDB_MONITOR_FREE_MEMORY, CTDB_MONITOR_FREE_MEMORY_WARN and CTDB_CHECK_SWAP_IS_NOT_USED are now ignored.
* The 62.cnfs eventscript has been removed.  To get a similar effect just do something like this:
      mmaddcallback ctdb-disable-on-quorumLoss \
        --command /usr/bin/ctdb \
        --event quorumLoss --parms "disable"

      mmaddcallback ctdb-enable-on-quorumReached \
        --command /usr/bin/ctdb \
        --event quorumReached --parms "enable"
* The CTDB tunable parameter EventScriptTimeoutCount has been renamed to MonitorTimeoutCount
* It has only ever been used to limit timed-out monitor events.

* Configurations containing CTDB_SET_EventScriptTimeoutCount=<n> will cause CTDB to fail at startup.  Useful messages will be logged.
* The commandline option "-n all" to CTDB tool has been removed.
* The option was not uniformly implemented for all the commands. Instead of command "ctdb ip -n all", use "ctdb ip all".
* All CTDB current manual pages are now correctly installed

===============================
EXPERIMENTAL FEATURES
===============================

------------------------

SMB3 Multi-Channel=
===============================

------------------------

Samba 4.4.0 adds *experimental* support for SMB3 Multi-Channel. Multi-Channel is an SMB3 protocol feature that allows the client to bind multiple transport connections into one authenticated SMB session. This allows for increased fault tolerance and throughput. The client chooses transport connections as reported by the server and also chooses over which of the bound transport connections to send traffic. I/O operations for a given file handle can span multiple network connections this way.
An SMB multi-channel session will be valid as long as at least one of its channels are up.

In Samba, multi-channel can be enabled by setting the new smb.conf option "server multi channel support" to "yes". It is disabled by default.

Samba has to report interface speeds and some capabilities to the client. On Linux, Samba can auto-detect the speed of an interface. But to support other platforms, and in order to be able to manually override the detected values, the "interfaces" smb.conf option has been given an extended syntax, by which an interface specification can additionally carry speed and capability information. The extended syntax looks like this for setting the speed to 1 gigabit per second:

    interfaces = 192.168.1.42;speed=1000000000

This extension should be used with care and are mainly intended for testing. See the smb.conf manual page for details.

CAVEAT: While this should be working without problems mostly, there are still corner cases in the treatment of channel failures that may result in DATA CORRUPTION when these race conditions hit. 

It is hence

    NOT RECOMMENDED TO USE MULTI-CHANNEL IN PRODUCTION

at this stage. This situation can be expected to improve during the life-time of the 4.4 release. Feed-back from test-setups is highly welcome.

===============================
REMOVED FEATURES
===============================

------------------------

Public headers=
===============================

------------------------

Several public headers are not installed any longer. They are made for internal use only. More public headers will very likely be removed in future releases.

The following headers are not installed any longer:
dlinklist.h, gen_ndr/epmapper.h, gen_ndr/mgmt.h, gen_ndr/ndr_atsvc_c.h, gen_ndr/ndr_epmapper_c.h, gen_ndr/ndr_epmapper.h, gen_ndr/ndr_mgmt_c.h, gen_ndr/ndr_mgmt.h,gensec.h, ldap_errors.h, ldap_message.h, ldap_ndr.h, ldap-util.h, pytalloc.h, read_smb.h, registry.h, roles.h, samba_util.h, smb2_constants.h, smb2_create_blob.h, smb2.h, smb2_lease.h, smb2_signing.h, smb_cli.h, smb_cliraw.h, smb_common.h, smb_composite.h, smb_constants.h, smb_raw.h, smb_raw_interfaces.h, smb_raw_signing.h, smb_raw_trans2.h, smb_request.h, smb_seal.h, smb_signing.h, smb_unix_ext.h, smb_util.h, torture.h, tstream_smbXcli_np.h.

vfs_smb_traffic_analyzer=
===============================

------------------------

The SMB traffic analyzer VFS module has been removed, because it is not maintained any longer and not widely used.

vfs_scannedonly=
===============================

------------------------

The scannedonly VFS module has been removed, because it is not maintained any longer.

===============================
smb.conf changes
===============================

------------------------

    arameter Name		Description		Default
    -------------		-----------		-------
    io max threads               New                     100
    dap page size		Changed default		1000
    erver multi channel support	New			No
    nterfaces			Extended syntax
</onlyinclude>

===============================
CHANGES SINCE 4.4.0rc5
===============================

------------------------

*  Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11796 BUG #11796]: smbd: Enable multi-channel if 'server multi channel support = yes' in the config.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11802 BUG #11802]: lib/socket/interfaces: Fix some uninitialied bytes.
*  Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11798 BUG #11798]: build: Fix build when '--without-quota' specified.

===============================
CHANGES SINCE 4.4.0rc4
===============================

------------------------

*  Andrew Bartlett <abartlet@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11780 BUG #11780]: mkdir can return ACCESS_DENIED incorrectly on create race.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11783 BUG #11783]: Mismatch between local and remote attribute ids lets replication fail with custom schema.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11789 BUG #11789]: Talloc: Version 2.1.6.
*  Ira Cooper <ira@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11774 BUG #11774]: vfs_glusterfs: Fix use after free in AIO callback.
*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11755 BUG #11755]: Fix net join.
*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11770 BUG #11770]: Reset TCP Connections during IP failover.
*  Justin Maggard <jmaggard10@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11773 BUG #11773]: s3:smbd: Add negprot remote arch detection for OSX.
*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11772 BUG #11772]: ldb: Version 1.1.26.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11782 BUG #11782]: "trustdom_list_done: Got invalid trustdom response" message should be avoided.
*  Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id=11769 BUG #11769]: libnet: Make Kerberos domain join site-aware.
* * [https://bugzilla.samba.org/show_bug.cgi?id=11788 BUG #11788]: Quota is not supported on Solaris 10.

===============================
CHANGES SINCE 4.4.0rc2
===============================

------------------------

*  Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11723 BUG #11723]: lib:socket: Fix CID 1350010: Integer OVERFLOW_BEFORE_WIDEN.
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11735 BUG #11735]: lib:socket: Fix CID 1350009: Fix illegal memory accesses (BUFFER_SIZE_WARNING).
*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id= 10489 BUG #10489]: s3: smbd: posix_acls: Fix check for setting u:g:o entry on a filesystem with no ACL support.
*  Christian Ambach <ambi@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11700 BUG #11700]: s3:utils/smbget: Set default blocksize.
*  Anoop C S <anoopcs@redhat.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11734 BUG #11734]: lib/socket: Fix improper use of default interface speed.
*  Ralph Boehme <slow@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11714 BUG #11714]: lib/tsocket: Work around sockets not supporting FIONREAD.
*  Volker Lendecke <vl@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11724 BUG #11724]: smbd: Fix CID 1351215 Improper use of negative value.
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11725 BUG #11725]: smbd: Fix CID 1351216 Dereference null return value.
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11732 BUG #11732]: param: Fix str_list_v3 to accept ; again.
*  Noel Power <noel.power@suse.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11738 BUG #11738]: libcli: Fix debug message, print sid string for new_ace trustee.
*  Jose A. Rivera <jarrpa@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11727 BUG #11727]: s3:smbd:open: Skip redundant call to file_set_dosmode when creating a new file.
*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11730 BUG #11730]: docs: Add manpage for cifsdd.
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11739 BUG #11739]: Fix installation path of Samba helper binaries.
*  Berend De Schouwer <berend.de.schouwer@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11643 BUG #11643]: docs: Add example for domain logins to smbspool man page.
*  Martin Schwenke <martin@meltin.net>
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11719 BUG #11719]: ctdb-scripts: Drop use of "smbcontrol winbindd ip-dropped ..."
*  Hemanth Thummala <hemanth.thummala@nutanix.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11708 BUG #11708]: loadparm: Fix memory leak issue.
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11740 BUG #11740]: Fix memory leak in loadparm.

===============================
CHANGES SINCE 4.4.0rc1
===============================

------------------------

*  Michael Adam <obnox@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11715 BUG #11715]: s3:vfs:glusterfs: Fix build after quota changes.

*  Jeremy Allison <jra@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11703 BUG #11703]: s3: smbd: Fix timestamp rounding inside SMB2 create.

*  Christian Ambach <ambi@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11700 BUG #11700]:  Streamline 'smbget' options with the rest of the Samba utils.

*  Günther Deschner <gd@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11696 BUG #11696]: ctdb: Do not provide a useless pkgconfig file for ctdb.

*  Stefan Metzmacher <metze@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11699 BUG #11699]: Crypto.Cipher.ARC4 is not available on some platforms, fallback to M2Crypto.RC4.RC4 then.

*  Amitay Isaacs <amitay@gmail.com>
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11705 BUG #11705]: Sockets with htons(IPPROTO_RAW) and [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-8543 CVE-2015-8543].

*  Andreas Schneider <asn@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11690 BUG #11690]: docs: Add smbspool_krb5_wrapper manpage.

*  Uri Simchoni <uri@samba.org>
* * [https://bugzilla.samba.org/show_bug.cgi?id= 11681 BUG #11681]: smbd: Show correct disk size for different quota and dfree block sizes.

===============================
KNOWN ISSUES
===============================

------------------------

Currently none.

 https://download.samba.org/pub/samba/rc/samba-4.4.0rc3.WHATSNEW.txt

----
`Category:Release Notes`