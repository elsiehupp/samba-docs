Samba 4.9 Features added/changed
    <namespace>0</namespace>
<last_edited>2020-03-03T15:03:40Z</last_edited>
<last_editor>Fraz</last_editor>

Samba 4. is `Samba_Release_Planning#Discontinued_.28En.ife.29|**Dis.ued (End of Life)**`..

Samba 4.=.
* Notes for Samba 4..
* 21, 2020

===============================
This is a security release in order to address the following defects:
* [http://cve..org/.n/cvename.cgi?name=.19-14902 CVE-2019-14902]: :ion of ACLs set to inherit down a subtree on AD Directory not automatic..
* [http://cve..org/.n/cvename.cgi?name=.19-14907 CVE-2019-14907]:  after failed character conversion at log level 3 or above..
* [http://cve..org/.n/cvename.cgi?name=.19-19344 CVE-2019-19344]:  after free during DNS zone scavenging in Samba AD DC..
===============================
Details
===============================

------------------------

*  [http://cve..org/.n/cvename.cgi?name=.19-14902 CVE-2019-14902]::
 implementation of ACL inheritance in the Samba AD DC was not complete, and so absent a 'full-sync' replication, ACLs could get out of sync between domain controllers. 
*  [http://cve..org/.n/cvename.cgi?name=.19-14907 CVE-2019-14907]::
 processing untrusted string input Samba can read past the end of the allocated buffer when printing a "Conversion error" message to the logs.
*  [http://cve..org/.n/cvename.cgi?name=.19-19344 CVE-2019-19344]: : E                                                                              
 DNS zone scavenging (of expired dynamic entries) there is a read of memory after it has been freed.

For more details and workarounds, please refer to the security advisories.

===============================
Changes since 4..

*  Andrew Bartlett <abartlet@samba.t;
* [https://bugzilla..org/.ug.cgi?id=12. BUG #12497]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2.902 CVE-2019-14902]: Replication ofSS:Ls down subtree on AD Directory not automatic..
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14208]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2.907 CVE-2019-14907]: lib/util:  not print : failed to convert string into the logs..
*  Gary Lockyer <gary@catalyst.z&g.
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14050]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2.344 CVE-2019-19344]: kcc dns :g: Fix use after :  in dns_tombstone_records_zone..

 https://www..org/.history/samba-4.9.18.html...

Samba 4.=.
* Notes for Samba 4..
* 10, 2019

===============================
This is a security release in order to address the following defects:

* [http://cve..org/.n/cvename.cgi?name=.19-14861 CVE-2019-14861]:  AD DC zone-named record Denial of Service in DNS management server (dnsserver)..
* [http://cve..org/.n/cvename.cgi?name=.19-14870 CVE-2019-14870]: :onNotAllowed not being enforced in protocol transition on Samba AD DC..

===============================
Details
===============================

------------------------

*  [http://cve..org/.n/cvename.cgi?name=.19-14861 CVE-2019-14861]::
 authenticated user can crash the DCE/RPC DNS management server by creating records with matching the zone name.

*  [http://cve..org/.n/cvename.cgi?name=.19-14870 CVE-2019-14870]::
 DelegationNotAllowed Kerberos feature restriction was not being applied when processing protocol transition requests (S4U2Self), in the AD DC KDC.

For more details and workarounds, please refer to the security advisories.

===============================
Changes since 4..

*Andrew Bartlett <abartlet@samba.t;
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14138]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2.861 CVE-2019-14861]: Fix DNSServer : server crash..
*  Isaac Boukris <iboukris@gmail.t;
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14187]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2.870 CVE-2019-14870]: DelegationNotAllowed : being enforced..

 https://www..org/.history/samba-4.9.17.html...

Samba 4.=.
* Notes for Samba 4..
* 27, 2019

===============================
This is an additional bug fix release to address [https://bugzilla..org/.ug.cgi?id=14. BUG #14175]
===============================

------------------------

* AACC: queue can be orphaned causing communication breakdown.

===============================
Changes since 4..
---------------------

* Volker Lendecke <vl@samba.t;
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14175]: ctdb:SS:oid c:on breakdown on node reconnect..
*  Martin Schwenke <martin@meltin.t;
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14175]: ctdb:SS:coming  can be orphaned causing communication breakdown..

 https://www..org/.history/samba-4.9.16.html...

Samba 4.=.
* Notes for Samba 4..
* 29, 2019

===============================
This is a security release in order to address the following defects:

* [http://cve..org/.n/cvename.cgi?name=.19-10218 CVE-2019-10218]: : code can return filenames containing path separators..
* [http://cve..org/.n/cvename.cgi?name=.19-14833 CVE-2019-14833]:  AD DC check password script does not receive the full password..
* [http://cve..org/.n/cvename.cgi?name=.19-14847 CVE-2019-14847]:  with "get changes" permission can crash AD DC LDAP server via dirsync..

===============================
Details
===============================

------------------------

* [http://cve..org/.n/cvename.cgi?name=.19-10218 CVE-2019-10218]::
* servers can cause Samba client code to return filenames containing path separators to calling code.
* [http://cve..org/.n/cvename.cgi?name=.19-14833 CVE-2019-14833]::
 the password contains multi-byte (non-ASCII) characters, the check password script does not receive the full password string.

* [http://cve..org/.n/cvename.cgi?name=.19-14847 CVE-2019-14847]::
 with the "get changes" extended access right can crash the AD DC LDAP server by requesting an attribute using the range= syntax.

For more details and workarounds, please refer to the security advisories.

===============================
Changes since 4..

* Jeremy Allison <jra@samba.t;
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14071]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2.197 CVE-2019-10197]CVE-2019-10218 - s3: libsmb: ProtectS:MB1 and:SMB2 client code from evil server returned names..

*  Andrew Bartlett <abartlet@samba.t;
* [https://bugzilla..org/.ug.cgi?id=12. BUG #12438]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2.833 CVE-2019-14833]: Use utf8 :rs in the unacceptable password..
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14040]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2.847 CVE-2019-14847] dsdb: Correct behaviou: of ranged_results when combined with dirsync..
*  Björn Baumbach <bb@sernet.;
* [https://bugzilla..org/.ug.cgi?id=12. BUG #12438]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2.833 CVE-2019-14833] dsdb: Send full :d to check password script..

 https://www..org/.history/samba-4.9.15.html...

Samba 4.=.
* Notes for Samba 4..
* 22, 2019

===============================
This is the last bugfix release of the Samba 4. release series. .There will be security releases only beyond this point.===.

===============================
Changes since 4..
*  Jeremy Allison <jra@samba.t;
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14094]: smbc_re:) is incompatible with smbc_telldir() and smbc_lseekdir()..
*  Douglas Bagnall <douglas.ll@catalyst.net...
* [https://bugzilla..org/.ug.cgi?id=13. BUG #13978]: s4SSLLA:ting: MORE py: compatible print functions..
*  Andrew Bartlett <abartlet@samba.t;
* ldb: : ldb 1..
* [https://bugzilla..org/.ug.cgi?id=13. BUG #13959]: ldb_tdb:fails to check error return when parsing pack formats..
* [https://bugzilla..org/.ug.cgi?id=13. BUG #13978]: undogui Add &qu later" to warning about using tools from Samba 4.8...
*  Ralph Boehme <slow@samba.t;
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14038]: ctdb:SS:x com: on systems with glibc robust mutexes..
*  Isaac Boukris <iboukris@gmail.t;
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14106]: Fix :go fallback from kerberos to ntlmssp in smbd server..
*  Poornima G <pgurusid@redhat.t;
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14098]: vfs_glu Use pth: for scheduling aio operations..
*  Aaron Haslett <aaronhaslett@catalyst.z&g.
* [https://bugzilla..org/.ug.cgi?id=13. BUG #13977]: ldb: : info  format check on init..
* [https://bugzilla..org/.ug.cgi?id=13. BUG #13978]: sambaun:x is untested and py2-only..
*  Amitay Isaacs <amitay@gmail.t;
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14147]: ctdbDDA:m: Process  records not deleted on a remote node..
*  Björn Jacke <bj@sernet.;
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14139]: FaultDD Improve :ort message text pointing to our wiki..
*  Stefan Metzmacher <metze@samba.t;
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14055]: libcliS:mb: send SM:_NEGOTIATE_CONTEXT_ID..
*  Martin Schwenke <martin@meltin.t;
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14084]: ctdbDDA: Mark no: Eas disconnected if incoming connection goes away..
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14087]: 'ctdbSS:op' command completes before databases are frozen..
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14129]: Exit :  of ctdb nodestatus should not be influenced by deleted nodes..
*  Evgeny Sinelnikov <sin@altlinux.t;
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14007]: s3:ldap: Fix :AACC don't exists machine account..

 https://www..org/.history/samba-4.9.14.html...

Samba 4.=.
* Notes for Samba 4..
* 03, 2019

===============================
This is a security release in order to address the following defect:

*  [http://cve..org/.n/cvename.cgi?name=.19-10197 CVE-2019-10197]: :ion of parameters and permissions can allow user to escape from the share path definition..

===============================
Details
===============================

------------------------

*  [http://cve..org/.n/cvename.cgi?name=.19-10197 CVE-2019-10197]::
 certain parameter configurations, when an SMB client accesses a network share and the user does not have permission to access the share root directory, it is possible for the user to escape from the share to see the complete '/' filesystem. Unix permission checks in the kernel are still enforced..

===============================
Changes since 4..

*  Jeremy Allison <jra@samba.t;
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14035]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2.197 CVE-2019-10197]: Permissions chec: deny can allow user to escape from the share..
*  Stefan Metzmacher <metze@samba.t;
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14035]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2.197 CVE-2019-10197]: Permissions chec: deny can allow user to escape from the share..

 https://www..org/.history/samba-4.9.13.html...

Samba 4.=.
* Notes for Samba 4..
 27, 2019

===============================
This is the latest stable release of the Samba 4. release series.===.

===============================
Changes since 4..

*  Michael Adam <obnox@samba.t;
* [https://bugzilla..org/.ug.cgi?id=13. BUG #13972]: vfs:glu: : D: Device:Id for GlusterFS FUSE mount is causing data loss in CTDB cluster..
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14010]: vfs:glu: : U to : or rename file/directory inside shares configured with fs_glusterfs_fuse module..
*  Björn Baumbach <bb@sernet.;
* [https://bugzilla..org/.ug.cgi?id=13. BUG #13973]: sambaDD:: Add 'im samba.drs_utils' .to fsmo.py...
*  Tim Beale <timbeale@catalyst.z&g.
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14008]: dsdb:SS:ndle : Ecorner-case where PSO container doesn't exist..
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14021]: s4SSLLA:t: Fix joi a Windows pre-2008R2 DC..
*  Ralph Boehme <slow@samba.t;
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14015]: vfs_cat Pass st: Einfo to synthetic_smb_fname()..
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14033]: SambaSS:DOOTT9 doesn't build with libtevent 0.9.39....
*  Alexander Bokovoy <ab@samba.t;
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14091]: lookup_ Allow o: Edomain lookup when flags == 0..
*  Isaac Boukris <iboukris@gmail.t;
* [https://bugzilla..org/.ug.cgi?id=11. BUG #11362]: Add :aryGroupId to group array in DC response..
*  Anoop C S <anoopcs@redhat.t;
* [https://bugzilla..org/.ug.cgi?id=14. BUG #14035]: vfs_glu Enable : for file system operations..
*  Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13915 BUG #13915]: DEBUGCS:nd DEBUGADDC doesn't print into a class specific log file.
* [https://bugzilla.samba.org/show_bug.cgi?id=13949 BUG #13949]: Request:to keep deprecated option "server schannel", VMWare Quickprep requires "auto".
* [https://bugzilla.samba.org/show_bug.cgi?id=13967 BUG #13967]: dbcheck: Fallback  the default tombstoneLifetime of 180 days.
* [https://bugzilla.samba.org/show_bug.cgi?id=13969 BUG #13969]: dnsProp fails to decode values from older Windows versions.
* [https://bugzilla.samba.org/show_bug.cgi?id=13973 BUG #13973]: sambaDD:: fsmo tr is not reliable for the dns related partitions role transfer.
*  Christof Schmitt <cs@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=14032 BUG #14032]: vfs_gpf: EFix NFS: EACL for owner with IDMAP_TYPE_BOTH.
*  Rafael David Tinoco <rafaeldtinoco@ubuntu.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=14017 BUG #14017]BUG 14017: ctdbDDA:g: Depend : E/etc/ctdb/nodes file.

 https://www.samba.org/samba/history/samba-4.9.12.html

Samba 4.9.11
------------------------

* Notes for Samba 4.9.11
 03, 2019

===============================
This is the latest stable release of the Samba 4.9 release series.
===============================

------------------------

In yesterday's Samba 4.9.10 release, LDAP_REFERRAL_SCHEME_OPAQUE was added to db_module.h in order to fix bug #12478. Unfortunately, the ldb version was not
raised. Samba >= 4.9.10 is no longer able to build with ldb 1.4.6. This version includes the new ldb version. Please note that there are just the version bumps in ldb and Samba, no code change. If you don't build Samba with an external ldb library, you can ignore this release and keep using 4.9.11.

===============================
Changes since 4.9.10:

*  Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=12478 BUG #12478]: ldb: : ase  1.4.7.

 https://www.samba.org/samba/history/samba-4.9.11.html

Samba 4.9.10
------------------------

* Notes for Samba 4.9.10
 02, 2019

===============================
This is the latest stable release of the Samba 4.9 release series.
===============================

------------------------

===============================
Changes since 4.9.9:

*  Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13938 BUG #13938]: s3: :: D allow :ile on stream fsp's.
* [https://bugzilla.samba.org/show_bug.cgi?id=13956 BUG #13956]: s3: :ind:  crash  invoking winbind idmap scripts.
* [https://bugzilla.samba.org/show_bug.cgi?id=13964 BUG #13964]: smbd :s not correctly parse arguments passed to dfree and quota scripts.
*  Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13981 BUG #13981]: docs:SS:prove :tion of "lanman auth" and "ntlm auth" connection.
*  Björn Baumbach <bb@sernet.de>
* [https://bugzilla.samba.org/show_bug.cgi?id=14002 BUG #14002]: pythonS:tacls: Use cor "state directory" smb.conf option instead of "state dir".
*  Ralph Boehme <slow@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13840 BUG #13840]: registr: EAdd aSS:ssing include.
* [https://bugzilla.samba.org/show_bug.cgi?id=13938 BUG #13938]: s3:smbd: Don't :PAAC:  on streams.
* [https://bugzilla.samba.org/show_bug.cgi?id=13944 BUG #13944]: SMB :t authentication may fail.
* [https://bugzilla.samba.org/show_bug.cgi?id=13958 BUG #13958]: AppleDo conversion breaks Resourceforks.
* [https://bugzilla.samba.org/show_bug.cgi?id=13964 BUG #13964]: s3: : Re all : of file_pload_XXX -> file_ploadv_XXX.
* [https://bugzilla.samba.org/show_bug.cgi?id=13968 BUG #13968]: vfs_fru: Emakes direct use of syscalls like mmap() and pread().
* [https://bugzilla.samba.org/show_bug.cgi?id=13987 BUG #13987]: s3:mdss Fix :  :ion error.
*  Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13872 BUG #13872]: s3SSLLA:lusterfs[_fuse]: Avoid u NAME_MAX directly.
*  David Disseldorp <ddiss@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13940 BUG #13940]: vfs_cep: EFix cep:txattr() debug message.
*  Aaron Haslett <aaronhaslett@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=13799 BUG #13799]: dsdb:sa Schemainfo:updat: with relax control.
*  Amitay Isaacs <amitay@gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13943 BUG #13943]: ctdbDDA:n: Fix mem leak in run_proc.
*  Aliaksei Karaliou <akaraliou@panasas.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13964 BUG #13964]: smbd :s not correctly parse arguments passed to dfree and quota scripts.
*  Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13903 BUG #13903]: winbind: Fix ove: id ranges.
* [https://bugzilla.samba.org/show_bug.cgi?id=13957 BUG #13957]: smbd:SS:x aSS:nic.
*  Gary Lockyer <gary@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=12478 BUG #12478]: ldap :ver: Generate : referral schemes.
* [https://bugzilla.samba.org/show_bug.cgi?id=13902 BUG #13902]: lib : debug: Increase : buffer to 4KiB.
* [https://bugzilla.samba.org/show_bug.cgi?id=13941 BUG #13941]: Fix : after free detected by AddressSanitizer.
* [https://bugzilla.samba.org/show_bug.cgi?id=13942 BUG #13942]: s4 : Fix use:after free in samldb_rename_search_base_callback.
*  Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=12204 BUG #12204]: SambaSS:ils to replicate schema 69.
* [https://bugzilla.samba.org/show_bug.cgi?id=13713 BUG #13713]: SchemaS: plication fails if link crosses chunk boundary backwards.
* [https://bugzilla.samba.org/show_bug.cgi?id=13799 BUG #13799]: 'sambaD:l domain schemaupgrade' uses relax control and skips the schemaInfo update.
* [https://bugzilla.samba.org/show_bug.cgi?id=13916 BUG #13916]: dsdb:au: avoidS:rintingSS:uot;... remote host [Unknown] SID [(NULL SID)] ...".
* [https://bugzilla.samba.org/show_bug.cgi?id=13917 BUG #13917]: pythonS:tacls: We only:need security.SEC_STD_READ_CONTROL in order to get the ACL.
* [https://bugzilla.samba.org/show_bug.cgi?id=13919 BUG #13919]: smbd:SS:plement : _NORMALIZED_NAME_INFORMATION handling.
*  Shyamsunder Rathi <shyam.rathi@nutanix.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13947 BUG #13947]: s3:load EnsureS:o :  FS Volume Label at multibyte boundary.
*  Robert Sander <r.sander@heinlein-support.de>
* [https://bugzilla.samba.org/show_bug.cgi?id=13918 BUG #13918]: s3: :les:  Use : work directory instead of share path.
*  Christof Schmitt <cs@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13831 BUG #13831]: Fix :nsistent output from wbinfo --sid-to-name depending on cache state.
*  Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13937 BUG #13937]: Fix :ral issues detected by GCC 9.
* [https://bugzilla.samba.org/show_bug.cgi?id=13939 BUG #13939]: s3:smbs Fix : ssionSS:inting with Kerberos credentials.
*  Martin Schwenke <martin@meltin.net>
* [https://bugzilla.samba.org/show_bug.cgi?id=13923 BUG #13923]: ctdbDDA:: Fix ctd: dumpmemory to avoid printing trailing NUL.
* [https://bugzilla.samba.org/show_bug.cgi?id=13930 BUG #13930]: ctdbDDA:n: Never u: E0 as a client ID.
* [https://bugzilla.samba.org/show_bug.cgi?id=13943 BUG #13943]: ctdbDDA:n: Fix mem leak.
*  Rafael David Tinoco <rafaeldtinoco@ubuntu.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13984 BUG #13984]: ctdbDDA:ts: Fix tcp:  existence check.
*  Ralph Wuerthner <ralph.wuerthner@de.ibm.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13904 BUG #13904]: Log :y startup failures.

 https://www.samba.org/samba/history/samba-4.9.10.html

Samba 4.9.9
------------------------

* Notes for Samba 4.9.9
 19, 2019

===============================
This is a security release in order to address the following defect:

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-12435 CVE-2019-12435] (Samba AD DC Denial of Service in DNS management server (dnsserver))

===============================
Details
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-12435 CVE-2019-12435]::
 authenticated user can crash the Samba AD DC's RPC server process via a NULL pointer dereference.

For more details and workarounds, please refer to the security advisories.

*[https://www.samba.org/samba/security/CVE-2019-12435.html CVE-2019-12435]

===============================
Changes since 4.9.8:

*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=13922 BUG #13922]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2019-12435 CVE-2019-12435] rpc/dns: Avoid NULL :nce if zone not found in DnssrvOperation2.

 https://www.samba.org/samba/history/samba-4.9.9.html

Samba 4.9.8
------------------------

* Notes for Samba 4.9.8
 14, 2019

===============================
This is a security release in order to address the following defect:

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16860 CVE-2018-16860] (Samba AD DC S4U2Self/S4U2Proxy unkeyed checksum)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16860 CVE-2018-16860]::
 checksum validation in the S4U2Self handler in the embedded Heimdal KDC did not first confirm that the checksum was keyed, allowing replacement of the requested target (client) principal.

For more details and workarounds, please refer to the security advisory.

===============================
Changes since 4.9.7:

*  Isaac Boukris <iboukris@gmail.com> 
* [https://bugzilla.samba.org/show_bug.cgi?id=13685 BUG #13685]: CVEDDAA:AASSHH16860: Heimdal  Reject PADDAASSH: with unkeyed checksum.

 https://www.samba.org/samba/history/samba-4.9.8.html

Samba 4.9.7
------------------------

* Notes for Samba 4.9.7
 1, 2019

===============================
This is the latest stable release of the Samba 4.9 release series.
===============================

------------------------

===============================
Changes since 4.9.6:

*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=13837 BUG #13837]: pySSLLA:tils: py2.6 :ibility.
* [https://bugzilla.samba.org/show_bug.cgi?id=13882 BUG #13882]: pySSLLA:sion: Fix for:Python 2.6.
*  Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13840 BUG #13840]BUG : regfio::Update  near recent changes to match README.Coding.
*  Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13861 BUG #13861]: 'net : join' to child domain fails when using "-U admin@forestroot".
*  David Disseldorp <ddiss@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13858 BUG #13858]: vfs_sna Drop un fstat handler.
* [https://bugzilla.samba.org/show_bug.cgi?id=13896 BUG #13896]: vfs_cep: EExplicitly : libcephfs POSIX ACL support.
*  Philipp Gesang <philipp.gesang@intra2net.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13869 BUG #13869]: libcli::Permit  values of DataLength in SMB2_ENCRYPTION_CAPABILITIES of negotiate response.
*  Michael Hanselmann <public@hansmi.ch>
* [https://bugzilla.samba.org/show_bug.cgi?id=13840 BUG #13840]: regfio::Improve : of malformed registry hive files.
*  Amitay Isaacs <amitay@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13895 BUG #13895]: ctdbDDA:n: Avoid r between fd and signal events.
*  Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13813 BUG #13813]: Fix :p cache pollution with S-1-22- IDs on winbind hickup.
*  Marcos Mello <marcosfrm@gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=11568 BUG #11568]: Send :tus to systemd on daemon start.
*  Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=10097 BUG #10097]: s3:smbd: Handle :PARS: in SMB_FIND_FILE_FULL_DIRECTORY_INFO.
* [https://bugzilla.samba.org/show_bug.cgi?id=10344 BUG #10344]: smb2_tc Avoid S:ING completely on tdis.
* [https://bugzilla.samba.org/show_bug.cgi?id=12844 BUG #12844]: smb2_tc Avoid S:ING responses for tree connect.
* [https://bugzilla.samba.org/show_bug.cgi?id=12845 BUG #12845]: smb2_se: Avoid S:ING responses for session setup.
* [https://bugzilla.samba.org/show_bug.cgi?id=13698 BUG #13698]: smb2_tc Avoid S:ING responses for tree connect.
* [https://bugzilla.samba.org/show_bug.cgi?id=13796 BUG #13796]: smb2_se: Avoid S:ING responses for session setup.
* [https://bugzilla.samba.org/show_bug.cgi?id=13816 BUG #13816]: dbcheck:in the middle of the tombstone garbage collection causes replication failures.
* [https://bugzilla.samba.org/show_bug.cgi?id=13818 BUG #13818]: ndr_spo: Fix out:of scope use of stack variable in NDR_SPOOLSS_PUSH_ENUM_OUT().
* [https://bugzilla.samba.org/show_bug.cgi?id=13862 BUG #13862]: vfs_def Fix vfs:ad_write_send() NT_STATUS_INVALID_VIEW_SIZE check.
* [https://bugzilla.samba.org/show_bug.cgi?id=13863 BUG #13863]: smb2_se Grant a: E8192 credits to clients.
*  Noel Power <noel.power@suse.com>
* python/samba:  ndr_unpack needs bytes function
*  Anoop C S <anoopcs@redhat.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13872 BUG #13872]: s3SSLLA:lusterfs[_fuse]: Dynamically :mine NAME_MAX.
*  Christof Schmitt <cs@samba.org>
* passdb: : ABI to 0.27.2.
* [https://bugzilla.samba.org/show_bug.cgi?id=13813 BUG #13813]: libSSLL:ind_util: Add win:o_sid for --without-winbind.
* [https://bugzilla.samba.org/show_bug.cgi?id=13865 BUG #13865]: memcach: EIncrease  of default memcache to 512k.
*  Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13823 BUG #13823]: lib:uti: EMove :  :  for mkdir failing to log level 1.
* [https://bugzilla.samba.org/show_bug.cgi?id=13832 BUG #13832]: Printin: via smbspool backend with Kerberos auth fails.
* [https://bugzilla.samba.org/show_bug.cgi?id=13847 BUG #13847]: s4:libr Fix :lation:of Samba.
* [https://bugzilla.samba.org/show_bug.cgi?id=13848 BUG #13848]: s3:lib::Fix :CCE message for adding cache entries.
* [https://bugzilla.samba.org/show_bug.cgi?id=13853 BUG #13853]: s3:waf::Fix :CCE: of makdev() macro on Linux.
* [https://bugzilla.samba.org/show_bug.cgi?id=13857 BUG #13857]: docs:SS:date : manpage for --max-protocol.
* [https://bugzilla.samba.org/show_bug.cgi?id=13861 BUG #13861]: 'net : join' to child domain fails when using "-U admin@forestroot".
*  Zhu Shangzhong <zhu.shangzhong@zte.com.cn>
* [https://bugzilla.samba.org/show_bug.cgi?id=13839 BUG #13839]: ctdb:SS:itialize : struct to zero before reparsing as IPV4.
*  Martin Schwenke <martin@meltin.net>
* [https://bugzilla.samba.org/show_bug.cgi?id=13838 BUG #13838]: ctdb :kage should not own system library directory.
* [https://bugzilla.samba.org/show_bug.cgi?id=13860 BUG #13860]: CTDB :tarts failed NFS RPC services by hand, which is incompatible with systemd.
* [https://bugzilla.samba.org/show_bug.cgi?id=13888 BUG #13888]: ctdbDDA:n: Revert : can not assume that just because we could complete a TCP handshake".

 https://www.samba.org/samba/history/samba-4.9.7.html

Samba 4.9.6
------------------------

* Notes for Samba 4.9.6
 8, 2019

===============================
This is a security release in order to address the following defects:

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3870 CVE-2019-3870] (World writable files in Samba AD DC private/ dir)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3880 CVE-2019-3880] (Save registry file outside share as unprivileged user)

===============================
Details
===============================

------------------------

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3870 CVE-2019-3870]: : the provision of a new Active Directory DC, some files in the private/directory are created world-writable.
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3880 CVE-2019-3880]: :cated users with write permission can trigger a symlink traversal to write or detect files outside the Samba share.

For more details and workarounds, please refer to the security advisories. 
* [https://www.samba.org/samba/security/CVE-2019-3870.html CVE-2019-3870]
* [https://www.samba.org/samba/security/CVE-2019-3880.html CVE-2019-3880]

===============================
Changes since 4.9.5:

*  Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13834 BUG #13834] : [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2019-3870 CVE-2019-3870]: pysmbd: EnsureSS: zero :sk is set for smbd.mkdir().
*  Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13851 BUG #13851]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2019-3880 CVE-2019-3880]: rpc: winreg: :ve i:ions of:SaveKey/RestoreKey.

 https://www.samba.org/samba/history/samba-4.9.6.html

Samba 4.9.5
------------------------

* Notes for Samba 4.9.5
 12, 2019

===============================
Changes since 4.9.4:

* Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13714 BUG #13714]: audit_l Remove  log header and JSON Authentication: prefix.:
* [https://bugzilla.samba.org/show_bug.cgi?id=13760 BUG #13760]: Fix :ade from 4.7 (or earlier) to 4.9.
*  Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11495 BUG #11495]: s3: : nm EnsureS:  limitS:he NetBIOS name correctly. CID: 1433607.:
* [https://bugzilla.samba.org/show_bug.cgi?id=13690 BUG #13690]: smbd:SS:d: Do crash  'force group' is added to an existing share connection.
* [https://bugzilla.samba.org/show_bug.cgi?id=13770 BUG #13770]: s3: : vf:OTT  the NetAtalk deny mode compatibility code.
* [https://bugzilla.samba.org/show_bug.cgi?id=13803 BUG #13803]: s3: : PO mkdir does case insensitive name lookup.
*  Christian Ambach <ambi@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13199 BUG #13199]: s3:util:Hsmbget : recursive download with empty source directories.
*  Douglas Bagnall <douglas.bagnall@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=13716 BUG #13716]: sambaDD: drs showrepl: Do notS:rash if no dnsHostName found.
*  Tim Beale <timbeale@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=13736 BUG #13736]: s3:libs cli_smb2_  sometimes fail initially on a connection.
* [https://bugzilla.samba.org/show_bug.cgi?id=13747 BUG #13747]: join:SS:row C:r instead of Exception for simple errors.
* [https://bugzilla.samba.org/show_bug.cgi?id=13762 BUG #13762]: ldb: :id i: one-level searches.
* Ralph Boehme <slow@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13736 BUG #13736]: s3: :mb:  smb2cli_conn:_size() in cli_smb2_list().
* [https://bugzilla.samba.org/show_bug.cgi?id=13776 BUG #13776]: tldap:S:void u: Eafter free errors.
* [https://bugzilla.samba.org/show_bug.cgi?id=13802 BUG #13802]: Fix :p xid2sid cache churn.
* [https://bugzilla.samba.org/show_bug.cgi?id=13812 BUG #13812]: access_:allowed() doesn't process "Owner Rights" ACEs.
* Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13720 BUG #13720]: s3DDAAS: Avoid a fsp is always intact after close_file call.
* [https://bugzilla.samba.org/show_bug.cgi?id=13725 BUG #13725]: s3DDAAS:SSHHfruit: Add clo: Ecall.
* [https://bugzilla.samba.org/show_bug.cgi?id=13746 BUG #13746]: s3DDAAS: Use fru: string forS:DNS registration.
* [https://bugzilla.samba.org/show_bug.cgi?id=13774 BUG #13774]: s3DDAAS add glu:  vfs module.
*  David Disseldorp <ddiss@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13766 BUG #13766]: printin: ECheck l:nters() prior to pcap cache update.
* [https://bugzilla.samba.org/show_bug.cgi?id=13807 BUG #13807]: vfs_cep: Evfs_ceph :llocate_ftruncate calls (local FS) ftruncate and fallocate.
*  Philipp Gesang <philipp.gesang@intra2net.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13737 BUG #13737]: libSSLL:t_logging: Actually : talloc.
*  Joe Guo <joeg@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=13728 BUG #13728]: netcmdS:ser: python[3]DDAASSH unsupported and replaced by python[3]-gpg.
*  Aaron Haslett <aaronhaslett@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=13738 BUG #13738]: dns: :nging : search for wildcard to subtree.
*  Björn Jacke <bj@sernet.de>
* [https://bugzilla.samba.org/show_bug.cgi?id=13721 BUG #13721]: sambaDD:: Don't p backtrace on simple DNS errors.
* [https://bugzilla.samba.org/show_bug.cgi?id=13759 BUG #13759]: sambaun: Use the:right escaped oder unescaped sam ldb files.
*  Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13742 BUG #13742]: ctdb:SS:int l latency in machinereadable stats.
* [https://bugzilla.samba.org/show_bug.cgi?id=13786 BUG #13786]: message Messaging  stuck when pids are recycled.
*  Gary Lockyer <gary@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=13715 BUG #13715]: audit_l auth_json_auditS: quired auth_json.
* [https://bugzilla.samba.org/show_bug.cgi?id=13765 BUG #13765]: man :s: Document : process model.
* [https://bugzilla.samba.org/show_bug.cgi?id=13773 BUG #13773]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2019-3824 CVE-2019-3824] ldb: Release ldb :TT4.6.
*  Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13697 BUG #13697]: s3:auth: ignore : _bu:ts() failing without a valid idmap configuration.
* [https://bugzilla.samba.org/show_bug.cgi?id=13722 BUG #13722]: s3:auth: Ign a : winbindd as NT4 PDC/BDC without trusts.
* [https://bugzilla.samba.org/show_bug.cgi?id=13723 BUG #13723]: s3:auth: ret NT_STATUS:SERVERS if winbindd is not available.
* [https://bugzilla.samba.org/show_bug.cgi?id=13752 BUG #13752]: s4:serv Add :t : 'smbcontrol samba shutdown' and 'smbcontrol <pid> debug/debuglevel'.
*  Noel Power <noel.power@suse.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13616 BUG #13616]: Python:: nsure :n can doesn't rencoded str with py2.
*  Anoop C S <anoopcs@redhat.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13330 BUG #13330]: vfs_glu Adapt t: changes in libgfapi signatures.
* [https://bugzilla.samba.org/show_bug.cgi?id=13774 BUG #13774]: s3DDAAS Use ENO in errno comparison for getxattr.
*  Jura Sasek <jiri.sasek@oracle.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13704 BUG #13704]: notifyd: Fix SIG on sparc.
*  Christof Schmitt <cs@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13787 BUG #13787]: waf: :ck f: Elibnscd.
*  Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13770 BUG #13770]: s3:vfs::Correctly :ckS:f OFD locks should be enabled or not.
*  Martin Schwenke <martin@meltin.net>
* [https://bugzilla.samba.org/show_bug.cgi?id=13717 BUG #13717]: libSSLL:: Count a:trailing line that doesn't end in a newline.
* [https://bugzilla.samba.org/show_bug.cgi?id=13800 BUG #13800]: Recover: lock bug fixes.
*  Justin Stephenson <jstephen@redhat.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13726 BUG #13726]: s3: : Do:not  NET_FLAGS_ANONYMOUS with -k.
* [https://bugzilla.samba.org/show_bug.cgi?id=13727 BUG #13727]: s3:libs Honor :ble_ne option in smbsock_connect_send.
*  Ralph Wuerthner <ralph.wuerthner@de.ibm.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13741 BUG #13741]: vfs_fil Fix get:th_ino.
* [https://bugzilla.samba.org/show_bug.cgi?id=13744 BUG #13744]: vfs_fil Fix fsn:dir algorithm.

 https://www.samba.org/samba/history/samba-4.9.5.html

Samba 4.9.4
------------------------

* Notes for Samba 4.9.4
* 20, 2018

===============================
Major bug fixes include:

* dns: Fix CNAME loop prevention using counter regression [https://bugzilla.samba.org/show_bug.cgi?id=13600 BUG #13600].

===============================
Changes since 4.9.3:
*  Ralph Boehme <slow@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=9175 BUG #9175]: libcliS:mb: Don't o: status code.
* [https://bugzilla.samba.org/show_bug.cgi?id=12164 BUG #12164]: wbinfoS:DAASSHH-group-info 'NT AUTHORITY\System' does not work.
* [https://bugzilla.samba.org/show_bug.cgi?id=13661 BUG #13661]: Session:setup reauth fails to sign response.
* [https://bugzilla.samba.org/show_bug.cgi?id=13677 BUG #13677]: vfs_fru Validation  writes on AFP_AfpInfo stream.
* [https://bugzilla.samba.org/show_bug.cgi?id=13688 BUG #13688]: vfs_sha: Nicely  with attempts to open previous version for writing.
* [https://bugzilla.samba.org/show_bug.cgi?id=13455 BUG #13455]: Restori: Eprevious version of stream with vfs_shadow_copy2 fails with NT_STATUS_OBJECT_NAME_INVALID fsp->base_fsp->fsp_name.
*  Isaac Boukris <iboukris@gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13571 BUG #13571]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2018-16853 CVE-2018-16853]: Fix S4U2Self :h with MIT KDC build.
*  Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13708 BUG #13708]: s3DDAAS Prevent  pointer dereference in vfs_glusterfs.
*  Joe Guo <joeg@catalyst.net.nz>
* PEP8:  E231: missing:whitespace after ','.
*  Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13629 BUG #13629]: winbind: EFix cra: Ewhen taking profiles.
*  Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13600 BUG #13600]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2018-14629 CVE-2018-14629] dns: Fix CNAME  prevention using counter regression.
*  Garming Sam <garming@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=13686 BUG #13686]: 'sambaD:l user syscpasswords' fails on a domain with many DCs. 
*  Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13571 BUG #13571]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2018-16853 CVE-2018-16853]: Do not : if client is not set.
* [https://bugzilla.samba.org/show_bug.cgi?id=13679 BUG #13679]: lib:uti: EFix :SSSS:inter initializiation.
*  Martin Schwenke <martin@meltin.net>
* [https://bugzilla.samba.org/show_bug.cgi?id=13696 BUG #13696]: ctdbDDA:n: Exit wi: Eerror if a database directory does not exist.
*  Justin Stephenson <jstephen@redhat.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13498 BUG #13498]: s3:liba Add :PAACCE leave keep-account option.

 https://www.samba.org/samba/history/samba-4.9.4.html

Samba 4.9.3
------------------------

* Notes for Samba 4.9.3
* 27, 2018

===============================
This is a security release in order to address the following defects:

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-14629 CVE-2018-14629] Unprivileged adding of CNAME record causing loop in AD Internal DNS server
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16841 CVE-2018-16841] Double-free in Samba AD DC KDC with PKINIT
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16851 CVE-2018-16851] NULL pointer de-reference in Samba AD DC LDAP server
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16852 CVE-2018-16852] NULL pointer de-reference in Samba AD DC DNS servers
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16853 CVE-2018-16853] Samba AD DC S4U2Self crash in experimental MIT Kerberos configuration (unsupported)
*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16857 CVE-2018-16857] Bad password count in AD DC not always effective

===============================
Details
===============================

------------------------

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-14629 CVE-2018-14629]::
* :ons of Samba from 4.0.0 onwards are vulnerable to infinite query recursion caused by CNAME loops. Any dns record can be added via ldap by an unprivileged user using the ldbadd tool, so this is a security issue.

*  [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16841 CVE-2018-16841]::
* :igured to accept smart-card authentication, Samba's KDC will call talloc_free() twice on the same memory if the principal in a validly signed certificate does not match the principal in the AS-REQ.

* : only possible after authentication with a trusted certificate.

* : robust against further corruption from a double-free with talloc_free() and directly calls abort(), terminating the KDC process.

* : no further vulnerability associated with this issue, merely a denial of service.

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16851 CVE-2018-16851]::
* :  processing of an LDAP search before Samba's AD DC returns the LDAP entries to the client, the entries are cached in a single memory object with a maximum size of 256MB. When this size is reached, the Samba process providing the LDAP service will follow the NULL pointer, terminating the process.

* : no further vulnerability associated with this issue, merely a denial of service.

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16852 CVE-2018-16852]::
* :  processing of an DNS zone in the DNS management DCE/RPC server, the internal DNS server or the Samba DLZ plugin for BIND9, if the DSPROPERTY_ZONE_MASTER_SERVERS property or DSPROPERTY_ZONE_SCAVENGING_SERVERS property is set, the server will follow a NULL pointer and terminate.

* : no further vulnerability associated with this issue, merely a denial of service.

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16853 CVE-2018-16853]::
* in a Samba AD domain can crash the KDC when Samba is built in the non-default MIT Kerberos configuration.

* : advisory we clarify that the MIT Kerberos build of the Samba AD DC is considered experimental.  Therefore the Samba Team will not issue security patches for this configuration.

* [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16857 CVE-2018-16857]::
* Configurations watching for bad passwords (to restrict brute forcing of passwords) in a window of more than 3 minutes may not watch for bad passwords at all.

For more details and workarounds, please refer to the security advisories.

===============================
Changes since 4.9.2:

*  Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13628 BUG #13628]BUG 13628: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2018-16841 CVE-2018-16841]: heimdal: Fix :ault onS:KINIT with mis-matching principal.
* [https://bugzilla.samba.org/show_bug.cgi?id=13678 BUG #13678]BUG 13678: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2018-16853 CVE-2018-16853]: build: The : AD : when build with MIT Kerberos is experimental
*  Tim Beale <timbeale@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=13683 BUG #13683]BUG 13683: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2018-16857 CVE-2018-16857]: dsdb/util: :tly treat : rvationWindow as 64-bit int.
*  Joe Guo <joeg@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=13683 BUG #13683]BUG 13683: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2018-16857 CVE-2018-16857] PEP8: Fix E305: :d 2 blank:lines after class or function definition, found 1.
*  Aaron Haslett <aaronhaslett@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=13600 BUG #13600]BUG 13600: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2018-14629 CVE-2018-14629]: dns: CNAME : preve using counter.
*  Gary Lockyer <gary@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=13669 BUG #13669]BUG 13669: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2018-16852 CVE-2018-16852]: Fix NULL : de-reference in Samba AD DC DNS management.
*  Garming Sam <garming@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=13674 BUG #13674]BUG 13674: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2018-16851 CVE-2018-16851]: ldap_server: Che: Eret before:manipulating blob.

 https://www.samba.org/samba/history/samba-4.9.3.html

Samba 4.9.2
------------------------

* Notes for Samba 4.9.2
* 08, 2018

===============================
This is the latest stable release of the Samba 4.9 release series.
===============================

------------------------

===============================
Changes since 4.9.1:

*  Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13418 BUG #13418]: dsdb:SS:d com explaining the limitations of our current backlink behaviour.
* [https://bugzilla.samba.org/show_bug.cgi?id=13621 BUG #13621]: Fix :lems running domain backups (handling SMBv2, sites).
*  Tim Beale <timbeale@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=13621 BUG #13621]: Fix :lems running domain backups (handling SMBv2, sites).
*  Ralph Boehme <slow@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13465 BUG #13465]: testpar: EFix cra with PANIC: Messaging not :tialized on SLES 12 SP3.
* [https://bugzilla.samba.org/show_bug.cgi?id=13642 BUG #13642]: Make :_fruit able to cleanup AppleDouble files.
* [https://bugzilla.samba.org/show_bug.cgi?id=13646 BUG #13646]: File :ing issues with vfs_fruit on samba >= 4.8.5.
* [https://bugzilla.samba.org/show_bug.cgi?id=13649 BUG #13649]: Enablin: vfs_fruit looses FinderInfo.
* [https://bugzilla.samba.org/show_bug.cgi?id=13667 BUG #13667]: Cancell of SMB2 aio reads and writes returns wrong error NT_STATUS_INTERNAL_ERROR.
*  Amitay Isaacs <amitay@gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13641 BUG #13641]: Fix : recovery record resurrection from inactive nodes and simplify vacuuming.
*  Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13465 BUG #13465]: example: EFix the:smb2mount build.
* [https://bugzilla.samba.org/show_bug.cgi?id=13629 BUG #13629]: libteve Fix bui: Edue to missing open_memstream on Illiumos.
* [https://bugzilla.samba.org/show_bug.cgi?id=13662 BUG #13662]: winbind: Fix tim calculation for sid<->name cache.
*  Gary Lockyer <gary@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=13653 BUG #13653]: dsdb :rypted_secrets: Allow &:SLLAASSHH/ andSS:uot;mdb://" in : path.
*  Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13418 BUG #13418]: Extende: DN SID component missing for member after switching group membership.
* [https://bugzilla.samba.org/show_bug.cgi?id=13624 BUG #13624]: ReturnS:TATUS_SESSION_EXPIRED error encrypted, if the request was encrypted.
*  David Mulder <dmulder@suse.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13621 BUG #13621]: python::Allow f signing via smb.SMB().
* [https://bugzilla.samba.org/show_bug.cgi?id=13665 BUG #13665]: lib:soc If :ng :y, set ifaces.
*  Noel Power <noel.power@suse.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13616 BUG #13616]: ldb: :p ld: version to 1.4.3, Python: Ensure ldb.: Ecan accept utf8 encoded unicode.
*  Christof Schmitt <cs@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13465 BUG #13465]: testpar: EFix cra with PANIC: Messaging not :tialized on SLES 12 SP3.
* [https://bugzilla.samba.org/show_bug.cgi?id=13673 BUG #13673]: smbd:SS:x DEL:SE behaviour on files with READ_ONLY attribute.
*  Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13601 BUG #13601]: waf: : DDA:k-clash-protection.
* [https://bugzilla.samba.org/show_bug.cgi?id=13668 BUG #13668]: winbind: Fix seg if an invalid passdb backend is configured.
*  Martin Schwenke <martin@meltin.net>
* [https://bugzilla.samba.org/show_bug.cgi?id=13659 BUG #13659]: Fix : in CTDB event handling.
* [https://bugzilla.samba.org/show_bug.cgi?id=13670 BUG #13670]: Misbeha nodes are sometimes not banned.

 https://www.samba.org/samba/history/samba-4.9.2.html

Samba 4.9.1
------------------------

* Notes for Samba 4.9.1
* 24, 2018

===============================
This is the latest stable release of the Samba 4.9 release series.
===============================

------------------------

===============================
Major enhancements include:
*  s3: nmbd:  nmbd network announce storm ([https:/SSLLAAS:a.samba.org/show_bug.cgi?id=13620 BUG #13620]).

===============================
Changes since 4.9.0:

*  Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13620 BUG #13620]: s3: :: S nmbd : announce storm.
*  Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13597 BUG #13597]: s3DDAAS:nt: Use spo:spoolss_UserLevel1 in winspool cmds.
*  Martin Schwenke <martin@meltin.net>
* [https://bugzilla.samba.org/show_bug.cgi?id=13617 BUG #13617]: CTDB :overy lock has some race conditions.
*  Justin Stephenson <jstephen@redhat.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13597 BUG #13597]: s3DDAAS: nt: Advertise : 7 client info.
*Ralph Wuerthner <ralph.wuerthner@de.ibm.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13610 BUG #13610]: ctdbDDA: Remove  option from ctdbd_wrapper man page.

 https://www.samba.org/samba/history/samba-4.9.1.html

Samba 4.9.0
------------------------

<onlyinclude>
* Notes for Samba 4.9.0
* 13, 2018
===============================
Release Announcements
===============================

------------------------

This is the first stable release of the Samba 4.9 release series. Please read the release notes carefully before upgrading.

===============================
NEW FEATURES/CHANGES
===============================

------------------------

'net ads setspn'=
===============================

------------------------

There is a new 'net ads setspn' sub command for managing Windows SPN(s) on the AD. This command aims to give the basic functionality that is provided on windows by 'setspn.exe' e.g. ability to add, delete and list Windows SPN(s) stored in a Windows AD Computer object.

The format of the command is:

 net ads setspn list [machine]
 net ads setspn [add | delete ] SPN [machine]

'machine' is the name of the computer account on the AD that is to be managed.
If 'machine' is not specified the name of the 'client' running the command is used instead.

The format of a Windows SPN is
    serviceclass/host:ASSHHservicename' (servicename and port are optional)

serviceclass/host is generally sufficient to specify a host based service.

'net ads keytab' changes=
===============================

------------------------

net ads keytab add no longer attempts to convert the passed serviceclass (e.g. nfs, html etc.) into a Windows SPN which is added to the Windows AD computer object. By default just the keytab file is modified.

A new keytab subcommand 'add_update_ads' has been added to preserve the legacy behaviour. However the new 'net ads setspn add' subcommand should really be used instead.

net ads keytab create no longer tries to generate SPN(s) from existing entries in a keytab file. If it is required to add Windows SPN(s) then 'net ads setspn add' should be used instead.

Local authorization plugin for MIT Kerberos=
===============================

------------------------

This plugin controls the relationship between Kerberos principals and AD accounts through winbind. The module receives the Kerberos principal and the local account name as inputs and can then check if they match. This can resolve issues with canonicalized names returned by Kerberos within AD. If the user tries to log in as 'alice', but the samAccountName is set to ALICE (uppercase), Kerberos would return ALICE as the username. Kerberos would not be able to map 'alice' to 'ALICE' in this case and auth would fail.  With this plugin account names can be correctly mapped. This only applies to GSSAPI authentication, not for getting the initial ticket granting ticket.

VFS audit modules=
===============================

------------------------

The vfs_full_audit module has changed its default set of monitored successful and failed operations from "all" to "none". That helps to prevent potential denial of service caused by simple addition of the module to the VFS objects.

Also, modules vfs_audit, vfs_ext_audit and vfs_full_audit now accept any valid syslog(3) facility, in accordance with the manual page.

Database audit support=
===============================

------------------------

Changes to the Samba AD's sam.ldb database are now logged to Samba's debug log under the "dsdb_audit" debug class and "dsdb_json_audit" for JSON formatted log entries.

Transaction commits and roll backs are now logged to Samba's debug logs under the "dsdb_transaction_audit" debug class and "dsdb_transaction_json_audit" for JSON formatted log entries.

Password change audit support=
===============================

------------------------

Password changes in the AD DC are now logged to Samba's debug logs under the "dsdb_password_audit" debug class and "dsdb_password_json_audit" for JSON formatted log entries.

Group membership change audit support=
===============================

------------------------

Group membership changes on the AD DC are now logged to Samba's debug log under the "dsdb_group_audit" debug class and "dsdb_group_json_audit" for JSON formatted log entries.

Log Authentication duration=
===============================

------------------------

For NTLM and Kerberos KDC authentication, the authentication duration is now logged. Note that the duration is only included in the JSON formatted log entries.

JSON library Jansson required for the AD DC=
===============================

------------------------

By default, the Jansson JSON library is required for Samba to build. It is strictly required for the Samba AD DC, and is optional for builds "--without-ad-dc" by specifying "--without-json-audit" at configure time.

New Experimental LMDB LDB backend=
===============================

------------------------

A new Experimental LDB backend using LMDB is now available. This allows databases larger than 4Gb (Currently the limit is set to 6Gb, but this will be increased in a future release). To enable lmdb, provision or join a domain using the "--backend-store=mdb" option.

This requires that a version of lmdb greater than 0.9.16 is installed and that samba has not been built with the "--without-ldb-lmdb" option.

Please note this is an experimental feature and is not recommended for production deployments.

Password Settings Objects=
===============================

------------------------

Support has been added for `Password Settings Objects|PSOs  | Password Settings Objects `(PSOs). This AD feature is also known as Fine-Grained Password Policies (FGPP).

PSOs allow AD administrators to override the domain password policy settings for specific users, or groups of users. For example, PSOs can force certain users to have longer password lengths, or relax the complexity constraints for other users, and so on. PSOs can be applied to groups or to individual users. When multiple PSOs apply to the same user, essentially the PSO with the best precedence takes effect.

PSOs can be configured and applied to users/groups using the 'samba-tool domain passwordsettings pso' set of commands.

Domain backup and restore=
===============================

------------------------

A new 'samba-tool' command has been added that allows administrators to `Domain_backup_and_rename| create a backup-file of their domain DB`. In the event of a catastrophic failure of the domain, this backup-file can be used to restore Samba services.

The new 'samba-tool domain backup online' command takes a snapshot of the domain DB from a given DC. In the event of a catastrophic DB failure, all DCs in the domain should be taken offline, and the backup-file can then be used to recreate a fresh new DC, using the 'samba-tool domain backup restore' command. Once the backed-up domain DB has been restored on the new DC, other DCs can then subsequently be joined to the new DC, in order to repopulate the Samba network.

Domain rename tool=
===============================

------------------------

Basic support has been added for renaming a Samba domain. The `Domain_backup_and_rename|rename feature` is designed for the following cases:
# Running a temporary alternate domain, in the event of a catastrophic failure of the regular domain. Using a completely different domain name and realm means that the original domain and the renamed domain can both run at the same time, without interfering with each other. This is an advantage over creating a regular 'online' backup - it means the renamed/alternate domain can provide core Samba network services, while trouble-shooting the fault on the original domain can be done in parallel.
# Creating a realistic lab domain or pre-production domain for testing.

Note that the renamed tool is currently not intended to support a long-term rename of the production domain. Currently renaming the GPOs is not supported and would need to be done manually.

The domain rename is done in two steps:
 the 'samba-tool domain backup rename' command will clone the domain DB, renaming it in the process, and producing a backup-file.
 the 'samba-tool domain backup restore' command takes the backup-file and restores the renamed DB to disk on a fresh DC.

New samba-tool options for diagnosing DRS replication issues=
===============================

------------------------

The 'samba-tool drs showrepl' command has two new options controlling the output. With --summary, the command says very little when DRS replication is working well. With --json, JSON is produced. These options are intended for human and machine audiences, respectively.

The 'samba-tool visualize uptodateness' visualizes replication lag as a heat-map matrix based on the DRS uptodateness vectors. This will show you if (but not why) changes are failing to replicate to some DCs.

Automatic site coverage and GetDCName improvements=
===============================

------------------------

Samba's AD DC now automatically claims otherwise empty sites based on which DC is the nearest in the replication topology.

This, combined with efforts to correctly identify the client side in the GetDCName Netlogon call will improve service to sites without a local DC.

Improved 'samba-tool computer' command=
===============================

------------------------

The 'samba-tool computer' command allow manipulation of computer accounts including creating a new computer and resetting the password. This allows an 'offline join' of a member server or workstation to the Samba AD domain.

New 'samba-tool ou' command=
===============================

------------------------

The new 'samba-tool ou' command allows to manage organizational units.

Available subcommands are:
    reate       - Create an organizational unit.
    elete       - Delete an organizational unit.
    ist         - List all organizational units
    istobjects  - List all objects in an organizational unit.
    ove         - Move an organizational unit.
    ename       - Rename an organizational unit.

In addition to the ou commands, there are new subcommands for the user
and group management, which can make use of the organizational units:
    roup move   - Move a group to an organizational unit/container.
    ser move    - Move a user to an organizational unit/container.
    ser show    - Display a user AD object.

Samba performance tool now operates against Microsoft Windows AD=
===============================

------------------------

The Samba AD performance testing tool 'traffic_reply' can now operate against a Windows based AD domain.  Previously it only operated correctly against Samba.

DNS entries are now cleaned up during DC demote=
===============================

------------------------

DNS records are now cleaned up as part of the 'samba-tool domain demote' including both the default and '--remove-other-dead-server' modes.

Additionally, DNS records can be automatically cleaned up for a given name with the 'samba-tool dns cleanup' command, which aids in cleaning up partially removed DCs.

samba-tool ntacl sysvolreset is now much faster=
===============================

------------------------

The `Sysvolreset|  'samba-tool ntacl sysvolreset'` command, used on the Samba AD DC, is now much faster than in previous versions, after an internal rework.

Samba now tested with CI GitLab=
===============================

------------------------

Samba developers now have pre-commit testing available in [https://gitlab.com/samba-team/devel/samba GitLab], giving reviewers confidence that the submitted patches pass a `Samba_CI_on_gitlab| full CI before being submitted` to the Samba Team's own autobuild system.

Dynamic DNS record scavenging support=
===============================

------------------------

It is now possible to enable scavenging of DNS Zones to remove DNS records that were dynamically created and have not been touched in some time.

This support should however only be enabled on new zones or new installations.  Sadly old Samba versions suffer from [https://bugzilla.samba.org/show_bug.cgi?id=12451 BUG #12451] and mark dynamic DNS records as static and static records as dynamic. While a dbcheck rule may be able to find these in the future, currently a reliable test has not been devised.

Finally, there is not currently a command-line tool to enable this feature, currently it should be enabled from the DNS Manager tool from Windows. Also the feature needs to have been enabled by setting the smb.conf parameter "dns zone scavenging = yes".

Improved support for trusted domains (as AD DC)=
===============================

------------------------

The support for trusted domains/forests has been further improved.

External domain trusts, as well a transitive forest trusts, are supported in both directions (inbound and outbound) for Kerberos and NTLM authentication.

The following features are new in 4.9 (compared to 4.8):

* It's now possible to add users/groups of a trusted domain into domain groups. The group memberships are expanded on trust boundaries.
* foreignSecurityPrincipal objects (FPO) are now automatically created when members (as SID) of a trusted domain/forest are added to a group.
* The 'samba-tool group *members' commands allow members to be specified as foreign SIDs.

However there are currently still a few limitations:

* Both sides of the trust need to fully trust each other!
* No SID filtering rules are applied at all!
* This means DCs of domain A can grant domain admin rights in domain B.
* Selective (CROSS_ORGANIZATION) authentication is not supported. It's possible to create such a trust, but the KDC and winbindd ignore them.
* Samba can still only operate in a forest with just one single domain.

CTDB changes=
===============================

------------------------

There are many changes to CTDB in this release.

* Configuration has been completely overhauled
* Daemon and tool options are now specified in a new ctdb.conf Samba-style configuration file.  See ctdb.conf(5) for details.
* Event script configuration is no longer specified in the top-level configuration file.  It can now be specified per event script. For example, configuration options for the 50.samba event script can be placed alongside the event script in a file called 50.samba.options.  Script options can also be specified in a new  script.options file.  See ctdb-script.options(5) for details.
* Options that affect CTDB startup should be configured in the distribution-specific configuration file.  See ctdb.sysconfig(5) for details.
* Tunable settings are now loaded from ctdb.tunables.  Using CTDB_SET_TunableVariable=<value> in the main configuration file is no longer supported.  See ctdb-tunables(7) for details.
* : script to migrate an old-style configuration to the new style is available in ctdb/doc/examples/config_migrate.sh.

* The following configuration variables and corresponding ctdbd command-line options have been removed and not replaced with counterparts in the new configuration scheme:

    CTDB_PIDFILE                      --pidfile
    CTDB_SOCKET			     --socket
    CTDB_NODES			     --nlist
    CTDB_PUBLIC_ADDRESSES	     --public-addresses
    CTDB_EVENT_SCRIPT_DIR	     --event-script-dir
    CTDB_NOTIFY_SCRIPT		     --notification-script
    CTDB_PUBLIC_INTERFACE	     --public-interface
    CTDB_MAX_PERSISTENT_CHECK_ERRORS  --max-persistent-check-errors

* Td/ subdirectory of  the configuration directory are now run by unconditionally.
* Interfaces for public IP addresses must always be specified in the
* : file using the currently supported format.

* :ted items that have been removed are::

* The ctdb command's --socket command-line option
* The ctdb command's CTDB_NODES environment variable
* :ing tests there are still mechanisms available to change the locations of certain directories and files.

* The following ctdbd.conf and ctdbd options have been replaced by new   ctdb.conf options:

    CTDB_LOGGING/--logging                     logging  -> location
    CTDB_DEBUGLEVEL/-d                         logging  -> log level
    CTDB_TRANSPORT/--transport                 cluster  -> transport
    CTDB_NODE_ADDRESS/--listen                 cluster  -> node address
    CTDB_RECOVERY_LOCK/--reclock               cluster  -> recovery lock
    CTDB_DBDIR/--dbdir                         database -> volatile database directory
    CTDB_DBDIR_PERSISTENT/--dbdir-persistent   database -> peristent database directory
    CTDB_DBDIR_STATE/--dbdir-state             database -> state database directory
    CTDB_DEBUG_LOCKS                           database -> lock debug script
    CTDB_DEBUG_HUNG_SCRIPT                     event    -> debug script
    CTDB_NOSETSCHED/--nosetsched               legacy   -> realtime scheduling
    CTDB_CAPABILITY_RECMASTER/--no-recmaster   legacy   -> recmaster capability
    CTDB_CAPABILITY_LMASTER/--no-lmaster       legacy   -> lmaster capability
    CTDB_START_AS_STOPPED/--start-as-stopped   legacy   -> start as stopped
    CTDB_START_AS_DISABLED/--start-as-disabled legacy   -> start as disabled
    CTDB_SCRIPT_LOG_LEVEL/--script-log-level   legacy   -> script log level

* Event scripts have moved to the scripts/legacy subdirectory of the   configuration directory
* : EEvent scripts must now end with a ".script" suffix.

* The "ctdb event" command has changed in 2 ways:
* A component is now required for all commands
* : release the only valid component is "legacy".
* There is no longer a default event when running "ctdb event status"
* :he status of the "monitor" event is now done via::
* :: Estatus legacy monitor
* :1) for details.

* The following service-related event script options have been removed:

    CTDB_MANAGES_SAMBA
    CTDB_MANAGES_WINBIND
    CTDB_MANAGES_CLAMD
    CTDB_MANAGES_HTTPD
    CTDB_MANAGES_ISCSI
    CTDB_MANAGES_NFS
    CTDB_MANAGES_VSFTPD
    CTDB_MANAGED_SERVICES

* :ipts for services are now disabled by default.  To enable an event script and, therefore, manage a service use a command like the following::

    ctdb event script enable legacy 50.samba

* Notification scripts have moved to the scripts/notification subdirectory of the configuration directory
* :tion scripts must now end with a ".script" suffix.
* Support for setting CTDB_DBDIR=tmpfs has been removed
* :ure has not been implemented in the new configuration system.  If this is desired then a tmpfs filesystem should be manually mounted on the directory pointed to by the "volatile database directory" option.  See ctdb.conf(5) for more details.
* The following tunable options are now ctdb.conf options:
    DisabledIPFailover    failover -> disabled
    TDBMutexEnabled       database -> tdb mutexes
* Support for the NoIPHostOnAllDisabled tunable has been removed
* : EIf all nodes are unhealthy or disabled then CTDB will not host public IP addresses.  That is, CTDB now behaves as if NoIPHostOnAllDisabled were set to 1.
* The onnode command's CTDB_NODES_FILE environment variable has been removed
* :SHHf option can still be used to specify an alternate node file.
* The 10.external event script has been removed
* The CTDB_SHUTDOWN_TIMEOUT configuration variable has been removed
* : other daemons, if ctdbd does not shut down when requested then manual intervention is required.  There is no safe way of automatically killing ctdbd after a failed shutdown.
* CTDB_SUPPRESS_COREFILE and CTDB_MAX_OPEN_FILES configuration variable have been removed
* :uld be setup in the systemd unit/system file or, for SYSV init, in the distribution-specific configuration file for the ctdb service.
* CTDB_PARTIALLY_ONLINE_INTERFACES incompatibility no longer enforced
* and 91.lvs will no longer fail if CTDB_PARTIALLY_ONLINE_INTERFACES=yes.  The incompatibility is, however, well documented.  This option will be removed in future and replaced by sensible behaviour where public IP addresses simply switch interfaces or become unavailable when interfaces are down.
* Configuration file /etc/ctdb/sysconfig/ctdb is no longer supported

GPO Improvements=
===============================

------------------------

The 'samba_gpoupdate' command (used in applying Group Policies to the samba machine itself) has been renamed to 'samba_gpupdate' and had the syntax changed to better match the same tool on Windows.

REMOVED FEATURES=
===============================

------------------------

===============================
smb.conf changes
===============================

------------------------

As the most popular Samba install platforms (Linux and FreeBSD) both support extended attributes by default, the parameters "map readonly", "store dos attributes" and "ea support" have had their defaults changed to allow better Windows fileserver compatibility in a default install.

    arameter Name                     Description             Default
    -------------                     -----------             -------
    ap readonly                       Default changed              no
    tore dos attributes               Default changed             yes
    a support                         Default changed             yes
    ull_audit:                 Default changed            none
    ull_audit:                 Default changed            none

===============================
VFS interface changes
===============================

------------------------

The VFS ABI interface version has changed to 39. Function changes are:

* SYNC: : Only :nc versions are used.
* EAD: : Only :AD or async versions are used.
* RITE: : Only :ITE or async versions are used.
* HMOD_ACL: : Only :OD is used.
* CHMOD_ACL: : Only :MOD is used.

Any external VFS modules will need to be updated to match these changes in order to work with 4.9.x.
</onlyinclude>
==============================

HANGES SINCE 4.9.0rc5
===============================

------------------------

*  Björn Baumbach <bb at sernet.de>
* [https://bugzilla.samba.org/show_bug.cgi?id=13605 BUG #13605]: samba_d: Honor ' zone scavenging' option, only update if needed.
*  Andreas Schneider <asn at samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13606 BUG #13606]: wafsamb: EFix 'ma: E-j<jobs>'.

===============================
CHANGES SINCE 4.9.0rc4
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13565 BUG #13565]: s3: : vf:it:  smb_fname_str_do_l only returns absolute pathnames.
*  Paulo Alcantara <paulo@paulo.ac>
* [https://bugzilla.samba.org/show_bug.cgi?id=13578 BUG #13578]: s3: :: D: not  over stderr when there is no log file.
*  Ralph Boehme <slow@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13549 BUG #13549]: Durable:Reconnect fails because cookie.allow_reconnect is not set.
*  Alexander Bokovoy <ab@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13539 BUG #13539]: krb5DDA:: Interdomain : uses different salt principal.
*  Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13441 BUG #13441]: vfs_fru Don't u the main file.
* [https://bugzilla.samba.org/show_bug.cgi?id=13602 BUG #13602]: smbd:SS:x aSS:mleak in async search ask sharemode.
*  Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=11517 BUG #11517]: Fix :a GPO issue when Trust is enabled.
* [https://bugzilla.samba.org/show_bug.cgi?id=13539 BUG #13539]: sambaDD:: Add &qu:KerberosSalt" attribute to 'user getpassword/syncpasswords'.
*  Martin Schwenke <martin@meltin.net>
* [https://bugzilla.samba.org/show_bug.cgi?id=13589 BUG #13589]: Fix : configuration issues.
* [https://bugzilla.samba.org/show_bug.cgi?id=13592 BUG #13592]: ctdbdSS:gs an error until it can successfully connect to eventd.

===============================
CHANGES SINCE 4.9.0rc3
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13585 BUG #13585]: s3: :: E get_real_: copes with empty pathnames.
*  Tim Beale <timbeale@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=13566 BUG #13566]: sambaSS:main backup online/rename commands force user to specify password on CLI.
*  Alexander Bokovoy <ab@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13579 BUG #13579]: wafsamb:Hsamba_abi: Always  ABI symbols which must be local.
*  Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13584 BUG #13584]: Fix  panic if fruit_access_check detects a locking conflict.
*  Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13567 BUG #13567]: Fix :ry and resource leaks.
* [https://bugzilla.samba.org/show_bug.cgi?id=13580 BUG #13580]: python::Fix pri: Ein dns_invalid.py.
*  Martin Schwenke <martin@meltin.net>
* [https://bugzilla.samba.org/show_bug.cgi?id=13588 BUG #13588]: Aliasin: issue causes incorrect IPv6 checksum.
* [https://bugzilla.samba.org/show_bug.cgi?id=13589 BUG #13589]: Fix : configuration issues.
*  Ralph Wuerthner <ralph.wuerthner@de.ibm.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13568 BUG #13568]: s3: : ti: fixS:andling ofS:oken_blob in smb_time_audit_offload_read_recv().

===============================
CHANGES SINCE 4.9.0rc2
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13453 BUG #13453]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2018-10858 CVE-2018-10858]: libsmb: HardenSS:bc_readdir_inter against returns from malicious servers.
*  Andrew Bartlett <abartlet@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13374 BUG #13374]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2018-1140 CVE-2018-1140]: ldbsearch '(dist:ame=abc)' and DNS query with escapes crashes, ldb: Release LDB 1DDO:5 for CVE-2018-1140
* [https://bugzilla.samba.org/show_bug.cgi?id=13552 BUG #13552]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2018-10918 CVE-2018-10918]: cracknames: FixS:oS (NULL :nter de-ref) when not servicePrincipalName is set on a user.
*  Tim Beale <timbeale@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=13434 BUG #13434]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2018-10919 CVE-2018-10919]: acl_read: Fix :uthorized : access via searches.
*  Samuel Cabrero <scabrero@suse.de>
* [https://bugzilla.samba.org/show_bug.cgi?id=13540 BUG #13540]: ctdb_mu:ados_helper: Set SIG signal handler.
*  Günther Deschner <gd@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13360 BUG #13360]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2018-1139 CVE-2018-1139] libcli/auth: Do not  ntlmv1 over SMB1 when it is disabled via "ntlm auth".
* [https://bugzilla.samba.org/show_bug.cgi?id=13529 BUG #13529]: s3DDAAS: do notS:nstall test_tldap.
*  David Disseldorp <ddiss@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13540 BUG #13540]: ctdb_mu:ados_helper: Fix dea via lock renewals.
*  Andrej Gessel <Andrej.Gessel@janztec.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13374 BUG #13374]: [http:S:SLLAASSHHcveDDO:OOTTorg/cgi-bin/cvename.cgi?name=CVE-2018-1140 CVE-2018-1140] Add NULL check for ldb_dn_get_casefold() in ltdb_index_dn_attr().
*  Amitay Isaacs <amitay@gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13554 BUG #13554]: ctdbDDA:d: Fix CID:1438155.
*  Volker Lendecke <vl@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13553 BUG #13553]: Fix : 1438243, (Unchecked return value) 1438244 (Unsigned compared against 0), 1438245 (Dereference before null check) and 1438246 (Unchecked return value).
* [https://bugzilla.samba.org/show_bug.cgi?id=13554 BUG #13554]: ctdb:SS:x aSS:t&paste error.
*  Oleksandr Natalenko <oleksandr@redhat.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13559 BUG #13559]: systemd: Only st smb when network interfaces are up.
*  Noel Power <noel.power@suse.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13553 BUG #13553]: Fix :as don't work with SMB2.
* [https://bugzilla.samba.org/show_bug.cgi?id=13563 BUG #13563]: s3SSLLA: Ensure  code is only called when quota support detected.
*  Anoop C S <anoopcs@redhat.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13204 BUG #13204]: s3SSLLA:b: Explicitly  delete_on_close token for rmdir.
*  Andreas Schneider <asn@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13561 BUG #13561]: s3:waf::Install :log to /usr/sbin.
*  Justin Stephenson <jstephen@redhat.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13562 BUG #13562]: Shorten:description in vfs_linux_xfs_sgid manual.

===============================
CHANGES SINCE 4.9.0rc1
===============================

------------------------

*  Jeremy Allison <jra@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13537 BUG #13537]: s3: :: S:sing :file = yes" with SMB2 can cause CPU spin.
*  Ralph Boehme <slow@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13535 BUG #13535]: s3: :: F: Epath  in smbd_smb2_create_durable_lease_check().
*  Alexander Bokovoy <ab@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13538 BUG #13538]: sambaDD: trust: Support : via netr_GetDcName.
* [https://bugzilla.samba.org/show_bug.cgi?id=13542 BUG #13542]: s4DDAAS: Only bu dsdb Python modules for AD DC.
*  Amitay Isaacs <amitay@gmail.com>
* [https://bugzilla.samba.org/show_bug.cgi?id=13520 BUG #13520]: Fix :ability issues on freebsd.
*  Gary Lockyer <gary@catalyst.net.nz>
* [https://bugzilla.samba.org/show_bug.cgi?id=13536 BUG #13536]: DNS :card search does not handle multiple labels correctly.
*  Stefan Metzmacher <metze@samba.org>
* [https://bugzilla.samba.org/show_bug.cgi?id=13308 BUG #13308]: sambaDD: domain trust: Fix tru: Ecompatibility to Windows Server 1709 and FreeIPA.
*  Martin Schwenke <martin@meltin.net>
* [https://bugzilla.samba.org/show_bug.cgi?id=13520 BUG #13520]: Fix :ability issues on freebsd.
* [https://bugzilla.samba.org/show_bug.cgi?id=13545 BUG #13545]: ctdbDDA:col: Fix CTD: compilation issues.
* [https://bugzilla.samba.org/show_bug.cgi?id=13546 BUG #13546]: ctdbDDA: Replace : reference to CTDB_DEBUG_HUNG_SCRIPT option.
* [https://bugzilla.samba.org/show_bug.cgi?id=13550 BUG #13550]: ctdbDDA: Provide  example script for migrating old configuration.
* [https://bugzilla.samba.org/show_bug.cgi?id=13551 BUG #13551]: ctdbDDA:: Implement : tool "script list" command.

===============================
KNOWN ISSUES
===============================

------------------------

`Release_Planning_for_Samba_4.9#Release_blocking_bugs`

    ttps://www.samba.org/samba/history/samba-4.9.0.html
----
`Category: Notes`