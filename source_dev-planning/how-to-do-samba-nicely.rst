How to do Samba: Nicely
    <namespace>0</namespace>
<last_edited>2020-06-09T22:LL:_edited>
<last_editor>Abartlet</last_editor>

==============================

ow to be an effective and professional member of the Samba user and development Community: Nicely
===============================

Samba strives to be a welcoming community, for users, developers and all who seek to use or improve Samba.

Samba is a Free Software project, and the quality of Samba is just as
much represented by the community we build as well as the software we
produce.

To aid this goal, the following are some hints of how we do community, collaboration and development together. 

Communication
------------------------

===============================
Mailing lists
===============================

------------------------

The Samba project is mailing list oriented.  Make sure you are subscribed to either or both of the [https://lists.samba.org/mailman/listinfo/samba-technical samba-technical] and [https://lists.samba.org/mailman/listinfo/samba samba] mailing lists, and be active on those lists, remembering to help fellow users and developers as much as you ask for assistance. 

Remember that these mailing lists are archived, so please be professional.  

Your posts will be read by current and future employers, across multiple cultures. 

Making things even harder, much context and nuance is lost in e-mail and other text-based communication.

If an email wouldn't be considered appropriate in your home, work, or college setting, it isn't appropriate on the Samba mail lists either, so please take care before pressing 'send'.

===============================
Asking Questions and Reporting issues
===============================

------------------------

Whether it be asking questions on the [https://lists.samba.org/mailman/listinfo/samba samba] mailing list, reporting issues on [https://lists.samba.org/mailman/listinfo/samba-technical samba-technical] or in [https:/SSLLAAS:a.samba.org the Samba Bugzilla] or confidentially reporting security issues to **security@samba.org**, please be clear, kind and include steps to reproduce your issue.

Nothing strikes fear into the heart of a software developer than the suggestion that there is the possibility of data corruption.  Similarly security issues are really important, and get a lot of developer attention away from the public view.  

Therefore, doing everything you can to describe the steps to reproduce your issue will make a big difference, and even better if you have an exploit or reproducer, which helps remove the doubt (and may end up in the testsuite for the patch once issued).  Please help those helping you by being proactive in providing debug logs, and ready to provide `Capture_Packets|network captures` (in private).

Likewise, if your concern comes from a thought experiment, say so clearly.  

Finally, please be humble:  We all have a emotional stake in Samba so a little 'I think' or 'please help me understand how I'm wrong' goes a long way! Please remember that to each other, we are volunteers, and we get to posts as we are able.  For [https://www.samba.org/samba/support/globalsupport.html Commercial Support] on an urgent or confidential basis, please contact an appropriate vendor, or your enterprise Linux distributor.  Supporting them supports Samba development too!

===============================
Answering questions
===============================

------------------------

Samba is critical to daily operations for many of our users.  For this and many other understandable reasons, our users may be a little frustrated, or under pressure by the time they reach the Samba mailing list.  When helping our users with their difficulties, please be gentle. 

Many Samba configurations are strange:  Samba has been a long-term part of networks for many years, and configurations are often based on internet examples or distribution default files from a decade ago or more.  As long as they work they tend to stay in use.  While we may chuckle at how much our users think needs to be configured, where possible try to help our users while keeping the unrelated settings that are otherwise working for them.

Collaboration and Contribution
------------------------

===============================
Proposing patches
===============================

------------------------

Patches are the lifeblood of Samba.  The more associated explanation of what the patch does or is intended to fix the better. If it's designed to fix a specific bug, please add a reference to the [https://bugzilla.samba.org Samba bugzilla] URL containing the bug in the form BUG: :/SSLLAAS:OOTT  Please see the step by step details `Contribute|on how to Contribute to Samba`.

===============================
Testing
===============================

------------------------

When `Contribute|proposing changes` to Samba via `Samba_CI_on_gitlab|GitLab`, a CI (automated test) run will be
started and the result will be visible to those reviewing your work.  You should ensure that the changes you make are covered by these tests.

You should clearly mention that the change is still a *work in progress* with a subject tag like **WIP if your work is not finished or you do not expect it to pass this test.

Currently the gitlab CI tests can be a little flaky, so failing these tests isn't necessarily a sign the patch is bad. If the tests fail, try re-starting the pipeline. If they always fail on the same test then the patch probably needs work. If they fail seemingly at random on other tests that are unrelated to the change you're trying to make (usually the python AD-DC tests) then the patch is probably OK.

===============================
Seeking Feedback
===============================

------------------------

Actively seek feedback for your changes, ideally when still a work in progress.  Others may have considered this area of Samba previously, or have an interest, their input can make your changes better.

===============================
Giving Feedback
===============================

------------------------

Giving feedback and reviewing the code of fellow developers is a vital task, because `CodeReview|code review` is required on all changes to Samba's itself.  

Be timely with feedback.  Particularly if you work full time on Samba, please put some time aside each day and week to review the work of your fellow community members.

Be practical with feedback.  If possible, feedback should be concrete, a revised patch is even better (as it leaves less room for confusion).  

Be positive with feedback.  Even if many changes are still needed, make sure to also comment on what is right about a change, or give a positive review to the part of the patch set that is already acceptable.

Give your feedback early if possible.  The earlier the feedback is given the easier it is to incorporate your thoughts.  Likewise, if changes have already been proposed a number of times, please help finish the patches off rather than suggest a total re-work.

Always play the ball, not the player.  All of us are invested deeply in the work we do, so be respectful of the effort put in to make the changes.  A few words of thanks and appreciation go a long way, especially when a concern needs to be raised. 

Finally, do avoid the ever-tempting risk of *bike-shedding*.

===============================
Accepting Feedback
===============================

------------------------

Be honoured that a fellow member of the community has taken the time to look at your changes, and be humble when accepting the feedback.  Try not to take feedback on the changes personally, but do seek to improve both personally and professionally in response to concerns or suggestions.

Take the time to work though the feedback given.  While it will sometimes seem to be out of all proportion to the change involved, respect those giving their time to comment on your work.

===============================
Requesting a change be reverted or withdrawn
===============================

------------------------

If possible, we prefer not to revert changes that have already gone into master, and to avoid withdrawing patches already proposed.  If changes need to be made to code that has gone into master, we would prefer a revision on top of the existing master code so we have a record of the technical decisions made in review. If there is a practical technical reason such a change should not be accepted or reviewed, please make this case on the appropriate list. If it's a case of an "oops, mistake" in the patch we'll be happy to pretend it never got posted.

Upholding this ideal
------------------------

We, as members of the [https://samba.org/samba/team Samba Team] and Samba community are responsible for upholding this ideal by example in our own behaviour.  Far more than any document, the *day-to-day behaviour of the community* will set and maintain the values of the community to new and old alike.

However there will be times when we fail to live to these ideals, or there may be new members of our community who are unaware of how we do Samba.

In these cases, please **gently** remind your fellow community members that this is how we behave, in and around the Samba community.

Feel free to link to this wiki page for clarification.

===============================
Contacts
===============================

------------------------

If you have an issue you'd like to raise privately and not on the lists, feel free to email any member of the Samba Project Leadership Committee directly to express your concern. The Samba Project Leadership Committee is elected by the Samba Team members and each person serves for a term of two years. Currently the elected Committee members are:

* Andrew Bartlett of Catalyst: <abartlet@samba.org>

* Jim McDonough of SUSE: <jmcd@samba.org>

* Karolin Seeger of SerNet: <kseeger@samba.org>