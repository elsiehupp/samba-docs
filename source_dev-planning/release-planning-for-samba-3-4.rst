Release Planning for Samba 3.4
    <namespace>0</namespace>
<last_edited>2013-05-17T13:13:26Z</last_edited>
<last_editor>Kseeger</last_editor>

With the release of Samba 4.0.0, Samba 3.4 has been marked **discontinued**.

Samba 3.4 discontinued
------------------------

(**Updated 11-December-2013**)

With the release of Samba 4.0.0, Samba 3.4 has been marked **discontinued**.

Samba 3.4.17 
------------------------

(**Updated 30-April-2012**)

* Monday, April 30 - Samba 3.4.17 **Security Release** has been released in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-2111 CVE-2012-2111 (Incorrect permission checks when granting/removing privileges can compromise file server security)].
    http://www.samba.org/samba/history/samba-3.4.17.html Release Notes Samba 3.4.17]

Samba 3.4.16 
------------------------

(**Updated 10-April-2012**)

* Tuesday, April 10 - Samba 3.4.16 **Security Release** has been released in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-1182 CVE-2012-1182 ("root" credential remote code execution)].
    http://www.samba.org/samba/history/samba-3.4.16.html Release Notes Samba 3.4.16]

Samba 3.4.15 
------------------------

(**Updated 23-August-2011**)

* Tuesday, August 23 - Samba 3.4.15 has been released.
    http://www.samba.org/samba/history/samba-3.4.15.html Release Notes Samba 3.4.15]

Please note that this will be the **last bugfix release** of Samba 3.4 series!

Samba 3.4.14 
------------------------

(**Updated 26-July-2011**)

* Tuesday, July 26 - Samba 3.4.14 has been released to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-2522 CVE-2011-2522] and [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-2694 CVE-2011-2694].
    http://www.samba.org/samba/history/samba-3.4.14.html Release Notes Samba 3.4.14]

Samba 3.4.13 
------------------------

(**Updated 21-April-2011**)

* Thursday, April 21 - Samba 3.4.13 has been released
    http://www.samba.org/samba/history/samba-3.4.13.html Release Notes Samba 3.4.13]

Samba 3.4.12 
------------------------

(**Updated 28-February-2011**)

* Monday, February 28 - Samba 3.4.12 has been released to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-0719 CVE-2011-0719].
    http://www.samba.org/samba/history/samba-3.4.12.html Release Notes Samba 3.4.12]

Samba 3.4.11 
------------------------

(**Updated 23-January-2011**)

* Sunday, January 23 - Samba 3.4.11 has been released to fix connections to port-139 only servers (broken in Samba 3.4.10, please see [https://bugzilla.samba.org/show_bug.cgi?id=7881 bug #7881] for details)
    http://www.samba.org/samba/history/samba-3.4.11.html Release Notes Samba 3.4.11]

Samba 3.4.10 
------------------------

(**Updated 22-January-2011**)

* Saturday, January 22 - Samba 3.4.10 has been released
    http://www.samba.org/samba/history/samba-3.4.10.html Release Notes Samba 3.4.10]

Samba 3.4.9 
------------------------

(**Updated 14-September-2010**)

* Tuesday, September 14 - Samba 3.4.9 has been released to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-3069 CVE-2010-3069].
    http://www.samba.org/samba/history/samba-3.4.9.html Release Notes Samba 3.4.9]

Samba 3.4.8 
------------------------

(**Updated 11-March-2010**)

* Tuesday, May 11 - Samba 3.4.8 has been released
    http://www.samba.org/samba/history/samba-3.4.8.html Release Notes Samba 3.4.8]

Samba 3.4.7 
------------------------

(**Updated 09-March-2010**)

* Monday, March 8 - Samba 3.4.7 has been released to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-0728 CVE-2010-0728].
    http://www.samba.org/samba/history/samba-3.4.7.html Release Notes Samba 3.4.7]

Samba 3.4.6 
------------------------

(**Updated 24-February-2010**)

* Wednesday, February 24 - Samba 3.4.6 has been released
    http://www.samba.org/samba/history/samba-3.4.6.html Release Notes Samba 3.4.6]

Samba 3.4.5 
------------------------

(**Updated 19-January-2010**)

* Tuesday, January 19 - Samba 3.4.5 has been released
    http://www.samba.org/samba/history/samba-3.4.5.html Release Notes Samba 3.4.5]

Samba 3.4.4 
------------------------

(**Updated 07-January-2010**)

* Thursday, January 7 - Samba 3.4.4 has been released
    http://www.samba.org/samba/history/samba-3.4.4.html Release Notes Samba 3.4.4]

Samba 3.4.3 
------------------------

(**Updated 29-October-2009**)

* Thursday, October 29 - Samba 3.4.3 has been released
    http://www.samba.org/samba/history/samba-3.4.3.html Release Notes Samba 3.4.3]

Samba 3.4.2 
------------------------

(**Updated 1-October-2009**)

* Thursday, October 1 - Samba 3.4.2 has been issued as **Security Release** to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1888 CVE-2009-2906],
[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-2906 CVE-2009-2906] and
[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-2813 CVE-2009-2813].
    http://www.samba.org/samba/history/samba-3.4.2.html Release Notes Samba 3.4.2]

Samba 3.4.1 
------------------------

(**Updated 9-September-2009**)

* Wednesday, September 9 - Samba 3.4.1 has been released
    http://www.samba.org/samba/history/samba-3.4.1.html Release Notes Samba 3.4.1]

Samba 3.4.0 
------------------------

(**Updated 3-July-2009**)

* Tuesday, June 2 - Samba 3.4.0pre2 has been released
* Thursday, April 30 - Samba 3.4.0pre1 has been released
* Friday, June 19 - Samba 3.4.0rc1 has been released
* Friday, July 3 - Samba 3.4.0 has been released
    http://www.samba.org/samba/history/samba-3.4.0.html Release Notes Samba 3.4.0]