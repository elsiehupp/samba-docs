SoC/
    <namespace>0</namespace>
<last_edited>2020-06-04T23:40:27Z</last_edited>
<last_editor>Abartlet</last_editor>

Questions about Samba's participation in the program or about a project idea, scope, or design should be mailed to [https://OOTTorg/mailman/listinfo//SHHtech/aDDAASSH/@lists.samba.org] (our public mailing list). Note that you are not limited to the project ideas listed here. Please email us if you have a new idea you would like to discuss.

If you are considering applying to SoC, please make sure to read the Student FAQ. Pay close attention as the review process for Samba applications will be stricter this year. It is very important you convince us that:

# you will, in fact, be able to do the work you are proposing
# you understand the scope of the problem

Best of luck to all the Summer of Code applicants. -- The Samba Team

Ideas
------------------------

`SoC/ject ideas` for potential students. You can also submit your own ideas which might be completely different from the ones we have suggested, but please discuss the idea with a team member before you submit.

Writing a SoC application
------------------------

You need to demonstrate:
* enthusiasm!
* skill in the area you want to work on
* thorough research of the project idea
* commitment to be part of the Samba project long term

Discuss your project
------------------------

Make sure you talk to possible mentors on the project IRC channel (#samba-technical on freenode) or mailing list (the [mailto:samba-technical@samba.org samba-technical mailing list]) before you submit your application. You should show them a draft application and listen carefully to any criticisms or comments they might have.

If you are interested in more than one of the project ideas, then you can submit multiple applications, but it might be better to instead discuss the project ideas with possible mentors and work out which one you really want to work on.