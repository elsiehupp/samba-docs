DCERPC Hardening
    <namespace>0</namespace>
<last_edited>2016-10-13T09:05:31Z</last_edited>
<last_editor>Metze</last_editor>

DCERPC Hardening
------------------------

TODO... For now see:
https://www.samba.org/~metze/presentations/2016/SDC/StefanMetzmacher_sdc2016_dcerpc_security_rev1-handout.pdf

Related Branches (work in progress!)
------------------------

* https://git.samba.org/?p=metze/samba/wip.git;a=shortlog;h=refs/heads/master4-dcerpc-protocol

Related Pages
------------------------

* `DCERPC|DCERPC`

Talks
------------------------

* https://www.samba.org/~metze/presentations/2016/SDC/StefanMetzmacher_sdc2016_dcerpc_security_rev1-handout.pdf
* https://www.samba.org/~metze/presentations/2016/metze_sambaxp2016_badlock-handout.pdf
* https://www.samba.org/~metze/presentations/2014/StefanMetzmacher_sdc2014_dcerpc-handout.pdf