DCERPC
    <namespace>0</namespace>
<last_edited>2016-10-13T09:04:20Z</last_edited>
<last_editor>Metze</last_editor>

DCERPC / Overview
------------------------

The samba DCERPC infrastructure should support all protocol features described in:
* DCE 1.1: Remote Procedure Call http://www.opengroup.org/onlinepubs/9629399/toc.htm
* MS-RPCE - Remote Procedure Call Protocol Extensions http://msdn.microsoft.com/en-us/library/cc243560%28PROT.10%29.aspx
* More links: http://www.dcerpc.org/documentation.html

Samba3 vs. Samba4
------------------------

We have four separate (all incomplete) implementations of DCERPC (two servers and two clients).
The aim is to merge the good parts of all implementations together and extend the result
to be more feature complete.

DCERPC Design Goals
------------------------

Base the whole infrastructure on talloc, tevent and tstream. All internals are fully async.
This will allow us to support also the most complex problems like:
* DCERPC pipes see:
** http://www.opengroup.org/onlinepubs/9629399/chap4.htm#tagcjh_08_02_14
** http://msdn.microsoft.com/en-us/library/cc243879%28PROT.10%29.aspx
** http://msdn.microsoft.com/en-us/library/aa367140%28VS.85%29.aspx
** http://msdn.microsoft.com/en-us/library/aa373629%28VS.85%29.aspx
* DCERPC callbacks see:
** http://msdn.microsoft.com/en-us/library/cc243711%28PROT.10%29.aspx
** http://msdn.microsoft.com/en-us/library/aa366746%28v=VS.85%29.aspx

DCERPC merge/ DONE
------------------------

* pidl: generic client stub bindings based on 'struct dcerpc_binding_handle'
* source4: change callers to explicit check r.out.result for NTSTATUS based functions
* librpc/dcerpc: generic 'struct dcerpc_binding_handle' based infrastructure for DCERPC Calls
* source4/librpc: provide a dcerpc_binding_handle implementation on top of the current 'dcerpc_pipe' infrastructure
* pidl: remove old Samba4 style stub bindings
* source3/rpc_client: provide a dcerpc_binding_handle implementation on top of the current 'rpc_pipe_client' infrastructure
* source4/irpc: provide a dcerpc_binding_handle implementation to hide IRPC specific details.
* python: use pidl bindings also for irpc calls.
* source3: convert the callers to use the dcerpc_binding_handle based stubs
* source3/rpc_client: hide rpc_cli_transport infrastructure behind a tstream implementation.
* source4/librpc: replace dcerpc_transport infrastructure by using tstream as interface to the low level transport.

DCERPC merge/ TODO
------------------------

* let python bindings explicitly use the dcerpc_binding_handle abstraction (better handling for multiple binding handles on the same connection).
* maybe use better ways to handle application level errors vs. exceptions in the python bindings.
* start to think about how we can have a common code base between the dcerpc_binding_handle layer and the tstream layer... (see master4-dcerpc-new branch)
* start to think about how to make the server part more generic... (see master4-dcerpc-new branch)

Related Branches (work in progress!)
------------------------

* https://git.samba.org/?p=metze/samba/wip.git;a=shortlog;h=refs/heads/master4-dcerpc
* https://git.samba.org/?p=metze/samba/wip.git;a=shortlog;h=refs/heads/master4-dcerpc-ok
* https://git.samba.org/?p=metze/samba/wip.git;a=shortlog;h=refs/heads/master4-dcerpc-base
* https://git.samba.org/?p=metze/samba/wip.git;a=shortlog;h=refs/heads/master4-dcerpc-new
* https://git.samba.org/?p=metze/samba/wip.git;a=shortlog;h=refs/heads/master4-pidl-pipe
* https://git.samba.org/?p=metze/samba/wip.git;a=shortlog;h=refs/heads/master3-gb
* https://git.samba.org/?p=metze/samba/wip.git;a=shortlog;h=refs/heads/master4-schannel
* https://git.samba.org/?p=metze/samba/wip.git;a=shortlog;h=refs/heads/master4-schannel-ok
* https://git.samba.org/?p=metze/samba/wip.git;a=shortlog;h=refs/heads/master4-schannel-base
* https://git.samba.org/?p=ddiss/samba.git;a=shortlog;h=refs/heads/async_fsrvp_srv_wip_xp2013

Related Pages
------------------------

* `DCERPC_Hardening|DCERPC Hardening`

Talks
------------------------

* https://www.samba.org/~metze/presentations/2014/StefanMetzmacher_sdc2014_dcerpc-handout.pdf