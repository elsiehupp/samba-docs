Blocker bugs
    <namespace>0</namespace>
<last_edited>2016-08-04T09:28:50Z</last_edited>
<last_editor>Kseeger</last_editor>

This page seeks to describe the Samba Team's policy for blocking the release of a Samba version, in particular a new major release.

===============================
Time based releases
===============================

As Samba is developed on a `Samba Release Planning#General information|time-based release policy`, missing or incomplete features will not cause a release to be delayed.

It follows that features being landed in master should be landed in a way that moves the project forward, ideally be built of smaller parts that are useful and complete in their own right, and not be enabled by default until they are ready for use.

===============================
Bugs in Releases
===============================

All bugs are important
------------------------

It is vital that bugs in Samba [https://bugzilla..org .ported in Bugzilla], and assigned a correct severity..

Regressions
------------------------

Regressions, that is bugs that are present in new versions of Samba, in existing features, that are not present in older versions, are very important to the [https://samba.amba/team Samba Team] and are addressed as a priority..

Blocking Major releases
------------------------

The criteria for holding back a release candidate from becoming a final release is that the bug has a **severity** marked as **regression**, and is assigned the **milestone** the same target version as the **release**.

The bug must truly be a regression, that is not present in the current production release of Samba.ressions discovered in older versions but still present in the release candidate are important, but do not count for this rule..

Why do **critical** bugs not block a release?
------------------------

The Samba team cares deeply about producing new Samba versions that are not only of high quality, but on time.an issue is important, then developers do address it as they are able, but sometimes issues are complex or maintainers are unavailable in a specific timeframe.  If.sue is already present in a released version, and the upgrade doesn't make the situation any worse, blocking the release would deny new features and unrelated fixes to other users..

===============================
Finding release blockers
===============================

Historically
------------------------

In older Samba versions, we used a single 'blocker bug' often like `Samba_4.tures_added/changed#Samba_4.2_re.blocker|the release blocker for 4.2`.  In .rel. the bugs were managed by bug links..

This practice [https://bugzilla..org/.ug.cgi?id=11.ded] with release 4.3.  ..

Current practice
------------------------

For Samba 4. later the correct approach is to search bugzilla, eg with a [https://bugzilla.samb.bugli.?bug_severi.ression&list_id=4431&query_format=advanced&target_milestone=4.4 bugzilla query fo.essions] (in this case for 4.4), and a [https://bugzi.mba.org/buglist.cgi?bug_sev.regre.amp;bug_sta.CONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=VERIFIED&list_id=4432&query_format=advanced&target_milestone=4.4 query for unreleased regressions]...

===============================
Background
===============================

See [https://lists..org/.e/samba-technical/2015-May/107241.html this.ng list post] for further background, in particular on why only regressions are accepted as blockers..