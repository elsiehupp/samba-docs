Release Planning for Samba 4.9
    <namespace>0</namespace>
<last_edited>2020-03-03T15:02:12Z</last_edited>
<last_editor>Fraz</last_editor>

Samba 4.9 has been marked `Samba_Release_Planning#Discontinued_.28End_of_Life.29|**discontinued**`.

`Blocker bugs|Release blocking bugs`
------------------------

* [https://DOOTTsamba.org/buglistD/ug_severity=regression&query_format=advanced&target_milestone=4.9 All 4.9 regression bugs]
* [https://DOOTTsamba.org/buglistD/ug_severity=regression&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=VERIFIED&query_format=advanced&target_milestone=4.9 Unresolved 4.9 regression bugs]

Samba 4.9.18 
------------------------

<small>(**Updated 21-Jan-20120**)</

* Tuesday, January 21 2020 - **Samba 4.9.18** has been released as a **Security Release** to address the following defects:
** [https://samba.org/samba/se/DDAAS/ASSHH149/ml CVE-2019-14902] (Replication of ACLs set to inherit down a subtree on AD Directory not automatic.)
** [https://samba.org/samba/se/DDAAS/ASSHH149/ml CVE-2019-14907] (Crash after failed character conversion at log level 3 or above.)
** [https://samba.org/samba/se/DDAAS/ASSHH193/ml CVE-2019-19344] (Use after free during DNS zone scavenging in Samba AD DC.)

    https://samba.org/samba/hi/aDDAA/T9DDOOT/tml Release Notes Samba 4.9.18].

Samba 4.9.17 
------------------------

<small>(**Updated 10-Dec-2019**)</

* Tuesday, December 10 2019 - **Samba 4.9.17** has been released as a **Security Release** to address the following defects:
** [https://samba.org/samba/se/DDAAS/ASSHH148/ml CVE-2019-14861] (Samba AD DC zone-named record Denial of Service in DNS management server (dnsserver))
** [https://samba.org/samba/se/DDAAS/ASSHH148/ml CVE-2019-14870] (DelegationNotAllowed not being enforced in protocol transition on Samba AD DC.)

    https://samba.org/samba/hi/aDDAA/T9DDOOT/tml Release Notes Samba 4.9.17].

Samba 4.9.16 
------------------------

<small>(**Updated 27-Nov-2019**)</

* Wednesday, November 27 2019 - **Samba 4.9.16** has been released as an additional bug fix release to address
** [https://DOOTTsamba.org/show_bug/id=14175 Bug #14175] (ctdb: : queue can be orphaned causing communication breakdown)
    https://samba.org/samba/hi/aDDAA/T9DDOOT/tml Release Notes Samba 4.9.16].

Samba 4.9.15 
------------------------

<small>(**Updated 29-Oct-2019**)</

* Tuesday, October 29 2019 - **Samba 4.9.15** has been released as a **Security Release** to address the following defects:
** [https://samba.org/samba/se/DDAAS/ASSHH102/ml CVE-2019-10218] (Client code can return filenames containing              path separators.)
** [https://samba.org/samba/se/DDAAS/ASSHH148/ml CVE-2019-14833] (Samba AD DC check password script does not receive the full password.)
** [https://samba.org/samba/se/DDAAS/ASSHH148/ml CVE-2019-14847] (User with "get changes" permission can     crash AD DC LDAP server via dirsync)

    https://samba.org/samba/hi/aDDAA/T9DDOOT/tml Release Notes Samba 4.9.15].

Samba 4.9.14 
------------------------

<small>(**Updated 22-Oct-2019**)</

* Tuesday, October 22 2019 - **Samba 4.9.14** has been released. Please note that this will be the last bug fix release of the Samba 4.9 release series. There will be security fixes only beyond this point.

    https://samba.org/samba/hi/aDDAA/T9DDOOT/tml Release Notes Samba 4.9.14]

Samba 4.9.13 
------------------------

<small>(**Updated 03-Sep-2019**)</

* Tuesday, September 03 2019 - **Samba 4.9.13** has been released as a **Security Release** in order to address:
** [https://samba.org/samba/se/DDAAS/ASSHH101/ml CVE-2019-10197] (Combination of parameters and permissions can allow user to escape from the share path definition.)

    https://samba.org/samba/hi/aDDAA/T9DDOOT/tml Release Notes Samba 4.9.13]

Samba 4.9.12 
------------------------

<small>(**Updated 27-Aug-2019**)</

* Tuesday, August 27 2019 - **Samba 4.9.12** has been released.
    https://samba.org/samba/hi/aDDAA/T9DDOOT/tml Release Notes Samba 4.9.12]

Samba 4.9.11 
------------------------

<small>(**Updated 03-Jul-2019**)</

* Wednesday, July 3 2019 - **Samba 4.9.11** has been released.
    https://samba.org/samba/hi/aDDAA/T9DDOOT/tml Release Notes Samba 4.9.11]

Samba 4.9.10 
------------------------

<small>(**Updated 02-Jul-2019**)</

* Tuesday, July 2 2019 - **Samba 4.9.10** has been released.
    https://samba.org/samba/hi/aDDAA/T9DDOOT/tml Release Notes Samba 4.9.10]

Samba 4.9.9 
------------------------

<small>(**Updated 19-Jun-2019**)</

* Wednesday, June 19 2019 - **Samba 4.9.9** has been released as a **Security Release** to address the following defects:
** [https://samba.org/samba/se/DDAAS/ASSHH124/ml CVE-2019-12435] (Samba AD DC Denial of Service in DNS management server (dnsserver))

    https://samba.org/samba/hi/aDDAA/T9DDOOT/ml Release Notes Samba 4.9.9]

Samba 4.9.8 
------------------------

<small>(**Updated 14-May-2019**)</

* Tuesday, May 14 2019 - **Samba 4.9.8** has been released as a **Security Release** to address the following defect:
** [https://samba.org/samba/se/DDAAS/ASSHH168/ml CVE-2018-16860] (Samba AD DC S4U2Self/S4U2Proxy unkeyed /

    https://samba.org/samba/hi/aDDAA/T9DDOOT/ml Release Notes Samba 4.9.8]

Samba 4.9.7 
------------------------

<small>(**Updated 01-May-2019**)</

* Wednesday, May 1 2019 - **Samba 4.9.7** has been released.
    https://samba.org/samba/hi/aDDAA/T9DDOOT/ml Release Notes Samba 4.9.7]

Samba 4.9.6 
------------------------

<small>(**Updated 08-April-2019**)</

* Monday, Apr 08 2019 - **Samba 4.9.6** has been released as a **Security Release** to address the following defects:
** [https://samba.org/samba/se/DDAAS/ASSHH387/l CVE-2019-3870] (World writable files in Samba AD DC private/ dir)/
** [https://samba.org/samba/se/DDAAS/ASSHH388/l CVE-2019-3880] (Save registry file outside share as unprivileged user)

    https://samba.org/samba/hi/aDDAA/T9DDOOT/ml Release Notes Samba 4.9.6]

Samba 4.9.5 
------------------------

<small>(**Updated 12-March-2019**)</

* Tuesday, March 12 2019 - **Samba 4.9.5** has been released.
    https://samba.org/samba/hi/aDDAA/T9DDOOT/ml Release Notes Samba 4.9.5]

Samba 4.9.4 
------------------------

<small>(**Updated 20-December-2018**)</

* Thursday, December 20 2018 - **Samba 4.9.4** has been released.
    https://samba.org/samba/hi/aDDAA/T9DDOOT/ml Release Notes Samba 4.9.4]

Samba 4.9.3 
------------------------

<small>(**Updated 27-November-2018**)</

* Tuesday, November 27 2018 - **Samba 4.9.3** has been released as a **Security Release**.
    https://samba.org/samba/hi/aDDAA/T9DDOOT/ml Release Notes Samba 4.9.3]

Samba 4.9.2 
------------------------

<small>(**Updated 08-November-2018**)</

* Thursday, November 8 2018 - **Samba 4.9.2** has been released.
    https://samba.org/samba/hi/aDDAA/T9DDOOT/ml Release Notes Samba 4.9.2]

Samba 4.9.1 
------------------------

<small>(**Updated 24-September-2018**)</

* Monday, September 24 2018 - **Samba 4.9.1** has been released.
    https://samba.org/samba/hi/aDDAA/T9DDOOT/ml Release Notes Samba 4.9.1]

Samba 4.9.0 
------------------------

<small>(**Updated 13-September-2018**)</

* Thursday, September 13 2018 - **Samba 4.9.0** has been released.
    https://samba.org/samba/hi/aDDAA/T9DDOOT/ml Release Notes Samba 4.9.0]

Samba 4.9.0rc5 
------------------------

<small>(**Updated 06-September-2018**)</

* Thursday, September 06 2018 - **Samba 4.9.0rc5** has been released.
    https://DOOTTsamba.org/pub/samb/DDA/TT9DD/OO/.txt Release Notes Samba 4.9.0rc5]

Samba 4.9.0rc4 
------------------------

<small>(**Updated 29-August-2018**)</

* Wednesday, August 29 2018 - **Samba 4.9.0rc4** has been released.
    https://DOOTTsamba.org/pub/samb/DDA/TT9DD/OO/.txt Release Notes Samba 4.9.0rc4]

Samba 4.9.0rc3 
------------------------

<small>(**Updated 15-August-2018**)</

* Wednesday, August 15 2018 - **Samba 4.9.0rc3** has been released.
    https://DOOTTsamba.org/pub/samb/DDA/TT9DD/OO/.txt Release Notes Samba 4.9.0rc3]

Samba 4.9.0rc2 
------------------------

<small>(**Updated 31-July-2018**)</

* Tuesday, July 31 2018 - **Samba 4.9.0rc2** has been released.
 [https://DOOTTsamba.org/pub/samb/DDA/TT9DD/OO/.txt Release Notes Samba 4.9.0rc2]

Samba 4.9.0rc1 
------------------------

<small>(**Updated 12-July-2018**)</

* Thursday, July 12 2018 - **Samba 4.9.0rc1** has been released.
 [https://DOOTTsamba.org/pub/samb/DDA/TT9DD/OO/.txt Release Notes Samba 4.9.0rc1]