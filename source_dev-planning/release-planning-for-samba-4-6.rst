Release Planning for Samba 4.6
    <namespace>0</namespace>
<last_edited>2018-09-18T09:25:14Z</last_edited>
<last_editor>Kseeger</last_editor>

Samba 4.6 has been marked `Samba_Release_Planning#Discontinued_.28End_of_Life.29|**discontinued**`.

`Blocker bugs|Release blocking bugs`
------------------------

* [https://bugzilla.samba.org/buglist.cgi?bug_severity=regression&query_format=advanced&target_milestone=4.6 All 4.6 regression bugs]
* [https://bugzilla.samba.org/buglist.cgi?bug_severity=regression&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=VERIFIED&query_format=advanced&target_milestone=4.6 Unresolved 4.6 regression bugs]

Samba 4.6.16 
------------------------

<small>(**Updated 14-August-2018**)</small>

* Tuesday, August 14 2018 - **Samba 4.6.16** has been released as a **Security Release** to address the following defects:
** [https://www.samba.org/samba/security/CVE-2018-10858.html CVE-2018-10858] (Insufficient input validation on client directory listing in libsmbclient.)
** [https://www.samba.org/samba/security/CVE-2018-10919.html CVE-2018-10919] (Confidential attribute disclosure from the AD LDAP server.)

    https://www.samba.org/samba/history/samba-4.6.16.html Release Notes Samba 4.6.16]

Samba 4.6.15 
------------------------

<small>(**Updated 13-April-2018**)</small>

* Friday, April 13 2018 - **Samba 4.6.15** has been released. This will very likely be the last 4.6 maintenance release. There will be security releases only beyond this point.
    https://www.samba.org/samba/history/samba-4.6.15.html Release Notes Samba 4.6.15]

Samba 4.6.14 
------------------------

<small>(**Updated 13-March-2018**)</small>

* Tuesday, March 13 2018 - **Samba 4.6.14** has been released as a **security release** in order to address [https://www.samba.org/samba/security/CVE-2018-1050.html CVE-2018-1050] (Denial of Service Attack on external print server) and [https://www.samba.org/samba/security/CVE-2018-1057.html CVE-2018-1057] (Authenticated users can change other users' password).
    https://www.samba.org/samba/history/samba-4.6.14.html Release Notes Samba 4.6.14]

Samba 4.6.13 
------------------------

<small>(**Updated 14-February-2018**)</small>

* Wednesday, February 14 2018 - **Samba 4.6.13** has been released.
    https://www.samba.org/samba/history/samba-4.6.13.html Release Notes Samba 4.6.13]

Samba 4.6.12 
------------------------

<small>(**Updated 20-December-2017**)</small>

* Wednesday, December 20 2017 - **Samba 4.6.12** has been released.
    https://www.samba.org/samba/history/samba-4.6.12.html Release Notes Samba 4.6.12]

Samba 4.6.11 
------------------------

<small>(**Updated 21-November-2017**)</small>

* Tuesday, November 21 2017 - **Samba 4.6.11** has been released as a **security release** in order to address [https://www.samba.org/samba/security/CVE-2017-14746.html CVE-2017-14746] (Use-after-free vulnerability) and [https://www.samba.org/samba/security/CVE-2017-15275.html CVE-2017-15275] (Server heap memory information leak).
    https://www.samba.org/samba/history/samba-4.6.11.html Release Notes Samba 4.6.11]

Samba 4.6.10 
------------------------

<small>(**Updated 15-November-2017**)</small>

* Wednesday, November 15 2017 - **Samba 4.6.10** has been released to address a [https://bugzilla.samba.org/show_bug.cgi?id=13130 data corruption bug].
    https://www.samba.org/samba/history/samba-4.6.10.html Release Notes Samba 4.6.10]

Samba 4.6.9 
------------------------

<small>(**Updated 25-October-2017**)</small>

* Wednesday, October 25 2017 - **Samba 4.6.9** has been released.
    https://www.samba.org/samba/history/samba-4.6.9.html Release Notes Samba 4.6.9]

Samba 4.6.8 
------------------------

<small>(**Updated 20-August-2017**)</small>

* Wednesday, September 20 2017 - **Samba 4.6.8** has been released as a **Security Release** in order to address
** [https://www.samba.org/samba/security/CVE-2017-12150.html CVE-2017-12150] (SMB1/2/3 connections may not require signing where they should),
** [https://www.samba.org/samba/security/CVE-2017-12151.html CVE-2017-12151] (SMB3 connections don't keep encryption across DFS redirects) and
** [https://www.samba.org/samba/security/CVE-2017-12163.html CVE-2017-12163] (Server memory information leak over SMB1).
    https://www.samba.org/samba/history/samba-4.6.8.html Release Notes Samba 4.6.8]

Samba 4.6.7 
------------------------

<small>(**Updated 09-August-2017**)</small>

* Wednesday, August 9 2017 - **Samba 4.6.7** has been released.
    https://www.samba.org/samba/history/samba-4.6.7.html Release Notes Samba 4.6.7]

Samba 4.6.6 
------------------------

<small>(**Updated 13-July-2017**)</small>

* Wednesday, July 12 2017 - **Samba 4.6.6** has been released as a **Security Release** in order to address [https://www.samba.org/samba/security/CVE-2017-11103.html CVE-2017-11103] (Orpheus' Lyre mutual authentication validation bypass).
    https://www.samba.org/samba/history/samba-4.6.6.html Release Notes Samba 4.6.6]

Samba 4.6.5 
------------------------

<small>(**Updated 06-Jun-2017**)</small>

* Tuesday, June 6 2017 - **Samba 4.6.5** has been released.
    https://www.samba.org/samba/history/samba-4.6.5.html Release Notes Samba 4.6.5]

Samba 4.6.4 
------------------------

<small>(**Updated 24-May-2017**)</small>

* Wednesday, May 24 2017 - **Samba 4.6.4** has been released as a **Security Release** in order to address [https://www.samba.org/samba/security/CVE-2017-7494.html CVE-2017-7494] (Remote code execution from a writable share.).
    https://www.samba.org/samba/history/samba-4.6.4.html Release Notes Samba 4.6.4]

Samba 4.6.3 
------------------------

<small>(**Updated 25-April-2017**)</small>

* Tuesday, April 25 2017 - **Samba 4.6.3** has been released.
    https://www.samba.org/samba/history/samba-4.6.3.html Release Notes Samba 4.6.3]

Samba 4.6.2 
------------------------

<small>(**Updated 31-March-2017**)</small>

* Friday, March 31 2017 - **Samba 4.6.2** has been released.
    https://www.samba.org/samba/history/samba-4.6.2.html Release Notes Samba 4.6.2]

Samba 4.6.1 
------------------------

<small>(**Updated 23-March-2017**)</small>

* Thursday, March 23 2017 - **Samba 4.6.1** has been released as a **Security Release** in order to address [https://www.samba.org/samba/security/CVE-2017-2619.html CVE-2017-2619] (Symlink race allows access outside share definition).
    https://www.samba.org/samba/history/samba-4.6.1.html Release Notes Samba 4.6.1]

Samba 4.6.0 
------------------------

<small>(**Updated 07-March-2017**)</small>

* Tuesday, March 7 2017 - **Samba 4.6.0** has been released
    https://www.samba.org/samba/history/samba-4.6.0.html Release Notes Samba 4.6.0]

Samba 4.6.0rc4 
------------------------

<small>(**Updated 01-December-2016**)</small>

* Tuesday, February 28 2017 - **Samba 4.6.0rc4** has been released
    https://download.samba.org/pub/samba/rc/samba-4.6.0rc4.WHATSNEW.txt Release Notes Samba 4.6.0rc4]

Samba 4.6.0rc3 
------------------------

<small>(**Updated 14-November-2016**)</small>

* Tuesday, February 14 2017 - **Samba 4.6.0rc3** has been released
    https://download.samba.org/pub/samba/rc/samba-4.6.0rc3.WHATSNEW.txt Release Notes Samba 4.6.0rc3]

Samba 4.6.0rc2 
------------------------

<small>(**Updated 14-November-2016**)</small>

* Tuesday, January 24 2017 - **Samba 4.6.0rc2** has been released
    https://download.samba.org/pub/samba/rc/samba-4.6.0rc2.WHATSNEW.txt Release Notes Samba 4.6.0rc2]

Samba 4.6.0rc1 
------------------------

<small>(**Updated 05-January-2017**)</small>

* Thursday, January 5 2017 - **Samba 4.6.0rc1** has been released
    https://download.samba.org/pub/samba/rc/samba-4.6.0rc1.WHATSNEW.txt Release Notes Samba 4.6.0rc1]