Release Planning for Samba 3.5
    <namespace>0</namespace>
<last_edited>2013-10-11T08:14:17Z</last_edited>
<last_editor>Kseeger</last_editor>

With the release of Samba 4.1.0, Samba 3.5 has been marked `Samba_Release_Planning#Discontinued|**discontinued**`.

Samba 3.5.22 
------------------------

(**Updated 05-August-2013**)

* Monday, August 05 - Samba 3.5.22 has been released as a **Security Release**
    http://www.samba.org/samba/history/samba-3.5.22.html Release Notes Samba 3.5.22]

Samba 3.5.21 
------------------------

(**Updated 30-January-2013**)

* Wednesday, January 30 - Samba 3.5.21 has been issued as a **Security Release** in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-0213 CVE-2013-0213] (Clickjacking issue in SWAT) and [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-0214 CVE-2013-0214] (Potential XSRF in SWAT).
    http://www.samba.org/samba/history/samba-3.5.21.html Release Notes Samba 3.5.21]

Samba 3.5.20 
------------------------

(**Updated 17-December-2012**)

* Monday, December 17 - Samba 3.5.20 has been released - **Please note that this will probably be the last bugfix release of the 3.5 series**
    http://www.samba.org/samba/history/samba-3.5.20.html Release Notes Samba 3.5.20]

Samba 3.5.19 
------------------------

(**Updated 05-November-2012**)

* Monday, November 5 - Samba 3.5.19 has been released
    http://www.samba.org/samba/history/samba-3.5.19.html Release Notes Samba 3.5.19]

Samba 3.5.18 
------------------------

(**Updated 24-September-2012**)

* Monday, September 24 - Samba 3.5.18 has been released
    http://www.samba.org/samba/history/samba-3.5.18.html Release Notes Samba 3.5.18]

Samba 3.5.17 
------------------------

(**Updated 13-August-2012**)

* Monday, August 13 - Samba 3.5.17 has been released
    http://www.samba.org/samba/history/samba-3.5.17.html Release Notes Samba 3.5.17]

Samba 3.5.16 
------------------------

(**Updated 02-July-2012**)

* Monday, July 2 - Samba 3.5.16 has been released
    http://www.samba.org/samba/history/samba-3.5.16.html Release Notes Samba 3.5.16]

Samba 3.5.15 
------------------------

(**Updated 30-April-2012**)

* Monday, April 30 - Samba 3.5.15 **Security Release** has been released in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-2111 CVE-2012-2111 (Incorrect permission checks when granting/removing privileges can compromise file server security)].
    http://www.samba.org/samba/history/samba-3.5.15.html Release Notes Samba 3.5.15]

Samba 3.5.14 
------------------------

(**Updated 10-April-2012**)

* Tuesday, April 10 - Samba 3.5.14 **Security Release** has been released in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-1182 CVE-2012-1182 ("root" credential remote code execution)].
    http://www.samba.org/samba/history/samba-3.5.14.html Release Notes Samba 3.5.14]

Samba 3.5.13 
------------------------

(**Updated 12-March-2012**)

* Monday, March 12 - Samba 3.5.13 has been released
    http://www.samba.org/samba/history/samba-3.5.13.html Release Notes Samba 3.5.13]

Samba 3.5.12 
------------------------

(**Updated 2-November-2011**)

* Wednesday, November 2 - Samba 3.5.12 has been released
    http://www.samba.org/samba/history/samba-3.5.12.html Release Notes Samba 3.5.12]

Samba 3.5.11 
------------------------

(**Updated 04-August-2011**)

* Thursday, August 4 - Samba 3.5.11 has been released
    http://www.samba.org/samba/history/samba-3.5.11.html Release Notes Samba 3.5.11]

Samba 3.5.10 
------------------------

(**Updated 26-July-2011**)

* Tuesday, July 26 - Samba 3.5.10 has been released to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-2522 CVE-2011-2522] and [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-2694 CVE-2011-2694].
    http://www.samba.org/samba/history/samba-3.5.10.html Release Notes Samba 3.5.10]

Samba 3.5.9 
------------------------

(**Updated 14-June-2011**)

* Tuesday, June 14 - Samba 3.5.9 has been released
    http://www.samba.org/samba/history/samba-3.5.9.html Release Notes Samba 3.5.9]

Samba 3.5.8 
------------------------

(**Updated 07-March-2011**)

* Monday, March 7 - Samba 3.5.8 has been released
    http://www.samba.org/samba/history/samba-3.5.8.html Release Notes Samba 3.5.8]

Samba 3.5.7 
------------------------

(**Updated 28-February-2011**)

* Monday, February 28 - Samba 3.5.7 has been released to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-0719 CVE-2011-0719].
    http://www.samba.org/samba/history/samba-3.5.7.html Release Notes Samba 3.5.7]

Samba 3.5.6 
------------------------

(**Updated 08-October-2010**)

* Friday, October 8 - Samba 3.5.6 has been released
    http://www.samba.org/samba/history/samba-3.5.6.html Release Notes Samba 3.5.6]

Samba 3.5.5 
------------------------

(**Updated 14-September-2010**)

* Tuesday, September 14 - Samba 3.5.5 has been released to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-3069 CVE-2010-3069].
    http://www.samba.org/samba/history/samba-3.5.5.html Release Notes Samba 3.5.5]

Samba 3.5.4 
------------------------

(**Updated 23-June-2010**)

* Wednesday, June 23 - Samba 3.5.4 has been released
    http://www.samba.org/samba/history/samba-3.5.4.html Release Notes Samba 3.5.4]

Samba 3.5.3 
------------------------

(**Updated 19-May-2010**)

* Wednesday, May 19 - Samba 3.5.3 has been released
    http://www.samba.org/samba/history/samba-3.5.3.html Release Notes Samba 3.5.3]

Samba 3.5.2 
------------------------

(**Updated 07-April-2010**)

* Wednesday, April 7 - Samba 3.5.2 has been released
    http://www.samba.org/samba/history/samba-3.5.2.html Release Notes Samba 3.5.2]

Samba 3.5.1 
------------------------

(**Updated 09-March-2010**)

* Monday, March 8 - Samba 3.5.1 has been released to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-0728 CVE-2010-0728].
    http://www.samba.org/samba/history/samba-3.5.1.html Release Notes Samba 3.5.1]

Samba 3.5.0 
------------------------

(**Updated 01-March-2010**)

* Monday, March 1 - Samba 3.5.0 has been released
    http://www.samba.org/samba/history/samba-3.5.0.html Release Notes Samba 3.5.0]