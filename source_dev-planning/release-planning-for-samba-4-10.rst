Release Planning for Samba 4.10
    <namespace>0</namespace>
<last_edited>2020-10-06T07:35:02Z</last_edited>
<last_editor>Kseeger</last_editor>

Samba 4.10 has been marked `Samba_Release_Planning#Discontinued_.28End_of_Life.29|**discontinued**`.

`Blocker bugs|Release blocking bugs`
------------------------

* [https://bugzilla.samba.org/buglist.cgi?bug_severity=regression&query_format=advanced&target_milestone=4.10 All 4.10 regression bugs]
* [https://bugzilla.samba.org/buglist.cgi?bug_severity=regression&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=VERIFIED&query_format=advanced&target_milestone=4.10 Unresolved 4.10 regression bugs]

Samba 4.10.18 
------------------------

<small>(**Updated 18-Sep-2020**)</small>

* Friday, September 18 2020 - **Samba 4.10.18** has been released as a **Security Release** to address the following defect:
** [https://www.samba.org/samba/security/CVE-2020-1472.html CVE-2020-1472] (Unauthenticated domain takeover via netlogon ("ZeroLogon")).
    https://www.samba.org/samba/history/samba-4.10.18.html Release Notes Samba 4.10.18]

Samba 4.10.17 
------------------------

<small>(**Updated 02-Jul-2020**)</small>

* Thursday, July 2 2020 - **Samba 4.10.17** has been released as a **Security Release** to address the following defects:
** [https://www.samba.org/samba/security/CVE-2020-10730.html CVE-2020-10730] (NULL pointer de-reference and use-after-free in Samba AD DC LDAP Server with ASQ, VLV and paged_results).
** [https://www.samba.org/samba/security/CVE-2020-10745.html CVE-2020-10745] (Parsing and packing of NBT and DNS packets can consume excessive CPU).
** [https://www.samba.org/samba/security/CVE-2020-10760.html CVE-2020-10760] (LDAP Use-after-free in Samba AD DC Global Catalog with paged_results and VLV).
** [https://www.samba.org/samba/security/CVE-2020-14303.html CVE-2020-14303] (Empty UDP packet DoS in Samba AD DC nbtd).

    https://www.samba.org/samba/history/samba-4.10.17.html Release Notes Samba 4.10.17]

Samba 4.10.16 
------------------------

<small>(**Updated 25-May-2020**)</small>

* Monday, May 25 2020 - **Samba 4.10.16** has been released as an additional bugfix release to address the following issues:

    https://www.samba.org/samba/history/samba-4.10.16.html Release Notes Samba 4.10.16]

Samba 4.10.15 
------------------------

<small>(**Updated 28-April-2020**)</small>

* Tuesday, April 28 2020 - **Samba 4.10.15** has been released as a **Security Release** to address the following defects:
** [https://www.samba.org/samba/security/CVE-2020-10700.html CVE-2020-10700] (Use-after-free in Samba AD DC LDAP Server with ASQ.)
** [https://www.samba.org/samba/security/CVE-2020-10704.html CVE-2020-10704] (LDAP Denial of Service (stack overflow) in Samba AD DC.)

    https://www.samba.org/samba/history/samba-4.10.15.html Release Notes Samba 4.10.15]

Samba 4.10.14 
------------------------

<small>(**Updated 26-March-2020**)</small>

* Thursday, March 26 2020 - **Samba 4.10.14** has been released as the last 4.10 bugfix release. There will be Security releases only beyond this point.

    https://www.samba.org/samba/history/samba-4.10.14.html Release Notes Samba 4.10.14].

Samba 4.10.13 
------------------------

<small>(**Updated 23-Jan-2020**)</small>

* Thursday, January 23 2020 - **Samba 4.10.13** has been released.

    https://www.samba.org/samba/history/samba-4.10.13.html Release Notes Samba 4.10.13].

Samba 4.10.12 
------------------------

<small>(**Updated 21-Jan-20120**)</small>

* Tuesday, January 21 2020 - **Samba 4.10.12** has been released as a **Security Release** to address the following defects:
** [https://www.samba.org/samba/security/CVE-2019-14902.html CVE-2019-14902] (Replication of ACLs set to inherit down a subtree on AD Directory not automatic.)
** [https://www.samba.org/samba/security/CVE-2019-14907.html CVE-2019-14907] (Crash after failed character conversion at log level 3 or above.)
** [https://www.samba.org/samba/security/CVE-2019-19344.html CVE-2019-19344] (Use after free during DNS zone scavenging in Samba AD DC.)

    https://www.samba.org/samba/history/samba-4.10.12.html Release Notes Samba 4.10.12]

Samba 4.10.11 
------------------------

<small>(**Updated 10-Dec-2019**)</small>

* Tuesday, December 10 2019 - **Samba 4.10.11** has been released as a **Security Release** to address the following defects:
** [https://www.samba.org/samba/security/CVE-2019-14861.html CVE-2019-14861] (Samba AD DC zone-named record Denial of Service in DNS management server (dnsserver))
** [https://www.samba.org/samba/security/CVE-2019-14870.html CVE-2019-14870] (DelegationNotAllowed not being enforced in protocol transition on Samba AD DC.)

    https://www.samba.org/samba/history/samba-4.10.11.html Release Notes Samba 4.10.11]

Samba 4.10.10 
------------------------

<small>(**Updated 29-Oct-2019**)</small>

* Tuesday, October 29 2019 - **Samba 4.10.10** has been released as a **Security Release** to address the following defects:
** [https://www.samba.org/samba/security/CVE-2019-10218.html CVE-2019-10218] (Client code can return filenames containing              path separators.)
** [https://www.samba.org/samba/security/CVE-2019-14833.html CVE-2019-14833] (Samba AD DC check password script does not receive the full password.)
** [https://www.samba.org/samba/security/CVE-2019-14847.html CVE-2019-14847] (User with "get changes" permission can     crash AD DC LDAP server via dirsync)

    https://www.samba.org/samba/history/samba-4.10.10.html Release Notes Samba 4.10.10]

Samba 4.10.9 
------------------------

<small>(**Updated 17-Oct-2019**)</small>

* Thursday, October 17 2019 - **Samba 4.10.9** has been released.
    https://www.samba.org/samba/history/samba-4.10.9.html Release Notes Samba 4.10.9]

Samba 4.10.8 
------------------------

<small>(**Updated 03-Sep-2019**)</small>

* Tuesday, September 03 2019 - **Samba 4.10.8** has been released as a **Security Release** in order to address:
** [https://www.samba.org/samba/security/CVE-2019-10197.html CVE-2019-10197] (Combination of parameters and permissions can allow user to escape from the share path definition.)

    https://www.samba.org/samba/history/samba-4.10.8.html Release Notes Samba 4.10.8]

Samba 4.10.7 
------------------------

<small>(**Updated 22-Aug-2019**)</small>

* Thursday, August 22 2019 - Planned release date for **Samba 4.10.7**.
    https://www.samba.org/samba/history/samba-4.10.7.html Release Notes Samba 4.10.7]

Samba 4.10.6 
------------------------

<small>(**Updated 08-Jul-2019**)</small>

* Monday, July 8 2019 - **Samba 4.10.6** has been released.

    https://www.samba.org/samba/history/samba-4.10.6.html Release Notes Samba 4.10.6]

Samba 4.10.5 
------------------------

<small>(**Updated 19-Jun-2019**)</small>

* Wednesday, June 19 2019 - **Samba 4.10.5** has been released as a **Security Release** to address the following defects:
** [https://www.samba.org/samba/security/CVE-2019-12435.html CVE-2019-12435] (Samba AD DC Denial of Service in DNS management server (dnsserver))
** [https://www.samba.org/samba/security/CVE-2019-12436.html CVE-2019-12436] (Samba AD DC LDAP server crash (paged searches))

    https://www.samba.org/samba/history/samba-4.10.5.html Release Notes Samba 4.10.5]

Samba 4.10.4 
------------------------

<small>(**Updated 22-May-2019**)</small>

* Wednesday, May 22 2019 - **Samba 4.10.4** has been released.
    https://www.samba.org/samba/history/samba-4.10.4.html Release Notes Samba 4.10.4]

Samba 4.10.3 
------------------------

<small>(**Updated 14-May-2019**)</small>

* Tuesday, May 14 2019 - **Samba 4.10.3** has been released as a **Security Release** to address the following defect:
** [https://www.samba.org/samba/security/CVE-2018-16860.html CVE-2018-16860] (Samba AD DC S4U2Self/S4U2Proxy unkeyed checksum)

    https://www.samba.org/samba/history/samba-4.10.3.html Release Notes Samba 4.10.3]

Samba 4.10.2 
------------------------

<small>(**Updated 08-April-2019**)</small>

* Monday, Apr 08 2019 - **Samba 4.10.2** has been released as a **Security Release** to address the following defects:
** [https://www.samba.org/samba/security/CVE-2019-3870.html CVE-2019-3870] (World writable files in Samba AD DC private/ dir)
** [https://www.samba.org/samba/security/CVE-2019-3880.html CVE-2019-3880] (Save registry file outside share as unprivileged user)

    https://www.samba.org/samba/history/samba-4.10.2.html Release Notes Samba 4.10.2]

Samba 4.10.1 
------------------------

<small>(**Updated 03-April-2019**)</small>

* Wednesday, April 03 2019 - **Samba 4.10.1** has been released.
    https://www.samba.org/samba/history/samba-4.10.1.html Release Notes Samba 4.10.1]

Samba 4.10.0 
------------------------

<small>(**Updated 19-March-2019**)</small>

* Tuesday, March 19 2019 - **Samba 4.10.0** has been released.
    https://www.samba.org/samba/history/samba-4.10.0.html Release Notes Samba 4.10.0]

Samba 4.10.0rc1 
------------------------

<small>(**Updated 15-January-2019**)</small>

* Tuesday, January 15 2019 - **Samba 4.10.0rc1** has been released.
    https://download.samba.org/pub/samba/rc/samba-4.10.0rc1.WHATSNEW.txt Release Notes Samba 4.10.0rc1]

Samba 4.10.0rc2 
------------------------

<small>(**Updated 06-February-2019**)</small>

* Wednesday, February 6 2019 - **Samba 4.10.0rc2** has been released.
    https://download.samba.org/pub/samba/rc/samba-4.10.0rc2.WHATSNEW.txt Release Notes Samba 4.10.0rc2]

Samba 4.10.0rc3 
------------------------

<small>(**Updated 22-Ferbuary-2018**)</small>

* Friday, February 22 2019 - **Samba 4.10.0rc3** has been released.
    https://download.samba.org/pub/samba/rc/samba-4.10.0rc3.WHATSNEW.txt Release Notes Samba 4.10.0rc3]

Samba 4.10.0rc4 
------------------------

<small>(**Updated 06-March-2019**)</small>

* Wednesday, March 6 2019 - **Samba 4.10.0rc4** has been released.
    https://download.samba.org/pub/samba/rc/samba-4.10.0rc4.WHATSNEW.txt Release Notes Samba 4.10.0rc4]