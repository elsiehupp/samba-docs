Samba4/Tests
    <namespace>0</namespace>
<last_edited>2010-05-03T17:16:43Z</last_edited>
<last_editor>Anatoliy</last_editor>

Test coverage information (regularly updated) can be found here:

* http://build.samba.org/lcov/data/magni/samba_4_0_test/

Areas which need improvements wrt tests:

* wins
* tls
* upgrade
* dsdb
* client
* ctdb
* lib/compression
* lib/socket/access.c
* lib/socket_wrapper
* libcli/dgram
* nbt_server/dgram
* rpc_server/drsuapi
* rpc_server/epmapper
* rpc_server/remote
* rpc_server/wkssvc
* web_server/
* winbind

Blackbox tests:
* nmblookup

I'd also like to parse what tests have failed and what have succeeded in the build farm, so we can show a summary of what tests fail on the main build farm page.

Test environments 
------------------------

Currently supported test environments:

* dc - A standalone Samba 4 DC
* none - Nothing set up (used for local tests)
* member - Samba4 DC with domain member joined to it

Run the testsuite with GDB 
------------------------

* make test GDBTEST=1
* ../buildtools/bin/waf test --quicktest --gdbtest

For other options see selftest/wscript file and assign values to dest keywords