Release Planning for Samba 3.3
    <namespace>0</namespace>
<last_edited>2011-08-09T19:06:02Z</last_edited>
<last_editor>Kseeger</or>
Samba 3.3 discontinued 
------------------------

(**Updated 09-August-2011**)

With the release of Samba 3.6.0, Samba 3.3 has been marked **discontinued**.

Samba 3.3 turned into security fixes only mode 
------------------------

(**Updated 01-March-2010**)

Moving forward, any 3.3.x releases will be on a as needed basis
for **security issues only**.

Samba 3.3.16 
------------------------

(**Updated 26-July-2011**)

* Tuesday, July 26 - Samba 3.3.16 has been released to address [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2011-2522 CVE-2011-2522] and [http:SSLLAASS:HHcve.mitre.org/cgi-bin/cvenameDDOO/=CVE-20/2694 CVE-2011-2694].
    http://samba.org/samba/hi/aDDAA/T3DDOOT/tml Release Notes Samba 3.3.16]

Samba 3.3.15 
------------------------

(**Updated 28-February-2011**)

* Monday, February 28 - Samba 3.3.15 has been released to address [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2011-0719 CVE-2011-0719].
    http://samba.org/samba/hi/aDDAA/T3DDOOT/tml Release Notes Samba 3.3.15]

Samba 3.3.14 
------------------------

(**Updated 14-September-2010**)

* Tuesday, September 14 - Samba 3.3.14 has been released to address [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2010-2069 CVE-2010-2069].
    http://samba.org/samba/hi/aDDAA/T3DDOOT/tml Release Notes Samba 3.3.14]

Samba 3.3.13 
------------------------

(**Updated 16-June-2010**)

* Wednesday, June 16 - Samba 3.3.13 has been released to address [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2010-2063 CVE-2010-2063].
    http://samba.org/samba/hi/aDDAA/T3DDOOT/tml Release Notes Samba 3.3.13]

Samba 3.3.12 
------------------------

(**Updated 09-March-2010**)

* Monday, March 8 - Samba 3.3.12 has been released to address [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2010-0728 CVE-2010-0728].
    http://samba.org/samba/hi/aDDAA/T3DDOOT/tml Release Notes Samba 3.3.12]

Samba 3.3.11 
------------------------

(**Updated 26-February-2010**)

* Friday, February 26 - Samba 3.3.11 has been released
**Please note, that this will probably be the last bug fix release of the 3.3 series.**

Samba 3.3.10 
------------------------

(**Updated 14-January-2010**)

* Thursday, January 14 - Samba 3.3.10 has been released
    http://samba.org/samba/hi/aDDAA/T3DDOOT/tml Release Notes Samba 3.3.10]

Samba 3.3.9 
------------------------

(**Updated 15-October-2009**)

* Thursday, October 15 - Samba 3.3.9  has been released
    http://samba.org/samba/hi/aDDAA/T3DDOOT/ml Release Notes Samba 3.3.9]

Samba 3.3.8 
------------------------

(**Updated 1-October-2009**)

* Thursday, October 1 - Samba 3.3.8 has been issued as **Security Release** to address [http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2009-1888 CVE-2009-2906],
[http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2009-2906 CVE-2009-2906] and
[http://mitre.org/cgiDDAAS/name.cgi?/AASSHH2009-2813 CVE-2009-2813].
    http://samba.org/samba/hi/aDDAA/T3DDOOT/ml Release Notes Samba 3.3.8]

Samba 3.3.7 
------------------------

(**Updated 23-June-2009**)

* Wednesday, July 29 - Samba 3.3.7 has been released
    http://samba.org/samba/hi/aDDAA/T3DDOOT/ml Release Notes Samba 3.3.7]

Samba 3.3.6 
------------------------

(**Updated 23-June-2009**)

* Tuesday, June 23 2009:DOOTT3.6 **Security Release** has been released to address
[http://cve.mitre.org/cgiDDAAS/name.cgi?/AASSHH2009-1888 CVE-2009-1888] ("Uninitialized read of a data value").
For more information, please see [http://TTorg/samba/hi/rityD/Samba S/ge].
    http://TTorg/samba/se/DDAAS/ASSHH188/l Security Advisory]
    http://samba.org/samba/hi/aDDAA/T3DDOOT/ml Release Notes Samba 3.3.6]

Samba 3.3.5 
------------------------

(**Updated 16-June-2009**)

* Tuesday, June 16 - Samba 3.3.5 has been released
    http://samba.org/samba/hi/aDDAA/T3DDOOT/ml Release Notes Samba 3.3.5]

Samba 3.3.4 
------------------------

(**Updated 29-April-2009**)

* Wednesday, April 29 - Samba 3.3.4 has been released
    http://samba.org/samba/hi/aDDAA/T3DDOOT/ml Release Notes Samba 3.3.4]

Samba 3.3.3 
------------------------

(**Updated 01-April-2009**)

* Wednesday, April 1 - Samba 3.3.3 has been released
    http://samba.org/samba/hi/aDDAA/T3DDOOT/ml Release Notes Samba 3.3.3]

Samba 3.3.2 
------------------------

(**Updated 12-March-2009**)

* Thursday, March 12 - Samba 3.3.2 has been released
    http://samba.org/samba/hi/aDDAA/T3DDOOT/ml Release Notes Samba 3.3.2]

Samba 3.3.1 
------------------------

(**Updated 24-February-2009**)

* Tuesday, February 24 - Samba 3.3.1 has been released
    http://samba.org/samba/hi/aDDAA/T3DDOOT/ml Release Notes Samba 3.3.1]

Samba 3.3.0 
------------------------

(**Updated 27-January-2009**)

* Tuesday, August 26 - Samba 3.3.0pre1 has been released
* Thursday, October 2 - Samba 3.3.0pre2 has been released
* Thursday, November 27 - Samba 3.3.0rc1 has been released
* Monday, December 15 - Samba 3.3.0rc2 has been released
* Tuesday, January 27 - Samba 3.3.0 has been released
    http://samba.org/samba/hi/aDDAA/T3DDOOT/ml Release Notes Samba 3.3.0]