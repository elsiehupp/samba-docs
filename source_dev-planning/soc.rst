SoC
    <namespace>0</namespace>
<last_edited>2020-03-09T23:00:40Z</last_edited>
<last_editor>Abartlet</last_editor>

==============================

oogle Summer of Code
===============================

For a list of ideas for projects see `SoC/Ideas`.

[https://summerofcode.withgoogle.com Details on the Google Summer of Code program]

2020 Samba applications 
------------------------

See the [https://summerofcode.withgoogle.com/organizations/5926743954685952/ Samba Organisation page] for details on applying to Samba in particular.

We will be looking for applications that comply with [https://google.github.io/gsocguides/student/writing-a-proposal#elements-of-a-quality-proposal Google's Guide on writing a Quality Proposal] and we suggest students read [https://medium.com/@i.oleks/how-to-apply-for-google-summer-of-code-95c1bfcd41a5 this guide by a former SoC student (at another project)].

**In particular, do not just copy our `SoC/Ideas|Ideas page` text into your application without also showing you understand what you are suggesting to do**

=================
Projects 2017
=================

List of 2017 students and projects: `SoC/2017`

=================
Projects 2014
=================

List of 2014 students and projects: `SoC/2014`

=================
Projects 2013
=================

*`GSOC_GPO|GPO for Samba`
*`GSOC_smbclient_archive|smbclient archive improvements`

=================
Projects 2012
=================

List of 2012 students and projects: `SoC/2012`

=================
Projects 2011
=================

List of 2011 students and projects: `SoC/2011`

=================
Previous Results
=================

Kai wrote a `GSoC_2007_Winbind_Summary | summary on his 2007 project`.

Jelmer wrote a [http://www.samba.org/~jelmer/soc.html summary on his 2005 project].