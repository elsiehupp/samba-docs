Roadmap
    <namespace>0</namespace>
<last_edited>2021-05-14T14:09:10Z</last_edited>
<last_editor>Darkpixel</last_editor>

===============================

Introduction
===============================

This page describes the bigger next steps in the development of Samba. The purpose is to point out the broader direction into which Samba is heading.

If a feature listed below is flagged as **FUNDED**, this means that someone is currently being paid to work on it. Hence there are realistic chances that this feature might be completed in a reasonably short time frame. For all other features, further involvement is needed: Otherwise it could even take years to complete even if a feature is flagged as work in progress (WIP), since these are usually being worked on in someone's spare time.

Involvement is highly welcome and can come in various guises: **Manpower for coding, testing, documentation, ...**

**Contact the Samba Team go get involved!**

=================

Features
=================

File Server (smbd) 
------------------------

`Samba3/SMB2|SMB2/SMB3`
------------------------

* **FUNDED**: Implement multi-channel (`User:Obnox|Michael`, `User:Metze|Metze`)
* **FUNDED**: Implement the witness service (Günther, `User:Metze|Metze`)
** Prerequisite: An asynchronous RPC server. See `DCERPC`
* **FUNDED for Gluster**: Clustering (continuous availability, scale-out) - Planning (`User:Ira|Ira`, `User:Obnox|Michael`, ...)
* **FUNDED for Gluster**: Persistent file handles - Planning (`User:Ira|Ira`, `User:Obnox|Michael`, ...) (Do not expect this immediately - Ira)
* RDMA (SMB direct) - Planning (`User:Metze|Metze`, `User:Rsharpe|Richard`, `User:Obnox|Michael`, `User:Ira|Ira`)
* Directory leases

Clustering - CTDB
------------------------

* integrate the clustered file server into selftest/autobuild - WIP (`User:Obnox|Michael`)

File Systems
------------------------

Support for special features of various file systems, especially cluster file systems, typically through VFS modules.

* **FUNDED**: gpfs
* **FUNDED**: GlusterFS
* CephFS
* ...

Performance
------------------------

Performance tuning and optimization is an important reoccurring topic. It is difficult
to really track the current issues...

* performance in clusters, TDB/CTDB
* Parallel, small I/O (HyperV) workload

Print Server (smbd|spoolssd) 
------------------------

* 'MS-PAR'
* `Spoolss|SPOOLSS`
** Improve Spoolss Server performance
** Improve Spoolss Testsuite
* `Winreg|Improve Winreg performance`

Active Directory Server 
------------------------

* **HELP NEEDED:** S4U2Self, S4U2Proxy, PKINIT ... You can find more details at: `Roadmap_MIT_KDC`
* **HELP NEEDED:** Two-way forest trusts (Metze)
* **FUNDED:** Correct non-mesh inter-site and intra-site replication via Knowledge Consistency Checker (KCC) (Andrew Bartlett, Garming Sam, Douglas
Bagnall)

* `The_Samba_AD_DNS_Back_Ends`
* `Samba4/DRS_TODO_List|Directory Replication Service (DRS)`
* sysvol Replication (FRS/DFSR)
* Read-only Domain Controller (RODC)
* Subdomain support
* One way trusts
* Support all LSA and Netlogon server functions

`DCERPC` infrastructure 
------------------------

The RPC server infrastructure component is of crucial importance for both the file server and the active directory server. A few tasks in for the RPC server are prerequisites for higher level features in the file server and the active directory server. See `DCERPC` for details.

* Merge source3 and source4 server and client implementations (Metze)
* Make RPC server (and client) implementation fully asynchronuous (Metze)
* Merge `Endpoint_Mapper|endpoint mapper` implementations
* Implement Association groups

* async schannel (NETLOGON) client (Metze)
* merged crypto handling for samlogon cred validation (Günther/Metze)
* merged libnetjoin interfaces (Günther/Metze)

Testing 
------------------------

* Multi-trust environments setup to test trusts
* Rewrite and improve the Selftest Suite

=================

Completed tasks
=================

File Server
------------------------

* SMB 2.0 durable file handles
* SMB 2.1 Leases
* SMB 2.1 Multi-Credit
* SMB 3.0 protocol support (including encryption)

* Transparent file compression
* Serverside copy using COPYCHUNK

* Improved performance on small-CPUs
* Improved TDB database performance (using robust mutex locking)

Clustering - CTDB
------------------------

* integrate CTDB master into samba master:
** integrate the code under ctdb/
** integrate the build into the top level waf build

Active Directory Server
------------------------

* internal dns server
* use smbd as file server
* use winbindd for id-mapping

DCERPC Infrastructure
------------------------

* common secure channel implementation

Testing
------------------------

* Implement preloadable wrappers for better testing - [https://cwrap.org The cwrap project]