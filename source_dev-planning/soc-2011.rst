SoC/
    <namespace>0</namespace>
<last_edited>2012-12-13T22:12:07Z</last_edited>
<last_editor>Obnox</last_editor>

=================
SoC Projects 2011
=================

RPC support for samba configuration 
------------------------

Samba has a registry based configuration backend:ation data is stored inside the registry key HKEY_LOCAL_MACHINE\Software\Samba\smbconf. A tool for reading and writing this registry stored configuration is "net conf". Access to the registry-stored configuration is performed through a module that makes use of the "reg_api" module which is the backend code for direct local access to the registry database. The reg_api interface is similar to the WINREG rpc interface. The basic goal of this project is to develop a remote variant of the "net conf" tool that allows for configuring a remote samba server with the same convenience as offered by the "net conf" tool. 

* Language(s):
* Mentors:Michael Adam` and `User:GlaDiaC|:hneider`
* Student: Ciorbaru
* Student's project page:LAASSHH2011/