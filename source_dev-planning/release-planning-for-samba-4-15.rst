Release Planning for Samba 4.15
    <namespace>0</namespace>
<last_edited>2021-05-05T15:41:37Z</last_edited>
<last_editor>Kseeger</last_editor>

Samba 4.15 is the `Samba_Release_Planning#Upcoming_Release|**new upcoming release branch**`.

`Blocker bugs|Release blocking bugs`
------------------------

* [https://bugzilla.samba.org/buglist.cgi?bug_severity=regression&query_format=advanced&target_milestone=4.15 All 4.15 regression bugs]
* [https://bugzilla.samba.org/buglist.cgi?bug_severity=regression&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=VERIFIED&query_format=advanced&target_milestone=4.15 Unresolved 4.15 regression bugs]

Samba 4.15.0rc1 
------------------------

<small>(**Updated 05-May-2021**)</small>

* Thursday, July 8 2021 - Planned release date for **Samba 4.15.0rc1**.