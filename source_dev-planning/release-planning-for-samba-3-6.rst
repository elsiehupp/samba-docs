Release Planning for Samba 3.6
    <namespace>0</namespace>
<last_edited>2015-09-08T14:38:24Z</last_edited>
<last_editor>Kseeger</last_editor>

With the release of Samba 4.2.0, Samba 3.6 has been marked `Samba_Release_Planning#Discontinued_.28End_of_Life.29|**discontinued**`.

Samba 3.6.25 
------------------------

(**Updated 23-February-2015**)

* Monday, February 23 - Samba 3.6.25 has been released as a **Security Release** in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-0240 CVE-2015-0240] (Unexpected code execution in smbd) and [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0178 CVE-2014-0178] (Malformed FSCTL_SRV_ENUMERATE_SNAPSHOTS response).
    http://www.samba.org/samba/history/samba-3.6.25.html Release Notes Samba 3.6.25]

Samba 3.6.24 
------------------------

(**Updated 23-June-2014**)

* Monday, June 23 - Samba 3.6.24 has been released as a **Security Release** in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0244 CVE-2014-0244] (Denial of service - CPU loop) and [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-3493 CVE-2014-3493] (Denial of service - Server crash/memory corruption).
    http://www.samba.org/samba/history/samba-3.6.24.html Release Notes Samba 3.6.24]

Samba 3.6.23 
------------------------

(**Updated 11-March-2014**)

* Tuesday, March 11 - Samba 3.6.23 has been released as a **Security Release** in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4496 CVE-2013-4496] (Password lockout not enforced for SAMR password changes).
    http://www.samba.org/samba/history/samba-3.6.23.html Release Notes Samba 3.6.23]

Samba 3.6.22 
------------------------

(**Updated 09-December-2013**)

* Monday, December 09 - Samba 3.6.22 has been released as a **Security Release** in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4408 CVE-2013-4408] (ACLs are not checked on opening an alternate data stream on a file or directory) and [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-6150 CVE-2012-6150] (pam_winbind login without require_membership_of restrictions).
    http://www.samba.org/samba/history/samba-3.6.22.html Release Notes Samba 3.6.22]

Samba 3.6.21 
------------------------

(**Updated 29-November-2013**)

* Friday, November 29 - Samba 3.6.21 has been released as the **last maintenance release** of the 3.6 release series
    http://www.samba.org/samba/history/samba-3.6.21.html Release Notes Samba 3.6.21]

Samba 3.6.20 
------------------------

(**Updated 11-November-2013**)

* Monday, November 11 - Samba 3.6.20 has been released as a **Security Release** on order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4475 CVE-2013-4475] (ACLs are not checked on opening an alternate data stream on a file or directory).
    http://www.samba.org/samba/history/samba-3.6.20.html Release Notes Samba 3.6.20]

Samba 3.6.19 
------------------------

(**Updated 25-September-2013**)

* Wednesday, September 25 - Samba 3.6.19 has been released
    http://www.samba.org/samba/history/samba-3.6.19.html Release Notes Samba 3.6.19]

Samba 3.6.18 
------------------------

(**Updated 14-August-2013**)

* Wednesday, August 14 - Samba 3.6.18 has been released
    http://www.samba.org/samba/history/samba-3.6.18.html Release Notes Samba 3.6.18]

Samba 3.6.17 
------------------------

(**Updated 05-August-2013**)

* Monday, August 05 - Samba 3.6.17 has been released as a **Security Release**
    http://www.samba.org/samba/history/samba-3.6.17.html Release Notes Samba 3.6.17]

Samba 3.6.16 
------------------------

(**Updated 19-June-2013**)

* Wednesday, June 19 - Samba 3.6.16 has been released
    http://www.samba.org/samba/history/samba-3.6.16.html Release Notes Samba 3.6.16]

Samba 3.6.15 
------------------------

(**Updated 08-May-2013**)

* Wednesday, May 08 - Samba 3.6.15
    http://www.samba.org/samba/history/samba-3.6.15.html Release Notes Samba 3.6.15]

Samba 3.6.14 
------------------------

(**Updated 29-April-2013**)

* Monday, April 29 - Samba 3.6.14 has been released
    http://www.samba.org/samba/history/samba-3.6.14.html Release Notes Samba 3.6.14]

Samba 3.6.13 
------------------------

(**Updated 18-March-2013**)

* Monday, March 18 - Samba 3.6.13 has been released
    http://www.samba.org/samba/history/samba-3.6.13.html Release Notes Samba 3.6.13]

Samba 3.6.12 
------------------------

(**Updated 30-January-2013**)

* Wednesday, January 30 - Samba 3.6.12 has been issued as a **Security Release** in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-0213 CVE-2013-0213] (Clickjacking issue in SWAT) and [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-0214 CVE-2013-0214] (Potential XSRF in SWAT).
    http://www.samba.org/samba/history/samba-3.6.12.html Release Notes Samba 3.6.12]

Samba 3.6.11 
------------------------

(**Updated 21-January-2013**)

* Monday, January 21 - Samba 3.6.11 has been released
    http://www.samba.org/samba/history/samba-3.6.11.html Release Notes Samba 3.6.11]

Samba 3.6.10 
------------------------

(**Updated 10-December-2012**)

* Monday, December 10 - Samba 3.6.10 has been released
    http://www.samba.org/samba/history/samba-3.6.10.html Release Notes Samba 3.6.10]

Samba 3.6.9 
------------------------

(**Updated 29-October-2012**)

* Monday, October 29 - Samba 3.6.9 has been released
    http://www.samba.org/samba/history/samba-3.6.9.html Release Notes Samba 3.6.9]

Samba 3.6.8 
------------------------

(**Updated 17-September-2012**)

* Monday, September 17 - Samba 3.6.8 has been released
    http://www.samba.org/samba/history/samba-3.6.8.html Release Notes Samba 3.6.8]

Samba 3.6.7 
------------------------

(**Updated 06-August-2012**)

* Monday, August 6 - Samba 3.6.7 has been released
    http://www.samba.org/samba/history/samba-3.6.7.html Release Notes Samba 3.6.7]

Samba 3.6.6 
------------------------

(**Updated 25-June-2012**)

* Monday, June 25 - Samba 3.6.6 has been released
    http://www.samba.org/samba/history/samba-3.6.6.html Release Notes Samba 3.6.6]

Samba 3.6.5 
------------------------

(**Updated 30-April-2012**)

* Monday, April 30 - Samba 3.6.5 **Security Release** has been released in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-2111 CVE-2012-2111 (Incorrect permission checks when granting/removing privileges can compromise file server security)].
    http://www.samba.org/samba/history/samba-3.6.5.html Release Notes Samba 3.6.5]

Samba 3.6.4 
------------------------

(**Updated 10-April-2012**)

* Tuesday, April 10 - Samba 3.6.4 **Security Release** has been released in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-1182 CVE-2012-1182 ("root" credential remote code execution)].
    http://www.samba.org/samba/history/samba-3.6.4.html Release Notes Samba 3.6.4]

Samba 3.6.3 
------------------------

(**Updated 29-January-2012**)

* Sunday, January 29 - Samba 3.6.3 has been released as a **security release** in order to address
    http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-0817 CVE-2012-0817]
    http://samba.org/samba/history/samba-3.6.3.html Release Notes Samba 3.6.3]

Samba 3.6.2 
------------------------

(**Updated 25-January-2012**)

* Wednesday, January 25 - Samba 3.6.2 has been released
    http://samba.org/samba/history/samba-3.6.2.html Release Notes Samba 3.6.2]

Samba 3.6.1 
------------------------

(**Updated 12-October-2011**)

* Thursday, October 20 - Samba 3.6.1 has been released
    http://samba.org/samba/history/samba-3.6.1.html Release Notes Samba 3.6.1]

Samba 3.6.0 
------------------------

(**Updated 09-August-2011**)

* Tuesday, August 9 2011 - Samba 3.6.0 has been released
    http://samba.org/samba/history/samba-3.6.0.html Release Notes Samba 3.6.0]

Samba 3.6.0rc3 
------------------------

(**Updated 26-July-2011**)

* Tuesday, July 26 2011 - Samba 3.6.0rc3 has been released
    http://samba.org/samba/ftp/rc/WHATSNEW-3-6-0rc3.txt Release Notes Samba 3.6.0rc3]

Samba 3.6.0rc2 
------------------------

(**Updated 7-June-2011**)

* Tuesday, June 7 2011 - Samba 3.6.0rc2 has been released
    http://samba.org/samba/ftp/rc/WHATSNEW-3-6-0rc2.txt Release Notes Samba 3.6.0rc2]

Samba 3.6.0rc1 
------------------------

(**Updated 17-May-2011**)

* Tuesday, May 17 - Samba 3.6.0rc1 has been released
    http://samba.org/samba/ftp/rc/WHATSNEW-3-6-0rc1.txt Release Notes Samba 3.6.0rc1]

Samba 3.6.0pre3 
------------------------

(**Updated 23-March-2011**)

* Tuesday, April 26 - Samba 3.6.0pre3 has been released
    http://samba.org/samba/ftp/pre/WHATSNEW-3-6-0pre3.txt Release Notes Samba 3.6.0pre3]

Samba 3.6.0pre2 
------------------------

(**Updated 12-April-2011**)

* Tuesday, April 12 - Samba 3.6.0pre2 has been released
    http://samba.org/samba/ftp/pre/WHATSNEW-3-6-0pre2.txt Release Notes Samba 3.6.0pre2]

Samba 3.6.0pre1 
------------------------

(**Updated 28-July-2010**)

* Wednesday, July 28 - Samba 3.6.0pre1 has been released
    http://samba.org/samba/ftp/pre/WHATSNEW-3-6-0pre1.txt Release Notes Samba 3.6.0pre1]