Release Planning for Samba 3.0
    <namespace>0</namespace>
<last_edited>2011-07-12T19:16:33Z</last_edited>
<last_editor>Kseeger</last_editor>

Samba 3.0 - discontinued 
------------------------

(**Updated 05-August-2009**)

After the release of Samba 3.0.36, the 3.0 series has been closed.
Please see `Samba Release Planning|Samba3 Release Planning` for information on current release
series. Patches for Samba 3.0 can be found [http://samba.org/samba/patches/ here].

Samba 3.0.37 
------------------------

(**Updated 1-October-2009**)

* Thursday, October 1 - Samba 3.3.8 has been issued as **Security Release** to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1888 CVE-2009-2906],
[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-2906 CVE-2009-2906] and
[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-2813 CVE-2009-2813].
Please note that Samba 3.0 is not under maintenace any longer. This release has been shipped
on a voluntary basis.
    http://www.samba.org/samba/history/samba-3.0.37.html Release Notes Samba 3.0.37]

Samba 3.0.36 
------------------------

(**Updated 05-August-2009**)

* Wednesday, August 5 2009: Samba 3.0.36 has been released
**Please note that this is the last release of the 3.0 series!**
    http://www.samba.org/samba/history/samba-3.0.36.html Release Notes Samba 3.0.36]

Samba 3.0.35 
------------------------

(**Updated 23-June-2009**)

* Tuesday, June 23 2009: Samba 3.0.35 **Security Release** has been released to address
[http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1888 CVE-2009-1888] ("Uninitialized read of a data value").
For more information, please see [http://samba.org/samba/history/security.html Samba Security page].
    http://samba.org/samba/security/CVE-2009-1888.html Security Advisory]

Samba 3.0.34 
------------------------

(**Updated 20-January-2009**)
* Tuesday, January 20 2009: Samba 3.0.34 has been released
    http://www.samba.org/samba/history/samba-3.0.34.html Release Notes Samba 3.0.34]

Samba 3.0.33 
------------------------

(**Updated 27-November-2008**)
* Thursday, November 27 - Samba 3.0.33 **Security Release** has been released to address [http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2008-4314 CVE-2008-4314] ("Potential leak of arbitrary memory contents").
    http://www.samba.org/samba/security/CVE-2008-4314.html Security advisory]

Samba 3.0.32 
------------------------

(**Updated 25-August-2008**)
* Samba 3.0.32 has been released on August 25.
    http://www.samba.org/samba/history/samba-3.0.32.html Release Notes Samba 3.0.32]

Samba 3.0.31 
------------------------

(**Updated 31-July-2008**)
* Samba 3.0.31 has been released on July 10.
    http://www.samba.org/samba/history/samba-3.0.31.html Release Notes Samba 3.0.31]

Samba 3.0.30 
------------------------

(**Updated 28-May-2008**)
* Wednesday, May 28 - Samba 3.0.30 has been released (security release ([http://www.samba.org/samba/security/CVE-2008-1105.html advisory CVE-2008-1105]))

Samba 3.0.29 
------------------------

(**Updated 23-May-2008**) 
* Thursday, May 22 - Samba 3.0.29 has been released
    http://www.samba.org/samba/history/samba-3.0.29.html Release Notes Samba 3.0.29]

Samba 3.0.28a 
------------------------

(**Updated 08-March-2008**) The following time based release schedule is currently in play for Samba 3.0.28a:

* Saturday, March 08 - Samba 3.0.28a has been released
    http://www.samba.org/samba/history/samba-3.0.28a.html Release Notes Samba 3.0.28a]

Samba 3.0.28 
------------------------

(**Updated 28-February-2008**)

* Monday, December 10 - Samba 3.0.28 has been released
    http://www.samba.org/samba/history/samba-3.0.28.html Release Notes Samba 3.0.28]

Samba 3.0.26 
------------------------

(**Updated 18-May-2007**)

* Monday, June 4 - Feature freeze for in the SAMBA_3_0_26 svn branch.
* Friday, June 15 - Samba 3.0.26pre1
* Remaining releases - TBD
    http://www.samba.org/samba/history/samba-3.0.26.html Release Notes Samba 3.0.26]

Samba 3.0.25a 
------------------------

(**Updated 18-May-2007**) 

* Friday, May 25 - Official 3.0.25a release

This means that all code must checked into the SAMBA_3_0_25 branch by Thursday, May 24, 7am (GMT-5).
The current [https://bugzilla.samba.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&product=Samba+3.0&target_milestone=3.0.25&long_desc_type=allwordssubstr&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&emailtype1=exact&email1=&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=&chfieldto=Now&chfieldvalue=&cmdtype=doit&order=Reuse+same+sort+as+last+time&field0-0-0=noop&type0-0-0=noop&value0-0-0= list of open bugs] has been targeted for the 3.0.25 milestone in Bugzilla.
    http://www.samba.org/samba/history/samba-3.0.25a.html Release Notes Samba 3.0.25a]

Samba 3.0.25 
------------------------

(**Updated 6-Apr-2007**)

Part of the Samba 3.0.25 release process includes several `Bugzilla Day` bug hunts.  See the [https://bugzilla.samba.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&product=Samba+3.0&target_milestone=3.0.25&long_desc_type=allwordssubstr&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&emailtype1=exact&email1=&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=&chfieldto=Now&chfieldvalue=&cmdtype=doit&order=Reuse+same+sort+as+last+time&field0-0-0=noop&type0-0-0=noop&value0-0-0= list of open bugs].

* Tuesday, Feb 27 - Samba 3.0.25pre1 (done)
* Tuesday, March 20  - Samba 3.0.25pre2 (done)
* Monday, April 9 - Samba 3.0.25 RC1 (done)
* Sunday, April 22 - Samba 3.0.25 RC2 (done)
* Wednesday, April 25 - Samba 3.0.25 RC3 (done)
* Monday, May 14 - Final 3.0.25 release (done).
    http://www.samba.org/samba/history/samba-3.0.25.html Release Notes Samba 3.0.25]

You can review the [http://lists.samba.org/archive/samba-technical/2007-February/051430.html original email thread on samba-technical] for more details.