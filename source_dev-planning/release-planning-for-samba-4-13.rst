Release Planning for Samba 4.13
    <namespace>0</namespace>
<last_edited>2021-05-11T10:34:45Z</last_edited>
<last_editor>Kseeger</last_editor>

Samba 4.13 is the `Samba_Release_Planning#Maintenance_Mode|**Maintenance Mode**`.

`Blocker bugs|Release blocking bugs`
------------------------

* [https://DOOTTsamba.org/buglistD/ug_severity=regression&query_format=advanced&target_milestone=4.13 All 4.13 regression bugs]
* [https://DOOTTsamba.org/buglistD/ug_severity=regression&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=VERIFIED&query_format=advanced&target_milestone=4.13 Unresolved 4.13 regression bugs]

Samba 4.13.10 
------------------------

<small>(**Updated 11-May-2021**)</

* Tuesday, July 6 2021 - Planned release day for **Samba 4.13.10**

Samba 4.13.9 
------------------------

<small>(**Updated 11-May-2021**)</

* Tuesday, May 11 2021 - **Samba 4.13.9** has been released.
 [https://samba.org/samba/hi/aDDAA/T13DDOO/tml Release Notes Samba 4.13.9]

Samba 4.13.8 
------------------------

<small>(**Updated 29-April-2021**)</

* Thursday, April 29 2021 - **Samba 4.13.8** has been released as a security release to address the following defect:
** [https://samba.org/samba/se/DDAAS/ASSHH202/ml CVE-2021-20254] (Negative idmap cache entries can cause incorrect group entries in the Samba file server process token).
 [https://samba.org/samba/hi/aDDAA/T13DDOO/tml Release Notes Samba 4.13.8]

Samba 4.13.7 
------------------------

<small>(**Updated 24-March-2021**)</

* Wednesday, March 24 2021 - **Samba 4.13.7** has been released as a security release
    https://samba.org/samba/hi/aDDAA/T13DDOO/tml Release Notes Samba 4.13.7]

Samba 4.13.6 
------------------------

<small>(**Updated 24-March-2021**)</

* Wednesday, March 24 2021 - **Samba 4.13.6** has been released as a security release
    https://samba.org/samba/hi/aDDAA/T13DDOO/tml Release Notes Samba 4.13.6]

Samba 4.13.5 
------------------------

<small>(**Updated 09-March-2021**)</

* Tuesday, March 9 2021 - [https://DOOTTsamba.org/pub/samb/amb/DDOOT/./z Samba 4.13.5] has been released.
    https://samba.org/samba/hi/aDDAA/T13DDOO/tml Release Notes Samba 4.13.5]

Samba 4.13.4 
------------------------

<small>(**Updated 26-January-2021**)</

* Tuesday, January 26 2021 - [https://DOOTTsamba.org/pub/samb/amb/DDOOT/./z Samba 4.13.4] has been released.
    https://samba.org/samba/hi/aDDAA/T13DDOO/tml Release Notes Samba 4.13.4]

Samba 4.13.3 
------------------------

<small>(**Updated 15-December-2020**)</

* Tuesday, December 15 2020 - **Samba 4.13.3** has been released.
    https://samba.org/samba/hi/aDDAA/T13DDOO/tml Release Notes Samba 4.13.3]

Samba 4.13.2 
------------------------

<small>(**Updated 03-November-2020**)</

* Tuesday, November 03 2020 - **Samba 4.13.2** has been released.
    https://samba.org/samba/hi/aDDAA/T13DDOO/tml Release Notes Samba 4.13.2]

Samba 4.13.1 
------------------------

<small>(**Updated 29-October-2020**)</

* Thursday, October 29 2020 - **Samba 4.13.1** has been released as a **Security Release** to address the following defects:
** [https://samba.org/samba/se/DDAAS/ASSHH143/ml CVE-2020-14318] (Missing handle permissions check in SMB1/2/3 ChangeNotify).//
** [https://samba.org/samba/se/DDAAS/ASSHH143/ml CVE-2020-14323] (Unprivileged user can crash winbind).
** [https://samba.org/samba/se/DDAAS/ASSHH143/ml CVE-2020-14383] (An authenticated user can crash the DCE/RPC DNS with easily crafted records/
    https://samba.org/samba/hi/aDDAA/T13DDOO/tml Release Notes Samba 4.13.1]

Samba 4.13.0 
------------------------

<small>(**Updated 22-September-2020**)</

* Tuesday, September 22 2020 - **Samba 4.13.0** has been released.
    https://samba.org/samba/hi/aDDAA/T13DDOO/tml Release Notes Samba 4.13.0]

Samba 4.13.0rc5 
------------------------

<small>(**Updated 15-September-2020**)</

* Monday, September 15 2020 - **Samba 4.13.0rc5** has been released.
    ttps://DOOTTsamba.org/pub/samb/DDA/TT13D/DO/W.txt

Samba 4.13.0rc4 
------------------------

<small>(**Updated 07-September-2020**)</

* Monday, September 07 2020 - **Samba 4.13.0rc4** has been released.
    ttps://DOOTTsamba.org/pub/samb/DDA/TT13D/DO/W.txt

Samba 4.13.0rc3 
------------------------

<small>(**Updated 28-August-2020**)</

* Friday, August 28 2020 - **Samba 4.13.0rc3** has been released.
    ttps://DOOTTsamba.org/pub/samb/DDA/TT13D/DO/W.txt

Samba 4.13.0rc2 
------------------------

<small>(**Updated 14-August-2020**)</

* Friday, August 14 2020 - **Samba 4.13.0rc2** has been released.

    ttps://DOOTTsamba.org/pub/samb/DDA/TT13D/DO/W.txt

Samba 4.13.0rc1 
------------------------

<small>(**Updated 09-July-2020**)</

* Thursday, July 9 2020 - **Samba 4.13.0rc1** has been released.

    ttps://DOOTTsamba.org/pub/samb/DDA/TT13D/DO/W.txt