Release Planning for Samba 4.3
    <namespace>0</namespace>
<last_edited>2017-03-07T11:08:52Z</last_edited>
<last_editor>Kseeger</last_editor>

Samba 4.3 has been marked `Samba_Release_Planning#Discontinued_.28End_of_Life.29|**discontinued**`.

Samba 4.3.13 
------------------------

<small>(**Updated 19-December-2016**)</small>

* Monday, December 19 2016 - **Samba 4.3.13** has been released as a **Security Release** in order to address the following CVEs:
**[https://www.samba.org/samba/security/CVE-2016-2123.html CVE-2016-2123] (Samba NDR Parsing ndr_pull_dnsp_name Heap-based Buffer Overflow Remote Code Execution Vulnerability),
** [https://www.samba.org/samba/security/CVE-2016-2125.html CVE-2016-2125] (Unconditional privilege delegation to Kerberos servers in trusted realms) and 
** [https://www.samba.org/samba/security/CVE-2016-2126.html CVE-2016-2126] (Flaws in Kerberos PAC validation can trigger privilege elevation).
    https://www.samba.org/samba/history/samba-4.3.13.html Release Notes Samba 4.3.13]

Samba 4.3.12 
------------------------

<small>(**Updated 03-November-2016**)</small>

* Wednesday, November 3 - **Samba 4.3.12** has been released as the last bugfix release of 4.3
    https://www.samba.org/samba/history/samba-4.3.12.html Release Notes Samba 4.3.12]

Samba 4.3.11 
------------------------

<small>(**Updated 07-July-2016**)</small>

* Thursday, July 7 - **Samba 4.3.11** has been released as a **Security Release** in order to address [https://www.samba.org/samba/security/CVE-2016-2119.html CVE-2016-2119] (Client side SMB2/3 required signing can be downgraded).
    https://www.samba.org/samba/history/samba-4.3.11.html Release Notes Samba 4.3.11]

Samba 4.3.10 
------------------------

<small>(**Updated 15-June-2016**)</small>

* Wednesday, June 15 - **Samba 4.3.10** has been released
    https://www.samba.org/samba/history/samba-4.3.10.html Release Notes Samba 4.3.10]

Samba 4.3.9 
------------------------

<small>(**Updated 02-May-2016**)</small>

* Monday, May 02 - Samba 4.3.9 has been released
    https://www.samba.org/samba/history/samba-4.3.9.html Release Notes Samba 4.3.9]

Samba 4.3.8 
------------------------

<small>(**Updated 12-April-2016**)</small>

* Tuesday, April 12 - Samba 4.3.8 has been released as a **Security Release**
    https://www.samba.org/samba/history/samba-4.3.8.html Release Notes Samba 4.3.8]

Samba 4.3.7 
------------------------

<small>(**Updated 12-April-2016**)</small>

* Tuesday, April 12 - Samba 4.3.7 has been released as a **Security Release**
    https://www.samba.org/samba/history/samba-4.3.7.html Release Notes Samba 4.3.7]

Samba 4.3.6 
------------------------

<small>(**Updated 08-March-2016**)</small>

* Tuesday, March 8 - Samba 4.3.6 has been released as a **Security Release**
    https://www.samba.org/samba/history/samba-4.3.6.html Release Notes Samba 4.3.6]

Samba 4.3.5 
------------------------

<small>(**Updated 23-February-2016**)</small>

* Tuesday, February 23 - Samba 4.3.5 has been released
    https://www.samba.org/samba/history/samba-4.3.5.html Release Notes Samba 4.3.5]

Samba 4.3.4 
------------------------

<small>(**Updated 12-January-2016**)</small>

* Tuesday, January 12 - Samba 4.3.3 has been released
    https://www.samba.org/samba/history/samba-4.3.4.html Release Notes Samba 4.3.4]

Samba 4.3.3 
------------------------

<small>(**Updated 16-December-2015**)</small>

* Wednesday, December 15 - Samba 4.3.3 has been released as a **Security Release**
    https://www.samba.org/samba/history/samba-4.3.3.html Release Notes Samba 4.3.3]

Samba 4.3.2 
------------------------

<small>(**Updated 01-December-2015**)</small>

* Tuesday, December 1 - Samba 4.3.2 has been released
    https://www.samba.org/samba/history/samba-4.3.2.html Release Notes Samba 4.3.2]

Samba 4.3.1 
------------------------

<small>(**Updated 20-October-2015**)</small>

* Tuesday, October 20 - Samba 4.3.1 has been released
    https://www.samba.org/samba/history/samba-4.3.1.html Release Notes Samba 4.3.1]

Samba 4.3.0 
------------------------

<small>(**Updated 08-September-2015**)</small>

* Tuesday, September 8 - Samba 4.3.0 has been released
    https://www.samba.org/samba/history/samba-4.3.0.html Release Notes Samba 4.3.0]

Samba 4.3.0rc4 
------------------------

<small>(**Updated 01-September-2015**)</small>

* Tuesday, September 1 - Samba 4.3.0rc4 has been released
    https://download.samba.org/pub/samba/rc/samba-4.3.0rc4.WHATSNEW.txt Release Notes Samba 4.3.0rc4]

Samba 4.3.0rc3 
------------------------

<small>(**Updated 18-August-2015**)</small>

* Tuesday, August 18 - Samba 4.3.0rc3 has been released
    https://download.samba.org/pub/samba/rc/samba-4.3.0rc3.WHATSNEW.txt Release Notes Samba 4.3.0rc3]

Samba 4.3.0rc2 
------------------------

<small>(**Updated 18-August-2015**)</small>

* Tuesday, August 4 - Samba 4.3.0rc2 has been released
    https://download.samba.org/pub/samba/rc/samba-4.3.0rc2.WHATSNEW.txt Release Notes Samba 4.3.0rc2]

Samba 4.3.0rc1 
------------------------

<small>(**Updated 18-August-2015**)</small>

* Tuesday, July 21 - Samba 4.3.0rc1 has been released
    https://download.samba.org/pub/samba/rc/samba-4.3.0rc1.WHATSNEW.txt Release Notes Samba 4.3.0rc1]