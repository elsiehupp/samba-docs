ADWS / AD Powershell compatibility
    <namespace>0</namespace>
<last_edited>2021-04-13T01:31:19Z</last_edited>
<last_editor>Abartlet</last_editor>

Samba does **not** support many of the AD PowerShell commands that manipulate AD objects, as these are backed by [https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2008-R2-and-2008/dd391908(v=ws.10)?redirectedfrom=MSDN Active Directory Web Services] on a Windows DC.

There are quite a few useful things [https://devblogs.microsoft.com/scripting/use-active-directory-cmdlets-with-powershell-to-find-users/ Powershell can do] and do simpler than [https://devblogs.microsoft.com/scripting/how-can-i-get-a-list-of-all-the-disabled-user-accounts-in-active-directory/ earlier approaches with VBScript].

So it would be useful if Samba provided this interface.

Thankfully a [https://gitlab.com/catalyst-samba/samba-adws prototype project] has been built by [https://catalyst.net.nz/services/samba Catalyst's Samba Team].

This proof of concept was built to prove the understanding of the protocol, and was tested with 
 AD-GetComputer 
it is likely that other ``AD-Get`` interfaces also work.

It has not been reviewed for security and directly accesses the Samba AD database in ``sam.ldb``