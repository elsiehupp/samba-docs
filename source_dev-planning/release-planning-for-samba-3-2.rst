Release Planning for Samba 3.2
    <namespace>0</namespace>
<last_edited>2011-08-09T19:01:35Z</last_edited>
<last_editor>Kseeger</last_editor>

Samba 3.2 discontinued 
------------------------

(**Updated 01-March-2010**)

With the release of Samba 3.5.0, Samba 3.2 has been marked **discontinued**.

Samba 3.2 turned into security fixes only mode 
------------------------

(**Updated 11-August-2009**)

Moving forward, any 3.2.x releases will be on a as needed basis
for **security issues only**.

Samba 3.2.15 
------------------------

(**Updated 1-October-2009**)

* Thursday, October 1 - Samba 3.2.15 has been issued as **Security Release** to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1888 CVE-2009-2906],
[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-2906 CVE-2009-2906] and
[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-2813 CVE-2009-2813].
    http://www.samba.org/samba/history/samba-3.2.15.html Release Notes Samba 3.2.15]

Samba 3.2.14 
------------------------

(**Updated 23-June-2009**)

* Wednesday, August 12 - Samba 3.2.14 has been released
**Please note that this is the last bug fix release of the 3.2 series!**
    http://www.samba.org/samba/history/samba-3.2.14.html Release Notes Samba 3.2.14]

Samba 3.2.13 
------------------------

(**Updated 23-June-2009**)

* Tuesday, June 23 2009: Samba 3.2.13 **Security Release** has been released to address
[http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1886 CVE-2009-1886]
("Formatstring vulnerability in smbclient") and
[http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1888 CVE-2009-1888] ("Uninitialized read of a data value").
For more information, please see [http://samba.org/samba/history/security.html Samba Security page].
    http://samba.org/samba/security/CVE-2009-1886.html Security Advisory for CVE-2009-1886]
    http://samba.org/samba/security/CVE-2009-1888.html Security Advisory for CVE-2009-1888]

Samba 3.2.12 
------------------------

(**Updated 16-June-2009**)

* Tuesday, June 16 - Samba 3.2.12 has been released
    http://www.samba.org/samba/history/samba-3.2.12.html Release Notes Samba 3.2.12]

Samba 3.2.11 
------------------------

(**Updated 17-April-2009**)

* Friday, April 17 - Samba 3.2.11 has been release to address [https://bugzilla.samba.org/show_bug.cgi?id=6263 BUG 6263] and [https://bugzilla.samba.org/show_bug.cgi?id=6089 BUG 6089].
    http://www.samba.org/samba/history/samba-3.2.11.html Release Notes Samba 3.2.11]

Samba 3.2.10 
------------------------

(**Updated 1-April-2009**)

* Wednesday, April 1 - Samba 3.2.10 has been released due to update problems in Samba 3.2.9 (see [https://bugzilla.samba.org/show_bug.cgi?id=6195 BUG 6195] and the [http://www.samba.org/samba/history/samba-3.2.10.html release notes] for more details).

Samba 3.2.9 
------------------------

(**Updated 31-March-2009**)

* Tuesday, March 31 - Samba 3.2.9 has been released
    http://www.samba.org/samba/history/samba-3.2.9.html Release Notes Samba 3.2.9]

Samba 3.2.8 
------------------------

(**Updated 03-February-2009**)

* Tuesday, February 03 - Samba 3.2.8 has been released
    http://www.samba.org/samba/history/samba-3.2.8.html Release Notes Samba 3.2.8]

Samba 3.2.7 
------------------------

(**Updated 05-January-2009**)

* Monday, January 05 - Samba 3.2.7 **Security Release** has been released to address [http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-0022 CVE-2009-0022] ("Potential access to "/" in setups with registry shares enabled")
    http://www.samba.org/samba/security/CVE-2009-0022.html Security advisory]

Samba 3.2.6 
------------------------

(**Updated 28-November-2008**)

* Wednesday, December 10 - Samba 3.2.6 has been released.
    http://www.samba.org/samba/history/samba-3.2.6.html Release Notes Samba 3.2.6]

Samba 3.2.5 
------------------------

(**Updated 27-November-2008**)

* Thursday, November 27 - Samba 3.2.5 **Security Release** has been released to address [http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2008-4314 CVE-2008-4314] ("Potential leak of arbitrary memory contents").
    http://www.samba.org/samba/security/CVE-2008-4314.html Security advisory]

Samba 3.2.4 
------------------------

(**Updated 18-September-2008**)

* Thursday, September 18 - Samba 3.2.4 has been released
    http://www.samba.org/samba/history/samba-3.2.4.html Release Notes Samba 3.2.4]

Samba 3.2.3 
------------------------

(**Updated 27-August-2008**)

* Wednesday, August 27 - Samba 3.2.3 **Security Release** has been released to address CVE-2008-3789 ("Wrong permissions of group_mapping.ldb")       
    http://www.samba.org/samba/security/CVE-2008-3789.html Security advisory]

Samba 3.2.2 
------------------------

(**Updated 18-August-2008**)

* Tuesday, August 19 - Planned release date for 3.2.2 (bugfix release)
    http://www.samba.org/samba/history/samba-3.2.2.html Release Notes Samba 3.2.2]

Samba 3.2.1 
------------------------

(**Updated 20-July-2008**) 

* Tuesday, August 5 - 3.2.1 has been released.
    http://www.samba.org/samba/history/samba-3.2.1.html Release Notes Samba 3.2.1]

Samba 3.2.0 
------------------------

(**Updated 02-July-2008**) The following time based release schedule is currently in play for Samba 3.2.0:

* Friday, February 29 - Feature freeze for Samba 3.2.0pre2 in the v3-2-stable git branch.
* Tuesday, March 4 - Samba 3.2.0pre2 has been released.
* Friday, March 28 - Planned release date for 3.2.0pre3: **postponed** due to some critical bugs.
* Friday, April 18 - Planned release date for 3.2.0.: **postponed**
* Tuesday, April 22 - Feature freeze for Samba 3.2.0pre3 in the v3-2-stable git branch.
* Friday, April 25 - Samba 3.2.0pre3 has been released.
* Friday, May 23 - Samba 3.2.0rc1 has been released.
* Tuesday, June 10 - Samba 3.2.0rc2 has been released.
* Tuesday, July 1 - 3.2.0 final has been released.
    http://www.samba.org/samba/history/samba-3.2.0.html Release Notes Samba 3.2.0]