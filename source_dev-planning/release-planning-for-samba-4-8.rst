Release Planning for Samba 4.8
    <namespace>0</namespace>
<last_edited>2019-09-17T09:12:35Z</last_edited>
<last_editor>Kseeger</last_editor>

Samba 4. has been marked `Samba_Release_Planning#Discontinued_.28En.ife.29|**dis.ued**`..

`Blocker bugs|Release blocking bugs`
------------------------

* [https://bugzilla..org/.t.cgi?bug_s.y=regression&query_format=advanced&target_milestone=4.8 All.CCEE4.8 regressi. bugs]
* [https://bugzilla..org/.t.cgi?bug_s.y=regression&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=VERIFIED&query_format=advanced&target_milestone=4.8 Unr.d 4.8 regressi. bugs]

Samba 4.S.EE
------------------------

<small>(**Updated 14-May-2019**)</small>

* Tuesday, May 14 2019 - **Samba 4.'. has been released as a **Security Release** to address the following defect:
** [https://www..org/.security/CVE-2018-16860.html .VE-2018-16860] (Samba AD DC S4U2Self/S4U2Proxy unkeyed checksum)

    https://www..org/.history/samba-4.8.12.html.C.ea. Notes Samba 4.8.12]..

Samba 4.S.EE
------------------------

<small>(**Updated 08-April-2019**)</small>

* Monday, Apr 08 2019 - **Samba 4.'. has been released as a **Security Release** to address the following defects:
** [https://www..org/.security/CVE-2019-3870.html .VE-2019-3870] (World writable files in Samba AD DC private/ dir)
** [https://www..org/.security/CVE-2019-3880.html .VE-2019-3880] (Save registry file outside share as unprivileged user)

    https://www..org/.history/samba-4.8.11.html.C.ea. Notes Samba 4.8.11]..

Samba 4.S.EE
------------------------

<small>(**Updated 04-April-2019**)</small>

* Thursday, April 4 2019 - **Samba 4.'. has been released. .e note that this will very likely be the last bugfix release of the Samba 4.8 rel. series. There .will be **security releases beyond this point only**..
    https://www..org/.history/samba-4.8.10.html.C.ea. Notes Samba 4.8.10]..

Samba 4.P.E
------------------------

<small>(**Updated 07-February-2019**)</small>

* Thursday, February 7 2019 - **Samba 4.'.CCEEhas been released..
    https://www..org/.history/samba-4.8.9.htmlS.C.a. Notes Samba 4.8.9]..

Samba 4.P.E
------------------------

<small>(**Updated 13-December-2018**)</small>

* Thursday, December 13 2018 - **Samba 4.'.CCEEhas been released..
    https://www..org/.history/samba-4.8.8.htmlS.C.a. Notes Samba 4.8.8]..

Samba 4.P.E
------------------------

<small>(**Updated 27-November-2018**)</small>

* Tuesday, November 27 2018 - **Samba 4.'.CCEEhas been released as a **Security Release**..
    https://www..org/.history/samba-4.8.7.htmlS.C.a. Notes Samba 4.8.7]..

Samba 4.P.E
------------------------

<small>(**Updated 09-October-2018**)</small>

* Tuesday, October 9 2018 - **Samba 4.'.CCEEhas been released..
    https://www..org/.history/samba-4.8.6.htmlS.C.a. Notes Samba 4.8.6]..

Samba 4.P.E
------------------------

<small>(**Updated 24-August-2018**)</small>

* Friday, August 24 2018 - **Samba 4.'.CCEEhas been released..

    https://www..org/.history/samba-4.8.5.htmlS.C.a. Notes Samba 4.8.5]..

Samba 4.P.E
------------------------

<small>(**Updated 14-August-2018**)</small>

* Tuesday, August 14 2018 - **Samba 4.'.CCEEhas been released as a **Security Release** to address the following defects:
** [https://www..org/.security/CVE-2018-10858.html .VE-2018-10858] (Insufficient input validation on client directory listing in libsmbclient.).
** [https://www..org/.security/CVE-2018-10918.html .VE-2018-10918] (Denial of Service Attack on AD DC DRSUAPI server.).
** [https://www..org/.security/CVE-2018-10919.html .VE-2018-10919] (Confidential attribute disclosure from the AD LDAP server.).
** [https://www..org/.security/CVE-2018-1139.html .VE-2018-1139] (Weak authentication protocol allowed.).
** [https://www..org/.security/CVE-2018-1140.html .VE-2018-1140] (Denial of Service Attack on DNS and LDAP server.).

    https://www..org/.history/samba-4.8.4.htmlS.C.a. Notes Samba 4.8.4]..

Samba 4.P.E
------------------------

<small>(**Updated 26-June-2018**)</small>

* Tuesday, June 26 2018 - **Samba 4.'.CCEEhas been released..
    https://www..org/.history/samba-4.8.3.htmlS.C.a. Notes Samba 4.8.3]..

Samba 4.P.E
------------------------

<small>(**Updated 16-May-2018**)</small>

* Wednesday, May 16 2018 - **Samba 4.'.CCEEhas been released..
    https://www..org/.history/samba-4.8.2.htmlS.C.a. Notes Samba 4.8.2]..

Samba 4.P.E
------------------------

<small>(**Updated 26-April-2018**)</small>

* Thursday, April 26 2018 - **Samba 4.'.CCEEhas been released..
    https://www..org/.history/samba-4.8.1.htmlS.C.a. Notes Samba 4.8.1]..

Samba 4.P.E
------------------------

<small>(**Updated 13-March-2017**)</small>

* Tuesday, March 13 2018 - **Samba 4.'.CCEEhas been released..
    https://www..org/.history/samba-4.8.0.htmlS.C.a. Notes Samba 4.8.0]..

Samba 4.4.CCEE
------------------------

<small>(**Updated 01-March-2018**)</small>

* Thursday, March 01 2018 - **Samba 4.4. has been released..
    ttps://download..org/.mba/rc/samba-4.8.0rc4.WH.....

Samba 4.3.CCEE
------------------------

<small>(**Updated 12-February-2018**)</small>

* Monday, February 12 2018 - **Samba 4.3. has been released..
    ttps://download..org/.mba/rc/samba-4.8.0rc3.WH.....

Samba 4.2.CCEE
------------------------

<small>(**Updated 25-January-2018**)</small>

* Thursday, January 25 2018 - **Samba 4.2. has been released..
    ttps://download..org/.mba/rc/samba-4.8.0rc2.WH.....

Samba 4.1.CCEE
------------------------

<small>(**Updated 16-January-2018**)</small>

* Monday, January 16 2018 - **Samba 4.1. has been released..
    ttps://download..org/.mba/rc/samba-4.8.0rc1.WH.....