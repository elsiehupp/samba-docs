Release Planning for Samba 4.5
    <namespace>0</namespace>
<last_edited>2018-09-18T09:24:24Z</last_edited>
<last_editor>Kseeger</last_editor>

Samba 4.5 has been marked `Samba_Release_Planning#Discontinued_.28End_of_Life.29|**discontinued**`.

`All bugs|All known bugs`
------------------------

* [https://bugzilla.samba.org/buglist.cgi?bug_severity=regression&bug_severity=critical&bug_severity=major&bug_severity=normal&bug_severity=minor&bug_severity=trivial&bug_severity=enhancement&query_format=advanced&target_milestone=4.5 All 4.5 bugs]
* [https://bugzilla.samba.org/buglist.cgi?bug_severity=regression&bug_severity=critical&bug_severity=major&bug_severity=normal&bug_severity=minor&bug_severity=trivial&bug_severity=enhancement&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=VERIFIED&query_format=advanced&target_milestone=4.5 All unresolved 4.5 bugs]

Samba 4.5.16 
------------------------

<small>(**Updated 13-March-2018**)</small>

* Tuesday, March 13 2018 - **Samba 4.5.16** has been released as a **security release** in order to address [https://www.samba.org/samba/security/CVE-2018-1050.html CVE-2018-1050] (Denial of Service Attack on external print server) and [https://www.samba.org/samba/security/CVE-2018-1057.html CVE-2018-1057] (Authenticated users can change other users' password).
    https://www.samba.org/samba/history/samba-4.5.16.html Release Notes Samba 4.5.16]

Samba 4.5.15 
------------------------

<small>(**Updated 21-November-2017**)</small>

* Tuesday, November 21 2017 - **Samba 4.5.15** has been released as a **security release** in order to address [https://www.samba.org/samba/security/CVE-2017-14746.html CVE-2017-14746] (Use-after-free vulnerability) and [https://www.samba.org/samba/security/CVE-2017-15275.html CVE-2017-15275] (Server heap memory information leak).
    https://www.samba.org/samba/history/samba-4.5.15.html Release Notes Samba 4.5.15]

Samba 4.5.14 
------------------------

<small>(**Updated 20-August-2017**)</small>

* Wednesday, September 20 2017 - **Samba 4.5.14** has been released as a **Security Release** in order to address
** [https://www.samba.org/samba/security/CVE-2017-12150.html CVE-2017-12150] (SMB1/2/3 connections may not require signing where they should),
** [https://www.samba.org/samba/security/CVE-2017-12151.html CVE-2017-12151] (SMB3 connections don't keep encryption across DFS redirects) and
** [https://www.samba.org/samba/security/CVE-2017-12163.html CVE-2017-12163] (Server memory information leak over SMB1).
    https://www.samba.org/samba/history/samba-4.5.14.html Release Notes Samba 4.5.14]

Samba 4.5.13 
------------------------

<small>(**Updated 31-August-2017**)</small>

* Thursday, August 31 2017 - **Samba 4.5.13** has been released
    https://www.samba.org/samba/history/samba-4.5.13.html Release Notes Samba 4.5.13]

Samba 4.5.12 
------------------------

<small>(**Updated 13-July-2017**)</small>

* Wednesday, July 12 2017 - **Samba 4.5.12** has been released as a **Security Release** in order to address [https://www.samba.org/samba/security/CVE-2017-11103.html CVE-2017-11103] (Orpheus' Lyre mutual authentication validation bypass).
    https://www.samba.org/samba/history/samba-4.5.12.html Release Notes Samba 4.5.12]

Samba 4.5.11 
------------------------

<small>(**Updated 06-July-2017**)</small>

* Thursday, July 6 2017 - **Samba 4.5.11** has been released
    https://www.samba.org/samba/history/samba-4.5.11.html Release Notes Samba 4.5.11]

Samba 4.5.10 
------------------------

<small>(**Updated 24-May-2017**)</small>

* Wednesday, May 24 2017 - **Samba 4.5.10** has been released as a **Security Release** in order to address [https://www.samba.org/samba/security/CVE-2017-7494.html CVE-2017-7494] (Remote code execution from a writable share).
    https://www.samba.org/samba/history/samba-4.5.10.html Release Notes Samba 4.5.10]

Samba 4.5.9 
------------------------

<small>(**Updated 18-May-2017**)</small>

* Thursday, May 18 2017 - **Samba 4.5.9** has been released
    https://www.samba.org/samba/history/samba-4.5.9.html Release Notes Samba 4.5.9]

Samba 4.5.8 
------------------------

<small>(**Updated 31-March-2017**)</small>

* Friday, March 31 2017 - **Samba 4.5.8** has been released
    https://www.samba.org/samba/history/samba-4.5.8.html Release Notes Samba 4.5.8]

Samba 4.5.7 
------------------------

<small>(**Updated 23-March-2017**)</small>

* Thursday, March 23 2017 - **Samba 4.5.7** has been released as a **Security Release** in order to address [https://www.samba.org/samba/security/CVE-2017-2619.html CVE-2017-2619] (Symlink race allows access outside share definition).
    https://www.samba.org/samba/history/samba-4.5.7.html Release Notes Samba 4.5.7]

Samba 4.5.6 
------------------------

<small>(**Updated 09-March-2017**)</small>

* Thursday, March 9 2017 - **Samba 4.5.6** has been released
    https://www.samba.org/samba/history/samba-4.5.6.html Release Notes Samba 4.5.6]

Samba 4.5.5 
------------------------

<small>(**Updated 30-January-2017**)</small>

* Monday, January 30 2017 - **Samba 4.5.5** has been released
    https://www.samba.org/samba/history/samba-4.5.5.html Release Notes Samba 4.5.5]

Samba 4.5.4 
------------------------

<small>(**Updated 18-January-2017**)</small>

* Wednesday, January 18 2017 - **Samba 4.5.4** has been released
    https://www.samba.org/samba/history/samba-4.5.4.html Release Notes Samba 4.5.4]

Samba 4.5.3 
------------------------

<small>(**Updated 19-December-2016**)</small>

* Monday, December 19 2016 - **Samba 4.5.3** has been released as a **Security Release** in order to address the following CVEs:
**[https://www.samba.org/samba/security/CVE-2016-2123.html CVE-2016-2123] (Samba NDR Parsing ndr_pull_dnsp_name Heap-based Buffer Overflow Remote Code Execution Vulnerability),
** [https://www.samba.org/samba/security/CVE-2016-2125.html CVE-2016-2125] (Unconditional privilege delegation to Kerberos servers in trusted realms) and 
** [https://www.samba.org/samba/security/CVE-2016-2126.html CVE-2016-2126] (Flaws in Kerberos PAC validation can trigger privilege elevation).
    https://www.samba.org/samba/history/samba-4.5.3.html Release Notes Samba 4.5.3]

Samba 4.5.2 
------------------------

<small>(**Updated 07-December-2016**)</small>

* Wednesday, December 7 2016 - **Samba 4.5.2** has been released
    https://www.samba.org/samba/history/samba-4.5.2.html Release Notes Samba 4.5.2]

Samba 4.5.1 
------------------------

<small>(**Updated 26-October-2016**)</small>

* Wednesday, 26 October 2016 - **Samba 4.5.1** has been released
    https://www.samba.org/samba/history/samba-4.5.1.html Release Notes Samba 4.5.1]

Samba 4.5.0 
------------------------

<small>(**Updated 07-September-2016**)</small>

* Wednesday, 07 September 2016 - **Samba  4.5.0** has been released
    https://www.samba.org/samba/history/samba-4.5.0.html Release Notes Samba 4.5.0]

Samba 4.5.0rc3 
------------------------

<small>(**Updated 29-August-2016**)</small>

* Monday, 29 August 2016 - **Samba 4.5.0rc3** has been released
    https://download.samba.org/pub/samba/rc/samba-4.5.0rc3.WHATSNEW.txt Release Notes Samba 4.5.0rc3]

Samba 4.5.0rc2 
------------------------

<small>(**Updated 29-August-2016**)</small>

* Wednesday, 10 August 2016 - **Samba 4.5.0rc2** has been released
    https://download.samba.org/pub/samba/rc/samba-4.5.0rc2.WHATSNEW.txt Release Notes Samba 4.5.0rc2]

Samba 4.5.0rc1 
------------------------

<small>(**Updated 28-July-2016**)</small>

* Wednesday, 28 July 2016 - **Samba 4.5.0rc1** has been released
    https://download.samba.org/pub/samba/rc/samba-4.5.0rc1.WHATSNEW.txt Release Notes Samba 4.5.0rc1]