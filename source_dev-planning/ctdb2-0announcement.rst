CTDB2.0announcement
    <namespace>0</namespace>
<last_edited>2012-10-31T17:20:20Z</last_edited>
<last_editor>Fraz</last_editor>

==============================

nnouncement CTDB release 2.0
===============================

This is long overdue CTDB release. There have been numerous code enhancements and bug fixes since the last release of CTDB.

Highlights
------------------------

* Support for readonly records (http://ctdb.samba.org/doc/readonlyrecords.txt)
* Locking API to detect deadlocks between ctdb and samba
* Fetch-lock optimization to rate-limit concurrent requests for same record
* Support for policy routing
* Modified IP allocation algorithm
* Improved database vacuuming
* New test infrastructure

Reporting bugs & Development Discussion
------------------------

Please discuss this release on the samba-technical mailing list or by joining the #ctdb IRC channel on irc.freenode.net.

All bug reports should be filed under CTDB product in the project's Bugzilla database.
* https://bugzilla.samba.org/

Download Details
------------------------

The source code can be downloaded from:
* http://ftp.samba.org/pub/ctdb/

Git repository
* git://git.samba.org/ctdb.git
* http://git.samba.org/?p=ctdb.git;a=summary  (Git via web)

CTDB documentation
* https://ctdb.samba.org/