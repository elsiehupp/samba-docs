Release Planning for Samba 4.4
    <namespace>0</namespace>
<last_edited>2017-09-21T09:22:38Z</last_edited>
<last_editor>Kseeger</last_editor>

Samba 4.4 has been marked `Samba_Release_Planning#Discontinued_.28End_of_Life.29|**discontinued**`.

Samba 4.4.16 
------------------------

<small>(**Updated 20-August-2017**)</small>

* Wednesday, September 20 2017 - **Samba 4.4.16** has been released as a **Security Release** in order to address
** [https://www.samba.org/samba/security/CVE-2017-12150.html CVE-2017-12150] (SMB1/2/3 connections may not require signing where they should),
** [https://www.samba.org/samba/security/CVE-2017-12151.html CVE-2017-12151] (SMB3 connections don't keep encryption across DFS redirects) and
** [https://www.samba.org/samba/security/CVE-2017-12163.html CVE-2017-12163] (Server memory information leak over SMB1).
    https://www.samba.org/samba/history/samba-4.4.16.html Release Notes Samba 4.4.16]

Samba 4.4.15 
------------------------

<small>(**Updated 13-July-2017**)</small>

* Wednesday, July 12 2017 - **Samba 4.4.15** has been released as a **Security Release** in order to address [https://www.samba.org/samba/security/CVE-2017-11103.html CVE-2017-11103] (Orpheus' Lyre mutual authentication validation bypass).
    https://www.samba.org/samba/history/samba-4.4.15.html Release Notes Samba 4.4.15]

Samba 4.4.14 
------------------------

<small>(**Updated 24-May-2017**)</small>

* Wednesday, May 24 2017 - **Samba 4.4.14** has been released as a **Security Release** in order to address [https://www.samba.org/samba/security/CVE-2017-7494.html CVE-2017-7494] (Remote code execution from a writable share).
    https://www.samba.org/samba/history/samba-4.4.14.html Release Notes Samba 4.4.14]

Samba 4.4.13 
------------------------

<small>(**Updated 31-March-2017**)</small>

* Friday, March 31 - **Samba 4.4.13** has been released
    https://www.samba.org/samba/history/samba-4.4.13.html Release Notes Samba 4.4.13]

Samba 4.4.12 
------------------------

<small>(**Updated 23-March-2017**)</small>

* Thursday, March 23 - **Samba 4.4.12** has been released as a **Security Release** in order to address [https://www.samba.org/samba/security/CVE-2017-2619.html CVE-2017-2619] (Symlink race allows access outside share definition).
    https://www.samba.org/samba/history/samba-4.4.12.html Release Notes Samba 4.4.12]

Samba 4.4.11 
------------------------

<small>(**Updated 16-March-2017**)</small>

* Thursday,  March 16 - **Samba 4.4.11** has been released. This is the **last bug fix release** of the Samba 4.4 release series.
    https://www.samba.org/samba/history/samba-4.4.11.html Release Notes Samba 4.4.11]

Samba 4.4.10 
------------------------

<small>(**Updated 01-March-2017**)</small>

* Wednesday,  March 1 - **Samba 4.4.10** has been released
    https://www.samba.org/samba/history/samba-4.4.10.html Release Notes Samba 4.4.10]

Samba 4.4.9 
------------------------

<small>(**Updated 04-January-2017**)</small>

* Wednesday, January 4 - **Samba 4.4.9** has been released
    https://www.samba.org/samba/history/samba-4.4.9.html Release Notes Samba 4.4.9]

Samba 4.4.8 
------------------------

<small>(**Updated 19-December-2016**)</small>

* Monday, December 19 2016 - **Samba 4.4.8** has been released as a **Security Release** in order to address the following CVEs:
**[https://www.samba.org/samba/security/CVE-2016-2123.html CVE-2016-2123] (Samba NDR Parsing ndr_pull_dnsp_name Heap-based Buffer Overflow Remote Code Execution Vulnerability),
** [https://www.samba.org/samba/security/CVE-2016-2125.html CVE-2016-2125] (Unconditional privilege delegation to Kerberos servers in trusted realms) and 
** [https://www.samba.org/samba/security/CVE-2016-2126.html CVE-2016-2126] (Flaws in Kerberos PAC validation can trigger privilege elevation).
    https://www.samba.org/samba/history/samba-4.4.8.html Release Notes Samba 4.4.8]

Samba 4.4.7 
------------------------

<small>(**Updated 26-October-2016**)</small>

* Wednesday, October 26 - **Samba 4.4.7** has been released
    https://www.samba.org/samba/history/samba-4.4.7.html Release Notes Samba 4.4.7]

Samba 4.4.6 
------------------------

<small>(**Updated 22-September-2016**)</small>

* Thursday, September 22 - **Samba 4.4.6** has been released
    https://www.samba.org/samba/history/samba-4.4.6.html Release Notes Samba 4.4.6]

Samba 4.4.5 
------------------------

<small>(**Updated 07-July-2016**)</small>

* Thursday, July 7 - **Samba 4.4.5** has been released as a **Security Release** in order to address [https://www.samba.org/samba/security/CVE-2016-2119.html CVE-2016-2119] (Client side SMB2/3 required signing can be downgraded).
    https://www.samba.org/samba/history/samba-4.4.5.html Release Notes Samba 4.4.5]

Samba 4.4.4 
------------------------

<small>(**Updated 07-June-2016**)</small>

* Tuesday, June 07 - **Samba 4.4.4** has been released
    https://www.samba.org/samba/history/samba-4.4.4.html Release Notes Samba 4.4.4]

Samba 4.4.3 
------------------------

<small>(**Updated 02-May-2016**)</small>

* Monday, May 02 - **Samba 4.4.3** has been released
    https://www.samba.org/samba/history/samba-4.4.3.html Release Notes Samba 4.4.3]

Samba 4.4.2 
------------------------

<small>(**Updated 12-April-2016**)</small>

* Tuesday, April 12 - **Samba 4.4.2** has been released as a **Security Release**
    https://www.samba.org/samba/history/samba-4.4.2.html Release Notes Samba 4.4.2]

Samba 4.4.1 
------------------------

<small>(**Updated 12-April-2016**)</small>

* Tuesday, April 12 - **Samba 4.4.1** has been released as a **Security Release**
    https://www.samba.org/samba/history/samba-4.4.1.html Release Notes Samba 4.4.1]

Samba 4.4.0 
------------------------

<small>(**Updated 22-March-2016**)</small>

* Tuesday, March 22 - **Samba 4.4.0** has been released
    https://www.samba.org/samba/history/samba-4.4.0.html Release Notes Samba 4.4.0]

Samba 4.4.0rc5 
------------------------

<small>(**Updated 16-March-2016**)</small>

* Wednesday, March 16 - Samba 4.4.0rc5 has been released
    https://download.samba.org/pub/samba/rc/samba-4.4.0rc5.WHATSNEW.txt Release Notes Samba 4.4.0rc5]

Samba 4.4.0rc4 
------------------------

<small>(**Updated 08-March-2016**)</small>

* Tuesday, March 8 - Samba 4.4.0rc4 has been released
    https://download.samba.org/pub/samba/rc/samba-4.4.0rc4.WHATSNEW.txt Release Notes Samba 4.4.0rc4]

Samba 4.4.0rc3 
------------------------

<small>(**Updated 23-February-2016**)</small>

* Tuesday, February 23- Samba 4.4.0rc3 has been released
    https://download.samba.org/pub/samba/rc/samba-4.4.0rc3.WHATSNEW.txt Release Notes Samba 4.4.0rc3]

Samba 4.4.0rc2 
------------------------

<small>(**Updated 09-February-2016**)</small>

* Tuesday, February 9 - Samba 4.4.0rc2 has been released
    https://download.samba.org/pub/samba/rc/samba-4.4.0rc2.WHATSNEW.txt Release Notes Samba 4.4.0rc2]

Samba 4.4.0rc1 
------------------------

<small>(**Updated 27-Januar-2016**)</small>

* Wednesday, January 27 - Samba 4.4.0rc1 has been released
    https://download.samba.org/pub/samba/rc/samba-4.4.0rc1.WHATSNEW.txt Release Notes Samba 4.4.0rc1]