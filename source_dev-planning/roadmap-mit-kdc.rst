Roadmap MIT KDC
    <namespace>0</namespace>
<last_edited>2019-10-17T09:13:23Z</last_edited>
<last_editor>GlaDiaC</last_editor>

=================
Samba AD with MIT KDC= 

This page lists tasks which need to be done to bring the MIT KDC support for Samba AP on the same functional level as we have with Heimdal. We need help to implement those features. Let us know if you want to pick up a task, the Samba Team is not actively working on those!

TODO 
------------------------

* Implement KDC-canon tests for MIT KDC -> source4/torture/krb5/kdc-canon-heimdal.c
* PKINIT support required for using smart cards
* Service for User to Self-service (S4U2self)
* Service for User to Proxy (S4U2proxy)
* Allow starting the MIT KDC with multiple worker processes (``-w numworkers``)
* Add auth logging support ([https://git.samba.org/?p=asn/samba.git;a=shortlog;h=refs/heads/master-mit-kdc-ok WIP branch])
* Computer GPO's are not applied, see  [https://bugzilla.samba.org/show_bug.cgi?id=13516 Bug 13516]
* Define API for a libkdc in MIT Kerberos
* Running as a Read only domain controller (RODC)
* Add support for [http://k5wiki.kerberos.org/wiki/Projects/IAKERB IAKERB]