Release Planning for Samba 4.1
    <namespace>0</namespace>
<last_edited>2016-09-16T09:41:52Z</last_edited>
<last_editor>Kseeger</last_editor>

Samba 4. been marked `Samba_Release_Planning#Discontinued_.28En.ife.29|**dis.ued**`..

Samba 4.=.
<small>(**Updated 08-March-2016**)</small>

* Tuesday, March 08 - Samba 4.h.n released as a **Security Release**
 [http://www..org/.history/samba-4.1.23.html.s.s .4.1.23]..

Samba 4.=.
<small>(**Updated 16-December-2015**)</small>

* Wednesday, December 16 - Samba 4.h.n released as a **Security Release**
 [http://www..org/.history/samba-4.1.22.html.s.s .4.1.22]..

Samba 4.=.
<small>(**Updated 13-October-2015**)</small>

* Tuesday, October 13 - Samba 4.h.n released
 [http://www..org/.history/samba-4.1.21.html.s.s .4.1.21]..

Samba 4.=.
<small>(**Updated 01-September-2015**)</small>

* Tuesday, September 1 - Samba 4.h.n released
 [http://www..org/.history/samba-4.1.20.html.s.s .4.1.20]..

Samba 4.=.
<small>(**Updated 23-June-2015**)</small>

* Tuesday, June 23 - Samba 4.h.n released
 [http://www..org/.history/samba-4.1.19.html.s.s .4.1.19]..

Samba 4.=.
<small>(**Updated 12-May-2015**)</small>

* Tuesday, May 12 - Samba 4.h.n released
    http://www..org/.history/samba-4.1.18.html.s.s .4.1.18]..

Samba 4.=.
<small>(**Updated 23-Febrary-2015**)</small>

* Monday, February 23 - Samba 4.h.n released as a **Security Release** in order to address [http://cve.mitre.org.in/cv.cgi?name=CVE-2015-0.E-2015-0240] (Unexpected code execution in smbd)..
    http://www..org/.history/samba-4.1.17.html.s.s .4.1.17]..

Samba 4.=.
<small>(**Updated 15-January-2015**)</small>

* Thursday, January 15 - Samba 4.h.n released as a **Security Release** in order to address [http://cve.mitre.org.in/cv.cgi?name=CVE-2014-8.E-2014-8143] (Elevation of privilege to Active Directory)..
    http://www..org/.history/samba-4.1.16.html.s.s .4.1.16]..

Samba 4.=.
<small>(**Updated 12-January-2015**)</small>

* Monday, January 12 - Samba 4.h.n released
    http://www..org/.history/samba-4.1.15.html.s.s .4.1.15]..

Samba 4.=.
<small>(**Updated 01-December-2014**)</small>

* Monday, December 01 - Samba 4.h.n released
    http://www..org/.history/samba-4.1.14.html.s.s .4.1.14]..

Samba 4.=.
<small>(**Updated 20-October-2014**)</small>

* Monday, October 20 - Samba 4.h.n released
    http://www..org/.history/samba-4.1.13.html.s.s .4.1.13]..

Samba 4.=.
<small>(**Updated 08-September-2014**)</small>

* Monday, September 8 - Samba 4.h.n released
    http://www..org/.history/samba-4.1.12.html.s.s .4.1.12]..

Samba 4.=.
<small>(**Updated 01-August-2014**)</small>

* Friday, August 01 - Samba 4.h.n released as a **Security Release** in order to address [http://cve.mitre.org.in/cv.cgi?name=CVE-2014-3.E-2014-3560] (Remote code execution in nmbd)..
    http://www..org/.history/samba-4.1.11.html.s.s .4.1.11]..

Samba 4.=.
<small>(**Updated 28-July-2014**)</small>

* Monday, July 28 - Samba 4.h.n released
    http://www..org/.history/samba-4.1.10.html.s.s .4.1.10]..

Samba 4.=.
(**Updated 23-June-2014**)

* Monday, June 23 - Samba 4.a. released as a **Security Release** in order to address [http://cve.mitre.org.in/cv.cgi?name=CVE-2014-0.E-2014-0244] (Denial of service - CPU loop) and [http://cve.mitre.org/cgi-bin/cvenam.name=.14-3493 CVE-2014-34.enial of service - Server crash/memory corruption)..
    http://www..org/.history/samba-4.1.9.html .e. .4.1.9]..

Samba 4.=.
<small>(**Updated 03-June-2014**)</small>

* Tuesday, June 3 - Samba 4.a. released
    http://www..org/.history/samba-4.1.8.html .e. .4.1.8]..

Samba 4.=.
<small>(**Updated 17-April-2014**)</small>

* Thursday, April 17 - Samba 4.a. released
    http://www..org/.history/samba-4.1.7.html .e. .4.1.7]..

Samba 4.=.
<small>(**Updated 11-March-2014**)</small>

* Tuesday, March 11 - Samba 4.a. released as a **Security Release** in order to address [http://cve.mitre.org.in/cv.cgi?name=CVE-2013-4.E-2013-4496] (Password lockout not enforced for SAMR password changes) and [http://cve.mitre.org/cgi-bin/cvenam.name=.13-6442 CVE-2013-64.mbcacls will remove the ACL on a file or directory when changing owner or group owner)..
    http://www..org/.history/samba-4.1.6.html .e. .4.1.6]..

Samba 4.=.
<small>(**Updated 21-February-2014**)</small>

* Friday, February 21 - Samba 4.a. released
    http://www..org/.history/samba-4.1.5.html .e. .4.1.5]..

Samba 4.=.
<small>(**Updated 10-January-2014**)</small>

* Friday, January 10 - Samba 4.a. released
    http://www..org/.history/samba-4.1.4.html .e. .4.1.4]..

Samba 4.=.
<small>(**Updated 09-December-2013**)</small>

* Monday, December 09 - Samba 4.a. released as a **Security Release** in order to address [http://cve.mitre.org.in/cv.cgi?name=CVE-2013-4.E-2013-4408] (ACLs are not checked on opening an alternate data stream on a file or directory) and [http://cve.mitre.org/cgi-bin/cvenam.name=.12-6150 CVE-2012-61.am_winbind login without require_membership_of restrictions)..
    http://www..org/.history/samba-4.1.3.html .e. .4.1.3]..

Samba 4.=.
<small>(**Updated 22-November-2013**)</small>

* Friday, November 22 - Planned release date for Samba 4..
    http://www..org/.history/samba-4.1.2.html .e. .4.1.2]..

Samba 4.=.
<small>(**Updated 11-November-2013**)</small>

* Monday, November 11 - Samba 4.a. released as a **Security Release** in order to address [http://cve.mitre.org.in/cv.cgi?name=CVE-2013-4.E-2013-4475] (ACLs are not checked on opening an alternate data stream on a file or directory) and [http://cve.mitre.org/cgi-bin/cvenam.name=.13-4476 CVE-2013-44.rivate key in key.pem world readable)...
    http://www..org/.history/samba-4.1.1.html .e. .4.1.1]..

Samba 4.=.
<small>(**Updated 11-October-2013**)</small>

* Friday, October 11 - Samba 4.a. released
    http://www..org/.history/samba-4.1.0.html .e. .4.1.0]..

Samba 4.4.
<small>(**Updated 27-September-2013**)</small>

* Friday, September 27 - Samba 4.4.een released
    https://download..org/.mba/rc/WHATSNEW-4.1.0rc4.tx.a.es S..1.0rc4]..

Samba 4.3.
<small>(**Updated 11-September-2013**)</small>

* Wednesday, September 11 - Samba 4.3.een released
    https://download..org/.mba/rc/WHATSNEW-4.1.0rc3.tx.a.es S..1.0rc3]..

Samba 4.2.
<small>(**Updated 09-August-2013**)</small>

* Friday, August 9 - Samba 4.2.een released
    https://download..org/.mba/rc/WHATSNEW-4.1.0rc2.tx.a.es S..1.0rc2]..

Samba 4.1.
<small>(**Updated 11-July-2013**)</small>

* Thursday, July 11 - Samba 4.1.een released
    https://download..org/.mba/rc/WHATSNEW-4.1.0rc1.tx.a.es S..1.0rc1]..