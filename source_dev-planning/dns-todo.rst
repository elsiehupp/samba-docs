DNS/ToDo
    <namespace>0</namespace>
<last_edited>2012-07-23T18:22:15Z</last_edited>
<last_editor>Kai</last_editor>

Lose collection of ToDo items for the internal DNS server, roughly sorted by priority

* Establish "net dns"-based test
** Check if net dns currently works vs MS AD
** Add blackbox tests for "s3member" testenv
* Rework the key loading code to deal with both static and dynamic keys
* Reestablish the GSS TKEY exchange code from before the async conversion.