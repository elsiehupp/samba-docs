Testing removal of ifdef or configure checks
    <namespace>0</namespace>
<last_edited>2020-03-14T21:58:57Z</last_edited>
<last_editor>Abartlet</last_editor>

==============================

ifdef removal is risky
===============================

Large-scale removal of "unused" ifdef or the configure checks that produce #defines is an important, but risky task.  

It is difficult (and time consuming) to prove that the code is unused and that there is no change to the compiled output.

A script to help obtain CPP output
------------------------

`File:Gcc-E.py.txt` is a Python script that wraps gcc -E (to get the CPP output) to attempt to solve this dilemma, so as to provide a result that can be reasoned with via tools like [https://www.samba.org/ftp/paulus/ dirdiff]

===============================
Getting the results
===============================

------------------------

 git checkout origin/master
 git clean -x -f -d
 CC="gcc-E" ./configure.developer && make -j
 mv bin /tmp/master-bin

 git checkout newbranch
 git clean -x -f -d
 CC="gcc-E" ./configure.developer && make -j

===============================
Reading the results
===============================

------------------------

 dirdiff /tmp/master-bin bin
or
 diff -ur --ignore-matching-lines="^#" /tmp/master-bin/ bin> /tmp/diff

Getting just one file's CPP output
------------------------

Thanks to [https://lists.samba.org/archive/samba-technical/2020-March/134980.html waf author Thomas Nagy]
 WAF_CMD_FORMAT=string PYTHONHASHSEED=1 WAF_MAKE=1  ./buildtools/bin/waf -v build

Then just edit the command to add the -E (and perhaps a new output file).