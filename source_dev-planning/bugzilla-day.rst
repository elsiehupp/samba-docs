Bugzilla Day
    <namespace>0</namespace>
<last_edited>2008-05-13T09:06:57Z</last_edited>
<last_editor>Kseeger</last_editor>


9 May 2008 
------------------------

The third preview of Samba 3.2.0 has been released on Friday, April 25.
That is why the next Bugzilla Day is planned for Friday, 9 May.
The focus will be on installing Samba 3.2.0pre3 on non-production servers and
eating our own dogfood.  We'll also work on performing a triage of the 
bugs filed against Samba Samba 3.2 and determine if there are any open showstoppers.

If you want to participate, here are a few guidelines:

* You do not have to be a developer.
* You must be running 3.2.0pre3 on some server or client.
* You must be using a configuration that worked successfully on a prior version of Samba 3.0.  We'll not spend time debugging server configurations unrelated to the 3.2.0 release.
* You must be comfortable providing sufficient debug information in the case that we identify a legitimate failure.  This might be debug logs, network traces, etc...

**Summary:**
This Bugzilla Day was cancelled due to a lack of participation.
List of  [https://bugzilla.samba.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&version=3.2.0&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&emailassigned_to1=1&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailqa_contact2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=&chfieldto=Now&chfieldvalue=&cmdtype=doit&order=Reuse+same+sort+as+last+time&field0-0-0=noop&type0-0-0=noop&value0-0-0= open bugs].

18 Mar 2008 
------------------------

The second preview of Samba 3.2.0 has been released on Wednesday, March 5.
That is why the next Bugzilla Day is planned for Tuesday, 18 March.
The focus will be on installing Samba 3.2.0pre2 on non-production servers and
eating our own dogfood.  We'll also work on performing a triage of the 
bugs filed against Samba Samba 3.2 and determine if there are any open showstoppers.

If you want to participate, here are a few guidelines:

* You do not have to be a developer.
* You must be running 3.2.0pre2 on some server or client.
* You must be using a configuration that worked successfully on a prior version of Samba 3.0.  We'll not spend time debugging server configurations unrelated to the 3.2.0 release.
* You must be comfortable providing sufficient debug information in the case that we identify a legitimate failure.  This might be debug logs, network traces, etc...

**Summary:**
The list of [https://bugzilla.samba.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&version=3.2.0&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&bug_status=RESOLVED&emailassigned_to1=1&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailqa_contact2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=&chfieldto=Now&chfieldvalue=&cmdtype=doit&order=Reuse+same+sort+as+last+time&field0-0-0=noop&type0-0-0=noop&value0-0-0= closed bugs against 3.2.0] is growing, but there are still [https://bugzilla.samba.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&version=3.2.0&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&emailassigned_to1=1&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailqa_contact2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=&chfieldto=Now&chfieldvalue=&cmdtype=doit&order=Reuse+same+sort+as+last+time&field0-0-0=noop&type0-0-0=noop&value0-0-0= open bugs]. 
Plans are to release 3.2.0pre3 on March 28.

9 Apr 2007 
------------------------

The First Release Candidate of SAMBA_3_0_25 will be on Monday, April 9.  So we'll have a bug hunt on the following Tuesday.  The focus will be on installing Samba 3.0.25rc1 on non-production (or semi-production) servers and eating our own dogfood.  We'll also work on performing a triage of the [https://bugzilla.samba.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&product=Samba+3.0&long_desc_type=allwordssubstr&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&emailtype1=exact&email1=&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=&chfieldto=Now&chfieldvalue=&cmdtype=doit&order=Reuse+same+sort+as+last+time&field0-0-0=noop&type0-0-0=noop&value0-0-0=   bugs filed against Samba Samba 3.0] and determine if there are any open showstoppers.  Here is the list of [https://bugzilla.samba.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&product=Samba+3.0&target_milestone=3.0.25&long_desc_type=allwordssubstr&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&emailtype1=exact&email1=&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=&chfieldto=Now&chfieldvalue=&cmdtype=doit&order=Reuse+same+sort+as+last+time&field0-0-0=noop&type0-0-0=noop&value0-0-0= bugs tagged to be fixed] for 3.0.25 final.

If you want to participate, here are a few guidelines:

* You do not have to be a developer.
* You must be running 3.0.25rc1 on some server or client.
* You must be using a configuration that worked successfully on a prior version of Samba 3.0.  We'll not spend time debugging server configurations unrelated to the 3.0.25 release.
* You must be comfortable providing sufficient debug information in the case that we identify a legitimate failure.  This might be debug logs, network traces, etc....

**Summary**: The 3.0.25rc1 release is holding up well so far.  The list of [https://bugzilla.samba.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&product=Samba+3.0&target_milestone=3.0.25&long_desc_type=allwordssubstr&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&emailtype1=exact&email1=&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=&chfieldto=Now&chfieldvalue=&cmdtype=doit&order=Reuse+same+sort+as+last+time&field0-0-0=noop&type0-0-0=noop&value0-0-0= fixed or closed bugs against 3.0.25] is growing.  You can stay current by applying the [http://www.samba.org/~jerry/patches/samba-3-0-25rc1-bugday-apr10.patch patch for 3.0.25rc1 from the BugHunt].  Plans are to do another quick RC2 release on Monday, April 16.

1 Mar 2007 
------------------------

After some hickups and last minute pressing bugs, we are back on for the initial 
3.0.25 preview release.  The tentative release schedule has been updated on the 
`TODOs_for_3.0.25`.  

The first preview release of Samba 3.0.25 is scheduled for Tuesday, Feb 27st.  Therefore, the theme of the next Bugzilla Day is to bash on 3.0.25pre1.  We'll keep the same time from 6am - 6pm Pacific Standard Time (GMT-8) on the #samba-technical channel at irc.freenode.net.  Talk to **coffeedude** if you need help getting starting.

**Update**  Samba 3.0.25pre1 is now publicly available and can be downloaded from [http://download.samba.org/samba/ftp/pre/ here].  Or if 
you prefer, just grab a copy of the [http://devel.samba.org/ SAMBA_3_0_25 svn tree].  Time to start testing things such as file service, printing, and various authentication configurations such as domain controllers and member servers.
If you find a bug, please [https://bugzilla.samba.org/ report it] and if you fix one, make sure to attach the patch to the BUG report and email the [mailto:samba-technical@samba.org mailing list].

**Summary:**  We found some bugs.  We fixed some bugs.  We checked some code into svn.  Overall the 3.0.25pre1 testing went well.  There are still some issues to sort out with the new IdMap interface and winbindd running on a Samba DC, but things are generally working smoothly.  We solved some issues with NTLMv2, the CIFS Unix Extensions, Winbind's connection manager, and User Manager against a Samba DC.  A pretty productive day.   Jerry won the stupid act of the day for locking himself out of his Windows Vista VMware session.  

8 Feb 2007 
------------------------

The Bug bash is on from 6am - 6pm Pacific Standard Time (GMT-8) on the #samba-technical 
channel at irc.freenode.net.  Talk to **coffeedude** if you need help getting starting.

Today's bug day theme is Vista.  There are several [https://bugzilla.samba.org/buglist.cgi?query_format=specific&order=relevance+desc&bug_status=__open__&product=Samba+3.0&content=Vista open bugs filed against Samba and Vista clients].  it is also a good idea to have a Windows XP client setup as well to verify that whatever might be failing with Vista actually works with XP.  This will also allow you to do comparative tracing using [http://www.wireshark.org/ Wireshark] between the two clients and see why one succeeds and the other fails.

You can download the [http://www.samba.org/samba/patches/ current set of vista patches against Samba 3.0.24] and start testing.  The patches set was generated using [http://savannah.nongnu.org/projects/quilt/ Quilt] and will will be updated throughout the day.  All of these should already been in the SAMBA_3_0_25 svn tree.  If you are testing against that code base, please ignore the previous link.

If you don't have a Vista client to test with and would still like to help out, please spend some time 
testing any client and any configuration against the SAMBA_3_0_25 branch.

**Summary:** The first bugzilla day went off pretty well.  We were able to close off several bugs
([https://bugzilla.samba.org/show_bug.cgi?id=4356 Bug 4356], [https://bugzilla.samba.org/show_bug.cgi?id=4093 Bug 4093] (partial fix), 
[https://bugzilla.samba.org/show_bug.cgi?id=4188 Bug 4188], 
and [http://lists.samba.org/archive/samba-technical/2007-February/051453.html several printing issues]).  The final patchset from today's work will be posted at http://www.samba.org/samba/patches/.