Presentations
    <namespace>0</namespace>
<last_edited>2020-06-18T12:59:31Z</last_edited>
<last_editor>Fraz</last_editor>

=================
2020
=================

May 2020 sambaXP
------------------------

===============================
Day 1
===============================

------------------------

* [https://sambaxp.org/archive-data-samba/sxp20/sxp20-d1/ The Tutorial]
===============================
Day 2
===============================

------------------------

* [https://sambaxp.org/archive-data-samba/sxp20/sxp20-d2/ The conference 1]
===============================
Day 3
===============================

------------------------

* [https://sambaxp.org/archive-data-samba/sxp20/sxp20-d3/ The conference 2]
February 2020 FOSDEM
------------------------

* Speaker
* :Jeremy Allison
*[https://fosdem.org/2020/schedule/event/whats_new_in_samba/ Whats new in Samba]
*[https://fosdem.org/2020/schedule/event/whats_new_in_samba/attachments/slides/4140/export/events/attachments/whats_new_in_samba/slides/4140/samba_current_state_FOSDEM_2020.odp slides]
* movie
**[https://ftp.osuosl.org/pub/fosdem/2020/H.1308/whats_new_in_samba.webm webm]
**[https://video.fosdem.org/2020/H.1308/whats_new_in_samba.mp4 mp4]

January 2020 Linux.conf.au 
------------------------

* Speaker
* :Andrew Bartlett
*[https://sysadmin.miniconf.org/presentations20.html#140 Abstracts]
*[https://www.youtube.com/watch?v=D5hl0fqA0Bc Video]
*[https://sysadmin.miniconf.org/2020/lca2020-andrew_bartlett-samba_2020_why_are_we_still_in_the_1980s_for_auth.pdf slides]
*[https://twitter.com/NextDayVideo/status/1216938358779203584 twitter]

=================

2019 = 
February 2019 FOSDEM Brussels 
------------------------

===============================
Clustered Samba: Witness Protection Programming
===============================

------------------------

* Speakers
* :David Disseldorp
* :Samuel Cabrero
*[https://fosdem.org/2019/schedule/event/clustered_samba/attachments/slides/3385/export/events/attachments/clustered_samba/slides/3385/clustered_samba_slides.pdf slides ]
*[https://video.fosdem.org/2019/H.2214/clustered_samba.mp4 Video]
===============================
SMB2 POSIX Extensions - Where we are, what remains to be done.
===============================

------------------------

* Speakers
* :Jeremy Allison
*[https://video.fosdem.org/2019/H.2214/smb2_posix_extensions.mp4 Video]
===============================
Handling Security Flaws in an Open Source Project
===============================

------------------------

* Speakers
* :Jeremy Allison
*[https://video.fosdem.org/2019/K.1.105/security_flaws.mp4 Video]

January 2019 Linux.conf.au Christchurch
------------------------

* [http://youtu.be/KEyzrnr2UhU YouTube] [https://archive.org/details/lca2019-Samba_for_the_100000_user_enterprise_are_we_there_yet MP4] [http://sysadmin.miniconf.org/2019/lca2019-andrew_bartlett-samba_for_the_100000_user_enterprise.pdf Samba for the 100,000 user enterprise: are we there yet?] Andrew Bartlett
* [https://www.youtube.com/watch?v=Dte0ovSNGLk YouTube] [http://sysadmin.miniconf.org/2019/lca2019-kirin_van_der_veer-dirty_samba_hacks_to_make_your_life_easier.odp Dirty Samba hacks to make your life easier] Kirin van der Veer

=================

2018
=================

November 2018 Linux Plumbers Conference 2018
------------------------

* [https://linuxplumbersconf.org/event/2/contributions/83/attachments/35/40/linux-plumbersv2.pdf Solving Linux File System Pain Points]
October 2018 Open Source Summit Europe 2018
------------------------

* [https://events.linuxfoundation.org/wp-content/uploads/2017/12/Handling-Security-Flaws-in-an-Open-Source-Project-Jeremy-Allison-Google.pdf How to Handle Security Flaws in an Open Source Project]
June 2018 SambaXP 
------------------------

* [https://sambaxp.org/archive_data/SambaXP2018-SLIDES/SambaXP_2018_presentations.zip SambaXP 2018]
* [https://sambaxp.org/fileadmin/user_upload/sambaXP2018-Slides/Bartlett-SambaXP-2018-patterns-anti-patterns-16-9.pdf Patterns and anti-patterns in Samba Development] by [https://www.samba.org/~abartlet Andrew Bartlett]

May 2018 Linux Storage, Filesystem, and Memory-Management Summit (LSFMM)
------------------------

* [https://lwn.net/Articles/754506/ Network filesystem topics]
* [https://lwn.net/Articles/754507/ SMB/CIFS compounding support]

February 2018 FOSDEM 2018
------------------------

* [https://fosdem.org/2018/schedule/event/samba_authentication_authorization/ Samba authentication and authorization]  Volker Lendecke
* [https://fosdem.org/2018/schedule/event/samba_ad_in_fedora/ Samba AD in Fedora] Andreas Schneider
* [https://fosdem.org/2018/schedule/event/samba_ad_it_works/ SaMBa-AD, it works] Vincent Cardon

Articles at https://lwn.net
* [https://lwn.net/SubscriberLink/747122/dcffef1611e424b4/|Samba authentication and authorization]  
* [https://lwn.net/SubscriberLink/747098/929c9b6e3712e5eb/|Two FOSDEM talks on Samba 4]

January 2018 LinuxConfAU
------------------------

*  [http://sysadmin.miniconf.org/2018/lca2018-andrew_bartlett-fixing_tridges_mistakes-taking_samba_ad_to_scale.pdf Fixing tridge's mistakes: Taking Samba AD to scale] Andrew Bartlett
* *   [http://youtu.be/JNR8bJkWCjA on youtube]

=================

2017 = 

May 2017 - SambaXP Göttingen, Germany
------------------------

*[https://sambaxp.org/archive_data/SambaXP2017-SLIDES/ SambaXP 2017]

===============================
2nd Day
===============================

------------------------

* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day2/Fixing%20a%20mess%20Symlinks%20and%20security%20-%20Jeremy%20Allison.odp Fixing a mess: Symlinks and security], Jeremy Allison
* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day2/Samba_Microsoft%20alignment%20-%20possible%20future%20directions%20-%20Tom%20Talpey.pdf Samba/Microsoft alignment: possible future directions], Tom Talpey
* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day2/Track1/Measuring%20Samba%20performance%20-%20Douglas%20Bagnall/douglas-bagnall.pdf Measuring Samba Performance], Douglas Bagnall
* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day2/Track1/New%20printing%20protocols%20in%20Samba%20-%20G%fcnther%20Deschner.pdf New printing protocols in Samba], Günther Deschner
* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day2/Track1/Pushing%20the%20Boundaries%20of%20SMB3%20Status%20of%20the%20Linux%20kernel%20client%20and%20interoperability%20with%20Samba%20-%20Steven%20French.pdf Pushing the Boundaries of SMB3: Status of the Linux Kernel client and interoperability with Samba], Steve French
* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day2/Track1/Something%20must%20be%20done%20-%20Ralph%20B%f6hme.pdf Something must be done], Ralph Böhme
* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day2/Track1/Windows%20Search%20Protocol%20recap%20&%20update%20-%20Jim%20McDonough.odp Windows Search Protocol recap & update], Jim McDonough
* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day2/Track2/CTDB%20remix%20%23U2013%201st%20movement%20%23U2013%20dreaming%20the%20fantasy%20-%20Amitay%20Isaacs.pdf CTDB Remix I: Dreaming the Fantasy], Amitay Isaacs
* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day2/Track2/CTDB%20remix%20%23U2013%202nd%20movement%20%23U2013%20designing%20the%20reality%20-%20Martin%20Schwenke.pdf CTDB remix II: Designing the Reality], Martin Schwenke
* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day2/Track2/Samba,%20quo%20vadis%20-%20Kai%20Blin.pdf Samba, Quo Vadis?], Kai Blin
* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day2/Track2/Samba%20and%20Python%203%20-%20Petr%20Viktorin.html Python 3], Lumír Balhar
* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day2/Track2/Samba%20at%20Scale%20100,000%20user%20AD%20Domains%20-%20Andrew%20Bartlett.pdf Samba at Scale 100,000 user AD Domains] Andrew Bartlett
===============================
3rd Day
===============================

------------------------

* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day3/Is%20Samba%204%20AD%20Ready%20for%20Global%20Enterprise%20-%20Kevin%20Kunkel.pdf Is Samba 4 AD Ready for Global Enterprise?], Kevin Kunkel
* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day3/Playing%20with%20domains%20not%20the%20Windows%20way%20-%20Denis%20Cardon.pdf Playing with AD Domains], Denis Cardon
* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day3/Track1/Can%20we%20Fake%20a%20Failover%20-%20Christopher%20Hertel.pdf Can we Fake a Failover], Christopher Hertel
* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day3/Track1/Distributed%20Filesystem%20(DFS)%20in%20cifs.ko%20what%23U2019s%20new%20and%20ideas%20for%20future%20improvements%20-%20Aur%23U00e9lien%20Aptel/aurelien-aptel-dfs.html Distributed Filesystem (DFS) in cifs.ko what's 2019s new and ideas for future improvements], Aurelien Aptel
* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day3/Track1/How%20to%20write%20a%20Samba%20VFS%20module%20-%20Jeremy%20Allison.odp How to write a Samba VFS module], Jeremy Allison
* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day3/Track1/Samba%20KCC%20Saying%20No%20to%20Full-Mesh%20Replication%20-%20Garming%20Sam.pdf Samba KCC: Saying No to Full Mesh Replication], Garming Sam
* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day3/Track2/Global%20Catalog%20implementation%20in%20FreeIPA%20-%20Alexander%20Bokovoy.pdf Global Catalog Service implementation in FreeIPA], Alexander Bokovoy
* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day3/Track2/Samba%20AD%20for%20the%20Enterprise%20-%20Andreas%20Schneider.pdf SAMBA AD for the Enterprise], Andreas Schneider
* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day3/Track2/Samba%20Group%20Policy%20for%20AD%20DC%20-%20David%20Mulder.odp Samba Group Policy for AD DC], David Mulder
* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/Day3/Track2/The%20Important%20Details%20Of%20Windows%20Authentication%20-%20Stefan%20Metzmacher.pdf The Important Details Of Windows Authentication], Stefan Metzmacher

===============================
complete archive
===============================

------------------------

* [https://sambaxp.org/archive_data/SambaXP2017-SLIDES/SambaXP_2017_presentations.zip complete zip-archive]

=================

2016
=================

[https://sambaxp.org/archive/ SambaXP 2016] 
------------------------

May 2016 - Berlin, Germany

* [https://sambaxp.org/archive_data/SambaXP2016-SLIDES/wed/sambaxp2016-wed-Stefan_Metzmacher-BadlockBug.pdf Badlock Bug], Stefan Metzmacher (SerNet)

* [https://sambaxp.org/archive_data/SambaXP2016-SLIDES/wed/sambaxp2016-wed-Simo_Sorce-SambaAndLinuxDistributionsLetsIntegrateBetter.pdf Samba and Linux Distributions: let’s integrate better], Simo Sorce (Red Hat)

* [https://sambaxp.org/archive_data/SambaXP2016-SLIDES/wed/sambaxp2016-wed-Sumit_Bose-WinbindAndSSSDCanTheyBeFriends/ Winbind and SSSD – can they be friends?], Sumit Bose (Red Hat)

* [https://sambaxp.org/archive_data/SambaXP2016-SLIDES/wed/sambaxp2016-wed-Michael_Adam-SMB3MultiChannelInSambaNowReally.pdf SMB3 Multi-Channel in Samba … Now Really!], Michael Adam (Red Hat)

* [https://sambaxp.org/archive_data/SambaXP2016-SLIDES/wed/sambaxp2016-wed-Ralph_Boehme-JuicingTheFruit.pdf Juicing the Fruit], Ralph Böhme (SerNet)

* [https://sambaxp.org/archive_data/SambaXP2016-SLIDES/wed/sambaxp2016-wed-Denis_Cardon-SaMBa4ADItWorksStoriesOfBattlesFoughtAndWon.pdf SaMBa4-AD, it works : stories of battles fought and won], Denis Cardon (Tranquil IT Systems)

* [https://sambaxp.org/archive_data/SambaXP2016-SLIDES/wed/sambaxp2016-wed-Alexander_Bokovoy-EnterpriseDesktopImprovingClientSideInTheAgeOfSambaADAndFreeIPA.pdf Enterprise desktop: improving client side in the age of Samba AD and FreeIPA], Alexander Bokovoy (Red Hat)

* [https://sambaxp.org/archive_data/SambaXP2016-SLIDES/thu/track1/sambaxp2016-thu-track1-Kai_Blin-ClusteredSambaInABriefcase.pdf Clustered Samba in a Briefcase], Kai Blin

* [https://sambaxp.org/archive_data/SambaXP2016-SLIDES/thu/track2/sambaxp2016-thu-track2-Jose_Rivera-IntroducingStorhaugHighAvailabilityStorageForLinux.pdf Introducing storhaug: High-Availability Storage for Linux], Jose Rivera (Red Hat)

* [https://sambaxp.org/archive_data/SambaXP2016-SLIDES/thu/track1/sambaxp2016-thu-track1-Ira_Cooper-CephalopodsAndSamba.pdf Cephalopods and Samba?], Ira Cooper (Red Hat)

* [https://sambaxp.org/archive_data/SambaXP2016-SLIDES/thu/track2/sambaxp2016-thu-track2-Alexander_Bokovoy-Andreas_Schneider-SambaAndFreeIPAAnUpdateOnActiveDirectoryIntegration.pdf An update on Samba and FreeIPA Active Directory integration], Alexander Bokovoy (Red Hat), Andreas Schneider (Red Hat)

* [https://sambaxp.org/archive_data/SambaXP2016-SLIDES/thu/track1/sambaxp2016-thu-track1-Stefan_Kania-GlusterFSAsClusterfilesystemForCTDB.pdf GlusterFS as Clusterfilesystem for CTDB], Stefan Kania

* [https://sambaxp.org/archive_data/SambaXP2016-SLIDES/thu/track2/sambaxp2016-thu-track2-Martin_Schwenke-UntanglingAndRestructuringCTDB.pdf Untangling and Restructuring CTDB], Martin Schwenke (IBM)

* [https://sambaxp.org/archive_data/SambaXP2016-SLIDES/thu/track1/sambaxp2016-thu-track1-Steve_French-AccessingSambaFromLinuxWhatsNewWhatsFasterWhatsBetter.pdf Accessing Samba from Linux. What’s new? What’s faster? What’s better?], Steve French (PrimaryData)

* [https://sambaxp.org/archive_data/SambaXP2016-SLIDES/thu/track2/sambaxp2016-thu-track2-Volker_Lendecke-LockingtdbWithoutLocks.pdf Locking.tdb without locks?], Volker Lendecke (SerNet)

* [https://sambaxp.org/archive_data/SambaXP2016-SLIDES/thu/sambaxp2016-thu-Guenther_Deschner-ClusterImprovementsInSamba4.pdf Cluster improvements in Samba 4], Günther Deschner (Red Hat)

* [https://sambaxp.org/archive_data/SambaXP2016-SLIDES/thu/sambaxp2016-thu-Amitay_Isaacs-CTDBPerformance.pdf CTDB performance], Amitay Isaacs (IBM)

* [https://sambaxp.org/archive_data/SambaXP2016-SLIDES/thu/sambaxp2016-thu-Tom_Talpey-SMB3ExtensionsForLowLatency.pdf SMB3 Extensions for Low Latency], Tom Talpey (Microsoft)

* [https://sambaxp.org/archive_data/SambaXP2016-SLIDES/thu/sambaxp2016-thu-Jeremy_Allison-SMB2AndLinuxASeamlessFileServingProtocol.pdf SMB2 and Linux – a seamless file serving protocol], Jeremy Allison (Google)

=================

2015
=================

[https://sambaxp.org/archive/ SambaXP 2015] 
------------------------

May 2015 – Göttingen, Germany

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/sambaxp2015-wed-Marc_Muehlfeld-WhatSambaNeedsToDoToAccomplishUserAndEnterpriseRequirements.pdf What Samba needs to do to accomplish user and enterprise requirements], Marc Muehlfeld

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/sambaxp2015-wed-Tom_Talpey-Microsoft-File-Sharing-Protocols-Update.pdf Microsoft File Sharing Protocols Update], Tom Talpey (Microsoft)

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/wed/track1/sambaxp2015-wed-track1-Michael_Adam-SMB3MultiChannelInSamba.pr.pdf SMB3 Multi-Channel in Samba], Michael Adam (Red Hat)

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/wed/track2/sambaxp2015-wed-track2-Amitay_Isaacs-CTDBStories.pdf CTDB Stories], Amitay Isaacs (IBM)

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/wed/track1/sambaxp2015-wed-track1-Ira_Cooper-HandlingPersistentProblemsPersistentHandlesInSamba.pdf Handling Persistent Problems: Persistent Handles in Samba], Ira Cooper (Red Hat)

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/wed/track2/sambaxp2015-wed-track2-Martin_Schwenke-CTDBWhereToFromHereAndHowCanWeGetThere.pdf CTDB: Where to from here and how can we get there?], Martin Schwenke (IBM)

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/wed/track1/sambaxp2015-wed-track1-Guenther_Deschner-ImplementingTheWitnessProtocolInSamba.pdf Implementing the Witness protocol in Samba], Günther Deschner (Red Hat)

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/wed/track2/sambaxp2015-wed-track2-Volker_Lendecke-PastPresentAndFutureOfSambaMessaging.pdf Past, present and future of Samba messaging], Volker Lendecke (SerNet)

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/wed/track1/sambaxp2015-wed-track1-David_Holder-DeployingIPv6OnlySamba4Environments.pdf Deploying IPv6-only Samba 4 Environments], David Holder (Erion Ltd)

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/wed/track2/sambaxp2015-wed-track2-Jose_Rivera-PlayingNiceWithOthersSambaHAWithPacemaker.pdf Playing nice with others: Samba HA with Pacemaker], José Rivera (Red Hat)

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/wed/track1/sambaxp2015-wed-track1-Sumit_Bose-OpeningSambaToAWiderWorldWithPlugins/#/ Opening Samba to a Wider World with plugins], Sumit Bose (Red Hat)

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/wed/track2/sambaxp2015-wed-track2-Richard_Sharpe-ClusteringSambaWithZookeeperAndCassandra.pdf Clustering Samba with Zookeeper and Cassandra], Richard Sharpe (Nutanix)

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/wed/track1/sambaxp2015-wed-track1-Noel_Power-WindowSearchProtocolAndSamba.pdf Window Search Protocol & Samba], Noel Power (SUSE)

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/wed/track2/sambaxp2015-wed-track2-Ira_Cooper-365ShadesOfGreyRelesePlanningForSamba.pdf 365 Shades of Grey – Release Planning for Samba], Ira Cooper

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/thu/track1/sambaxp2015-thu-track1-Jakub_Hrozek-UsingSambaLibrariesOutsideSamba.pdf Using Samba libraries outside Samba], Jakub Hrozek (Red Hat)

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/thu/track2/sambaxp2015-thu-track2-Torsten_Kurbad-IntroducingBebopToSamba4.pdf Introducing Bebop to Samba 4], Torsten Kurbad (Leibniz-Institut für Wissensmedien)

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/thu/track3/sambaxp2015-thu-track3-Andrew_Bartlett-UnforkingSamba4.pdf Unforking Samba4], Andrew Bartlett (Catalyst)

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/thu/track1/sambaxp2015-thu-track1-Guenther_Deschner-StatusUpdateMITKDCIntegration.pdf Status update MIT KDC integration], Günther Deschner (Red Hat)

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/thu/track2/sambaxp2015-thu-track2-Christopher_Hertel-PowerfulToysWindowsInteroperabilityToolkit.pdf Powerful Toys: Windows Interoperability Toolkit], Christopher R. Hertel

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/thu/track3/sambaxp2015-thu-track3-Jeremy_Allison-TheFutureisCloudySambaGatewaysToACloudStorageWorld.pdf The Future is Cloudy – Samba Gateways to a Cloud Storage World], Jeremy Allison (Google)

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/thu/track1/sambaxp2015-thu-track1-Alexander_Bokovoy-HandlingPOSIXAttributesForTrustedActiveDirectoryUsersAndGroupsIn.pdf Handling POSIX attributes for trusted Active Directory users and groups in FreeIPA], Alexander Bokovoy (Red Hat)

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/thu/track2/sambaxp2015-thu-track2-Steven_French-SMB3AndBeyondAccessingSambaFromLinux.pdf SMB3 and beyond, accessing Samba from Linux], Steven French (Primary Data)

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/thu/track3/sambaxp2015-thu-track3-Andreas_Schneider-HowToUseTheSambaSelftestSuite.pdf How to use the Samba Selftest Suite], Andreas Schneider (Red Hat)

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/thu/track2/sambaxp2015-thu-track2-David_Disseldorp-SMBInTheCloud.pdf SMB in the cloud], David Disseldorp (SUSE)

* [http://www.sambaxp.org/archive_data/SambaXP2015-SLIDES/thu/track3/sambaxp2015-thu-track3-Michael_Adam-ToolsForTheVagabondingSambaDeveloper.pdf Tools for the Vagabonding Samba Developer], Michael Adam (Red Hat)

[http://lca2015.linux.org.au/ Linux.conf.au 2015] 
------------------------

January 2015 – Auckland, New Zealand

* [https://ftp.samba.org/pub/samba/slides/linux.conf.au-abartlet-samba4-2015.pdf Pushing users into the pit of success - War stories from the Samba 4.0 upgrade], [http://mirror.linux.org.au/linux.conf.au/2015/OGGB_FP/Thursday/Pushing_users_into_the_pit_of_success_stories_from_the_Samba_3_Samba_4_transition.webm Video], Andrew Bartlett
* [https://ftp.samba.org/pub/samba/slides/linux.conf.au-deb-miniconf-abartlet-samba4-2015.pdf Packaging Samba for Debian], [http://mirror.linux.org.au/linux.conf.au/2015/OGGB_260098/Monday/Packaging_Samba4_for_Debian.webm Video], Andrew Bartlett
* [http://meltin.net/media/2016/06/linux.conf_.au_2015-schwenke_isaacs-methodical_makeover_for_ctdb.pdf A methodical makeover for CTDB], Martin Schwenke, Amitay Isaacs

=================

2014
=================

[http://www.snia.org/events/storage-developer SDC 2014] 
------------------------

September 2014 - Santa Clara, California, USA

* [http://www.samba.org/~obnox/presentations/sdc-2014-obnox-smb3status/ smb(3)status, Michael Adam]
* [https://www.samba.org/~metze/presentations/2014/StefanMetzmacher_sdc2014_dcerpc-handout.pdf A new DCERPC infrastructure for Samba, Stefan Metzmacher]
* [https://ftp.samba.org/pub/samba/slides/sdc-samba4-abartlet-2014.pdf Samba's AD DC: Samba 4.2 and Beyond], Andrew Bartlett

[http://archive.sambaxp.org/past-conferences/sambaxp-2014/archive.html SambaXP 2014] 
------------------------

May 2014 – Göttingen, Germany

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/wed/Neil_Martin-ImplementingMicrosoftOpenSpecifications.pdf Implementing Microsoft Open Specifications], Neil Martin (Microsoft)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/wed/track2/Richard_Sharpe-ImprovingtheSAMBAVFSforZeroSambaMods.pdf Improving the Samba VFS for zero Samba Mods], Richard Sharpe (Panzura)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/wed/track1/Ralph_Boehme-AppledancesSamba.pdf Apple dances SAMBA], Ralph Böhme (SerNet)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/wed/track2/Stefan_Metzmacher-AnewDCERPCinfrastructureforSamba.pdf A new DCERPC infrastructure for Samba], Stefan Metzmacher (SerNet)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/wed/track1/Richard_Sharpe-HowIlearnedtoloveSharingViolation.pdf How I learned to love SHARING VIOLATION], Richard Sharpe (Panzura)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/wed/track2/Andreas_Schneider-TheroadtoMITKerberossupport.pdf The road to MIT Kerberos support], Andreas Schneider (Red Hat)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/wed/track1/Jim_McDonough-Usersarecrazy_Whataretheythinking.pdf Users are crazy! What are they thinking?], Jim McDonough (SUSE)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/wed/track2/Volker_Lendecke-Scalablefilechangenotify.pdf Scalable file change notify], Volker Lendecke (SerNet)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/wed/track1/Michael_Adam-Samba__SMB3__Clustering_-TheRoadToHyper-V.pdf Samba, SMB3, Clustering - The Road To Hyper-V!?], Michael Adam (SerNet)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/wed/track2/Sumit_Bose-IDMappingofActiveDirectoryuserswithSSSD.pdf ID Mapping of Active Directory users with SSSD], Sumit Bose (Red Hat)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/wed/track1/Steven_French-SMB3-BringingHighPerformanceFileAccesstoLinux.pdf SMB3 - Bringing High Performance File Access to Linux], Steven French (IBM)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/wed/track2/Mathias_Dietz-Sambainacrossprotocolenvironment.pdf Samba in a cross protocol environment], Mathias Dietz (IBM)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/thu/track1/Andreas_Schneider-Testingyourfullsoftwarestackonasinglehostwithcwrap.pdf Testing your full software stack on a single host with cwrap], Andreas Schneider (Red Hat)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/thu/track2/Michael_Adam-Towinbindornottowinbind-thatisNOTthequestion.pdf To winbind or not to winbind - that is NOT the question!], Michael Adam (SerNet)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/thu/track1/Nadezhd_Ivanova-Samba4withOpenLDAPBackend-It_sAlive.pdf Samba4 with OpenLDAP Backend - It's Alive!], Nadezhda Ivanova (Symas)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/thu/track2/Alexander_Bokovoy_-TrustingActiveDirectorywithFreeIPA__astorybeyondSamba.pdf Trusting Active Directory with FreeIPA: a story beyond Samba], Alexander Bokovoy (Red Hat)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/thu/track1/Kai_Blin-Samba-ScalingSambaDowntoMicroServers.pdf µSamba - Scaling Samba Down to Micro Servers], Kai Blin (MPG)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/thu/track2/Amitay_Isaacs-SustainingCTDBDevelopment.pdf Sustaining CTDB Development], Amitay Isaacs (IBM)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/thu/track1/Julien_Kerihuel-OpenChange_BeyondTechnicalFulfillment.pdf OpenChange: Beyond Technical Fulfillment], Julien Kerihuel (OpenChange)

* [ftp://ftp.samba.org/pub/samba/slides/sambaxp_2014-schwenke-ctdb_scaling_ips.pdf Scaling IP address handling in CTDB], Martin Schwenke

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/thu/track2/Ingo_Meents-ExperiencesofApplyingSambainEnterpriseNASProducts.pdf Experiences of Applying Samba in Enterprise NAS Products], Ingo Meents (IBM)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/thu/track1/David_Disseldorp-Elastocloudstorage.pdf Elasto cloud storage], David Disseldorp (SUSE)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/thu/track2/Alexander_Werth-RecentimprovementsinusingNFS4ACLswithSamba.pdf Recent improvements in using NFS4 ACLs with Samba], Alexander Werth (IBM)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/thu/Simo_Sorce-openstackandsamba.pdf OpenStack and Samba], Simo Sorce (Red Hat)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2014-DATA/thu/Jeremy_Allison-synchronoussmbd-thefutureoffileserving.pdf Asynchronous smbd - the future of file serving], Jeremy Allison (Google)

[http://lca2014.linux.org.au/ Linux.conf.au 2014] 
------------------------

January 2014 – Perth, Australia

* [ftp://ftp.samba.org/pub/samba/slides/linux.conf.au_2014-isaacs_schwenke-best_ctdb_bugs_ever.pdf The best CTDB bugs ever!], [http://mirror.linux.org.au/linux.conf.au/2014/Friday/112-The_best_CTDB_bugs_ever_-_Amitay_Isaacs_and_Martin_Schwenke.mp4 Video], Amitay Isaacs & Martin Schwenke

=================

2013
=================

[http://archive.sambaxp.org/past-conferences/sambaxp-2013/archive.html SambaXP 2013] 
------------------------

May 2013 – Göttingen, Germany

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/wed/Michael_Adam_Stefan_Metzmacher-Samba4.pdf So Samba 4.0 is out - And What's Next?], Michael Adam, Stefan Metzmacher (Samba Team, SerNet)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/wed/track2/Kai_Blin-DNSing_Samba.pdf DNSing Samba - The guide to using Samba 4.0 DNS], Kai Blin (Samba Team)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/wed/track1/Tom_Talpey-SMB3_Application_Workload_Performance.pdf SMB3 Application Workload Performance], Tom Talpey (Microsoft)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/wed/track2/Fabrizio_Manfredi_Guiseppe_Guarino-SMC2.pdf Samba 4 management with SMC], Fabrizio Manfredi

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/wed/track1/Matthieu_Patou-State_of_Samba.pdf State of Samba, a year in the life of Samba], Matthieu Patou (Samba Team)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/wed/track2/Mathias_Dietz-AutoRID.pdf AutoRID module extensions to control ID generation], Mathias Dietz (IBM)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/wed/track1/Chris_Hertel_Jose_Rivera-Prequel.pdf Prequel: Distributed Caching and WAN Acceleration], Jose Rivera (RedHat), Chris Hertel (Samba Team, RedHat)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/wed/track2/Alexander_Werth-NFSv4-ACLs.pdf Using NFSv4 ACLs with Samba in a multiprotocol environment - Status and outlook], Alexander Werth (IBM)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/wed/track1/David_Disseldorp-Make-it_Snappery.pdf Make it Snappery - Snapshots with Samba and Snapper], David Disseldorp (Samba Team, SUSE Linux)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/wed/track2/Luk_Claes_Ivo_De_Decker-Fileserving_Challenges_Ghent_University.pdf File serving challenges at Ghent University], Luk Claes, Ivo De Decker (Ghent University)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/wed/track1/Neil_Martin-Advances_in_SMB2_Capture.pdf Advances in Network Capture of SMB3], Neil Martin (Microsoft)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/thu/track1/Gregor_Beck-First_Aid_Kit.pdf First Aid Kit - Some new tools to fiddle with the not so trivial database], Gregor Beck (SerNet)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/thu/track2/Julien_Kerihuel-OpenChange-Ecosystem.pdf OpenChange 2.0 Ecosystem], Julien Kerihuel (OpenChange)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/thu/track1/Amitay_Isaacs-CTDB2_and_Beyond.pdf CTDB 2.0 and Beyond], Amitay Isaacs (IBM)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/thu/track2/Arvid_Requate-Experiences_Samba4.pdf Experiences of 1.5 years of Samba 4 in the field], Arvid Requate (Univention)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/thu/track1/Christian_Ambach_Volker_Lendecke-Samba_Performance_Tuning.pdf Samba Performance Tuning], Volker Lendecke (Samba Team, SerNet), Christian Ambach (Samba Team, IBM)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/thu/track2/Samuel_Cabrero-Experiences_Integrating_Samba4_in_Zentyal.pdf Challenges and Experiences of the Samba 4 Integration in Zentyal], Samuel Cabrero (Zentyal)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/thu/track1/Ira_Cooper-DTrace_and_Samba.pdf DTrace and Samba], Ira Cooper (Samba Team, The Mathworks Inc)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/thu/track2/Michael_Adam_Bjoern_Baumbach-samba4app.pdf samba4app - Building an OpenSource Product on Samba 4], Michael Adam (Samba Team, SerNet), Börn Baumbach (SerNet)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/thu/track1/Steve_French-CIFS-SMB3-update.pdf Linux and SMB3 - Where are we now? What works? What is coming soon?], Steven French (Samba Team, IBM)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/thu/track2/Stef_Walter-Polishing_Active_Directory_Integration.pdf Polished AD Integration], Stef Walter (RedHat)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/thu/track1/Jeff_Layton-Linux-CIFS-Client.pdf Getting the Most out of the Linux CIFS Client], Jeff Layton (Samba Team)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/thu/track2/Alexander_Bokovoy_Simo_Sorce-Samba-4-Fedora.pdf Samba 4 in Fedora], Alexander Bokovoy, Simo Sorce (Samba Team, RedHat)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/thu/track1/Matthieu_Patou-Smaller_Faster_Scalier.pdf Smaller, Faster, Scalier], Matthieu Patou (Samba Team)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/thu/track2/Chris_Hertel-Samba_on_Gluster.pdf Samba on Gluster: Issues and Challenges], Chris Hertel (Samba Team, RedHat), Jose Rivera (RedHat)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/thu/track1/Jeremy_Allison-Samba-Development.pdf The Samba Engineering Process], Jeremy Allison (Samba Team, Google)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2013-DATA/thu/track2/Guenther_Deschner_Andreas_Schneider-Printing_Samba_4.pdf Printing in Samba 4.0], Günther Deschner, Andreas Schneider (Samba Team, RedHat)

[http://lca2013.linux.org.au/ Linux.conf.au 2013] 
------------------------

January 2013 – Canberra, Australia

* Samba 4.0 [http://mirror.linux.org.au/linux.conf.au/2013/ogv/Samba_4.0.ogv Video], Andrew Bartlett
* [https://ftp.samba.org/pub/samba/slides/linux.conf.au-2013-abartlet-ci-miniconf-samba.pdf Two years with Samba's autobuild] [http://mirror.linux.org.au/linux.conf.au/2013/ogv/Two_years_with_Sambas_autobuild.ogv Video], [https://www.youtube.com/watch?v=EZIUxFgneoo YouTube], Andrew Bartlett

=================

2012
=================

[http://archive.sambaxp.org/past-conferences/sambaxp-2012/archive.html SambaXP 2012] 
------------------------

May 2012 – Göttingen, Germany

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2012-DATA/wed/John_Terpstra_20_Years_Samba.pdf Keynote "20 years of Samba"], John Terpstra (Dell, Samba Team)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2012-DATA/wed/Bradley_Kuhn-GPL-compliance.pdf GPL Compliance Is Easier Than You Think], Bradley Kuhn (Software Freedom Conservancy)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2012-DATA/wed/Thomas_Pfenning-SMB23.pdf Microsoft Storage Directions and SMB2 futures], Thomas Pfenning (Microsoft)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2012-DATA/wed/Tom_Talpey_SMB3.0.pdf SMB2.2 Server Application Deployment End-to-End], Tom Talpey (Microsoft)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2012-DATA/wed/Michael_Adam_Stefan_Metzmacher_Durable_File_Handles.pdf SMB2 in Samba: Durable Handles And Beyond], Michael Adam (SerNet, Samba Team), Stefan Metzmacher (SerNet, Samba Team)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2012-DATA/wed/Julien_Kerihuel_Openchange.pdf Unleashing the Borgs], Julien Kerihuel (Openchange)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2012-DATA/thu/track1/Christopher_Hertel-Implementing-PeerDist.pdf Implementing PeerDist -- The BranchCache[TM] Protocol], Christopher R. Hertel (Red Hat, Samba Team)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2012-DATA/thu/track2/David_Disseldorp_Samba-and-btrfs.pdf Samba and Btrfs - A Snapshot of Progress], David Disseldorp (SUSE, Samba Team)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2012-DATA/thu/track1/Christian_Ambach-Integrating-Samba-and-GFS.pdf Integrating GPFS and Samba - Status and outlook], Christian Ambach (IBM, Samba Team)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2012-DATA/thu/track2/Matthieu_Patou-Windows-Update-Management.pdf Windows update management using Samba], Matthieu Patou (Samba Team)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2012-DATA/thu/track1/Ira_Cooper-Mathworks.pdf A Case Study: Unique NAS Issues and Solutions at The MathWorks], Ira Cooper (The MathWorks Inc., Samba Team)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2012-DATA/thu/track2/Steve_French-SMB3.pdf The Future of File Protocols: cifs, smb2, smb3 meets Linux], Steve French (IBM / Samba Team)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2012-DATA/thu/track1/Fabrizio_Manfredi-RestFS.pdf RestFS and Samba], Fabrizio Manfredi

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2012-DATA/thu/track2/Richard-Sharpe-Developing-Samba-VFS-Modules.pdf Developing Samba VFS Modules], Richard Sharpe

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2012-DATA/thu/track1/Jelmer_Vernooij-samba4-cloud.pdf Samba 4 Cloud Deployment], Jelmer Vernooij (Canonical, Samba Team)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2012-DATA/thu/track1/Alexander_Bokovoy_and_Andreas_Schneider-FreeIPA-CrossForrest_Trusts.pdf FreeIPA Cross Forest Trusts], Alexander Bokovoy (Red Hat, Samba Team), Andreas Schneider (Red Hat, Samba Team)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2012-DATA/thu/track1/Jeremy_Allison-The-Evolution-of-IO.pdf The evolution of Asynchronous IO (aio) in Samba)], Jeremy Allison (Google, Samba Team)

[http://lca2012.linux.org.au/ Linux.conf.au 2012] 
------------------------

January 2012 – Ballarat, Australia

* [https://ftp.samba.org/pub/samba/slides/linux.conf.au_2012-schwenke_sahlberg-testing_ctdb.pdf Testing CTDB – not necessarily trivial!], Martin Schwenke & Ronnie Sahlberg
* [https://ftp.samba.org/pub/samba/slides/linux.conf.au-2012-abartlet-amitay-scripting.pdf The Samba Tour of Scripting Languages] [http://mirror.linux.org.au/pub/linux.conf.au/2012/samba4_after_merge_ready_real_world.ogv Video], Andrew Bartlett and Amitay Isaacs
* High Availability Login Services - Samba4 Active Directory [http://mirror.linux.org.au/pub/linux.conf.au/2012/high_availability_login_services_samba4_active_directory.ogv Video] Kai Blin
* After the Merge, Ready for the Real World [http://mirror.linux.org.au/pub/linux.conf.au/2012/samba4_after_merge_ready_real_world.ogv Video] Andrew Bartlett, Andrew Tridgell, Kai Blin and Amitay Isaacs

=================

2011
=================

[http://archive.sambaxp.org/past-conferences/sambaxp-2011/archive.html SambaXP 2011] 
------------------------

May 2011 – Göttingen, Germany

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2011-DATA/day1/samba4-status.pdf Samba 4 status report], Andrew Tridgell (Samba Team, IBM)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2011-DATA/day1/snia2010-death-of-filesharing.pdf The Death of File Protocols. This time I really mean it!], Jeremy Allison (Samba Team, Google)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2011-DATA/day1/DevelopersViewOfMSDocumentation.pdf Microsoft SMB documentation - a developer's view], Tom Talpey (Microsoft)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2011-DATA/day1/sambaxp-smf-2011-final.pdf Linux Kernel Clients - Reviewing A Great Year for Accessing Samba servers], Steven French (Samba Team, IBM)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2011-DATA/day1/sambaxp-2011-talk-idmap-presentation.pdf Finally Curing The Identity Crisis?], Michael Adam (Samba Team, SerNet)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2011-DATA/day2track1/SambaXP%202011%20Ambach%20Dietz.pdf Clustered Samba performance analysis and improvement], Christian Ambach (IBM)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2011-DATA/day2track1/EnterpriseSamba-Akelbein-Meents-2000511.pdf Using Samba in Enterprise Products], Jens-Peter Akelbein (IBM)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2011-DATA/day2track1/Cloud_Storage.pdf Link Samba to Cloud Storage and Samba Management Console], Fabrizio Manfredi (Zeropiu)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2011-DATA/day2track1/richacls_xp2011_final.pdf Migrating Access Control to Samba], David Disseldorp (Novell)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2011-DATA/day2track1/dcerpc.pdf DCERPC and Endpoint Mapper], Andreas Schneider (Samba Team, Red Hat)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2011-DATA/day2track1/smbta.pdf What's new on SMB Traffic Analyzer], Holger Hetterich, Benjamin Brunner (Novell)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2011-DATA/day2track1/samba4-merge.pdf The road to a unified Samba 4.0], Andrew Bartlett (Samba Team)

=================

2010
=================

[http://archive.sambaxp.org/past-conferences/sambaxp-2010/archive.html SambaXP 2010] 
------------------------

May 2010 – Göttingen, Germany

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/Michael_Adam_Tutorial.pdf Clustered CIFS-Server with Samba and CTDB Part I], Michael Adam (Samba Team, SerNet)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/Andrew_Tridgell.pdf DRS Replication in Samba], Andrew Tridgell (Samba Team, IBM)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/Jeremy_Allison.pdf How to make a product with Samba], Jeremy Allison (Samba Team, Google)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/Simo_Sorce.pdf Directory Services strategies for interoperability], Simo Sorce (Samba Team, RedHat)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/Tom_Talpey.pdf SMB2 Unix Extensions Protocol Initiative], Tom Talpey (Microsoft)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/Chris_Hertel_Jose_Rivera.pdf Are We There Yet? The long, winding road to SMB/CIFS specifications], Chris Hertel (Samba Team, ubiqx), José Rivera (ubiqx)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/Michael_Adam.pdf Lessons Learned in Clustered Samba], Michael Adam (Samba Team, SerNet)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/Jim_McDonough.pdf Improving the AD/Linux Desktop Experience], Jim McDonough (Samba Team, Novell)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/Rolf_Schmidt.pdf Clustered Samba in SLES11], Rolf Schmidt (Novell)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/Julien_Kerihuel.pdf OpenChange: ambitious ace of spades], Julien Kerihuel (OpenChange)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/Mathias_Dietz.pdf Centralized configuration management using registry tdb in a ctdb cluster], Mathias Dietz (IBM), Christian Ambach (IBM)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/Steven_Danneman.pdf A Comparison between the Samba 3 and Likewise Lwiod SMB File Servers], Steven Danneman (Samba Team, Isilon)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/John_Terpstra.pdf Samba-CTDB Clustering Experiences], John Terpstra (Samba Team, Primastasys)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/John_Terpstra.pdf Samba-CTDB Clustering Experiences], John Terpstra (Samba Team, Primastasys)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/Jeff_Layton.pdf Multi-session Linux CIFS], Jeff Layton (Samba Team, RedHat)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/Olaf_Flebbe.pdf Avoiding Disk Fragmentation], Dr. Olaf Flebbe (science + computing)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/Guenther_Deschner.pdf Samba and Printing], Günther Deschner (Samba Team, RedHat)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/Steve_Langasek.pdf Keynote:Samba's role in the Linux desktop], Steve Langasek (Canonical)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/Fabrizio_Manfredi.pdf Samba Management Console (SMC)], Fabrizio Manfredi

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/Kai_Blin.pdf Active Directory For Your Breast Pocket], Kai Blin (Samba Team)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/Andreas_Schneider.pdf Documenting the source], Andreas Schneider (Samba Team, RedHat)

* [http://archive.sambaxp.org/fileadmin/user_upload/SambaXP2010-DATA/Jelmer_Vernooij.pdf Samba 4 Status Update], Jelmer Vernooij (Samba Team, Canonical)