Release Planning for Samba 4.7
    <namespace>0</namespace>
<last_edited>2019-09-17T09:12:01Z</last_edited>
<last_editor>Kseeger</last_editor>

Samba 4.7 has been marked `Samba_Release_Planning#Discontinued_.28End_of_Life.29|**discontinued**`.

`Blocker bugs|Release blocking bugs`
------------------------

* [https://bugzilla.samba.org/buglist.cgi?bug_severity=regression&query_format=advanced&target_milestone=4.7 All 4.7 regression bugs]
* [https://bugzilla.samba.org/buglist.cgi?bug_severity=regression&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=VERIFIED&query_format=advanced&target_milestone=4.7 Unresolved 4.7 regression bugs]

Samba 4.7.12 
------------------------

<small>(**Updated 27-November-2018**)</small>

* Tuesday, November 27 2018 - **Samba 4.7.12** has been released as a **Security Release**.
    https://www.samba.org/samba/history/samba-4.7.12.html Release Notes Samba 4.7.12]

Samba 4.7.11 
------------------------

<small>(**Updated 23-October-2018**)</small>

* Tuesday, October 23 2018 - **Samba 4.7.11** has been released. This is the **last bugfix release** of the 4.7 release series!
    https://www.samba.org/samba/history/samba-4.7.11.html Release Notes Samba 4.7.11]

Samba 4.7.10 
------------------------

<small>(**Updated 27-August-2018**)</small>

* Monday, August 27 2018 - **Samba 4.7.10** has been released.
    https://www.samba.org/samba/history/samba-4.7.10.html Release Notes Samba 4.7.10]

Samba 4.7.9 
------------------------

<small>(**Updated 14-August-2018**)</small>

* Tuesday, August 14 2018 - **Samba 4.7.9** has been released as a **Security Release** to address the following defects:
** [https://www.samba.org/samba/security/CVE-2018-10858.html CVE-2018-10858] (Insufficient input validation on client directory listing in libsmbclient.)
** [https://www.samba.org/samba/security/CVE-2018-10918.html CVE-2018-10918] (Denial of Service Attack on AD DC DRSUAPI server.)
** [https://www.samba.org/samba/security/CVE-2018-10919.html CVE-2018-10919] (Confidential attribute disclosure from the AD LDAP server.)
** [https://www.samba.org/samba/security/CVE-2018-1139.html CVE-2018-1139] (Weak authentication protocol allowed.)

    https://www.samba.org/samba/history/samba-4.7.9.html Release Notes Samba 4.7.9]

Samba 4.7.8 
------------------------

<small>(**Updated 21-June-2018**)</small>

* Thursday, June 21 2018 - **Samba 4.7.8** has been released
    https://www.samba.org/samba/history/samba-4.7.8.html Release Notes Samba 4.7.8]

Samba 4.7.7 
------------------------

<small>(**Updated 17-April-2018**)</small>

* Tuesday, April 17 2018 - **Samba 4.7.7** has been released
    https://www.samba.org/samba/history/samba-4.7.7.html Release Notes Samba 4.7.7]

Samba 4.7.6 
------------------------

<small>(**Updated 13-March-2018**)</small>

* Tuesday, March 13 2018 - **Samba 4.7.6** has been released as a **security release** in order to address [https://www.samba.org/samba/security/CVE-2018-1050.html CVE-2018-1050] (Denial of Service Attack on external print server) and [https://www.samba.org/samba/security/CVE-2018-1057.html CVE-2018-1057] (Authenticated users can change other users' password).
    https://www.samba.org/samba/history/samba-4.7.6.html Release Notes Samba 4.7.6]

Samba 4.7.5 
------------------------

<small>(**Updated 07-February-2018**)</small>

* Wednesday, February 07 2018 - **Samba 4.7.5** has been released.
    https://www.samba.org/samba/history/samba-4.7.5.html Release Notes Samba 4.7.5]

Samba 4.7.4 
------------------------

<small>(**Updated 22-December-2017**)</small>

* Friday, December 22 2017 - **Samba 4.7.4** has been released
    https://www.samba.org/samba/history/samba-4.7.4.html Release Notes Samba 4.7.4]

Samba 4.7.3 
------------------------

<small>(**Updated 21-November-2017**)</small>

* Tuesday, November 21 2017 - **Samba 4.7.3** has been released as a **security release** in order to address [https://www.samba.org/samba/security/CVE-2017-14746.html CVE-2017-14746] (Use-after-free vulnerability) and [https://www.samba.org/samba/security/CVE-2017-15275.html CVE-2017-15275] (Server heap memory information leak).
    https://www.samba.org/samba/history/samba-4.7.3.html Release Notes Samba 4.7.3]

Samba 4.7.2 
------------------------

<small>(**Updated 15-November-2017**)</small>

* Wednesday, November 15 2017 - **Samba 4.7.2** has been released to address a [https://bugzilla.samba.org/show_bug.cgi?id=13130 data corruption bug].
    https://www.samba.org/samba/history/samba-4.7.2.html Release Notes Samba 4.7.2]

Samba 4.7.1 
------------------------

<small>(**Updated 02-November-2017**)</small>

* Thursday, November 2 2017 - **Samba 4.7.1** has been released.
    https://www.samba.org/samba/history/samba-4.7.1.html Release Notes Samba 4.7.1]

Samba 4.7.0 
------------------------

<small>(**Updated 21-September-2017**)</small>

* Thursday, September 21 2017 - **Samba 4.7.0** has been released.
    https://www.samba.org/samba/history/samba-4.7.0.html Release Notes Samba 4.7.0]

Samba 4.7.0rc6 
------------------------

<small>(**Updated 17-September-2017**)</small>

* Sunday, September 17 2017 - **Samba 4.7.0rc6** has been released.
    https://download.samba.org/pub/samba/rc/samba-4.7.0rc6.WHATSNEW.txt Release Notes Samba 4.7.0rc6]

Samba 4.7.0rc5 
------------------------

<small>(**Updated 29-August-2017**)</small>

* Tuesday, August 29 2017 - **Samba 4.7.0rc5** has been released.
    https://download.samba.org/pub/samba/rc/samba-4.7.0rc5.WHATSNEW.txt Release Notes Samba 4.7.0rc5]

Samba 4.7.0rc4 
------------------------

<small>(**Updated 15-August-2017**)</small>

* Tuesday, August 15 2017 - **Samba 4.7.0rc4** has been released.
    https://download.samba.org/pub/samba/rc/samba-4.7.0rc4.WHATSNEW.txt Release Notes Samba 4.7.0rc4]

Samba 4.7.0rc3 
------------------------

<small>(**Updated 25-July-2017**)</small>

* Tuesday, July 25 2017 - **Samba 4.7.0rc3** has been released.
    https://download.samba.org/pub/samba/rc/samba-4.7.0rc3.WHATSNEW.txt Release Notes Samba 4.7.0rc3]

Samba 4.7.0rc2 
------------------------

<small>(**Updated 13-July-2017**)</small>

* Wednesday, July 12 2017 - **Samba 4.7.0rc2** has been released a **Security Release** in order to address [https://www.samba.org/samba/security/CVE-2017-11103.html CVE-2017-11103] (Orpheus' Lyre mutual authentication validation bypass).
    https://download.samba.org/pub/samba/rc/samba-4.7.0rc2.WHATSNEW.txt Release Notes Samba 4.7.0rc2]

Samba 4.7.0rc1 
------------------------

<small>(**Updated 13-July-2017**)</small>

* Tuesday, July 4 2017 - **Samba 4.7.0rc1** has been released
    https://download.samba.org/pub/samba/rc/samba-4.7.0rc1.WHATSNEW.txt Release Notes Samba 4.7.0rc1]