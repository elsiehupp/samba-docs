Release Planning for Samba 4.2
    <namespace>0</namespace>
<last_edited>2016-09-16T09:42:32Z</last_edited>
<last_editor>Kseeger</last_editor>

Samba 4.2 has been marked `Samba_Release_Planning#Discontinued_.28End_of_Life.29|**discontinued**`.

Samba 4.2.14 
------------------------

<small>(**Updated 07-July-2016**)</small>

* Thursday, July 7 - **Samba 4.2.14** has been released as a **Security Release** in order to address [https://www.samba.org/samba/security/CVE-2016-2119.html CVE-2016-2119] (Client side SMB2/3 required signing can be downgraded).
    https://www.samba.org/samba/history/samba-4.2.14.html Release Notes Samba 4.2.14]

Samba 4.2.13 
------------------------

<small>(**Updated 17-June-2016**)</small>

* Friday, June 17 - **Samba 4.2.13** has been released (although 4.2 is in the security fixes only mode, we provide another maintenance release, because some important bug fixes were available).
    https://www.samba.org/samba/history/samba-4.2.13.html Release Notes Samba 4.2.13]

Samba 4.2.12 
------------------------

<small>(**Updated 02-May-2016**)</small>

* Monday, May 02 - Samba 4.2.12 has been released
    https://www.samba.org/samba/history/samba-4.2.12.html Release Notes Samba 4.2.12]

Samba 4.2.11 
------------------------

<small>(**Updated 12-April-2016**)</small>

* Tuesday, April 12 - Samba 4.2.11 has been released as a **Security Release**
    https://www.samba.org/samba/history/samba-4.2.11.html Release Notes Samba 4.2.11]

Samba 4.2.10 
------------------------

<small>(**Updated 12-April-2016**)</small>

* Tuesday, April 12 - Samba 4.2.10 has been releases as a **Security Release**
    https://www.samba.org/samba/history/samba-4.2.10.html Release Notes Samba 4.2.10]

Samba 4.2.9 
------------------------

<small>(**Updated 08-March-2016**)</small>

* Tuesday, March 08 - Samba 4.2.9 has been released as a **Security Release**
    https://www.samba.org/samba/history/samba-4.2.9.html Release Notes Samba 4.2.9]

Samba 4.2.8 
------------------------

<small>(**Updated 02-February-2016**)</small>

* Tuesday, February 2 - Samba 4.2.8 has been released
    https://www.samba.org/samba/history/samba-4.2.8.html Release Notes Samba 4.2.8]

Samba 4.2.7 
------------------------

<small>(**Updated 16-December-2015**)</small>

* Wednesday, December 16 - Samba 4.2.7 has been released as a Security Release
    https://www.samba.org/samba/history/samba-4.2.7.html Release Notes Samba 4.2.7]

Samba 4.2.6 
------------------------

<small>(**Updated 08-December-2015**)</small>

* Tuesday, December 8 - Samba 4.2.6 has been released
    https://www.samba.org/samba/history/samba-4.2.6.html Release Notes Samba 4.2.6]

Samba 4.2.5 
------------------------

<small>(**Updated 27-October-2015**)</small>

* Tuesday, October 27 - Samba 4.2.5 has been released
    https://www.samba.org/samba/history/samba-4.2.5.html Release Notes Samba 4.2.5]

Samba 4.2.4 
------------------------

<small>(**Updated 08-September-2015**)</small>

* Tuesday, September 8 - Samba 4.2.4 has been released
    https://www.samba.org/samba/history/samba-4.2.4.html Release Notes Samba 4.2.4]

Samba 4.2.3 
------------------------

<small>(**Updated 14-July-2015**)</small>

* Tuesday, July 14 - Samba 4.2.3 has been released
    https://www.samba.org/samba/history/samba-4.2.3.html Release Notes Samba 4.2.3]

Samba 4.2.2 
------------------------

<small>(**Updated 27-May-2015**)</small>

* Wednesday, May 27 - Samba 4.2.2 has been released
    https://www.samba.org/samba/history/samba-4.2.2.html Release Notes Samba 4.2.2]

Samba 4.2.1 
------------------------

<small>(**Updated 15-April-2015**)</small>

* Wednesday, April 15 - Samba 4.2.1 has been released
    https://www.samba.org/samba/history/samba-4.2.1.html Release Notes Samba 4.2.1]

Samba 4.2.0 
------------------------

<small>(**Updated 04-March-2015**)</small>

* Wednesday, March 04 - Samba 4.2.0 has been released
    https://www.samba.org/samba/history/samba-4.2.0.html Release Notes Samba 4.2.0]

Samba 4.2.0rc5 
------------------------

<small>(**Updated 24-February-2015**)</small>

* Tuesday, February 24 - Samba 4.2.0rc5 has been released
    https://download.samba.org/pub/samba/rc/WHATSNEW-4.2.0rc5.txt Release Notes Samba 4.2.0rc5]

Samba 4.2.0rc4 
------------------------

<small>(**Updated 16-January-2015**)</small>

* Friday, January 17 - Samba 4.2.0rc4 has been released
    https://download.samba.org/pub/samba/rc/WHATSNEW-4.2.0rc4.txt Release Notes Samba 4.2.0rc4]

Samba 4.2.0rc3 
------------------------

<small>(**Updated 20-December-2014**)</small>

* Saturday, December 20 - Samba 4.2.0rc3 has been released
    https://download.samba.org/pub/samba/rc/WHATSNEW-4.2.0rc3.txt Release Notes Samba 4.2.0rc3]

Samba 4.2.0rc2 
------------------------

<small>(**Updated 15-October-2014**)</small>

* Wednesday, October 15 - Samba 4.2.0rc2 has been released
    https://download.samba.org/pub/samba/rc/WHATSNEW-4.2.0rc2.txt Release Notes Samba 4.2.0rc2]

Samba 4.2.0rc1 
------------------------

<small>(**Updated 01-October-2014**)</small>

* Wednesday, October 1 - Samba 4.2.0rc1 has been released
    https://download.samba.org/pub/samba/rc/WHATSNEW-4.2.0rc1.txt Release Notes Samba 4.2.0rc1]