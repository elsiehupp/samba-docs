UpgradeprovisionPlans
    <namespace>0</namespace>
<last_edited>2010-07-11T14:18:15Z</last_edited>
<last_editor>Ekacnet</last_editor>

Futur plans for upgradeprovision 
------------------------

Close futur
------------------------

* Handle updates on provision with user amended schema (patch available already at `http://git.samba.org/?p=mat/samba.git;a=shortlog;h=refs/heads/upgradeprovision-misc| my upgradeprovison-misc branch`)
* Handle renaming of default GPO to correct GUID
* Handle RID stuff in a better way

A bit less closer
------------------------

* Allow migration from Openldap backend to ldb backend

Even more away
------------------------

* Handle migration of samba3 provision to samba4
* Allow migration from ldb backend to Openldap backen