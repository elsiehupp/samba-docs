SoC/2014/BuildFarm
    <namespace>0</namespace>
<last_edited>2014-05-25T09:21:59Z</last_edited>
<last_editor>Krishnatejaperannagari</last_editor>

Improving build farm look and speed: 
------------------------

The project to improve build farm look and speed is accepted this year for Google summer of code. Mentors **Jelmer Vernooij** and **Kai Blin** will be assisting the summer of code intern. The following are some of the main ideas and deliverables of this project:

1. To replace the existing build farm style with new style that is used in the main website

2. Adding new features like:
   a. Auto adjust to screen size

   b. A better search feature to view builds,hosts and check-ins by listing only the valid combinations

   c. A page that displays a only the failed builds. the cause of the failure and other information to help the developers in determining the problem with the hosts and identify bugs.

3. Improve page loading speed by:
   a. Building a separate daemon to process the build logs as they come

   b. Use of Ajax to display the changes effectively and as new content is available 

4. Reduce email alerts and sending mail with correct cause of failure

5. Make test errors quickly accessible by modifying the structure of the log and page display