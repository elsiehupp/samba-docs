Test Infrastructure Sprint
    <namespace>0</namespace>
<last_edited>2014-10-25T21:53:02Z</last_edited>
<last_editor>JelmerVernooij</last_editor>

* drop the samba3/samba4 distinction?
* randomize test order [mat]
* statistics!
** measure total run time and per test run time
** prevent slow test addition
** record env startup time [jelmer]
* make testsuite output top X [mat]
* reduce the cardinality of RPC tests
* parallelize with testr [jelmer]
** migrate to subunit2
** make all tests support --list
** <s>require all tests output subunit</s>
** reduce selftest run overhead
*** <s>make symbol deduplication a test, not a build phase</s>
* documentation
* simplification of test run infrastructure