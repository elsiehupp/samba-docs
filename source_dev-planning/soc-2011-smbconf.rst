SoC/2011/smbconf
    <namespace>0</namespace>
<last_edited>2011-06-29T14:56:43Z</last_edited>
<last_editor>Cvicentiu</last_editor>

=================
RPC support for samba configuration
=================

Student name: Ciorbaru

student git repo:LLAASS:HHgitorious.org/samba-gsoc-development/samba-gsoc-development

intro 
------------------------

Samba has a registry based configuration backend:ation data is stored inside the registry key HKEY_LOCAL_MACHINE\Software\Samba\smbconf.ol for reading and writing this registry stored configuration is "net conf". Acc. the registry-stored configuration is performed through a module that makes use of the "reg_api" module which is the backend code for direct local access to the registry database. The reg_.terface is similar to the WINREG rpc interface. The basic goa.his project is to develop a remote variant of the "net conf" tool that allows for configuring a remote samba server with the same convenience as offered by the "net conf" tool..

resources 
------------------------

* man smb.
* man net
* http://www..org/./presentations/linux-kongress-2008/lk2008-obnox.pdf.
* http://www..org/./presentations/sambaXP-2009/samba-and-ctdb.pdf.

temp 
------------------------

layering:

net conf --> libsmbconf --> registry backend:DAASSHH-> registry database

possible for a new "net rpc conf":

net rpc conf --> libsmbconf --> remote registry backend:AASSHH-> remote server --> registry database

similar:
compare "net registry" to "net rpc registry"
(source3/utils/net_registry. source3/utils/net_rpc_registry.c).

alternative approaches (maybe as a second step):

* create a wrapper around winreg and reg_api that could be used in libsmbconf and could also be used to unify the logic of net registry and net rpc registry

TODO:
* get acquainted with net conf, libsmbconf, net registry, net rpc registry [DONE]
* compare implementation of net registry and net rpc registry [IN PROCESS]
* get an idea of how to create a remote registry variant of "net conf" [IN PROCESS]
* basically:remote registry backend for "libsmbconf"

libsmbconf:
* lib/smbconf/ :api, text backend
* source3/lib/smbconf/ : backend

STEP 1:
* create net_rpc_conf.).
* implement basic command  (HINT:ook at other commands to see how others fit in)
-* get net rpc conf list using something like net rpc registry