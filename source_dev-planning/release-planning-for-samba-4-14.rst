Release Planning for Samba 4.14
    <namespace>0</namespace>
<last_edited>2021-04-29T08:49:00Z</last_edited>
<last_editor>Kseeger</last_editor>

Samba 4.14 is the `Samba_Release_Planning#Current_Stable_Release|**current stable release series**`.

`Blocker bugs|Release blocking bugs`
------------------------

* [https://bugzilla.samba.org/buglist.cgi?bug_severity=regression&query_format=advanced&target_milestone=4.14 All 4.14 regression bugs]
* [https://bugzilla.samba.org/buglist.cgi?bug_severity=regression&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=VERIFIED&query_format=advanced&target_milestone=4.14 Unresolved 4.14 regression bugs]

Samba 4.14.5 
------------------------

<small>(**Updated 29-April-2021**)</small>

* Tuesday, June 1 2021 - Planned release date for **Samba 4.14.4**

Samba 4.14.4 
------------------------

<small>(**Updated 29-April-2021**)</small>

* Thursday, April 29 2021 - **Samba 4.14.4** has been released as a security release to address the following defect:
** [https://www.samba.org/samba/security/CVE-2021-20254.html CVE-2021-20254] (Negative idmap cache entries can cause incorrect group entries in the Samba file server process token).
 [https://www.samba.org/samba/history/samba-4.14.4.html Release Notes Samba 4.14.4]

Samba 4.14.3 
------------------------

<small>(**Updated 20-April-2021**)</small>

* Tuesday, April 20 2021 - **Samba 4.14.3** has been released.
    https://www.samba.org/samba/history/samba-4.14.3.html Release Notes Samba 4.14.3]

Samba 4.14.2 
------------------------

<small>(**Updated 24-March-2021**)</small>

* Wednesday, March 24 2021 - **Samba 4.14.2** has been released as a security release
    https://www.samba.org/samba/history/samba-4.14.2.html Release Notes Samba 4.14.2]

Samba 4.14.1 
------------------------

<small>(**Updated 24-March-2021**)</small>

* Wednesday, March 24 2021 - **Samba 4.14.1** has been released as a security release
    https://www.samba.org/samba/history/samba-4.14.1.html Release Notes Samba 4.14.1]

Samba 4.14.0 
------------------------

<small>(**Updated 09-March-2021**)</small>

* Tuesday, March 09 2021 - [https://download.samba.org/pub/samba/stable/samba-4.14.0.tar.gz Samba 4.14.0] has been released
    https://www.samba.org/samba/history/samba-4.14.0.html Release Notes Samba 4.14.0]

Samba 4.14.0rc4 
------------------------

<small>(**Updated 01-March-2021**)</small>

* Monday, March 1 2021 - [https://download.samba.org/pub/samba/rc/samba-4.14.0rc4.tar.gz Samba 4.14.0rc4] has been released.
   https://download.samba.org/pub/samba/rc/samba-4.14.0rc4.WHATSNEW.txt

Samba 4.14.0rc3 
------------------------

<small>(**Updated 18-February-2021**)</small>

* Thursday, February 18 2021 - [https://download.samba.org/pub/samba/rc/samba-4.14.0rc3.tar.gz Samba 4.14.0rc3] has been released.
   https://download.samba.org/pub/samba/rc/samba-4.14.0rc3.WHATSNEW.txt

Samba 4.14.0rc2 
------------------------

<small>(**Updated 04-February-2021**)</small>

* Thursday, February 04 2021 - [https://download.samba.org/pub/samba/rc/samba-4.14.0rc2.tar.gz Samba 4.14.0rc2] has been released
   https://download.samba.org/pub/samba/rc/samba-4.14.0rc2.WHATSNEW.txt

Samba 4.14.0rc1 
------------------------

<small>(**Updated 21-January-2021**)</small>

* Thursday, January 21 2021 - [https://download.samba.org/pub/samba/rc/samba-4.14.0rc1.tar.gz Samba 4.14.0rc1] has been released.
    ttps://download.samba.org/pub/samba/rc/samba-4.14.0rc1.WHATSNEW.txt