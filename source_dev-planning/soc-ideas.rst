SoC/Ideas
    <namespace>0</namespace>
<last_edited>2021-04-19T09:54:26Z</last_edited>
<last_editor>Aaptel</last_editor>

=================
Applying to Samba = 

See our main `SoC|Google Summer of Code @ Samba` page for **important details**, `SoC/Applying|plus these extended notes on applying` to Samba.

=================
Google Summer of Code: Suggested Project ideas
=================

The following are Samba project ideas for Summer of Code.
Of course you are free to come up with Samba related ideas not listed here.
Please discuss your planned project by either sending an email to [https://lists.samba.org/mailman/listinfo/samba-technical samba-technical@lists.samba.org] or joining us on irc://irc.freenode.net/#samba-technical.

Samba
------------------------

Some additional possible GSoC topics can be found in Bugzilla in the form of bugs which are marked as "Feature request": [https://bugzilla.samba.org/buglist.cgi?query_format=advanced&short_desc=Feature%20request&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&short_desc_type=allwordssubstr&product=Samba%204.0 here]. Questions regarding complexity and requirements should be directed to the technical mailing list.

<!-- Commented out possibly stale proposals

===============================
Browsing support in Samba 4
===============================

------------------------

Samba 4 still needs support for mailslots in general and in particular for the BROWSE mailslot. Should come with tests. Documentation of the BROWSER protocol is available here:
http://msdn.microsoft.com/en-us/library/cc201609(PROT.10).aspx
http://ubiqx.org/cifs/Browsing.html

*Difficulty: Hard
*Language(s): C
*Possible mentors: Stefan Metzmacher

===============================
Implement login / logout related counter update
===============================

------------------------

For the moment the attributes related to login and logout are not 
updated by Samba4.
The goal of this project is to understand in which case windows update 
the counters (ie. most probably during interactive logon but also maybe 
with some netlogon calls ?) and to implement counter and timestamp 
update is Samba code so that this information can be available.
This project of course includes the development of unit tests.

*Difficulty: Easy
*Language(s): C
*Possible mentors: Andrew Bartlett

===============================
Improve regedit
===============================

------------------------

	 
Last year someone has started to write a ncurses based registry editor. The editor could be improved, like put some effort in a better look and feel and adding dcerpc winreg support to remotely connect to the registry.

See https://git.samba.org/?p=asn/samba.git;a=shortlog;h=refs/heads/regedit

* Difficulty: Medium
* Language(s): C
* Possible mentors: Andreas Schneider, Michael Adam

===============================
Utilize libsmbclient server-side copy support in file managers
===============================

------------------------

With libsmbclient now supporting server-side copy requests via [https://git.samba.org/?p=samba.git;a=commit;h=f73bcf4934be89f83e86459bc695b7d28348565c cli_splice()], file managers making use of libsmbclient can be changed to utilize server-side copy support for greatly improved remote copy performance. Potential file manager targets include [https://bugzilla.gnome.org/show_bug.cgi?id=771022 GNOME Files/Nautilus (gvfs_smb)], Dolphin (kio_smb) and Kodi's File Manager.

*Difficulty: Easy, Medium
*Language(s): C, C++
*Possible mentors: David Disseldorp

===============================
Windows Search Protocol WSP client library and torture tests
===============================

------------------------

The Windows Search Protocol WSP is used to implement remote full filesystem indexing (indexed search) between Windows machines. We would like to support this functionality in Samba, interfacing with existing indexing tools on Unix systems (such as GNOME Tracker).

This is a DCE/RPC protocol. See http://msdn.microsoft.com/en-us/library/cc251767.aspx .

The student should write a (un)marshalling library to push and pull PDUs and an asynchronous client library on top of the Samba raw smb client library.

The student should write sub-tests for smbtorture which should demonstrate how the protocol works against a Windows server.
The student doesn't have to implement the Samba server code.
Noel Power from SUSE has done some basic server implementation, he should be able to give guidance

*Difficulty: Medium, Hard
*Language(s): C, (Python)
*Possible Mentors: Noel Power

===============================
Ceph RADOS key-value store as an alternative to TDB
===============================

------------------------

Ceph offers a highly scalable and fault-tolerant storage system. Samba is already capable of sharing data located on the [https://ceph.com/ceph-storage/file-system/ Ceph Filesystem], however scale-out sharing (the same data exposed by multiple Samba nodes) currently requires the use of [https://ctdb.samba.org/ CTDB] for consistent and coherent state across Samba cluster nodes. In such a setup CTDB provides a clustered database with persistent key-value data storage and locking. Database usage is abstracted out via a generic *dbwrap* interface.

Ceph's librados library provides an API for the storage and retrieval of arbitrary key-value data via the *omap* functions. A watch/notify protocol is also provided as a mechanism for synchronising client state (locking). Key-value data stored in the RADOS back-end inherits the same redundancy features as regular objects, making it a potentially good candidate as a replacement for CTDB in scale-out Samba clusters.

This task involves the implementation and testing of a new *dbwrap* back-end that uses librados for the storage, retrieval and locking of Samba key-value state. Ideally, the candidate would also allow time for benchmarking.

*Difficulty: Medium
*Language(s): C
*Possible Mentors: David Disseldorp

-->
Samba binary size reduction
------------------------

Samba has grown to quite a bloated beast. This task will focus on some areas where the bloat can easily be reduced, e.g.
* Removal of unused autogenerated librpc code
** RPC client and server code when only serialization functions are used
** optional struct print routines (pidl noprint?)
** Some knowledge of perl would help here (for pidl)
* Add new build options to compile Samba without certain functionality
** Undesired DCE/RPC services
** Printing support
** Legacy SMB/CIFS support (stretch goal)

 The NDR parsing Gordian knot =
===============================

------------------------

What we need is for someone to take the ideas in the page, and the
concepts from the patch and make them a practical solution for Samba.

A particular spot where we over-link is via the ndr-table subsytem,
which links to most of our (large) generated parsers for every RPC
protocol we know.

Some bits of the code that link to ndr-table only want some metadata,
but they end up linking to each NDR_* subsystem because the metadata is
at the bottom of each parser (the ndr_*.c files).  

The primary task may well end up being in Perl, or Python, or C, the
challenge here will not be deep programming but lateral thinking about
how to break up the dependency chains.

Making this more challenging or interesting (depending on your
perspective), the RPC server is being rewritten, but for now the nexus
in source3/rpc_server/rpc_ncacn_np.c and the calls to
ndr_table_by_uuid() remain.

What applicants may wish to do is see if that can be re-written in 
such a way that does not require linking in the whole NDR
parser, just to obtain the information actually used. 

The broader issue=
===============================

------------------------

The challenge is that Samba over-links because some of the dependencies
are not fine grained enough.

The fundamental challenge is likely to be a lot of cases where:

* A depends on B
* C depends on D

But B and C are in the same file, but B does not actually depend on C.

There are likely many other parts of Samba which link to large things
that are not needed.  For example, perhaps we should allow a build
without printing support, or parsing of the printing protocols?

The challenge is is doing this in a way that is clean, and mostly done
in the build system, not by #ifdef in the code.

Skills in determining binary dependencies, as well as the build system
dependencies (to compare with) would be very helpful in this project. 

*Difficulty: medium
*Language(s): Some knowledge of C and python would be helpful
*Possible Mentors: Andrew Bartlett
*References: [https://lists.samba.org/archive/samba-technical/2019-November/134539.html patches by Andrew Bartlett in 2020]

Samba selftest efficiency improvement
------------------------

Samba's selftest and GitLab CI pipeline has grown to quite a bloated beast. Even where we save wall-clock time by using parallel virtual machines, CPU time is spent costing the Samba Team money and using electricity more broadly.

This task will focus on some areas where the expense, bloat and duplication can easily be reduced, e.g.
* De-duplication of duplicate test runs
* Caching of compilation output between stages
* Use of pipeline stages to avoid starting 20 potentially expensive jobs if a smoke-test build does not pass

The ideal candidate will do some of their own investigation to show they understand how savings could be made, rather than just sticking to this list.

*Difficulty: medium
*Language(s): Some knowledge of C and python would be helpful, an understanding of GitLab pipelines will help a lot
*Possible Mentors: Andrew Bartlett

===============================
Samba AD DC as the ideal POSIX Directory
===============================

------------------------

Samba is a great Active Directory Domain Controller, but it is not an ideal directory server for a large, passionate and important user base:  Sites with Samba SMB servers, but also general purpose Linux servers.  A smaller subset of these sites also have Linux desktops.  These sites may also have Windows servers, but they like the Windows desktops, are not the focus. 

These sites often used Samba + OpenLDAP, and are finding the move to Samba's AD DC a bit difficult, because schema extension is hard, some things are not done automatically (like uidNumber allocation), and in general the focus has been around matching Windows not listening to the needs of this part of our user base. 

Specific research should be done into what FreeIPA does well in targeting this user segment, and what customisations advanced users of OpenLDAP apply. 

This project would be to propose a number of specific improvements, and to add both tests and an implementation of these improvements to Samba.

*Difficulty: Hard
*Languages(s): C, Python
*Possible Mentors: Andrew Bartlett

===============================
GitLab CI of Samba for non-linux platforms (FreeBSD in particular)
===============================

------------------------

Samba uses GitLab CI to improve the quality of our patches.  Efforts are currently underway to extend the docker container used from just Ubuntu 14.04 to later versions and other distributions.

However, we occasionally have issues ensuring Samba still builds and operates on FreeBSD and other non-linux platforms.

The idea would be to have a docker image and .gitlab-ci.yml code to support it that runs FreeBSD and then builds and runs Samba's testsuite inside that FreeBSD nested VM, while still outputting the results to the normal gitlab-ci.

This differs from just running GitLab CI runners on FreeBSD as we need auto-scale, destroy the host and guest at the end of the test and run on Linux docker (such as the free GitLab,com CI runners). 

As a stretch goal, being able to run some tests against a specific linux kernel and a raw ext4 filesystem (rather than unionfs) via qemu rather than depending on the docker host configuraiton.

*Difficulty: Medium
*Language(s): Python, shell, YAML
*Possible Mentors: Andrew Bartlett

===============================
Integrate Samba AD DC deployment and management with Cockpit
===============================

------------------------

A prototype at https://github.com/abbra/cockpit-app-samba-ad shows how we can integrate Samba AD deployment with Cockpit Linux management console. A goal of this task is to move forward with this prototype to produce a fully working Samba AD management tool for common operations supported by 'samba-tool' command line utility.

The prototype is only a demo what could be done. For comprehensive work following materials need to be consulted:

* cockpit-design, as they provide a good researched UX and UI designs for Cockpit apps for multiple areas over past few years, https://github.com/cockpit-project/cockpit-design/
* SuSE YaST work around https://github.com/yast?q=samba (see https://sambaxp.org/fileadmin/user_upload/sambaxp2019-slides/mulder_sambaxp2019_samba_active_adrectory_tools_windows_admin.pdf for some details)
* Cockpit starter kit, https://github.com/cockpit-project/starter-kit
* 389-ds Cockpit plugin, https://pagure.io/389-ds-base/blob/master/f/src/cockpit/389-console
* Cockpit virtual machines interfaces (both cockpit-docker and cockpit-podman), see more https://github.com/cockpit-project

The project would need to investigate:

* a possible UX and UI design
* define base set of use cases that can be mapped to distinct Samba AD as covered in https://wiki.samba.org/index.php/Setting_up_Samba_as_an_Active_Directory_Domain_Controller, https://wiki.samba.org/index.php/Setting_up_Samba_as_a_Domain_Member, https://wiki.samba.org/index.php/Joining_a_Samba_DC_to_an_Existing_Active_Directory, and other domain controller related tasks as described in https://wiki.samba.org/index.php/User_Documentation
* build actual Cockpit app that implements a clearly defined subset of those use cases.

A successful project proposal would be able to present a set of use cases proposed to focus on since this is a huge area, a scope to work on, how one would expect to work on deliverables, technology and process wise.

It would be nice to see how this project would evolve after the GSoC session would end, since it is clearly a longer term work that would need to be passed over and shared with more contributors.

*Difficulty: Medium
*Language(s): Python, JavaScript (React), CSS, HTML
*Possible Mentors: Alexander Bokovoy

<!-- Commented out possibly stale proposals

===============================
Make libsmbclient thread-safe for Gnome VFS
===============================

------------------------

libsmbclient is currently not thread-safe, even when different threads use different libsmbclient contexts. This has a big impact on use by the Gnome VFS code. The easiest fix is to add a "Big Lock" around all elements of libsmbclient that are not currently thread safe. This is not fine-grained threading support, but would add mutexes to creating and any manipulation of contexts. Locks also need to be added around any calls into the parameter reading/writing subsystems, and many other places within libsmbclient. Success would be to report safe hellgrind usage on test code using multiple libsmbclient contexts to access common files from a single server/share.

*Difficulty: Medium, Hard
*Language(s): C
*Possible mentors: Jeremy Allison
-->

Linux Kernel SMB Client Improvements
------------------------

The Linux Kernel has a module called cifs.ko which is independent from Samba (it doesn't share code) that allows users to mount remote shares. It supports multiple dialects of SMB (1, 2, 3). The protocol dialects are now officially documented by Microsoft (See [https://msdn.microsoft.com/en-us/library/cc246231.aspx MS-SMB], [https://msdn.microsoft.com/en-us/library/cc246482.aspx MS-SMB2]) so students shouldn't have to worry about reverse engineering to understand them. The [https://www.wireshark.org/ Wireshark] open source network sniffer&dissector is a very good learning tool as well.

Interested students should contact Steve French or the [mailto:linux-cifs@vger.kernel.org linux-cifs mailing list] to discuss possible improvements to the Linux Kernel CIFS VFS client.

Add machine-readable debug & stats /proc file
------------------------

* We currently output debug and statistic information under /proc/fs/cifs/ (DebugData, Stats, ...). We need to stop outputing free format text that breaks all parsers out there everytime we add things to it. Clean up the cifsdebug.c file (it is kind of messy). Possibly generate a hierarchy of /proc files (e.g. a dir per tcp connection, subdirs for session, files for tcons) instead of dumping everything in one file.
* Make a nice visualizer/dashboard thing to get an overview. This could be console or GUI or...
* Language: C for the kernel stuff, Userspace can be C, C++, Python.
* Difficulty: Low

Add additional ftrace (trace-cmd) tracepoints and better GUI
------------------------

* Add more dynamic tracepoints to cifs.ko for commonly needed user scenarios, and add GUI (and/or CLI) tool to make it easier to enable/disable cifs.ko dynamic trace points (See /sys/kernel/debug/tracing/events/cifs/ for the pseudo-files that are currently configured manually for tracing or via trace-cmd) Make a nice native/console/web UI for it.
* Language: C (any kernel changes) and userspace C, C++ or Python
* Difficulty: Low

Add performance analysis cli tools
------------------------

* Add more perf tools for SMB3 client (similar to iostat or nfsstat) that leverage (and possibly extend what is captured) in /proc/fs/cifs/Stats but make it easier to analyze performance of a cifs mount
* Language: C, C++ or Python
* Difficulty: Low

Write the One-True-Tool to unify probe/setup/configuration cifs.ko properly
------------------------

* There are too many knobs in different places at the moment: request-keys, idmap, cifscreds, /proc stuff. This goal of this project would be to write one CLI tool that would wrap everything under a common interface. It could handle getting/setting ACL as well.
* This would be a userspace project.
* The implementation shouldn't too hard but the student will need to learn about the existing configuration ways and usecases which can be long.
* Language: C, C++, Python
* Difficulty: Medium

Add support for DAC (Claims Based ACLs) to the smb3 kernel module and tools
------------------------

* Similar to what was done to extend the Apache access control model to allow richer ACL semantics,
* SMB3's access control model was extended (at least in Windows).   See e.g. [https://docs.microsoft.com/en-us/windows/security/identity-protection/access-control/dynamic-access-control Dynamic Access Control].
* Add support to the SMB3 kernel module and user space tools (or create new ones) to allow viewing and managing claims based ACLs (DAC) from the client.
* Difficulty: Medium
* Language: C (kernel), C/C++/Python (user space tools)

Add support for ODX (T10) Copy Offload to the smb3 kernel module
------------------------

* Windows, and various NAS servers support ODX copy offload (e.g. [https://docs.netapp.com/ontap-9/index.jsp?topic=%2Fcom.netapp.doc.cdot-famg-cifs%2FGUID-1323806A-F37B-46AF-B123-E40FCD362B33.html NetApp]),
* to allow much faster server side copy.
* Add support to the kernel client for this and integration with existing or new copy offload tools to make it easy to use.  
* For more information see [https://msdn.microsoft.com/en-us/library/cc246482.aspx MS-SMB2] and [https://www.slideshare.net/gordonross/smb3-offload-data-transfer-odx SMB3 offload data transfer] and [https://docs.microsoft.com/en-us/windows-hardware/drivers/ifs/offloaded-data-transfers Offloaded data transfers] and references at bottom of `Server-Side_Copy`. 
* Language: C
* Difficulty: Medium/High

===============================
VFS change notification support in cifs.ko
------------------------

* The kernel provides a file/dir notification API (inotify, dnotify). The SMB protocol also provides a way to get notified of file changes. This project would be about implementing the inotify API for cifs.ko by making use of the SMB notification mechanism.
* Difficulty: Hard
* Language: C

<!-- Commenting out stale proposals

===============================
Failover/Continuous Availability and HA improvements (Witness protocol)
===============================

------------------------

* Benefits: Improved reliability, data integrity - may also allow planned migrations (moving data from one server to another).  
* Challenges: Complexity, requires additional RPC infrastructure in client.  There is a Samba user space prototype of the Witness protocol that could be reused (since we only need the client part of the RPC calls).
* Language: C
* Difficulty: High

===============================
File Copy Offload: T10 operations, and improved tools for using CopyChunk
===============================

------------------------

* Benefits: Improved performance.  Copy offload is useful for quickly replicating large files, and for backup and for virtualization. Good news is that one copy offload mechanism (CopyChunk) already works.  Windows 2012 introduced a [https://msdn.microsoft.com/en-us/library/windows/desktop/hh848056(v=vs.85).aspx second mechanism] and also see pages 33 to 42 of [http://www.snia.org/sites/default/files/SNIA_SMB3_final.pdf SNIA SMB3]).  May be even more useful if TRIM/DISCARD support also added.   This is also very timely given the recent support in the linux kernel vfs being added for the copy_range API.
* Challenges: Ensuring semantics match what is being used in the new copy_range Linux kernel interface, and if not either emulate the alternate semantics, enhance copy_range or provide additional private ioctls to handle the SMB3 copy offload semantics (CopyChunk vs. ODX)
* Language: C
* Difficulty: Low / moderate
* Possible Mentors: Steve French

===============================
Multiadapter support
===============================

------------------------

* Benefits: Big performance advantage for some common cases (e.g. RSS capable adapters, and also two adapter scenarios) and prepares for RDMA in the future which will help cifs.ko in even more workloads.
* Challenges: Testing may require more physical hardware (two, dual adapter machines to demonstrate performance improvements).
* Language: C
* Difficulty: Moderate
* Possible Mentors: Steve French

===============================
Directory oplocks
===============================

------------------------

* Benefits: Will reduce network load a lot in some workloads, and improve performance as well. Works with recent Windows servers (Windows 2012 and later e.g.).
* Challenges: Samba does not support it yet (although this might help drive changes to the Server and Linux VFS eventually, if we have client support).
* Language: C
* Difficulty: Moderate
* Possible Mentors: Steve French

Support for SELinux
------------------------

* Mac Security Label support is important for virtualization and useful for improved security some workloads.  Support for setting/getting these labels over the wire was investigated in the NFS version 4 workgroup.  Adding support to the CIFS Unix Extensions (Linux kernel client and Samba server) should be possible, especially if this is just a new class of extended attribute.  The goal would be to support this feature of SELinux to allow KVM and other applications to take advantage of security labels.  Some of the background requirements are loosely related to the (nfs equivalent of) what is mentioned in:  [http://tools.ietf.org/html/draft-quigley-nfsv4-sec-label-01 NFSv4]
* Language: C
* Difficulty: Hard
* Possible Mentors: Steve French

===============================
Create GUI or command-line tools for displaying /proc/fs/cifs statistics and and mount/session status
===============================

------------------------

* Might also involve some cleanup of the in-kernel stats / status output.
* A mostly complete [http://oss.sgi.com/archives/pcp/2013-08/msg00090.html cifs.ko Performance Co-Pilot (PCP) monitoring agent] was implemented in 2013.
* Language: some C (for kernel code), something else for GUI?
* Difficulty: Easy
* Possible Mentors: Steve French

===============================
Create a common uid mapping mechanism for Linux nfs and cifs vfs clients
===============================

------------------------

* or maybe just figure out a way to hook cifs up to rpc.idmapd
* add a way for the client to remap the uids returned by the server to uids which would be valid on the client (or to a default if such uid does not exist).
* This is helpful especially when the server supports the CIFS Unix Extensions and has different uids and gids mapping than the client
* Difficulty: Hard
* Possible Mentors: Steve French

===============================
Support for retrieving snapshots, encrypted files, or compressed files from Windows
===============================

------------------------

* Difficulty: Medium
* Possible Mentors: Steve French

===============================
cifs->Samba automated test facility
===============================

------------------------

* Do build verification similar to what we can now do with the Samba server and tools in the Samba build farm.  Mounts from the Linux SMB3, SMB2 and CIFS kernel clients could be tested with posix file i/o tests which might include modified versions of the "connectathon" and xfstest test suites and others.  The goal is to quickly identify problems with newly integrated patches by running automatically against a variety of cifs/smb2/smb3 mounts (and mount options) to ensure that regressions aren't introduced.
* xfstests support for CIFS was added as part of `SoC/2014`.
* Difficulty: Medium
* Possible Mentors: Steve French

===============================
Other Random Ideas
===============================

------------------------

* Ideas aren't limited to these, feel free to propose something else:
** Improve integration between cifs.ko and userspace Samba tools and libraries.  Allow userspace Samba libraries to use an existing CIFS mount if it exists by passing requests (via an ioctl or other user->kernel IPC) to cifs.ko.  This could improve performance but also more naturally allow use of the same credentials for a user across file and management operations (e.g. listing shares via smbclient and mounting that share).
** Create a GUI for creating and managing Linux cifs mounts, and more easily configuring the many complex cifs mount options, statistics (/proc/fs/cifs)
** Support for alternate transport protocols (other than TCP sockets).  Adding support for SCTP to cifs/smb2 kernel clients and Samba server or perhaps more interesting add support for Linux's "virtio" transport to the cifs/smb2 kernel clients and Samba server (to allow optimized mounts and zero-copy transfer of data from virtualized guests to hosts on the same box)
** Support for features (such as directory delegations) which NFS version 4.1 has but which current CIFS even with the most current CIFS->Samba protocol extensions (CIFS Unix Extensions) do not have -- will probably need server support too.
** Add additional library support or modify Samba client libraries so they can use existing kernel cifs functions (such as sending SMBs on negotiated sessions when the kernel client already has a session to the server).  With the addition of library to access cifs's pipe (in kernel), Samba client libraries or other dce/rpc code could use cifs kernel sessions for management of and over cifs mounts.
** Add libraries and utilities to manage acls (cifs kernel client has an extended attribute for setting/getting "raw" cifs acls but userspace posix acl tools obviously can't be used to manage cifs specific acl features).
*Difficulty: Low
*Language(s): C
*Possible mentors: Steve French

Build Farm
------------------------

The `http://build.samba.org/ Build Farm` is a set of machines with different configuration that regularly rebuild the latest snapshots of Samba and other projects on different platforms, to catch portability issues. It has a web interface and sends out emails.

===============================
Improve Build Farm look and Feel
===============================

------------------------

Samba's [http://build.samba.org build farm] still hasn't adopt the new Samba graphical chart and the look and feel is not very good.
With this submission we propose to address this with the following objectives:

*Main ideas:
** Adopt the new samba style 
** Improve reporting (ie. present which are the build that can't be built and which are not, daily emails, ...)
** Make test errors quickly accessible, in this [http://build.samba.org/build.cgi/build/d72e624c4a62a62e8d34b0c54efc2a97c0493aa9 example], user has to scroll a long time before meeting the errors
** Add the capacity to manage flaky tests, reduce emails alerts (ie. need 2 consecutive builds with the same flacky test to trigger a real error)
** Improve page loading speed (ajax ?)
*Difficulty: Easy to Medium
*Language(s): HTML, CSS, Python
*Possible mentors: Matthieu Patou
-->

Wireshark 
------------------------

Wireshark has two SMB dissectors: "smb" for SMB1, "smb2" for SMB2 and above. It also has a DCE/RPC ([https://en.wikipedia.org/wiki/Microsoft_RPC MSRPC]) dissector that is generated from Samba IDL files.