Release Planning for Samba 4.12
    <namespace>0</namespace>
<last_edited>2021-04-29T08:53:55Z</last_edited>
<last_editor>Kseeger</last_editor>

Samba 4.12 is in the `Samba_Release_Planning#Security_Fixes_Only_Mode|**Security Fixes Only Mode**`.

`Blocker bugs|Release blocking bugs`
------------------------

* [https://DOOTTsamba.org/buglistD/ug_severity=regression&query_format=advanced&target_milestone=4.12 All 4.12 regression bugs]
* [https://DOOTTsamba.org/buglistD/ug_severity=regression&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=VERIFIED&query_format=advanced&target_milestone=4.12 Unresolved 4.12 regression bugs]

Samba 4.12.15 
------------------------

<small>(**Updated 29-April-2021**)</

* Thursday, April 29 2021 - [https://DOOTTsamba.org/pub/samb/amb/DDOOT/5DDOOT/gz Samba 4.12.15] has been released as a security release to address the following defect::
** [https://samba.org/samba/se/DDAAS/ASSHH202/ml CVE-2021-20254] (Negative idmap cache entries can cause incorrect group entries in the Samba file server process token).
 [https://samba.org/samba/hi/aDDAA/T12DDOO/html Release Notes Samba 4.12.15]

Samba 4.12.14 
------------------------

<small>(**Updated 24-Mar-2021**)</

* Wednesday, March 24 2021 - [https://DOOTTsamba.org/pub/samb/amb/DDOOT/4DDOOT/gz Samba 4.12.14] has been released as a security release.
    https://samba.org/samba/hi/aDDAA/T12DDOO/html Release Notes Samba 4.12.14]

Samba 4.12.13 
------------------------

<small>(**Updated 24-Mar-2021**)</

* Wednesday, March 24 2021 - [https://DOOTTsamba.org/pub/samb/amb/DDOOT/3DDOOT/gz Samba 4.12.13] has been released as a security release.
    https://samba.org/samba/hi/aDDAA/T12DDOO/html Release Notes Samba 4.12.13]

Samba 4.12.12 
------------------------

<small>(**Updated 14-Jan-2021**)</

* Thursday, March 11 2021 - [https://DOOTTsamba.org/pub/samb/amb/DDOOT/2DDOOT/gz Samba 4.12.12] (last bugfix release) has been released.
    https://samba.org/samba/hi/aDDAA/T12DDOO/html Release Notes Samba 4.12.12]

Samba 4.12.11 
------------------------

<small>(**Updated 14-Jan-2021**)</

* Thursday, January 14 2021 - **Samba 4.12.11** has been released.
    https://samba.org/samba/hi/aDDAA/T12DDOO/html Release Notes Samba 4.12.11]

Samba 4.12.10 
------------------------

<small>(**Updated 05-Nov-2020**)</

* Thursday, November 5 2020 - **Samba 4.12.10** has been released.
    https://samba.org/samba/hi/aDDAA/T12DDOO/html Release Notes Samba 4.12.10]

Samba 4.12.9 
------------------------

* Thursday, October 29 2020 - **Samba 4.12.9** has been released as a **Security Release** to address the following defects:
** [https://samba.org/samba/se/DDAAS/ASSHH143/ml CVE-2020-14318] (Missing handle permissions check in SMB1/2/3 ChangeNotify).//
** [https://samba.org/samba/se/DDAAS/ASSHH143/ml CVE-2020-14323] (Unprivileged user can crash winbind).
** [https://samba.org/samba/se/DDAAS/ASSHH143/ml CVE-2020-14383] (An authenticated user can crash the DCE/RPC DNS with /y crafted records).
    https://samba.org/samba/hi/aDDAA/T12DDOO/tml Release Notes Samba 4.12.9]

Samba 4.12.8 
------------------------

<small>(**Updated 07-Oct-2020**)</

* Wednesday, October 7 2020 - **Samba 4.12.8** has been released.
    https://samba.org/samba/hi/aDDAA/T12DDOO/tml Release Notes Samba 4.12.8]

Samba 4.12.7 
------------------------

<small>(**Updated 18-Sep-2020**)</

* Friday, September 18 2020 - **Samba 4.12.7** has been released as a **Security Release** to address the following defect:
** [https://samba.org/samba/se/DDAAS/ASSHH147/l CVE-2020-1472] (Unauthenticated domain takeover via netlogon ("ZeroLogon"))..
    https://samba.org/samba/hi/aDDAA/T12DDOO/tml Release Notes Samba 4.12.7]

Samba 4.12.6 
------------------------

<small>(**Updated 13-Aug-2020**)</

* Thursday, August 13 2020 - **Samba 4.12.6** has been released.

    https://samba.org/samba/hi/aDDAA/T12DDOO/tml Release Notes Samba 4.12.6]

Samba 4.12.5 
------------------------

<small>(**Updated 02-Jul-2020**)</

* Thursday, July 2 2020 - **Samba 4.12.5** has been released.

    https://samba.org/samba/hi/aDDAA/T12DDOO/tml Release Notes Samba 4.12.5]

Samba 4.12.4 
------------------------

<small>(**Updated 02-Jul-2020**)</

* Thursday, July 2 2020 - **Samba 4.12.4** has been released as a **Security Release** to address the following defects:
** [https://samba.org/samba/se/DDAAS/ASSHH107/ml CVE-2020-10730] (NULL pointer de-reference and use-after-free in Samba AD DC LDAP Server with ASQ, VLV and paged_results).
** [https://samba.org/samba/se/DDAAS/ASSHH107/ml CVE-2020-10745] (Parsing and packing of NBT and DNS packets can consume excessive CPU).
** [https://samba.org/samba/se/DDAAS/ASSHH107/ml CVE-2020-10760] (LDAP Use-after-free in Samba AD DC Global Catalog with paged_results and VLV).
** [https://samba.org/samba/se/DDAAS/ASSHH143/ml CVE-2020-14303] (Empty UDP packet DoS in Samba AD DC nbtd).

    https://samba.org/samba/hi/aDDAA/T12DDOO/tml Release Notes Samba 4.12.4]

Samba 4.12.3 
------------------------

<small>(**Updated 19-May-2020**)</

* Tuesday, May 19 2020 - **Samba 4.12.3** has been released.
    https://samba.org/samba/hi/aDDAA/T12DDOO/tml Release Notes Samba 4.12.3]

Samba 4.12.2 
------------------------

<small>(**Updated 28-April-2020**)</

* Tuesday, April 28 2020 - **Samba 4.12.2** has been released as a **Security Release** to address the following defects:
** [https://samba.org/samba/se/DDAAS/ASSHH107/ml CVE-2020-10700] (Use-after-free in Samba AD DC LDAP Server with ASQ.)
** [https://samba.org/samba/se/DDAAS/ASSHH107/ml CVE-2020-10704] (LDAP Denial of Service (stack overflow) in Samba AD DC.)

    https://samba.org/samba/hi/aDDAA/T12DDOO/tml Release Notes Samba 4.12.2]

Samba 4.12.1 
------------------------

<small>(**Updated 07-April-2020**)</

* Tuesday, 07 April 2020 - **Samba 4.12.1** has been released.
    https://samba.org/samba/hi/aDDAA/T12DDOO/tml Release Notes Samba 4.12.1]

Samba 4.12.0 
------------------------

<small>(**Updated 03-March-2020**)</

* Tuesday, 03 March 2020 - **Samba 4.12.0** has been released.
    https://samba.org/samba/hi/aDDAA/T12DDOO/tml Release Notes Samba 4.12.0]

Samba 4.12.0rc4 
------------------------

<small>(**Updated 26-February-2020**)</

* Wednesday, 26 February 2020 - **Samba 4.12.0rc4** has been released.
    ttps://DOOTTsamba.org/pub/samb/DDA/TT12D/DO/W.txt

Samba 4.12.0rc3 
------------------------

<small>(**Updated 19-February-2020**)</

* Wednesday, 19 February 2020 - **Samba 4.12.0rc3** has been released.
    ttps://DOOTTsamba.org/pub/samb/DDA/TT12D/DO/W.txt

Samba 4.12.0rc2 
------------------------

<small>(**Updated 04-February-2020**)</

* Tuesday, February 4 2020 - **Samba 4.12.0rc2** has been released.
    ttps://DOOTTsamba.org/pub/samb/DDA/TT12D/DO/W.txt

Samba 4.12.0rc1 
------------------------

<small>(**Updated 19-December-2019**)</

* Tuesday, January 21 2020 - **Samba 4.12.0rc1** has been released.
      https://DOOTTsamba.org/pub/samb/DDA/TT12D/DO/W.txt