Release Planning for Samba 4.11
    <namespace>0</namespace>
<last_edited>2021-03-09T13:01:32Z</last_edited>
<last_editor>Kseeger</last_editor>

Samba 4.11 has been marked `Samba_Release_Planning#Discontinued_.28End_of_Life.29|**discontinued**`.

`Blocker bugs|Release blocking bugs`
------------------------

* [https://bugzilla.samba.org/buglist.cgi?bug_severity=regression&query_format=advanced&target_milestone=4.11 All 4.11 regression bugs]
* [https://bugzilla.samba.org/buglist.cgi?bug_severity=regression&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=VERIFIED&query_format=advanced&target_milestone=4.11 Unresolved 4.11 regression bugs]

Samba 4.11.17 
------------------------

<small>(**Updated 03-December-2020**)</small>

* Thursday, December 03 2020 - **Samba 4.11.17** has been released as an extraordinary bugfix release.
    https://www.samba.org/samba/history/samba-4.11.17.html Release Notes Samba 4.11.17]

Samba 4.11.16 
------------------------

<small>(**Updated 04-November-2020**)</small>

* Wednesday, November 04 2020 - **Samba 4.11.16** has been released as an extraordinary bugfix release.
    https://www.samba.org/samba/history/samba-4.11.16.html Release Notes Samba 4.11.16]

Samba 4.11.15 
------------------------

<small>(**Updated 04-November-2020**)</small>

* Thursday, October 29 2020 - **Samba 4.11.15** has been released as a **Security Release** to address the following defects:
** [https://www.samba.org/samba/security/CVE-2020-14318.html CVE-2020-14318] (Missing handle permissions check in SMB1/2/3 ChangeNotify).
** [https://www.samba.org/samba/security/CVE-2020-14323.html CVE-2020-14323] (Unprivileged user can crash winbind).
** [https://www.samba.org/samba/security/CVE-2020-14383.html CVE-2020-14383] (An authenticated user can crash the DCE/RPC DNS with easily crafted records).
    https://www.samba.org/samba/history/samba-4.11.15.html Release Notes Samba 4.11.15]

Samba 4.11.14 
------------------------

<small>(**Updated 06-October-2020**)</small>

* Tuesday, October 06 2020 - **Samba 4.11.14** has been released. There will be security releases only beyond this point.
    https://www.samba.org/samba/history/samba-4.11.14.html Release Notes Samba 4.11.14]

Samba 4.11.13 
------------------------

<small>(**Updated 18-September-2020**)</small>

* Friday, September 18 2020 - **Samba 4.11.13** has been released as a **Security Release** to address the following defect:
** [https://www.samba.org/samba/security/CVE-2020-1472.html CVE-2020-1472] (Unauthenticated domain takeover via netlogon ("ZeroLogon"))..
    https://www.samba.org/samba/history/samba-4.11.13.html Release Notes Samba 4.11.13]

Samba 4.11.12 
------------------------

<small>(**Updated 25-August-2020**)</small>

* Tuesday, August 25 2020 - **Samba 4.11.12** has been released.

    https://www.samba.org/samba/history/samba-4.11.12.html Release Notes Samba 4.11.12]

Samba 4.11.11 
------------------------

<small>(**Updated 02-Jul-2020**)</small>

* Thursday, July 2 2020 - **Samba 4.11.11** has been released as a **Security Release** to address the following defects:
** [https://www.samba.org/samba/security/CVE-2020-10730.html CVE-2020-10730] (NULL pointer de-reference and use-after-free in Samba AD DC LDAP Server with ASQ, VLV and paged_results).
** [https://www.samba.org/samba/security/CVE-2020-10745.html CVE-2020-10745] (Parsing and packing of NBT and DNS packets can consume excessive CPU).
** [https://www.samba.org/samba/security/CVE-2020-10760.html CVE-2020-10760] (LDAP Use-after-free in Samba AD DC Global Catalog with paged_results and VLV).
** [https://www.samba.org/samba/security/CVE-2020-14303.html CVE-2020-14303] (Empty UDP packet DoS in Samba AD DC nbtd).

    https://www.samba.org/samba/history/samba-4.11.11.html Release Notes Samba 4.11.11]

Samba 4.11.10 
------------------------

<small>(**Updated 30-June-2020**)</small>

* Tuesday, June 30 2020 - **Samba 4.11.10** has been released.

    https://www.samba.org/samba/history/samba-4.11.10.html Release Notes Samba 4.11.10]

Samba 4.11.9 
------------------------

<small>(**Updated 05-May-2020**)</small>

* Tuesday, May 5 2020 - **Samba 4.11.9** has been released.

    https://www.samba.org/samba/history/samba-4.11.9.html Release Notes Samba 4.11.9]

Samba 4.11.8 
------------------------

<small>(**Updated 28-April-2020**)</small>

* Tuesday, April 28 2020 - **Samba 4.11.8** has been released as a **Security Release** to address the following defects:
** [https://www.samba.org/samba/security/CVE-2020-10700.html CVE-2020-10700] (Use-after-free in Samba AD DC LDAP Server with ASQ.)
** [https://www.samba.org/samba/security/CVE-2020-10704.html CVE-2020-10704] (LDAP Denial of Service (stack overflow) in Samba AD DC.)

    https://www.samba.org/samba/history/samba-4.11.8.html Release Notes Samba 4.11.8].

Samba 4.11.7 
------------------------

<small>(**Updated 10-Mar-2020**)</small>

* Tuesday, March 10 2020 - **Samba 4.11.7** has been released.

    https://www.samba.org/samba/history/samba-4.11.7.html Release Notes Samba 4.11.7].

Samba 4.11.6 
------------------------

<small>(**Updated 28-Jan-2020**)</small>

* Tuesday, January 28 2020 - **Samba 4.11.6** has been released.

    https://www.samba.org/samba/history/samba-4.11.6.html Release Notes Samba 4.11.6].

Samba 4.11.5 
------------------------

<small>(**Updated 21-Jan-20120**)</small>

* Tuesday, January 21 2020 - **Samba 4.11.5** has been released as a **Security Release** to address the following defects:
** [https://www.samba.org/samba/security/CVE-2019-14902.html CVE-2019-14902] (Replication of ACLs set to inherit down a subtree on AD Directory not automatic.)
** [https://www.samba.org/samba/security/CVE-2019-14907.html CVE-2019-14907] (Crash after failed character conversion at log level 3 or above.)
** [https://www.samba.org/samba/security/CVE-2019-19344.html CVE-2019-19344] (Use after free during DNS zone scavenging in Samba AD DC.)

    https://www.samba.org/samba/history/samba-4.11.5.html Release Notes Samba 4.11.5].

Samba 4.11.4 
------------------------

<small>(**Updated 16-Dec-2019**)</small>

* Monday, December 16 2019 - **Samba 4.11.4** has been released.
    https://www.samba.org/samba/history/samba-4.11.4.html Release Notes Samba 4.11.4].

Samba 4.11.3 
------------------------

* Tuesday, December 10 2019 - **Samba 4.11.3** has been released as a **Security Release** to address the following defects:
** [https://www.samba.org/samba/security/CVE-2019-14861.html CVE-2019-14861] (Samba AD DC zone-named record Denial of Service in DNS management server (dnsserver))
** [https://www.samba.org/samba/security/CVE-2019-14870.html CVE-2019-14870] (DelegationNotAllowed not being enforced in protocol transition on Samba AD DC.)

    https://www.samba.org/samba/history/samba-4.11.3.html Release Notes Samba 4.11.3].

Samba 4.11.2 
------------------------

<small>(**Updated 129-Oct-2019**)</small>

* Tuesday, October 29 2019 - **Samba 4.11.2** has been released as a **Security Release** to address the following defects:
** [https://www.samba.org/samba/security/CVE-2019-10218.html CVE-2019-10218] (Client code can return filenames containing              path separators.)
** [https://www.samba.org/samba/security/CVE-2019-14833.html CVE-2019-14833] (Samba AD DC check password script does not receive the full password.)
** [https://www.samba.org/samba/security/CVE-2019-14847.html CVE-2019-14847] (User with "get changes" permission can     crash AD DC LDAP server via dirsync)

    https://www.samba.org/samba/history/samba-4.11.2.html Release Notes Samba 4.11.2].

Samba 4.11.1 
------------------------

<small>(**Updated 18-Oct-2019**)</small>

* Friday, October 18 2019 - **Samba 4.11.1** has been released.
    https://www.samba.org/samba/history/samba-4.11.1.html Release Notes Samba 4.11.1]

Samba 4.11.0 
------------------------

<small>(**Updated 17-Sep-2019**)</small>

* Tuesday, September 17 2019 - **Samba 4.11.0** has been released.
    https://www.samba.org/samba/history/samba-4.11.0.html Release Notes Samba 4.11.0]

Samba 4.11.0rc4 
------------------------

<small>(**Updated 11-Sep-2019**)</small>

* Wednesday, September 11 2019 - **Samba 4.11.0rc4** has been released.
    ttps://download.samba.org/pub/samba/rc/samba-4.11.0rc4.WHATSNEW.txt

Samba 4.11.0rc3 
------------------------

<small>(**Updated 03-Sep-2019**)</small>

* Tuesday, September 03 2019 - **Samba 4.11.0rc3** has been released.
    ttps://download.samba.org/pub/samba/rc/samba-4.11.0rc3.WHATSNEW.txt

Samba 4.11.0rc2 
------------------------

<small>(**Updated 21-August-2019**)</small>

* Wednesday, August 21 2019 - **Samba 4.11.0rc2** has been released.
    ttps://download.samba.org/pub/samba/rc/samba-4.11.0rc2.WHATSNEW.txt

Samba 4.11.0rc1 
------------------------

<small>(**Updated 09-Jul-2019**)</small>

* Tuesday, July 09 2019 - **Samba 4.11.0rc1** has been released.

    ttps://download.samba.org/pub/samba/rc/samba-4.11.0rc1.WHATSNEW.txt