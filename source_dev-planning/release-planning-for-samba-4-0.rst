Release Planning for Samba 4.0
    <namespace>0</namespace>
<last_edited>2015-09-08T14:36:49Z</last_edited>
<last_editor>Kseeger</last_editor>

With the release of Samba 4.3.0, Samba 4.0 has been marked `Samba_Release_Planning#Discontinued_.28End_of_Life.29|**discontinued**`.

Samba 4.0.26 
------------------------

(**Updated 06-May-2015**)

* Monday, May 6 - Samba 4.0.26 has been released (please note that this is the **last bugfix release** of the Samba 4.0 release series!)
    http://www.samba.org/samba/history/samba-4.0.26.html Release Notes Samba 4.0.26]

Samba 4.0.25 
------------------------

(**Updated 23-Febrary-2015**)

* Monday, February 23 - Samba 4.0.25 has been released as a **Security Release** in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-0240 CVE-2015-0240] (Unexpected code execution in smbd).
    http://www.samba.org/samba/history/samba-4.0.25.html Release Notes Samba 4.0.25]

Samba 4.0.24 
------------------------

(**Updated 15-January-2015**)

* Thursday, January 15 - Samba 4.0.24 has been released as a **Security Release** in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-8143 CVE-2014-8143] (Elevation of privilege to Active Directory).
    http://www.samba.org/samba/history/samba-4.0.24.html Release Notes Samba 4.0.24]

Samba 4.0.23 
------------------------

(**Updated 08-December-2014**)

* Monday, December 8 - Samba 4.0.23 has been released
    http://www.samba.org/samba/history/samba-4.0.23.html Release Notes Samba 4.0.23]

Samba 4.0.22 
------------------------

(**Updated 15-September-2014**)

* Monday, September 15 - Samba 4.0.22 has been released
    http://www.samba.org/samba/history/samba-4.0.22.html Release Notes Samba 4.0.22]

Samba 4.0.21 
------------------------

(**Updated 01-August-2014**)

* Friday, August 01 - Samba 4.0.21 has been released as a **Security Release** in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-3560 CVE-2014-3560] (Remote code execution in nmbd).
    http://www.samba.org/samba/history/samba-4.0.21.html Release Notes Samba 4.0.21]

Samba 4.0.20 
------------------------

(**Updated 30-July-2014**)

* Wednesday, July 30 - Samba 4.0.20 has been released
    http://www.samba.org/samba/history/samba-4.0.20.html Release Notes Samba 4.0.20]

Samba 4.0.19 
------------------------

(**Updated 23-June-2014**)

* Monday, June 23 - Samba 4.0.19 has been released as a **Security Release** in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0244 CVE-2014-0244] (Denial of service - CPU loop) and [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-3493 CVE-2014-3493] (Denial of service - Server crash/memory corruption).
    http://www.samba.org/samba/history/samba-4.0.19.html Release Notes Samba 4.0.19]

Samba 4.0.18 
------------------------

(**Updated 27-May-2014**)

* Tuesday, May 27 - Samba 4.0.18 has been released
    http://www.samba.org/samba/history/samba-4.0.18.html Release Notes Samba 4.0.18]

Samba 4.0.17 
------------------------

(**Updated 15-April-2014**)

* Tuesday, April 15 - Samba 4.0.17 has been released
    http://www.samba.org/samba/history/samba-4.0.17.html Release Notes Samba 4.0.17]

Samba 4.0.16 
------------------------

(**Updated 11-March-2014**)

* Tuesday, March 11 - Samba 4.0.16 has been released as a **Security Release** in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4496 CVE-2013-4496] (Password lockout not enforced for SAMR password changes) and [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-6442 CVE-2013-6442] (smbcacls will remove the ACL on a file or directory when changing owner or group owner).
    http://www.samba.org/samba/history/samba-4.0.16.html Release Notes Samba 4.0.16]

Samba 4.0.15 
------------------------

(**Updated 18-February-2014**)

* Tuesday, February 18 - Samba 4.0.15 has been released
    http://www.samba.org/samba/history/samba-4.0.15.html Release Notes Samba 4.0.15]

Samba 4.0.14 
------------------------

(**Updated 07-January-2014**)

* Tuesday, January 7 - Samba 4.0.14 has been released
    http://www.samba.org/samba/history/samba-4.0.14.html Release Notes Samba 4.0.14]

Samba 4.0.13 
------------------------

(**Updated 09-December-2013**)

* Monday, December 09 - Samba 4.0.13 has been released as a **Security Release** in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4408 CVE-2013-4408] (ACLs are not checked on opening an alternate data stream on a file or directory) and [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-6150 CVE-2012-6150] (pam_winbind login without require_membership_of restrictions).
    http://www.samba.org/samba/history/samba-4.0.13.html Release Notes Samba 4.0.13]

Samba 4.0.12 
------------------------

(**Updated 19-November-2013**)

* Tuesday, November 19 - Samba 4.0.12 has been released
    http://www.samba.org/samba/history/samba-4.0.12.html Release Notes Samba 4.0.12]

Samba 4.0.11 
------------------------

(**Updated 11-November-2013**)

* Monday, November 11 - Samba 4.0.11 has been released as a **Security Release** on order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4475 CVE-2013-4475] (ACLs are not checked on opening an alternate data stream on a file or directory) and [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4476 CVE-2013-4476] (Private key in key.pem world readable).
    http://www.samba.org/samba/history/samba-4.0.11.html Release Notes Samba 4.0.11]

Samba 4.0.10 
------------------------

(**Updated 08-October-2013**)

* Tuesday, October 8 - Samba 4.0.10 has been released
    http://www.samba.org/samba/history/samba-4.0.10.html Release Notes Samba 4.0.10]

Samba 4.0.9 
------------------------

(**Updated 20-August-2013**)

* Tuesday, August 20 - Samba 4.0.9 has been released
    http://www.samba.org/samba/history/samba-4.0.9.html Release Notes Samba 4.0.9]

Samba 4.0.8 
------------------------

(**Updated 05-August-2013**)

* Monday, August 05 - Samba 4.0.8 has been released as a **Security Release**
    http://www.samba.org/samba/history/samba-4.0.8.html Release Notes Samba 4.0.8]

Samba 4.0.7 
------------------------

(**Updated 02-July-2013**)

* Tuesday, July 2 - Samba 4.0.7 has been released
    http://www.samba.org/samba/history/samba-4.0.7.html Release Notes Samba 4.0.7]

Samba 4.0.6 
------------------------

(**Updated 21-May-2013**)

* Tuesday, May 21 - Samba 4.0.6 has been released
    http://www.samba.org/samba/history/samba-4.0.6.html Release Notes Samba 4.0.6]

Samba 4.0.5 
------------------------

(**Updated 09-April-2013**)

* Tuesday, April 9 - Samba 4.0.5 has been released
    http://www.samba.org/samba/history/samba-4.0.5.html Release Notes Samba 4.0.5]

Samba 4.0.4 
------------------------

(**Updated 19-March-2013**)

* Tuesday, March 19 - Samba 4.0.4 has been released as a Security Release
    http://www.samba.org/samba/history/samba-4.0.4.html Release Notes Samba 4.0.4]

Samba 4.0.3 
------------------------

(**Updated 05-February-2013**)

* Tuesday, February 5 - Samba 4.0.3 has been released
    http://www.samba.org/samba/history/samba-4.0.3.html Release Notes Samba 4.0.3]

Samba 4.0.2 
------------------------

(**Updated 30-January-2013**)

* Wednesday, January 30 - Samba 4.0.2 has been issued as a **Security Release** in order to address [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-0213 CVE-2013-0213] (Clickjacking issue in SWAT) and [http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-0214 CVE-2013-0214] (Potential XSRF in SWAT).
    http://www.samba.org/samba/history/samba-4.0.2.html Release Notes Samba 4.0.2]

Samba 4.0.1 
------------------------

(**Updated 15-January-2013**)

* Tuesday, January 15 - Samba 4.0.1 has been released ([http://www.samba.org/samba/history/security.html Security Release])
    http://www.samba.org/samba/history/samba-4.0.1.html Release Notes Samba 4.0.1]

Samba 4.0.0 
------------------------

(**Updated 11-December-2012**)

* Tuesday, December 11 - Samba 4.0.0 has been released
    http://www.samba.org/samba/history/samba-4.0.0.html Release Notes Samba 4.0.0]

Samba 4.0.0rc6 
------------------------

(**Updated 04-December-2012**)

* Tuesday, December 4 - Samba 4.0.0rc6 has been released
    https://download.samba.org/pub/samba/rc/WHATSNEW-4-0-0rc6.txt Release Notes Samba 4.0.0rc6]

Samba 4.0.0rc5 
------------------------

(**Updated 13-November-2012**)

* Tuesday, November 13 - Samba 4.0.0rc5 has been released
    https://download.samba.org/pub/samba/rc/WHATSNEW-4-0-0rc5.txt Release Notes Samba 4.0.0rc5]

Samba 4.0.0rc4 
------------------------

(**Updated 30-October-2012**)

* Tuesday, October 30 - Samba 4.0.0rc4 has been released
    https://download.samba.org/pub/samba/rc/WHATSNEW-4-0-0rc4.txt Release Notes Samba 4.0.0rc4]

Samba 4.0.0rc3 
------------------------

(**Updated 16-October-2012**)

* Tuesday, October 16 - Samba 4.0.0rc3 has been released
    https://download.samba.org/pub/samba/rc/WHATSNEW-4-0-0rc3.txt Release Notes Samba 4.0.0rc3]

Samba 4.0.0rc2 
------------------------

(**Updated 02-October-2012**)

* Tuesday, October 02 - Samba 4.0.0rc2 has been released
    https://download.samba.org/pub/samba/rc/WHATSNEW-4-0-0rc2.txt Release Notes Samba 4.0.0rc2]

Samba 4.0.0rc1 
------------------------

(**Updated 13-September-2012**)

* Tuesday, September 13 - Samba 4.0.0rc1 has been released
    https://download.samba.org/pub/samba/rc/WHATSNEW-4-0-0rc1.txt Release Notes Samba 4.0.0rc1]