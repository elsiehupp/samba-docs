Samba4/LDAP
    <namespace>0</namespace>
<last_edited>2009-11-18T22:06:47Z</last_edited>
<last_editor>Edewata</last_editor>

=================
Structures
=================

ldap_Result 
------------------------

.. code-block::

    struct ldap_Result {
    int resultcode;
    const char *dn;
    const char *errormessage;
    const char *referral;
};

ldap_BindRequest 
------------------------

.. code-block::

    struct ldap_BindRequest {
    int version;
    const char *dn;
    enum ldap_auth_mechanism mechanism;
    union {
        const char *password;
        struct {
            const char *mechanism;
            DATA_BLOB *secblob;/* optional */
        } SASL;
    } creds;
};

ldap_BindResponse 
------------------------

.. code-block::

    struct ldap_BindResponse {
    struct ldap_Result response;
    union {
        DATA_BLOB *secblob;/* optional */
    } SASL;
};

ldap_UnbindRequest 
------------------------

.. code-block::

    struct ldap_UnbindRequest {
    uint8_t __dummy;
};

ldap_SearchRequest 
------------------------

.. code-block::

    struct ldap_SearchRequest {
    const char *basedn;
    enum ldap_scope scope;
    enum ldap_deref deref;
    uint32_t timelimit;
    uint32_t sizelimit;
    bool attributesonly;
    struct ldb_parse_tree *tree;
    int num_attributes;
    const char * const *attributes;
};

ldap_SearchResEntry 
------------------------

.. code-block::

    struct ldap_SearchResEntry {
    const char *dn;
    int num_attributes;
    struct ldb_message_element *attributes;
};

ldap_SearchResRef 
------------------------

.. code-block::

    struct ldap_SearchResRef {
    const char *referral;
};

ldap_mod 
------------------------

.. code-block::

    struct ldap_mod {
    enum ldap_modify_type type;
    struct ldb_message_element attrib;
};

ldap_ModifyRequest 
------------------------

.. code-block::

    struct ldap_ModifyRequest {
    const char *dn;
    int num_mods;
    struct ldap_mod *mods;
};

ldap_AddRequest 
------------------------

.. code-block::

    struct ldap_AddRequest {
    const char *dn;
    int num_attributes;
    struct ldb_message_element *attributes;
};

ldap_DelRequest 
------------------------

.. code-block::

    struct ldap_DelRequest {
    const char *dn;
};

ldap_ModifyDNRequest 
------------------------

.. code-block::

    struct ldap_ModifyDNRequest {
    const char *dn;
    const char *newrdn;
    bool deleteolddn;
    const char *newsuperior;/* optional */
};

ldap_CompareRequest 
------------------------

.. code-block::

    struct ldap_CompareRequest {
    const char *dn;
    const char *attribute;
    DATA_BLOB value;
};

ldap_AbandonRequest 
------------------------

.. code-block::

    struct ldap_AbandonRequest {
    int messageid;
};

ldap_ExtendedRequest 
------------------------

.. code-block::

    struct ldap_ExtendedRequest {
    const char *oid;
    DATA_BLOB *value;/* optional */
};

ldap_ExtendedResponse 
------------------------

.. code-block::

    struct ldap_ExtendedResponse {
    struct ldap_Result response;
    const char *oid;/* optional */
    DATA_BLOB *value;/* optional */
};

ldap_Request 
------------------------

.. code-block::

    union ldap_Request {
    struct ldap_Result 		GeneralResult;
    struct ldap_BindRequest 	BindRequest;
    struct ldap_BindResponse 	BindResponse;
    struct ldap_UnbindRequest 	UnbindRequest;
    struct ldap_SearchRequest 	SearchRequest;
    struct ldap_SearchResEntry 	SearchResultEntry;
    struct ldap_Result 		SearchResultDone;
    struct ldap_SearchResRef 	SearchResultReference;
    struct ldap_ModifyRequest 	ModifyRequest;
    struct ldap_Result 		ModifyResponse;
    struct ldap_AddRequest 		AddRequest;
    struct ldap_Result 		AddResponse;
    struct ldap_DelRequest 		DelRequest;
    struct ldap_Result 		DelResponse;
    struct ldap_ModifyDNRequest 	ModifyDNRequest;
    struct ldap_Result 		ModifyDNResponse;
    struct ldap_CompareRequest 	CompareRequest;
    struct ldap_Result 		CompareResponse;
    struct ldap_AbandonRequest 	AbandonRequest;
    struct ldap_ExtendedRequest 	ExtendedRequest;
    struct ldap_ExtendedResponse 	ExtendedResponse;
};

ldap_message 
------------------------

.. code-block::

    struct ldap_message {
    int                     messageid;
    enum ldap_request_tag   type;
    union ldap_Request      r;
    struct ldb_control    **controls;
    bool                   *controls_decoded;
};