Samba4/Linking AD and unix directories
    <namespace>0</namespace>
<last_edited>2009-02-23T06:08:04Z</last_edited>
<last_editor>Abartlet</last_editor>

==============================

n introduction to windows authentication
===============================

Before we start to look at how to link AD and unix directories using windows trusts, we must first look at windows authentication:

In AD, Windows authentication consists of two parts, which cannot be
reliably be separated:  Kerberos and NTLM

Kerberos has traditionally been one of the hardest networking
technologies to implement correctly, but one that Microsoft has
successfully deployed in AD.

The technology it displaced is also the reason that AD has worked so
well - NTLM authentication has been the backbone of windows networking
since before it even existed (the LM authentication part of this dates
back to the original SMB protocol from IBM). 

What makes this pair of authentication solutions so important is that
they reinforce each other.  

NTLM authentication is pretty simple in concept - it is a
challenge/response based authentication system, where the server
generates a challenge.  In the domain sense this is more complicated,
as the (file) server (for example) must generate the challenge, and
then supply both the challenge and response to whatever server holds
the password.  It does this over the NETLOGON protocol in AD domains,
and in return finds information about the user's groups. 

Kerberos is instead a 'ticket' based authentication system, based on
symetric cryptography to prove that because a user holds a ticket, the
user's identity is proved.

When Kerberos works, it works pretty well
- it is fast, reliable, secure and avoids the server needing to talk
to a DC.  However, when Kerberos fails - as it often does in the real
world, it fails spectacularly without the change Microsoft introduced,
and without the fallback to NTLM.

Whenever a user connects to a host that is not known in the Kerberos
database (AD), or is contacted by an IP address, or where for some
other reason Kerberos has failed, then the user will, without
prompting or any bother, simply fall back to NTLM.  This ensures the
connection still goes ahead dispite the most adverse of network
conditions.  Without inspecting the traffic, it should be impossible
to tell that a corporate network isn't using Kerberos.

The other things Microsoft did to Kerberos include support for name
canonicalisation of both clients and servers, and server-side
referrals.   

These allow users to log in with an e-mail address that may show no
resemblance to their actual principal.  (This is known as logging in
with a user principal name).  When in a forest (at least), the KDC
(for Kerberos) and NETLOGON server (for NTLM) will lookup the UPN
wherever it is in the forest (presumably via the global catalog) and
return either a ticket with the correct principal, or a referall (I
presume) to the other KDC where the user can actually log in.  (In
kerberos, unlike NTLM, the servers and KDC in particular do not talk
to each other on behalf of the user).

Similarly, the KDC will use it's directory knowledge (presumably the
global catalogue again) to help client computers find the correct realm
for a server, and to allow the server to be called many different
names.  These service principal names may or may not be directly tied
to the DNS form of the realm, and it is the local KDC that helps the
client computer find the correct destination.  No local configuration
is used (unlike the [domain_realm] mapping table in MIT kerberos).

Because client machines are only ever joined to one realm, and
typically tickets are only obtained from that realm or it's trusts, it
is clear who to ask for this initial information.

At the DC level, a general mapping between domains and realms is exchanged
when trusted domains are established.  See the
LsarQueryForestTrustInformation call. 

Finally, the PAC (an additional blob of information containing
information on users, groups, logon drives, profile paths, full names
etc) is attached to the kerberos ticket.  This avoids the need for any
lookups to the DC.  

The result of this is that to Microsoft's Kerberos is substantially
easier to use and deploy than traditionally the case with Unix Kerberos.

===============================
Trusted domain types
===============================

Active Directory has a growing array of different ways that it can be
integrated into a series of trusted domains.  These grew out of the
NT4 trusts, which where one-way and not transitive - so a large
company would have to set up a trust from every domain to every other
domain.  The tools eventually made it easier to setup two-way trusts,
but the underlying technology didn't change.

In AD, the following trust types are available:

Parent-child trust
------------------------

The simple case of a child domain.  By means
of parent-child trusts, every domain in the tree can reach the whole
tree (via the root if required)

===============================
Advantages and features
===============================

------------------------

Tight integration into the domain - all client types (not just Windows) will see the trusted domain as a full part of the network.  

===============================
Disadvantages
===============================

------------------------

For integrating between real-AD and Samba4-on-unix-directory this creates a very real challenge.  All hosts and domains must share the same schema and be capable of the domain functional level.  

Sites may be unwilling to create this level of trust, as an AD upgrade might be stimied by lack of a matching upgrade on the Samba4-unix-dir side. 

Tree-root trusts==  
Joins new trees to an existing forest.  Consider
the company merger for example.  

===============================
Advantages and features
===============================

------------------------

Same as for parent-child trust

===============================
Disadvantages
===============================

------------------------

Same as for parent-child trust

External trusts
------------------------

Akin to NT4 trusts.  

===============================
Advantages
===============================

------------------------

Supported in all versions of AD.  

===============================
Disadvantages
===============================

------------------------

Supports NTLM authentication only

MIT Realm trusts
------------------------

A Kerberos-only trust, supporting Unix-like realms for authentication (but not authorization).  

===============================
Advantages
===============================

------------------------

This trust type was introduced to allow migration from existing MIT (or Heimdal)-only environments that existed at some universities in particular.  A common configuration has all the 'unix realm' users also stored in the AD directory, where the users and groups are defined, and a link (altSecurityIdentities) to the 'unix realm' name of the user is made.

===============================
Disadvantages
===============================

------------------------

Complete duplication of all data except the password.  No authorization information is shared, so requires synchronisation or for (at most) clients on the unix side to know to look in AD

Forest trusts
------------------------

Used between two different companies.  Supports Kerberos.

===============================
Advantages
===============================

------------------------

No need for DRSUAPI replication between realms.  No need for common schema.  No need to have the identical functional level. Smallest subset of the AD protocol used across the trust boundary. 

This is clearly the smallest target, and at this time the only viable way to use Samba4 as a bridge between a unix directory and real AD domains. 

===============================
Disadvantages
===============================

------------------------

Only available if the AD domain is at 2003 functional level. 
Will all non-Microsoft clients honour users across this kind of trust?

===============================
Further resources
===============================

See these resources from Microsoft TechNet in particular:
* http://technet.microsoft.com/en-us/library/cc775736.aspx
* [http://technet.microsoft.com/en-us/library/cc772815.aspx How the Kerberos Version 5 Authentication Protocol Works]
* [http://technet.microsoft.com/en-us/library/cc773178.aspx A very good guide on the trusted domain types, and the ports and protocols used]

Also see the original [http://web.mit.edu/kerberos/www/dialogue.html Kerberos Dialogue] for a grounding in the fundamental concepts of Kerberos.

When trying to understand Kerberos referrals, the [http://tools.ietf.org/html/draft-ietf-krb-wg-kerberos-referrals-11 referrals internet-draft] is very useful.

Of referrals and cross-realm authentication
------------------------

The related concepts of [http://tools.ietf.org/html/draft-ietf-krb-wg-kerberos-referrals-11 referrals] and [http://www.faqs.org/faqs/kerberos-faq/general/section-18.html cross-realm authentication] are easily confused.  This is partly because referrals and cross-realm authentication are often used with each other.

===============================
Referrals
===============================

------------------------

Kerberos Referrals come in two varients:  client and server referrals.  

Both involve KDCs returning helpful pointers that the client can use to talk to KDCs in different realms.  In no case will the KDC contact another KDC on a user's behalf, it will simply return the information that the client needs to do the next hop.

This is considered more useful than the previous approach, where hosts were divided into Kerberos realms based on client-side configuration files (which present a management challenge) and strict DNS partitioning (where a DNS domain must strictly match the realm for which all hosts within it must be served)

Client referrals=
===============================

------------------------

Client referrals allow the KDC to return (in response to a kinit or similar) a different principal to the one requested, allowing a login with a more freindly name.  If the client always talks to it's local KDC, then this name might not even match the user's realm.  If the local KDC does not know the name, the error reply may include a hint of another realm to try.

Server referrals=
===============================

------------------------

Similarly for server referrals, the KDC may return an error with a clue as to another realm that should be tried, to find the target.  Cross-realm authentication can then be used to reach that realm, and obtain a ticket to the target.

===============================
Cross-realm authentication
===============================

------------------------

[http://www.faqs.org/faqs/kerberos-faq/general/section-18.html Cross-realm] authentication allows servers in one realm to accept the identity of a user principal from another realm.  Just as for referrals, the KDCs involved do not proxy or otherwise contact other KDCs, but instead allow the client to obtain tickets that are valid for a particular principal in the other realm (because the two realms share secrets for this special record).  Once the cross-realm ticket is obtained, it can be exchanged for a normal ticket to the target service.

The process of obtaining the cross-realm ticket is helped by (but may not use) the referrals, as these give clues as to the correct realms to contact.

===============================
Other challenges
===============================

SID filtering:  When implementing domain trusts, we should validate
the incoming user groups to ensure that they don't compromise security
in this domain.  Just because the SID is included does not mean the
other domain was right to include it.

How should 'Unix' clients be made to handle canonicalisation and
referrals?  Should they run winbindd or similar to know about
'AD-like' forests?

NTLM and Krb5 Implications for trusted domains
------------------------

The two authentication technologies in use here do have major
implications for the design of an AD <-> IPA trusted domain solution.
In particular, a solution that does not provide a target for NTLM
authentication will be significatnly less robust for users who are
'pure kerberos' users, but who access AD resources.  That is, at any time where the
'pure kerberos' user accesses another AD machine by IP address, or if something isn't quite
right for Kerberos (time sync for example), the access will fail for
the 'pure kerberos' users only. 

Also, Kerberos is not universally used or available, particularly
outside the corporate firewall (see for example MSCHAPv2 VPNs).

As such, it may be very much desired to implement the full NTLM stack
in a trusted domain solution.  Implemeting these NETLOGON calls would
not be a particualr additional burden on top of the LSA and other
things a trusted domain would need.