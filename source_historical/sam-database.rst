Samba4/SAM Database
    <namespace>0</namespace>
<last_edited>2009-10-21T01:56:42Z</last_edited>
<last_editor>Edewata</last_editor>

=================
Modules
=================

SAM database uses the following LDB modules:

* resolve_oids
* rootdse
* lazy_commit
* acl
* paged_results
* ranged_results
* anr
* server_sort
* asq
* extended_dn_store
* extended_dn_in
* rdn_name
* objectclass
* descriptor
* samldb
* password_hash
* operational
* kludge_acl
* instancetype
* Backend-specific modules:
** TDB: subtree_rename, subtree_delete, linked_attributes, extended_dn_out_ldb
** OpenLDAP: extended_dn_out_dereference
** Fedora DS: extended_dn_out_dereference
* show_deleted
* `Samba4/LDB/Partition | partition`

=================
Partitions
=================

SAM database defines the following partitions:

* Schema
** Base DN: CN=Schema,CN=Configuration,DC=samba,DC=example,DC=com
** URL: <LDAP_URL>
** Modules: schema_fsmo, <backend-specific modules>
* Configuration
** Base DN: CN=Configuration,DC=samba,DC=example,DC=com
** URL: <LDAP_URL>
** Modules: naming_fsmo, <backend-specific modules>
* User
** DC=samba,DC=example,DC=com
** URL: <LDAP_URL>
** Modules: pdc_fsmo, <backend-specific modules>

Backend-specific modules:
* TDB: none
* OpenLDAP: entryuuid, `Samba4/LDB/Paged Searches | paged_searches`
* Fedora DS: nsuniqueid, `Samba4/LDB/Paged Searches | paged_searches`

PARTITION_URL is the URL of the partition database. For example:
* TDB: users.ldb
* LDAP: ldapi://%2Froot%2FSamba%2Fsamba%2Fsource4%2Fst%2Fdc%2Fprivate%2Fldap%2Fldapi