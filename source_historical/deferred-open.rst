Deferred open
    <namespace>0</namespace>
<last_edited>2006-05-11T09:36:37Z</last_edited>
<last_editor>Dralex</last_editor>

`smbd` has the interface of deferring `SMB` **open** operation - in the case when there is some problem of file opening (like shared violation, delaying for `oplock|oplocks`, etc). 

Deferred open information consists of two parts:
* local (internal `smbd`'s) deferred queue (see *smbd/process.c/deferred_queue*)
* the shared information in *locking.tdb*. It is a set of deferred open entries bound to each file - no more than one for every process and file pair.

Deferred open is initiated in *smbd/open.c/defer_open* (see `open_file_ntcreate`) that is called when **open** operation is delayed for a `oplock|opportunistic lock` or when a share violation occurs. So **open** `SMB` message is stored in deferred queue and will be processed when timeout will have come (see *smbd/process.c/receive_message_or_smb*).

Shared deferred open entry is removed when deferred open is activated (**open** message processed again) and when **open** call finishes successfully.

If file is closed, the process should walk through all shared deferred open entries (of this file) and notify openers (send OPEN_RETRY message to them) to retry the **open** operation.