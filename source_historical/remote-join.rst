Remote join
    <namespace>0</namespace>
<last_edited>2008-11-07T01:28:44Z</last_edited>
<last_editor>Gd</last_editor>

Since version 3.2 has the ability to remotely join workstations and servers into and unjoin from Windows domains.

The reverse direction is also implemented, Samba itself can be joined into or unjoined from a Windows domain.

Implementation 
------------------------

This features is implemented in various places:

* *net* command
* *libnetapi* library
* *smbd* server

Other tools that use libnetapi are available as well:

* gtk domainjoin gui
* libnetapi example code

Joining/Unjoining a remote computer using Samba 
------------------------

- Using net:

* net dom join <domain=DOMAIN> <ou=OU> <account=ACCOUNT> <password=PASSWORD> <reboot>
* net dom unjoin <account=ACCOUNT> <password=PASSWORD> <reboot>

In order to connect to the remote computer you need to use the -S or -I switch to define either name or IP-address of the remote machine and -U to set the username that will be used to connect to the machine.

- Using libnetapi:

In the shared libnetapi library you will find two calls that implement the remote join/join:omain and NetUnjoinDomain. The prototype for these calls can be found in the netapi.h header file.

For demonstration purpose we provide a gtk user interface, called netdomjoin-gui. It is not built by default, you can manually build it by calling "make -C lib/netapi/examples". This tool can be found at lib/netapi/bin/netdomjoin-gui then.

* netdomjoin-gui
* netdomjoin

Joining/Unjoining Samba from a remote computer 
------------------------

Microsoft has made this API available in the netdom join tool. Other third party tools might exist that use the same API.