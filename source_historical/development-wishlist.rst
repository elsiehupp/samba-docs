Development Wishlist
    <namespace>0</namespace>
<last_edited>2009-05-05T07:04:32Z</last_edited>
<last_editor>Vl</or>
==============================

verview
===============================

Below is a list of new features and work items that have been proposed for the Samba project.  This list is loosely prioritized with the more important items at the top.  Please feel free to add onto this list with requested features.

Ideally entries on this list will gradually be removed as they become implemented features within the Samba project.

===============================
Features
===============================

# SMB2 Implementation
## This is quite a high priority for us, as sooner or later Vista will be our major client.
# Restructure Common Code
## An important subproject of the internal restructuring is to make use of shared libraries. With Samba 3.2 we internally separated out libwbclient, libtalloc and libtdb and link them in as shared objects in all binaries. We will identify more such libraries and make them available to external projects.
## Re-work the RPC infrastructure. We will see external named pipe handlers, so that someone who wants to rewrite the SPOOLSS server does not have to understand or even link to the rest of smbd.
# Merge Samba 3 and Samba 4 Code Bases 
## Previously known as Franky, the continuing merge of the Samba 3 and Samba 4 code bases into a single Samba project.
## Split Samba's primary features (File Server, AD Server, Auth Server) into separately installable, configurable, and runnable entities.
## Much work has already been done via the Franky project.
# Scalable File Server
## Use less system resources as concurrent connections increase
## Provide better operation concurrency across many connections/
## Possible re-architecture involving async operations, threads, single process model.
# Clustered File Server
## CTDB project is underway and actively being developed
# Graphical Configuration Tools
## Work has been started to do graphical wizards for example to join a domain.  This task needs polishing and completing.
## The SWAT tool is basically orphaned as none of the core Samba developers is a good web developer.
# Encrypted File System
## This is a completely new area, but now that the Microsoft protocol documentation is available via [http://TTmicrosoft.com/enDDAASS/ry/cc216517D/, / should be possible to implement.
# Restructure winbind
## Based on the work on asynchronous libraries, winbind will be restructured to become a single, async process
## Infrastructure for async RPC calls is done in Samba 3, SMB connection setup is not completed yet.
## A truly async ldap client library is required. Options are Samba4's library or a new one based on liblber