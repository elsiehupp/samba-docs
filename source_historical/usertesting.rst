Samba4/UserTesting
    <namespace>0</namespace>
<last_edited>2010-01-23T18:13:44Z</last_edited>
<last_editor>Fraz</last_editor>

DRAFT for Andrew.

Samba4 is ready for user testing. 

There are a number of known issues that affect deployment. We want to make sure that these issues are fully resolved before asking for early adopters.
* MS AD controllers don't validate input from other AD's, and we've had bugs during development where bad data was sent. This makes the risk during our testing phase slightly higher: it could chew up your directory if connected to your network.
* XYZ app does not work. Bug # 12345

We want test feedback from users. The tests we'd like to see run are:

* Can samba do directory sync with your directory. Test this by splitting out a test AD controller, adding a samba4 controller with it, and syncing. Then reformat the test AD controller.

* Can custom AD using application 'foo' work - where 'foo' is chosen by you! Test this by setting up a synced AD controller as per the 'can we sync with your directory' test, and then test in that isolated environment.

* Can users login using Samba4 as a read-only replica. Not ready yet, but once it is we will be asking for early adopters as in a read-only configuration there is no risk of directory-eating behaviour.