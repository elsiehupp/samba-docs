Samba4/
    <namespace>0</namespace>
<last_edited>2009-10-20T02:08:40Z</last_edited>
<last_editor>Edewata</last_editor>

=================
Structures
=================

auth_usersupplied_info 
------------------------

.. code-block::

    struct auth_usersupplied_info {

    const char *workstation_name;
    struct socket_address *remote_host;

    uint32_t logon_parameters;

    bool mapped_state;
    /ues the client gives us *//
    struct {
        const char *account_name;
        const char *domain_name;
    } client, mapped;

    enum auth_password_state password_state;

    union {
        struct {
            DATA_BLOB lanman;
            DATA_BLOB nt;
        } response;
        struct {
            struct samr_Password *lanman;
            struct samr_Password *nt;
        } hash;
		
        char *plaintext;
    } password;

    uint32_t flags;
};
</

auth_operations 
------------------------

.. code-block::

    struct auth_operations {

    const char *name;

    /are using this interface, then you are probably
     * getting something wrong.  This interface is only for
     * security=server, and makes a number of compromises to allow
     * that.  It is not compatible with being a PDC.  */

    NTSTATUS (*get_challenge)(struct auth_method_context *ctx, TALLOC_CTX *mem_ctx, DATA_BLOB *challenge);

    /he user supplied info, check if this backend want to handle the password checking *//

    NTSTATUS (*want_check)(struct auth_method_context *ctx, TALLOC_CTX *mem_ctx,
        const struct auth_usersupplied_info *user_info);

    /he user supplied info, check a password *//

    NTSTATUS (*check_password)(struct auth_method_context *ctx, TALLOC_CTX *mem_ctx,
        const struct auth_usersupplied_info *user_info,
        struct auth_serversupplied_info **server_info);

    /a 'server info' return based only on the principal *//

    NTSTATUS (*get_server_info_principal)(TALLOC_CTX *mem_ctx, 
        struct auth_context *auth_context,
        const char *principal,
        struct auth_serversupplied_info **server_info);
};
</

auth_method_context 
------------------------

.. code-block::

    struct auth_method_context {
    struct auth_method_context *prev, *next;
    struct auth_context *auth_ctx;
    const struct auth_operations *ops;
    int depth;
    void *private_data;
};
</

auth_context 
------------------------

.. code-block::

    struct auth_context {

    struct {
        / this up in the first place? */ /
        const char *set_by;

        bool may_be_modified;

        DATA_BLOB data; 
    } challenge;

    /, in the order they should be called *//
    struct auth_method_context *methods;

    /nt context to use for calls that can block *//
    struct tevent_context *event_ctx;

    /saging context which can be used by backends *//
    struct messaging_context *msg_ctx;

    /m context *//
    struct loadparm_context *lp_ctx;

    NTSTATUS (*check_password)(struct auth_context *auth_ctx,
        TALLOC_CTX *mem_ctx,
        const struct auth_usersupplied_info *user_info, 
        struct auth_serversupplied_info **server_info);
	
    NTSTATUS (*get_challenge)(struct auth_context *auth_ctx, const uint8_t **_chal);

    bool (*challenge_may_be_modified)(struct auth_context *auth_ctx);

    NTSTATUS (*set_challenge)(struct auth_context *auth_ctx, const uint8_t chal[8], const char *set_by);
	
    NTSTATUS (*get_server_info_principal)(TALLOC_CTX *mem_ctx, 
        struct auth_context *auth_context,
        const char *principal,
        struct auth_serversupplied_info **server_info);
};
</