1.0: Configuring Samba
    <namespace>0</namespace>
<last_edited>2007-06-18T06:08:24Z</last_edited>
<last_editor>Asender</last_editor>

**`Replicated Failover Domain Controller and file server using LDAP`:**

`1.0. Configuring Samba`: 

`2.0. Configuring LDAP`: 

`3.0. Initialization LDAP Database`: 

`4.0. User Management`: 

`5.0. Heartbeat HA Configuration`: 

`6.0. DRBD`: 

`7.0. BIND DNS`:

`1.0. Configuring Samba`: 
--------------------------

Samba is an ambitious project to provide solutions for file & print sharing between NIX* Linux ™ and Microsoft Windows.

If you are familiar with Samba this document may give you some ideas of how you can bundle different software packages together to produce a very reliable configuration.

We are building a fault tolerant domain controller, which provides you with the following;

**Samba Configuration**

***- Primary Domain Controller***

***- Backup Domain Controller***

A master domain controller, that provides authentication through the use of LDAP

A slave domain controlle
r that can load balance client login requests which also provide redundancy through the use of a replica LDAP database.

**Step1.**

Get the latest version of samba http://us4.samba.org/samba/ftp/samba-latest.tar.gz

It is essential that bot
h the PDC and BDC are running the same version of samba.

     [root@node1 samba]# wget http://us4.samba.org/samba/ftp/samba-latest.tar.gz
 --19:28:04--  http://us4.samba.org/samba/ftp/samba-latest.tar.gz
                => `samba-latest.tar.gz'
 Resolving us4.samba.org... 192.48.170.15
 Connecting to us4.samba.org|192.48.170.15|:80... connected.
 HTTP request sent, awaiting response... 200 OK
 Length: 17,704,221 (17M) [application/x-tar]
 100%[====================================>] 17,704,221    53.01K/s    ETA 00:00
 19:33:40 (51.62 KB/s) - `samba-latest.tar.gz' saved [17704221/17704221]

**Step2.**

 [root@node1 samba]# tar zxvf samba-latest.tar.gz
 [root@node1 samba]# cd samba-3.0.23d/

Choose the appropriate distribution.

 [root@node1 samba-3.0.23d]# cd packaging/
 bin/      Example/  Mandrake/ RedHat-9/ SGI/      SuSE/
 Debian/   LSB/      README    RHEL/     Solaris/  sysv/

**Step3.**

This will take some time.

 [root@node1 samba-3.0.23d]# cd packaging/RHEL/
 [root@node1 RHEL]# ls
 makerpms.sh  makerpms.sh.tmpl  samba.spec  samba.spec.tmpl  setup

 [root@node1 RHEL]# chmod 777 makerpms.sh
 [root@node1 RHEL]# ./makerpms.sh
 Wrote: /usr/src/redhat/SRPMS/samba-3.0.23d-1.src.rpm
 Wrote: /usr/src/redhat/RPMS/i386/samba-3.0.23d-1.i386.rpm
 Wrote: /usr/src/redhat/RPMS/i386/samba-client-3.0.23d-1.i386.rpm
 Wrote: /usr/src/redhat/RPMS/i386/samba-common-3.0.23d-1.i386.rpm
 Wrote: /usr/src/redhat/RPMS/i386/samba-swat-3.0.23d-1.i386.rpm
 Wrote: /usr/src/redhat/RPMS/i386/samba-doc-3.0.23d-1.i386.rpm
 Wrote: /usr/src/redhat/
RPMS/i386/samba-debuginfo-3.0.23d-1.i386.rpm
 makerpms.sh: Done.
 [root@node1 RHEL]#

**Step4.**

Install the RPM files we built from source.

 [root@node2]# cd /usr/src/redhat/RPMS/i386/

 [root@node1 i386]# rpm -Uvh samba-3.0.23d-1.i386.rpm samba-client-3.0.23d-1.i386.rpm samba-common-3.0.23d-1.i386.rpm samba-debuginfo-3.0.23d-1.i386.rpm samba-doc-3.0.23d-1.i386.rpm samba- swat-3.0.23d-1.i386.rpm
 Preparing...               ########################################### [100%]
   1:samba-common           ########################################### [ 17%]
   2:samba                  ########################################### [ 33%]
   3:samba-client           ########################################### [ 50%]
   4:samba-debuginfo        ########################################### [ 67%]
   5:samba-doc              ########################################### [ 83%]
   6:samba-swat         
------------------------

####################### [100%]
 [root@node1 i386]#

**Step5.**

Login to node2 – the backup domain controller and repeat the above steps.

`1.1. smb.conf PDC`: 
------------------------

You will need to replace the high lightened parameters with your domain name. Take note of the use of failover ldap backbends; this is very useful. 

 [root@node1 ~]# mkdir /data
 [root@node1 ~]# vi /etc/samba/smb.conf

 # # Primary Domain Controller smb.conf

 # # Global parameters
 [global]
 unix charset = LOCALE
 workgroup = DDESIGN
 netbios name = node1
 #passdb backend = ldapsam:ldap://127.0.0.1
 #passdb backend = ldaps
am:"ldap://192.168.0.2 ldap://192.168.0.3"
 passdb backend =ldapsam:"ldap://node1.differentialdesign.org ldap://node2.differentialdesign.org"
 username map = /etc/samba/smbusers
 log level = 1
 syslog = 0
 log file = /var/log/samba/%m
 max log size = 0
 name resolve order = wins bcast hosts
 time server = Yes
 printcap name = CUPS
 add user script = /opt/IDEALX/sbin/smbldap-useradd -m '%u'
 delete user script = /opt/IDEALX/sbin/smbldap-userdel '%u'
 add group script = /opt/IDEALX/sbin/smbldap-groupadd -p '%g'
 delete group script = /opt/IDEALX/sbin/smbldap-groupdel '%g'
 add user to group script = /opt/IDEALX/sbin/smbldap-groupmod -m '%g' '%u'
 delete user from group script = /opt/IDEALX/sbin/smbldap-groupmod -x '%g' '%u'
 set primary group script = /opt/IDEALX/sbin/smbldap-usermod -g '%g' '%u'
 add machine script = /opt/IDEALX/sbin/smbldap-useradd -w '%u'
 shutdown script = /var/lib/samba/scripts/shutdown.sh
 abort shutdown script = /sbin/shutdown -c
 logon script = %u.bat
 #logon path = \\192.168.0.4\profiles\%u
 logon path = \\nodes.differentialdesign.org\profiles\%u
 logon drive = H:
 domain logons = Yes
 domain master = Yes

 wins support = Yes
 # peformance optimization all users stored in ldap
 ldapsam:trusted = yes
 ldap suffix = dc=differentialdesign,dc=org
 ldap machine suffix = ou=Computers,ou=Users
 ldap user suffix = ou=People,ou=Users
 ldap group suffix = ou=Groups
 ldap idmap suffix = ou=Idmap
 ldap admin dn = cn=sambaadmin,dc=differentialdesign,dc=org
 idmap backend = ldap://127.0.0.1
 idmap uid = 10000-20000
 idmap gid = 10000-20000
 printer admin = root
 printing = cups

 #========================Share Definitions====================

 [homes]
    omment = Home Directories
    alid users = %S

    rowseable = yes
    ritable = yes
    reate mask = 0600
    irectory mask = 0700

 [netlogon]
 comment = Network Logon Service
 path = /data/samba/netl
ogon
 writeable = yes
 browseable = yes
 read only = no

 [profiles]
 path = /data/samba/profiles
 writeable = yes
 browseable = no
------------------------

 create mode = 0777

 directory mode = 0777

 [Documents]
 comment = share to test samba
 path = /data/documents
 writeable = yes
 browseable = yes
 read only = no
 valid users = "@Domain Users"

`1.2. smb.conf BDC`: 
------------------------

 [root@node2 ~]# mkdir /
data
 [root@node2 ~]# vi /etc/samba/smb.conf

 # # Backup Domain Controller
 # # Global parameters

 [global]
 unix charset = LOCALE
 workgroup = DDESIGN
 netbios name = node2
 #passdb backend = ldapsam:ldap://127.0.0.1
 #passdb backend = ldaps
am:"ldap://192.168.0.2 ldap://192.168.0.3"
 passdb backend = ldapsam:"ldap://node2.differentialdesign.org ldap://node1.differentialdesign.org"
 username map = /etc/samba/smbusers
 log level = 1
 syslog = 0
 log file = /var/log/samba/%m
 max log size = 50
 name resolve order = wins bcast hosts
 printcap name = CUPS

 show add printer wizard = No
 logon script = %u.bat
 #logon path = \\192.168.0.4\profiles\%u
 logon path = \\nodes.differentialdesign.org\profiles\%u
 logon drive = H:

 domain logons = Yes
 os level = 63
 domain master = No
 wins server = node1.differentialdesign.org
 ldap suffix = dc=differentialdesign,dc=org
 ldap machine suffix = ou=Computers,ou=Users
 ldap user suffix = ou=People,ou=Users
 ldap group suffix = ou=Groups
 ldap idmap suffix = ou=Idmap
 ldap admin dn = cn=sambaadmin,dc=differentialdesign,dc=org
 utmp = Yes
 idmap backend = ldap://node1.differentialdesign.org
 idmap uid = 10000-20000
 idmap gid = 10000-20000
 printing = cups

==============

Share Definitions
==============

 [homes]
 comment = Home Directories
 valid users = %S
 browseable = yes
 writable = yes
 create mask = 0600

 directory mask = 0700

 [netlogon]
 comment = Network Logon
 Service
 path = /data/samba/netlogon
------------------------

 browseable = yes
 read only = no

 [profiles]
 path = /data/samba/profiles
 writeable = yes
 browseable = no
 read only = no
 create mode = 0777
 directory mode = 0777

 [Documents]
 comment = share to test samba
 path = /data/documents
 writeable = yes
 browseable = yes
 read only = no
 valid users = "@Domain Users"

`1.3. /etc/hosts`: 

------------------------

In order to correctly resolve name to IP address we need some sort of name resolution. We already have a DNS name server which is capable of doing this as per section `7.0. BIND DNS`:; however it is desirable to have a backup feature such as entries in the /etc/hosts file. 

**Step1.** 

On node1 we will edit the hosts file to reflect our configuration. 

------------------------

/hosts

 # Do not remove the following line, or various programs
 # that require network functionality will fail.
 127.0.0.1       node1   localhost.localdomain   localhost
 192.168.0.2     node1.differentialdesign.org
 192.168.0.3     node2.differentialdesign.org
 192.168.0.4     nodes.differentialdesign.org

**Step2.** 

Login to node2 and edit the /etc/hosts file. 

 [root@node2 ~]# vi /etc/hosts

 # Do not remove the following line, or various programs
 # that require network functionality will fail.
 127.0.0.1       node2   localhost.localdomain   localhost
 192.168.0.2     node1.differentialdesign.org
 192.168.0.3     node2.differentialdesign.org
 192.168.0.4     nodes.differentialdesign.org

`1.4. Samba Security`: 
------------------------

There are many additional features we can add to Samba to make it more secure. We can add some additional comments to our smb.conf to achieve this. 

One of the great features of Samba is the “host allow =” option. This can be applied on a global scale to all the shares in the smb.conf by placing the global section of the smb.conf or to specific shares, but not both. 

The example limits access to Samba shares to clients on the 192.168.0.0/24 network as it is defined it in the glocal section of the smb.conf. 

 ## /etc/samba/smb.conf

 ## Global parameters

------------------------

 workgroup = DDESIGN
 security = user
 hosts allow = 192.168.0.0/24

For additional network share security, we can use this option on a per share basis, giving us greater flexability. 

This limits access to this share to the client with the 192.168.0.100/24 IP address; you of course can use multiple addresses and subnets. 

 ## /etc/samba/smb.conf
     ## ==== Share Definitions
------------------------ 
 [Documents]
 comment = share to test samba
 path = /data/documents
 writeable = yes
 browseable = yes
 read only = no
 valid users = "@Domain Users"
 hosts allow = 192.168.0.100/24

`1.5. Filesystem ACLs`: 
------------------------

In many cases Linux users and group permissions are sufficient for small workgroups, using ACL's we can extend permissions on directories and files without adding users to additional groups.