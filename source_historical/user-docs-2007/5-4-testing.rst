5.4 Testing
    <namespace>0</namespace>
<last_edited>2007-01-25T13:49:48Z</last_edited>
<last_editor>Asender</last_editor>

`1.0:ing Samba` 

`2.0:ing LDAP` 

`3.0:zation LDAP Database` 

`4.0:agement` 

`5.0:t HA Configuration` 

`6.0:

`7.0:`

----

<u>**Table of Contents**</u>

`5.1 Requirements` 

`5.2 Installation` 

`5.3 Configuration` 

`5.3.1 ha.cf` 

`5.3.2 haresources` 

`5.3.3 authkeys` 

`5.4 Testing`