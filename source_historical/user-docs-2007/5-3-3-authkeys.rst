5.3.3 authkeys
    <namespace>0</namespace>
<last_edited>2007-01-25T13:49:38Z</last_edited>
<last_editor>Asender</last_editor>

`1.0: Configuring Samba` 

`2.0: Configuring LDAP` 

`3.0: Initialization LDAP Database` 

`4.0: User Management` 

`5.0: Heartbeat HA Configuration` 

`6.0: DRBD` 

`7.0: BIND DNS`

----

<u>**Table of Contents**</u>

`5.1 Requirements` 

`5.2 Installation` 

`5.3 Configuration` 

`5.3.1 ha.cf` 

`5.3.2 haresources` 

`5.3.3 authkeys` 

`5.4 Testing` 

The below method provides no security or authentication, so we recommended not to use. If however heartbeat communicates over a private link such as in our case (serial and crossover cable) there is no need to add this additional security. 

**Step1**

 [root@node1]# vi authkeys

 ## /etc/ha.d/authkeys
 auth 1
 1 crc

The preferred method is to sha encryption to authenticate nodes and their packets as below.

 ## /etc/ha.d/authkeys
 auth 1
 1 sha HeartbeatPassword

**Step2**

Give the authkeys file correct permissions.

 [root@node1]# chmod 600 /etc/ha.d/authkeys

**Step3**

Copy the authkeys file to node2 so they can authenticate with each other.

 [root@node1]# scp /etc/ha.d/authkeys root@node2:/etc/ha.d/

**Step4**

Login to node2 – your backup domain controller, use the exact same configuration as the primary domain controllers configuration files for heartbeat.