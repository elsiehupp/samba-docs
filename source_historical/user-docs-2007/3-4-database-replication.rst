3.4 Database Replication
    <namespace>0</namespace>
<last_edited>2007-01-25T13:43:18Z</last_edited>
<last_editor>Asender</last_editor>

`1.0: Configuring Samba` 

`2.0: Configuring LDAP` 

`3.0: Initialization LDAP Database` 

`4.0: User Management` 

`5.0: Heartbeat HA Configuration` 

`6.0: DRBD` 

`7.0: BIND DNS`

----

<u>**Table of Contents**</u>

`3.1 Provisioning Database` 

`3.2 Preload LDIF` 

`3.3 LDAP Population` 

`3.4 Database Replication`

If we choose to use syncrepl instead of slurpd daemon as per sections 2.2.1 slapd.conf Slave Synrepl and 2.2.1.1 slapd.conf Slave delta-syncrepl 4 Openldap2.3  there is no need to do this section, the database will be copied across initially when the consumer requests is restarted. 

**Step1.** 

Dump the LDAP database, copy it across to node2.

 [root@node1 ~]# slapcat –b “dc=differentialdesign,dc=org” -v -l transfer.ldif

 # id=00000001
 # id=00000002
 # id=00000003
 # id=00000004
 # id=00000005
 # id=00000006
 # id=00000007
 # id=00000008
 # id=00000009
 # id=0000000a
 # id=0000000b
 # id=0000000c
 # id=0000000d
 # id=0000000e
 # id=0000000f
 # id=00000010
 # id=00000011
 # id=00000012
 # id=00000013
 # id=00000014
 # id=00000015
 # id=00000017
 # id=00000018
     [root@node1 ~]# scp transfer.ldif root@node2:/root/

**Step2.**

Transfer the database to node2.

 [root@node2 ~]# slapadd –b “dc=differentialdesign,dc=org” -v -l transfer.ldif

 added: "dc=differentialdesign,dc=org" (00000001)
 added: "cn=Manager,dc=differentialdesign,dc=org" (00000002)
 added: "cn=syncuser,dc=differentialdesign,dc=org" (00000003)
 added: "cn=sambaadmin,dc=differentialdesign,dc=org" (00000004)
 added: "cn=mailadmin,dc=differentialdesign,dc=org" (00000005)
 added: "ou=Users,dc=differentialdesign,dc=org" (00000006)
 added: "ou=People,ou=Users,dc=differentialdesign,dc=org" (00000007)
 added: "ou=Computers,ou=Users,dc=differentialdesign,dc=org" (00000008)
 added: "ou=Groups,dc=differentialdesign,dc=org" (00000009)
 added: "ou=Domains,dc=differentialdesign,dc=org" (0000000a)
 added: "sambaDomainName=DDESIGN,ou=Domains,dc=differentialdesign,dc=org" (0000000b)
 added: "cn=Domain Admins,ou=Groups,dc=differentialdesign,dc=org" (0000000c)
 added: "cn=Domain Users,ou=Groups,dc=differentialdesign,dc=org" (0000000d)
 added: "cn=Domain Guests,ou=Groups,dc=differentialdesign,dc=org" (0000000e)
 added: "cn=Domain Computers,ou=Groups,dc=differentialdesign,dc=org" (000000f)
 added: "cn=Administrators,ou=Groups,dc=differentialdesign,dc=org" (00000010)
 added: "cn=Account Operators,ou=Groups,dc=differentialdesign,dc=org" (00000011)
 added: "cn=Print Operators,ou=Groups,dc=differentialdesign,dc=org" (00000012)
 added: "cn=Backup Operators,ou=Groups,dc=differentialdesign,dc=org" (00000013)
 added: "cn=Replicators,ou=Groups,dc=differentialdesign,dc=org" (00000014)
 added: "uid=root,ou=People,ou=Users,dc=differentialdesign,dc=org" (00000015)
 added: "uid=asender,ou=People,ou=Users,dc=differentialdesign,dc=org" (00000016)

**Step3.**

Make sure LDAP database is owned by LDAP

 [root@node2 ~]# chown –R ldap.ldap /var/lib/ldap 

**Step4.**

 [root@node1 ~]# service ldap restart
 Stopping slapd:                                      [  OK  ]
 Stopping slurpd:                                     [  OK  ]
 Checking configuration files for slapd:  config file testing succeeded
                                                      [  OK  ]
 Starting slapd:                                      [  OK  ]
 Starting slurpd:                                     [  OK  ]

 [root@node1 ~]# service smb restart
 Shutting down SMB services:                          [  OK  ]
 Shutting down NMB services:                          [  OK  ]
 Starting SMB services:                               [  OK  ]
 Starting NMB services:                               [  OK  ]

**Step5.**

Login to node1 or your Primary Domain Controller and add another user as done so in section 3.6 LDAP population Step5, we will then check replication by logging onto node2 and see if the user exists on that machine.

 [root@node1 sbin]# ./smbldap-useradd -m -a testuser
 [root@node1 sbin]# ./smbldap-passwd testuser
 Changing password for testuser
 New password :
 Retype new password :

 [root@node1 sbin]# smbpasswd testuser
 New SMB password:
 Retype new SMB password:

 [root@node1 sbin]# ssh node2
 root@node2's password:
 Last login: Mon Dec 18 02:43:33 2006 from 192.168.0.2

 [root@node2 ~]# id testuser
 uid=1009(testuser) gid=513(Domain Users) groups=513(Domain Users)