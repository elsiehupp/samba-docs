Overview
    <namespace>0</namespace>
<last_edited>2009-11-23T15:28:38Z</last_edited>
<last_editor>Asender</last_editor>

**`Replicated Failover Domain Controller and file server using LDAP`**

`1.0. Configuring Samba` 

`2.0. Configuring LDAP` 

`3.0. Initialization LDAP Database` 

`4.0. User Management` 

`5.0. Heartbeat HA Configuration` 

`6.0. DRBD` 

`7.0. BIND DNS`

----

I created this wiki with the intention of eventually creating a book, however most of this has just been written on the fly with little or no reviews. 

There are so many high spec machines sit at idle, you can really do quite allot with 2 servers creating a small cluster to support many different applications and even virtualization for an entire infrastructure.

We will be configuring a 2 node cluster using Samba and Openldap to provide windows domain authentication. Heartbeat has really been phased out and no longer maintained; I prefer to use ctdb which can provide us with ip failover and many other features.

Most of us are familiar with some form of RAID; we will be using DRBB software RAID1 over LAN to provide real time data replication, it replicates the data on a block level; if a failure occurs on node1 or it becomes unresponsive resources will be migrated to node2 and the DRBD drive mounted. In newer versions of drbd 8.0 we cam put a clustered file system on top to create a SAN like environment which is far more tolerant then a SAN afaik. 

We should start with 2 identical machines each with 2 hard drives. One of these drives will be used for the operating system; the other is our DRBD RAID1 over LAN drive. 

By today’s standards anything in the Pentium 4 range and above will suit, but you should try to get a VT enabled cpu. Operating system drive should be no less then approximately 40GB, the DRBD replication drive should be approximately 300GB each - SATA and SCSI are also fine. DRBD 8.3 and above can address and replicate data storage up to 16TB, previous versions are limited to 4TB.

Once familiar with this kind of configuration you can easily take one node offline to upgrade additional storage or any hardware requirements without users suffering.

High Availability and data replication should not replace traditional backups such as tape and external media devices, especially if you are using this configuration and are not familiar with the workings.