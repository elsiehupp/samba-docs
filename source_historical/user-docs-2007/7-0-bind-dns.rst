7.0: BIND DNS
    <namespace>0</namespace>
<last_edited>2007-02-26T01:44:05Z</last_edited>
<last_editor>WhitÐµcraig</last_editor>

**`Replicated Failover Domain Controller and file server using LDAP`**

`1.0. Configuring Samba` 

`2.0. Configuring LDAP` 

`3.0. Initialization LDAP Database` 

`4.0. User Management` 

`5.0. Heartbeat HA Configuration` 

`6.0. DRBD` 

`7.0. BIND DNS`

----

`7.0. BIND DNS` 
------------------------

We can use BIND – The Berkley Internet Name Domain in a high availability configuration. We can make 2 nodes appear as one, zone files will we stored on a DRBD drive, if node1 fails node2 can take over and automatically start NAMED.

BIND is able to have its /var/named directory relocated to a more appropriate location such as /data/dnszones; this enables us to provide real time replication of the zone files; the standby node2 will have to have its default directory modified to /data/dnszones. 

We have 2 servers, and we will refer to the cluster as cluster.differentialdesign.org. It is assumed that these machines are behind a firewall with NAT and port forwarding to the appropriate ports.

When setting up Domain Names through a registrar you would want 2 separate name servers. It is recommended to setup an additional slave DNS server.

An example may be

**Name Server:CLUSTER.DIFFERENTIALDESIGN.ORG <- Primary Name Server(s)**

**Name Server:NS1.DIFFERENTIALDESIGN.ORG**

**Name Server:NS2.DIFFERENTIALDESIGN.ORG**

`7.1. Configuration` 
------------------------

**Step1.**

We will now create a directory on our DRBD drive /data/dnszones.

 [root@node1 ~]# mkdir /data/dnszones

**Step2.**

Change the location of the zone files to our replicated drive

 [root@node1 ~]# named ?
 usage: named [-4|-6] [-c conffile] [-d debuglevel] [-f|-g] [-n number_of_cpus]
             [-p port] [-s] [-t chrootdir] [-u username]
             [-m {usage|trace|record}]
             [-D ]
 named: extra command line arguments

 [root@node1 ~]# named -t /data/dnszones/

**Step3.**

Copy the default zone files to our new location and set the permissions.

 [root@node1 ~]# rsync -avz /var/named/ /data/dnszones/

 [root@node1 ~]# chown –R named.named /data/dnszones/

`7.1.1. named.conf` 
------------------------

It is important that all machines on the network use cluster.differentialdesign.org or its local IP address address as DNS servers. This way we can assure correct name resolution.

We will now edit the /etc/named.conf 

Take note of the below file, you can see highlighted in red our secondary DNS servers, these are the IP addresses of ns1.differentialdesign.org and ns2.differentialdesign.org

The named.conf needs to be the same on both node1 and node2; you could manually copy the file over using SCP, or link it to the /data/dnszones directory using a symbolic link.

 [root@node1 ~]# vi /etc/named.conf

 //
 // named.conf for Red Hat caching-nameserver
 //

 options {
        directory "/data/dnszones";
        dump-file "/data/dnszones/data/cache_dump.db";
        statistics-file "/data/dnszones/data/named_stats.txt";
        /*
         * If there is a firewall between you and nameservers you want
         * to talk to, you might need to uncomment the query-source
         * directive below.  Previous versions of BIND always asked
         * questions using port 53, but BIND 8.1 uses an unprivileged
         * port by default.
         */
         // query-source address * port 53;

        allow-transfer {
                127.0.0.1;              // localhost
                202.161.90.250;               // secondary DNS server for my zone
                202.161.90.251;               // secondary DNS server for my zone
          };

 };

 //
 // a caching only nameserver config
 //
 controls {
        inet 127.0.0.1 allow { localhost; } keys { rndckey; };
 };

 zone "." IN {
        type hint;
        file "named.ca";
 };

 zone "localdomain" IN {
        type master;
        file "localdomain.zone";
        allow-update { none; };
 };

 zone "localhost" IN {
        type master;
        file "localhost.zone";
        allow-update { none; };
 };

 zone "0.0.127.in-addr.arpa" IN {
        type master;
        file "named.local";
        allow-update { none; };
 };

 zone "0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa" IN {
        type master;
        file "named.ip6.local";
        allow-update { none; };
 };

 zone "255.in-addr.arpa" IN {
        type master;
        file "named.broadcast";
        allow-update { none; };
 };

 zone "0.in-addr.arpa" IN {
        type master;
        file "named.zero";
        allow-update { none; };
 };

 zone "differentialdesign.org" {
        type master;
        file "/data/dnszones/differentialdesign.org/named.differentialdesign.org.hosts";
        allow-update { none; };
 };

`7.1.2. zone file` 
------------------------

In our named.conf file we have the following zone defined;

 zone "differentialdesign.org" {
        type master;
        file "/data/dnszones/differentialdesign.org/named.differentialdesign.org.hosts";
        allow-update { none; };

We can see the zone file located in /data/dnszones/

**Step1.**

Create a sub folder where we will store our zone files.

 [root@node1 ~]# mkdir /data/dnszones/differentialdesign.org/

**Step2.**

Create a new file called named.differentialdesign.org.hosts.

 [root@node1 ~]# vi /data/dnszones/differentialdesign.org/named.differentialdesign.org.hosts

You will see below that nodes.differentialdesign.org. IN 192.168.0.4 is an “A record” which points us to the virtual IP address of the cluster. When setting up mapped drives it is best to use the name instead of IP address.

 $TTL 8h
 differentialdesign.org.    IN      SOA     cluster.differentialdesign.org.  asender.mail.samba.org. (
                        2006211201
                        10800
                        3600
                        3600000
                        86400 )
 differentialdesign.org.            IN      NS               cluster.differentialdesign.org.
 differentialdesign.org.            IN      NS               ns1.differentialdesign.org.
 differentialdesign.org.            IN      NS               ns2.differentialdesign.org.
 differentialdesign.org.            IN      MX      10    mail.differentialdesign.org.
 mail.differentialdesign.org.       IN      A             202.161.90.245
 www.differentialdesign.org.        IN      A             202.161.90.245
 cluster.differentialdesign.org.    IN      A             202.161.90.241
 node1.differentialdesign.org.      IN      A             192.168.0.2
 node2.differentialdesign.org.      IN      A             192.168.0.3
 nodes.differentialdesign.org.      IN      A             192.168.0.4