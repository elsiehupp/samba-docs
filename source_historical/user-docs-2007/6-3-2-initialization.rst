6.3.2 Initialization
    <namespace>0</namespace>
<last_edited>2007-01-25T13:52:57Z</last_edited>
<last_editor>Asender</last_editor>

`1.0: Configuring Samba` 

`2.0: Configuring LDAP` 

`3.0: Initialization LDAP Database` 

`4.0: User Management` 

`5.0: Heartbeat HA Configuration` 

`6.0: DRBD` 

`7.0: BIND DNS`

----

<u>**Table of Contents**</u>

`6.1 Requirements` 

`6.2 Installation` 

`6.3 Configuration` 

`6.3.1 drbd.conf` 

`6.3.2 Initialization` 

`6.4 Testing` 

In the following steps we will configure the disks to synchronize and choose a master node.

**Step1**

On the Primary Domain Controller

 [root@node1]# service drbd start

On the Backup Domain Controller

 [root@node2]# service drbd start

**Step2**

 [root@node1]# service drbd status
 drbd driver loaded OK; device status:
 version: 0.7.17 (api:77/proto:74)
 SVN Revision: 2093 build by root@node1, 2006-04-23 14:40:20
 0: cs:Connected st:Secondary/Secondary ld:Inconsistent
    ns:25127936 nr:3416 dw:23988760 dr:4936449 al:19624 bm:1038 lo:0 pe:0 ua:0 ap:0

You can see both devices are ready, and waiting for a Primary drive to be activated which will do an initial synchronization to the secondary device.

**Step3**

Stop the heartbeat service on both nodes.

**Step4**

We are now telling DRBD to make node1 the primary drive.

 [root@node1]#  drbdadm -- --do-what-I-say primary all

 [root@node1 ~]# service drbd status
 drbd driver loaded OK; device status:
 version: 0.7.23 (api:79/proto:74)
 SVN Revision: 2686 build by root@node1, 2007-01-23 20:26:13
 0: cs:SyncSource st:Primary/Secondary ld:Consistent
    ns:67080 nr:85492 dw:91804 dr:72139 al:9 bm:268 lo:0 pe:30 ua:2019 ap:0
        [==>.................] sync'ed: 12.5% (458848/520196)K
        finish: 0:01:44 speed: 4,356 (4,088) K/sec

**Step5**

Create a filesystem on our RAID devices.

 [root@node1]# mkfs.ext3 /dev/drbd0