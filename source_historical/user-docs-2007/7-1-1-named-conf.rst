7.1.1 named.conf
    <namespace>0</namespace>
<last_edited>2007-01-25T13:55:17Z</last_edited>
<last_editor>Asender</last_editor>

`1.0: Configuring Samba` 

`2.0: Configuring LDAP` 

`3.0: Initialization LDAP Database` 

`4.0: User Management` 

`5.0: Heartbeat HA Configuration` 

`6.0: DRBD` 

`7.0: BIND DNS`

----

<u>**Table of Contents**</u>

`7.1 Configuration` 

`7.1.1 named.conf` 

`7.1.2 zone file` 

It is important that all machines on the network use cluster.differentialdesign.org or its local IP address address as DNS servers. This way we can assure correct name resolution.

We will now edit the /etc/named.conf 

Take note of the below file, you can see highlighted in red our secondary DNS servers, these are the IP addresses of ns1.differentialdesign.org and ns2.differentialdesign.org

The named.conf needs to be the same on both node1 and node2; you could manually copy the file over using SCP, or link it to the /data/dnszones directory using a symbolic link.

 [root@node1 ~]# vi /etc/named.conf

 //
 // named.conf for Red Hat caching-nameserver
 //

 options {
        directory "/data/dnszones";
        dump-file "/data/dnszones/data/cache_dump.db";
        statistics-file "/data/dnszones/data/named_stats.txt";
        /*
         * If there is a firewall between you and nameservers you want
         * to talk to, you might need to uncomment the query-source
         * directive below.  Previous versions of BIND always asked
         * questions using port 53, but BIND 8.1 uses an unprivileged
         * port by default.
         */
         // query-source address * port 53;

        allow-transfer {
                127.0.0.1;              // localhost
                202.161.90.250;               // secondary DNS server for my zone
                202.161.90.251;               // secondary DNS server for my zone
          };

 };

 //
 // a caching only nameserver config
 //
 controls {
        inet 127.0.0.1 allow { localhost; } keys { rndckey; };
 };

 zone "." IN {
        type hint;
        file "named.ca";
 };

 zone "localdomain" IN {
        type master;
        file "localdomain.zone";
        allow-update { none; };
 };

 zone "localhost" IN {
        type master;
        file "localhost.zone";
        allow-update { none; };
 };

 zone "0.0.127.in-addr.arpa" IN {
        type master;
        file "named.local";
        allow-update { none; };
 };

 zone "0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa" IN {
        type master;
        file "named.ip6.local";
        allow-update { none; };
 };

 zone "255.in-addr.arpa" IN {
        type master;
        file "named.broadcast";
        allow-update { none; };
 };

 zone "0.in-addr.arpa" IN {
        type master;
        file "named.zero";
        allow-update { none; };
 };

 zone "differentialdesign.org" {
        type master;
        file "/data/dnszones/differentialdesign.org/named.differentialdesign.org.hosts";
        allow-update { none; };
 };