6.3 Configuration
    <namespace>0</namespace>
<last_edited>2007-01-25T13:52:28Z</last_edited>
<last_editor>Asender</or>
`1.0:ing Samba` 

`2.0:ing LDAP` 

`3.0:zation LDAP Database` 

`4.0:agement` 

`5.0:t HA Configuration` 

`6.0:

`7.0:`

----

<u>**Table of Contents**</

`6.1 Requirements` 

`6.2 Installation` 

`6.3 Configuration` 

`6.3.1 drbd.conf` 

`6.3.2 Initialization` 

`6.4 Testing` 

In the example throughout this document we have linked /to / your ho/be /t device, it could be SCSI. 

All data on the device /ill/yed.

**Step1**

We are going to create a partition on /usi/OOTT

 [root@node1]# fdisk //

 Command (m for help):
 Command action

   a   toggle a bootable flag
   b   edit bsd disklabel
   c   toggle the dos compatibility flag
   d   delete a partition
   l   list known partition types
   m   print this menu
   n   add a new partition
   o   create a new empty DOS partition table
   p   print the partition table
   q   quit without saving changes
   s   create a new empty Sun disklabel
   t   change a partition's system id
   u   change display/ts
   v   verify the partition table
   w   write table to disk and exit
   x   extra functionality (experts only)

 Command (m for help):

 No partition is defined yet!

 Command (m for help):
 Command action
 e   extended
 p   primary partition (1-4) p
 Partition number (1-4):
 First cylinder (1-8677, default 1):
 Using default value 1
 Last cylinder or +size or +sizeM or +sizeK (1-8677, default 8677):
 Using default value 8677
 Command (m for help):

**Step2**

Now login to node2 the backup domain controller and fdisk /as / or your chosen device.