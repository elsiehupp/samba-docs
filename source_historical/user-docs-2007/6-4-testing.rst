6.4 Testing
    <namespace>0</namespace>
<last_edited>2007-01-25T13:53:48Z</last_edited>
<last_editor>Asender</last_editor>

`1.0: Configuring Samba` 

`2.0: Configuring LDAP` 

`3.0: Initialization LDAP Database` 

`4.0: User Management` 

`5.0: Heartbeat HA Configuration` 

`6.0: DRBD` 

`7.0: BIND DNS`

----

<u>**Table of Contents**</u>

`6.1 Requirements` 

`6.2 Installation` 

`6.3 Configuration` 

`6.3.1 drbd.conf` 

`6.3.2 Initialization` 

`6.4 Testing` 

We have a 2 node cluster replicating data, its time to test a failover. 

**Step1**

Start the heartbeat service on both nodes.

**Step2**

On node1 we can see the status of DRBD.

 [root@node1 ~]# service drbd status
 drbd driver loaded OK; device status:
 version: 0.7.23 (api:79/proto:74)
 0: cs:Connected st:Primary/Secondary ld:Consistent
    ns:1536 nr:0 dw:1372 dr:801 al:4 bm:6 lo:0 pe:0 ua:0 ap:0
 [root@node1 ~]#

On node2 we can see the status of DRBD.

 [root@node2 ~]# service drbd status
 drbd driver loaded OK; device status:
 version: 0.7.23 (api:79/proto:74)
 SVN Revision: 2686 build by root@node2, 2007-01-23 20:26:03
 0: cs:Connected st:Secondary/Primary ld:Consistent
    ns:0 nr:1484 dw:1484 dr:0 al:0 bm:6 lo:0 pe:0 ua:0 ap:0
 [root@node2 ~]#

That all looks good; we can see the devices are consistent and ready for use.

**Step3**

Now let’s check the mount point we created in the heartbeat haresources file.

We can see heartbeat has successfully mounted “/dev/drbd0 to the /data directory” of course your device will not have any data on it yet.

 [root@node1 ~]# df -h
 Filesystem            Size  Used Avail Use% Mounted on
 /dev/mapper/VolGroup00-LogVol00
                       35G   14G   20G  41% /
 /dev/hdc1              99M   21M   74M  22% /boot
 /dev/shm              506M     0  506M   0% /dev/shm
 /dev/drbd0             74G   37G   33G  53% /data
 [root@node1 ~]#

**Step4**

Login to node1 and execute the following command; once heartbeat is stopped it should only take a few seconds to migrate the services to node2.

 [root@node1 ~]# service heartbeat stop
 Stopping High-Availability services:
                                          [  OK  ]

 [root@node1 ~]# service drbd status
 drbd driver loaded OK; device status:
 version: 0.7.23 (api:79/proto:74)
 SVN Revision: 2686 build by root@node1, 2007-01-23 20:26:13
 0: cs:Connected st:Secondary/Primary ld:Consistent
    ns:5616 nr:85492 dw:90944 dr:2162 al:9 bm:260 lo:0 pe:0 ua:0 ap:0

We can see drbd change state to secondary on node1.

**Step5**

Now let’s check that status of DRBD on node2; we can see it has changed state and become the primary.

 [root@node2 ~]# service drbd status
 drbd driver loaded OK; device status:
 version: 0.7.23 (api:79/proto:74)
    VN Revision: 2686 build by root@node2, 2007-01-23 20:26:03
 0: cs:Connected st:Primary/Secondary ld:Consistent
    ns:4 nr:518132 dw:518136 dr:17 al:0 bm:220 lo:0 pe:0 ua:0 ap:0
 1: cs:Connected st:Primary/Secondary ld:Consistent
    ns:28 nr:520252 dw:520280 dr:85 al:0 bm:199 lo:0 pe:0 ua:0 ap:0

Check that node2 has mounted the device.

 [root@node2 ~]# df -h
 Filesystem            Size  Used Avail Use% Mounted on
 /dev/mapper/VolGroup00-LogVol00
                       35G   12G   22G  35% /
 /dev/hdc1              99M   17M   78M  18% /boot
 /dev/shm              506M     0  506M   0% /dev/shm
 /dev/hdh1             111G   97G  7.6G  93% /storage
 /dev/drbd0             74G   37G   33G  53% /data
 [root@node2 ~]#

**Step5**

Finally start the heartbeat service on node1 and be sure that all processes migrate back.