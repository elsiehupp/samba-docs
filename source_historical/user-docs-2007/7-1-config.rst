7.1 Configuration
    <namespace>0</namespace>
<last_edited>2007-01-25T13:55:04Z</last_edited>
<last_editor>Asender</last_editor>

`1.0: Configuring Samba` 

`2.0: Configuring LDAP` 

`3.0: Initialization LDAP Database` 

`4.0: User Management` 

`5.0: Heartbeat HA Configuration` 

`6.0: DRBD` 

`7.0: BIND DNS`

----

<u>**Table of Contents**</u>

`7.1 Configuration` 

`7.1.1 named.conf` 

`7.1.2 zone file` 

**Step1**

We will now create a directory on our DRBD drive /data/dnszones.

 [root@node1 ~]# mkdir /data/dnszones

**Step2**

Change the location of the zone files to our replicated drive

 [root@node1 ~]# named ?
 usage: named [-4|-6] [-c conffile] [-d debuglevel] [-f|-g] [-n number_of_cpus]
             [-p port] [-s] [-t chrootdir] [-u username]
             [-m {usage|trace|record}]
             [-D ]
 named: extra command line arguments

 [root@node1 ~]# named -t /data/dnszones/

**Step3**

Copy the default zone files to our new location and set the permissions.

 [root@node1 ~]# rsync -avz /var/named/ /data/dnszones/

 [root@node1 ~]# chown –R named.named /data/dnszones/