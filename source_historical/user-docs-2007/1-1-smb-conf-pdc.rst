1.1 smb.conf PDC
    <namespace>0</namespace>
<last_edited>2007-02-26T03:06:49Z</last_edited>
<last_editor>Jerry</last_editor>


`1.0: Configuring Samba`: 

`2.0: Configuring LDAP`: 

`3.0: Initialization LDAP Database`: 

`4.0: User Management`: 

`5.0: Heartbeat HA Configuration`: 

`6.0: DRBD`: 

`7.0: BIND DNS`:

----

============

Table of Contents
============

`1.1 smb.conf PDC`: 

`1.2 smb.conf BDC`: 

`1.3 /etc/hosts`: 

`1.4 Samba Security`:

You will need to replace the high lightened parameters with your domain name. Take note of the use of failover ldap backbends; this is very useful.

 [root@node2 ~]# mkdir /data
 [root@node1 ~]# vi /etc/samba/smb.conf

 # # Primary Domain Controller smb.conf

 # # Global parameters
 [global]
 unix charset = LOCALE
 workgroup = DDESIGN
 netbios name = node1
 #passdb backend = ldapsam:ldap://127.0.0.1
 #passdb backend = ldapsam:"ldap://192.168.0.2 ldap://192.168.0.3"
 passdb backend =ldapsam:"ldap://node1.differentialdesign.org ldap://node2.differentialdesign.org"
 username map = /etc/samba/smbusers
 log level = 1
 syslog = 0
 log file = /var/log/samba/%m
 max log size = 0
 name resolve order = wins bcast hosts
 time server = Yes
 printcap name = CUPS
 add user script = /opt/IDEALX/sbin/smbldap-useradd -m '%u'
 delete user script = /opt/IDEALX/sbin/smbldap-userdel '%u'
 add group script = /opt/IDEALX/sbin/smbldap-groupadd -p '%g'
 delete group script = /opt/IDEALX/sbin/smbldap-groupdel '%g'
 add user to group script = /opt/IDEALX/sbin/smbldap-groupmod -m '%g' '%u'
 delete user from group script = /opt/IDEALX/sbin/smbldap-groupmod -x '%g' '%u'
 set primary group script = /opt/IDEALX/sbin/smbldap-usermod -g '%g' '%u'
 add machine script = /opt/IDEALX/sbin/smbldap-useradd -w '%u'
 shutdown script = /var/lib/samba/scripts/shutdown.sh
 abort shutdown script = /sbin/shutdown -c
 logon script = %u.bat
 #logon path = \\192.168.0.4\profiles\%u
 logon path = \\nodes.differentialdesign.org\profiles\%u
 logon drive = H:
 domain logons = Yes
 domain master = Yes
 wins support = Yes
 ldap suffix = dc=differentialdesign,dc=org
 ldap machine suffix = ou=Computers,ou=Users
 ldap user suffix = ou=People,ou=Users
 ldap group suffix = ou=Groups
 ldap idmap suffix = ou=Idmap
 ldap admin dn = cn=sambaadmin,dc=differentialdesign,dc=org
 idmap backend = ldap://127.0.0.1
 idmap uid = 10000-20000
 idmap gid = 10000-20000
 printer admin = root
 printing = cups

 #========================Share Definitions====================

 [homes]
   comment = Home Directories
   valid users = %S
   browseable = yes
   writable = yes
   create mask = 0600
   directory mask = 0700

 [netlogon]
    omment = Network Logon Service
    ath = /data/samba/netlogon
    riteable = yes
    rowseable = yes
    ead only = no

 [profiles]
    ath = /data/samba/profiles
    riteable = yes
    rowseable = no
    ead only = no
    reate mode = 0777
    irectory mode = 0777

 [Documents]
    omment = share to test samba
    ath = /data/documents
    riteable = yes
    rowseable = yes
    ead only = no
    alid users = "@Domain Users"