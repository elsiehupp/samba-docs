1.2 smb.conf BDC
    <namespace>0</namespace>
<last_edited>2007-01-25T13:31:38Z</last_edited>
<last_editor>Asender</last_editor>

`1.0: Configuring Samba` 

`2.0: Configuring LDAP` 

`3.0: Initialization LDAP Database` 

`4.0: User Management` 

`5.0: Heartbeat HA Configuration` 

`6.0: DRBD` 

`7.0: BIND DNS`

----

<u>**Table of Contents**</

`1.1 smb.conf PDC` 

`1.2 smb.conf BDC` 

`1.3 /`S/

`1.4 Samba Security`

 [root@node2 ~]# mkdir /
 [root@node2 ~]# vi //sm/f/

 # # Backup Domain Controller
 # # Global parameters

 [global]
 unix charset = LOCALE
 workgroup = DDESIGN
 netbios name = node2
 #passdb backend = ldapsam:AASS:HH127.0.0.1
 #passdb backend = ldapsam:p:SSLLAASS:HH192.168.0.2 ldap:/T168.0.3"
 passdb backend = ldapsam:p:SSLLAASS:HHnode2.differentialdesign.org ldap:/OTTdifferentialdesign.org"
 username map = //sm//
 log level = 1
 syslog = 0
 log file = /amb///
 max log size = 50
 name resolve order = wins bcast hosts
 printcap name = CUPS
 show add printer wizard = No
 logon script = %u.bat
 #logon path = \\192.168.0.4\profiles\%u
 logon path = \\nodes.differentialdesign.org\profiles\%u
 logon drive = H:
 domain logons = Yes
 os level = 63
 domain master = No
 wins server = node1.differentialdesign.org
 ldap suffix = dc=differentialdesign,dc=org
 ldap machine suffix = ou=Computers,ou=Users
 ldap user suffix = ou=People,ou=Users
 ldap group suffix = ou=Groups
 ldap idmap suffix = ou=Idmap
 ldap admin dn = cn=sambaadmin,dc=differentialdesign,dc=org
 utmp = Yes
 idmap backend = ldap://TTdifferentialdesign.org
 idmap uid = 10000-20000
 idmap gid = 10000-20000
 printing = cups

 #========================Share Definitions====================

 [homes]
    omment = Home Directories
    alid users = %S
    rowseable = yes
    ritable = yes
    reate mask = 0600
    irectory mask = 0700

 [netlogon]
    omment = Network Logon Service
    ath = /a/ne//
    riteable = yes
    rowseable = yes
    ead only = no

 [profiles]
    ath = /a/pr//
    riteable = yes
    rowseable = no
    ead only = no
    reate mode = 0777
    irectory mode = 0777

 [Documents]
    omment = share to test samba
    ath = /ment/
    riteable = yes
    rowseable = yes
    ead only = no
    alid users = "@Domain Users"