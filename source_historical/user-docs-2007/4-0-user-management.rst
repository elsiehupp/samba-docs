4.0: User Management
    <namespace>0</namespace>
<last_edited>2007-09-19T03:49:25Z</last_edited>
<last_editor>Asender</or>
**`Replicated Failover Domain Controller and file server using LDAP`**

`1.0. Configuring Samba` 

`2.0. Configuring LDAP` 

`3.0. Initialization LDAP Database` 

`4.0. User Management` 

`5.0. Heartbeat HA Configuration` 

`6.0. DRBD` 

`7.0. BIND DNS`

----

`4.0. User Management` 
------------------------

`4.1. smbldap-tools` 
------------------------

We will not be using the smbldap-tools to populate the database; however we will use it to manage users & groups once the database has been populated. These scripts allow us to add users and machines using NT tools such as srvtools.exe, it also makes life easier to manage to add users on the fly.  However it is possible to create LDIF file to add users to the database. 

Smbldap-tools give us an advantage of been able to add machine accounts on the fly through the standard windows domain join. It also gives us the ability of been able to use srvtools.exe; however these tools lack custom control that can only be obtained through manually adding accounts through ldap.

This document configuration has been tested with smbldap-tools-0.9.1-1.

Install smbldap-tools-0.9.1-1on both nodes, this means we can add users and groups from either the PDC or BDC as long as the PDC is contactable. 

You may need to satisfy any dependencies.

 [root@node1 smbldap-tools]# rpm -Uvh smbldap-tools-0.9.1-1.noarch.rpm
   Preparing...                ########################################### [100%]
    :smbldap-tools           ########################################### [100%]
 [root@node1 smbldap-tools]#

 [root@node2 smbldap-tools]# rpm -Uvh smbldap-tools-0.9.1-1.noarch.rpm
    Preparing...                ########################################### [100%]
   1:smbldap-tools          ########################################### [100%]
 [root@node2 smbldap-tools]#

`4.1.1. smbldap.conf Master` 
------------------------

Because we did not use smbldap-tools to populate our database, we must manually configure the smbldap.conf. This configuration file only applies to smbldap-tools-0.9.1-1. If you are using a different version alterations will need to be made.

We will need to configure this file to suit.

 # /DEA/bld/nf//
 # smbldap-tools.conf : Q & D configuration file for smbldap-tools

 #  This code was developped by IDEALX (http://and/
 #  contributors (their names can be found in the CONTRIBUTORS file).
 #
 #                 Copyright (C) 2001-2002 IDEALX
 #
 #  This program is free software; you can redistribute it and/
 #  modify it under the terms of the GNU General Public License
 #  as published by the Free Software Foundation; either version 2
 #  of the License, or (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU General Public License for more details.
 #
 #  You should have received a copy of the GNU General Public License
 #  along with this program; if not, write to the Free Software
 #  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
 #  USA.

 #  Purpose :
 #       . be the configuration file for all smbldap-tools scripts

 ##############################################################################
 #
 # General Configuration
 #
 ##############################################################################

 # Put your own SID. To obtain this number do: "net getlocalsid".
 # If not defined, parameter is taking from "net getlocalsid" return

 SID="S-1-5-21-3809161173-2687474671-1432921517"

 # Domain name the Samba server is in charged.
 # If not defined, parameter is taking from smb.conf configuration file
 # Ex: sambaDomain="IDEALX-NT"
 sambaDomain="DDESIGN"

 ##############################################################################
 #
 # LDAP Configuration
 #
 ##############################################################################
     # Notes: to use to dual ldap servers backend for Samba, you must patch
 # Samba with the dual-head patch from IDEALX. If not using this patch
 # just use the same server for slaveLDAP and masterLDAP.
 # Those two servers declarations can also be used when you have
 # . one master LDAP server where all writing operations must be done
 # . one slave LDAP server where all reading operations must be done
 #   (typically a replication directory)
 # Slave LDAP server
 # Ex: slaveLDAP=127.0.0.1
 # If not defined, parameter is set to "127.0.0.1"
 slaveLDAP="192.168.0.3"

 # Slave LDAP port
 # If not defined, parameter is set to "389"
 slavePort="389"

 # Master LDAP server: needed for write operations
 # Ex: masterLDAP=127.0.0.1
 # If not defined, parameter is set to "127.0.0.1"
 masterLDAP="127.0.0.1"

 # Master LDAP port
 # If not defined, parameter is set to "389"
 masterPort="389"

 # Use TLS for LDAP
 # If set to 1, this option will use start_tls for connection
 # (you should also used the port 389)
 # If not defined, parameter is set to "1"
 ldapTLS="0"
     # How to verify the server's certificate (none, optional or require)
 # see "man Net::LDAP" in start_tls section for more details
 verify=""

 # CA certificate
 # see "man Net::LDAP" in start_tls section for more details
 cafile=""
     # certificate to use to connect to the ldap server
 # see "man Net::LDAP" in start_tls section for more details
 clientcert=""

 # key certificate to use to connect to the ldap server
 # see "man Net::LDAP" in start_tls section for more details
 clientkey=""

 # LDAP Suffix
 # Ex: suffix=dc=IDEALX,dc=ORG
 suffix="dc=differentialdesign,dc=org"

 # Where are stored Users
 # Ex: usersdn="ou=Users,dc=IDEALX,dc=ORG"
 # Warning: if 'suffix' is not set here, you must set the full dn for usersdn
 usersdn="ou=People,ou=Users,${suffix}"

 # Where are stored Computers
 # Ex: computersdn="ou=Computers,dc=IDEALX,dc=ORG"
 # Warning: if 'suffix' is not set here, you must set the full dn for computersdn
 computersdn="ou=Computers,ou=Users,${suffix}"

 # Where are stored Groups
 # Ex: groupsdn="ou=Groups,dc=IDEALX,dc=ORG"
 # Warning: if 'suffix' is not set here, you must set the full dn for groupsdn
 groupsdn="ou=Groups,${suffix}"

 # Where are stored Idmap entries (used if samba is a domain member server)
 # Ex: groupsdn="ou=Idmap,dc=IDEALX,dc=ORG"
 # Warning: if 'suffix' is not set here, you must set the full dn for idmapdn
 idmapdn="ou=Idmap,${suffix}"

 # Where to store next uidNumber and gidNumber available for new users and groups
 # If not defined, entries are stored in sambaDomainName object.
 # Ex: sambaUnixIdPooldn="sambaDomainName=${sambaDomain},${suffix}"
 # Ex: sambaUnixIdPooldn="cn=NextFreeUnixId,${suffix}"
 sambaUnixIdPooldn="sambaDomainName=DDESIGN,ou=Domains,${suffix}"

 # Default scope Used
 scope="sub"

 # Unix password encryption (CRYPT, MD5, SMD5, SSHA, SHA, CLEARTEXT)
 hash_encrypt="MD5"

 # if hash_encrypt is set to CRYPT, you may set a salt format.
 # default is "%s", but many systems will generate MD5 hashed
 # passwords if you use "$1$%.8s". This parameter is optional!

 crypt_salt_format=""

 ##############################################################################
 #
 # Unix Accounts Configuration
 #
 ##############################################################################

 # Login defs 
 # Default Login Shell
 # Ex: userLoginShell="/quo/
 userLoginShell="/quo/

 # Home directory
 # Ex: userHome="/uot;/
 userHome="//%U&//

 # Default mode used for user homeDirectory
 userHomeDirectoryMode="700"

 # Gecos
 userGecos="System User"

 # Default User (POSIX and Samba) GID
 defaultUserGid="513"

 # Default Computer (Samba) GID
 defaultComputerGid="515"

 # Skel dir
 skeletonDir="/quo/

 # Default password validation time (time in days) Comment the next line if
 # you don't want password to be enable for defaultMaxPasswordAge days (be
 # careful to the sambaPwdMustChange attribute's value)
 defaultMaxPasswordAge="45"

 ##############################################################################
 #
 # SAMBA Configuration
 #
 ##############################################################################

 # The UNC path to home drives location (%U username substitution)
 # Just set it to a null string if you want to use the smb.conf 'logon home'
 # directive and/e roaming profiles
 # Ex: userSmbHome="\\PDC-SMB3\%U"
 userSmbHome="\\192.168.0.4\%U"

 # The UNC path to profiles locations (%U username substitution)
 # Just set it to a null string if you want to use the smb.conf 'logon path'
 # directive and/e roaming profiles
 # Ex: userProfile="\\PDC-SMB3\profiles\%U"
 userProfile="\\192.168.0.4\profiles\%U"

 # The default Home Drive Letter mapping
 # (will be automatically mapped at logon time if home directory exist)
 # Ex: userHomeDrive="H:"
 userHomeDrive="H:"

 # The default user netlogon script name (%U username substitution)
 # if not used, will be automatically username.cmd
 # make sure script file is edited under dos
 # Ex: userScript="startup.cmd" # make sure script file is edited under dos
 userScript="%U.bat"

 # Domain appended to the users "mail"-attribute
 # when smbldap-useradd -M is used
 # Ex: mailDomain="idealx.com"
 mailDomain="differentialdesign.org"

 ##############################################################################
 #
 # SMBLDAP-TOOLS Configuration (default are ok for a RedHat)
 #
 ##############################################################################

 # Allows not to use smbpasswd (if with_smbpasswd == 0 in smbldap_conf.pm) but
 # prefer Crypt::SmbHash library
 with_smbpasswd="0"
 smbpasswd="/mbp/;/

 # Allows not to use slappasswd (if with_slappasswd == 0 in smbldap_conf.pm)
 # but prefer Crypt:: libraries
 with_slappasswd="0"
 slappasswd="/sla/ot;/

 # comment out the following line to get rid of the default banner
 # no_banner="1"

`4.1.2. smbldap.conf Slave` 
------------------------

It is not necessary to install smbldap-tools on the backup domain controller. However this lets you add users from the BDC which will refer its update to the PDC ldap database.

 # /DEA/bld/nf //
 #
 # smbldap-tools.conf : Q & D configuration file for smbldap-tools
 #  This code was developped by IDEALX (http://and/
 #  contributors (their names can be found in the CONTRIBUTORS file).
 #
 #                 Copyright (C) 2001-2002 IDEALX
 #
 #  This program is free software; you can redistribute it and/
 #  modify it under the terms of the GNU General Public License
 #  as published by the Free Software Foundation; either version 2
 #  of the License, or (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU General Public License for more details.
 #
 #  You should have received a copy of the GNU General Public License
 #  along with this program; if not, write to the Free Software
 #  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
 #  USA. 

 #  Purpose :
 #       . be the configuration file for all smbldap-tools scripts 

 ##############################################################################
 #
 # General Configuration
 #
 ############################################################################## 

 # Put your own SID. To obtain this number do: "net getlocalsid".
 # If not defined, parameter is taking from "net getlocalsid" return
 SID="S-1-5-21-3809161173-2687474671-1432921517"

 # Domain name the Samba server is in charged.
 # If not defined, parameter is taking from smb.conf configuration file
 # Ex: sambaDomain="IDEALX-NT"
 sambaDomain="DDESIGN" 

 ##############################################################################
 #
 # LDAP Configuration
 #
 ##############################################################################

 # Notes: to use to dual ldap servers backend for Samba, you must patch
 # Samba with the dual-head patch from IDEALX. If not using this patch
 # just use the same server for slaveLDAP and masterLDAP.
 # Those two servers declarations can also be used when you have
 # . one master LDAP server where all writing operations must be done
 # . one slave LDAP server where all reading operations must be done
 #   (typically a replication directory) 

 # Slave LDAP server
 # Ex: slaveLDAP=127.0.0.1
 # If not defined, parameter is set to "127.0.0.1"
 slaveLDAP="127.0.0.1" 

 # Slave LDAP port
 # If not defined, parameter is set to "389"
 slavePort="389" 

 # Master LDAP server: needed for write operations
 # Ex: masterLDAP=127.0.0.1
 # If not defined, parameter is set to "127.0.0.1"
 masterLDAP="192.168.0.2"
     # Master LDAP port
 # If not defined, parameter is set to "389"
 masterPort="389"

 # Use TLS for LDAP
 # If set to 1, this option will use start_tls for connection
 # (you should also used the port 389)
 # If not defined, parameter is set to "1"
 ldapTLS="0"
     # How to verify the server's certificate (none, optional or require)
 # see "man Net::LDAP" in start_tls section for more details
 verify=""
     # CA certificate
 # see "man Net::LDAP" in start_tls section for more details
 cafile=""

 # certificate to use to connect to the ldap server
 # see "man Net::LDAP" in start_tls section for more details
 clientcert=""

 # key certificate to use to connect to the ldap server
 # see "man Net::LDAP" in start_tls section for more details
 clientkey=""

 # LDAP Suffix
 # Ex: suffix=dc=IDEALX,dc=ORG
 suffix="dc=differentialdesign,dc=org"
     # Where are stored Users
 # Ex: usersdn="ou=Users,dc=IDEALX,dc=ORG"
 # Warning: if 'suffix' is not set here, you must set the full dn for usersdn
 usersdn="ou=People,ou=Users,${suffix}"
     # Where are stored Computers
 # Ex: computersdn="ou=Computers,dc=IDEALX,dc=ORG"
 # Warning: if 'suffix' is not set here, you must set the full dn for computersdn
 computersdn="ou=Computers,ou=Users,${suffix}"
     # Where are stored Groups
 # Ex: groupsdn="ou=Groups,dc=IDEALX,dc=ORG"
 # Warning: if 'suffix' is not set here, you must set the full dn for groupsdn
 groupsdn="ou=Groups,${suffix}"
     # Where are stored Idmap entries (used if samba is a domain member server)
 # Ex: groupsdn="ou=Idmap,dc=IDEALX,dc=ORG"
 # Warning: if 'suffix' is not set here, you must set the full dn for idmapdn
 idmapdn="ou=Idmap,${suffix}"
     # Where to store next uidNumber and gidNumber available for new users and groups
 # If not defined, entries are stored in sambaDomainName object.
 # Ex: sambaUnixIdPooldn="sambaDomainName=${sambaDomain},${suffix}"
 # Ex: sambaUnixIdPooldn="cn=NextFreeUnixId,${suffix}"
 sambaUnixIdPooldn="sambaDomainName=DDESIGN,ou=Domains,${suffix}"
     # Default scope Used
 scope="sub"
     # Unix password encryption (CRYPT, MD5, SMD5, SSHA, SHA, CLEARTEXT)
 hash_encrypt="MD5"

 # if hash_encrypt is set to CRYPT, you may set a salt format.
 # default is "%s", but many systems will generate MD5 hashed
 # passwords if you use "$1$%.8s". This parameter is optional!
 crypt_salt_format=""
     ##############################################################################
 #
 # Unix Accounts Configuration
 #
 ##############################################################################
     # Login defs
 # Default Login Shell
 # Ex: userLoginShell="/quo/
 userLoginShell="/quo/
     # Home directory
 # Ex: userHome="/uot;/
 userHome="//%U&//
     # Default mode used for user homeDirectory
 userHomeDirectoryMode="700"
     # Gecos
 userGecos="System User"
     # Default User (POSIX and Samba) GID
 defaultUserGid="513"
     # Default Computer (Samba) GID
 defaultComputerGid="515"
     # Skel dir
 skeletonDir="/quo/
     # Default password validation time (time in days) Comment the next line if
 # you don't want password to be enable for defaultMaxPasswordAge days (be
 # careful to the sambaPwdMustChange attribute's value)
 defaultMaxPasswordAge="45"
     ##############################################################################
 #
 # SAMBA Configuration
 #
 ##############################################################################

 # The UNC path to home drives location (%U username substitution)
 # Just set it to a null string if you want to use the smb.conf 'logon home'
 # directive and/e roaming profiles
 # Ex: userSmbHome="\\PDC-SMB3\%U"
 userSmbHome="\\192.168.0.4\%U"
     # The UNC path to profiles locations (%U username substitution)
 # Just set it to a null string if you want to use the smb.conf 'logon path'
 # directive and/e roaming profiles
 # Ex: userProfile="\\PDC-SMB3\profiles\%U"
 userProfile="\\192.168.0.4\profiles\%U"
     # The default Home Drive Letter mapping
 # (will be automatically mapped at logon time if home directory exist)
 # Ex: userHomeDrive="H:"
 userHomeDrive="H:"
     # The default user netlogon script name (%U username substitution)
 # if not used, will be automatically username.cmd
 # make sure script file is edited under dos
 # Ex: userScript="startup.cmd" # make sure script file is edited under dos
 userScript="%U.bat"

 # Domain appended to the users "mail"-attribute
 # when smbldap-useradd -M is used
 # Ex: mailDomain="idealx.com"
 mailDomain="differentialdesign.org"
     ##############################################################################
 #
 # SMBLDAP-TOOLS Configuration (default are ok for a RedHat)
 #
 ##############################################################################
     # Allows not to use smbpasswd (if with_smbpasswd == 0 in smbldap_conf.pm) but
 # prefer Crypt::SmbHash library
 with_smbpasswd="0" 
 smbpasswd="/mbp/;/
     # Allows not to use slappasswd (if with_slappasswd == 0 in smbldap_conf.pm)
 # but prefer Crypt:: libraries
 with_slappasswd="0"
 slappasswd="/sla/ot;/
     # comment out the following line to get rid of the default banner
 # no_banner="1"

`4.1.3. smbldap_bind.conf` 
------------------------

You can see in this file we use "cn=sambaadmin" to bind to the LDAP database when useing the smbldap-tools from IDEALX. 

When a new user is created or an account modified for samba using these tools, you should see in you openldap database entries for the account have been made by "cn=sambaadmin".

creatorsName: cn=sambaadmin,dc=differentialdesign,dc=org

modifiersName: cn=sambaadmin,dc=differentialdesign,dc=org

 ############################
 # Credential Configuration #
 ############################
 # Notes: you can specify two differents configuration if you use a
 # master ldap for writing access and a slave ldap server for reading access
 # By default, we will use the same DN (so it will work for standard Samba
 # release)
 slaveDN="cn=sambaadmin,dc=differentialdesign,dc=org"
 slavePw="SambaAdmin"
 masterDN="cn=sambaadmin,dc=differentialdesign,dc=org"
 masterPw="SambaAdmin"

`4.2. Samba Net Tools Utility` 
------------------------

The "net" tools are included in the samba suite; they are general purpose tools and designed to have similar use to that of the Windows tools available in dos. 

We will cover only some of the basic usage here as this is already documented in the official samba documentation.

`4.2.1. Adding Users` 
------------------------

We can use the "net user" command to add & remove users to our samba domain.