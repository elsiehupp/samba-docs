4.1 smbldap-tools
    <namespace>0</namespace>
<last_edited>2007-01-25T13:44:53Z</last_edited>
<last_editor>Asender</last_editor>

`1.0: Configuring Samba` 

`2.0: Configuring LDAP` 

`3.0: Initialization LDAP Database` 

`4.0: User Management` 

`5.0: Heartbeat HA Configuration` 

`6.0: DRBD` 

`7.0: BIND DNS`

----

<u>**Table of Contents**</u>

`4.1 smbldap-tools` 

`4.1.1 smbldap.conf Master` 

`4.1.2 smbldap.conf Slave` 

We will not be using the smbldap-tools to populate the database; however we will use it to manage users & groups once the database has been populated. These scripts allow us to add users and machines using NT tools such as srvtools.exe, it also makes life easier to manage to add users on the fly.  However it is possible to create LDIF file to add users to the database. 

Smbldap-tools give us an advantage of been able to add machine accounts on the fly through the standard windows domain join. It also gives us the ability of been able to use srvtools.exe; however these tools lack custom control that can only be obtained through manually adding accounts through ldap.

This document configuration has been tested with smbldap-tools-0.9.1-1.

Install smbldap-tools-0.9.1-1on both nodes, this means we can add users and groups from either the PDC or BDC as long as the PDC is contactable. 

You may need to satisfy any dependencies.

 [root@node1 smbldap-tools]# rpm -Uvh smbldap-tools-0.9.1-1.noarch.rpm
   Preparing...                ########################################### [100%]
    :AASSHHtools           ########################################### [100%]
 [root@node1 smbldap-tools]#

 [root@node2 smbldap-tools]# rpm -Uvh smbldap-tools-0.9.1-1.noarch.rpm
    Preparing...                ########################################### [100%]
   1:AASSHHtools          ########################################### [100%]
 [root@node2 smbldap-tools]#