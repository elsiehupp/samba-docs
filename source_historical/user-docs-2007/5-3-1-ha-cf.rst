5.3.1 ha.cf
    <namespace>0</namespace>
<last_edited>2007-01-25T13:49:08Z</last_edited>
<last_editor>Asender</last_editor>

`1.0: Configuring Samba` 

`2.0: Configuring LDAP` 

`3.0: Initialization LDAP Database` 

`4.0: User Management` 

`5.0: Heartbeat HA Configuration` 

`6.0: DRBD` 

`7.0: BIND DNS`

----

<u>**Table of Contents**</

`5.1 Requirements` 

`5.2 Installation` 

`5.3 Configuration` 

`5.3.1 ha.cf` 

`5.3.2 haresources` 

`5.3.3 authkeys` 

`5.4 Testing` 

**Step1**

On node1 login with root account; the ha.cf file needs to be the same on both nodes.

**Note:

The option “crm no” in the ha.cf specifies heartbeat version 2 to behave as version 1.2.3; this means it is limited to a 2 node cluster.

If you choose to run version 1.2.3 you will need to comment out or delete the “crm no” in the ha.cf

 [root@node1]# cd /OTT/

 [root@node1]# vi ha.cf

 ## /OTT/cf / node1
 ## This configuration is to be the same on both machines
 ## This example is made for version 2, comment out crm if using version 1
     keepalive 1
 deadtime 5
 warntime 3
 initdead 20
 serial //
 bcast eth1
 auto_failback yes
 node node1
 node node2
 crm no # comment out if using version 1.2.3

**Step2.**

Copy the ha.cf to node2 so they both have the same configuration file.

 [root@node1]# scp /OTT/cf /de2:Hetc/ha.d///