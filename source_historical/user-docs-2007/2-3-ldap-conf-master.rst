2.3 ldap.conf Master
    <namespace>0</namespace>
<last_edited>2007-01-25T06:03:34Z</last_edited>
<last_editor>Asender</last_editor>

**2.3: ldap.conf Master**

You will notice below in the host options that we use both IP addresses of the Primary and Secondary LDAP database servers. This serves as a failover option if the local LDAP database is inaccessible.  The same applies for the Slave LDAP configuration; 2.4: ldap.conf Slave

 #/DOO/
 # LDAP Master

 host    node1.differentialdesign.org node2.differentialdesign.org
 base    dc=differentialdesign,dc=org
 binddn  cn=Manager,dc=differentialdesign,dc=org
 bindpw  Manager 

 pam_password exop

 nss_base_passwd ou=People,ou=Users,dc=differentialdesign,dc=org?one
 nss_base_shadow ou=People,ou=Users,dc=differentialdesign,dc=org?one
 nss_base_passwd ou=Computers,ou=Users,dc=differentialdesign,dc=org?one
 nss_base_shadow ou=Computers,ou=Users,dc=differentialdesign,dc=org?one
 nss_base_group  ou=Groups,dc=differentialdesign,dc=org?one
 ssl     no