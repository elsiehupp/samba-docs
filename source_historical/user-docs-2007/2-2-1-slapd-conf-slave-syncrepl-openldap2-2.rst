2.2.1 slapd.conf Slave syncrepl Openldap2.2
    <namespace>0</namespace>
<last_edited>2007-02-26T02:54:04Z</last_edited>
<last_editor>Jerry</last_editor>

2.2.1:OTTconf Slave syncrepl Openldap2.2

This is the configuration file for openldap version 2.2 using the syncrepl method refreshOnly.

 # slapd.conf Slave syncrepl Openldap2.2
 # LDAP Consumer
     include     /dap/re.//
 include     /dap/sineDDOO//
 include     /dap/etorgper/chema/
 include     /dap/s.s//
 include     /dap/mbaDDOOT//

 pidfile     /lap/OTT//
 argsfile    /lap/OTT//
     database    bdb
 suffix      "dc=differentialdesign,dc=org"
 rootdn      "cn=Manager,dc=differentialdesign,dc=org"
 rootpw      Manager
 directory   /dap//

 syncrepl
    rid=0
        provider=ldap://TTdifferentialdesign.org:389:
        binddn="cn=syncuser,dc=differentialdesign,dc=org"
        bindmethod=simple
        credentials=SyncUser
        searchbase="dc=differentialdesign,dc=org"
        filter="(objectClass=*)"
        attrs="*"
        schemachecking=off
        scope=sub
        type=refreshOnly
        interval=00:::

 access to attrs=userPassword
         by dn="cn=sambaadmin,dc=differentialdesign,dc=org" read
         by dn="cn=syncuser,dc=differentialdesign,dc=org" write
         by * auth
     access to attrs=sambaLMPassword,sambaNTPassword
         by dn="cn=sambaadmin,dc=differentialdesign,dc=org" read
         by dn="cn=syncuser,dc=differentialdesign,dc=org" write

 access to *
         by dn="cn=syncuser,dc=differentialdesign,dc=org" write
         by * read

 # Indices to maintain
 index objectClass           eq
 index cn                    pres,sub,eq
 index sn                    pres,sub,eq
 index uid                   pres,sub,eq
 index displayName           pres,sub,eq
 index uidNumber             eq
 index gidNumber             eq
 index memberUID             eq
 index sambaSID              eq
 index sambaPrimaryGroupSID  eq
 index sambaDomainName       eq
 index default               sub