6.3.1 drbd.conf
    <namespace>0</namespace>
<last_edited>2007-01-25T13:52:41Z</last_edited>
<last_editor>Asender</last_editor>

`1.0:ing Samba` 

`2.0:ing LDAP` 

`3.0:zation LDAP Database` 

`4.0:agement` 

`5.0:t HA Configuration` 

`6.0:

`7.0:`

----

<u>**Table of Contents**</u>

`6.1 Requirements` 

`6.2 Installation` 

`6.3 Configuration` 

`6.3.1 drbd.conf` 

`6.3.2 Initialization` 

`6.4 Testing` 

Create this file on both you master and slave server, it should be identical however it is not a requirement. As long as the partition size is the same any mount point can be used.

**Step1**

The below file is fairly self explanatory, you see the real disk link to the DRBD kernel module device.

[root@node1]# vi /etc/drbd.conf

 # Datadrive (/data) /dev/hdd1 80GB

 resource drbd1 {
    rotocol C;
    isk {
    on-io-error panic;

    et {
    max-buffers 2048;
    ko-count 4;
    on-disconnect reconnect;

    yncer {
    rate 700000;

    n node1 {
    device    /dev/drbd0;
    disk      /dev/hdd1;
    address   10.0.0.1:
    meta-disk internal;

    n node2 {
    device    /dev/drbd0;
    disk      /dev/hdd1;
    address   10.0.0.2:
    meta-disk internal;

 }

**Step2**

 [root@node1]# scp /etc/drbd.conf root@node2:Hetc/