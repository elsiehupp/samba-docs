2.1 slapd.conf Master
    <namespace>0</namespace>
<last_edited>2007-01-25T13:40:53Z</last_edited>
<last_editor>Asender</last_editor>

`1.0: Configuring Samba` 

`2.0: Configuring LDAP` 

`3.0: Initialization LDAP Database` 

`4.0: User Management` 

`5.0: Heartbeat HA Configuration` 

`6.0: DRBD` 

`7.0: BIND DNS`

----

<u>**Table of Contents**</u>

`2.1 slapd.conf Master` 

`2.1.1 slapd.conf Master syncrepl Openldap2.2` 

`2.1.2 slapd.conf Master delta-syncrepl Openldap2.3` 

This is the original method for replicating the database to slave ldap servers. We are using the slurpd which has been around for a long time and proven itself to be stable. 

This configuration file should work on any version of Openldap.

 # /etc/openldap/slapd.conf
 # using slurpd
 # LDAP Master

 include     /etc/openldap/schema/core.schema
 include     /etc/openldap/schema/cosine.schema
 include     /etc/openldap/schema/inetorgperson.schema
 include     /etc/openldap/schema/nis.schema
 include     /etc/openldap/schema/samba.schema

 pidfile     /var/run/slapd/slapd.pid
 argsfile    /var/run/slapd/slapd.args

 database    bdb
 suffix       "dc=differentialdesign,dc=org"
 rootdn      "cn=Manager,dc=differentialdesign,dc=org"
 rootpw      Manager
 directory   /var/lib/ldap

 replica  host=node2.differentialdesign.org:389
            suffix="dc=differentialdesign,dc=org"
            binddn="cn=syncuser,dc=differentialdesign,dc=org"
            bindmethod=simple credentials=SyncUser

 replogfile  /var/lib/ldap/replogfile

 access to attrs=userPassword
         by self write
         by dn="cn=sambaadmin,dc=differentialdesign,dc=org" write
         by dn="cn=syncuser,dc=differentialdesign,dc=org" read
         by * auth
     access to attrs=sambaLMPassword,sambaNTPassword
         by dn="cn=sambaadmin,dc=differentialdesign,dc=org" write
         by dn="cn=syncuser,dc=differentialdesign,dc=org" read

 access to *
         by dn="cn=sambaadmin,dc=differentialdesign,dc=org" write
         by dn="cn=syncuser,dc=differentialdesign,dc=org" read
         by * read

 # Indices to maintain
 index objectClass           eq
 index cn                    pres,sub,eq
 index sn                    pres,sub,eq
 index uid                   pres,sub,eq
 index displayName           pres,sub,eq
 index uidNumber             eq
 index gidNumber             eq
 index memberUID             eq
 index sambaSID              eq
 index sambaPrimaryGroupSID  eq
 index sambaDomainName       eq
 index default               sub