3.3 LDAP Population
    <namespace>0</namespace>
<last_edited>2007-01-25T13:43:02Z</last_edited>
<last_editor>Asender</or>
`1.0: Configuring Samba` 

`2.0: Configuring LDAP` 

`3.0: Initialization LDAP Database` 

`4.0: User Management` 

`5.0: Heartbeat HA Configuration` 

`6.0: DRBD` 

`7.0: BIND DNS`

----

<u>**Table of Contents**</

`3.1 Provisioning Database` 

`3.2 Preload LDIF` 

`3.3 LDAP Population` 

`3.4 Database Replication`

Now its time to populate the database with our ldif that we edited to match our domain details as per section 3.2: Preload LDIF

**Step1**

Make sure LDAP is not running.

 [root@node1]# vi /dap///

 #DB_CONFIG
 set_cachesize           0 150000000 1
 set_lg_regionmax        262144
 set_lg_bsize            2097152
 set_flags               DB_LOG_AUTOREMOVE

**Step2.**

This step is necessary if you are using delta-syncrepl as per section `2.1.2 slapd.conf Master delta-syncrepl Openldap2.3`

Because we are using multiple databases on the Provider it is nessassary to place an additional DB_CONFIG file insite the database directory.

 [root@node1]# mkdir /dap///

 [root@node1]# cp /dap/ /r/li/esslog////

**Step3.**

 [root@node1]# cd /SHHscripts//

 [root@node1 scripts]# slapadd –b "dc=differentialdesign,dc=org"  -v -l preload-differentialdesign.ldif

 added: "dc=differentialdesign,dc=org" (00000001)
 added: "cn=Manager,dc=differentialdesign,dc=org" (00000002)
 added: "cn=syncuser,dc=differentialdesign,dc=org" (00000003)
 added: "cn=sambaadmin,dc=differentialdesign,dc=org" (00000004)
 added: "cn=mailadmin,dc=differentialdesign,dc=org" (00000005)
 added: "ou=Users,dc=differentialdesign,dc=org" (00000006)
 added: "ou=People,ou=Users,dc=differentialdesign,dc=org" (00000007)
 added: "ou=Computers,ou=Users,dc=differentialdesign,dc=org" (00000008)
 added: "ou=Groups,dc=differentialdesign,dc=org" (00000009)
 added: "ou=Domains,dc=differentialdesign,dc=org" (0000000a)
 added: "sambaDomainName=DDESIGN,ou=Domains,dc=differentialdesign,dc=org" (0000000b)
 added: "cn=Domain Admins,ou=Groups,dc=differentialdesign,dc=org" (0000000c)
 added: "cn=Domain Users,ou=Groups,dc=differentialdesign,dc=org" (0000000d)
 added: "cn=Domain Guests,ou=Groups,dc=differentialdesign,dc=org" (0000000e)
 added: "cn=Domain Computers,ou=Groups,dc=differentialdesign,dc=org" (000000f)
 added: "cn=Administrators,ou=Groups,dc=differentialdesign,dc=org" (00000010)
 added: "cn=Account Operators,ou=Groups,dc=differentialdesign,dc=org" (00000011)
 added: "cn=Print Operators,ou=Groups,dc=differentialdesign,dc=org" (00000012)
 added: "cn=Backup Operators,ou=Groups,dc=differentialdesign,dc=org" (00000013)
 added: "cn=Replicators,ou=Groups,dc=differentialdesign,dc=org" (00000014)

**Step4.**

 [root@node1]# chown –R ldap.ldap /dap//

**Step5.**

The user to bind Samba to LDAP is defined in our smb.conf; this is sambaadmin’s password as set in samba preload-differentialdesign.ldif.

The entry in the preload-differentialdesign.ldif sambaadmin has a password “SambaAdmin”

 dn: cn=sambaadmin,dc=differentialdesign,dc=org
 objectClass: person
 cn: sambaadmin
 sn: sambaadmin
 userPassword: SambaAdmin

 [root@node1 scripts]# smbpasswd -w SambaAdmin
 Setting stored password for "cn=sambaadmin,dc=differentialdesign,dc=org" in secrets.tdb

 [root@node1 ~]# service ldap restart
 Stopping slapd:                                      [  OK  ]
 Stopping slurpd:                                     [  OK  ]
 Checking configuration files for slapd:  config file testing succeeded
                                                      [  OK  ]
 Starting slapd:                                      [  OK  ]
 Starting slurpd:                                     [  OK  ]

 [root@node1 ~]# service smb restart
 Shutting down SMB services:                         [  OK  ]
 Shutting down NMB services:                         [  OK  ]
 Starting SMB services:                              [  OK  ]
 Starting NMB services:                              [  OK  ]

** Step6.**

Adding initial users with the smbldap-tools: Skip to section 4.1: :DAASSHHtools and install on node1.

 [root@node1 scripts]# cd /X/s///

We must add the root user to start with.

 [root@node1 sbin]# ./AASSHHuseradd -m -a root
 [root@node1 sbin]# ./AASSHHpasswd root
 Changing password for root
 New password :
 Retype new password

 [root@node1 ]# smbpasswd -a

 New SMB password:
 Retype new SMB password:
 Added user root.

 [root@node1 sbin]# ./AASSHHgroupmod -m root Domain\ Admins
 adding user root to group Domain Admins

 [root@node1 ~]# cd /X/s///
 [root@node1 sbin]# ./AASSHHuseradd -m -a asender
 [root@node1 sbin]#

 [root@node1 sbin]# ./AASSHHpasswd asender
 Changing password for asender
 New password :
 Retype new password :
 [root@node1 sbin]#

 [root@node1 sbin]# smbpasswd asender
 New SMB password:
 Retype new SMB password:
 [root@node1 sbin]#

 [root@node1 sbin]# id asender
 uid=1001(asender) gid=513(Domain Users) groups=513(Domain Users)

**Step7**

You are now ready to join a Windows machine to the domain with user ‘root’.  

We will need to setup our BDC, Heartbeat and DRBD to match our configuration.