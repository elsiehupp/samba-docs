3.0: Initialization LDAP Database
    <namespace>0</namespace>
<last_edited>2007-03-14T12:49:15Z</last_edited>
<last_editor>Asender</last_editor>

**`Replicated Failover Domain Controller and file server using LDAP`**

`1.0. Configuring Samba` 

`2.0. Configuring LDAP` 

`3.0. Initialization LDAP Database` 

`4.0. User Management` 

`5.0. Heartbeat HA Configuration` 

`6.0. DRBD` 

`7.0. BIND DNS`

----

`3.0. Initialization LDAP Database` 
------------------------

There are many ways to initialize the LDAP database backend for samba and many scripts to help you out; however these loose our initial control of the database and can lead to issues such as database management.

Once your server is up and running with users on it, the database can not really be manipulated without knowing the full workings of LDAP, so for many of us we are stuck with what we created.

The future of Samba is changing to Active Directory; we keep this in mind when creating the database so it can be an easier upgrade path migrating to Samba4; eventually Samba4 will be able to support OpenLDAP as a modular backend.

`3.1. Provisioning Database` 
------------------------

We are going to manually create our initial LDAP database in a text file and be confident to use it in a full production environment.

Our LDAP database structure will look like the following if using the preload ldif as per section 3.2 Preload LDIF

 |-Samba Base
 |---Manager                  
 |------syncuser                
 |------sambaadmin           
 |------mailadmin               
 |---------Users                              
          |-----------People                          
                      |-----------root                       
                      |-----------asender
                      |-----------simo
          |-----------Computers                     
                      |-----------workstation1$
                      |-----------workstation2$
 |---------Groups                
          |-----------Domain Admin               
                      |-----------root                     
          |-----------Domain Users                
                      |-----------root
                      |-----------asender
                      |-----------simo
          |-----------Domain Guests            
                      |------------nobody
          |-----------Domain Computers       
                      |-----------workstation1$
                      |-----------workstation2$
 |-----------Domains             
 |-------------sambaDomainName

**Step1.**

 [root@node1]# /etc/rc.d/init.d/slapd stop

or 

 [root@node1]# ps aux | grep slapd
 [root@node1]# kill PID

**Step2.**

Delete all runtime files from prior Samba operation by executing;

 [root@node1]#   rm /etc/samba/*tdb
 [root@node1]#   rm /var/lib/samba/*tdb
 [root@node1]#   rm /var/lib/samba/*dat
 [root@node1]#   rm /var/log/samba/* 

**Step3.** 

Delete any previous LDAP database

 [root@node1]#  cd /var/lib/ldap
 [root@node1]#  rm –rf *

**Step4.**

Login to node2 - the backup domain controller, and do the same. 

**Step5.**

 [root@node1 ~]# net getlocalsid
 SID for domain NODE1 is: S-1-5-21-3809161173-2687474671-1432921517

Your SID will differ to the one above; you will need to alter the preload LDIF as per below.

**Step6.**

Login to your backup domain controller (node2) and type the following command using the SID obtained from step4.

 [root@node2 ~]# net setlocalsid S-1-5-21-3809161173-2687474671-1432921517

`3.2. Preload LDIF` 
------------------------

**Step1.**

Create a .txt or .ldif file containing the following contents.

 [root@node1]#vi preload-differentialdesign.ldif                                                  

Subsitute SID <font color=blue>S-1-5-21-3809161173-2687474671-1432921517</font> with your domain SID, be sure to leave the SID group mapping.

Subsitute dc=differentialdesign,dc=org with your fully qualified domain name.
Subsitute sambaDomainName: DDESIGN with your Samba Domain Name

 # SAMBA LDAP PRELOAD
 # Subsitute SID <font color=blue>S-1-5-21-3809161173-2687474671-1432921517</font> with your domain SID
 # Be sure to leave the SID group mapping.
 # Subsitute dc=<font color=blue>differentialdesign</font>,dc=<font color=blue>org</font> with your fully qualified domain name.
 # Subsitute sambaDomainName: <font color=blue>DDESIGN</font> with your Samba Domain Name

 # The user to bind Samba to LDAP is defined in our smb.conf; 
 # [root@node1]#  smbpasswd –w SambaAdmin)
 # [root@node2]#  smbpasswd –w SambaAdmin)

 <font color=blue># SID S-1-5-21-3809161173-2687474671-1432921517</font>.

 dn: dc=<font color=blue>differentialdesign</font>,<font color=blue>dc=org</font>
 objectClass: dcObject
 objectClass: organization
 dc: <font color=blue>differentialdesign</font>
 o: <font color=blue>DDESIGN</font>
 description: Posix and Samba LDAP Identity Database

 dn: cn=Manager,dc=<font color=blue>differentialdesign</font>,dc=<font color=blue>org</font>
 objectClass: organizationalRole
 cn: Manager
 description: Directory Manager

 dn: cn=syncuser,dc=<font color=blue>differentialdesign</font>,dc=<font color=blue>org</font>
 objectClass: person
 cn: syncuser
 sn: syncuser
 userPassword: SyncUser

 dn: cn=sambaadmin,dc=<font color=blue>differentialdesign</font>,dc=<font color=blue>org</font>
 objectClass: person
 cn: sambaadmin
 sn: sambaadmin
 userPassword: SambaAdmin

 dn: cn=mailadmin,dc=<font color=blue>differentialdesign</font>,dc=<font color=blue>org</font>
 objectClass: person
 cn: mailadmin
 sn: mailadmin
 userPassword: MailAdmin

 dn: ou=Users,dc=<font color=blue>differentialdesign</font>,dc=<font color=blue>org</font>
 objectClass: top
 objectClass: organizationalUnit
 ou: Users

 dn: ou=People,ou=Users,dc=<font color=blue>differentialdesign</font>,dc=<font color=blue>org</font>
 objectClass: top
 objectClass: organizationalUnit
 ou: People

 dn: ou=Computers,ou=Users,dc=<font color=blue>differentialdesign</font>,dc=<font color=blue>org</font>
 objectClass: top
 objectClass: organizationalUnit
 ou: Computers

 dn: ou=Groups,dc=<font color=blue>differentialdesign</font>,dc=<font color=blue>org</font>
 objectClass: top
 objectClass: organizationalUnit
 ou: Groups

 dn: ou=Domains,dc=<font color=blue>differentialdesign</font>,dc=<font color=blue>org</font>
 objectClass: top
 objectClass: organizationalUnit
 ou: Domains

 dn: sambaDomainName=<font color=blue>DDESIGN</font>,ou=Domains,dc=<font color=blue>differentialdesign</font>,dc=<font color=blue>org</font>
 objectClass: sambaDomain
 objectClass: sambaUnixIdPool
 uidNumber: 1000
 gidNumber: 1000
 sambaDomainName: <font color=blue>DDESIGN</font>
 sambaSID: <font color=blue>S-1-5-21-3809161173-2687474671-1432921517</font>
 sambaAlgorithmicRidBase: 1000
 structuralObjectClass: sambaDomain

 dn: cn=Domain Admins,ou=Groups,dc=<font color=blue>differentialdesign</font>,dc=<font color=blue>org</font>
 objectClass: posixGroup
 objectClass: sambaGroupMapping
 gidNumber: 512
 cn: Domain Admins
 sambaSID: <font color=blue>S-1-5-21-3809161173-2687474671-1432921517</font>-512
 sambaGroupType: 2
 displayName: Domain Admins
 description: Domain Administrators

 dn: cn=Domain Users,ou=Groups,dc=<font color=blue>differentialdesign</font>,dc=<font color=blue>org</font>
 objectClass: posixGroup
 objectClass: sambaGroupMapping
 gidNumber: 513
 cn: Domain Users
 sambaSID: <font color=blue>S-1-5-21-3809161173-2687474671-1432921517</font>-513
 sambaGroupType: 2
 displayName: Domain Users
 description: Domain Users 

 dn: cn=Domain Guests,ou=Groups,dc=<font color=blue>differentialdesign</font>,dc=<font color=blue>org</font>
 objectClass: posixGroup
 objectClass: sambaGroupMapping
 gidNumber: 514
 cn: Domain Guests
 sambaSID: <font color=blue>S-1-5-21-3809161173-2687474671-1432921517</font>-514
 sambaGroupType: 2
 displayName: Domain Guests
 description: Domain Guests

 dn: cn=Domain Computers,ou=Groups,dc=<font color=blue>differentialdesign</font>,dc=<font color=blue>org</font>
 objectClass: posixGroup
 objectClass: sambaGroupMapping
 gidNumber: 515
 cn: Domain Computers
 sambaSID: <font color=blue>S-1-5-21-3809161173-2687474671-1432921517</font>-515
 sambaGroupType: 2
 displayName: Domain Computers
 description: Domain Computers

 dn: cn=Administrators,ou=Groups,dc=<font color=blue>differentialdesign</font>,dc=<font color=blue>org</font>
 objectClass: posixGroup
 objectClass: sambaGroupMapping
 gidNumber: 544
 cn: Administrators
 sambaSID: <font color=blue>S-1-5-21-3809161173-2687474671-1432921517</font>-544
 sambaGroupType: 5
 displayName: Administrators
 description: Administrators

 dn: cn=Account Operators,ou=Groups,dc=<font color=blue>differentialdesign</font>,dc=<font color=blue>org</font>
 objectClass: posixGroup
 objectClass: sambaGroupMapping
 gidNumber: 548
 cn: Account Operators
 sambaSID: <font color=blue>S-1-5-21-3809161173-2687474671-1432921517</font>-548
 sambaGroupType: 5
 displayName: Account Operators
 description: Account Operators

 dn: cn=Print Operators,ou=Groups,dc=<font color=blue>differentialdesign</font>,dc=<font color=blue>org</font>
 objectClass: posixGroup
 objectClass: sambaGroupMapping
 gidNumber: 550
 cn: Print Operators
 sambaSID: <font color=blue>S-1-5-21-3809161173-2687474671-1432921517</font>-550
 sambaGroupType: 5
 displayName: Print Operators
 description: Print Operators

 dn: cn=Backup Operators,ou=Groups,dc=<font color=blue>differentialdesign</font>,dc=<font color=blue>org</font>
 objectClass: posixGroup
 objectClass: sambaGroupMapping
 gidNumber: 551
 cn: Backup Operators
 sambaSID: <font color=blue>S-1-5-21-3809161173-2687474671-1432921517</font>-551
 sambaGroupType: 5
 displayName: Backup Operators
 description: Backup Operators

 dn: cn=Replicators,ou=Groups,dc=<font color=blue>differentialdesign</font>,dc=<font color=blue>org</font>
 objectClass: posixGroup
 objectClass: sambaGroupMapping
 gidNumber: 552
 cn: Replicators
 sambaSID: <font color=blue>S-1-5-21-3809161173-2687474671-1432921517</font>-552
 sambaGroupType: 5
 displayName: Replicators
 description: Replicators

`3.3. LDAP Population` 
------------------------

Now its time to populate the database with our ldif that we edited to match our domain details as per section 3.2: Preload LDIF

**Step1.**

Make sure LDAP is not running.

 [root@node1]# vi /var/lib/ldap/DB_CONFIG

 #DB_CONFIG
 set_cachesize           0 150000000 1
 set_lg_regionmax        262144
 set_lg_bsize            2097152
 set_flags               DB_LOG_AUTOREMOVE

**Step2.**

This step is necessary if you are using delta-syncrepl as per section [http://wiki.samba.org/index.php/2.0._Configuring_LDAP#2.2.2._slapd.conf_Master_delta-syncrepl_Openldap2.3 2.2.2. slapd.conf Master delta-syncrepl Openldap2.3]

Because we are using multiple databases on the Provider it is nessassary to place an additional DB_CONFIG file inside the accesslog database directory.

 [root@node1]# mkdir /usr/local/var/openldap-data/accesslog

 [root@node1]# cp /usr/local/var/openldap-data/DB_CONFIG /usr/local/var/openldap-data/accesslog

**Step3.**

The base dn of the database you are adding needs to be specified if you have multiple databases.

 [root@node1]# cd /ldap-scripts/

Substitute base "dc=differentialdesign,dc=org" with "dc=example,dc=net,dc=au.

 [root@node1 scripts]# slapadd –b "dc=differentialdesign,dc=org"  -v -l preload-differentialdesign.ldif

 added: "dc=differentialdesign,dc=org" (00000001)
 added: "cn=Manager,dc=differentialdesign,dc=org" (00000002)
 added: "cn=syncuser,dc=differentialdesign,dc=org" (00000003)
 added: "cn=sambaadmin,dc=differentialdesign,dc=org" (00000004)
 added: "cn=mailadmin,dc=differentialdesign,dc=org" (00000005)
 added: "ou=Users,dc=differentialdesign,dc=org" (00000006)
 added: "ou=People,ou=Users,dc=differentialdesign,dc=org" (00000007)
 added: "ou=Computers,ou=Users,dc=differentialdesign,dc=org" (00000008)
 added: "ou=Groups,dc=differentialdesign,dc=org" (00000009)
 added: "ou=Domains,dc=differentialdesign,dc=org" (0000000a)
 added: "sambaDomainName=DDESIGN,ou=Domains,dc=differentialdesign,dc=org" (0000000b)
 added: "cn=Domain Admins,ou=Groups,dc=differentialdesign,dc=org" (0000000c)
 added: "cn=Domain Users,ou=Groups,dc=differentialdesign,dc=org" (0000000d)
 added: "cn=Domain Guests,ou=Groups,dc=differentialdesign,dc=org" (0000000e)
 added: "cn=Domain Computers,ou=Groups,dc=differentialdesign,dc=org" (000000f)
 added: "cn=Administrators,ou=Groups,dc=differentialdesign,dc=org" (00000010)
 added: "cn=Account Operators,ou=Groups,dc=differentialdesign,dc=org" (00000011)
 added: "cn=Print Operators,ou=Groups,dc=differentialdesign,dc=org" (00000012)
 added: "cn=Backup Operators,ou=Groups,dc=differentialdesign,dc=org" (00000013)
 added: "cn=Replicators,ou=Groups,dc=differentialdesign,dc=org" (00000014)

**Step4.**

 [root@node1]# chown –R ldap.ldap /var/lib/ldap

**Step5.**

The user to bind Samba to LDAP is defined in our smb.conf; this is sambaadmin’s password as set in samba preload-differentialdesign.ldif.

The entry in the preload-differentialdesign.ldif sambaadmin has a password “SambaAdmin”

 dn: cn=sambaadmin,dc=differentialdesign,dc=org
 objectClass: person
 cn: sambaadmin
 sn: sambaadmin
 userPassword: SambaAdmin

 [root@node1 scripts]# smbpasswd -w SambaAdmin
 Setting stored password for "cn=sambaadmin,dc=differentialdesign,dc=org" in secrets.tdb

 [root@node1 ~]# service smb restart
 Shutting down SMB services:                         [  OK  ]
 Shutting down NMB services:                         [  OK  ]
 Starting SMB services:                              [  OK  ]
 Starting NMB services:                              [  OK  ]

** Step6.**

Adding initial users with the smbldap-tools: Skip to section 4.1: smbldap-tools and install on node1.

 [root@node1 scripts]# cd /opt/IDEALX/sbin/

We must add the root user to start with.

 [root@node1 sbin]# ./smbldap-useradd -m -a root
 [root@node1 sbin]# ./smbldap-passwd root
 Changing password for root
 New password :
 Retype new password

 [root@node1 ]# smbpasswd -a

 New SMB password:
 Retype new SMB password:
 Added user root.

 [root@node1 sbin]# ./smbldap-groupmod -m root Domain\ Admins
 adding user root to group Domain Admins

 [root@node1 ~]# cd /opt/IDEALX/sbin/
 [root@node1 sbin]# ./smbldap-useradd -m -a asender
 [root@node1 sbin]#

 [root@node1 sbin]# ./smbldap-passwd asender
 Changing password for asender
 New password :
 Retype new password :
 [root@node1 sbin]#

 [root@node1 sbin]# smbpasswd asender
 New SMB password:
 Retype new SMB password:
 [root@node1 sbin]#

 [root@node1 sbin]# id asender
 uid=1001(asender) gid=513(Domain Users) groups=513(Domain Users)

**Step7.**

You are now ready to join a Windows machine to the domain with user ‘root’.  

We will need to setup our BDC, Heartbeat and DRBD to match our configuration.

`3.4 Running LDAP` 
------------------------

If you have not compiled LDAP from source you can start it by using the ldap script located in "/etc/rc.d/init.d/"

The below example shows Openldap starting with the slurpd daemon as per slapd.conf section [http://wiki.samba.org/index.php/2.0._Configuring_LDAP#2.2._slapd.conf_Master 2.2 slapd.conf Master 2.2] 

 [root@node1 ~]# /etc/rc.d/init.d/ldap restart
 Stopping slapd:                                      [  OK  ]
 Stopping slurpd:                                     [  OK  ]
 Checking configuration files for slapd:  config file testing succeeded
                                                      [  OK  ]
 Starting slapd:                                      [  OK  ]
 Starting slurpd:                                     [  OK  ]

With Openldap 2.3 we will start the daemon slightly different without using a init script.

Referencing back to where we compiled LDAP from source you can where slapd is located.
[http://wiki.samba.org/index.php/2.0._Configuring_LDAP#2.1._Installing_LDAP 2.1 Installing LDAP] 
 [root@node1 ~]# cd /usr/local/libexec/
 [root@node1 libexec]#

 [root@node1 libexec]# ls
 openldap  slapd  slurpd

We can initially run slapd like this:

 [root@node1 libexec]# /usr/local/libexec/slapd

Now lets grep the process to check that its alive.

 [root@node1 ~]# ps aux | grep slapd
 root      3741  0.6  1.1 420792 11396 ?        Ssl  02:35   0:00 /usr/local/libexec/slapd

To stop the LDAP server kill the PID, which in this case would be done as follows.

 [root@node1 ~]# kill 3741

`3.4.1 Starting LDAP On Boot` 
------------------------

For starting LDAP during boot up time, it is recommended to run the slapd deamon directly and place it in our rc.local file.

 # /etc/rc.local
 #!/bin/sh
 #
 # This script will be executed *after* all the other init scripts.
 # You can put your own initialization stuff in here if you don't
 # want to do the full Sys V style init stuff.

 touch /var/lock/subsys/local

 #start slapd deamon
 /usr/local/libexec/slapd

Although it is recommended to start Openldap 2.3 as above, we can use an init script which can ease management.

`3.5. Database Replication` 
------------------------

If we choose to use syncrepl instead of slurpd daemon as per sections [http://wiki.samba.org/index.php/2.0._Configuring_LDAP#2.1.1._slapd.conf_Master_syncrepl_Openldap2.2  2.1.1. slapd.conf Master syncrepl Openldap2.2] and [http://wiki.samba.org/index.php/2.0._Configuring_LDAP#2.1.2._slapd.conf_Master_delta-syncrepl_Openldap2.3 2.1.2. slapd.conf Master delta-syncrepl Openldap2.3] there is no need to do this section, the database will be copied across initially when the consumer is restarted. 

**Step1.** 

Dump the LDAP database, copy it across to node2.

Substitute base "dc=differentialdesign,dc=org" with "dc=example,dc=net,dc=au.

 [root@node1 ~]# slapcat –b “dc=differentialdesign,dc=org” -v -l transfer.ldif

 # id=00000001
 # id=00000002
 # id=00000003
 # id=00000004
 # id=00000005
 # id=00000006
 # id=00000007
 # id=00000008
 # id=00000009
 # id=0000000a
 # id=0000000b
 # id=0000000c
 # id=0000000d
 # id=0000000e
 # id=0000000f
 # id=00000010
 # id=00000011
 # id=00000012
 # id=00000013
 # id=00000014
 # id=00000015
 # id=00000017
 # id=00000018

 [root@node1 ~]# scp transfer.ldif root@node2:/root/

**Step2.**

Transfer the database to node2.

 [root@node2 ~]# slapadd –b “dc=differentialdesign,dc=org” -v -l transfer.ldif

 added: "dc=differentialdesign,dc=org" (00000001)
 added: "cn=Manager,dc=differentialdesign,dc=org" (00000002)
 added: "cn=syncuser,dc=differentialdesign,dc=org" (00000003)
 added: "cn=sambaadmin,dc=differentialdesign,dc=org" (00000004)
 added: "cn=mailadmin,dc=differentialdesign,dc=org" (00000005)
 added: "ou=Users,dc=differentialdesign,dc=org" (00000006)
 added: "ou=People,ou=Users,dc=differentialdesign,dc=org" (00000007)
 added: "ou=Computers,ou=Users,dc=differentialdesign,dc=org" (00000008)
 added: "ou=Groups,dc=differentialdesign,dc=org" (00000009)
 added: "ou=Domains,dc=differentialdesign,dc=org" (0000000a)
 added: "sambaDomainName=DDESIGN,ou=Domains,dc=differentialdesign,dc=org" (0000000b)
 added: "cn=Domain Admins,ou=Groups,dc=differentialdesign,dc=org" (0000000c)
 added: "cn=Domain Users,ou=Groups,dc=differentialdesign,dc=org" (0000000d)
 added: "cn=Domain Guests,ou=Groups,dc=differentialdesign,dc=org" (0000000e)
 added: "cn=Domain Computers,ou=Groups,dc=differentialdesign,dc=org" (000000f)
 added: "cn=Administrators,ou=Groups,dc=differentialdesign,dc=org" (00000010)
 added: "cn=Account Operators,ou=Groups,dc=differentialdesign,dc=org" (00000011)
 added: "cn=Print Operators,ou=Groups,dc=differentialdesign,dc=org" (00000012)
 added: "cn=Backup Operators,ou=Groups,dc=differentialdesign,dc=org" (00000013)
 added: "cn=Replicators,ou=Groups,dc=differentialdesign,dc=org" (00000014)
 added: "uid=root,ou=People,ou=Users,dc=differentialdesign,dc=org" (00000015)
 added: "uid=asender,ou=People,ou=Users,dc=differentialdesign,dc=org" (00000016)

**Step3.**

Make sure LDAP database is owned by LDAP

 [root@node2 ~]# chown –R ldap.ldap /var/lib/ldap 

**Step4.**

 [root@node1 ~]# service ldap restart
 Stopping slapd:                                      [  OK  ]
 Stopping slurpd:                                     [  OK  ]
 Checking configuration files for slapd:  config file testing succeeded
                                                      [  OK  ]
 Starting slapd:                                      [  OK  ]
 Starting slurpd:                                     [  OK  ]

 [root@node1 ~]# service smb restart
 Shutting down SMB services:                          [  OK  ]
 Shutting down NMB services:                          [  OK  ]
 Starting SMB services:                               [  OK  ]
 Starting NMB services:                               [  OK  ]

**Step5.**

Login to node1 or your Primary Domain Controller and add another user as done so in section 3.6 LDAP population Step5, we will then check replication by logging onto node2 and see if the user exists on that machine.

 [root@node1 sbin]# ./smbldap-useradd -m -a testuser
 [root@node1 sbin]# ./smbldap-passwd testuser
 Changing password for testuser
 New password :
 Retype new password :

 [root@node1 sbin]# smbpasswd testuser
 New SMB password:
 Retype new SMB password:

 [root@node1 sbin]# ssh node2
 root@node2's password:
 Last login: Mon Dec 18 02:43:33 2006 from 192.168.0.2

 [root@node2 ~]# id testuser
 uid=1009(testuser) gid=513(Domain Users) groups=513(Domain Users)

`3.6. Debugging LDAP` 
------------------------

It is a good idea to enable debugging when configuring Openldap. The easiest way to do this is to add an entry to our syslog.conf.

**Step1.** 

 [root@node1 ~]# vi /etc/syslog.conf

**Step2.** 

The following entry at the end of the syslog.conf file will dump ldap log to a file called ldaplogs.

 local4.*                                                /var/log/ldaplogs

**Step3.**

 [root@node1 ~]# /etc/rc.d/init.d/syslog restart
 Shutting down kernel logger:                               [  OK  ]
 Shutting down system logger:                               [  OK  ]
 Starting system logger:                                    [  OK  ]
 Starting kernel logger:                                    [  OK  ]
 [root@node1 ~]#

**Step4.**

 [root@node1 ~]# tail -f /var/log/ldaplogs

**Common issue**

If you are debugging LDAP and find this error in the logs: 

<font color=red>*Feb  8 00:55:24 node1 slapd2.3[6171]: <= bdb_equality_candidates: (sambaSIDList) index_param failed (18)*</font>

It can be ignored and means no harm, it is produced as a result of having the same entries in /etc/passwd or /etc/groups as your LDAP database and is considered normal operation.

You may run into this problem when running slaptest, we need to add our base dn, start LDAP and this error will then go away; you also may have forgotten to create a directory specified in the slapd.conf after compiling LDAP. 

 [root@node1 OSBACKUP-8.2.07]# slaptest
 bdb_db_open: db_open(/var/lib/ldap/accesslog/id2entry.bdb) failed: No such file or directory (2)
 bdb(cn=accesslog): Unknown locker ID: 0
 backend_startup_one: bi_db_open failed! (2)
 slap_startup failed (test would succeed using the -u switch)