5.2 Installation
    <namespace>0</namespace>
<last_edited>2007-01-25T13:48:36Z</last_edited>
<last_editor>Asender</last_editor>

`1.0: Configuring Samba` 

`2.0: Configuring LDAP` 

`3.0: Initialization LDAP Database` 

`4.0: User Management` 

`5.0: Heartbeat HA Configuration` 

`6.0: DRBD` 

`7.0: BIND DNS`

----

<u>**Table of Contents**</u>

`5.1 Requirements` 

`5.2 Installation` 

`5.3 Configuration` 

`5.3.1 ha.cf` 

`5.3.2 haresources` 

`5.3.3 authkeys` 

`5.4 Testing` 

Heartbeat can now be downloaded with YUM, it will download version 2.

Repeat this process on node2 your backup domain controller, so they are both running identical versions of heartbeat.

Install heartbeat on both nodes

 [root@node1 programs]# cd heartbeat-1.2.3/

 [root@node1 heartbeat-1.2.3]# ls
 heartbeat-1.2.3-2.rh.9.i386.rpm
 heartbeat-ldirectord-1.2.3-2.rh.9.i386.rpm
 heartbeat-pils-1.2.3-2.rh.9.i386.rpm
 heartbeat-stonith-1.2.3-2.rh.9.i386.rpm

 [root@node1 heartbeat-1.2.3]#rpm -Uvh heartbeat-1.2.3-2.rh.9.i386.rpm 
 heartbeat-ldirectord-1.2.3-2.rh.9.i386.rpm heartbeat-pils-1.2.3-2.rh.9.i386.rpm 
 heartbeat- stonith-1.2.3-2.rh.9.i386.rpm