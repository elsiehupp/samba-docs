1.3 /etc/hosts
    <namespace>0</namespace>
<last_edited>2007-01-25T13:31:56Z</last_edited>
<last_editor>Asender</last_editor>

`1.0: Configuring Samba` 

`2.0: Configuring LDAP` 

`3.0: Initialization LDAP Database` 

`4.0: User Management` 

`5.0: Heartbeat HA Configuration` 

`6.0: DRBD` 

`7.0: BIND DNS`

----

<u>**Table of Contents**</u>

`1.1 smb.conf PDC` 

`1.2 smb.conf BDC` 

`1.3 /etc/hosts` 

`1.4 Samba Security`

In order to correctly resolve name to IP address we need some sort of name resolution. We already have a DNS name server which is capable of doing this as per section 7.0: BIND DNS; however it is desirable to have a backup feature such as entries in the /etc/hosts file.

**Step1**

On node1 we will edit the hosts file to reflect our configuration.

 [root@node1 ~]# vi /etc/hosts

 # Do not remove the following line, or various programs
 # that require network functionality will fail.
 127.0.0.1       node1   localhost.localdomain   localhost
 192.168.0.2     node1.differentialdesign.org
 192.168.0.3     node2.differentialdesign.org
 192.168.0.4     nodes.differentialdesign.org

**Step2**

Login to node2 and edit the /etc/hosts file.

 [root@node2 ~]# vi /etc/hosts

 # Do not remove the following line, or various programs
 # that require network functionality will fail.
 127.0.0.1       node2   localhost.localdomain   localhost
 192.168.0.2     node1.differentialdesign.org
 192.168.0.3     node2.differentialdesign.org
 192.168.0.4     nodes.differentialdesign.org