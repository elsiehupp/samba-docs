5.1 Requirements
    <namespace>0</namespace>
<last_edited>2007-01-25T13:48:21Z</last_edited>
<last_editor>Asender</last_editor>

`1.0: Configuring Samba` 

`2.0: Configuring LDAP` 

`3.0: Initialization LDAP Database` 

`4.0: User Management` 

`5.0: Heartbeat HA Configuration` 

`6.0: DRBD` 

`7.0: BIND DNS`

----

<u>**Table of Contents**</u>

`5.1 Requirements` 

`5.2 Installation` 

`5.3 Configuration` 

`5.3.1 ha.cf` 

`5.3.2 haresources` 

`5.3.3 authkeys` 

`5.4 Testing` 

Get the following RPM’s from the http://www.linux-ha.org web site.

Version 1.2.3 has proven rock solid in many mission critical environments.

You may need to satisfy dependencies.

If you chose to install heartbeat version 1.2.3 take note of the configuration file 5.3 Configuration PDC it differs slightly.