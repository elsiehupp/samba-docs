5.3.2 haresources
    <namespace>0</namespace>
<last_edited>2007-01-25T13:49:23Z</last_edited>
<last_editor>Asender</last_editor>

`1.0: Configuring Samba` 

`2.0: Configuring LDAP` 

`3.0: Initialization LDAP Database` 

`4.0: User Management` 

`5.0: Heartbeat HA Configuration` 

`6.0: DRBD` 

`7.0: BIND DNS`

----

<u>**Table of Contents**</u>

`5.1 Requirements` 

`5.2 Installation` 

`5.3 Configuration` 

`5.3.1 ha.cf` 

`5.3.2 haresources` 

`5.3.3 authkeys` 

`5.4 Testing` 

The haresorces file is called when heartbeat starts. Throughout this document we have used /data as our mount point for replication raid1 over LAN.

We use node1, which is the master server and use 192.168.0.4 which is the clusters virtual IP address which will be displayed as eth0:0 on the primary node.

You will see drbddisk Filesystem::/dev/drbd0::/data:: xt3 - /dev/drbd0 is our DRBD drive. We have chosen to mount our DRBD file system at /data – this is our replication mount point, which we configured in our samba and smbldap-tools configuration.

You can easily make services highly available by adding the appropriate name to the haresources file as specified below with DNS service named.

**Step1**

 [root@node1]# vi haresources

 ## /etc/ha.d/haresources
 ## This configuration is to be the same on both nodes

 node1 192.168.0.4 drbddisk Filesystem::/dev/drbd0::/data:: xt3 named 

**Step2**

Copy the haresources file across to node2 so they are both identical.

 [root@node1]# scp /etc/ha.d/haresources root@node2:/etc/ha.d/