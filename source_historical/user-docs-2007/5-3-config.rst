5.3 Configuration
    <namespace>0</namespace>
<last_edited>2007-01-25T13:48:53Z</last_edited>
<last_editor>Asender</last_editor>

`1.0: Configuring Samba` 

`2.0: Configuring LDAP` 

`3.0: Initialization LDAP Database` 

`4.0: User Management` 

`5.0: Heartbeat HA Configuration` 

`6.0: DRBD` 

`7.0: BIND DNS`

----

<u>**Table of Contents**</u>

`5.1 Requirements` 

`5.2 Installation` 

`5.3 Configuration` 

`5.3.1 ha.cf` 

`5.3.2 haresources` 

`5.3.3 authkeys` 

`5.4 Testing` 

Heartbeat running as version 1.2.3 is very easy to configure and manage. The never version 2 is able to support multiple nodes and uses xml type configuration files. If you are using version 2 I recommend running using crm = no option which provides 1.2.3 backwards compatability.

Just remember to always run the same version of heartbeat on both nodes.