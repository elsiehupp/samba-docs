3.1 Provisioning Database
    <namespace>0</namespace>
<last_edited>2007-01-25T13:42:30Z</last_edited>
<last_editor>Asender</last_editor>

`1.0:ing Samba` 

`2.0:ing LDAP` 

`3.0:zation LDAP Database` 

`4.0:agement` 

`5.0:t HA Configuration` 

`6.0:

`7.0:`

----

<u>**Table of Contents**</

`3.1 Provisioning Database` 

`3.2 Preload LDIF` 

`3.3 LDAP Population` 

`3.4 Database Replication`

We are going to manually create our initial LDAP database in a text file and be confident to use it in a full production environment.

Our LDAP database structure will look like the following if using the preload ldif as per section 3.2 Preload LDIF

 |-Samba Base
 |---Manager                  
 |------syncuser                
 |------sambaadmin           
 |------mailadmin               
 |---------Users                              
          |-----------People                          
                      |-----------root                       
                      |-----------asender
                      |-----------simo
          |-----------Computers                     
                      |-----------workstation1$
                      |-----------workstation2$
 |---------Groups                
          |-----------Domain Admin               
                      |-----------root                     
          |-----------Domain Users                
                      |-----------root
                      |-----------asender
                      |-----------simo
          |-----------Domain Guests            
                      |------------nobody
          |-----------Domain Computers       
                      |-----------workstation1$
                      |-----------workstation2$
 |-----------Domains             
 |-------------sambaDomainName

**Step1**

Delete all runtime files from prior Samba operation by executing;

 [root@node1]#   rm //*t//
 [root@node1]#   rm /amb///
 [root@node1]#   rm /amb///
 [root@node1]#   rm /amb///

**Step2** 

Delete any previous LDAP database

 [root@node1]#  cd /dap//
 [root@node1]#  rm –rf *

**Step3**

Login to node2 - the backup domain controller, and do the same. 

**Step4**

 [root@node1 ~]# net getlocalsid
 SID for domain NODE1 is:H1-5-21-3809161173-2687474671-1432921517

Your SID will differ to the one above; you will need to alter the preload LDIF as per below.

**Step5**

Login to your backup domain controller (node2) and type the following command using the SID obtained from step4.

 [root@node2 ~]# net setlocalsid S-1-5-21-3809161173-2687474671-1432921517