6.1 Requirements
    <namespace>0</namespace>
<last_edited>2007-01-25T13:52:01Z</last_edited>
<last_editor>Asender</last_editor>

`1.0: Configuring Samba` 

`2.0: Configuring LDAP` 

`3.0: Initialization LDAP Database` 

`4.0: User Management` 

`5.0: Heartbeat HA Configuration` 

`6.0: DRBD` 

`7.0: BIND DNS`

----

<u>**Table of Contents**</u>

`6.1 Requirements` 

`6.2 Installation` 

`6.3 Configuration` 

`6.3.1 drbd.conf` 

`6.3.2 Initialization` 

`6.4 Testing` 

You will need to install the DRBD kernel Module. We will build our own RPM kernel modules so it is optimized for our architecture. 

I have tested many different kernels with DRBD, some are not stable so you will need to check Google to make sure your kernel is compatible with the particular DRBD release, most of the time this isn’t an issue.

Both the following kernels are recommended for Fedora Core 4; up to version drbd-0.7.23 I have used. 

kernel-smp-2.6.14-1.1656_FC4
kernel-smp-2.6.11-1.1369_FC4

Please browse this list http://www.linbit.com/support/drbd-current/ and look for packages available.

**Step1**

Get a serial cable and connect it to each nodes com1 port. 
Execute the following; you may see a lot of garbage on the screen.

 [root@node1 ~]# cat </dev/ttyS0 

**Step2**

You may have to repeat the below a couple of times in rapid succession to see the output on node1.

 [root@node2 ~]# echo hello >/dev/ttyS0