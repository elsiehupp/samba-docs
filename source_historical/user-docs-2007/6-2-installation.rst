6.2 Installation
    <namespace>0</namespace>
<last_edited>2007-01-25T13:52:16Z</last_edited>
<last_editor>Asender</last_editor>

`1.0:ing Samba` 

`2.0:ing LDAP` 

`3.0:zation LDAP Database` 

`4.0:agement` 

`5.0:t HA Configuration` 

`6.0:

`7.0:`

----

<u>**Table of Contents**</u>

`6.1 Requirements` 

`6.2 Installation` 

`6.3 Configuration` 

`6.3.1 drbd.conf` 

`6.3.2 Initialization` 

`6.4 Testing` 

**Step1**

Extract the latest stable version of DRBD.

 [root@node1 stable]# tar zxvf drbd-0.7.20.tar.gz
 [root@node1 stable]# cd drbd-0.7.20
 [root@node1 drbd-0.7.20]#

**Step2**

It is nice to make your own rpm for your distribution. It makes upgrades seamless.

This will give us a RPM build specifically to our kernel, it may take some time.

 [root@node1 drbd-0.7.20]# make
 [root@node1 drbd-0.7.20]# make rpm

**Step3**

 [root@node1 drbd-0.7.20]# cd dist RPMS/i386/

 [root@node1 i386]# ls
 drbd-0.7.20-1.i386.rpm
 drbd-debuginfo-0.7.20-1.i386.rpm
 drbd-km-2.6.14_1.1656_FC4smp-0.7.20-1.i386.rpm

**Step4**

We will now install DRBD and our Kernel module which we built earlier.

 [root@node1 i386]# rpm -Uvh drbd-0.7.20-1.i386.rpm drbd-debuginfo-0.7.20-1.i386.rpm 
    rbd-km-2.6.14_1.1656_FC4smp-0.7.20-1.i386.rpm

**Step5**

Login to node 2 the backup domain controller and do the same.