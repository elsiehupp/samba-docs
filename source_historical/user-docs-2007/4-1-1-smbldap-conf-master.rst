4.1.1 smbldap.conf Master
    <namespace>0</namespace>
<last_edited>2007-01-25T13:45:06Z</last_edited>
<last_editor>Asender</last_editor>

`1.0:ing Samba` 

`2.0:ing LDAP` 

`3.0:zation LDAP Database` 

`4.0:agement` 

`5.0:t HA Configuration` 

`6.0:

`7.0:`

----

<u>**Table of Contents**</u>

`4.1 smbldap-tools` 

`4.1.1 smbldap.conf Master` 

`4.1.2 smbldap.conf Slave` 

Because we did not use smbldap-tools to populate our database, we must manually configure the smbldap.conf. This configuration file only applies to smbldap-tools-0.9.1-1. If you are using a different version alterations will need to be made.

We will need to configure this file to suit.

 # /etc/opt/IDEALX/sbin/smbldap.conf
 # smbldap-tools.conf :D configuration file for smbldap-tools

 #  This code was developped by IDEALX (http://IDEALX.org/) and
 #  contributors (their names can be found in the CONTRIBUTORS file).
 #
 #                 Copyright (C) 2001-2002 IDEALX
 #
 #  This program is free software; you can redistribute it and/or
 #  modify it under the terms of the GNU General Public License
 #  as published by the Free Software Foundation; either version 2
 #  of the License, or (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU General Public License for more details.
 #
 #  You should have received a copy of the GNU General Public License
 #  along with this program; if not, write to the Free Software
 #  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
 #  USA.

 #  Purpose :
 #       . be the configuration file for all smbldap-tools scripts

 ##############################################################################
 #
 # General Configuration
 #
 ##############################################################################

 # Put your own SID. To obtain this number do:t getlocalsid".
 # If not defined, parameter is taking from "net getlocalsid" return

 SID="S-1-5-21-3809161173-2687474671-1432921517"

 # Domain name the Samba server is in charged.
 # If not defined, parameter is taking from smb.conf configuration file
 # Ex:ain="IDEALX-NT"
 sambaDomain="DDESIGN"

 ##############################################################################
 #
 # LDAP Configuration
 #
 ##############################################################################
     # Notes:o dual ldap servers backend for Samba, you must patch
 # Samba with the dual-head patch from IDEALX. If not using this patch
 # just use the same server for slaveLDAP and masterLDAP.
 # Those two servers declarations can also be used when you have
 # . one master LDAP server where all writing operations must be done
 # . one slave LDAP server where all reading operations must be done
 #   (typically a replication directory)
 # Slave LDAP server
 # Ex:P=127.0.0.1
 # If not defined, parameter is set to "127.0.0.1"
 slaveLDAP="192.168.0.3"

 # Slave LDAP port
 # If not defined, parameter is set to "389"
 slavePort="389"

 # Master LDAP server:or write operations
 # Ex:AP=127.0.0.1
 # If not defined, parameter is set to "127.0.0.1"
 masterLDAP="127.0.0.1"

 # Master LDAP port
 # If not defined, parameter is set to "389"
 masterPort="389"

 # Use TLS for LDAP
 # If set to 1, this option will use start_tls for connection
 # (you should also used the port 389)
 # If not defined, parameter is set to "1"
 ldapTLS="0"
     # How to verify the server's certificate (none, optional or require)
 # see "man Net::t_tls section for more details
 verify=""

 # CA certificate
 # see "man Net::t_tls section for more details
 cafile=""
     # certificate to use to connect to the ldap server
 # see "man Net::t_tls section for more details
 clientcert=""

 # key certificate to use to connect to the ldap server
 # see "man Net::t_tls section for more details
 clientkey=""

 # LDAP Suffix
 # Ex:c=IDEALX,dc=ORG
 suffix="dc=differentialdesign,dc=org"

 # Where are stored Users
 # Ex:"ou=Users,dc=IDEALX,dc=ORG"
 # Warning:ix' is not set here, you must set the full dn for usersdn
 usersdn="ou=People,ou=Users,${suffix}"

 # Where are stored Computers
 # Ex:sdn="ou=Computers,dc=IDEALX,dc=ORG"
 # Warning:ix' is not set here, you must set the full dn for computersdn
 computersdn="ou=Computers,ou=Users,${suffix}"

 # Where are stored Groups
 # Ex:="ou=Groups,dc=IDEALX,dc=ORG"
 # Warning:ix' is not set here, you must set the full dn for groupsdn
 groupsdn="ou=Groups,${suffix}"

 # Where are stored Idmap entries (used if samba is a domain member server)
 # Ex:="ou=Idmap,dc=IDEALX,dc=ORG"
 # Warning:ix' is not set here, you must set the full dn for idmapdn
 idmapdn="ou=Idmap,${suffix}"

 # Where to store next uidNumber and gidNumber available for new users and groups
 # If not defined, entries are stored in sambaDomainName object.
 # Ex:xIdPooldn="sambaDomainName=${sambaDomain},${suffix}"
 # Ex:xIdPooldn="cn=NextFreeUnixId,${suffix}"
 sambaUnixIdPooldn="sambaDomainName=DDESIGN,ou=Domains,${suffix}"

 # Default scope Used
 scope="sub"

 # Unix password encryption (CRYPT, MD5, SMD5, SSHA, SHA, CLEARTEXT)
 hash_encrypt="MD5"

 # if hash_encrypt is set to CRYPT, you may set a salt format.
 # default is "%s", but many systems will generate MD5 hashed
 # passwords if you use "$1$%.8s". This parameter is optional!

 crypt_salt_format=""

 ##############################################################################
 #
 # Unix Accounts Configuration
 #
 ##############################################################################

 # Login defs 
 # Default Login Shell
 # Ex:nShell="/bin/bash"
 userLoginShell="/bin/bash"

 # Home directory
 # Ex:="/home/%U"
 userHome="/data/home/%U"

 # Default mode used for user homeDirectory
 userHomeDirectoryMode="700"

 # Gecos
 userGecos="System User"

 # Default User (POSIX and Samba) GID
 defaultUserGid="513"

 # Default Computer (Samba) GID
 defaultComputerGid="515"

 # Skel dir
 skeletonDir="/etc/skel"

 # Default password validation time (time in days) Comment the next line if
 # you don't want password to be enable for defaultMaxPasswordAge days (be
 # careful to the sambaPwdMustChange attribute's value)
 defaultMaxPasswordAge="45"

 ##############################################################################
 #
 # SAMBA Configuration
 #
 ##############################################################################

 # The UNC path to home drives location (%U username substitution)
 # Just set it to a null string if you want to use the smb.conf 'logon home'
 # directive and/or disable roaming profiles
 # Ex:ome="\\PDC-SMB3\%U"
 userSmbHome="\\192.168.0.4\%U"

 # The UNC path to profiles locations (%U username substitution)
 # Just set it to a null string if you want to use the smb.conf 'logon path'
 # directive and/or disable roaming profiles
 # Ex:ile="\\PDC-SMB3\profiles\%U"
 userProfile="\\192.168.0.4\profiles\%U"

 # The default Home Drive Letter mapping
 # (will be automatically mapped at logon time if home directory exist)
 # Ex:Drive="H:":
 userHomeDrive="H:

 # The default user netlogon script name (%U username substitution)
 # if not used, will be automatically username.cmd
 # make sure script file is edited under dos
 # Ex:pt="startup.cmd" # make sure script file is edited under dos
 userScript="%U.bat"

 # Domain appended to the users "mail"-attribute
 # when smbldap-useradd -M is used
 # Ex:in="idealx.com"
 mailDomain="differentialdesign.org"

 ##############################################################################
 #
 # SMBLDAP-TOOLS Configuration (default are ok for a RedHat)
 #
 ##############################################################################

 # Allows not to use smbpasswd (if with_smbpasswd == 0 in smbldap_conf.pm) but
 # prefer Crypt::
 with_smbpasswd="0"
 smbpasswd="/usr/bin/smbpasswd"

 # Allows not to use slappasswd (if with_slappasswd == 0 in smbldap_conf.pm)
 # but prefer Crypt::
 with_slappasswd="0"
 slappasswd="/usr/sbin/slappasswd"

 # comment out the following line to get rid of the default banner
 # no_banner="1"