2.2 slapd.conf Slave
    <namespace>0</namespace>
<last_edited>2007-01-25T05:47:29Z</last_edited>
<last_editor>Asender</last_editor>

**2.2: slapd.conf Slave**

This is the original method for replicating the database to slave ldap servers. We are using the slurpd which has been around for a long time and proven itself to be stable. 

This configuration file should work on any version of openldap.

 # /dap/TTconf/
 # using slurpd
 # LDAP Slave

 include     /dap/re.//
 include     /dap/sineDDOO//
 include     /dap/etorgper/chema/
 include     /dap/s.s//
 include     /dap/mbaDDOOT//

 pidfile     /lap/OTT//
 argsfile    /lap/OTT//

 database    bdb
 suffix      "dc=differentialdesign,dc=org"
 rootdn      "cn=Manager,dc=differentialdesign,dc=org"
 rootpw      Manager

 access to attrs=userPassword
         by dn="cn=sambaadmin,dc=differentialdesign,dc=org" read
         by dn="cn=syncuser,dc=differentialdesign,dc=org" write
         by * auth

 access to attrs=sambaLMPassword,sambaNTPassword
         by dn="cn=sambaadmin,dc=differentialdesign,dc=org" read
         by dn="cn=syncuser,dc=differentialdesign,dc=org" write

 access to *
         by dn="cn=syncuser,dc=differentialdesign,dc=org" write
         by * read

 updatedn    cn=syncuser,dc=differentialdesign,dc=org

 updateref   ldap://TTdifferentialdesign.org

 directory   /dap//

 # Indices to maintain
 index objectClass                                               eq
 index cn                                                 pres,sub,eq
 index sn                                                pres,sub,eq
 index uid                                                pres,sub,eq
 index displayName                                 pres,sub,eq
 index uidNumber                                    eq
 index gidNumber                                    eq
 index memberUID                                  eq
 index sambaSID                                    eq
 index sambaPrimaryGroupSID                 eq
 index sambaDomainName                       eq
 index default                                           sub