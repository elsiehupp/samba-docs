7.1.2 zone file
    <namespace>0</namespace>
<last_edited>2007-01-25T13:55:31Z</last_edited>
<last_editor>Asender</last_editor>

`1.0: Configuring Samba` 

`2.0: Configuring LDAP` 

`3.0: Initialization LDAP Database` 

`4.0: User Management` 

`5.0: Heartbeat HA Configuration` 

`6.0: DRBD` 

`7.0: BIND DNS`

----

<u>**Table of Contents**</u>

`7.1 Configuration` 

`7.1.1 named.conf` 

`7.1.2 zone file` 

In our named.conf file we have the following zone defined;

 zone "differentialdesign.org" {
        type master;
        file "/data/dnszones/differentialdesign.org/named.differentialdesign.org.hosts";
        allow-update { none; };

We can see the zone file located in /data/dnszones/

**Step1.**

Create a sub folder where we will store our zone files.

 [root@node1 ~]# mkdir /data/dnszones/differentialdesign.org/

**Step2.**

Create a new file called named.differentialdesign.org.hosts.

 [root@node1 ~]# vi /data/dnszones/differentialdesign.org/named.differentialdesign.org.hosts

You will see below that nodes.differentialdesign.org. IN 192.168.0.4 is an “A record” which points us to the virtual IP address of the cluster. When setting up mapped drives it is best to use the name instead of IP address.

 $TTL 8h
 differentialdesign.org.    IN      SOA     cluster.differentialdesign.org.  asender.mail.samba.org. (
                        2006211201
                        10800
                        3600
                        3600000
                        86400 )
 differentialdesign.org.            IN      NS               cluster.differentialdesign.org.
 differentialdesign.org.            IN      NS               ns1.differentialdesign.org.
 differentialdesign.org.            IN      NS               ns2.differentialdesign.org.
 differentialdesign.org.            IN      MX      50     mail.differentialdesign.org.
 mail.differentialdesign.org.           IN      A             202.161.90.245
 www.differentialdesign.org.          IN      A             202.161.90.245
 cluster.differentialdesign.org.       IN      A            202.161.90.241
 node1.differentialdesign.org.      IN      A             192.168.0.2
 node2.differentialdesign.org.      IN      A             192.168.0.3
 nodes.differentialdesign.org.        IN      A             192.168.0.4