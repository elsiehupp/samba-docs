2.4 ldap.conf Slave
    <namespace>0</namespace>
<last_edited>2007-01-25T06:04:51Z</last_edited>
<last_editor>Asender</last_editor>

**2.4: ldap.conf Slave**

 #/etc/ldap.conf
 # LDAP Slave

 host    node2.differentialdesign.org node1.differentialdesign.org
 base    dc=differentialdesign,dc=org
 binddn  cn=Manager,dc=differentialdesign,dc=org
 bindpw  Manager

 pam_password exop

 nss_base_passwd ou=People,ou=Users,dc=differentialdesign,dc=org?one
 nss_base_shadow ou=People,ou=Users,dc=differentialdesign,dc=org?one
 nss_base_passwd ou=Computers,ou=Users,dc=differentialdesign,dc=org?one
 nss_base_shadow ou=Computers,ou=Users,dc=differentialdesign,dc=org?one
 nss_base_group  ou=Groups,dc=differentialdesign,dc=org?one
 ssl     no