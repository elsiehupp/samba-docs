2.0: Configuring LDAP
    <namespace>0</namespace>
<last_edited>2007-09-21T03:LL:_edited>
<last_editor>Asender</last_editor>

**`Replicated Failover Domain Controller and file server using LDAP`**

`1.P.Configuring Samba` 

`2.P.Configuring LDAP` 

`3.P.Initialization LDAP Database` 

`4.P.User Management` 

`5.P.Heartbeat HA Configuration` 

`6.P.DRBD` 

`7.P.BIND DNS`

----

`2.P.Configuring LDAP` 
------------------------

This chapter aims to give end users working configurations examples. We provide 3 different replication technologies which can be put in place in order to achieve high availability. .Slurpd, syncrepl and its successor delta syncrepl..

It is necessary to use LDAP as our database backend for Samba when using Backup Domain Controllers. This is the recommended design to replicate records to BDC(s)..

There are two methods for providing replication, the first and original design was using openldap’s “slurpd” to provide Master / Slave operation, the database is pushed to slaves defined in slapd. on the master LDAP server; here is an example of the original way defined in [http://wiki.samb.index..0._Confi._LDAP.s.onf_Master_slurpdSS.E.  .pd.conf Master .u...

 replica     host=192..3:..
             suffix="dc=differentialdesign,dc=org"
             binddn="cn=syncuser,dc=differentialdesign,dc=org"
             bindmethod=simple credentials=SyncUser

In order to bind to the database, the slave replicas will need to use “syncusers’s” password defined above as **“credentials=SyncUser“**. Initially, you'll need to populate the slave database as a manual step as defined in section [http://wiki.samb.index..0:_Initi.Datab.5._Database_ReplicationS.C. Database Repli.]. .

**Openldap 2. Original Style Replication Configuration**

**Master**

**Slave(s)**

A master LDAP database that pushes its database to the slaves providing a persistent connection. 

The slave LDAP server requires no additional configuration, as long as it has correct ACLs set in the database and slapd..

The main restriction with using this original design is the ldap database needs to be restarted on both the master and the slave when adding additional replicas. It is also no longer under active development..

In version Openldap 2. "delta-syncrepl" was invented as the original syncrepl method used too much network bandwidth. .Developers recommend you use the latest version of Openldap (as version 2.2 . decommissioned over a year ago)..

**Openldap 2. .Esyncrepl/delta-syncrepl LDAP Replication Configuration**

**Provider**

**Consumers(s)**

The provider LDAP server does not need to be restarted when adding additional slave servers. Configurations will differ depending on your replication methods chosen for syncrepl/delta-syncrepl. .

The consumer no longer needs to have its database manually added for initial population. It can request an update at a set interval, or provide a pesistent connection. .For persistent connections, delta-syncrepl is the recommended choice. .-synrepl was invented as an efficient means for database replication over WAN links where bandwidth was an issue..

These modes of operation are known as syncrepl; which is included in the ldap daemon. This means we no longer need to run the additional slurpd daemon to replicate the database. .

On the consumer syncrepl needs to know what mode to operate in: **“refeshOnly”** operation where the consumer requests an update from the provider at set time interval defined as **“interval=00:00:10:00:CC:PA: pull the provider every 10 minutes. The more desirable way is to use **“refrshAndPersist”** which provides a persistant connection. .Instead of using a time interval to poll the provider we have the parameter   **“retry="30 10 300 +"** which means it will retry 10 times every 30 seconds, then every 300 seconds if connection is lost;  **“+”** indicates indefinite number of retries..

[http://www.dap.org/.min23/ Please read the official OpenLDAP documentation for detailed explanations of the material covered here.].

`2.P.Installing LDAP` 
------------------------

This section is designed for configuring LDAP and our overlays suited for delta-syncrepl replication.

People often run into problems installing ldap; however if you read the documentation and understand what you are doing things can be quite painless.

It's a good idea to understand modules and their file locations so you can properly setup your slapd. .

While we recommend compiling from source, you can obtain binary Openldap-2. RPM packages here for RHE4, CentOS and all versions of Fedora:

For x86 : http:SSLLAASS:HHanorien.ck.ac.u.or.an/rhel4/openldap/

For x86-64: http:SSLLAASS:HHanorien.ck.ac.u.or.an/rhel4-x86_64/openldap/

Fedora 7 now has a bug fix in openldap-2.3.  . #246036 which means you can now use yum to download the latest openldap and it will include the needed modules so there is no need to compile from source..

We will compile LDAP from source so we can use the latest version of Openldap. When compiling from source, remove any previous versions to avoid complications. .Get the latest version of Openldap here http://www.openldap..ftware/d.d/

**Step1.

Extract the contents of the file in a suitable location; I put it in /programs/openldap/release.

 [root@node1 release]# tar zxvf openldap-2.t..

**Step2.

Change to the openldap directory.

 [root@node1 release]# cd openldap-2..

**Step3.

This will take some time; when it has completed it will ask us to run "make depend"

 [root@node1 openldap-2.#.CCEE./configur. --prefix=/usr/local --enable-slapd --enable-syslog --with-cyrus-sasl=yes --enable-dynamic --enable-rewrite --disable-ipv6 --disable-shell --disable-sql --with-threads --enable-modules --enable-backends=mod --enable-overlays=mod --with-tls --enable-wrapper

If you run into any dependency problems here, you will need to resolve the issue before continuing.

<font color=red>*configure: error:  not locate libtool ltdl.t;/font>

You may need to recompile libtools as below.

 [root@node1 openldap-2.#.CCEEcd /usr/share/libtool/libltdl
 [root@node1 libltdl]# .igure
 [root@node1 libltdl]# make
 [root@node1 libltdl]# make install

**Step4.

Please run "make depend" to build dependencies

 [root@node1 openldap-2.#.CCEEmake depend

**Step5.

Now lets compile Openldap.

 [root@node1 openldap-2.#.CCEEmake

**Step6.

This step requires root privileges and will install Openldap onto our system.

 [root@node1 openldap-2.#.CCEEmake install

Take particular note where the default installs to: "/usr/local/etc/openldap/" as this is where we configure the slapd..

So we can provide certain features (such as delta-syncrepl, accesslog and the BDB database); we need to configure the slapd. to support our overlays..

The module path is set to "/usr/local/libexec/openldap". This is where synprov, accesslog and back_bdb modules are located. .There are also many more modules available that you can load..

**Step7.

Now we have compiled Openldap it is time to configure our slapd. on our primary node1; use this configuration file: [http:SSLLAASS:HHwiki.samb.index..0._Confi._LDAP....conf_Master_delta-.p.l. .apd.conf Master de.ncrepl .ap2.3 slapd.conf]..

**Step8.

Create the directories needed as specified in our delta-syncrepl slapd. .If you do not create these directories as specified in slapd.conf, .you will not be able to start ldap and you will get errors..

 [root@node1 ~]# mkdir /usr/local/var/openldap-data
 [root@node1 ~]# mkdir /usr/local/var/openldap-data/accesslog
 [root@node1 ~]# mkdir /usr/local/var/run/slapd/

**Step9.

Login to node2 and repeat the above steps as done on the provider. 

Configure your slapd. on your consumer as per here: [http:SSLLAASS:HHwiki.samb.index..0._Confi._LDAP....conf_Slave_delta-s.l.d.  ..2 slapd.conf Sla. delta-.p. Openldap2.3..

`2.P.slapd.conf .aster slurpd` 
------------------------

This is the original method for replicating the database to slave ldap servers. We are using the slurpd which has been around for a long time and proven itself to be stable. .

This configuration file should work on any version of Openldap.

 # /etc/openldap/slapd.
 # using slurpd
 # LDAP Master

 include     /etc/openldap/schema/core.a
 include     /etc/openldap/schema/cosine.a
 include     /etc/openldap/schema/inetorgperson.a
 include     /etc/openldap/schema/nis.a
 include     /etc/openldap/schema/samba.a

 pidfile     /var/run/slapd/slapd.
 argsfile    /var/run/slapd/slapd.

 database    bdb
 suffix       "dc=differentialdesign,dc=org"
 rootdn      "cn=Manager,dc=differentialdesign,dc=org"
 rootpw      Manager
 directory   /var/lib/ldap

 replica  host=node2.rentialdesign.org:.
            suffix="dc=differentialdesign,dc=org"
            binddn="cn=syncuser,dc=differentialdesign,dc=org"
            bindmethod=simple credentials=SyncUser

 replogfile  /var/lib/ldap/replogfile

 access to attrs=userPassword
         by self write
         by dn="cn=sambaadmin,dc=differentialdesign,dc=org" write
         by dn="cn=syncuser,dc=differentialdesign,dc=org" read
         by * auth
     access to attrs=sambaLMPassword,sambaNTPassword
         by dn="cn=sambaadmin,dc=differentialdesign,dc=org" write
         by dn="cn=syncuser,dc=differentialdesign,dc=org" read

 access to *
         by dn="cn=sambaadmin,dc=differentialdesign,dc=org" write
         by dn="cn=syncuser,dc=differentialdesign,dc=org" read
         by * read

 # Indices to maintain
 index objectClass           eq
 index cn                    pres,sub,eq
 index sn                    pres,sub,eq
 index uid                   pres,sub,eq
 index displayName           pres,sub,eq
 index uidNumber             eq
 index gidNumber             eq
 index memberUID             eq
 index sambaSID              eq
 index sambaPrimaryGroupSID  eq
 index sambaDomainName       eq
 index default               sub

`2.S.E..conf . syncrepl Openldap2.2` ==.

This is the slapd. master ldap file; we are using syncrepl instead of slurpd which is the traditional method. .

This configuration file is specifically designed for openldap 2. and supports syncrepl refreshOnly mode..

 # slapd. Master syncrepl Openldap2.2.
 # Provider

 include     /etc/openldap/schema/core.a
 include     /etc/openldap/schema/cosine.a
 include     /etc/openldap/schema/inetorgperson.a
 include     /etc/openldap/schema/nis.a
 include     /etc/openldap/schema/samba.a

 pidfile     /var/run/slapd/slapd.
 argsfile    /var/run/slapd/slapd.

 database    bdb
 suffix      "dc=differentialdesign,dc=org"
 rootdn      "cn=Manager,dc=differentialdesign,dc=org"
 rootpw      Manager
 directory   /var/lib/ldap

 access to attrs=userPassword
         by self write
         by dn="cn=sambaadmin,dc=differentialdesign,dc=org" write
         by dn="cn=syncuser,dc=differentialdesign,dc=org" read
         by * auth

 access to attrs=sambaLMPassword,sambaNTPassword
         by dn="cn=sambaadmin,dc=differentialdesign,dc=org" write
         by dn="cn=syncuser,dc=differentialdesign,dc=org" read

 access to *
         by dn="cn=sambaadmin,dc=differentialdesign,dc=org" write
         by dn="cn=syncuser,dc=differentialdesign,dc=org" read
         by * read

 # Indices to maintain

 index objectClass           eq
 index cn                    pres,sub,eq
 index sn                    pres,sub,eq
 index uid                   pres,sub,eq
 index displayName           pres,sub,eq
 index uidNumber             eq
 index gidNumber             eq
 index memberUID             eq
 index sambaSID              eq
 index sambaPrimaryGroupSID  eq
 index sambaDomainName       eq
 index default               sub

`2.S.E..conf . delta-syncrepl Openldap2.3` ==.

This configuration file is designed to support Openldap’s newest features.  We will be using delta-syncrepl which supports refreshAndPersist performance similar to that of slurpd. .

The below slapd. will only run on Openldap 2.3.SS.E. is assumed that you have compiled from source. .

Take note of the “modulepath /usr/local/libexec/openldap” in the below file, you will need to change this to where you have your modules located.

 #slapd. Master delta syncrepl Openldap2.3.
 #path: /usr/local/etc/openldap/slapd.
 #provider

 include     /usr/local/etc/openldap/schema/core.a
 include     /usr/local/etc/openldap/schema/cosine.a
 include     /usr/local/etc/openldap/schema/inetorgperson.a
 include     /usr/local/etc/openldap/schema/nis.a
 include     /usr/local/etc/openldap/schema/samba.a

 #If your slapd was configured with dynamic module support, and your backends and overlays are not statically compiled, you will need these module statements.
 modulepath /usr/local/libexec/openldap
 moduleload syncprov.
 moduleload accesslog.
 moduleload back_bdb.

 pidfile     /usr/local/var/run/slapd/slapd.
 argsfile    /usr/local/var/run/slapd/slapd.
     # Accesslog database definitions
 database    bdb
 suffix      cn=accesslog
 directory   /usr/local/var/openldap-data/accesslog
 rootdn      cn=accesslog
 index default eq
 index entryCSN,objectClass,reqEnd,reqResult,reqStart

 overlay syncprov
 syncprov-nopresent TRUE
 syncprov-reloadhint TRUE

 # Samba Primary Database differentialdesign.
 database    bdb
 suffix      "dc=differentialdesign,dc=org"
 directory   /usr/local/var/openldap-data
 rootdn      "cn=Manager,dc=differentialdesign,dc=org"
 rootpw      Manager
 index entryCSN eq
 index entryUUID eq

 overlay syncprov
 syncprov-checkpoint 1000 60

 # accesslog overlay definitions for primary database
 overlay accesslog
 logdb cn=accesslog
 logops writes
 logsuccess TRUE
 # scan the accesslog DB every day, and purge entries older than 7 days
 logpurge 07+00 01+00:00:

 # give syncuser DN limitless searches
 limits dn.="cn=syncuser,dc=differentialdesign,dc=org" time.soft.ited time.hard=unli. size.soft=unlimited.CCEEsize.hard=unlimited.

 access to attrs=userPassword
        by self write
        by dn="cn=sambaadmin,dc=differentialdesign,dc=org" write
        by dn="cn=syncuser,dc=differentialdesign,dc=org" read
        by * auth

 access to attrs=sambaLMPassword,sambaNTPassword
        by dn="cn=sambaadmin,dc=differentialdesign,dc=org" write
        by dn="cn=syncuser,dc=differentialdesign,dc=org" read

 access to *
        by dn="cn=sambaadmin,dc=differentialdesign,dc=org" write
        by dn="cn=syncuser,dc=differentialdesign,dc=org" read
        by * read

 # Indices to maintain
 index objectClass             eq
 index cn                      pres,sub,eq
 index sn                      pres,sub,eq
 index uid                     pres,sub,eq
 index displayName             pres,sub,eq
 index uidNumber               eq
 index gidNumber               eq
 index memberUID               eq
 index sambaSID                eq
 index sambaPrimaryGroupSID    eq
 index sambaDomainName         eq
 index default                 sub

`2.P.slapd.conf .lave slurpd` 
------------------------

This is the original method for replicating the database to slave ldap servers. We are using the slurpd which has been around for a long time and proven itself to be stable. .

This configuration file should work on any version of openldap.

 # /etc/openldap/slapd.
 # using slurpd
 # LDAP Slave

 include     /etc/openldap/schema/core.a
 include     /etc/openldap/schema/cosine.a
 include     /etc/openldap/schema/inetorgperson.a
 include     /etc/openldap/schema/nis.a
 include     /etc/openldap/schema/samba.a

 pidfile     /var/run/slapd/slapd.
 argsfile    /var/run/slapd/slapd.

 database    bdb
 suffix      "dc=differentialdesign,dc=org"
 rootdn      "cn=Manager,dc=differentialdesign,dc=org"
 rootpw      Manager

 access to attrs=userPassword
         by dn="cn=sambaadmin,dc=differentialdesign,dc=org" read
         by dn="cn=syncuser,dc=differentialdesign,dc=org" write
         by * auth

 access to attrs=sambaLMPassword,sambaNTPassword
         by dn="cn=sambaadmin,dc=differentialdesign,dc=org" read
         by dn="cn=syncuser,dc=differentialdesign,dc=org" write

 access to *
         by dn="cn=syncuser,dc=differentialdesign,dc=org" write
         by * read

 updatedn    cn=syncuser,dc=differentialdesign,dc=org

 updateref   ldap://node1.rentialdesign.org.

 directory   /var/lib/ldap

 # Indices to maintain
 index objectClass                          eq
 index cn                                   pres,sub,eq
 index sn                                   pres,sub,eq
 index uid                                  pres,sub,eq
 index displayName                          pres,sub,eq
 index uidNumber                            eq
 index gidNumber                            eq
 index memberUID                            eq
 index sambaSID                             eq
 index sambaPrimaryGroupSID                 eq
 index sambaDomainName                      eq
 index default                              sub

`2.S.E..conf . syncrepl Openldap2.2` ==.

This is the configuration file for openldap version 2. using the syncrepl method refreshOnly..

 # slapd. Slave syncrepl Openldap2.2.
 # LDAP Consumer
     include     /etc/openldap/schema/core.a
 include     /etc/openldap/schema/cosine.a
 include     /etc/openldap/schema/inetorgperson.a
 include     /etc/openldap/schema/nis.a
 include     /etc/openldap/schema/samba.a

 pidfile     /var/run/slapd/slapd.
 argsfile    /var/run/slapd/slapd.
     database    bdb
 suffix      "dc=differentialdesign,dc=org"
 rootdn      "cn=Manager,dc=differentialdesign,dc=org"
 rootpw      Manager
 directory   /var/lib/ldap

 syncrepl
    rid=0
        provider=ldap://node1.rentialdesign.org:.
        binddn="cn=syncuser,dc=differentialdesign,dc=org"
        bindmethod=simple
        credentials=SyncUser
        searchbase="dc=differentialdesign,dc=org"
        filter="(objectClass=*)"
        attrs="*"
        schemachecking=off
        scope=sub
        type=refreshOnly
        interval=00:SP::

 access to attrs=userPassword
         by dn="cn=sambaadmin,dc=differentialdesign,dc=org" read
         by dn="cn=syncuser,dc=differentialdesign,dc=org" write
         by * auth
     access to attrs=sambaLMPassword,sambaNTPassword
         by dn="cn=sambaadmin,dc=differentialdesign,dc=org" read
         by dn="cn=syncuser,dc=differentialdesign,dc=org" write

 access to *
         by dn="cn=syncuser,dc=differentialdesign,dc=org" write
         by * read

 # Indices to maintain
 index objectClass           eq
 index cn                    pres,sub,eq
 index sn                    pres,sub,eq
 index uid                   pres,sub,eq
 index displayName           pres,sub,eq
 index uidNumber             eq
 index gidNumber             eq
 index memberUID             eq
 index sambaSID              eq
 index sambaPrimaryGroupSID  eq
 index sambaDomainName       eq
 index default               sub

`2.S.E..conf . delta-syncrepl Openldap2.3` ==.

 # slapd. delta synrepl Openldap2.3.
 # LDAP Consumer
     include     /usr/local/etc/openldap/schema/core.a
 include     /usr/local/etc/openldap/schema/cosine.a
 include     /usr/local/etc/openldap/schema/inetorgperson.a
 include     /usr/local/etc/openldap/schema/nis.a
 include     /usr/local/etc/openldap/schema/samba.a

 modulepath /usr/local/libexec/openldap
 moduleload back_bdb.

 pidfile     /usr/local/var/run/slapd/slapd.
 argsfile    /usr/local/var/run/slapd/slapd.
     database    bdb
 suffix      "dc=differentialdesign,dc=org"
 directory   /usr/local/var/openldap-data
 rootdn      "cn=Manager,dc=differentialdesign,dc=org"
 rootpw      Manager

 # syncrepl directives
 syncrepl  rid=0
        provider=ldap://node1.rentialdesign.org:.
        bindmethod=simple
        binddn="cn=syncuser,dc=differentialdesign,dc=org"
        credentials=SyncUser
        searchbase="dc=differentialdesign,dc=org"
        logbase="cn=accesslog"
        logfilter="(&(objectClass=auditWriteObject)(reqResult=0))"
        schemachecking=on
        type=refreshAndPersist
        retry="60 +"
        syncdata=accesslog
     access to attrs=userPassword
         by dn="cn=sambaadmin,dc=differentialdesign,dc=org" read
         by dn="cn=syncuser,dc=differentialdesign,dc=org" write
         by * auth

 access to attrs=sambaLMPassword,sambaNTPassword
         by dn="cn=sambaadmin,dc=differentialdesign,dc=org" read
         by dn="cn=syncuser,dc=differentialdesign,dc=org" write

 access to *
         by dn="cn=sambaadmin,dc=differentialdesign,dc=org" read
         by dn="cn=syncuser,dc=differentialdesign,dc=org" write
         by * read

 updateref   ldap://node1.rentialdesign.org.

 # Indices to maintain
 index objectClass           eq
 index cn                    pres,sub,eq
 index sn                    pres,sub,eq
 index uid                   pres,sub,eq
 index displayName           pres,sub,eq
 index uidNumber             eq
 index gidNumber             eq
 index memberUID             eq
 index sambaSID              eq
 index sambaPrimaryGroupSID  eq
 index sambaDomainName       eq
 index default               sub

`2.P.ldap.conf .aster` 
------------------------

You will notice below in the host options that we use both IP addresses of the Primary and Secondary LDAP database servers. This serves as a failover option if the local LDAP database is inaccessible. . The same applies for the Slave LDAP configuration; 2.4: ldap.c. .

 #/etc/ldap.
 # LDAP Master

 host    node1.rentialdesign.orgS.CEEnode2.different.ign.org.
 base    dc=differentialdesign,dc=org
 binddn  cn=Manager,dc=differentialdesign,dc=org
 bindpw  Manager 

 bind_policy soft 
 pam_password exop

 nss_base_passwd ou=People,ou=Users,dc=differentialdesign,dc=org?one
 nss_base_shadow ou=People,ou=Users,dc=differentialdesign,dc=org?one
 nss_base_passwd ou=Computers,ou=Users,dc=differentialdesign,dc=org?one
 nss_base_shadow ou=Computers,ou=Users,dc=differentialdesign,dc=org?one
 nss_base_group  ou=Groups,dc=differentialdesign,dc=org?one
 ssl     no

`2.P.ldap.conf .lave` 
------------------------

 #/etc/ldap.
 # LDAP Slave

 host    node2.rentialdesign.orgS.CEEnode1.different.ign.org.
 base    dc=differentialdesign,dc=org
 binddn  cn=Manager,dc=differentialdesign,dc=org
 bindpw  Manager

 bind_policy soft 
 pam_password exop

 nss_base_passwd ou=People,ou=Users,dc=differentialdesign,dc=org?one
 nss_base_shadow ou=People,ou=Users,dc=differentialdesign,dc=org?one
 nss_base_passwd ou=Computers,ou=Users,dc=differentialdesign,dc=org?one
 nss_base_shadow ou=Computers,ou=Users,dc=differentialdesign,dc=org?one
 nss_base_group  ou=Groups,dc=differentialdesign,dc=org?one
 ssl     no

`2.P./etc/nsswitch.conf` .E
------------------------

You can see how Linux will resolve the users and groups; it will first check system files, if user or group is not found it will then use ldap.

You can see the hosts options uses DNS and WINS; the same also applies.

On both nodes edit your nsswitch. as follows; leave all other settings as defaults..

 #/etc/nsswitch.

 passwd:         files ldap
 shadow:         files ldap
 group:          files ldap

 hosts:          files dns wins
 networks:       files dns

`2.P.Berkeley DB` 
------------------------

We have chosen to use Berkeley DB as our database backend for Openldap. It is the recommended backend and actively maintained..

In order to properly maintain our berkeley database we need to install the tools provided with this package.

**Step1.

Download the source file in an appropriate directory.

 [root@node1 bdb]# wget http://download.e.com/.ey-db/db-4.5.20.tar.....

**Step2.

Uncompress the tarball.

 [root@node1 bdb]# tar zxvf db-4.t...

**Step3.

Change directory into the uncompressed folder.

 [root@node1 bdb]# cd db-4..
 [root@node1 db-4.#.

We are now in the source folder, however because there are many different build enviroments available, we must specify we are using some flavour of *Nix.

 [root@node1 db-4.#.CCEEcd build_unix/
 [root@node1 build_unix]#

**Step4.

From here we beed to run ..igure so we can build the correct make files. . the prefix to match our Openldap prefix..

 [root@node1 build_unix]# ..igure --prefix=/usr/local

If you get this error it means you are not in the correct build directory.

 [root@node1 dist]# .igure --prefix=/usr/local
 checking build system type...pc-linux-gnu
 checking host system type...pc-linux-gnu
 checking if building in the top-level or dist directories...
 configure: error: : DB should not be built in the top-level or "dist" directories.
 Change directory to the build_unix directory and run ..igure from there..
 [root@node1 dist]#

**Step5.

 [root@node1 build_unix]# make

**Step6.

The following requires root privileges and will install Berkeley DB onto our system.

 [root@node1 build_unix]# make install

**Step7.

Now we need to check that our database tools have been installed correctly.

 [root@node1 build_unix]# cd /usr/local/bin/
 [root@node1 bin]# ls
 db_archive     db_hotbackup  db_stat     ldapcompare  ldappasswd
 db_checkpoint  db_load       db_upgrade  ldapdelete   ldapsearch
 db_deadlock    db_printlog   db_verify   ldapmodify   ldapwhoami
 db_dump        db_recover    ldapadd     ldapmodrdn