3.2 Preload LDIF
    <namespace>0</namespace>
<last_edited>2007-01-25T13:42:44Z</last_edited>
<last_editor>Asender</last_editor>

`1.0:ing Samba` 

`2.0:ing LDAP` 

`3.0:zation LDAP Database` 

`4.0:agement` 

`5.0:t HA Configuration` 

`6.0:

`7.0:`

----

<u>**Table of Contents**</u>

`3.1 Provisioning Database` 

`3.2 Preload LDIF` 

`3.3 LDAP Population` 

`3.4 Database Replication`

**Step1**

Create a .txt file containing the following contents.

 [root@node1]#vi preloaddifferentialdesign.ldif                                                  

Subsitute SID  S-1-5-21-3809161173-2687474671-1432921517 with your domain SID, be sure to leave the SID group mapping.

Subsitute dc=differentialdesign,dc=org with your fully qualified domain name.
Subsitute sambaDomainName:with your Samba Domain Name

 #SAMBA LDAP PRELOAD
 # Subsitute SID  S-1-5-21-3809161173-2687474671-1432921517 with your domain SID, be sure 
 # to leave the SID group mapping.
 # Subsitute dc=differentialdesign,dc=org with your fully qualified domain name.
 # Subsitute sambaDomainName:with your Samba Domain Name

 ##The user to bind Samba to LDAP is defined in our smb.conf; 
 ##[root@node1]#  smbpasswd –w SambaAdmin)
 ##[root@node2]#  smbpasswd –w SambaAdmin)

 #SID S-1-5-21-3809161173-2687474671-1432921517

 dn:rentialdesign,dc=org
 objectClass:
 objectClass:tion
 dc:tialdesign
 o:
 description:d Samba LDAP Identity Database

 dn: r,dc=differentialdesign,dc=org
 objectClass:tionalRole
 cn:
 description:y Manager

 dn:ser,dc=differentialdesign,dc=org
 objectClass:
 cn:
 sn:
 userPassword:

 dn:admin,dc=differentialdesign,dc=org
 objectClass:
 cn:in
 sn:in
 userPassword:in

 dn:dmin,dc=differentialdesign,dc=org
 objectClass:
 cn:n
 sn:n
 userPassword:n
 dn:,dc=differentialdesign,dc=org
 objectClass:
 objectClass:tionalUnit
 ou:

 dn: ,ou=Users,dc=differentialdesign,dc=org
 objectClass:
 objectClass:tionalUnit
 ou:
 dn:ters,ou=Users,dc=differentialdesign,dc=org
 objectClass:
 objectClass:tionalUnit
 ou:s

 dn:s,dc=differentialdesign,dc=org
 objectClass:
 objectClass:tionalUnit
 ou:

 dn:ns,dc=differentialdesign,dc=org
 objectClass:
 objectClass:tionalUnit
 ou:

 dn:ainName=DDESIGN,ou=Domains,dc=differentialdesign,dc=org
 objectClass:ain
 objectClass:xIdPool
 uidNumber:
 gidNumber:
 sambaDomainName:
 sambaSID:H1-5-21-3809161173-2687474671-1432921517
 sambaAlgorithmicRidBase:
 structuralObjectClass:ain

 dn:n Admins,ou=Groups,dc=differentialdesign,dc=org
 objectClass:up
 objectClass:upMapping
 gidNumber:
 cn:dmins
 sambaSID:H1-5-21-3809161173-2687474671-1432921517-512
 sambaGroupType:
 displayName:dmins
 description:dministrators

 dn:n Users,ou=Groups,dc=differentialdesign,dc=org
 objectClass:up
 objectClass:upMapping
 gidNumber:
 cn:sers
 sambaSID:H1-5-21-3809161173-2687474671-1432921517-513
 sambaGroupType:
 displayName:sers
 description:sers 

 dn:n Guests,ou=Groups,dc=differentialdesign,dc=org
 objectClass:up
 objectClass:upMapping
 gidNumber:
 cn:uests
 sambaSID:H1-5-21-3809161173-2687474671-1432921517-514
 sambaGroupType:
 displayName:uests
 description:uests

 dn:n Computers,ou=Groups,dc=differentialdesign,dc=org
 objectClass:up
 objectClass:upMapping
 gidNumber:
 cn:omputers
 sambaSID:H1-5-21-3809161173-2687474671-1432921517-515
 sambaGroupType:
 displayName:omputers
 description:omputers

 dn:istrators,ou=Groups,dc=differentialdesign,dc=org
 objectClass:up
 objectClass:upMapping
 gidNumber:
 cn:rators
 sambaSID:H1-5-21-3809161173-2687474671-1432921517-544
 sambaGroupType:
 displayName:rators
 description:rators

 dn:nt Operators,ou=Groups,dc=differentialdesign,dc=org
 objectClass:up
 objectClass:upMapping
 gidNumber:
 cn:Operators
 sambaSID:H1-5-21-3809161173-2687474671-1432921517-548
 sambaGroupType:
 displayName:Operators
 description:Operators

 dn: Operators,ou=Groups,dc=differentialdesign,dc=org
 objectClass:up
 objectClass:upMapping
 gidNumber:
 cn: rators
 sambaSID:H1-5-21-3809161173-2687474671-1432921517-550
 sambaGroupType:
 displayName: rators
 description: rators

 dn:p Operators,ou=Groups,dc=differentialdesign,dc=org
 objectClass:up
 objectClass:upMapping
 gidNumber:
 cn:perators
 sambaSID:H1-5-21-3809161173-2687474671-1432921517-551
 sambaGroupType:
 displayName:perators
 description:perators

 dn:cators,ou=Groups,dc=differentialdesign,dc=org
 objectClass:up
 objectClass:upMapping
 gidNumber:
 cn:ors
 sambaSID:H1-5-21-3809161173-2687474671-1432921517-552
 sambaGroupType:
 displayName:ors
 description:ors