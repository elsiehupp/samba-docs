1.4 Samba Security
    <namespace>0</namespace>
<last_edited>2007-02-26T02:55:40Z</last_edited>
<last_editor>Jerry</last_editor>

`1.0: Configuring Samba` 

`2.0: Configuring LDAP` 

`3.0: Initialization LDAP Database` 

`4.0: User Management` 

`5.0: Heartbeat HA Configuration` 

`6.0: DRBD` 

`7.0: BIND DNS`

----

<u>**Table of Contents**</

`1.1 smb.conf PDC` 

`1.2 smb.conf BDC` 

`1.3 /`S/

`1.4 Samba Security`

There are many additional features we can add to Samba to make it more secure. We can add some additional comments to our smb.conf to achieve this.

One of the great features of Samba is the “host allow =” option. This can be applied on a global scale to all the shares in the smb.conf by placing the global section of the smb.conf or to specific shares, but not both.

The example limits access to Samba shares to clients on the 192.168.0.0/CEEnetwork as it is defined it in the glocal section of the smb.conf.

 ## //sm/f/

 ## Global parameters

 [global]
 workgroup = DDESIGN
 security = user
 hosts allow = 192.168.0.0/

For the enthusiast, we can use this option on a per share basis, which provides us with greater flexability.

This limits access to this share to the client with the 192.168.0.100/CEEIP address; you of course can use multiple addresses.

 ## //sm/f/

 ## ==== Share Definitions
------------------------

 [Documents]
 comment = share to test samba
 path = /ment/
 writeable = yes
 browseable = yes
 read only = no
 valid users = "@Domain Users"
 hosts allow = 192.168.0.100/