SambaGtk/SoC2010
    <namespace>0</namespace>
<last_edited>2010-05-21T19:44:00Z</last_edited>
<last_editor>Sergio97</last_editor>

Sergio Martins' proposal is to create/improve samba-gtk utilities. Below is a copy of his proposal. Some parts of this proposal have been edited/removed in order to remove sensitive information.

Introduction 
------------------------

Hello Jelmer, mentor of the “Extension of the GTK+ frontends” project. I would like to congratulate and applaud your involvement in Google’s Summer of Code program. Here is my formal application to be a part of this exciting project. The goal of my application is to show that I am the best candidate for this project because I am extremely motivated and willing to commit completely to this project!

My name is Sergio and I am enrolled in a four year Bachelor of Information Technology program at the University of Ontario Institute of Technology (UOIT). I’m currently completing my second year of study. I chose to come to UOIT because the university is new and very innovative. My experiences at UOIT have inspired me to think big and to work as hard as I can on projects that I am passionate about!

Background 
------------------------

I am about to complete my second year of university and in that time I have learned a great deal of programming directly through school assignments and indirectly through my own research and personal projects. I also have a solid understanding of how networks work and common protocols used on them, as well as experience with many different tools to troubleshoot network problems.

Being a quick learner has helped me grasp new topics quickly and without wasting a lot of time. I had many successful jobs throughout high school, many of which were typical of students at that age. All of my previous employers would vouch for the fact that I am hard working and dedicated. I work effectively as part of a team but I am also motivated to work on my own.

I have learned a lot from the numerous programming projects that I’ve completed in the past few years. One of my first personal projects was the creation of a multiplayer game that worked over a TCP connection which taught me a lot about the frustrations of programming network applications. A more recent example of my programming experience is <information withheld>. I work with a colleague on coding this project and use subversion for source control. All of the GUI elements were designed and created by me. I am very interested in building a great GUI for the samba project!

My primary operating system is usually Windows 7 but I have spent a lot of time on Linux distributions such as Fedora, Ubuntu, and Android so I know my way around. For this project I would be using a dedicated Linux distribution (either Fedora or Ubuntu).

I am well accustomed to using C++ and Java through the many applications, both big and small, that I have worked on. I have introductory experience with C and Python but I am absolutely willing to put in the extra effort to completely learn the language for this project. I find the best way to learn a new programming language is when there is a task to be completed. I have exposure to many other programming and scripting languages such as actionscript (used in flash), JASS, Visual Basic, and various web languages. Having exposure to so many languages makes it easier for me to learn new ones. My programming background makes me a great candidate for working on this Goggle Summer of Code project!

Project 
------------------------

I am very interested in your project to develop a GTK+ frontend for Samba4. Being a Linux user who still isn’t completely accustomed to all the different commands used in Linux, I know the importance of creating a great user interface. Many users who are new to Linux may have a network that contains both Windows and Linux machines so software such as this could be very helpful for them.
I am enthusiastic and determined to devote the time and effort required to complete this project to the best of my abilities. I am very resourceful when I run into problems and I don’t hesitate to ask questions when I need help. I hope to keep in close contact with my mentor as part of the Google Summer of Code experience is learning and gaining wisdom from my mentor. I won’t hesitate to ask Jelmer questions about implementation or just say hi and update him on my progress. The Samba community seems like a great group of people and I can’t wait to be part of it and start making a real contribution!

I have familiarized myself with the work that Calin (the previous GSoC student to work on this) has done and I have a clear vision of what needs to be done. I plan to continue his work by fixing any bugs present in the current code and implementing all the GUI widget actions and callbacks. Additionally, I want to create a main launcher window that would present all 5 utilities windows as tabs or some other form. This way the user would access all the utilities from one location, which is simpler for the user. The user would enter the server’s information once and have access to all these tools without entering any additional information. This is also ideal for adding an icon for samba into the applications menu. Any new dialogs and windows created would be done using Glade and if there is time left over towards the end of the project I intend to convert current windows to Glade XML as well. Below is a [very] rough layout of what I envision.

`Image:gsoc_figure1.png`

Also I want to create a GUI for gregedit so that it will look like regedit from windows. Regedit has remained almost exactly the same throughout the history of Microsoft Windows so Windows users are very accustomed to it by now. Creating a clone of this GUI would make remote editing Windows’ registry very intuitive for the user. Additionally I would like to add some basic regedit-type functionality; namely, the ability to export and import specific registry keys. I realize that this feature isn’t GUI related, but I believe I could accomplish it within the allotted time. I didn’t include a sample picture of this because I assume most people have seen the regedit utility at some point.

Between now and the official date for student to start coding I plan to further familiarize myself with Glade, Python, and the work already done by Calin. I plan to keep in touch with Jelmer and others in the samba community to get an even better idea of what the best user interface would look like.

Proposed Timeline 
------------------------

{| class="wikitable" border="1" style="text-align: left; height: 700px;"
|+ draft timeline
|- style="background: lightgrey"
! **Week**
! **Task(s)**
|- 
| Week 1 (May 24th)
| Determine which existing windows would be changed and how. Design main launcher and any other UI elements that don't already exist.
|- style="background: lightgrey"
| Week 2
| Finalize design and consult with Jelmer. Begin building interface(s) using Glade.
|-
| Week 3
| Finish main interface elements and implement callbacks.
|- style="background: lightgrey"
| Week 4
| Finish main interface elements and implement callbacks.
|-
| Week 5
| Test Task Scheduler (gwcrontab) and User Manager (gwsam) to ensure they work correctly. Make adjustments as needed.
|- style="background: lightgrey"
| Week 6
| Test Registry Editor (gregedit) and Endpoint Dumper (gepdump) to ensure they work correctly. Make adjustments as needed.
|-
| Week 7
| Test Service Manager (gwsvcctl) to ensure it is working correctly. Make adjustments as needed. Create entry in applications menu with appropriate icon.
|- style="background: lightgrey"
| Week 8 (Midterm)
| Tie up any lose ends, evaluate progress. (Submit mid-term Evaluation)
|-
| Week 9
| Build GUI for Linux look-alike of regedit.
|- style="background: lightgrey"
| Week 10
| Connect the GUI with the information backend.
|-
| Week 11
| Connect the GUI with the information backend.
|- style="background: lightgrey"
| Week 12
| Implement export and import feature for a single branch and the entire registry.
|-
| Week 13
| Suggested "pencils down". I will use this extra time to finish anything that has taken longer than expected. I will also polish the GUI as needed and clean up the code.
|- style="background: lightgrey"
| End of August
| Submit final evaluation
|-
| September
| Rejoice in my accomplishment! I would also like to continue helping Samba in whatever way I can. I want to stay involved with the open source community!
|}

I want to develop each utility’s GUI and test them extensively to ensure they work completely and correctly. While doing this I would also be learning a lot about Glade, Python, and Linux in general. This would be a great opportunity for me to dive into Linux in a way that I might not otherwise get to do. I approach this project with the point of view of a somewhat novice user of Linux, allowing me to design GUIs in the most intuitive manner rather than the manner which is easier to program or already partially existing. I hope the outcome of my application coincides well with your visions of the GUI for samba.

Commitment 
------------------------

I am fully and completely committed to devoting the effort required to finish this project. I have a very good feeling that I will spend more than 30 hours  a week getting familiar with the ins-and-outs of Linux and specifically samba. Samba is very interesting to me, most people use it without realizing what it is and how it works. I’ve always loved to learn how things work and I’ve always wondered about SMB file and print sharing.
I will be updating my mentor on a weekly basis and keeping in touch with others via the IRC channel. I plan to review my work often and ensure that everything is working correctly or else I will have let down myself and the whole samba team. 
Thank you so much for taking the time to review my application. I hope I have been able to express to you the effort and energy I can bring to this project. I want to think you for being a mentor, regardless of who you will be mentoring, because without you Google Summer of Code wouldn’t be as extraordinary as it is. Thank you!