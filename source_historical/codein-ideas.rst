CodeIn/Ideas
    <namespace>0</namespace>
<last_edited>2010-11-04T10:50:08Z</last_edited>
<last_editor>JelmerVernooij</last_editor>

This page contains ideas for the Google Code In. If you're interested in contributing to Samba (but not participating in the Google Code In) this page might also be useful to you, but please edit the wiki to indicate you are working on a particular idea .

Switch standalone TDB to use waf-based build.<br/>
Category: Code<br/>
Level: easy<br/>
Possible mentors: Jelmer<br/>

Switch standalone talloc to use waf-based build.<br/>
Level: easy<br/>
Possible mentors: Jelmer<br/>

Write a manual page for samba-tool<br/>
Level: easy<br/>
Possible mentors: Jelmer, Kai(?)<br/>

write a manual page for tevent<br/>
Level: medium<br/>
Possible mentors: Jelmer<br/>

Convert py* utilities in samba-gtk to use optparse<br/>
Level: medium<br/>
Possible mentors: Jelmer<br/>

Design an icon for one of the samba-gtk tools<br/>
Level: medium<br/>
Possible mentors: Jelmer<br/>

Support macro to allow subsystems to specify what system functionality they need in libreplace<br/>
Level: hard<br/>
Possible mentors: Jelmer<br/>

Automatically create /var/run/samba directory if it doesn't exist (bug 7261)<br/>
Level: medium<br/>
Possible mentors: Jelmer<br/>

Extend libnet to support managing services<br/>
Level: hard<br/>
Possible mentors: Kai (?)<br/>

Integrate Luk's myldap-pub.py script into upgrade_from_samba3<br/>
Level: hard<br/>
Possible mentors: Matthieu (?), Jelmer<br/>

Document upgrade process between Samba 4 releases
Level: hard<br/>
Possible mentors: Matthieu (?)<br/>