Samba4/ActiveDirectory
    <namespace>0</namespace>
<last_edited>2008-12-11T21:18:55Z</last_edited>
<last_editor>Abadent</last_editor>

This is the Samba 4 Active Directory TODO list as assembled by metze.

(taken from http://ftp.sernet.de/pub/samba4AD/samba4ad-TODO-20061002.txt)

(see also: http:SSLLAASS:HHftp.sernet.de/pub/samba4ad/)

(additional links http://ftp.sernet.de/pub/samba4ad/samba4ad-LINKS-20061002.txt)

Status: 02.10.2006

Schema 
------------------------

Task Schema#01
------------------------

For making samba4 compatible with the Active Directory Schema
tests need to be written, so that all attributes of the "objectSchema"
and "attributeSchema" objectclasses are known in their meaning.
This is important because samba4 should be able to work as Domain Controller
in an existing windows domain together with Windows Domain Controllers.
Therefore it is needed that samba4 work with an unmodified schema
from a windows server.

NOTE: MS extended schema for unix ?

As a first goal the dependencies of the attribute description in the schema
(descriped via an attributeSchema object) and the access properties of the attribute at
the API level need to be compared. E.g. we need to find out which settings cause
an attribute to be changeable, we need to find out which settings make an attribute
constructed at write time or at search time.

Task Schema#02
------------------------

It is already known that objectclasses and attributes have 4 identifiers
The first one is the name ("lDAPDisplayName"), the second is the OID
("governsID" resp. "attributeID"), a GUID ("schemaIDGUID") and at the DRSUAPI
they are identified by 32bit number which is constructed out of the
OID (for details see drsuapi.idl).

For better handling in the schema checking code the schema should be loaded
into memory at startup as a 'struct schema_context *'. 'schema_*' functions
should provide a useful interface for accessing the schema elements by the different
identifiers and in form of structs with useful C datatypes. This schema_context
can then passed to all the places in the code which need to access the
schema (via a ldb handle and a ldb_opaque ptr)

Task Schema#03
------------------------

Informations about the schema are provided also via the "subSchema" object.
This object has some attributes which are constructed at search time and
only attached to the search response, when the requests has explicit asked for
them. Windows only provides attributeTypes, objectClasses and dITContentRules.
The content of this attributes should be a dump of the information
that's in the schema_context structure. For details see RFC 4512 section 4.2.

Task Schema#04
------------------------

All origin changes to the directory database need to be validated by the schema.
Basically the objectClass atttribute values need to be completed and sorted
and the objectCategory needs to be found.

It is assumed that windows uses default value completion based on the objectCategory,
but maybe they're per objectClass. Tests need to be written to find out more details.
Maybe only some object categories have this completion. What is with user defined
classes?

Also it needs to be checked if all needed attributes are present and that all
attributes have the correct value syntax and valid values.

Task Schema#05
------------------------

Maybe some msDS-* attributes can be written into the directory without
being defined in the schema when the SYSTEM security token is used.
Further research is needed.

Task Schema#06
------------------------

Windows Servers provide some schema specific attributes (dsSchemaAttrCount,
dsSchemaClassCount and  dsSchemaPrefixCount) on the rootDSE.
This attributes are constructed at search time and only attached to the
search response, when the requests has explicit asked for them.
The meaning of the dsSchemaPrefixCount is currently unknown
and further research is needed. The returned information
should be taken from the schema_context structure.

There're also a lot of other informational attributes on the rootDSE.
See http://technet2.microsoft.com/WindowsServer/en/Library/7cfc8997-bab2-4770-aff2-be424fd03cda1033.mspx

But only dsSchema*Count and validFSMOs seem to be implemented by
windows 2003.

LDAP 
------------------------

Task LDAP#01
------------------------

The behavior of LDAP searches are different on the LDAP port 
and on the Global Catalog Port. On the GC Port the non-existance
of the basedn object doesn't cause the NoSuchObject error.

Tests should be written which demonstrate and test this difference.

And make the server passing this tests.

This is also related to the LDAP_SERVER_SEARCH_OPTIONS Control with
supports the following options: 

* SERVER_SEARCH_FLAG_DOMAIN_SCOPE limits a search to a specific domain and is identical to the LDAP_SERVER_DOMAIN_SCOPE Control.
* SERVER_SEARCH_FLAG_PHANTOM_ROOT which uses a virtual base partition and allows searches with non existing basedns and returns results from all existing partitions of used domain controller

See http://msdn.microsoft.com/library/default.asp?url=/library/en-us/ldap/ldap/ldap_server_sort_oid.asp

Task LDAP#02
------------------------

Tests should be written to demonstrate and test the object creation
depending on the directory partition and the parent container.

Because the "rDNAttId" attriute of the "classSchema" objects in the schema,
should deny some parent child combinations. This needs testing
in terms of inheriting object classes where the "rDNAttID" value differs
between the object classes.

And make the server passing this tests.

Task LDAP#03
------------------------

A general test should be written which loops over all objectClasses
found in the schema and tries to objects with only one object class
given and the "name" attribute. It is known that Windows allows this
for some objectClasses and fills in the required "must" attributes
with autogenerated or default values.

The goal of this test is to find out if this is the behavior of all
object classes or only of a few ones. This needs testing with administator
defined object classes.

And make the server passing this tests.

Task LDAP#04
------------------------

Some attributes are autocreated by the system at the "add" operation
but readonly for modify requests. This maybe also apply to some attributes
the user can specify at the "add" operation. This should be tested
generically with the attribute list from the schema. And the expected result
should also be calculated using the information from the schema.

eg: ObjectCategory

And make the server passing this tests.

Task LDAP#05 === 

The behavior of Paged-Results searches needs more detailed testing
in terms of adding, modifying and deleting objects between the
search requests.

And make the server passing this tests.

Task LDAP#06
------------------------

Add the infrastructure for extended operations
to LDB and pass them from the LDAP server.

Task LDAP#07: (done!)
------------------------

The LDAP server needs to support the StartTLS extended operation.
This is already implemented.

NOTE: needs to be tested in 'make test'

Task LDAP#08
------------------------

The LDB or LDAP server layer needs to enforce the timelimit and 
sizelimit of search requests and reply with an error if one
of them exceed.

Task LDAP#09: (done!)
------------------------

The LDAP server needs to support a SASL wrapping layer
to support sign and/or seal.
This is already implemented.

NOTE: tested in 'make test' ?

Task LDAP#10
------------------------

The LDAP server should correctly handle async requests and needs to implement
the AbandonRequest to cancel pending requests. This is needed to correctly
support the NOTIFICATION Control.

See http://msdn.microsoft.com/library/default.asp?url=/library/en-us/ldap/ldap/ldap_server_notification_oid.asp

Also the sequence of StartTLS, LDAP Bind (simple and SASL), LDAP Unbind
and SASL sign/seal operations needs to tested more detailed.
Then the LDAP server should handle all the found cases correct.

ACL 
------------------------

Task ACL#01
------------------------

We need to work out which strategy we will use to be compatible with
ACLs in Active Directory, while avoiding relevant Microsoft
patents. Legal advice from the Software Freedom Law Center will be
sought to ensure we do not infringe on relevant patents.

Task ACL#02
------------------------

Tests should be written to verify the meaning of each flag of
ACE's access mask. This tests should cover read and write operations.

And later the LDAP server should pass this tests.

similar to RAW-ACLS code.

Task ACL#03
------------------------

Tests need to be written to demonstrate and test the ACL inheriting
behavior. Also with per attribute or extended-rights for different
child objectclasses.

And later the LDAP server should pass this tests.

Task ACL#04
------------------------

Tests need to be written to demonstrate the behavior of
per property (attribute) and propertySet.

And later the LDAP server should pass this tests.

Task ACL#05
------------------------

Research needs to be done to find out the relationship between
"schemaIDGUID", "attributeSecurityGUID", "rightsGUID", "appliesTo" and the GUID
used in the ACE's. Tests are needed to demonstrate and test the behavior
of the different forms of granting or denying access to properties
and propertySets.

It seems like access can be granted to specific attribute by
adding an ACE with the "schemaIDGUID" of the attribute.

It seems like access can be granted to multiple attributes by
adding an ACE with the "attributeSchemaGUID" of the attributes.
(But this needs verification!)

It seems like access can be granted to multiple attributes by
adding an ACE with the binary form of the "rightsGUID" of a
"controlAccessRight" object (Extended-Right). Where the "appliesTo" values
list to the attributes by their "schemaIDGUID" values in text form.
It needs to be tests if the "appliesTo" value can hold an
"attributeSchemaGUID" value.
(But this needs verification!)

See http://msdn.microsoft.com/library/default.asp?url=/library/en-us/ad/ad/setting_permissions_to_a_specific_property.asp
and http://msdn.microsoft.com/library/default.asp?url=/library/en-us/ad/ad/setting_permissions_on_a_group_of_properties.asp
and http://msdn.microsoft.com/library/default.asp?url=/library/en-us/adschema/adschema/control_access_rights.asp.

Task ACL#06
------------------------

Active Directory uses extended-rights to control access to operations on directory objects.
Tests need to be written to demonstrate the behavior of them.

See http://msdn.microsoft.com/library/default.asp?url=/library/en-us/ad/ad/control_access_rights.asp.

Task ACL#07
------------------------

Maybe the access control should be implemented via callback in the main
ldb backend (ldb_tdb) instead of an ldb module?

Task ACL#08
------------------------

With the LDAP_SERVER_SD_FLAGS Control the content of the returned ntSecurityDescriptor
attribute can be controlled on searches (and maybe also on write operations).
The tests for this control should similar to the same tests on files and directories.

See http://msdn.microsoft.com/library/default.asp?url=/library/en-us/ldap/ldap/ldap_server_sd_flags_oid.asp

Task ACL#09
------------------------

According to "How the Data Store Works", security descriptors are 
just referenced at the storage layer by objects with the same
security descriptors. This speeds up operations which modify the
security descriptor of a top level container from which the
security descriptor is inherited by most of the sublevel objects.

See http://technet2.microsoft.com/WindowsServer/en/Library/54094485-71f6-4be8-8ebf-faa45bc5db4c1033.mspx?mfr=true
(How the Data Store Works).

DSDB (Directory Service DB backend) 
------------------------

Task DSDB#01
------------------------

The directory database needs to autocreate the objectGUID
on creation and disallow values from a caller. There's
an extended-right called 'Add-GUID', some research is needed
on this.

Note that the uSNCreated and uSNChanged are just overwritten
and don't cause an error when they're specified on an add operation
and maybe also on an modify operation. Tests are needed to verify
the behavior.

Task DSDB#02
------------------------

The directory database needs to hold multiple directory partitions.
Directory partitions are also known as naming contexts.
Typically a domain controller hold a domain-, a configuration- and schema-partition.

Global catalog servers also hold read only copies of all other partitions
in the whole forest.

Partition heads are identified by the INSTANCE_TYPE_IS_NC_HEAD in the "instanceType"
attribute of the base object. There are also some other related flags in the
"systemFlags" attribute.

Windows also supports Application Data Partitions. They allow
applications to use the active directory database to store their data
and take advantage of the replication to distribute the information to multiple
servers.

The application partitions can be created via the LDAP api.
Research needs to be done in this area.

Maybe the creating of an application partition causes the "invocationID" of
the local domain controllers NTDS Settings object to change.
This has effects on replication metadata which uses this "invocationID" to identify
the originating domain controller.

Maybe the creation is only possible on the server with is the Infrastructure Master
(the FSMO role owner of cn=Partitions,cn=Configuration,... a the crossRefContainer object).

Application partitions have the msDS-SDReferenceDomain set in the crossRef object.
It is assumed that this is needed to specify the default domain sid when
real security descriptors are created from the "defaultSecurityDescriptor" attribute
which is specified in the schema.

Operations typically use one single partition, therefore all
partitions are independent in terms some special containers
(LostAndFoundConfig, NTDS Quotas and DeletedObjects) and in terms of
replication.

For searches which reach a partition head as subobject in a search a
LDAP referral is returned, but it seems to differ with different
search scopes (base, onelevel or subtree).  This can also be
controlled with the LDAP_SERVER_SEARCH_OPTIONS control.  This behavior
also differs between normal LDAP and the Global Catalog.  The Global
Catalog uses the SEARCH_OPTIONS_PHANTOM_ROOT always and only grants
read access.

Task DSDB#03
------------------------

The directory database needs to store replication metadata for all
(maybe only all replicated) attributes of all objects.
They need to be updated by each write operation.

Task DSDB#04
------------------------

The directory database needs to support linked attributes
This means that attribute which hold a DN-Reference to another
directory object, a back link is created on this object holding
a DN-Reference to the original object. This links are create, modified
and deleted automatically by the directory database.

In Windows 2000 forest linked attributes have the same replication metadata
as normal attributes, which results in a 5000 value limit for e.g. the
"member/memberOf" attributes.

In Windows 2003 forest linked attributes are handled separate from the objects
they belong to in terms of their replication metadata. Each value of a linked attribute
is replicated individual and the 5000 value limit is gone.

On an upgrade from Windows 2000 to 2003 existing attributes are not converted,
but new values will use the per value metadata.

See http://technet2.microsoft.com/WindowsServer/en/Library/e3525d00-a746-4466-bb87-140acb44a6031033.mspx?mfr=true
(How the Active Directory Schema Works).

Maybe the LDAP_SERVER_VERIFY_NAMES control is also related to linked attributes
and needed when an object is referenced that is not yet replicated to the current
domain controller and the existance of the object needs to be verified by another
server.

See http://msdn.microsoft.com/library/default.asp?url=/library/en-us/ldap/ldap/ldap_server_sd_flags_oid.asp.

Task DSDB#05
------------------------

The directory database needs to keep deleted objects for a while to replicate
the deletion to all domain controllers. Normally objects are normally moved
into the "Deleted Objects" container of the partition with an RDN containing
the old RDN + the GUID of the object. Deleted objects are identified by the
"isDeleted" attribute with "TRUE" as value.

The "dSCorePropagationData" attribute appears also on deleted objects
and has maybe something to do with the tombstone lifetime.

Some objects will not be moved when they have isCriticalSystemObject=TRUE.
Tests need to be written to verify this behavior.

Deleted Objects are returned in LDAP search requests when the LDAP_SERVER_SHOW_DELETED
control is used.

See http://msdn.microsoft.com/library/default.asp?url=/library/en-us/ldap/ldap/ldap_server_show_deleted_oid.asp.

Task DSDB#06
------------------------

As domains can run in mixed mode with support NT4 BDC's
the directory database maybe need to keep track of metadata for
the replication with NT4. Under NT4 privileges are also replicated to the BDC's
and they're not stored in the directory database as far as known currently.

There's some more research needed in this area and maybe the
DsGetNT4ChangeLog() function gives more details.

Task DSDB#07
------------------------

An extended operation should be used to pass replication data from
other domain controllers to the directory database.

Also an extended operation should be used to get the replication data
for other domain controllers out of the directory database.

Task DSDB#08
------------------------

Most operations on the directory database are possible on all
domain controllers with a writable copy of a partition. However
there are some operations which can only be done on one server called
Flexible Single Master Operations Role Owner.

The directory database layer should only allow this operations on the server
which holds the FSMO role ownership.

The PDC-Emulator, Infrastructure-Master and Rid-Master roles are per domain,
where the Domain-Master and Schema-Master roles are per forest.

Task DSDB#09
------------------------

The directory database layer should provide a way to get event triggered
notifications about changes in the directory database. This can be implemented
in a similar way as the NTVFS notify support of samba4.

With the LDAP_SERVER_NOTIFICATION control ldap clients can ask for such notifications.

See http://msdn.microsoft.com/library/default.asp?url=/library/en-us/ldap/ldap/ldap_server_notification_oid.asp.

KCC (Knowledge Consistency Checker) 
------------------------

Task KCC#01
------------------------

Some research is needed to find out what objects and attributes
the Knowledge Consistency Checker autocreates.

Task KCC#02
------------------------

The KCC notices changes in the configuration partition,
e.g. new partitions are added or removed, new sites are created
or servers are moved between sites and then reacts on them
with verifying the configuration for the local server.

Task KCC#03
------------------------

The KCC creates the "repsFrom" attributes for every directory partition
head which holds the configuration of the outgoing pull replication.
One attribute value for each source domain controller.

The KCC maybe also creates the "replUpToDateVector" attribute for every directory
partition.

See drsblobs.idl in samba4.

Task KCC#04
------------------------

The KCC also generates the intrasite replication topology used by the
local domain controller.

On servers which are the intersite topology generator of the site
(specified by the "interSiteTopologyGenerator" attribute of the "nTDSSiteSettings" object)
the KCC also generates the intersite replication topology for the site.

See http://technet2.microsoft.com/WindowsServer/en/Library/c238f32b-4400-4a0c-b4fb-7b0febecfc731033.mspx?mfr=true
(How the Active Directory Topologie Works).

Task KCC#05: 
===============================

------------------------

The KCC needs to take care of removing expired tombstone records.

index on tombstone attribute, so we can find them fast in a regular
sweep

Task KCC#06
------------------------

The KCC should notify replication partners (found in the "repsTo" attribute of the partition head)
about updates to the directory database. The notifications are delayed a bit
because typically multple directory database updates happen within a few seconds.
The delays are different between for normal and urgent replication.

See http://technet2.microsoft.com/WindowsServer/en/Library/1465d773-b763-45ec-b971-c23cdc27400e1033.mspx?mfr=true
(How the Active Directory Replication Model Works).

Task KCC#07
------------------------

The KCC also handles the pull replication calls depending on the content of the "repsFrom"
attributes. Only one partition and source domain controller at a time.

Task KCC#08
------------------------

While generating the intersite replication topology the KCC cares
about the costs of site links.

DRSUAPI 
------------------------

Task DRSUAPI#01 (done!)
------------------------

A very important task for this project is finding out
the encryption algorithm which is used for the "dBCSPwd",
"unicodePwd", "ntPwdHistory", "lmPwdHistory" and 
"supplementalCredentials" attributes durring the DeGetNCChanges() call.
Also the unencrypted format of this attributes needs work.

Without this knowledge the whole project will fail as no 
password replication with windows can happen!

Task DRSUAPI#01
------------------------

The DeReplicaSync() call needs to be implemented in the drsuapi
rpc server to receive incoming change notifications. This
information should passed to the KCC task via IRPC.

Task DRSUAPI#02
------------------------

The DsGetNCChanges() call needs to be implemented in the drsuapi
rpc server to return the directory database changes to a
destination domain controller.

There's also a bit of research needed to find out the meaning of some
unknown data in this call.

Task DRSUAPI#03
------------------------

A lot of research is needed to find how the DsGetNT4ChangeLog()
function works and what information it provides. Maybe it's
used by some diagnostic tools for active directory.

The implementation of this call in the rpc server is maybe not needed.

Task DRSUAPI#04
------------------------

A lot more research is needed to find out how the DsAddEntry() works.
Maybe there are levels to modify or delete entries.

Task DRSUAPI#05
------------------------

The DsAddEntry() call needs to be implemented in the rpc server, because
it is needed in the join process as a domain controller. It's basicly
the same as the LDAP add operation.

Task DRSUAPI#06
------------------------

Windows 2000 uses MSZIP compresession for replicating
directory database updates. Currently samba4 only supports
MSZIP decompression. Maybe it's needed to implement the compression
too.

NOTE: for an initial implementation we should be able to avoid
negotiating compression

Task DRSUAPI#07
------------------------

Windows 2003 uses a new compression algorithm which is also used by an
Exchange-Server. The flag which indicates support for this algorithm
is called DRSUAPI_SUPPORTED_EXTENSION_XPRESS_COMPRESS and is present
within the DsBind() call. The algorithm looks like a sort of Lempel-Ziv
compression maybe a variant of LZX which is also used in Microsoft
CAB files.

But as it is possible to use MSZIP on Windows 2003 with a registry key
it is hopefully not needed to find the algorithm out.

Task DRSUAPI#08
------------------------

As add-on it would be nice to implement the XPRESS (de)compression
once it is found out how it works.

Task DRSUAPI#09
------------------------

The DeReplicaUpdateRefs() call needs to be implemented in the drsuapi
rpc server. There's some research needed to find out what this function
really do. It is used while joining a domain controller to an existing domain.

See http://msdn.microsoft.com/library/default.asp?url=/library/en-us/ad/ad/dsreplicaupdaterefs.asp.

Task DRSUAPI#10
------------------------

The DeWriteAccountSPN() call needs to be implemented in the drsuapi
rpc server. It is used while joining a domain controller to an existing domain.

See http://msdn.microsoft.com/library/default.asp?url=/library/en-us/ad/ad/dswriteaccountspn.asp.

Task DRSUAPI#11
------------------------

Maybe other calls from the DRSUAPI are needed to to be implemented.
More research is needed for this.

See http://msdn.microsoft.com/library/default.asp?url=/library/en-us/ad/ad/dc_and_replication_management_functions.asp.

SYSVOL 
------------------------

Task SYSVOL#01
------------------------

Each domain controller needs to provide a SYSVOL and a NETLOGON
share which hold the Group Policy files and logon scripts.

This shares need to syncronized between the domain controllers.
(As the NETLOGON directory is a grandchild of the SYSVAL directory, only the SYSVOL directory needs to be syncronized)

In Windows this is done using the File Replication Service.
See http://technet2.microsoft.com/WindowsServer/en/Library/965a9e1a-8223-4d3e-8e5d-39aeb70ec5d91033.mspx.
(How FRS Works)

Because it is completely unknown how the FRS works, a solution
using the SMB protocol with NTTRANS_NOTIFY_CHANGE can be used
as workaround. But more research is needed in this area.

Task SYSVOL#02
------------------------

The SYSVOL and NETLOGON shares are exported as DFS shares.
So DFS support needs to be added to samba4's SMB server,
maybe the DFS managment functions are not needed.

See http://technet2.microsoft.com/WindowsServer/en/Library/20ffb860-f802-455c-9ca2-5194f79a9eb41033.mspx.
(How DFS Works)

Browsing (Domain Master Browser) 
------------------------

Task BROWSE#UR
------------------------

Task BROWSE#UC
------------------------

As browsing through the Network Neighborhood should work,
support for being a Domain Master Browser is needed.

Basically the same functionality that samba3 has needs to be
implemented in samba4.

See http://ubiqx.org/cifs/Browsing.html 
and the samba3 source code.

* JelmerVernooij is now working on this.

DNS 
------------------------

Task DNS#UR
------------------------

Task DNS#UC
------------------------

As Active Directory uses DNS a lot, some more research is needed
in this area.

Task DNS#01
------------------------

The samba4 server needs to register its names using DNS dynamic
(secure) updates, via native C code or by calling a perl script,
because there's a working script for this task available.

Task DNS#02 (done!)
------------------------

As Windows clients want to dynamically register their names a server
with support for that is needed. The BIND DNS Server is
enough for this task, and the right magic for the named.conf is generated by the provision.

NTP 
------------------------

Task NTP#UR
------------------------

Task NTP#UC (done!)
------------------------

Windows clients need to synchronize their clocks with a time server.

The patch for the ntp.org NTP server is pending upstream approval

TrustDom 
------------------------

Task TrustDom#01
------------------------

Informations about trusted domains are stored as "trustedDomain" objects
under the "CN=System" container in of the domain partition.

It is assumed that the trust password is stored in the
"trustAuthOutgoing", "trustAuthIncoming",
"initialAuthOutgoing" and "initialAuthIncoming" attributes.
The encryption and format of them within the DsGetNCChanges() call
needs a lot of work!

Some other secrets are stored as "secret" objects 
under the "CN=System" container in of the domain partition.

It is assumed that the trust password is stored in the
"priorValue" and "currentValue" attributes.
The encryption and format of them within the DsGetNCChanges() call
needs a lot of work!

Task TrustDom#02
------------------------

The samba4 winbind server needs to be completed. It should do nearly
the same as the samba3 version of winbind (but faster, and async)

The IDMAP problem needs to be solved!
Some research should be done with the windows implementations
including the "uidNumber" and "gidNumber" attributes.
Also the "trustPosixOffset" attribute of the "trustedDomain" objects
need some research.

Task TrustDom#03
------------------------

The gensec_update() and auth_check_password() calls need
to get a async version to support passthrough authentification
nicely.

Task TrustDom#04
------------------------

A auth_winbind modules that uses IRPC to the winbind task is already
written, but needs testing.

Task TrustDom#05
------------------------

Operations as lookupnames, lookupsids or ds_cracknames and maybe some othere
should use winbind (via IRPC) as proxy to pass requests to trusted domain controllers
or a Global Catalog server.

Task TrustDom#06
------------------------

As we want to use the rpc client library in an async way, we need
to implement a send queue at the rpc layer, because some authtypes
need a strict request/response order.

Similar code needs to be added to the SMBtrans/SMBtrans2 client code
to send large rpc fraqments over IPC$.

Windows Client Testing 
------------------------

Task WinCli#UR
------------------------

Task WinCli#UC
------------------------

In general there's a lot of testing using windows clients
and servers needed.

Task WinCli#01
------------------------

Specific research needs to be done to find out if the client
decides that passwords are always changed on the PDC-Emulator
with fallback to the nearest DC.

Task WinCli#02
------------------------

Specific research needs to be done to find out if the client
decides after a few unsuccessful logons to try the PDC-Emulator,
which then may lockout the account and cause an urgent replicaton
to propagate this to all other DC's.

Wireshark 
------------------------

Task WIRESHARK#UR
------------------------

Task WIRESHARK#UC
------------------------

It might be needed to add some new dissection code to the wireshark
network analyser in order to make understanding of protocol or protocol
elements easier.