Building a Samba Active Directory Domain Controller on CentOS 6
    <namespace>0</namespace>
<last_edited>2013-05-18T08:34:37Z</last_edited>
<last_editor>Leftcase</last_editor>

Introduction 
------------------------

{|style="background: yellow; padding: 6px; width: 300px; margin: 6px; float:right; border:1px solid #AAA; text-align:center;"
| `File:Warning.svg.png|left|40px` The Samba project moves fast. This article was last checked for version **4.04**. Your mileage may vary!
|}

This tutorial will take you through building a Samba Active Directory Domain Controller on CentOS 6. As CentOS is derived from RHEL, the tutorial should work equally on Red Hat, Scientific Linux and Oracle Linux.

Where possible this tutorial links back to core non-distro specific pages elsewhere on this wiki. This helps ensure that you don't miss out on information other contributors have added or changed since this tutorial was last edited.

Preparing your system 
------------------------

Pre-requisite packages
------------------------

We're going to build Samba and BIND from source. Therefore you'll need to setup your system with the pre-requisite packages for doing so. The required packages are listed [https://wiki.samba.org/index.php/Samba_4/OS_Requirements#Red_Hat_Enterprise_Linux_or_CentOS here] under the **Red Hat Enterprise Linux or CentOS** header. Install these before continuing.

Filesystem changes
------------------------

Samba requires **A**ccess **C**ontrol **L**ist functionality enabling. ACL provides a more flexible way of dealing with file permissions. ACL is already installed in CentOS 6, but you need to enable it.

Edit {{code|code=/etc/fstab}} and add the acl attribute to the partition which will hold your Samba installation. You can do this by adding {{code|code=acl}} after any other options as shown below. 

    ID=xxxxxxxxxxxxxxxxxxxxx /           ext4    defaults,acl        0       2

Grabbing the source
------------------------

Make a directory on your machine to hold the source code. {{code|code=/root/source}} will do. We're going to grab the source code for Samba and BIND and store it locally.