Samba4/ProvisionIssues
    <namespace>0</namespace>
<last_edited>2009-07-19T14:46:17Z</last_edited>
<last_editor>JelmerVernooij</last_editor>

* Needs to make sure that the provided realm name is all-uppercase (otherwise the KDC refuses its own tickets)
* Need to refuse _ in the DNS hostname (since bind doesn't like that)
* Need to check that netbios name in smb.conf matches what is specified to provision