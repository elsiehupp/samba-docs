Feature/OneFS VFS Module
    <namespace>0</namespace>
<last_edited>2009-05-04T04:53:44Z</last_edited>
<last_editor>Sdanneman</last_editor>

==============================

verview
===============================

The OneFS VFS module provides support for the advanced features of the OneFS file system via the Samba 3 VFS interface.

The core features are implemented in the *onefs.so* VFS module which is conditionally compiled if the OneFS operating system is detected.  Additional, modules used on OneFS include *onefs_shadow_copy.so* which provides the VSS shadow copy service to Windows clients, and the *perfcount_onefs.so* module which implements the `Feature/Performance_Counter|Performance Counter` statistics aggregation functions of OneFS.

===============================
Components
===============================

{| class="sortable wikitable" border="1" cellpadding="5" cellspacing="0"
|-style="background-color:#DCDCDC;"
!Component || Description || Developer || Status
|-
|NTFS ACLs || Use OneFS provides NTFS style ACLs || `sdanneman` || <font color="green">Completed</font>
|-
|CreateFile() || Refactor general open() path.  Add SMB_VFS_CREATE_FILE(). || `tprouty` || <font color="green">Completed</font>
|-
|ADS || Route alternate data stream open operations through SMB_VFS_CREATE_FILE() || `tprouty` || <font color="green">Completed</font>
|-
|readdirplus() || Implement bulk directory enumeration under the POSIX style readdir/seekdir VFS API. || `sdanneman` || <font color="green">Completed</font>
|-
|recvfile() || Call OneFS recvfile implementation. || `tprouty` || <font color="green">Completed</font>
|-
|sendfile() || Add support for OneFS atomic sendfile implementation. || `tprouty` || <font color="green">Completed</font>
|-
|Oplocks || Add support for kernel level II oplocks.  Call into OneFS kernel oplock implementation. || `tprouty` || <font color="green">Completed</font>
|-
|BRL || Add VFS interface for BRL calls.  Call into OneFS's Windows semantics byte range locking API. || `zkirsch` || <font color="green">Completed</font>
|-
|Change Notify || Use OneFS kernel file notification events. || `sdanneman` || <font color="green">Completed</font>
|-
|Snapshots || Add module for shadow copy services. || `tprouty` || <font color="green">Completed</font>
|}