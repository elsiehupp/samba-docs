Lessons learned first tutorial
    <namespace>0</namespace>
<last_edited>2009-12-03T11:41:56Z</last_edited>
<last_editor>Fernandojvsilva</last_editor>

Introduction
------------------------

This is a quick guide for the lessons learned during Tridge's first tutorial (10/14/2009). Meeting's video can be found [http://samba.org/tridge/DRS-tutorial/20091014_Meeting.avi here].

Useful scripts 
------------------------

There are a set of useful scripts to use during development in:
samba/source4/scripting/devel/drs/

"vars" file should be updated to use these scripts.

Git grep 
------------------------

Look for specified patterns in the working tree files, blobs registered in the index file, or given tree objects.

e.g.: *git grep replPropertyMetaData*

Windows command to add user accounts:
------------------------

*C:\Users\Administrator>net user USERNAME PASSWORD /add

C:\Users\Administrator>net group "domain admins" USERNAME /add*

Searching an user description against a domain controller 
------------------------

*bin/ldbsearch -H ldap://w2k8 -Uadministrator%PASSWORD samaccountname=USERNAME description*

ldap://w2k8: windows domain controller

~/prefix/private/sam.ldb: local domain controler

Searching for a GUID 
------------------------

*bin/ldbsearch -H ../../prefix/private/sam.ldb --controls search_options:1:2 objectguid=b2c204f0-b050-4d8f-90e2-70d51adc070b dn objectGUID*

"--controls search_options:1:2" - LDAP searches across the entire databases, crossing partitions boundaries

Repadmin 
------------------------

Replication administration command that has all sort of useful operations where we can force Windows to do particular replication operations.

*repadmin /kcc*

Forces the knowledge consistency checker to run. Useful command to kickoff Windows to look for other domain controllers and to add them properly in the domain.

*repadmin /syncall /A*

Tells Windows Domain Controller to imediate do a full replication against all other domain controllers for all partitions.

DSReplicaSync message 
------------------------

This is a mechanism used by one domain controller to notify another domain controller that the first one has changes that the other domain controller should pull.

Replication happens as a pull operation, a domain controller do a GetNCChanges and that asks all the changes that another domain controller has to be packaged up and sent to the first domain controller.

replPropertyMetaData 
------------------------

Contains all the replication information of an object. Contains information about every replicable attribute and replicable object and says when it was last changed, how many times it has changed, and who last changed it.

*bin/ldbsearch -H ldap://w2k8 -Uadministrator%passL3N0V0 samaccountname=tridge replPropertyMetaData --show-binary*

repsTo and repsFrom 
------------------------

Used for a domain controller to replicate to/from another domain controller.
DSReplicaSync message uses repsTo.

*bin/ldbsearch -H ldap://w2k8 -Uadministrator%passL3N0V0 -s base --show-binary repsFrom*

@REPLCHANGED 
------------------------

Changes to the partitions are detected using @REPLCHANGED record. It tells that something has changed in the partition. Every time there is a change made to a replPropertyMetaData attribute, it also increases the uSNHighest attribute in the @REPLCHANGED object for each partition.

*bin/ldbsearch -H ../../prefix/private/users.ldb -b @REPLCHANGED -s base guid*

Changes are detected using @REPLCHANGED object. Then a DSReplicaSync message is sent to the other domain controllers. The other domain controllers then do a GetNCChanges that pulls the changed objects and updates the database.

replUpToDateVector 
------------------------

This attribute stored in each partition gives information about all of the previous replications that have happened to each other domain controller surround

*bin/ldbsearch -H ../../prefix/private/sam.ldb -s base replUpToDateVector --show-binary*

See Also 
------------------------

*`Samba4/ActiveDirectory`
*`Samba4/DRS TODO List`

External Links 
------------------------

* [http://www.google.com.br/url?sa=t&source=web&ct=res&cd=4&ved=0CBcQFjAD&url=http%3A%2F%2Fsamba.org%2F~metze%2Fpresentations%2F2007%2Fthesis%2FStefanMetzmacher_Bachelorthesis_ENG_Draft-9811557.pdf&ei=DhgQS4fKB4yIuAe60KS4CQ&usg=AFQjCNHWeFqiHoQ_Qdk2LBF216uRmPAMdQ&sig2=lxVeOAP1hK_NoCB2YO2L5A Stefan Metzmacher Bachelor Thesis about Active Directory Replication (English translated version) ]
* [http://msdn.microsoft.com/en-us/library/cc228086(PROT.13).aspx MS-DRSR: Directory Replication Service (DRS) Remote Protocol Specification at msdn.microsoft.com]
* [http://samba.org/tridge/DRS-tutorial/20091014_Meeting.avi DRS Tutorial about DRSUAPI Implementation in Samba and other stuff (Video)]