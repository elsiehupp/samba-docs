Smbd
    <namespace>0</namespace>
<last_edited>2006-05-12T08:07:07Z</last_edited>
<last_editor>Dralex</last_editor>

`smbd` is one of `Samba3` daemons. It does all SMB-protocol related work: receiving new client connections, processing `SMB` messages, etc. The main process runs with the root rights and starts new `smbd` process for all incoming tcp connections.

----

`Image:smbd.png`

----