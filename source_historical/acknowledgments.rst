Acknowledgments
    <namespace>0</namespace>
<last_edited>2007-09-20T07:02:40Z</last_edited>
<last_editor>Asender</last_editor>

==============

Acknowledgments
==============

Special thanks to Simo Sorce for your guidance and motivation to make this document happen.

Kudos to Howard Chu and the Openldap team for your help with replication methodologies. 

Thanks to all users who have provided feedback to help improve this document.