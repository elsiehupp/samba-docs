Registry Configuration
    <namespace>0</namespace>
<last_edited>2008-07-08T21:47:46Z</last_edited>
<last_editor>Obnox</last_editor>

==============================

egistry Configuration
===============================

UNFINISHED

Since version 3.2.0, Samba offers a registry based configuration system to complement the original text-only configuration via smb.conf. The new "net conf" command offers a dedicated interface for reading and modifying the registry based configuration.

Registry configuration can be activated in three levels:

* **registry shares**:finitions can be loaded from registry in addition to the shares defined in smb.conf. These shares are loaded not at samba startup time but on demand when accessed. Registry shares are activated by setting "registry shares = yes" in the global section of smb.conf.
* **registry only config**:;config backend = registry" is found in the global section of smb.conf, the text based configuration is completely ignored, global configuration is read from registry, and registry shares are enabled.
* **mixed text and registry config**: w special meaning of "include = registry" (in the global section of smb.conf), global configuration options are read from registry with the same semantics as an include file, i.e options found in smb.conf before "include = registry" are overriden by options from registry, and these in turn will be overridden by options in smb.conf found after  "include = registry". THis automatically activates registry shares.