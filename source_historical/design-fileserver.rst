Design/FileServer
    <namespace>0</namespace>
<last_edited>2009-05-03T15:35:10Z</last_edited>
<last_editor>Metze</last_editor>

==============================

esign ideas for a slim non blocking file server
===============================

* The socket to the client is in non blocking mode.
**IO only happens via tevent_req based functions.

* Each connection is represented by a structure.
** Everything that belongs to the connection is a talloc child of this structure.
** The connection has one tevent_context to operate on.
** No global variables.
** exit() is not used, but a ..._disconnect() function which gets the connection structure passed.
** All structures and functions should have nice names with a unique prefix (something like smbsrv_*).

* Authentication is done via a wbclient library (or something similar) only.
** The library should be tevent_req based.
** The library should also be used for krb5.

* The file server handles SMB1 and SMB2.
** The marshalling is strictly separated from the logic.
** But sendfile/recvfile/splice must still be possible.
** The file server shouldn't know anything about unix users/uids.

* The file server talks to a protocol independent ntvfs layer.
** To keep it simple only one module is allowed per tree connect (maybe extended to a module chain later).
** All functions are tevent_req based.
** All structures and functions should have nice names with a unique prefix (something like ntvfs_*).
** All structures and functions within a backend should have the same prefix.

* The IPC backend operates only async:
** Named pipes are supported genericly via unix sockets. (see lib/named_pipe_auth/)
** The LANMAN pipe for RAP also talks to a different process via an unix socket.
** We need to think a bit more about how to handle DFS requests.

* The PRINT backend operates only async:
** It uses a local directory to store files.
** It prints the localy stored files via ncalrpc to the local spoolss server.

* The FILE backend operates only async:
** It uses the tevent_req based thread trick to make system calls non blocking.
** We could use a tevent_req based wrapper to the `Portable Coroutine Library` to make complex functions fully async (we need to experiment a bit first).
** Filesystem specific features can be implemented by a posix vfs layer (Maybe the existing Samba3 VFS layer, but maybe much simpler)
** The async wbclient library is used to translate uids/gids to sids.