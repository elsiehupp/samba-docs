Release/3.4
    <namespace>0</namespace>
<last_edited>2009-05-04T05:37:07Z</last_edited>
<last_editor>Sdanneman</last_editor>

==============================

verview
===============================

Samba, version 3.4 is the second major release of the Samba project in 2009. 

===============================
Dates
===============================

(**Updated 30-April-2009**)

* Thursday, April 30 - Samba 3.4.0pre1 has been released
* Wednesday, July 1 - Planned release date for Samba 3.4.0

===============================
Features
===============================

{| class="sortable wikitable" border="1" cellpadding="5" cellspacing="0"
|-style="background-color:quot;
!Feature || Description || Status
|-
|`Feature/OneFS_VFS_Module|OneFS VFS Module` || Provides support for the OneFS filesystem via the Samba3 VFS layer. || <font color="green">Completed</font>
|-
|`Feature/Performance_Counter|Performance Counter` || A extensible API for tracking per SMB file server statistics. || <font color="green">Completed</font>
|}