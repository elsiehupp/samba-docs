LibGPO
    <namespace>0</namespace>
<last_edited>2010-03-28T16:54:50Z</last_edited>
<last_editor>Wilco</last_editor>


* Python bindings
* Port samba 3 libgpo tools (net gpo) to samba 4
* Merge libgpo with libpolicy (except for .adm(x) support)
* Make a load_gpo function which looks up relevant group policies for the user from LDAP and patches the local samba registry.
* Create tests!

Libgpo 
------------------------

* Needed functions:
** GPO iniparser
** List GPO's
** Fetch GPO's
** Group policy extensions
** Create GPT
*** Create empty tree in sysvol
*** Generate GPT.INI
** Create GPO
*** create group policy container
** Add GPO to DN
** Open policy
** Set policy setting
** List policy settings
* Python bindings

* Create utility:
** Finish ADM parser
** Utility must load a local registry context which it patches, edits and exports.
**