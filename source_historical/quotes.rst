Quotes
    <namespace>0</namespace>
<last_edited>2009-12-05T13:38:07Z</last_edited>
<last_editor>JelmerVernooij</or>
(in a fortune-like format)

.. code-block::

    Killing children is easy.
 -- tridge
%
01 < idra> I need some rest
01 < idra> that module really harms my brain
01 < idra> abartlet, I do not envy you
%
The only spec I trust is written in C.
 -- Andrew Bartlett
%
Here's a new adage for you:
"Don't let the crazy hacks take over".
 -- tridge
%
I can say 'no' in 4 different languages.
 -- Jeremy Allison
%
The web is never tarballed, tagged, and released.
 -- Deryck Hodge on Web 3.0
%
Port 80 is the new HTTP.
 -- Jeremy Allison on Web Services
</