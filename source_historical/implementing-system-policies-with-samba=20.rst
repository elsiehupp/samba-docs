Implementing System Policies with Samba=20
    <namespace>0</namespace>
<last_edited>2006-09-26T19:18:10Z</last_edited>
<last_editor>Zogness</last_editor>

Thanks to Anthony Ciarochi at Centeris for this solution.

I have a Centos (Red Hat-based) server that is now accessible to AD users
AND local users via ssh. I can control which AD groups can login using the
syntax below. Red Hat-based distros use "pam_stack" in pam.d which is quite
different than Debian's "include" based pam.d.

cat /etc/pam.d/sshd

.. code-block::

    auth       required     pam_stack.so service=system-auth
auth       required     pam_nologin.so
account    sufficient   pam_succeed_if.so user ingroup sshlogin
account    sufficient   pam_succeed_if.so user ingroup wheel
password   required     pam_stack.so service=system-auth
session    required     pam_stack.so service=system-auth
session    required     pam_loginuid.so
session    sufficient   pam_mkhomedir.so skel=/etc/skel umask=0027

The critical lines are:

   account    sufficient   pam_succeed_if.so user ingroup sshlogin

The above is to allow an AD group "sshlogin" to ssh in.

   account    sufficient   pam_succeed_if.so user ingroup wheel

The above allows anyone in the *local machine* unix group "wheel" to ssh in.

   session    sufficient   pam_mkhomedir.so skel=/etc/skel umask=0027

The above creates home dirs and dot files for AD users when they login for
the first time.