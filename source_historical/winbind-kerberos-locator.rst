Winbind Kerberos Locator
    <namespace>0</namespace>
<last_edited>2007-10-30T12:05:42Z</last_edited>
<last_editor>Gd</last_editor>

With version x of Samba, the system library can use a samba krb5 locator plugin to detect KDCs.

A locator offers to bypass builtin name resolution routines of the kerberos library. By that, a locator plugin can force the kerberos library to use a specific server. Often, in Windows networks, this is exactly what you want to achieve. Very often you want to force the kerberos library to use exactly the same server as the samba binaries (smbd, winbind) do.

Build instructions 
------------------------

There is no need to enable the build of the locator plugin. Samba will automatically build it as long as the system kerberos library has support for locator plugins.

You can check your build output for that while looking for:

 ...
 checking for krb5/locate_plugin.h   yes
 ...

After finishing the build process, the locator plugin can be found in $SAMBA_SRC/bin/winbind_krb5_locator.so

Configuration 
------------------------

Once build, the locator needs to be installed into the appropriate plugin directories:

* Heimdal

* MIT

Usage 
------------------------

Winbind needs to run for the plugin to work.