OSW InteropDay
    <namespace>0</namespace>
<last_edited>2009-08-09T20:39:31Z</last_edited>
<last_editor>Jerry</last_editor>

Linux /t Interoperability Day: August 11, 2009 
------------------------

One part of the system administrator's job falls under both necessary daily routine and mysterious black art:ng your Microsoft systems to your open source ones.  In this community day, participants will blast aside the shadows of FUD, inflated claims, and complexity, and learn to make Microsoft Windows and open source play nicely -- from simple, fast file sharing up to a standards-based single sign-on environment.

Interoperability Day brings in both leading software developers and the sysadmins and CIOs who are making interoperability happen.

* Linux tools for windows client imaging and recovery
* Directory server strategies for reliable mixed environments on a budget
* Samba security, configuration, and performance tuning for Windows Vista and Windows 7 clients
* Relax, it works out of the box:ools you already have on any up-to-date Linux.
* Real-world Q&A:ur toughest interoperability problem.

Draft Schedule 
------------------------

<table>
    tr>
    <th align="left">Time</
    <th align="left">Speaker</
    <th align="left">Topic</
    /

    tr>
    <td>9:H10:00<SS:>
    <td>organizers</
    <td>Welcome and approve schedule</
    /

    tr>
    <td>10:H10:50<SS:>
    <td>Jeremy Allison</
    <td>Native Windows ACLs in Samba 3</
    /

    tr>
    <td>11:H11:50<SS:>
    <td>Gerald Carter</
    <td>[http://likewiseopen.org/ Likewis/atus Report</td>/
    /

    tr>
    <td>12:H1:30<SS:>
    <td>n/gt;/
    <td>Lunch</
    /

    tr>
    <td>4:H5:00<SS:>
    <td>All</
    <td>Interoperability Q and A</
    /

</

(this event is an unconference, so we will set the final schedule at the event itself.)

Organizers 
------------------------

* [mailto:p.org Don Marti] (OSW)
* [mailto:.org Jeremy Allison] (Samba Team)
* [mailto:ba.org Gerald Carter] (Likewise)
* [mailto:.org John Terpstra] (Samba/ys)
* [mailto:idertraining.com Alex Monteiro] (InsiderTraining)

Event Scheduling 
------------------------

Held immediately before OpenSource World::SSLLAASS:HHwww.opensourceworld.com/oscone Center in San Francisco, California.

This event is free of charge, but you need to register for OpenSource World and obtain a badge to enter.  If the OpenSource World system denies you, mail Don Marti:gp.org for help and/ess code.