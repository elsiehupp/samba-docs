Samba4/CLI/NTSTATUS
    <namespace>0</namespace>
<last_edited>2010-01-08T03:00:27Z</last_edited>
<last_editor>Edewata</last_editor>

=================
Data Types
=================

.. code-block::

    typedef uint32_t NTSTATUS;

=================
Functions
=================

nt_errstr() 
------------------------

.. code-block::

    const char *nt_errstr(NTSTATUS nt_code);

get_friendly_nt_error_msg() 
------------------------

.. code-block::

    const char *get_friendly_nt_error_msg(NTSTATUS nt_code);

get_nt_error_c_code() 
------------------------

.. code-block::

    const char *get_nt_error_c_code(NTSTATUS nt_code);

nt_status_string_to_code() 
------------------------

.. code-block::

    NTSTATUS nt_status_string_to_code(const char *nt_status_str);