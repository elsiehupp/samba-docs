PAM Kerberos Authentication
    <namespace>0</namespace>
<last_edited>2007-10-24T14:22:32Z</last_edited>
<last_editor>Gd</last_editor>

Kerberos Authentication using winbindd 
------------------------

In order to enable kerberos authentication configure Samba to use winbind in nsswitch and for PAM (FIXME: point to other docs).

Enabling Kerberos authentication in pam_winbind 
------------------------

First of all, make sure that you can login using PAM and your windows credentials, e.g. using ssh:
 ssh YOURDOM\\youruser@localhost

You cannot continue if login via PAM (pam_winbind) is not working.

Now, pam_winbind needs to set a kerberos flag, you can do so by either

* adding "krb5_auth = yes" and "krb5_ccache_type = FILE" to /etc/security/pam_winbind.conf. That file should look like this:

 #
 # pam_winbind configuration file
 #
 # /etc/security/pam_winbind.conf
 #
 [global]
 # authenticate using kerberos
 krb5_auth = yes

 # when using kerberos, request a "FILE" krb5 credential cache type
 # (leave empty to just do krb5 authentication but not have a ticket
 # afterwards)
 krb5_ccache_type = FILE

This will enable kerberos authentication globally for all applications using PAM. If you want to have more fine grained control about services that use pam_winbind's kerberos mode then you can do so by

* adding the "krb5_auth" and "krb5_ccache_type" option into individual pam-configuration files (usualy below /etc/pam.d/$SERVICE)

Testing Kerberos authentication 
------------------------

Start winbindd, authenticate successfully at least once while winbind is online

 /etc/init.d/winbind start

    binfo -K YOURDOM\\youruser%password

You should get

 plaintext kerberos password authentication for [YOURDOM\youruser%password] succeeded (requesting cctype: FILE)
 credentials were put in: FILE:/tmp/krb5cc_0

in the output.

Your system is now prepared to use pam_winbind for kerberos authentication. Please try to login to your localhost, e.g. using ssh
 ssh YOURDOM\\youruser@localhost

After successful login "klist" should show your ticket granting ticket.