Cluster
    <namespace>0</namespace>
<last_edited>2006-05-10T08:40:15Z</last_edited>
<last_editor>Dralex</last_editor>

A clustered file server ideally has the following properties: 
* All clients can connect to any server.
* A server can fail and clients are transparently reconnected to another server.
* All servers can serve out the same set of files.
* All file changes are immediately seen on all servers (distributed filesystem)
* Ability to scale by adding more servers/disk backend.
* Appears as a single large system.