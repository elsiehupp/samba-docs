LinuxCIFS CredentialStashing
    <namespace>0</namespace>
<last_edited>2010-05-06T07:24:47Z</last_edited>
<last_editor>Jlayton</last_editor>

Overview 
------------------------

This is a companion project to `LinuxCIFS_Multisession_Mount`. With them, session setup is delayed and we cannot prompt for authentication info at session setup time. This effectively limits that work to using Kerberos auth.

It needn't be that way however. If we could stash authentication info on a per-user basis, then we could use any NTLM-based authentication scheme.

A breakdown of the project 
------------------------

Core features:
* a userspace program that can stash the appropriate info in the keyctl keyring.
**The per-user keying is probably a good choice, but there may be an argument for doing this on a per-session basis.
* kernel code to get that info out of the keyring so that CIFS can use it

Optional (Future) features:
* working SPNEGO encapsulated NTLM 
** current code does not work correctly, and this would allow us to mix use of krb5 and NTLM  for multisession mounts
* an upcall that can get these credentials if they don't exist in the keyring (possibly from winbind)

Details 
------------------------

A representation of the upcall/downcall info will need to be settled upon. The upcall should be a string of text, and the downcall can be anything. A delimited text string may be sufficient, or a binary struct could be used. The latter may be more future-proof.

For the password field, we need to determine what the kernel should hold. Would it be better to store the password itself or an intermediate stage of the NTLM password? Consider that in the event of a crash and core dump, that the contents of the keyring could be exposed.

Consider how to distinguish credentials that are domain level, and those that are only valid on a particular host. The kernel may need to be fixed to take better advantage of the domain= mount option.