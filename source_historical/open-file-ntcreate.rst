Open file ntcreate
    <namespace>0</namespace>
<last_edited>2006-05-18T14:03:31Z</last_edited>
<last_editor>Dralex</last_editor>

See *smbd/open.c/open_file_ntcreate*.

The function implements **NTCreate & X** operation (create, open, overwrite file, check access right, etc) in `smbd`. It converts `SMB` parameters (open mode flags, sharing flags, etc) to UNIX parameters used in unix **open** call, checks share access to the file, obtains exclusive access to it and makes UNIX **open** operation.

Function arguments
------------------------

# *conn* - current `connection`
# *psbuf* - stat-structure of the file (may be invalid for a new file)
# *fname* - file name
# access_mask - file access mask:
#* DELETE - file can be deleted
#* FILE_APPEND_DATA - appending (for directory - creating subdirectories)
#* FILE_READ_DATA - reading
#* FILE_WRITE_DATA - writing
#* FILE_EXECUTE - executing
#* SYNCHRONIZE - synchonize all IO operations on the returned handle
#* FILE_READ_ATTRIBUTES - file attributes can be read
#* FILE_WRITE_ATTRIBUTES - file attributes can can be written
#* ...
# *share_access* - file shared access mode:
#* FILE_SHARE_NONE - sharing disabled
#* FILE_SHARE_DELETE - enables delete access for open files
#* FILE_SHARE_READ - shared read access
#* FILE_SHARE_WRITE - shared write access
# *create_disposition* - an action to take on files (if they exist or don't exist):
#* FILE_SUPERSEDE - if the file already exists, replace it with the given file. If it does not, create the given file
#* FILE_CREATE - if the file already exists, fail the request and do not create or open the given file. If it does not, create the given file
#* FILE_OPEN - if the file already exists, open it instead of creating a new file. If it does not, fail the request and do not create a new file
#* FILE_OPEN_IF - if the file already exists, open it. If it does not, create the given file
#* FILE_OVERWRITE - if the file already exists, open it and overwrite it. If it does not, fail the request
#* FILE_OVERWRITE_IF - if the file already exists, open it and overwrite it. If it does not, create the given file create_options - options of file creation:
#* FILE_DIRECTORY_FILE - the file being created or opened is a directory file
#* FILE_NON_DIRECTORY_FILE - the file being opened must not be a directory file or this call fails
#* FILE_WRITE_THROUGH - synchonous data transfer
#* FILE_SEQUENTIAL_ONLY - all accesses to the file are sequential
#* FILE_RANDOM_ACCESS - accesses to the file can be random
#* FILE_NO_INTERMEDIATE_BUFFERING - the file cannot be cached or buffered in a driver's internal buffers
#* FILE_DELETE_ON_CLOSE - delete the file when the last handle to it is passed to NtClose
# *oplock_request* - `oplock|opportunistic lock`, used with connection
# *pinfo* - points to a variable that receives the final completion status of the operation:
#* FILE_CREATED - file was created
#* FILE_OPENED - file was opened
#* FILE_OVERWRITTEN - file was overwritten
#* FILE_SUPERSEDED - file was superseded
#* FILE_EXISTS - error: file exists
#* FILE_DOES_NOT_EXIST - error: file doesn't exist

Fuction local variables
------------------------

* *flags* (for file io) & *flags2* (for file creation) - flags used for UNIX open file function (O_CREATE, O_TRUNC, etc)
* *file_existed* - a flag: TRUE if file already exists
* *def_acl* - a flag: TRUE if parent directory has default ACL
* *internal_only_open* - a flag: TRUE if `oplock|opportunistic lock` (or `deferred open|deferring`) is not used (file is opened for internal purposes)
* *dev* & *inode* - device and inode number of the existed file
* *fsp_open* - flag: TRUE if fsb was succesfully created (file was opened correctly)
* *fsp* - file structure
* *new_unx_mode* & *unx_mode* - dos file attributes projcted on the UNIX rights
* *info* - variable with status of the file operation (see *pinfo* above)
* *existing_dos_attributes* - dos file attributes
* *pml* - current `deferred open|deferred` smb messages list entry
* *mid* - current smb message id
* *delayed_for_oplocks* - flag: TRUE if message was delayed for oplock
* *request_time* - time of the open request
* *lck* - lock structure
* *status* - NT Status of the share mode check operation

Program flow diagram
------------------------

You can see program flow diagram in one PDF file: [http://samba.org/~ab/dralex/open_file_ntcreate.pdf] or find SVG sources here - [http://samba.org/~ab/dralex].

Call graph
------------------------

Huge call graph of the function: [http://wiki.samba.org/images/1/10/Open_file_ntcreate.png]