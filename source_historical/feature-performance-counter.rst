Feature/Performance Counter
    <namespace>0</namespace>
<last_edited>2009-05-04T05:44:09Z</last_edited>
<last_editor>Sdanneman</last_editor>

==============================

verview
===============================

Samba's new Performance Counter infrastructure provides PDU based statistics for the Samba file server.  Each incoming SMB creates a call to the generalized perfcount framework, from which a backend module can track PDU type, size, latency, and user.

Two backend modules are currently implemented.

===============================
Components
===============================

{| class="sortable wikitable" border="1" cellpadding="5" cellspacing="0"
|-style="background-color:quot;
!Component || Description || Developer || Status
|-
|SMBD Infrastructure || Implement a dispatch table API which different backends can plug into. || `tstecher` || <font color="green">Completed</font>
|-
|perfcount_onefs.so || A module specific to the OneFS operating system which makes syscalls storing aggregate SMB performance measurements in kernel memory.  These statistics are later retrieved via a command line tool. || `tstecher` || <font color="green">Completed</font>
|-
|perfcount_test.so || A simple module which writes packet data out to the samba.log file every 50 operations. || `tstecher` || <font color="green">Completed</font>
|}

===============================
Future Development
===============================

* Implement a more generic Unix backend which logs to a database.
* Integrate with Holger's [http://holger123.wordpress.com/smb-traffic-analyzer/ SMB Traffic Analyzer] backend and CLI tools.
* Remove deprecated START_PROFILE()/END_PROFILE() subsystem