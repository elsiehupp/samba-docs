Winreg
    <namespace>0</namespace>
<last_edited>2010-05-17T16:23:48Z</last_edited>
<last_editor>GlaDiaC</last_editor>

=================
WINREG Todos
=================

* add broader smbtorture tests (maybe also win32 tests)
* fix invalid winreg IDL where [size_is(*unique_ptr)] and [length_is(*unique_ptr)] (also in pidl if possible)
* avoid unique NULL pointer derref in various winreg server calls
* handle SEC_FLAG_MAXIMUM_ALLOWED request access_mask mapping
* handle sec_initial_uid() == geteuid() root free-pass
* think about avoiding data normalisation
* merge underlying registry implementations from s3 and s4 (possibly by first using the s3 backend code as one backend in the s4 registry library)

Backend cleanup 
------------------------

* remove remaining calls to reg_dispatcher calls (store_reg_key, fetch_reg_keys etc) from the code (e.g. services/services_db.c) so that only reg_api.c is used to locally access the registry, and make reg_dispatcher and friends private (`User:Obnox|obnox`)
* create an api layer that uses the winreg rpc_client stubs with a local rpc binding (rpc_pipe_open_internal()) to call out directly to the server code. this will unify the local and remote access to registry in the code. (`User:Obnox|obnox`)

=================
Registry Secrets
=================

REG_LINK 
------------------------

The MSDN docs describes REG_LINK as: "A null-terminated Unicode string that contains the target path of a symbolic link that was created by calling the RegCreateKeyEx function with REG_OPTION_CREATE_LINK."

Here’s the part that is not documented:

You have to set a special value called "SymbolicLinkValue" of type REG_LINK in the symbolic link key. The data associated with this special value is the target registry key to link to. The target registry key needs to be in kernel-mode registry syntax.

User-mode keys converted to kernel-mode:

    * HKEY_LOCAL_MACHINE is converted to \\registry\machine.
    * HKEY_USERS is converted to \\registry\user.
    * HKEY_CURRENT_USER is converted to \\registry\user\user_sid, where user_sid is the Security ID associated with the user.
    * HKEY_CLASSES_ROOT is converted to \\registry\machine\software\classes.

Registry links 
------------------------

Original:

    OFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Print\\Printers
    YSTEM\\CurrentControlSet001

Links:

    YSTEM\\CurrentControlSet -> SYSTEM\\CurrentControlSet001
    YSTEM\\CurrentControlSet001\\Control\\Print\\Printers -> SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Print\\Printers

history 
------------------------

--`User:Gd|gd` 04:00, 10 March 2010 (CST)

--`User:Obnox|obnox` 07:44, 12 May 2010 (CDT)

--`User:GlaDiaC|GlaDiaC` 18:21, 17 May 2010 (CEST)