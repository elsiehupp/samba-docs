Endpoint Mapper
    <namespace>0</namespace>
<last_edited>2011-08-17T10:18:45Z</last_edited>
<last_editor>GlaDiaC</last_editor>

=================
Research
=================

General/FreeDCE 
------------------------

EPM Server
------------------------

 Init =
===============================

------------------------

The endpoint mapper loads or creates a database (file). With the information it creates a in-memory structure. Then it starts a ping service to detect if an rpc service is alive. The ping services modifies the in-memory structure and the file database if one of the services died.
The reason of this is that you can restart the daemon.

 Insert and Delete =
===============================

------------------------

These function check if the entry handle is allowed to insert or delete a rpc service. Else it would be possible for unprivileged user to add or remove services.

http://www.opengroup.org/onlinepubs/9629399/apdxo.htm

 Map and Lookup =
===============================

------------------------

RPC Server
------------------------

The rpc services are using rpc_ep_register or rpc_ep_register_no_replace to tell the endpoint mapper that they are up an running now. On shutdown rpc_ep_unregister is called to remove the entry from the enpoint mapper.

http://www.opengroup.org/onlinepubs/9629399/rpc_ep_register.htm

rpc_ep_register is calling the rpc client function dceprc_epm_insert over ncalrpc (local interprocess communication).

Microsoft 
------------------------

This is a epmlookup for lsarpc against a win2k8 with ipv6:

.. code-block::

    12345778-1234-abcd-ef00-0123456789ac@ncacn_http:DOOTT0.0[49157]::
12345778-1234-abcd-ef00-0123456789ac@ncacn_ip_tcp:DOOTT0.0[49158]::
12345678-1234-abcd-ef00-01234567cffb@ncacn_np:SSHH80VUET7UE3R[\pipe\lsass]::