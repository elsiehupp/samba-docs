Replicated Failover Domain Controller and file server using LDAP
    <namespace>0</namespace>
<last_edited>2009-10-28T15:31:18Z</last_edited>
<last_editor>Asender</last_editor>

**SAMBA 3 EXTENSIONS**

**TECHNICAL CONFIGURATION**

**Adrian Sender** <asender@samba.org>

**Simo Sorce** <idra@samba.org>


***`Acknowledgments`**
***`Overview`**
***`Objectives`**

**Failover Domain Controller**

**High Availability Cluster**

**Directory Based**

**Small Business HA Solution**

**Clustered File System**

***`1.0. Configuring Samba`**

*[http://wiki.samba.org/index.php/1.0._Configuring_Samba#1.1._smb.conf_PDC 1.1. smb.conf PDC]

*[http://wiki.samba.org/index.php/1.0._Configuring_Samba#1.2._smb.conf_BDC 1.2. smb.conf BDC]

*[http://wiki.samba.org/index.php/1.0._Configuring_Samba#1.3._.2Fetc.2Fhosts 1.3. /etc/hosts]

*[http://wiki.samba.org/index.php/1.0._Configuring_Samba#1.4._Samba_Security 1.4. Samba Security]

***`2.0. Configuring LDAP`**

*[http://wiki.samba.org/index.php/2.0._Configuring_LDAP#2.1._Installing_LDAP 2.1. Installing LDAP]

*[http://wiki.samba.org/index.php/2.0._Configuring_LDAP#2.2._slapd.conf_Master_slurpd 2.2. slapd.conf slurpd]

*[http://wiki.samba.org/index.php/2.0._Configuring_LDAP#2.2.1._slapd.conf_Master_syncrepl_Openldap2.2 2.2.1. slapd.conf Master syncrepl Openldap2.2]

*[http://wiki.samba.org/index.php/2.0._Configuring_LDAP#2.2.2._slapd.conf_Master_delta-syncrepl_Openldap2.3 2.2.2. slapd.conf Master delta-syncrepl Openldap2.3]

*[http://wiki.samba.org/index.php/2.0._Configuring_LDAP#2.3._slapd.conf_Slave_slurpd 2.3. slapd.conf Slave slurpd]

*[http://wiki.samba.org/index.php/2.0._Configuring_LDAP#2.3.1._slapd.conf_Slave_syncrepl_Openldap2.2 2.3.1. slapd.conf Slave syncrepl Openldap2.2]

*[http://wiki.samba.org/index.php/2.0._Configuring_LDAP#2.3.2._slapd.conf_Slave_delta-syncrepl_Openldap2.3 2.3.2. slapd.conf Slave delta-syncrepl Openldap2.3]

*[http://wiki.samba.org/index.php/2.0._Configuring_LDAP#2.4._ldap.conf_Master 2.4. ldap.conf Master]

*[http://wiki.samba.org/index.php/2.0:_Configu:2.5._ldap.conf_Slave 2.5. ldap.conf Slave]

*[http://wiki.samba.org/index.php/2.0._Configuring_LDAP#2.6._.2Fetc.2Fnsswitch.conf 2.6. /etc/nsswitch.conf]

*[http://wiki.samba.org/index.php/2.0._Configuring_LDAP#2.7._Berkeley_DB 2.7. Berkeley DB]

***`3.0. Initialization LDAP Database`**

*[http://wiki.samba.org/index.php/3.0:_Initial:AP_Database#3.1._Provisioning_Database 3.1. Provisioning Database]

*[http://wiki.samba.org/index.php/3.0:_Initial:AP_Database#3.2._Preload_LDIF 3.2. Preload LDIF]

*[http://wiki.samba.org/index.php/3.0:_Initial:AP_Database#3.3._LDAP_Population 3.3. LDAP Population]

*[http://wiki.samba.org/index.php/3.0._Initialization_LDAP_Database#3.4_Running_LDAP 3.4. Running LDAP]

*[http://wiki.samba.org/index.php/3.0._Initialization_LDAP_Database#3.4.1_Starting_LDAP_On_Boot 3.4.1 Starting LDAP On Boot /etc/rc.local]

*[http://wiki.samba.org/index.php/3.0:_Initial:AP_Database#3.5._Database_Replication 3.5. Database Replication]

*[http://wiki.samba.org/index.php/3.0:_Initial:AP_Database#3.6._Debugging_LDAP 3.6 Debugging LDAP]

***`4.0. User Management`**

*[http://wiki.samba.org/index.php/4.0._User_Management#4.1._smbldap-tools 4.1. smbldap-tools]

*[http://wiki.samba.org/index.php/4.0._User_Management#4.1.1._smbldap.conf_Master 4.1.1. smbldap.conf Master]

*[http://wiki.samba.org/index.php/4.0._User_Management#4.1.2._smbldap.conf_Slave 4.1.2. smbldap.conf Slave]

*[http://wiki.samba.org/index.php/4.0._User_Management#4.1.3._smbldap_bind.conf 4.1.3. smbldap_bind.conf] 

***`5.0. Heartbeat HA Configuration`** *Obsolete - project merged to pacemaker. 

*[http://wiki.samba.org/index.php/5.0._Heartbeat_HA_Configuration#5.1._Requirements 5.1. Requirements]

*[http://wiki.samba.org/index.php/5.0._Heartbeat_HA_Configuration#5.2._Installation 5.2. Installation]

*[http://wiki.samba.org/index.php/5.0._Heartbeat_HA_Configuration#5.3._Configuration 5.3. Configuration] 

*[http://wiki.samba.org/index.php/5.0._Heartbeat_HA_Configuration#5.3.1._ha.cf 5.3.1. ha.cf]

*[http://wiki.samba.org/index.php/5.0._Heartbeat_HA_Configuration#5.3.2._haresources 5.3.2. haresources]

* [http://wiki.samba.org/index.php/5.0._Heartbeat_HA_Configuration#5.3.3._authkeys 5.3.3. authkeys]

*[http://wiki.samba.org/index.php/5.0._Heartbeat_HA_Configuration#5.4._Testing 5.4. Testing]

***`6.0. DRBD`** 

*[http://wiki.samba.org/index.php/6.0._DRBD#6.1._Requirements 6.1. Requirements]

*[http://wiki.samba.org/index.php/6.0._DRBD#6.2._Installation 6.2. Installation]

*[http://wiki.samba.org/index.php/6.0._DRBD#6.3._Configuration 6.3. Configuration]

*[http://wiki.samba.org/index.php/6.0._DRBD#6.3.1._drbd.conf 6.3.1. drbd.conf]

*[http://wiki.samba.org/index.php/6.0._DRBD#6.3.2._Initialization 6.3.2. Initialization]

*[http://wiki.samba.org/index.php/6.0._DRBD#6.4._Testing 6.4. Testing]

*[http://wiki.samba.org/index.php/6.0:_DRBD#6D:TT_DRBD_8.0_GFS2_Primary.2FPrimary_Clustered_Filesystem 6.5 DRBD 8.0 GFS2 Primary/Primary Clustered Filesystem]

*[http://wiki.samba.org/index.php/6.0:_DRBD#6D:TT_Virtualization 6.6 Virtualization]

***`7.0. BIND DNS`**

*[http://wiki.samba.org/index.php/7.0._BIND_DNS#7.1._Configuration 7.1. Configuration]

*[http://wiki.samba.org/index.php/7.0._BIND_DNS#7.1.1._named.conf 7.1.1. named.conf]

*[http://wiki.samba.org/index.php/7.0._BIND_DNS#7.1.2._zone_file 7.1.2. zone file]