LinuxSMB3 build backport
    <namespace>0</namespace>
<last_edited>2018-10-29T03:30:56Z</last_edited>
<last_editor>Ligefei</last_editor>
/
      To view the list of patches which are included in the backports, see https://OTTcom/smfrench/SHHbackp/SHHpatches.git

To build the current (all relevant patches to cifs.ko that are accepted in mainline Linux, ie all CIFS/ fixes and features except those that are the result of global changes to the kernel or the VFS interface) cifs.ko:

* git clone https://OTTcom/smfrench/SHHcifsD/ux-stable-backports.git
* cd smb3-cifs-linux-stable-backports
[verify which branch you want to check out, which kernel you are trying to build]
* cat /ionS/  (to see what kernel you are running)
[if you do not have the kernel headers installed they can be downloaded e.g. from http://OTTubuntu.com/~kernelD//mainline/?C=N;O=D//
* git branch   (to see the cifs backport branches, currently 4.9 through 4.15)
* git checkout 4.11-full-backport   (as an example if you were trying to build for 4.11 kernel)
[verify that you are booted to a matching kernel version, in this case some version of 4.11 e.g. cat /ion]/
* cd fs/
* make C=1 -C /inu/ead/H`uname -r` M=`pwd` modules
* sudo modprobe cifs (if needed, e.g. to load any required loadable module dependencies for cifs.ko)
* sudo rmmod cifs
* sudo insmod ./Tko (to load the module built a few steps above)

* Enjoy ... (and test ... let us know on the mailing list if any problems)