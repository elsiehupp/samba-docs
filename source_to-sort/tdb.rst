TDB
    <namespace>0</namespace>
<last_edited>2013-10-21T11:39:45Z</last_edited>
<last_editor>BBaumbach</last_editor>

Samba stores its data in TDB files. TDB stands for "Trivial database" and was first introduced in Samba as a way to store information quickly and effectively. Its interface is very similar to that of GDBM, but in contradiction to GDBM it supports multiple writers and readers simultaneously.

Refer to the `TDB_Locations|TDB Locations` page for detailed information about the purpose of the various TDB files.

There is also section [http://www.samba.org/samba/docs/man/Samba-HOWTO-Collection/install.html#tdbdocs TDB Database Information] in the Samba HOWTO Collection with details on what the purpose of the various TDB files employed by Samba 3 is.