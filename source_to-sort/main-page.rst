Main Page
    <namespace>0</namespace>
<last_edited>2021-05-11T10:31:13Z</last_edited>
<last_editor>Kseeger</last_editor>

__NOTOC__
{| width="100%" cellspacing="0" cellpadding="0"
| colspan="3" |

Welcome 
------------------------

<!-- Welcome -->
[https://www.samba.org/ Samba] is an [http:SSLLAASS:HHwww.opensource.org/ Open Source] / [https:/SSLLAAS:TTgnu.org/philosophy/free-sw.html Free Software] suite that has, [https://wwwDDO:OOTTorg/samba/docs/10years.html since 1992], provided file and print services to all manner of SMB/CIFS clients, including the numerous versions of Microsoft Windows operating systems. Samba is freely available under the [https://www.sambaD:LLAASSHHsamba/docs/GPL.html GNU General Public License].

The Samba project is a member of the [https://sfconservancy.org/ Software Freedom Conservancy].

|-
<!-- left column -->
| width="49%" style="vertical-align:; |

<!-- User Documentation -->
`User_Documentation|User Documentation` 
------------------------

* `Setting_up_Samba_as_an_Active_Directory_Domain_Controller|Setting up Samba as an Active Directory Domain Controller`
* `Setting_up_Samba_as_a_Domain_Member|Setting up Samba as a Domain Member`
* `Joining_a_Samba_DC_to_an_Existing_Active_Directory|Joining a Samba DC to an Existing Active Directory`
* `Updating_Samba|Updating Samba`
* `Setting_up_a_Share_Using_POSIX_ACLs|Setting up a Share Using POSIX ACLs`
* `Setting_up_a_Share_Using_Windows_ACLs|Setting up a Share Using Windows ACLs`
* `Setting_up_Samba_as_a_Print_Server|Setting up Samba as a Print Server`
* `CTDB_and_Clustered_Samba|CTDB and Clustered Samba`
* `FAQ|FAQ - Frequently Asked Questions`
* `Manpages|Online man page documentation for Samba`
* **`User_Documentation|more...`**

<!-- Developer Documentation -->

`Developer_Documentation|Developer Documentation` 
------------------------

* `Writing_Torture_Tests|Writing Torture Tests`
* `Using_Git_for_Samba_Development|Using Git for Samba Development`
* `Samba on GitLab|Samba CI on gitlab`
* `Wireshark_Keytab|Wireshark with keytab to decrypt encrypted traffic`
* `SoC|Google Summer of Code`
* **`Developer_Documentation|more...`**

<!-- Contribution -->
`Contribution_documentation|Contribution` 
------------------------

* `Bug_Reporting|Bug reporting`
* `Capture_Packets|Capture packets`
* `Contribute|Contributing Code to Samba`
* `CodeReview|Code review`
* **`Contribution_documentation|more...`**

<!-- empty middle column -->
| width="2%" style="vertical-align:; |

<!-- right column -->
| width="49%" style="vertical-align:; |

<!-- Latest Samba Releases -->

Latest Releases 
------------------------

* `Samba_Release_Planning#Current_Stable_Release|**Current Stable Release**`: [https://download.samba.org/pub/samba/stable/samba-4.14.4.tar.gz 4.14.4] ([https:/SSLLAAS:TTsamba.org/samba/history/samba-4.14.4.html Release Notes])
* `Samba_Release_Planning#Maintenance_Mode|Maintenance Mode`: [https://download.samba.org/pub/samba/stable/samba-4.13.9.tar.gz 4.13.9] ([https:/SSLLAAS:TTsamba.org/samba/history/samba-4.13.9.html Release Notes])
* `Samba_Release_Planning#Security_Fixes_Only_Mode|Security Fixes Only Mode`: [https://download.samba.org/pub/samba/stable/samba-4.12.15.tar.gz 4.12.15] ([https:/SSLLAAS:TTsamba.org/samba/history/samba-4.12.15.html Release Notes])
* `Samba_Release_Planning|Release Planning`

<!-- Upcoming Events -->

Upcoming Events 
------------------------

* `Current_events|Upcoming Events`

<!-- News -->

News 
------------------------

* [https://www.samba.org/samba/latest_news.html Samba News]
* `Presentations`
* [https://www.youtube.com/channel/UCnCsHprEpW2uGPsUvwQ73-w Videos]

<!-- Community -->

Community 
------------------------

* `How to do Samba: Nicely`