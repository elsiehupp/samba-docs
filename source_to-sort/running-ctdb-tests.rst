Running CTDB tests
    <namespace>0</namespace>
<last_edited>2020-03-07T22:19:20Z</last_edited>
<last_editor>MartinSchwenke</last_editor>

Introduction 
------------------------

CTDB has a standalone test suite, split into several collections:

;UNIT
* Tests that do not require CTDB daemons.  Useful for testing particular subsets of the CTDB code.
;INTEGRATION
* Tests that require a cluster of CTDB daemons.  These can be run as an unprivileged user against local CTDB daemons on a test or development machine.  Most will also run against a real (or virtual) cluster of nodes - those that do not will be skipped.  Each test case will start the CTDB daemons beforehand and stop them afterwards.
;CLUSTER
* These tests require a real (or virtual) cluster of nodes, plus an additional test node.  These tests *can not* be run on a single test or development machine (without virtualising the cluster nodes).  Each test case will start the CTDB daemons beforehand and stop them afterwards.

Examples below assume they are run from within the **ctdb/** subdirectory of the source tree.

Running tests 
------------------------

To run all the UNIT tests:

 $ ./tests/run_tests.sh UNIT/

See **ctdb/tests/README** for more detailed information on how to run tests.

If the tests have been installed (either directly via ``make install`` or via a package) then the command will be ``ctdb_run_tests``.

Starting a CTDB local daemons test environment 
------------------------

``./tests/local_daemons.sh`` can be used to start a local daemons test environment.  This is the same environment that is used in the INTEGRATION test collection.

 $ ./tests/local_daemons.sh foo setup
 $ ./tests/local_daemons.sh foo start all
 $ ./tests/local_daemons.sh foo onnode 0 ctdb status
 [wait a while]
 $ ./tests/local_daemons.sh foo print-log all | less

The first argument (``foo`` in the above example) is a daemon configuration directory.

See ``./tests/local_daemons.sh -h`` for usage information.

Individual commands can also have their usage shown (e.g. ``./tests/local_daemons.sh foo setup -h``).  Note that a daemon configuration directory must be given for this to work.

If the tests have been installed (either directly via ``make install`` or via a package) then the command will be ``ctdb_local_daemons``.