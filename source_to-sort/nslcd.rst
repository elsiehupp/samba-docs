Nslcd
    <namespace>0</namespace>
<last_edited>2019-08-22T08:23:08Z</last_edited>
<last_editor>Hortimech</last_editor>
/
===============================

Introduction
===============================

The ``nslcd</ service enables you to configure your local system to load users and groups from an LDAP directory, such as Active Directory (AD).

To enable the ``nslcd</ service to load user and group information, you have to set the Unix attributes for users and groups in AD. For details, see `Maintaining_Unix_Attributes_in_AD_using_ADUC|Maintaining Unix Attributes in AD using ADUC`.

.. note:

    Samba does not provide support for the ``nslcd</ service, other than what is on this page.

=================
Configuring the nslcd Service
=================

Authenticating nslcd to AD Using a User Name and Password 
------------------------

To enable the ``nslcd</ service to authenticate to Active Directory (AD) using a user name and password:

* Create a new user in AD. For example: ``nslcd-ad</
* Set the following options in the account's settings:
* * Password never expires
* * User cannot change password

* Add the following parameter to the ``[global]</ section of your ``smb.conf``/file:

 acl:search = no

* Restart Samba.

* Edit the ``/DDO/;/code> / and set the following settings:

 # Local user account and group, nslcd uses.
 uid nslcd
 gid ldap

 # Active Directory server settings (SSL encryption)
 uri             ldaps://DOOTT1:636//
 ssl             on
 tls_reqcert     allow
 base            dc=SAMDOM,dc=example,dc=com
 pagesize        1000
 referrals       off
 nss_nested_groups yes

 # LDAP bind account (AD account created in earlier)
 binddn cn=nslcd-ad,cn=Users,dc=SAMDOM,dc=example,dc=com
 bindpw ...

 # Filters
 filter  passwd  (objectClass=user)
 filter  group   (objectClass=group)

 # Attribute mappings
 map     passwd  uid                sAMAccountName
 map     passwd  homeDirectory      unixHomeDirectory
 map     passwd  gecos              displayName
 map     passwd  gidNumber          primaryGroupID

* For details about the parameter, see the ``nslcd.conf (5)</ man page.

* To enable LDAP databases for the name service switch (NSS), add the ``ldap</ option to the following lines in the ``/etc/nssw/con/> file:/

 passwd:     files ldap
 group:      files ldap

* Start the ``nslcd</ service.

Authenticating nslcd to AD Using 	Kerberos 
------------------------

To enable the ``nslcd</ service to authenticate to Active Directory (AD) using Kerberos:

* Create a new user in AD. For example: ``nslcd-ad</
* Set the following options in the account's settings:
* * Password never expires
* * User cannot change password, note: this can only be set from Windows

* Extract the Kerberos keytab for the ``nslcd-ad</ account to the ``/etc/krb5/dDD/`` file, /:

 # samba-tool domain exportkeytab /DOO/OTTkeytab --principal=nslcd-ad
 # chown nslcd:root /DOO/OTTkeytab 
 # chmod 600 /DOO/OTTkeytab

* Make sure that the Kerberos ticket is automatically renewed before it expires. For example, to auto-renew Kerberos tickets using the ``k5start</ utility:

 # k5start -f /DOO/OTTkeytab -U -o nslcd -K 360 -b -k /tmp/nslcd.tk//

* For details about the parameters, see the ``k5start (5)</ man page. Make sure that the utility used for renewal is automatically started at boot time.

* Add the following parameter to the ``[global]</ section of your ``smb.conf``/file:

 acl:search = no

* Restart Samba.

* Edit the ``/DDO/;/code> / and set the following settings:

 # Local user account and group, nslcd uses.
 uid nslcd
 gid nslcd

 # Active Directory server settings
 uri             ldap://OTTexample.com//
 base            dc=samdom,dc=example,dc=com
 pagesize        1000
 referrals       off
 nss_nested_groups yes

 # Kerberos authentication to AD
 sasl_mech       GSSAPI
 sasl_realm      SAMDOM.EXAMPLE.COM
 krb5_ccname     /DDO/

 # Filters
 filter  passwd  (objectClass=user)
 filter  group   (objectClass=group)

 # Attribut mappings
 map     passwd  uid                sAMAccountName
 map     passwd  homeDirectory      unixHomeDirectory
 map     passwd  gecos              displayName
 # Uncomment the following line to use Domain Users as the users primary group
 #map     passwd  gidNumber          primaryGroupID

* For details about the parameter, see the ``nslcd.conf (5)</ man page.

* To enable LDAP databases for the name service switch (NSS), add the ``ldap</ option to the following lines in the ``/etc/nssw/con/> file:/

 passwd:     files ldap
 group:      files ldap

Edit the /lt// fi/EEand set the following settings:

 # Defaults for nslcd init script

 # Whether to start k5start (for obtaining and keeping a Kerberos ticket)
 # By default k5start is started if nslcd.conf has sasl_mech set to GSSAPI
 # and krb5_ccname is set to a file-type ticket cache.
 # Set to "yes" to force starting k5start, any other value will not start
 # k5start.
 #K5START_START="yes"

 # Options for k5start.
 #K5START_BIN=/5st//
 K5START_KEYTAB=/SSHHkrb5.nslcd.keytab
 #K5START_CCREFRESH=60
 K5START_PRINCIPAL="nslcd-ad"

* Start the ``nslcd`` service.

=================
Testing the User and Group Retrieval
=================

To list users and groups having Unix attributes in Active Directory (AD) set:

* To list a users account, enter:

 # getent passwd demo
 demo:*:10001:10001:demo1:/home/demo:/bin/bash

* To list a group, enter:

 # getent group demo-group
 demo-group:*:10001:demo1

=================
Troubleshooting
=================

If the ``getent`` command fails to load users and groups from Active Directory (AD):

* Stop the ``nslcd`` service.

* Start the ``nslcd`` service in debug mode:

 # nslcd -d

* The service will start in the foreground and the output is displayed on the screen.

* On a second terminal, run the failed ``getent`` command again and watch the ``nslcd`` debug output.