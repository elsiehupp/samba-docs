Linux Kernel Server
    <namespace>0</namespace>
<last_edited>2021-04-01T13:47:16Z</last_edited>
<last_editor>Namjae Jeon</last_editor>

<Placeholder for documentation on building, testing, developing and using the SMB3 kernel server, fs/cifsd/ksmbd.ko>

Overview 
------------------------

What is SMB ?
-------------
The SMB ("Server Message Block") family of protocols is the most widely deployed network filesystem protocol, the default on Windows and Macs (and even on many phones and tablets), with clients and servers on all major
operating systems, but lacked a kernel server for Linux. For many cases the current userspace server choices were suboptimal either due to memory footprint, performance or difficulty integrating well with advanced Linux features.

What is "ksmbd"?
----------------

ksmbd is a new kernel module which implements the server-side of the SMB3 protocol.
The target is to provide optimized performance, GPLv2 SMB server, better
lease handling (distributed caching). The bigger goal is to add new
features more rapidly (e.g. RDMA aka "smbdirect", and recent encryption
and signing improvements to the protocol) which are easier to develop
on a smaller, more tightly optimized kernel server than for example
in Samba.  The Samba project is much broader in scope (tools, security services,
LDAP, Active Directory Domain Controller, and a cross platform file server
for a wider variety of purposes) but the user space file server portion
of Samba has proved hard to optimize for some Linux workloads, including
for smaller devices. This is not meant to replace Samba, but rather be
an extension to allow better optimizing for Linux, and will continue to
integrate well with Samba user space tools and libraries where appropriate.
Working with the Samba team we have already made sure that the configuration
files and xattrs are in a compatible format between the kernel and
user space server.

Setting up the Kernel Server 
------------------------

   a. download (and build) the kernel server source code:
        git clone -b ksmbd-next git://git.samba.org/sfrench/kernel-smb3

   b. download the corequisite userspace utilities that manage the kernel server module and compile them (see ksmbd-tools/README):
      git clone https://github.com/cifsd-team/ksmbd-tools

   c. Create user/password for SMB share.
        # mkdir /etc/ksmbd/
        # ksmbd.adduser -a <Enter USERNAME for SMB share access>

   d. Create /etc/ksmbd/smb.conf file, add SMB share in smb.conf file
        - Refer to smb.conf.example and Documentation/configuration.txt
          in ksmbd-tools

   e. Insert ksmbd.ko module
        # insmod ksmbd.ko

   f. Start ksmbd user space daemon
        # ksmbd.mountd

   g. Access share from Windows or Linux using SMB
       e.g. "mount -t cifs //server/share /mnt ..."

Feature Status 
------------------------

<TBD - Needs to be updated to be current>

Developer Information 
------------------------

Architecture
------------

                |--- ...
        --------|--- ksmbd/3 - Client 3
        | -------|--- ksmbd/2 - Client 2
        |        |         ____________________________________________________
        |        |        |- Client 1                                          |
 <--- Socket  ---|--- ksmbd/1   <<= Authentication :AASSHHNTLM2, Kerberos      |
        |        |      | |     <<= SMB engine :B2.1, SMB3, SMB3.0.2, |
        |        |      | |                SMB3.1.1                            |
        |        |      | |____________________________________________________|
        |        |      |
        |        |      |--- VFS --- Local Filesystem
        |        |
 KERNEL |--- ksmbd/0(forker kthread)
 ---------------||------------------------

------------------------

---------------
 USER           ||
                || communication using NETLINK
                ||  ______________________________________________
                || |                                              |
         ksmbd.mountd <<= DCE/RPC(srvsvc, wkssvc, samr, lsarpc)   |
                ^  |  <<= configure shares setting, user accounts |
                |  |______________________________________________|
                |
                |------ smb.conf(config file)
                |
                |------ ksmbdpwd.db(user account/password file)
                            ^
    smbd.adduser ------------|

The subset of performance related operations(open/read/write/close etc.) belong
in kernelspace(ksmbd) and the other subset which belong to operations(DCE/RPC,
user account/share database) which are not really related with performance are
handled in userspace(ksmbd.mountd).

When the ksmbd.mountd is started, It starts up a forker thread at initialization
time and opens a dedicated port 445 for listening to SMB requests. Whenever new
clients make request, Forker thread will accept the client connection and fork
a new thread for dedicated communication channel between the client and
the server.

Additional Information 
------------------------

Mailing lists:ASSHHcifsd-devel@lists.sourceforge.net and linux-cifs@vger.kernel.org

Wikipedia articles on CIFSD
* [https://en.wikipedia.org/wiki/CIFSD CIFSD]