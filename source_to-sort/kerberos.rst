Samba & Kerberos
    <namespace>0</namespace>
<last_edited>2007-02-26T03:15:49Z</last_edited>
<last_editor>Jerry</last_editor>

Heimdal or MIT Kerberos?
------------------------

Samba4 will bundle with Heimdal and Samba4's Authentication Developer works closely with Heimdal's primary maintainer.

From the mailing list:
<pre>> The biggest thing users will notice is that the error message system
> returns contextual errors, with the actual reason for the failure, not
> just the translated code.  It often includes the vital clues that help
> fix up the inevitable kerberos issues.  Although what this means in real-world usage, I do not know.  The general consensus I have found seems to be that one is just as good as the other, and you should just use what your linux distro has by default.