Verified Package Dependencies
    <namespace>0</namespace>
<last_edited>2021-03-18T17:40:03Z</last_edited>
<last_editor>Abartlet</last_editor>

Verified Package Dependencies
------------------------

These links are to ``https://git.samba.org`` and are to our tested package installation scripts.  The files are are contained in Samba's official git repository and are subject to Samba's `CodeReview|Code Review` and `Samba_CI_on_gitlab|testing`. 

Naturally, you should still check them before running them as root.  They may contain more packages than you strictly need, because some items required by Samba's `Samba_CI_on_gitlab|GitLab CI` and `autobuild` tooling are not needed in production.
{|
!Distribution
!Samba master
!Samba 4.14
!Samba 4.13
!Samba 4.12
!Samba 4.11
|-
|Debian 9
|
|
|
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/debian9/bootstrap.sh;hb=v4-12-test bootstrap/generated-dists/debian9/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/debian9/bootstrap.sh;hb=v4-11-test bootstrap/generated-dists/debian9/bootstrap.sh]
|-
|Debian 10
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/debian10/bootstrap.sh;hb=master bootstrap/generated-dists/debian10/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/debian10/bootstrap.sh;hb=v4-14-test bootstrap/generated-dists/debian10/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/debian10/bootstrap.sh;hb=v4-13-test bootstrap/generated-dists/debian10/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/debian10/bootstrap.sh;hb=v4-12-test bootstrap/generated-dists/debian9/bootstrap.sh]
|
|-
|Ubuntu 16.04
|
|
|
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/ubuntu1604/bootstrap.sh;hb=v4-12-test bootstrap/generated-dists/ubuntu1604/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/ubuntu1604/bootstrap.sh;hb=v4-11-test bootstrap/generated-dists/ubuntu1604/bootstrap.sh]
|-
|Ubuntu 18.04
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/ubuntu1804/bootstrap.sh;hb=master bootstrap/generated-dists/ubuntu1804/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/ubuntu1804/bootstrap.sh;hb=v4-14-test bootstrap/generated-dists/ubuntu1804/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/ubuntu1804/bootstrap.sh;hb=v4-13-test bootstrap/generated-dists/ubuntu1804/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/ubuntu1804/bootstrap.sh;hb=v4-12-test bootstrap/generated-dists/ubuntu1804/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/ubuntu1804/bootstrap.sh;hb=v4-11-test bootstrap/generated-dists/ubuntu1804/bootstrap.sh]
|-
|Ubuntu 20.04
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/ubuntu2004/bootstrap.sh;hb=master bootstrap/generated-dists/ubuntu2004/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/ubuntu2004/bootstrap.sh;hb=v4-14-test bootstrap/generated-dists/ubuntu2004/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/ubuntu2004/bootstrap.sh;hb=v4-13-test bootstrap/generated-dists/ubuntu2004/bootstrap.sh]
|
|
|-
|OpenSuSE 15.0
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/opensuse150/bootstrap.sh;hb=master bootstrap/generated-dists/opensuse150/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/opensuse150/bootstrap.sh;hb=v4-14-test bootstrap/generated-dists/opensuse150/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/opensuse150/bootstrap.sh;hb=v4-13-test bootstrap/generated-dists/opensuse150/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/opensuse150/bootstrap.sh;hb=v4-12-test bootstrap/generated-dists/opensuse150/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/opensuse150/bootstrap.sh;hb=v4-11-test bootstrap/generated-dists/opensuse150/bootstrap.sh]
|-
|OpenSuSE 15.1
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/opensuse151/bootstrap.sh;hb=master bootstrap/generated-dists/opensuse151/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/opensuse151/bootstrap.sh;hb=v4-14-test bootstrap/generated-dists/opensuse151/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/opensuse151/bootstrap.sh;hb=v4-13-test bootstrap/generated-dists/opensuse151/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/opensuse151/bootstrap.sh;hb=v4-12-test bootstrap/generated-dists/opensuse151/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/opensuse151/bootstrap.sh;hb=v4-11-test bootstrap/generated-dists/opensuse151/bootstrap.sh]
|-
|Fedora 29
|
|
|
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/fedora29/bootstrap.sh;hb=v4-12-test bootstrap/generated-dists/fedora29/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/fedora29/bootstrap.sh;hb=v4-11-test bootstrap/generated-dists/fedora29/bootstrap.sh]
|-
|Fedora 30
|
|
|
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/fedora30/bootstrap.sh;hb=v4-12-test bootstrap/generated-dists/fedora30/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/fedora30/bootstrap.sh;hb=v4-11-test bootstrap/generated-dists/fedora30/bootstrap.sh]
|-
|Fedora 31
|
|
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/fedora31/bootstrap.sh;hb=v4-13-test bootstrap/generated-dists/fedora31/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/fedora31/bootstrap.sh;hb=v4-12-test bootstrap/generated-dists/fedora31/bootstrap.sh]
|
|-
|Fedora 32
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/fedora32/bootstrap.sh;hb=master bootstrap/generated-dists/fedora32/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/fedora32/bootstrap.sh;hb=v4-14-test bootstrap/generated-dists/fedora32/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/fedora32/bootstrap.sh;hb=v4-13-test bootstrap/generated-dists/fedora32/bootstrap.sh]
|
|-
|Fedora 33
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/fedora33/bootstrap.sh;hb=master bootstrap/generated-dists/fedora33/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/fedora33/bootstrap.sh;hb=v4-14-test bootstrap/generated-dists/fedora33/bootstrap.sh]
|
|
|
|-
|CentOS 7
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/centos7/bootstrap.sh;hb=master bootstrap/generated-dists/centos7/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/centos7/bootstrap.sh;hb=v4-14-test bootstrap/generated-dists/centos7/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/centos7/bootstrap.sh;hb=v4-13-test bootstrap/generated-dists/centos7/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/centos7/bootstrap.sh;hb=v4-12-test bootstrap/generated-dists/centos7/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/centos7/bootstrap.sh;hb=v4-11-test bootstrap/generated-dists/centos7/bootstrap.sh]
|-
|CentOS 8
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/centos8/bootstrap.sh;hb=master bootstrap/generated-dists/centos8/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/centos8/bootstrap.sh;hb=v4-14-test bootstrap/generated-dists/centos8/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/centos8/bootstrap.sh;hb=v4-13-test bootstrap/generated-dists/centos8/bootstrap.sh]
|[https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/centos8/bootstrap.sh;hb=v4-12-test bootstrap/generated-dists/centos8/bootstrap.sh]
|}