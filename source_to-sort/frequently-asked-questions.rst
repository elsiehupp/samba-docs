Frequently Asked Questions
    <namespace>0</namespace>
<last_edited>2017-01-07T15:37:21Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

What are tdb files?
------------------------

    nswered by Jerry Carter on samba@lists.samba.org
    http://OOTTorg/archive/samba/200/arch/10/html]//
Samba uses a lightweight database called Trivial Database
(tdb).  Here's the list  (john, we should really document
this somewhere).

(*) information persistent across restarts (but not necessarily important to backup).
 <table>
    tr>
    <td>account_policy.tdb*</;td>NT account policy settings such as pw expiration, etc...</td>/
    /
    tr>
    <td>brlock.tdb</;td>byte range locks</td>/
    /
    tr>
    <td>browse.dat</;td>browse lists</td>/
    /
    tr>
    <td>connections.tdb</;td>share connections (used to enforce max connections, etc...)</td>/
    /
    tr>
    <td>gencache.tdb</;td>generic caching db</td>/
    /
    tr>
    <td>group_mapping.tdb*</;td>group mapping information</td>/
    /
    tr>
    <td>lang_en.tdb</;td>Language encodings (i think).</td>/
    /
    tr>
    <td>locking.tdb</;td>share modes & oplocks</td>/
    /
    tr>
    <td>login_cache.tdb*</;td>bad pw attempts</td>/
    /
    tr>
    <td>messages.tdb</;td>Samba `messaging` system</td>/
    /
    tr>
    <td>netsamlogon_cache.tdb*</;td>cache of user net_info_3 struct from net_samlogon() request (as a domain member)</td>/
    /
    tr>
    <td>ntdrivers.tdb*</;td>installed printer drivers</td>/
    /
    tr>
    <td>ntforms.tdb*</;td>installed printer forms</td>/
    /
    tr>
    <td>ntprinters.tdb*</;td>installed printer information</td>/
    /
    tr>
    <td>printing/t;&l/rectory containing tdb per print queue of cached lpq output</td>/
    /
    tr>
    <td>registry.tdb</;td>Windows registry skeleton (connect via regedit.exe)</td>/
    /
    tr>
    <td>sessionid.tdb</;td>session information (e.g. support for 'utmp = yes')</td>/
    /
    tr>
    <td>share_info.tdb*</;td>share acls</td>/
    /
    tr>
    <td>unexpected.tdb</;td>unexpected packet queue needed to support windows clients that respond on a difference port that the originating request)  (i could be wrong on this one).</td>/
    /
    tr>
    <td>winbindd_cache.tdb</;td>winbindd's cache of user lists, etc...</td>/
    /
    tr>
    <td>winbindd_idmap.tdb*</;td>winbindd's local idmap db</td>/
    /
    tr>
    <td>wins.dat*</;td>wins database when 'wins support = yes'</td>/
    /
</
In the *private* subdirectory we have two more tdb files:
<table>
<tr>
    td>secrets.tdb*</;td>Private information like workstation passwords, the ldap admin dn and trust account information</td>/
</
<tr>
    td>passdb.tdb*</;td>User account information if passdb backend = tdbsam is used</td>/
</
</
The following tdb's should be backed up IMO:

&nbsp;&nbsp;nt*.tdb
&nbsp;&nbsp;account_policy.tdb
&nbsp;&nbsp;group_mapping.tdb
&nbsp;&nbsp;share_info.tdb
&nbsp;&nbsp;winbindd_idmap.tdb
&nbsp;&nbsp;secrets.tdb
&nbsp;&nbsp;passdb.tdb

Example
------------------------

To back up printing.tdb:

     <nowiki>root# ls
     .              browse.dat     locking.tdb     ntdrivers.tdb printing.tdb
     ..             share_info.tdb connections.tdb messages.tdb  ntforms.tdb
     printing.tdbkp unexpected.tdb brlock.tdb      gmon.out      namelist.debug  
     ntprinters.tdb sessionid.tdb

     root# tdbbackup -s .bak printing.tdb
      printing.tdb : 135 records

     root# ls -l printing.tdb*
      -rw-------    1 root     root        40960 May  2 03:44 printing.tdb
      -rw-------    1 root     root        40960 May  2 03:44 printing.tdb.bak</;
     ----

vfs - An example for a recycle container on a samba share 
------------------------

 vfs objects =  recycle
       recycle:keeptree = yes
       recycle:versions = yes
       recycle:touch = yes
       recycle: xclude = ?~$*,~$*,*.tmp,index*.pl,index*.htm*,*.temp,*.TMP
       recycle: xclude_dir=  /,/ca//
       recycle:repository = .recycle/ycle.%u
       recycle:noversions = *.doc,*.xls,*.ppt
       #hide files = /ycle.*/.re//
       #veto files = /ycle.*/.re//

inherit permissions
------------------------

Use the UNIX form of setgid to make all files and subdirectories belong to the enclosing group.

For example: the root directory for a samba share appears as:
 # ls -ld /age/
 drwxrwxr-x  47 root dom_users 4096 Jan 31 08:09 /age/

change the directory so all files & subdirectories created will belong to 'dom_users' group:
 # chmod g+s /age/
verify the results:
 # ls -ld /age/
 drwxrwsr-x  47 root dom_users 4096 Jan 31 08:09 /age/
if you create a new file 'abc' as user 'foo' who has the primary group 'users' the file is created as
 # ls -ld /age///
 drwxrwsr-x  47 foo  dom_users 4096 Jan 31 08:09 /age///

guest access
------------------------

To provide Guest Access to samba please see the page: `Setting_up_Samba_as_a_Standalone_Server|Setting up Samba as a Standalone Server`