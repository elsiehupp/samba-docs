Linux SMB helper
    <namespace>0</namespace>
<last_edited>2019-02-14T15:50:27Z</last_edited>
<last_editor>Aaptel</last_editor>

`Category:CIFSclient`

There are too many knobs in different places at the moment: request-keys, idmap, cifscreds, /proc stuff. This goal of this project would be to write one CLI tool that would wrap everything under a common interface. It could handle getting/setting ACL as well. Ideally this would replace and unify all the little tools in cifs-utils.

=================
Implementation
=================

In order of preference:
* Python + a C python module for the syscalls and other ioctl which would not be so practical from python.
* Alternatively, all in C++. I'm just tired of plain C for non-critical stuff
* C + glib maybe.
* Go/Rust: interesting but will need to be learned

=================
smbmount -- unified utility to manage linux smb mounts
=================

Mounting 
------------------------

.. code-block::

    # smbmount //UNC /path [opts]
# or
# smbmount mount //UNC /path [opts]

Configuration 
------------------------

list things under "config"

<pre># smbmount config</pre>

Debug
------------------------

<pre>smbmount config debug [on|off]</pre>

* make cifs.ko verbose/quiet
* do something with ftrace?

.. code-block::

       echo 'module cifs +p' > /sys/kernel/debug/dynamic_debug/control
   echo 'file fs/cifs/* +p' > /sys/kernel/debug/dynamic_debug/control
   echo 1 > /proc/fs/cifs/cifsFYI
   echo 1 > /sys/module/dns_resolver/parameters/debug

Trace
------------------------

<pre># smbmount config trace [on|off|???]</pre>
* get/set traceSMB
* should this be in "config debug"?

DNS
------------------------

.. code-block::

    # smbmount config dns
# smbmount config dns cache
* list entries

<pre># smbmount config dns cache clear</pre>
* clear cache

<pre># smbmount config dns cache expire [time]</pre>
* get/set expire time

DFS
------------------------

.. code-block::

    # smbmount config dfs
# smbmount config dfs cache
* list entries

.. code-block::

    # smbmount config dfs clear
* clear cached entries

Credentials
------------------------

XXX: is "auth" better?

.. code-block::

    # smbmount config credentials
* allow to dump/edit in-kernel credentials
* TODO: this might require kernel changes
* we could entirely use the keyctl system for creds
* usecase: password expire or changes and you want to let cifs.ko reconnect without unmount/remounting

<pre># smbmount config kerberos</pre>
* kerberos things
* TODO: what exactly? figure out what this should include. tickets have to be renewed? how? etc
* XXX: should this go under "credentials"?

SMB-specific file opts 
------------------------

We would have a verb per operation

.. code-block::

    # smbmount sid
# smbmount fileinfo
# smbmount acl
# smbmount streams
# smbmount setattr
# smbmount ...
* TODO: complete

Global information 
------------------------

<pre># smbmount show</pre>
* list things under "show"

<pre># smbmount show connection [-v] [-f filters?]</pre>
* list tcp con, sess, tcon (read DebugData)

<pre># smbmount show mount [-v]</pre>
* list smb mount points with explained mount options

<pre># smbmount show stats [???]</pre>
* compile and list various stats from /proc/fs/cifs