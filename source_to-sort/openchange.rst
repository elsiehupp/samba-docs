OpenChange
    <namespace>0</namespace>
<last_edited>2018-01-25T01:37:45Z</last_edited>
<last_editor>Abartlet</last_editor>

==============================

penChange
===============================

OpenChange is a dormant open source project to implement the [https://en.wikipedia.org/wiki/MAPI MAPI protocol] based heavily around Samba, particularly the early Samba4 branch of the project. 

The primary target client was [https://en.wikipedia.org/wiki/Microsoft_Outlook Microsoft Outlook] which contacted the [https://en.wikipedia.org/wiki/Microsoft_Exchange_Server Microsoft Exchange] server over `DCERPC|DCE/RPC`.  While the MAPI protocol is not NDR based, our `PIDL| PIDL code generation tool` was used extensively both for MAPI and the Exchange RPC layer it is transported over. 

This lead to a clear connection with the Samba project, the tools and infrastructure of Samba were very attractive to the young OpenChange project, and indeed early versions of the project started as a friendly fork of Samba.  

Close co-operation
------------------------

The OpenChange team, particularly early in the development of the project was in close cooperation with the Samba Team.  Samba developers were also OpenChange developers and OpenChange was warmly received at [https://sambaxp.org SambaXP].  Indeed some of the very earliest development was with Samba Team members and Julien after-hours at SambaXP.

===============================
Strong Samba dependencies
===============================

OpenChange uses APIs provided by Samba that are not stable.  Changes to these APIs have regularly broken the OpenChange build.  Because OpenChange nor a test of the components it uses are not part of Samba, this tended to be noticed only long after a Samba release had been made.

===============================
Current Status
===============================

OpenChange is a great proof of concept, but it is not ready for production use.  [http://www.zentyal.org/ Zentyal], the main contributor to the project in the past few years, has stopped development and removed OpenChange component from their product.

The primary website openchange.org has been dead since 2016.  The [https://github.com/openchange/openchange OpenChange github] has not been updated since 2015.

===============================
A different problem space
===============================

While Samba and OpenChange share some significant technologies the problem space and goals of the two projects is quite distinct.

Even so, why can't Samba just pick it up?
------------------------

It might be possible for the Samba project to act as some kind of umbrella for the OpenChange project, but that still requires that there is an active development team for OpenChange.  

As the project is not in a production ready state it is not useful for the Samba Team to provide even a maintenance service over the codebase.

A [https://lists.samba.org/archive/samba-technical/2018-January/125183.html recent discussion thread] on samba-technical provides some more background.