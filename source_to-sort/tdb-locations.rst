TDB Locations
    <namespace>0</namespace>
<last_edited>2013-03-28T09:42:28Z</last_edited>
<last_editor>BBaumbach</last_editor>

List of Samba3 TDB files and their locations.

{| class="sortable wikitable" border="1" cellpadding="5" cellspacing="0"
|-style="background-color:#DCDCDC;"
!TDB File             || API      || Location|| Info                || Description
|-
|account_policy.tdb   || dbwrap   || state   ||                     || Samba/NT account policy settings, includes password expiration settings.
|-
|autorid.tdb          || dbwrap   || state   ||                     || Mappings of which domain is mapped to which range. 
|-
|brlock.tdb           || dbwrap   || lock    || TDB_CLEAR_IF_FIRST  || Byte-range locking information.
|-
|connections.tdb      || dbwrap   || lock    || TDB_CLEAR_IF_FIRST  || A temporary cache for current connection information used to enforce max connections.
|-
|eventlog/*.tdb       || tdb      || state   ||                     || Records of eventlog entries. In most circumstances this is just a cache of system logs.
|-
|g_lock.tdb           || dbwrap   || lock    || TDB_CLEAR_IF_FIRST  || Global locking information.
|-
|gencache.tdb         || tdb      || lock    || TDB_CLEAR_IF_FIRST* || Generic caching database for dead WINS servers and trusted domain data.
|-
|gencache_notrans.tdb || tdb      || lock    || TDB_CLEAR_IF_FIRST  || 
|-
|group_mapping.tdb    || dbwrap   || state   ||                     || Mapping table from Windows groups/SID to UNIX groups.
|-
|idmap2.tdb           || dbwrap   || private ||                     ||
|-
|locking.tdb          || dbwrap   || lock    || TDB_CLEAR_IF_FIRST  ||
|-
|login_cache.tdb      || tdb      || cache   ||                     || A temporary cache for login information, in particular bad password attempts.
|-
|messages.tdb         || tdb_wrap || lock    || TDB_CLEAR_IF_FIRST  || Temporary storage of messages being processed by smbd.
|-
|mutex.tdb            || tdb_wrap || lock    ||                     ||
|-
|netsamlogon_cache.tdb|| tdb      || cache   || TDB_CLEAR_IF_FIRST* || Caches user net_info_3 structure data from net_samlogon requests (as a domain member).
|-
|notify.tdb           || tdb_wrap || lock    || TDB_CLEAR_IF_FIRST  ||
|-
|notify_onelevel.tdb  || dbwrap   || lock    || TDB_CLEAR_IF_FIRST  ||
|-
|ntdrivers.tdb        || tdb      || state   ||                     || Removed in 3.6. Stores per-printer installed driver information.
|-
|ntforms.tdb          || tdb      || state   ||                     || Removed in 3.6. Stores per-printer installed forms information.
|-
|ntprinters.tdb       || tdb      || state   ||                     || Removed in 3.6. Stores the per-printer devmode configuration settings.
|-
|passdb.tdb           || dbwrap   || private ||                     || Exists only when the tdbsam passwd backend is used. This file stores the SambaSAMAccount information. Note: This file requires that user POSIX account information is available from either the /etc/passwd file, or from an alternative system source. 
|-
|perfmon/data.tdb     || tdb      || state   ||                      || Performance counter information.
|-
|perfmon/names.tdb    || tdb      || state   ||                      || Performance counter information.
|-
|printer_list.tdb     || dbwrap   || lock    || TDB_CLEAR_IF_FIRST   ||
|-
|printing/*.tdb       || tdb      || cache   ||        || Cached output from lpq command created on a per-print-service basis.
|-
|registry.tdb         || dbwrap   || state   ||        || Read-only Samba database of a Windows registry skeleton that provides support for exporting various database tables via the winreg RPCs. 
|-
|schannel_store.tdb   || tdb_wrap || private || TDB_CLEAR_IF_FIRST       || A confidential file, stored in the PRIVATE_DIR, containing crytographic connection information so that clients that have temporarily disconnected can reconnect without needing to renegotiate the connection setup process. 
|-
|secrets.tdb          || dbwrap   || private ||        || This file stores the Workgroup/Domain/Machine SID, the LDAP directory update password, and a further collection of critical environmental data that is necessary for Samba to operate correctly. This file contains very sensitive information that must be protected. It is stored in the PRIVATE_DIR directory. 
|-
|serverid.tdb         || tdb_wrap || lock    || TDB_CLEAR_IF_FIRST       ||
|-
|sessionid.tdb        || dbwrap   || lock    || TDB_CLEAR_IF_FIRST       || Temporary cache for miscellaneous session information and for utmp handling.
|-
|share_info.tdb       || dbwrap   || state   ||        || Stores per-share ACL information.
|-
|unexpected.tdb       || tdb      || lock    || TDB_CLEAR_IF_FIRST || Removed in 3.6. Stores packets received for which no process is actively listening.
|-
|winbindd_cache.tdb   || tdb      || cache   || TDB_CLEAR_IF_FIRST*       || Cache of Identity information received from an NT4 domain or from ADS. Includes user lists, etc.
|-
|winbindd_idmap.tdb   || tdb      || state   ||        || Winbindd's local IDMAP database.
|-
|wins.tdb             || tdb      || state   || TDB_CLEAR_IF_FIRST       ||
|-
|xattr.tdb            || dbwrap   || state   ||        ||
|-
|                     ||          ||         ||       ||
|-
|torture.tdb          || tdb      || torture || Test TDB       ||
|-
|test.tdb             || tdb_wrap || torture || Test TDB       ||
|-
|transtest.tdb        || dbwrap   || torture || Test TDB       ||
|}

 TDB_CLEAR_IF_FIRST means that Samba clears the TDB on each first open (for example after a reboot).
 * will be only cleaned (truncated) if file is corrupt