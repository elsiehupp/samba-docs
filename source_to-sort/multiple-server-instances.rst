Multiple Server Instances
    <namespace>0</namespace>
<last_edited>2012-05-22T20:08:49Z</last_edited>
<last_editor>Mc-sim</last_editor>

**Multiple Server Hosting**

The use of multiple server hosting involves running multiple separate instances of Samba, each with it's own configuration file. This method is complicated by the fact that each instance of nmbd, smbd and winbindd must have write access to entirely separate TDB files. The ability to keep separate the TDB files used by nmbd, smbd and winbindd can be enabled either by recompiling Samba for each server hosted so each has its own default TDB directories, or by configuring these in the smb.conf file, in which case each instance of nmbd, smbd and winbindd must be told to start up with its own smb.conf configuration file.

Each instance should operate on its own IP address (that independent IP address can be an IP Alias). Each instance of nmbd, smbd and winbindd should listen only on its own IP socket. This can be secured using the socket address parameter. Each instance of the Samba server will have its own SID also, this means that the servers are discrete and independent of each other.

As an alternative, you can setup Samba to run within [http://www.cl.cam.ac.uk/Research/SRG/netos/xen/ Xen]. Some Samba administrators may find it easier to set up, especially if you are going to set up more Samba and OpenLDAP instances on the same box.

Conventions 
------------------------

* this document will use the configuration file method
* this document is RHEL centric for the interfaces and file locations
* the domain will be kansai
* the instances will be named nara and heian
* nara will listen on eth0:1
* heian will listen on eth0:2

Preparation 
------------------------

To have multiple instances of samba running, you will need to setup at least one virtual interface per samba instance. We will setup two persistant virtual interfaces right now.

 cd /etc/sysconfig/network-scripts
 cat << EOF > ifcfg-eth0:1
 DEVICE=eth0:1
 BOOTPROTO=static
 BROADCAST=192.168.255.255
 HWADDR=00:08:02:91:2C:58
 IPADDR=192.168.127.1
 NETMASK=255.255.0.0
 NETWORK=192.168.0.0
 ONBOOT=yes
 TYPE=Ethernet
 EOF
 cat << EOF > ifcfg-eth0:2
 DEVICE=eth0:2
 BOOTPROTO=static
 BROADCAST=192.168.255.255
 HWADDR=00:08:02:91:2C:58
 IPADDR=192.168.127.2
 NETMASK=255.255.0.0
 NETWORK=192.168.0.0
 ONBOOT=yes
 TYPE=Ethernet
 EOF

 ifup eth0:1 
 ifup eth0:2

 cat << EOF >> /etc/hosts
 192.168.127.1		nara.kansai	nara
 192.168.127.2		heian.kansai	heian
 EOF

You will also need one separate set of directories per instance for the samba private files (pids, databases, ...) and shares:

 mkdir -p /var/run/samba/nara /var/run/samba/heian
 mkdir -p /var/cache/samba/nara /var/cache/samba/heian
 mkdir -p /var/log/samba/nara /var/log/samba/heian
 mkdir -p /shares/nara/todaiji /shares/heian/jingu

Don't forget to update logrotate to backup your new log directories (**NOTE:** if you copy/paste this into your own editor instead of using cat, make sure to remove the backslashes):

 cd /etc/logrotate.d
 cat << EOF > samba-nara
 /var/log/samba/nara/*.log {
    notifempty
    missingok
    sharedscripts
    copytruncate
    postrotate
        /bin/kill -HUP \`cat /var/run/samba/nara/smbd.pid /var/run/samba/nara/nmbd.pid /var/run/samba/nara/winbindd.pid 2> /dev/null\` 2> /dev/null || true
    endscript
 }
 EOF
 cat << EOF > samba-heian
 /var/log/samba/heian/*.log {
    notifempty
    missingok
    sharedscripts
    copytruncate
    postrotate
        /bin/kill -HUP \`cat /var/run/samba/heian/smbd.pid /var/run/samba/heian/nmbd.pid /var/run/samba/heian/winbindd.pid 2> /dev/null\` 2> /dev/null || true
    endscript
 }
 EOF

Configuration files 
------------------------

Create two configuration files: /etc/samba/smb.conf.nara and /etc/samba/smb.conf.heian

 cd /etc/samba

 cat <<EOF >smb.conf.nara
 [global]
         add share command          = /usr/bin/modify_samba_config.pl
         delete share command       = /usr/bin/modify_samba_config.pl
         change share command       = /usr/bin/modify_samba_config.pl
         workgroup                  = kansai
         security                   = user
         bind interfaces only       = yes
         interfaces                 = eth0:1
         netbios name               = nara
         pid directory              = /var/run/samba/nara
         lock directory             = /var/cache/samba/nara
         private dir                = /var/cache/samba/nara

 [todaiji]
         path = /shares/nara/todaiji
 EOF

 cat <<EOF >smb.conf.heian
 [global]
         add share command          = /usr/bin/modify_samba_config.pl
         delete share command       = /usr/bin/modify_samba_config.pl
         change share command       = /usr/bin/modify_samba_config.pl
         workgroup                  = kansai
         security                   = user
         bind interfaces only       = yes
         interfaces                 = eth0:2
         netbios name               = heian
         pid directory              = /var/run/samba/heian
         lock directory             = /var/cache/samba/heian
         private dir                = /var/cache/samba/heian

 [jingu]
         path = /shares/heian/jingu
 EOF

In /etc/sysconfig, create two files named samba.nara and samba.heian

 cd /etc/sysconfig

 cat <<EOF >samba.nara
 # Options to smbd
 SMBDOPTIONS="-D -s /etc/samba/smb.conf.nara -l /var/log/samba/nara"
 # Options to nmbd
 NMBDOPTIONS="-D -s /etc/samba/smb.conf.nara -l /var/log/samba/nara"
 # Options for winbindd
 WINBINDOPTIONS=""
 EOF

 cat <<EOF >samba.heian
 # Options to smbd
 SMBDOPTIONS="-D -s /etc/samba/smb.conf.heian -l /var/log/samba/heian"
 # Options to nmbd
 NMBDOPTIONS="-D -s /etc/samba/smb.conf.heian -l /var/log/samba/heian"
 # Options for winbindd
 WINBINDOPTIONS=""
 EOF

Startup scripts 
------------------------

The default RHEL startup scripts don't handle multiple processes, so we will use modified versions instead.  All of the changes have to do with explicitly specifying the pidfiles, instead of the functions guessing the PIDs.

 cat <<EOF > smb.nara
 #!/bin/sh
 #
 # chkconfig: - 91 35
 # description: Starts and stops the Samba smbd and nmbd daemons \
 #              used to provide SMB network services.
 #
 # pidfile: /var/run/samba/nara/smbd.pid
 # pidfile: /var/run/samba/nara/nmbd.pid
 # config:  /etc/samba/smb.conf.nara

 smb_pidfile="/var/run/samba/nara/smbd.pid"
 nmb_pidfile="/var/run/samba/nara/nmbd.pid"

 # Source function library.
 if [ -f /etc/init.d/functions ] ; then
   . /etc/init.d/functions
 elif [ -f /etc/rc.d/init.d/functions ] ; then
   . /etc/rc.d/init.d/functions
 else
   exit 0
 fi

 # Avoid using root's TMPDIR
 unset TMPDIR
     # Source networking configuration.
 . /etc/sysconfig/network

 if [ -f /etc/sysconfig/samba.nara ]; then
    . /etc/sysconfig/samba.nara
 fi

 # Check that networking is up.
 [ ${NETWORKING} = "no" ] && exit 0

 # Check that smb.conf exists.
 [ -f /etc/samba/smb.conf.nara ] || exit 0

 # Check that we can write to it... so non-root users stop here
 [ -w /etc/samba/smb.conf.nara ] || exit 0

 RETVAL=0

 start() {
         KIND="SMB"
         echo -n $"Starting $KIND services: "
         daemon --pidfile $smb_pidfile smbd $SMBDOPTIONS
         RETVAL=$?
         echo
         KIND="NMB"
         echo -n $"Starting $KIND services: "
         daemon --pidfile $nmb_pidfile nmbd $NMBDOPTIONS
         RETVAL2=$?
         echo
         [ $RETVAL -eq 0 -a $RETVAL2 -eq 0 ] && touch /var/lock/subsys/smb.nara || \
           RETVAL=1
         return $RETVAL
 }

 stop() {
         KIND="SMB"
         echo -n $"Shutting down $KIND services: "
         killproc -p $smb_pidfile smbd
         RETVAL=$?
         echo
         KIND="NMB"
         echo -n $"Shutting down $KIND services: "
         killproc -p $nmb_pidfile nmbd
         RETVAL2=$?
         [ $RETVAL -eq 0 -a $RETVAL2 -eq 0 ] && rm -f /var/lock/subsys/smb.nara
         echo ""
         return $RETVAL
 }

 restart() {
         stop
         start
 }

 reload() {
         echo -n $"Reloading smb.conf file: "
         killproc -p $smb_pidfile smbd -HUP
         RETVAL=$?
         echo
         return $RETVAL
 } 

 rhstatus() {
         status -p $smb_pidfile smbd
         status -p $nmb_pidfile nmbd
 }

 case "$1" in
   start)
         start
         ;;
   stop)
         stop
         ;;
   restart)
         restart
         ;;
   reload)
         reload
         ;;
   status)
         rhstatus
         ;;
   condrestart)
         [ -f /var/lock/subsys/smb.nara ] && restart || :
         ;;
   *)
         echo $"Usage: $0 {start|stop|restart|reload|status|condrestart}"
         exit 1
 esac

 exit $?
 EOF

 cat <<EOF >smb.heian
 #!/bin/sh
 #
 # chkconfig: - 91 35
 # description: Starts and stops the Samba smbd and nmbd daemons \
 #              used to provide SMB network services.
 #
 # pidfile: /var/run/samba/heian/smbd.pid
 # pidfile: /var/run/samba/heian/nmbd.pid
 # config:  /etc/samba/smb.conf.heian

 smb_pidfile="/var/run/samba/heian/smbd.pid"
 nmb_pidfile="/var/run/samba/heian/nmbd.pid"

 # Source function library.
 if [ -f /etc/init.d/functions ] ; then
   . /etc/init.d/functions
 elif [ -f /etc/rc.d/init.d/functions ] ; then
   . /etc/rc.d/init.d/functions
 else
   exit 0
 fi

 # Avoid using root's TMPDIR
 unset TMPDIR

 # Source networking configuration.
 . /etc/sysconfig/network

 if [ -f /etc/sysconfig/samba.heian ]; then
    . /etc/sysconfig/samba.heian
 fi

 # Check that networking is up.
 [ ${NETWORKING} = "no" ] && exit 0

 # Check that smb.conf exists.
 [ -f /etc/samba/smb.conf.heian ] || exit 0

 # Check that we can write to it... so non-root users stop here
 [ -w /etc/samba/smb.conf.heian ] || exit 0

 RETVAL=0

 start() {
         KIND="SMB"
         echo -n $"Starting $KIND services: "
         daemon --pidfile $smb_pidfile smbd $SMBDOPTIONS
         RETVAL=$?
         echo
         KIND="NMB"
         echo -n $"Starting $KIND services: "
         daemon --pidfile $nmb_pidfile nmbd $NMBDOPTIONS
         RETVAL2=$?
         echo
         [ $RETVAL -eq 0 -a $RETVAL2 -eq 0 ] && touch /var/lock/subsys/smb || \
            RETVAL=1
         return $RETVAL
 }

 stop() {
         KIND="SMB"
         echo -n $"Shutting down $KIND services: "
         killproc -p $smb_pidfile smbd
         RETVAL=$?
         echo
         KIND="NMB"
         echo -n $"Shutting down $KIND services: "
         killproc -p $nmb_pidfile nmbd
         RETVAL2=$?
         [ $RETVAL -eq 0 -a $RETVAL2 -eq 0 ] && rm -f /var/lock/subsys/smb.heian
         echo ""
         return $RETVAL
 }

 restart() {
         stop
         start
 }

 reload() {
         echo -n $"Reloading smb.conf file: "
         killproc -p $smb_pidfile smbd -HUP
         RETVAL=$?
         echo
         return $RETVAL
 }

 rhstatus() {
         status -p $smb_pidfile smbd
         status -p $nmb_pidfile nmbd
 }

 case "$1" in
   start)
         start
         ;;
   stop)
         stop
         ;;
   restart)
         restart
        ;;
   reload)
         reload
         ;;
   status)
         rhstatus
         ;;
   condrestart)
         [ -f /var/lock/subsys/smb.heian ] && restart || :
         ;;
   *)
         echo $"Usage: $0 {start|stop|restart|reload|status|condrestart}"
         exit 1
 esac

 exit $?
 EOF

Testing 
------------------------

stop and disable the default samba, start the instances

 root@naniwa: # service smb stop
 Shutting down smb:                                         [  OK  ]

 root@naniwa: # chkconfig smb off 

 root@naniwa: # chkconfig --list smb
 smb             0:off   1:off   2:off   3:off    4:off   5:off   6:off

 root@naniwa: # service smb.nara start
 Starting SMB:                                              [  OK  ]
 Starting NMB:                                              [  OK  ]

 root@naniwa: # service smb.heian start
 Starting SMB:                                              [  OK  ]
 Starting NMB:                                              [  OK  ]

enumerate the shares on each instance

 root@naniwa: # smbclient -L nara -k
 Domain=[kansai] OS=[Unix] Server=[Samba 3.0.9-1.3E.5]

        Sharename       Type      Comment
        ---------       ----      -------
        todaiji       	Disk
        IPC$            IPC       IPC Service (Samba 3.0.9-1.3E.5)
        ADMIN$          IPC       IPC Service (Samba 3.0.9-1.3E.5)
 Domain=[kansai] OS=[Unix] Server=[Samba 3.0.9-1.3E.5]

        Server               Comment
        ---------            -------
        nara                 Samba 3.0.9-1.3E.5

        Workgroup            Master
        ---------            -------
        kansai

 root@naniwa: # smbclient -L heian -k
 Domain=[kansai] OS=[Unix] Server=[Samba 3.0.9-1.3E.5]

        Sharename       Type      Comment
        ---------       ----      -------
        jingu       	Disk
        IPC$            IPC       IPC Service (Samba 3.0.9-1.3E.5)
        ADMIN$          IPC       IPC Service (Samba 3.0.9-1.3E.5)
 Domain=[kansai] OS=[Unix] Server=[Samba 3.0.9-1.3E.5]

        Server               Comment
        ---------            -------
        heian                Samba 3.0.9-1.3E.5
            Workgroup            Master
        ---------            -------
        kansai

Play with instances and test connectivity

 root@naniwa: # service smb.nara stop
 Shutting down SMB services:
 Shutting down NMB services:

 root@naniwa: # smbclient -L nara -k
 Error connecting to 192.168.127.1 (Connection refused)
 Connection to nara failed

 root@naniwa: # smbclient -L heian -k
 Domain=[kansai] OS=[Unix] Server=[Samba 3.0.9-1.3E.5]

        Sharename       Type      Comment
        ---------       ----      -------
        jingu       	Disk
        IPC$            IPC       IPC Service (Samba 3.0.9-1.3E.5)
        ADMIN$          IPC       IPC Service (Samba 3.0.9-1.3E.5)
 Domain=[kansai] OS=[Unix] Server=[Samba 3.0.9-1.3E.5]

        Server               Comment
        ---------            -------
        heian                Samba 3.0.9-1.3E.5

        Workgroup            Master
        ---------            -------
        kansai               

 root@naniwa: # service smb.nara start
 Starting SMB services:                                     [  OK  ]
 Starting NMB services:                                     [  OK  ]
     root@naniwa: # service smb.heian stop
 Shutting down SMB services:
 Shutting down NMB services:

 root@naniwa: # smbclient -L nara -U%
 Domain=[kansai] OS=[Unix] Server=[Samba 3.0.9-1.3E.5]

        Sharename       Type      Comment
        ---------       ----      -------
        todaiji       	Disk
        IPC$            IPC       IPC Service (Samba 3.0.9-1.3E.5)
        ADMIN$          IPC       IPC Service (Samba 3.0.9-1.3E.5)
 Domain=[kansai] OS=[Unix] Server=[Samba 3.0.9-1.3E.5]

        Server               Comment
        ---------            -------
        nara                 Samba 3.0.9-1.3E.5

        Workgroup            Master
        ---------            -------
        kansai

 root@naniwa: # smbclient -L heian -U%
 Error connecting to 192.168.127.2 (Connection refused)
 Connection to heian failed
 root@naniwa: # service smb.heian start
 root@naniwa: # smbclient -L heian -k
 Domain=[kansai] OS=[Unix] Server=[Samba 3.0.9-1.3E.5]

        Sharename       Type      Comment
        ---------       ----      -------
        todaiji       	Disk
        IPC$            IPC       IPC Service (Samba 3.0.9-1.3E.5)
        ADMIN$          IPC       IPC Service (Samba 3.0.9-1.3E.5)
 Domain=[kansai] OS=[Unix] Server=[Samba 3.0.9-1.3E.5]

        Server               Comment
        ---------            -------
        heian                Samba 3.0.9-1.3E.5

        Workgroup            Master
        ---------            -------
        kansai                   

Wrapup 
------------------------

As you can see, you now have two totally independent instances of samba running on the same host.

References 
------------------------

* [http://www.k-max.name/windows/howto-samba-na-2-interfejsa-i-2-seti-s-raznymi-smb-conf/ Multiple Server Instances Samba on Debian 6 in Russian.]

`User:Mpetre|Mpetre` 13:40, 14 March 2006 (CST)