Samba KDC Settings
    <namespace>0</namespace>
<last_edited>2014-01-31T12:42:19Z</last_edited>
<last_editor>Damien.dye</last_editor>

Samba4 DC Kerberos token settings 
------------------------

Samba defaults kerberos tickets expiry values to the following

in some environments it might not be practical that the user TGT expire after 10 hours.

Samba 4's KDC ticket life can be controlled using the parameters in smb.conf
the values take integer values and the values should be the hours the tickets should be valid.

 kdc: ticket lifetime = 1

 kdc ticket lifetime = 24

 kdc: lifetime = 120

in the above example the service tickets are valid for 1 hour before the samba has to reissue them

user TGT tickets are valid for 24 hours before needing them to be renewed.

tickets can be renewed for a maximum of 5 days from the date of original issue.