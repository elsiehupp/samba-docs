BIND9 FLATFILE DNS Back End
    <namespace>0</namespace>
<last_edited>2016-10-12T01:01:01Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

.. warning:

   Do not use the ``BIND9_FLATFILE`` DNS back end. It is not supported and will be removed in the future.
