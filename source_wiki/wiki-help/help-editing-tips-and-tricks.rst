Help: diting tips and tricks
    <namespace>Help</namespace>
<last_edited>2006-03-03T04:55:26Z</last_edited>
<last_editor>Whitecraig</last_editor>

Tips and tricks 
------------------------

===============================
Page protection
===============================

------------------------

In a few cases, where an administrator has `Help:Administration#Page_protection|protected` a page, the link labeled "{{MediaWiki: ditthispage}}" is replaced by the text "{{MediaWiki:Viewsource}}" (or equivalents in the language of the project). In that case the page cannot be edited. Protection of an image page includes protection of the image itself.

===============================
Edit conflicts
===============================

------------------------

If someone else makes an edit while you are making yours, the result is an `Help: dit conflict|edit conflict`. Many conflicts can be automatically resolved by the Wiki. If it can't be resolved, however, you will need to resolve it yourself. The Wiki gives you two text boxes, where the top one is the other person's edit and the bottom one is your edit. Merge your edits into the top edit box, which is the only one that will be saved.

===============================
Reverting
===============================

------------------------

The edit link of a page showing an old version leads to an edit page with the old wikitext. This is a useful way to `Help:Reverting a page to an earlier version|restore the old version` of a page. However, the edit link of a `Help:Diff|diff` page gives the current wikitext, even if the diff page shows an old version below the table of differences.

===============================
Error messages
===============================

------------------------

If you get an error message upon saving a page, you can't tell whether the actual save has failed or just the confirmation. You can go back and save again, and the second save will have no effect, or you can check "My contributions" to see whether the edit went through.

===============================
Checking spelling and editing in your favorite editor
===============================

------------------------

You may find it more convenient to copy and paste the text first into your favorite `w:text editor|text editor`, edit and spell check it there, and then paste it back into your `w:web browser|web browser` to preview. This way, you can also keep a local backup copy of the pages you have edited. It also allows you to make changes offline. 

If you edit this way, it's best to leave the editing page open after you copy from it, using the same edit box to submit your changes, so that the usual edit conflict mechanism can deal with it. If you return to the editing page later, please make sure that nobody else has edited the page in the meantime. If someone has, you'll need to merge their edits into yours by using the `Help:diff|diff` feature in the page history. 

===============================
Composition of the edit page
===============================

------------------------

The editing page consists of these sections:

* The `Help: dit toolbar|edit toolbar` (optional)
* The editing text box
* The `Help: dit summary|edit summary` box
* Save/Preview/Cancel links
* A list of `Help:Template|template`s used on the page
* A preview, if you have requested one. Your preferences may place the preview at the top of the page instead.

===============================
Position-independent wikitext
===============================

------------------------

No matter where you put these things in the wikitext, the resulting page is displayed the same way:
*`Help:Interwiki linking|Interlanguage link`s
*`Help:Category|Categories`
*The `Help:Magic words|magic words` <nowiki>__NOTOC__ and __FORCETOC__</nowiki>. See `Help:Section`.