Help:Wikitext quick reference
    <namespace>Help</namespace>
<last_edited>2006-09-15T11:51:31Z</last_edited>
<last_editor>ShadowKnight</last_editor>

Basic text formatting
------------------------

{| border="1" cellpadding="2" cellspacing="0"
|-
!What it looks like
!What you type
|-
|
You can *emphasize text* by putting two
apostrophes on each side. Three apostrophes
will emphasize it **strongly**. Five
apostrophes is ***even stronger***.
|<pre><nowiki>
You can *emphasize text* by putting two
apostrophes on each side. Three apostrophes
will emphasize it **strongly**. Five
apostrophes is ***even stronger***.
</nowiki></pre>
|-
|
A single newline
has no effect
on the layout.

But an empty line
starts a new paragraph.
|<pre><nowiki>
A single newline
has no effect
on the layout.

But an empty line
starts a new paragraph.
</nowiki></pre>
|-
|
You can break lines

without starting a new paragraph.

Please use this sparingly.
|<pre><nowiki>
You can break lines

without starting a new paragraph.

Please use this sparingly.
</nowiki></pre>
|-
|
You should "sign" your comments on talk pages:
* Three tildes gives your user name: `User:Karl Wick|Karl Wick`
* Four tildes give your user name plus date/time: `User:Karl Wick|Karl Wick` 07:46, 27 November 2005 (UTC)
* Five tildes gives the date/time alone: 07:46, 27 November 2005 (UTC)
|<pre><nowiki>
You should "sign" your comments on talk pages:
* Three tildes gives your user name: ~~~
* Four tildes give your user name plus date/time: ~~~~
* Five tildes gives the date/time alone: ~~~~~
</nowiki></pre>
|-
|
You can use <b>HTML tags</b>, too, if you
want. Some useful ways to use HTML:

Put text in a <tt>typewriter font</tt>.
The same font is generally used for
``computer code``.

<strike>Strike out</strike> or
<u>underline</u> text, or write it
<span style="font-variant:small-caps">
in small caps</span>.

Superscripts and subscripts:
x<sup>2</sup>, x<sub>2</sub>

Invisible comments that only appear while editing the page.
<!-- Note to editors: blah blah blah. -->
Comments should usually go on the talk page, though.
|<pre><nowiki>
You can use <b>HTML tags</b>, too, if you
want. Some useful ways to use HTML:

Put text in a <tt>typewriter font</tt>.
The same font is generally used for
``computer code``.

<strike>Strike out</strike> or
<u>underline</u> text, or write it
<span style="font-variant:small-caps">
in small caps</span>.

Superscripts and subscripts:
x<sup>2</sup>, x<sub>2</sub>

Invisible comments that only appear while editing the page.
<!-- Note to editors: blah blah blah. -->
Comments should usually go on the talk page, though.
</nowiki></pre>
|-
|
To Show "pre-formated text", for example a configureation file, use<nowiki><pre></pre></nowiki> tags
|

.. code-block::

    [home]
comment = a home directory

|}

For a list of HTML tags that are allowed, see `Help:HTML in wikitext|HTML in wikitext`. However, you should avoid HTML in favor of Wiki markup whenever possible.