Help: diting
    <namespace>Help</namespace>
<last_edited>2006-03-03T05:02:11Z</last_edited>
<last_editor>Whitecraig</last_editor>

http://meta.wikimedia.org/wiki/Help:Contents#For_editors

This Editing Overview has a lot of **`w:wikitext|wikitext`** examples. You may want to keep this page open in a separate browser window for reference while you edit.

Each of the topics covered here is covered somewhere else in more detail.  Please look in the box on the right for the topic you are interested in.
Editing basics 
------------------------

;Start editing
* To start editing a `w:MediaWiki|MediaWiki` page, click on the "**Edit this page**" (or just "**edit**") link at one of its edges. This will bring you to the **edit page**: a page with a text box containing the *`w:wikitext|wikitext`*: the editable source code from which the server produces the webpage. *If you just want to experiment, please do so in the `{{ns:4}}:Sandbox|sandbox`, not here*.

;Summarize your changes
* You should write a short `Help: dit summary|edit summary` in the small field below the edit-box. You may use shorthand to describe your changes, as described in the `{{ns:4}}: dit summary legend|legend`.

;Preview before saving
* When you have finished, press `Help:Show preview|preview` to see how your changes will look -- **before** you make them permanent.  Repeat the edit/preview process until you are satisfied, then click "Save" and your changes will be immediately applied to the article.

{{:Help:Wikitext quick reference}} <!-- Edit this at `Help:Wikitext quick reference` -->

{{:Help: diting tips and tricks}} <!-- Edit this at `Help: diting tips and tricks` -->

Minor edits 
------------------------

A `Help:Logging in|logged-in` user can mark an edit as "minor". `Help:Minor edit|Minor edit`s are generally spelling corrections, formatting, and minor rearrangement of text. Users may choose to *hide* minor edits when viewing `Help:Recent changes|Recent Changes`.

Marking a significant change as a minor edit is considered bad Wikiquette. If you have accidentally marked an edit as minor, make a `Help:Dummy edit|dummy edit`, verify that the "<small>**[&nbsp;] This is a minor edit**</small>" check-box is unchecked, and explain in the edit summary that the previous edit was not minor.

See also
------------------------

*`Help: diting FAQ`
*`Help:Automatic conversion of wikitext`
*`Help:Calculation`
*`Help: diting toolbar`
*`Help:HTML in wikitext`
*`Help:Administration#Page_protection| Protecting pages`
*`Help:Starting a new page`
*`Help:Variable`
*`w:HTML element|HTML elements`.
*`:Image:Special characters Verdana IE.png` - shows special characters with codes, and also shows problem characters.

External links
------------------------

*[http://home.earthlink.net/~awinkelried/keyboard_shortcuts.html Special characters in HTML]