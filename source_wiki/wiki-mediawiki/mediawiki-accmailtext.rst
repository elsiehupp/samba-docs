MediaWiki:Accmailtext
    <namespace>8</namespace>
<last_edited>2013-03-05T06:LL:_edited>
<last_editor>Bjacke</or>
A randomly generated password for `User talk: has been sent to $2.

You probably also want to *`Special:s/e`* the user so that he can edit sites. 

The password for this new account can be changed on the *`Special:sword|change password`* page upon logging in.