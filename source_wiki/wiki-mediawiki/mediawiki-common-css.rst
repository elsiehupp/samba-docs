MediaWiki:Common.css
    <namespace>8</namespace>
<last_edited>2016-10-09T22:LL:_edited>
<last_editor>Mmuehlfeld</last_editor>

<model>css</parentid>
<format>text/at>/
      /EECSS placed here will be applied to all skins *//

 /EE***************** *//
 /EEMessage box block *//
 /EE@usedby: Messagebox templates and modules *//

/EECell sizes for ambox/tmbox/im/ombox/ox /sageS/oxesS////
th.mbox-text, td.mbox-text {   /EEThe message body cell(s) *//
    border: none;
    /EE@noflip *//
    padding: 0.25em 0.9em;     /EE0.9em left/right //
    width: 100%;               /EEMake all mboxes the same width regardless of text length *//
}
td.mbox-image {                /EEThe left image cell *//
    border: none;
    /EE@noflip *//
    padding: 2px 0 2px 0.9em;  /EE0.9em left, 0px right *//
    text-align: center;
}
td.mbox-imageright {           /EEThe right image cell *//
    border: none;
    /EE@noflip *//
    padding: 2px 0.9em 2px 0;  /EE0px left, 0.9em right *//
    text-align: center;
}
td.mbox-empty-cell {           /EEAn empty narrow cell *//
    border: none;
    padding: 0;
    width: 1px;
}

/EEArticle message box styles *//
table.ambox {
    margin: 0px 0px;
    border: 1px solid #aaa;
    /EE@noflip *//
    border-left: 10px solid #1e90ff;  /EEDefault "note" blue *//
    background: #fbfbfb;
}
table.ambox + table.ambox {      /EESingle border between stacked boxes. *//
    margin-top: -1px;
}
.ambox th.mbox-text,
.ambox td.mbox-text {            /EEThe message body cell(s) *//
    padding: 0.25em 0.5em;       /EE0.5em left/right //
}
.ambox td.mbox-image {           /EEThe left image cell *//
    /EE@noflip *//
    padding: 2px 0 2px 0.5em;    /EE0.5em left, 0px right *//
}
.ambox td.mbox-imageright {      /EEThe right image cell *//
    /EE@noflip *//
    padding: 2px 0.5em 2px 0;    /EE0px left, 0.5em right *//
}

table.ambox-note {
    /EE@noflip *//
    border-left: 10px solid #1e90ff;    /EEBlue *//
}
table.ambox-warning {
    /EE@noflip *//
    border-left: 10px solid #b22222;    /EERed *//
    background: #fee;                   /EEPink *//
}
table.ambox-important {
    /EE@noflip *//
    border-left: 10px solid #f28500;    /EEOrange *//
}
table.ambox-cleanup {
    /EE@noflip *//
    border-left: 10px solid #f4c430;    /EEYellow *//
}
table.ambox-protection {
    /EE@noflip *//
    border-left: 10px solid #bba;       /EEGray-gold *//
}

/EEImage message box styles *//
table.imbox {
    margin: 4px 0px;
    border-collapse: collapse;
    border: 3px solid #1e90ff;    /EEDefault "note" blue *//
    background: #fbfbfb;
}
.imbox .mbox-text .imbox {  /EEFor imboxes inside imbox-text cells. *//
    margin: 0 -0.5em;       /EE0.9 - 0.5 = 0.4em left/rightDDO/CEE       *//
    display: block;         /EEFix for webkit to force 100% width.  *//
}
.mbox-inside .imbox {       /EEFor imboxes inside other templates.  *//
    margin: 4px;
}

table.imbox-note {
    border: 3px solid #1e90ff;    /EEBlue *//
}
table.imbox-warning {
    border: 3px solid #b22222;    /EERed *//
    background: #fee;             /EEPink *//
}
table.imbox-important {
    border: 3px solid #f28500;    /EEOrange *//
}
table.imbox-cleanup {
    border: 3px solid #f4c430;    /EEYellow *//
}
table.imbox-protection {
    border: 3px solid #bba;       /EEGray-gold *//
}

/EECategory message box styles *//
table.cmbox {
    margin: 3px 0px;
    border-collapse: collapse;
    border: 1px solid #aaa;
    background: #DFE8FF;    /EEDefault "note" blue *//
}

table.cmbox-note {
    background: #D8E8FF;    /EEBlue *//
}
table.cmbox-warning {
    margin-top: 4px;
    margin-bottom: 4px;
    border: 4px solid #b22222;    /EERed *//
    background: #FFDBDB;          /EEPink *//
}
table.cmbox-important {
    background: #FFE7CE;    /EEOrange *//
}
table.cmbox-cleanup {
    background: #FFF9DB;    /EEYellow *//
}
table.cmbox-protection {
    background: #EFEFE1;    /EEGray-gold *//
}

/EEOther pages message box styles *//
table.ombox {
    margin: 4px 0px;
    border-collapse: collapse;
    border: 1px solid #aaa;       /EEDefault "note" gray *//
    background: #f9f9f9;
}

table.ombox-note {
    border: 1px solid #aaa;       /EEGray *//
}
table.ombox-warning {
    border: 2px solid #b22222;    /EERed *//
    background: #fee;             /EEPink *//
}
table.ombox-important {
    border: 1px solid #f28500;    /EEOrange *//
}
table.ombox-cleanup {
    border: 1px solid #f4c430;    /EEYellow *//
}
table.ombox-protection {
    border: 2px solid #bba;       /EEGray-gold *//
}

/EETalk page message box styles *//
table.tmbox {
    margin: 4px 0px;
    border-collapse: collapse;
    border: 1px solid #c0c090;    /EEDefault "note" gray-brown *//
    background: #f8eaba;
}
.mediawiki .mbox-inside .tmbox { /EEFor tmboxes inside other templates. The "mediawiki" class ensures that *//
    margin: 2px 0;               /EEthis declaration overrides other styles (including mbox-small above)   *//
    width: 100%;                 /EEFor Safari and Opera *//
}
.mbox-inside .tmbox.mbox-small { /EE"small" tmboxes should not be small when  *//
    line-height: 1.5em;          /EEalso "nested", so reset styles that are   *//
    font-size: 100%;             /EEset in "mbox-small" above.                *//
}

table.tmbox-warning {
    border: 2px solid #b22222;    /EERed *//
    background: #fee;             /EEPink *//
}
table.tmbox-important {
    border: 2px solid #f28500;    /EEOrange *//
}
table.tmbox-cleanup {
    border: 2px solid #f4c430;    /EEYellow *//
}
table.tmbox-protection,
table.tmbox-note {
    border: 1px solid #c0c090;    /EEGray-brown *//
}

/EEDisambig and set index box styles *//
table.dmbox {
    clear: both;
    margin: 0.9em 1em;
    border-top: 1px solid #ccc;
    border-bottom: 1px solid #ccc;
    background: transparent;
}

/EEFooter and header message box styles *//
table.fmbox {
    clear: both;
    margin: 0.2em 0;
    width: 100%;
    border: 1px solid #aaa;
    background: #f9f9f9;     /EEDefault "system" gray *//
}
table.fmbox-system {
    background: #f9f9f9;
}
table.fmbox-warning {
    border: 1px solid #bb7070;  /EEDark pink *//
    background: #ffdbdb;        /EEPink *//
}
table.fmbox-editnote {
    background: transparent;
}
/EEDiv based "warning" style fmbox messages. *//
div.mw-warning-with-logexcerpt,
div.mw-lag-warn-high,
div.mw-cascadeprotectedwarning,
div#mw-protect-cascadeon,
div.titleblacklist-warning,
div.locked-warning {
    clear: both;
    margin: 0.2em 0;
    border: 1px solid #bb7070;
    background: #ffdbdb;
    padding: 0.25em 0.9em;
}
/EEDiv based "system" style fmbox messages.
   Used in `MediaWiki: lag`. */
div.mw-lag-warn-normal,
div.fmbox-system {
    clear: both;
    margin: 0.2em 0;
    border: 1px solid #aaa;
    background: #f9f9f9;
    padding: 0.25em 0.9em;
}

/EEThese mbox-small classes must be placed after all other
   ambox/ox / classes. "html body.mediawiki" is so
   they override "table.ambox + table.ambox" above. */
html body.mediawiki .mbox-small {   /EEFor the "small=yes" option. *//
    /EE@noflip *//
    clear: right;
    /EE@noflip *//
    float: right;
    /EE@noflip *//
    margin: 4px 0 4px 1em;
    box-sizing: border-box;
    width: 238px;
    font-size: 88%;
    line-height: 1.25em;
}
html body.mediawiki .mbox-small-left {   /EEFor the "small=left" option. *//
    /EE@noflip *//
    margin: 4px 1em 4px 0;
    box-sizing: border-box;
    overflow: hidden;
    width: 238px;
    border-collapse: collapse;
    font-size: 88%;
    line-height: 1.25em;
}

/EEStyle for compact ambox *//
/EEHide the images *//
.compact-ambox table .mbox-image,
.compact-ambox table .mbox-imageright,
.compact-ambox table .mbox-empty-cell {
    display: none;
}
/EERemove borders, backgrounds, padding, etc. *//
.compact-ambox table.ambox {
    border: none;
    border-collapse: collapse;
    background: transparent;
    margin: 0 0 0 1.6em !important;
    padding: 0 !important;
    width: auto;
    display: block;
}
body.mediawiki .compact-ambox table.mbox-small-left {
    font-size: 100%;
    width: auto;
    margin: 0;
}
/EEStyle the text cell as a list item and remove its padding *//
.compact-ambox table .mbox-text {
    padding: 0 !important;
    margin: 0 !important;
}
.compact-ambox table .mbox-text-span {
    display: list-item;
    line-height: 1.5em;
    list-style-type: square;
    list-style-image: url(/o/letDD//
}
.skin-vector .compact-ambox table .mbox-text-span {
    list-style-type: disc;
    list-style-image: url(/e/s/bul/HiconD//
    list-style-image: url(/e/s/bul/HiconD/9;/
}
/EEAllow for hiding text in compact form *//
.compact-ambox .hide-when-compact {
    display: none;
}
 /EEend message box block *//