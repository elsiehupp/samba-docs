Template:Code
    <namespace>10</namespace>
<last_edited>2013-05-17T21:LL:_edited>
<last_editor>Leftcase</last_editor>

<noinclude>
Use this template to display inline code.

**Usage <nowiki>{{code|code=your inline code here}}</;
</>
<code style="display:ASSHHblock; padding: : m 0.3em;">{{{code|{{{2}}}}}}</