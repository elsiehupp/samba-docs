Template:NewFeature
    <namespace>Template</namespace>
<last_edited>2019-05-29T03:LL:_edited>
<last_editor>Timbeale</last_editor>

Overview 
------------------------

**Added in Samba version:;The next Samba release containing the feature>*

*<What does the feature do, Why might users care about it, What is the Microsoft-equivalent that a Windows admin might be familiar with, etc>*

How to configure it
------------------------

*<Just reference the relevant samba-tool/smb.conf options (your man-page/help documentation should already be clear enough)>*

Known issues and limitations
------------------------

*<Any outstanding bugs, configurations not supported, etc>*

Troubleshooting
------------------------

*<What debug level do you need to run to see messages of interest.there any other ways to verify the feature is doing what it should>*

For Developers == 

How to test it
------------------------

.. code-block::

    make test TESTS=blah

*<Anything else notable about running the tests...

Where the code is located
------------------------

*<Notable files/functions to start looking at...

Reference Docs
------------------------

*<E.c. specification/section that describes the feature in more detail>*

<includeonly>`Category:re`</includeonly>