Template:Category handler/doc
    <namespace>Template</namespace>
<last_edited>2016-10-03T08:LL:_edited>
<last_editor>Mmuehlfeld</last_editor>

{{documentation subpage}}
<!-- PLEASE ADD CATEGORIES AND INTERWIKIS AT THE BOTTOM OF THIS PAGE -->

This is the {{tl|category handler}} meta-template.

It helps other templates to automate both categorization and `Help:s|category suppression`. 

Already when used with its default settings this template prevents auto-categorization in some namespaces and on some pages where we usually don't want categorization. Thus even the most basic usage like "``<nowiki>{{category handler|`Category:}}</nowiki>``" sees to that templates don't auto-categorize in the wrong places.

This template makes it easy to choose in what namespaces a template should and should not categorize. And it makes it easy to use different categories in different namespaces. And this template uses a central blacklist where we can add pages and page types where templates should not auto-categorize.

When not to use this template
------------------------

If a template only needs to categorize in one of the namespaces main (articles), file (images) or category, then using this template is overkill. Then instead use one of {{tl|main other}}, {{tl|file other}}, or {{tl|category other}}. But if your template needs to categorize in any other namespace, then we recommend you use this template, since it provides proper category suppression and makes it easy to select how to categorize in the different namespaces.

Namespaces
------------------------

This template detects and groups all the different `Project:s|namespaces` used on MediaWiki.org into several types. These types are used as parameter names in this template.

* ' = Main/article space, as in normal MediaWiki.org articles.
* ' = Any talk space, such as page names that start with "Talk:",S:quot;User talk:", : talk:" and  on.
* project, file, mediawiki, template, help, category** and **book** = The other namespaces except the talk pages.
* * = Any namespaces that were not specified as a parameter to the template. See examples below.

Basic usage
------------------------

This template takes two or more parameters. Here's an example with the full template code for an article message box:

.. code-block::

    {{ambox
| text = This is a box used in articles.
{{category handler
| `Category:
| nocat = {{{nocat|}}}   <!--So "nocat=true/false" works-->
<noinclude>

{{documentation}}
<!--Add categories and interwikis to the /doc subpage-->
</noinclude>

The above example uses the default settings for {{tl|category handler}}. That means the box will categorize on pages in the following namespaces: 
* ', **file**, **help**, **category**, **portal** and **book**
But it will *not* categorize in the following namespaces:
* ', **user**, **project**, **mediawiki** and **template**

And it will *not* categorize on blacklisted pages. (See section `#Blacklist|blacklist` below.)

The reason this template does not categorize in some of the namespaces is that in those namespaces most templates are just demonstrated or listed, not used. Thus most templates should not categorize in those namespaces. 

Any template that is meant for one or more of the namespaces where this template categorizes can use the basic syntax as shown above.

Advanced usage
------------------------

This template takes one or more parameters named after the different page types as listed in section `#Namespaces|namespaces` above. By using those parameters you can specify exactly in which namespaces your template should categorize. Like this:

.. code-block::

    {{mbox
| text = This is a box for articles and talk pages.
{{category handler
| main = `Category:]   <!--Categorize in main (article) space-->
| talk = `Category:]   <!--Categorize in talk space-->
| nocat = {{{nocat|}}}   <!--So "nocat=true" works-->

The above box will only categorize in main and talk space. But it will not categorize on /archive pages since they are blacklisted. (See section `#Blacklist|blacklist` below.) And if you need to demonstrate (discuss) that box on a talkpage, then you can feed "``nocat=true``" to prevent that template from categorizing. (See section `#Nocat|nocat` below.) Like this:

.. code-block::

    My new template 
------------------------

Hey guys, have you seen my new template?
{{mytemp|nocat=true}}
Nice, isn't it?
--~~~~

Sometimes we want to use the same category in several namespaces, then do like this:

.. code-block::

    {{mbox
| text = This is a box used in several namespaces.
{{category handler
| main = `Category:]
| 1 = `Category:]   <!--For help and user space-->
| help = 1
| user = 1
| talk =     <!--No categories on talk pages-->
| other = `Category:]   <!--For all other namespaces-->
| nocat = {{{nocat|}}}   <!--So "nocat=true/false" works-->

In the above example we use a numbered parameter to feed one of the categories, and then we tell this template to use that numbered parameter for both the help and user space.

This template understands the numbered parameters 1 to 10.

The **other** parameter defines what should be used in the remaining namespaces that have not explicitly been fed data.

Note the empty but defined **talk** parameter. That stops this template from showing what has been fed to the **other** parameter, when in talk space.

This template also has a parameter called **all**. It works like this:

.. code-block::

    {{mbox
| text = This is a box used in all namespaces.
{{category handler
| all = `Category:]   <!--Categorize in all namespaces-->
| nocat = {{{nocat|}}}   <!--So "nocat=true/false" works-->

The above example will categorize in all namespaces, but not on blacklisted pages. If you want to demonstrate that box on a page, then use "``nocat=true``" to prevent the template from categorizing. 

We suggest avoiding the **all** parameter, since templates should preferably only categorize in the namespaces they need to.

The all parameter can also be combined with the rest of the parameters. Like this:

.. code-block::

    {{mbox
| text = This is a box used in all namespaces.
{{category handler
| all = `Category:]   <!--Categorize in all namespaces-->
| main = `Category:]   <!--And add this in main space-->
| other = `Category:]   <!--And add this in all other namespaces-->
| nocat = {{{nocat|}}}   <!--So "nocat=true/false" works-->

If the above box is placed on an article, then it will add the categories "Somecat1" and "Somecat2". But on all other types of pages it will instead add "Somecat1" and "Somecat3". As the example shows, the all parameter works independently of the rest of the parameters.

Subpages
------------------------

This template understands the **subpage** parameter. Like this:

.. code-block::

    {{category handler
| subpage = no    <!--Don't categorize on subpages-->
| project = `Category:
| nocat = {{{nocat|}}}   <!--So "nocat=true/false" works-->

If "``subpage=no``" then this template will *not* categorize on subpages. For the rare occasion you *only* want to categorize on subpages, then use "``subpage=only``". If **subpage** is empty or undefined then this template categorizes both on basepages and on subpages.

Blacklist
------------------------

This template has a blacklist of the pages and page types where templates should not auto-categorize. Thus templates that use this meta-template will for instance not categorize on /archive pages and on the subpages of template messages.

If you want a template to categorize on a blacklisted page, then feed "``<nowiki>nocat = false</nowiki>``" to the template when you place it on the page, thus skipping the blacklist check. Note that this template only categorizes if it has data for the namespace. For instance, if the basic syntax is used (see `#Basic usage|basic usage` above), then even if you set "``nocat = false``" the template will not categorize on a talk page, since it has no data for talk pages. But it has data for help space, so on a blacklisted help page it will categorize.

The blacklist is in the sub-template {{tl|category handler/blacklist}}. To see or update the blacklist, go there.

The "nocat" parameter
------------------------

This template understands the **nocat** parameter:

* If "``nocat = true``" then this template does *not* categorize. 
* If **nocat** is empty or undefined then this template categorizes as usual. 
* If "``nocat = false``" this template categorizes even when on blacklisted pages. (See section `#Blacklist|blacklist` above.)

Templates that use {{tl|category handler}} should forward **nocat**, so they too understand **nocat**. The code "``<nowiki>nocat = {{{nocat|}}}</nowiki>``" shown in the examples on this page does that.

The "categories" parameter
------------------------

For backwards compatibility this template also understands the **categories** parameter. It works the same as **nocat**. Like this:

* If "``categories  = no``" then this template does *not* categorize. 
* If **categories** is empty or undefined then this template categorizes as usual. 
* If "``categories = yes``" this template categorizes even when on blacklisted pages.

When adding this template to a template that already uses the "``categories = no``" logic, then you can do like this to not break any existing usage:

.. code-block::

    {{category handler
| `Category:
| nocat = {{{nocat|}}}   <!--So "nocat=true/false" works-->
| categories = {{{categories|}}}   <!--So "categories=no" works-->

The "category2" parameter
------------------------

For backwards compatibility this template kind of supports the old "category =" parameter. But the parameter name "category" is already used in this template to feed category data for when in category space. So instead this template uses **category2** for the usage similar to **nocat**. Like this:

* If "``category2 =``" (empty but defined), or "``category2 = no``", or if **category2** is fed any other data (except as described in the next two points), then this template does *not* categorize.
* If **category2** is undefined or if "``category2 = ¬``", then this template categorizes as usual.
* If "``category2 = yes``" this template categorizes even when on blacklisted pages.

When adding this template to a template that already uses the "``category =``" logic, then you can do like this to not break any existing usage:

.. code-block::

    {{category handler
| `Category:
| nocat = {{{nocat|}}}   <!--So "nocat=true/false" works-->
| category2 = {{{category|¬}}}   <!--So "category =" works-->

Note that the "``¬``" is necessary, it helps this template to detect if the **category** parameter is defined but empty, or undefined.

Categories and text
------------------------

Besides from categories, you can feed anything else to this template, for instance some text. Like this:

.. code-block::

    {{tmbox
| text = This is a talk page message box.
{{category handler
| talk = `Category:
| other = :::: only be used on talk pages.
| nocat = {{{nocat|}}}   <!--So "nocat=true/false" works-->

When the template above is shown on anything else than a talk page, it will look like this (note the text below the box):

{{tmbox
| text = This is a talk page message box.
| nocat = true    <!--So this box doesn't add "Category message boxes"-->
{{category handler
| talk = `Category:
| other = :::: only be used on talk pages.
| nocat = {{{nocat|}}}   <!--So "nocat=true/false" works-->

That text will not show on blacklisted pages, so don't use this method to show any important information. Feeding "``nocat = true``" to the template hides the text, just as it suppresses any categories.

The "page" parameter
------------------------

For testing and demonstration purposes this template can take a parameter named **page**. Like this:

.. code-block::

    {{category handler
| main = Category cat
| talk = Category cat
| nocat = {{{nocat|}}}   <!--So "nocat=true/false" works-->
| page = User talk:

In the above code we on purpose left out the brackets around the category names so we see the output on the page. No matter on what kind of page the code above is used it will return this:

* y handler
| main = Category cat
| talk = Category cat
| nocat = {{{nocat|}}}   <!--So "nocat=true/false" works-->
| page = User talk:

The **page** parameter makes this template behave exactly as if on that page. Even the blacklist works. The pagename doesn't have to be an existing page.

If the **page** parameter is empty or undefined, the name of the current page determines the result.

You can make it so your template also understands the **page** parameter. That means you can test how your template will categorize on different pages, without having to actually edit those pages. Then do like this:

.. code-block::

    {{category handler
| main = Category cat
| talk = Category cat
| nocat = {{{nocat|}}}   <!--So "nocat=true/false" works-->
| page = {{{page|}}}   <!--For testing-->

Parameters
------------------------

List of all parameters:

.. code-block::

    {{category handler
| `Category:
| subpage = no / only
| 1
=================
...
| 10
=================
| all = `Category: / Text
| main = 1 / ... / 10 / `Category: / Text
...
| other = 1 / ... / 10 / `Category: / Text

| nocat = {{{nocat|}}} / true / false
| categories = {{{categories|}}} / no / yes
| category2 = {{{category|¬}}} / 'empty' / no / 'not defined' / ¬ / yes

| page  = {{{page|}}} / User:

Note that empty values to the "main" ... "other" parameters have special meaning (see examples above). The "all" parameter doesn't understand numbered parameters, since there should never be a need for that.

Technical details
------------------------

The centralised category suppression blacklist is in the sub-template {{tl|category handler/blacklist}}. To see or update the blacklist, go there.

For its internal parameter processing, this template uses the sub-template {{tl|category handler/numbered}}.

See also
------------------------

* `w::Category:suppression` – The how-to guide.
* `Project:s` – Lists all the namespaces.

<includeonly>
<!-- CATEGORIES AND INTERWIKIS HERE, THANKS -->
`Category: templates`

</includeonly>