Template:Imbox/doc
    <namespace>Template</namespace>
<last_edited>2016-10-12T00:LL:_edited>
<last_editor>Mmuehlfeld</last_editor>

{{Documentation subpage}}
This is the {{tl|Imbox}} ([**I**]mage [**m**]essage [**box**]) metatemplate.

Use it to highlight information.

* Keep the statements as brief and to the point as possible. 
* Use admonitions sparingly so that they do not lose their effectiveness. 

=================
Usage
=================

Example:

.. code-block::

    {{Imbox
| type = style
| text = The message body text.

=================
Image Message Box Types
=================

The following examples use different **type** parameters:

.. note:

    type=<u>note</u> (default)<br />Use this admonition to bring additional information to the user's attention.

.. warning:

   type=<u>important</u><br />Use this admonition to show the user a piece of information that should not be overlooked. While this information may not change anything the user is doing, it should show the user that this piece of information could be vital. 

{{Imbox
| type = warning
| text = type=<u>warning</u><br />Use this admonition to alert the reader that the current setup will change or be altered, such as files being removed, and not to perform the operation unless fully aware of the consequences. 

{{Imbox
| type = cleanup
| text = type=<u>cleanup</u><br />Use this admonition to mark a section as incomplete or that needs to be cleaned up.

{{Imbox
| type = protection
| text = type=<u>protection</u><br />Use this admonition only to mark a page as protected.

=================
Parameters
=================

.. code-block::

    {{Imbox
| type  = note / important / warning / cleanup / protected
| style = CSS value
| textstyle = CSS value
| text  = The message body text.

**type**
* type** parameter is given the template defaults to type **notice**. That means it gets a blue border.

**style**
* al cascading style sheets (CSS) value used by the entire message box table. Without quotation marks ``" "``. For example::
* := margin-bottom: 0.5em;<S:ode>

**textstyle**
* al CSS value used by the text cell. For example::
* :yle = text-align: center;<SSLLA:gt;

**text**
* ge body text.