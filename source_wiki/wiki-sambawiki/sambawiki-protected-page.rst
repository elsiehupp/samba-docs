SambaWiki:Protected page
    <namespace>SambaWiki</namespace>
<last_edited>2007-04-30T12:33:46Z</last_edited>
<last_editor>Jerry</last_editor>

How to get approval to edit content 
------------------------

In an effort to cut down on SPAM in the wiki, we are
instituting a means of approving accounts before a user
can edit content.  This will still be a fairly low bar to
entry but gives a means of preventing spammers from
registering new accounts with no oversight.  

In order to edit pages, you must first create an account
and then send mail to [mailto:wiki-editor@samba.org wiki-editor@samba.org] requesting
that your account be authorized to edit content. Please make sure to include your account name in the request. That's it.

For those people who have been doing a superb (or event
halfway decent job), this is no big deal.  We'll look over
the contributions log and verify that it's all legit, and
email you back as soon as possible.  For new accounts, this is still a pretty 
open trust policy. 

This is not an effort to control content or accuracy (everyone here does a good
job of that).   It is solely an effort to raise the signal to noise by reducing SPAM.

cheers, The Samba Team