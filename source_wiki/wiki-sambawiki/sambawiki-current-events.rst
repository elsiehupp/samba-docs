SambaWiki:Current events
    <namespace>4</namespace>
<last_edited>2021-03-02T07:LL:_edited>
<last_editor>Kseeger</or>
=================
2021
=================

* 2021-05-04 - 2021-05-07: [https://sambaxp.org SambaXP] (Samba Experience) online event incl. SMB IO Lab
* 2021-09-28 - 2021-09-29: [https://www.snia.org/orageD/eloper Storage Developer Conference]