Talk:Rsync based SysVol replication workaround
    <namespace>Talk</namespace>
<last_edited>2016-10-09T13:36:49Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

We are running 2 Samba domain controllers with rsync replication on Debian 7 (master) and Ubuntu 14.04.2 LTS (slave).
The sysvol share on slave DC runs into problems after each replication. The NTACL seems to be broken. It is not possible to access the share via smb/network.

We fixed this issue by adding

 samba-tool ntacl sysvolreset

command after the rsync command.

The full cron looks like:

 */5 * * * *          rsync -XAavz --delete-after --password-file=/usr/local/samba/etc/rsync-sysvol.secret rsync://sysvol-replication@{IP-of-you-PDC}/SysVol/ \
                      /path/to/your/sysvol/folder/ > /dev/null && samba-tool ntacl sysvolreset &> /dev/null

sysvolreset 
------------------------

The workaround with sysvolreset shouln't be necessary and will break your ACLs on the share, if it's not default. Make sure, that you have Make sure, that you have `Joining_a_Samba_DC_to_an_Existing_Active_Directory#GID_mappings_of_built-in_groups|identical IDs on build-in groups`. I've added a note to the page, too.