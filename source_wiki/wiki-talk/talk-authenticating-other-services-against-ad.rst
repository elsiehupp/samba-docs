Talk:Authenticating other services against AD
    <namespace>Talk</namespace>
<last_edited>2014-01-08T02:11:52Z</last_edited>
<last_editor>Unifex</last_editor>

I'd like to suggest editing this to include a scenario for each section and providing the examples in the context of the examples.  This would make it much easier for those of us that don't understand the placeholders so well to better understand them in the context of the scenario and hopefully aid us in mentally mapping the scenario described to the one we are working on.

To this end each section should start with an outline of the setup for the machines, hosts, domain names etc)

I'm currently working on an Apache Single Sign-On setup so will start with that.  This will be updated later to nginx at our end so I may be able to add documentation around that too. Reports inform me there's massive issues with SSO/AD/nginx so... yeah. The fun will begin.

Apache Single Sign-On
------------------------

===============================
Scenario
===============================

------------------------

This scenario has 3 websites (applications perhaps) running from one server and a Samba4 server acting as the AD.  This is provided to assist with visualising where the details in the examples come from.

* Web Server running on a dedicate machine on the host at shipnet.example.com
** IP address: 10.10.4.1 (as seen from the Samba4 server)
** Host : shipnet.example.com
** Site 1 at http://sso_testbed.shipnet.example.com
** Site 2 at http://sso_testcot.shipnet.example.com
** Site 3 at http://sso_testbunk.shipnet.example.com

* Samba4 server acting as AD running on a dedicate machine on the host at samba4.shipnet.example.com
** IP address : 10.10.4.23
** Host : samba4.shipnet
** Realm Name : SAMBA4.SHIPNET
** ADC (Active Directory domain controller) : dc1.samba4.shipnet

===============================
Setup
===============================

------------------------

Rest of docs follow...

Can someone look this over and verify that the scenario is plausible? It's based on what I'm currently trying to work on so should be realistic.

--`User:Unifex|Unifex` 02:11, 8 January 2014 (UTC)