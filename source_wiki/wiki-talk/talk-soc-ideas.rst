Talk:SoC/Ideas
    <namespace>Talk</namespace>
<last_edited>2007-03-01T17:39:25Z</last_edited>
<last_editor>JelmerVernooij</last_editor>

About Wireshark pidl extension)
------------------------

What extensions would exactly be needed that are not covered by the wireshark conformance files at the moment? From what I've understood from Ronnie Sahlberg, the current extensions are sufficient --`User:JelmerVernooij|ctrlsoft` 11:34, 1 March 2007 (CST)

smbclient in ejs
------------------------

I wouldn't be too much of a fan of this, given my reservations about EJS in non-web applications. Not sure if this is the right moment to voice them though, or rather when the proposals come in? --`User:JelmerVernooij|ctrlsoft` 11:39, 1 March 2007 (CST)