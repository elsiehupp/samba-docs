Talk:Samba/samba-tool/domain/classicupgrade/HOWTO
    <namespace>Talk</namespace>
<last_edited>2013-05-15T14:10:19Z</last_edited>
<last_editor>Kseeger</last_editor>

I have tried to combine the two sections in this HOWTO (Migrate on a new server and Migrate in place), or to at least merge them somewhat.  The two sections were largely redundant, but started diverging in how they explained the same steps, which was becoming confusing.  Instead, I explained how to copy over the production Samba3 files to the new server, and then said that the following steps in the conversion were the same.

The section on inserting static DNS records into the DLZ based zones is not strictly a "migration" issue, but I thought it would most often be hit by people who are trying to migrate an existing system (Samba domain and DNS) to a new server.