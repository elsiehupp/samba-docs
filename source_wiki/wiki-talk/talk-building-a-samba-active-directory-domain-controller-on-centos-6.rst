Talk:Building a Samba Active Directory Domain Controller on CentOS 6
    <namespace>Talk</namespace>
<last_edited>2014-05-18T22:37:55Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

In which way this page would differ from the main Samba AD HowTo? The pages linked there - like the one about required packages - contain RHEL/Centos 6 information, too.

Wouldn't it be more useful, to put the distro specific information to the existing pages about distro stuff? This would avoid having duplicate HowTos, that wouldn't be maintained/reviewed in future.