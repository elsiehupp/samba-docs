Talk:Samba AD on CentOS7
    <namespace>Talk</namespace>
<last_edited>2015-08-24T21:32:52Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

This page seems to be most a duplicated of other guide we're having. We should avoid this, because nobody will maintain these distro specific pages in future and users find old and outdated content. I suggest removing all content that is already described on the generic pages and keep only the very specific part.