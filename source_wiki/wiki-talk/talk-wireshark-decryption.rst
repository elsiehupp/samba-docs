Talk:Wireshark Decryption
    <namespace>Talk</namespace>
<last_edited>2018-12-14T15:35:16Z</last_edited>
<last_editor>Aaptel</last_editor>

Added a note about Wireshark's 64-bit Windows version not accepting the -K flag, which I ran into while capturing smart card login packets & figured I should share.