Talk:CTDB and Clustered Samba
    <namespace>Talk</namespace>
<last_edited>2021-02-01T05:58:31Z</last_edited>
<last_editor>MartinSchwenke</last_editor>

Note about CTDB not working with Samba AD DCs
------------------------

@Hortimech, you added a node saying:

 It should be noted that CTDB cannot be used with Samba AD DC's, it should only be used with Unix domain members or standalone servers.

Can you please explain why you believe this to be true?  We do our testing using a Samba AD DC and it seems to work.

In the interim I'm going to revert this note, which I only just noticed...

--`User:MartinSchwenke|MartinSchwenke` (`User talk:MartinSchwenke|talk`) 05:58, 1 February 2021 (UTC)