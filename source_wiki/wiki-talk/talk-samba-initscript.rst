Talk:Samba/InitScript
    <namespace>Talk</namespace>
<last_edited>2013-05-15T14:09:28Z</last_edited>
<last_editor>Kseeger</last_editor>

Debian Based Systems 
------------------------

    !/bin/sh

    ## BEGIN INIT INFO
     Provides:          samba
     Required-Start:    $network $local_fs $remote_fs
     Required-Stop:     $network $local_fs $remote_fs
     Default-Start:     2 3 4 5
     Default-Stop:      0 1 6
     Should-Start:      slapd
     Should-Stop:       slapd
     Short-Description: start Samba daemon (samba)
    ## END INIT INFO

Should-Start/Stop **slapd** doesn´t make sense IMO. Assuming Samba4 uses builtin LDAP service (which it does by default), might even prevent samba from starting.

`User:Intruder0815|Enrico Ehrhardt` 19:51, 24 July 2012 (UTC)