Talk:Terms and Abbreviations
    <namespace>Talk</namespace>
<last_edited>2014-07-01T20:37:43Z</last_edited>
<last_editor>Hartnegg</last_editor>

Is it OK to put Text from wikipedia in here? ( Creative Commons Attribution-ShareAlike License )
Or should we just put one summary sentence of description here and link to the WP instead?

I think we should have our own short descriptions here and not link to other pages. Why should people use this page, if it's just a link collection to other pages?. Then they can directly google the term or search Wikipedia. ;-)
And taking text from Wikipedia would be only possible, if the license allows and all obligations are fulfilled.

<hr>
Own texts can be made more specific to the DC topic, than the very general texts from Wikipedia
([https://wiki.samba.org/index.php?title=Terms_and_Abbreviations&diff=9086&oldid=9075 example]).

Also rewriting the few sentences probably takes less time than understanding the Wikipedia License.

`User:Hartnegg|Hartnegg` 20:37, 1 July 2014 (UTC)