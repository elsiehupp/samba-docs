Talk:Changing the DNS Back End of a Samba AD DC
    <namespace>Talk</namespace>
<last_edited>2016-10-12T01:07:03Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

A clean/default install of 4.1.6 does not have a "server services" section.
Is adding one with "-dns" really the best option for disabling internal dns?

Yes. It's the only option you have to disable it, since there are no separate options to turn on/off e. g. internal dns.