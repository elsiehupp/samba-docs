Talk:Maintaining Unix Attributes in AD using ADUC
    <namespace>Talk</namespace>
<last_edited>2019-08-06T20:01:54Z</last_edited>
<last_editor>Dmulder</last_editor>

=================
How do I get YaST ADUC module?
=================

The ADUC and ADSI modules can be downloaded in an [https://appimage.github.io/admin-tools AppImage here]. Simply download the binary and mark it executable. These have been tested on Ubuntu 18.04 and openSUSE Tumbleweed. Bugs can be reported to the [https://bugzilla.opensuse.org/enter_bug.cgi?product=openSUSE%20Tumbleweed openSUSE bugzilla].

The yast2-aduc module can be found in the software repositories of openSUSE Tumbleweed, openSUSE Leap 15.1, and SLE 15 sp1.
For the most up to date package builds, you can use the openSUSE build services YaST:Head repositories, or the AppImage.

Some more information is available at:
* https://sambaxp.org/fileadmin/user_upload/sambaxp2019-slides/mulder_sambaxp2019_samba_active_adrectory_tools_windows_admin.pdf