Talk:Active Directory Naming FAQ
    <namespace>Talk</namespace>
<last_edited>2016-04-02T16:47:29Z</last_edited>
<last_editor>Rsharpe</last_editor>

<blockquote>NetBIOS is similar to DNS in that it can serve as a directory service, but more limited as it has no provisions for a name hierarchy and names were limited to 15 characters.</blockquote>

Actually, no!

WINS and NetBIOS-NS is the directory service. NetBIOS is the transport. We should be clear about this even in a section that is giving historical info.