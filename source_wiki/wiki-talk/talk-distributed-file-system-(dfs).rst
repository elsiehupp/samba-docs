Talk:Distributed File System (DFS)
    <namespace>Talk</namespace>
<last_edited>2015-08-25T20:18:03Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

Some sources claim that the client randomly chooses a target, if the server reports more than one target.

Other sources claim that there is a selection process based on how near the client is to the server, and the description sounds as if the server would do this selection.

Does anybody know for sure how it works?