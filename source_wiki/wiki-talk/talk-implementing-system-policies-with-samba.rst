Talk:Implementing System Policies with Samba
    <namespace>Talk</namespace>
<last_edited>2007-11-08T20:31:54Z</last_edited>
<last_editor>Darden</last_editor>

`User:Mgpeter|Mgpeter` 16:03, 6 June 2006 (CDT)
Uploaded page off of my website, any and all improvements are welcome, also if anyone has any info on what impact Windows Vista will have to system policies please add it.  Finally, if anyone has any ideas on how to add an entire custom policy template (simple text file) to a wiki let me know.

--`User:Ngrier|Ngrier` 15:34, 16 January 2007 (CST) According to this [http://meta.wikimedia.org/wiki/Help:Images_and_other_uploaded_files#Uploading_non-image_files] uploading non-image files appears as if it's site-by-site and would require change in the website configuration. Alternatively, you can create a new page whose only content is the text of the specific file. But that can often violate usage/spirit of a wiki rules.

`User:Darden|Darden` 14:31, 8 November 2007 (CST)
I was able to download the policy editor from the office '97 resource package. I was not able to find it any place else. Just search for "policy editor" at microsoft.com.