Talk:Joining a Samba DC to an Existing Active Directory
    <namespace>Talk</namespace>
<last_edited>2016-10-09T13:32:34Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

I get the impression that the "Check required DNS entries of the new host" item is in the wrong place. 

The new DNS entries are not created until samba is first started. 

Am i right?