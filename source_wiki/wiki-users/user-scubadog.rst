User:ScubaDog
    <namespace>User</namespace>
<last_edited>2014-01-28T18:LL:_edited>
<last_editor>ScubaDog</last_editor>

Topics I want to write about.. but might not get to. 
------------------------

By Bryan Popham

9/9/13 - Ver. 0.1

Backup, Migration and/or Recovery 
------------------------

Migrate from a old ver of Samba4 in Ubuntu
(Samba4Alpha11 to Samba4 Current)

stop all DC and tar up the one your upgrading. scp it to the new server and unpack it..

git the latest samba, configure --enable-debug --enable-selftest

make

sudo make install

make a new samba dir with the new bin, sbin, modules & lib from make install

the all the rest of the folders from the old build

make sure the hosts, resolv.conf, fqdn & IP are the the same as the old server

run "samba-tool dbcheck --fix" 3x times till no errors

run "samba-tool dbcheck --reset-well-known-acls --fix" 2x times till no errors

run ~/samba-master/source4/scripting/bin/samba_upgradedns

rerun make install 

Recovering from a TAR backup in Ubuntu

Proper way to backup Samba4 AD DC with rsnapshot

https://irclog.samba.org/2013/09/20130909-Mon.log

Simple Fix 
------------------------

Setting the hardware clock.

Sometimes when the power has been out for over 30 mins and the server 

boots with no internet/intranet access to reach NTP the server will revert

to its hardware clock knocking the AD DC offline

From my howto - http://www.bryanpopham.com/tutorials/Set_System_and_Hardware_Date_in_Ubuntu.txt

Set system clock and date

user@svr:te 100910032012.02

Set hardware clock and date

user@svr:clock --set --date="10/09/12 10:13:00&qu::