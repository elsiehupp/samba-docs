User:Leftcase
    <namespace>User</namespace>
<last_edited>2013-05-17T20:LL:_edited>
<last_editor>Leftcase</last_editor>

**Name** - Chris 

**Handle** - Leftcase 

**Location** - Yorkshire<sup>[1]</sup>, UK

Hi there, I'm Chris and I'm trying to give a little back to the Samba project by adding to the wiki. If you want to connect, hit the discussion tab, email me at **chris <at> justuber <dot> com** or add me on `https://www.twitter.com/leftcase Twitter`

<sup>[1]</sup> * Wikipedia has a good article about `https://en.wikipedia.org/wiki/Yorkshire Yorkshire`, a great place to visit if you're ever in the UK. The Yorkshire accent is pretty similar to that spoken by the people of 'The North' in the TV series 'Game of Thrones'. Confused? Watch the bloke from 31 seconds https://www.youtube.com/watch?v=uI2WKeJpV8o. Sadly ah dohnt speek quait as brord as 'im though!*

Pages under construction 
------------------------

`Building a Samba Active Directory Domain Controller on CentOS 6`

Templates and formatting 
------------------------

Warning!
------------------------

**This text would come before the warning element**. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation 

{|style="background:padding: 6px; wi:; margin: 6:right; border::#AAA; textD:gn:center;&qu::
| `File:OOTTsvg.png|left|40px` This is an example warning message. It is designed to stand out against the rest of the article.
|} 

**Text placed after the warning element flows around it.** You've probably got to be somewhat careful when formatting like this because viewing on different sized screens can have unexpected results. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation

Images I'm using ==