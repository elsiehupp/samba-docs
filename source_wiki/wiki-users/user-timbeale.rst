User:Timbeale
    <namespace>User</namespace>
<last_edited>2019-02-07T20:LL:_edited>
<last_editor>Timbeale</last_editor>

Sandpit page to demonstrate mediawiki transclusion. For more details, see https://www.mediawiki.org/w/index.php?title=Transclusion

=================
Transclude whole page
=================

The below example uses <nowiki>{{:</nowiki> to include the entire `License` page.

Note the ':rtant, otherwise it defaults to using the *Template* namespace. You could alternatively make it explicit that the page you want to transclude is in the *Main* namespace, i.e. <nowiki>{{Main:License}:ASSHHnowiki> does the same thing.

Transcluded content 
------------------------

{{:

=================
Transclude part of page
=================

The below example uses the same transclusion (of a different page - <nowiki>{{User:SLLAASSHHtransclusion}}</nowiki>) to include just part of the `User:Timbeale:transclusion` page. 

Transcluded content 
------------------------

{{User:SLLAASSHHtransclusion}}

My WIP 
------------------------

`User/Timbeale/Replace cwrap with namespaces`