User talk:Debankur
    <namespace>User talk</namespace>
<last_edited>2008-12-16T09:LL:_edited>
<last_editor>Hhenkel</last_editor>

Description
------------------------

The "General Parallel File System" by IBM is a clustered file system with POSIX semantics. To make Samba interoperable with GPFS, IBM has provided a library including important calls for Samba is included beginning with the GPFS v3.1 PTF6. .

It contains function calls in the following areas:

===============================
Leases (analogous to OpLocks or NFSv4 delegations)
===============================

------------------------

Unless Samba oplocks are turned off (they are on by default) dual Windows-UNIX access to the same file (or multiple Samba servers) may result in the file data being corrupted (until this support is provided). The gpfs_set_lease() and gpfs_get_lease() functions allow Samba to set leases in the filesystem which are visibly for UNIX processes and on all nodes the filesystem is mounted on.

===============================
Share modes (Equivalent of Open Locks and NFSv4 reservations)
===============================

------------------------

Without this support, the implementation of multiple Samba servers (or dual Windows-UNIX access to the same file) may result in the file data being corrupted.  Needed to avoid impacting commands, like file deletion while ingesting and even to establish the possibility of proper collaboration.

With the gpfs_set_share() function available to Samba, Samba can provide the 1-second wait using the asynchronous character of the interface.

===============================
ACLs
===============================

------------------------

GPFS ACLs are currently available in POSIX format via the Linux getxattr inode operation.  Although equivalent for NFSv4 ACLs available, they are not provided by any shipped distro of linux. With gpfs_getacl() and gpfs_setacl() Samba can now get and set NFSv4 ACLs in GPFS on Linux.

Files
------------------------

     gpfs_gpl.h
     libgpfs_gpl.so

Function Calls
------------------------

===============================
gpfs_getacl()
===============================

------------------------

*FUNCTION*

Retrieves the ACL information for a file.
This function, together with gpfs_putacl, is intended for use by a backup program to save (gpfs_getacl) and  restore (gpfs_putacl) the ACL information for a file. The flags parameter must be zero (reserved for future use). The **aclP** parameter must point to a buffer mapped by the *gpfs_opaque_acl_t* structure. The first four bytes of the buffer must contain its total size.

Note:of gpfs_fgetattrs/gpfs_fputattrs is preferred.

*FUNCTION PROTOTYPE :
    int gpfs_getacl(char *pathname, int flags, void *acl);

*RETURNS :
      0      Successful
     -1      Failure
*ERROR :
     ENOSYS  function not available
     ENOSPC  buffer too small to return the entire ACL. 
             Needed size is returned in the first four bytes of the buffer pointed to by aclP.

===============================
* <b> gpfs_putacl() </b> *
===============================

------------------------

*FUNCTION :

Sets the ACL information for a file. 
The buffer passed in should contain the ACL data that was obtained by a previous call to gpfs_getacl. The flags parameter must be zero (reserved for future use).

Note:of gpfs_fgetattrs/gpfs_fputattrs is preferred.

*FUNCTION PROTOTYPE :
   int gpfs_putacl(char *pathname, int   flags, void *acl);

*RETURNS :
      0      Successful
     -1      Failure

*ERROR :
     ENOSYS  function not available

===============================
gpfs_set_share()
===============================

------------------------

*FUNCTION :

Acquire shares for Samba

       Input :
             fd    :criptor
             allow :pe being requested
                     GPFS_SHARE_NONE, GPFS_SHARE_READ,
                     GPFS_SHARE_WRITE, GPFS_SHARE_BOTH
             deny  :pe to deny to others
                     GPFS_DENY_NONE, GPFS_DENY_READ,
                     GPFS_DENY_WRITE, GPFS_DENY_BOTH

*FUNCTION PROTOTYPE :
    int gpfs_set_share(int fd, unsigned int allow, unsigned int deny);

    allow/deny specifications
    #define GPFS_SHARE_NONE   0
    #define GPFS_SHARE_READ   1
    #define GPFS_SHARE_WRITE  2
    #define GPFS_SHARE_BOTH   3
    #define GPFS_DENY_NONE    0
    #define GPFS_DENY_READ    1
    #define GPFS_DENY_WRITE   2
    #define GPFS_DENY_BOTH    3

*RETURNS :
      0      Successful
     -1      Failure

*ERROR :
    Specific error indication
    EACCES  share mode not available

===============================
gpfs_set_lease()
===============================

------------------------

*FUNCTION :

Acquire leases for Samba

    Input :
          fd        :criptor
          leaseType :pe being requested
                      GPFS_LEASE_NONE GPFS_LEASE_READ,
                      GPFS_LEASE_WRITE

*FUNCTION PROTOTYPE :
   int gpfs_set_lease(int fd, unsigned int leaseType);

      /* leaseType specifications */
      #define GPFS_LEASE_NONE    0
      #define GPFS_LEASE_READ    1
      #define GPFS_LEASE_WRITE   2

*RETURNS :
      0      Successful
     -1      Failure

*ERROR :
    Specific error indication
    EACCES  lease not available

===============================
gpfs_get_lease()
===============================

------------------------

*FUNCTION :
    Returns the type of lease currently held

*FUNCTION PROTOTYPE :
   int gpfs_get_lease(int fd);

*Returns :
        GPFS_LEASE_READ
        GPFS_LEASE_WRITE
        GPFS_LEASE_NONE
        -1      Failure

*ERROR :
    Specific error indication
    EINVAL

Download
------------------------

http://www14.software.ibm.com/webapp/set2/sas/f/gpfs/home.html

License
------------------------

Copyright (C) 2006 International Business Machines

All rights reserved.

This file is part of the GPFS.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the 
following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. The name of the author may not be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
DAMAGES (INCLUDING, BUT NOT LIMITED TO,PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
THE POSSIBILITY OF SUCH DAMAGE.