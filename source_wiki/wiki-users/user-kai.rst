User:Kai
    <namespace>User</namespace>
<last_edited>2008-07-09T11:LL:_edited>
<last_editor>Kai</last_editor>

ToDo 
------------------------

Samba3
------------------------

 net utility =
===============================

------------------------

* Remove use of global variables. ✓
* Make the commands more consistent. ✓
* Switch to using functable2 for all commands. ✓
* Make "help" generate help text from the function table. ✓
* Switch to using functable3 for all commands. ✓
* Switch to allocated function tables. ✓
* Make net use plugins for commands.
* Get rid of getuid() == 0 checks.
* i18n

Franky
------------------------

 net utility =
===============================

------------------------

* `User:SSHHnet_cli_redesign | Redesign` the command line interface of "net".
** Specify transport as option, not as command (net --ads instead of net ads).
** Unify the parameters to commands as much as possible
** Make use of libnetapi for net join, net user and net group where possible