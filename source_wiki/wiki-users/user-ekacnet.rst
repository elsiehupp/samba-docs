User: kacnet
    <namespace>User</namespace>
<last_edited>2011-06-10T20:LL:_edited>
<last_editor>Ekacnet</last_editor>

Hello this is my page ! 
I'm Matthieu Patou, you can reach me on #samba-technical with the nick ekacnet.

I'm now a Samba team member (yeah !) since May 2010.

I try to keep the [http://wiki.samba.org/index.php/Samba4/Andrew_and_Jelmers_Fantasy_Page fantasy page] up to date with what I'll try to do in the short term.

What I've done / For what you can blame me ! 
------------------------

* Upgradeprovsion, a script to update your running provision to adapt to the perpetual changes we do in the Samba Domain controller code (aka Samba4 so far).
* DFS referral resolution, so that multi DC setup can work correctly with newer client when accessing sysvol and netlogon share
* Protected storage, a protocol to decrypt selected user secrets with DCs private key, used mainly for certificates
* Dirsync, an LDAP control used for polling changes
* Pseudobacklinks, a technical artifact so that all attributes with DN syntax are updated when the targeted DN change its name, allow moving DC between sites

Short term jobs 
------------------------

* None

Medium term jobs 
------------------------

* File Replication Service
* Change indexing

About indexing 
------------------------

For the moment LDB use DN as entries of a index, as DN can be quite long we use a lot of space and memory for this.
The idea is to use instead GUID at least in indexes.

Currently an indexed search on attribute samAccountName for the value *mat* is done like this:

# search for DN:AMACCOU::
# get the different DNs in the @IDX attributes
# fetch each objects for which the DN was return in step 2
# do the filtering 

The idea is to do like this:
# search for DN:AMACCOU:  (via ltdb_se: d)
# get the different objectGUIDs in the @IDX attributes (through ltdb_index_dn or ltdb_index_dn_one is scope = ONELEVEL or directly if scope = BASE)
# for each GUID search the associated DN
# fetch each objects for which the DN was return in step 3 (in ltdb_index_filter)
# do the filtering (in ltdb_index_filter)

The following approach has the advantage of not modifying to much stuff while allowing to reduce index size, **but** it means that if an index contains 100 entries then we will do 201 fetchs (1 for the index, 100 for the index guid -> dn, 100 for each DN).

Ideas on how to do it 
------------------------

Search part
------------------------

Tag new indexes with version 3 and keep version 2 for the IDXONE indexes + index from GUID to DN.

The function ltdb_index_dn has to be modified to check if after the call has succeeded it should return a list of DN or a list of GUID, by default it seems more interesting to return a list of GUID (as ltdb_index_dn tends to call itself indirectly  quite a lot of time). The idea is to make ltdb_index_dn mostly deal with GUID but returns a list of DN at the very last moment (in ltdb_search_indexed).

Function list_intersect has to be changed to be able to do intersection on GUIDs rather than on DNs, it should also bring some speedup.

Function ltdb_index_dn_simple needs to be changed to return a list of GUIDs.

Function ltdb_index_dn_one needs just to translate each GUIDs in DNs.