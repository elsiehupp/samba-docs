User:Sgonzalez/Samba 4 OS requirements/Gentoo
    <namespace>User</namespace>
<last_edited>2012-09-11T14:LL:_edited>
<last_editor>Sgonzalez</last_editor>

Gentoo
------------------------

Please note that the following sections assume at least an intermediate understanding of the Gentoo packaging system.

 Python =
===============================

------------------------

Gentoo uses python-3 as the default python interpreter, but at this time Samba requires python-2 (2.4.2 or greater)  The following set of commands will install and set up python-2 as the default python interpreter.

 # emerge --ask '<dev-lang/python-3'
 # eselect python list # **Note the index number of the python-2 installation**
 # eselect python set <index number of python 2 from previous command>
 # python-updater

 Kerberos =
===============================

------------------------

On Gentoo, you have two choices for a kerberos implementation, **app-crypt/mit-krb5 and app-crypt/heimdal**.  Unfortunately the two implementations can not be installed at the same time.  Currently, the Samba developers recommend using **app-crypt/heimdal*.  So you must first uninstall *app-crypt/mit-krb5* (if installed,) then install **app-crypt/heimdal** and rebuild any packages that were using the old kerberos implementation.

 # emerge --unmerge --ask app-crypt/mit-krb5
 # emerge --ask app-crypt/heimdal
 # revdep-rebuild -- -ask

 Bind =
===============================

------------------------

To enable automatic zone management with bind, bind (and bind-tools) should be emerged with the USE flags for **berkdb**, **dlz** and **gssapi** set.
To enable them permanently, add the following to **/etc/package.use**:

 net-dns/bind berkdb dlz gssapi
 net-dns/bind-tools gssapi

Then, emerge **net-dns/bind**:

 # emerge --ask  net-dns/bind net-dns/bind-tools

Note that if you have problems with samba's gssapi updates to bind, try using the alternate kerberos implementation of app-crypt/mit-krb5.

 Other Misc. Build/Run Dependencies =
===============================

------------------------

To ensure a successful Samba-4 installation, there are a few other packages that should be installed, as shown below:

 # emerge --ask dev-libs/cyrus-sasl net-libs/gnutls dev-python/dnspython net-dns/libidn dev-python/subunit

 Samba-supplied Libraries (tdb/ldb/tevent) =
===============================

------------------------

There are a few Samba libraries that need to be installed, note that at this time these packages are keyworded as unstable,  so you will need to add the following to your **/etc/package.keywords**:

 ~sys-libs/tevent-0.9.17
 ~sys-libs/tdb-1.2.10
 ~sys-libs/ldb-1.1.12

Additionally, Samba requires **sys-libs/tdb** to be emerged with the USE flag **python** set.  To enable this permanently, add the following to **/etc/package.use**:

    ys-libs/tdb python

Now, emerge the packages:

 # emerge --ask '=sys-libs/tdb-1.2.10' '=sys-libs/tevent-0.9.17' '=sys-libs/ldb-1.1.12'

Note that ebuilds for the required versions of the above packages might not be availiable in the portage tree.  In this case, check [https://bugs.gentoo.org/ Gentoo's Bugzilla] for updated ebuilds.