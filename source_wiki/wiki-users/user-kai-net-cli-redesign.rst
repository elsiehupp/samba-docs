User:Kai/net cli redesign
    <namespace>User</namespace>
<last_edited>2008-06-04T13:LL:_edited>
<last_editor>Kai</last_editor>

The Samba3 net utility currently takes the following parameters:
 	struct poptOption long_options[] = {
 		{"help",	'h', POPT_ARG_NONE,   0, 'h'},
 		{"workgroup",	'w', POPT_ARG_STRING, &c->opt_target_workgroup},
 		{"user",	'U', POPT_ARG_STRING, &c->opt_user_name, 'U'},
 		{"ipaddress",	'I', POPT_ARG_STRING, 0,'I'},
 		{"port",	'p', POPT_ARG_INT,    &c->opt_port},
 		{"myname",	'n', POPT_ARG_STRING, &c->opt_requester_name},
 		{"server",	'S', POPT_ARG_STRING, &c->opt_host},
 		{"encrypt",	'e', POPT_ARG_NONE,   NULL, 'e', "Encrypt SMB transport (UNIX extended servers only)" },
 		{"container",	'c', POPT_ARG_STRING, &c->opt_container},
 		{"comment",	'C', POPT_ARG_STRING, &c->opt_comment},
 		{"maxusers",	'M', POPT_ARG_INT,    &c->opt_maxusers},
 		{"flags",	'F', POPT_ARG_INT,    &c->opt_flags},
 		{"long",	'l', POPT_ARG_NONE,   &c->opt_long_list_entries},
 		{"reboot",	'r', POPT_ARG_NONE,   &c->opt_reboot},
 		{"force",	'f', POPT_ARG_NONE,   &c->opt_force},
 		{"stdin",	'i', POPT_ARG_NONE,   &c->opt_stdin},
 		{"timeout",	't', POPT_ARG_INT,    &c->opt_timeout},
 		{"machine-pass",'P', POPT_ARG_NONE,   &c->opt_machine_pass},
 		{"myworkgroup", 'W', POPT_ARG_STRING, &c->opt_workgroup},
 		{"verbose",	'v', POPT_ARG_NONE,   &c->opt_verbose},
 		{"test",	'T', POPT_ARG_NONE,   &c->opt_testmode},
 		/* Options for 'net groupmap set' */
 		{"local",       'L', POPT_ARG_NONE,   &c->opt_localgroup},
 		{"domain",      'D', POPT_ARG_NONE,   &c->opt_domaingroup},
 		{"ntname",      'N', POPT_ARG_STRING, &c->opt_newntname},
 		{"rid",         'R', POPT_ARG_INT,    &c->opt_rid},
 		/* Options for 'net rpc share migrate' */
 		{"acls",	0, POPT_ARG_NONE,     &c->opt_acls},
 		{"attrs",	0, POPT_ARG_NONE,     &c->opt_attrs},
 		{"timestamps",	0, POPT_ARG_NONE,     &c->opt_timestamps},
 		{"exclude",	'X', POPT_ARG_STRING, &c->opt_exclude},
 		{"destination",	0, POPT_ARG_STRING,   &c->opt_destination},
 		{"tallocreport", 0, POPT_ARG_NONE,    &c->do_talloc_report},

 		POPT_COMMON_SAMBA
 		{ 0, 0, 0, 0}
 	};

The following options should be added:
 --ads  To force ADS transport if possible.
 --rpc  To force RPC transport if possible.
 --rap  To force RAP transport if possible.