User:Whitemice
    <namespace>User</namespace>
<last_edited>2011-09-20T14:LL:_edited>
<last_editor>Whitemice</last_editor>

*Location:Rapids, MI. USA (West Michigan)

* Network & Systems Administrator
** Experience in:
*** OpenLDAP
*** Samba
*** Postfix
*** Cyrus IMAP & SASL
*** VMware Workstation & ESX
*** Bind / DNS
*** LINUX & AIX Operating System administration & deployment
*** IPv4 & IPv6 network management & deployment including Cisco IOS
*** Kerberos (Hiemdal, MIT, & Samba 4)
*** Microsoft Windows Update Services
*** Informix & PostgreSQL relation database systems.
*** PHP application deployment and administration
* OpenGroupware Developer
** Lead developer for:
*** [http://sourceforge.net/projects/coils/ OpenGroupware Coils]
*** [http://sourceforge.net/p/snurtle/home/Home/ snurtle] a command-line interface for OpenGroupware Coils collaboration and workflow functions.
*** [http://sourceforge.net/projects/imbolc/ Imbolc] a Gtk client-application for OpenGroupware Coils task-management.
** Author of [http://sourceforge.net/projects/coils/files/WMOGAG-Coils.pdf/download "Whitemice Consulting OpenGroupware Administrator's Guide"], aka "WMOGAG".
** Package maintainer for OpenGroupware Legacy (Objective-C)