User:Obnox
    <namespace>User</namespace>
<last_edited>2012-12-13T22:LL:_edited>
<last_editor>Obnox</last_editor>

Real Name:Adam

Samba Developer.

Recent work:
* SMB2 and SMB3 support in the smbd file server
* winbindd id-mapping
* Clustering support (CTDB)
* libsmbconf
* Registry configuration backend.
* Work on file system ACL support.
* ...