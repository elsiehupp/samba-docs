User:Timbeale/transclusion
    <namespace>User</namespace>
<last_edited>2018-11-27T20:LL:_edited>
<last_editor>Timbeale</last_editor>

Sandpit page to demonstrate transclusion.

Sometimes you only want to transclude *part* of a wiki page, e.g. a key paragraph or section. The rest of the wiki page might contain other info that, while still useful, isn't related to the point you want to reiterate.

You can wrap the key paragraph in &lt;onlyinclude&gt; markers. You can only use these markers once per page. For example:

<onlyinclude>**Here!** This paragraph is wrapped in &lt;onlyinclude&gt; markers. It is both visible on the original page it appears on, and any other page that transcludes the original.</onlyinclude>

There are other transclusion markers - &lt;includeonly&gt; and &lt;noinclude&gt;. However, these get more complicated, as the content it wraps does not appear visible on the original page.