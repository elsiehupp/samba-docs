User:ShadowKnight
    <namespace>User</namespace>
<last_edited>2006-09-15T12:LL:_edited>
<last_editor>ShadowKnight</last_editor>

ShadowKnight:

ShadowKnight works for the Jara23 network (a small network with big aspirations). He's been writing documentation for far longer than he would have liked. For more information, check out http://www.jara23.co.uk and follow some of the links.

Any problems with the Samba how-to please contact me through http://ask.jara23.co.uk.