Module:Category handler/blacklist
    <namespace>Module</namespace>
<last_edited>2017-02-22T21:29:26Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

<model>Scribunto</model>
<format>text/plain</format>
-- This module contains the blacklist used by `Module:Category handler`.
-- Pages that match Lua patterns in this list will not be categorised unless
-- categorisation is explicitly requested.

return {
    '^Main Page$', -- don't categorise the main page.

    -- Don't categorise the following pages or their subpages.
    -- "%f[/\0]" matches if the next character is "/" or the end of the string.
    '^Wikipedia:Cascade%-protected items%f[/\0]',
    '^User:UBX%f[/\0]', -- The userbox "template" space.
    '^User talk:UBX%f[/\0]',

    -- Don't categorise subpages of these pages, but allow
    -- categorisation of the base page.
    '^Wikipedia:Template messages/.*$',

    '/[aA]rchive' -- Don't categorise archives.
}