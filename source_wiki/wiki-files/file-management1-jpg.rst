File:Management1.jpg
    <namespace>File</namespace>
<last_edited>2006-06-02T14:47:02Z</last_edited>
<last_editor>Mdietz</last_editor>

the current Samba3 implementation lacks a defined interface for managing sessions, shares, printers or parameters.
Each third party management software has to parse and write the smb.conf file on it's own: