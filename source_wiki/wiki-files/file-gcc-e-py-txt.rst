File:Gcc-E.py.txt
    <namespace>File</namespace>
<last_edited>2019-05-19T01:59:41Z</last_edited>
<last_editor>Abartlet</last_editor>

A compiler wrapper script to build complex projects with gcc -E despite the project needing to still use the results of a normal gcc run.