File:Windows7Features.png
    <namespace>File</namespace>
<last_edited>2015-02-13T19:41:30Z</last_edited>
<last_editor>Miguelmedalha</last_editor>

Image showing the "Turn Windows features on or off" dialog box under Windows 7. Remote Server Administration Tools are enabled/disabled from here.