File System Support
    <namespace>0</namespace>
<last_edited>2017-01-12T18:54:10Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

===============================

Introduction
===============================

To set up shares with extended access control list (ACL) support, the file system hosting the share must have the user and system ``xattr`` name space enabled. On a Samba Active Directory (AD) domain controller (DC), ``samba-tool`` verifies this setting automatically for the file system the Sysvol share is created on.

=================
ext4
=================

fstab 
------------------------

You do not have to modify /etc/fstab , ext4 uses all the required options by default.

Kernel support 
------------------------

Ensure that your kernel has the following options enabled:

 CONFIG_EXT4_FS_SECURITY=y
 CONFIG_EXT4_FS_POSIX_ACL=y


=================
ext3
=================

fstab 
------------------------

For filesystems of that type shared by Samba, add the following options to your /etc/fstab:

 /dev/...          /srv/samba/demo          ext3          <u>user_xattr,acl,barrier=1</u>          1 1

Note: The ``barrier=1`` option ensures that tdb transactions are safe against unexpected power loss.

Please be careful modifying your fstab. It can lead into an unbootable system!

Kernel support 
------------------------

Ensure that your kernel has the following options enabled:

 CONFIG_EXT3_FS_SECURITY=y
 CONFIG_EXT3_FS_POSIX_ACL=y

=================
XFS
=================

fstab 
------------------------

No special mount options are required in your fstab.

Kernel support 
------------------------

Ensure that your kernel has the following options enabled:

 CONFIG_XFS_POSIX_ACL=y

=================
File systems without xattr support
=================

**Note: This is not recommended!!!**

If you don't have a filesystem with xattr support, you can simulate it by adding the following line to your smb.conf:

   posix: adb = /usr/local/samba/private/eadb.tdb

This will place all extra file attributes (NT ACLs, DOS EAs, streams, etc), in that tdb.

**Note: This way it is not efficient and doesn't scale well. That's why it shouldn't be used in production!**

=================
Testing your filesystem
=================

*Note: This is not required for `#File_Systems_without_xattr_support|file systems without xattr support`.*

Before you start testing, ensure, that you have the ``attr`` package installed!

Run the following commands as root to test xattr support:

 # touch test.txt
 # setfattr -n user.test -v test test.txt
 # setfattr -n security.test -v test2 test.txt

The following commands should return the shown output:

 # getfattr -d test.txt
 # file: test.txt
 user.test="test"

 # getfattr -n security.test -d test.txt
 # file: test.txt
 security.test="test2"

Run the following commands as root to test extended ACL support:

 # touch test.txt
 # setfacl -m g:adm:rwx test.txt

The following command should return the shown output:

 # getfacl test.txt
 # file: test.txt
 # owner: root
 # group: root
 user::rw-
 group::r--
 group:adm:rwx
 mask::rwx
 other::r--

If an ``Operation not supported`` error is displayed:
* your kernel is not configured correctly,
* or your file system is not mounted using the correct options, or
* you did not ran the commands using the ``root`` user.