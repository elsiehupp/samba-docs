File:Sambaupdatecheck.txt
    <namespace>File</namespace>
<last_edited>2015-03-09T11:57:31Z</last_edited>
<last_editor>Mpreissner</last_editor>

Script to compare currently installed version of Samba (compiled from source) to the latest stable version available on www.samba.org.  Sends email notification if a newer version is available.