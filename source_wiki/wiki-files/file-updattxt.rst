File:Updatesamba.txt
    <namespace>File</namespace>
<last_edited>2015-03-09T11:59:29Z</last_edited>
<last_editor>Mpreissner</last_editor>

Script to download, compile, and install newest version of Samba over existing compiled-from-source installation.  Sends email notification when complete.