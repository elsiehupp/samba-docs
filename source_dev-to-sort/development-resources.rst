Development Resources
    <namespace>0</namespace>
<last_edited>2019-05-13T03:36:17Z</last_edited>
<last_editor>Garming</last_editor>

==============================

icrosoft Documentation
===============================

WSPP Documentation
------------------------

As a result of the EU case, the Samba team [http://www.samba.org/samba/PFIF/ gained access] to Microsoft's documentation of the protocols it uses.  This program is known as WSPP.

In March 2008, this documentation was made public, and is now available at http://msdn.microsoft.com/en-us/library/cc197979.aspx

WINS Documentation
------------------------

[http://www.microsoft.com/technet/archive/winntas/plan/winswp.mspx?mfr=true Planning WINS deployments in NT4]

[ftp://ftp.microsoft.com/bussys/winnt/winnt-docs/papers/WINSWP.doc WINS White Paper]

[http://msdn.microsoft.com/en-us/library/ms810878.aspx Microsoft Windows NT Server 4.0 WINS: Architecture and Capacity Planning]

===============================
Samba Team Documents
===============================

Thesis Documents
------------------------

[http://www.samba.org/samba/news/articles/abartlet_thesis.pdf Andrew Bartlett's thesis] on Samba4 and AD .

[http://samba.org/~metze/presentations/2007/thesis/ Stefan (metze) Metzmacher's thesis], covering the internal Samba architecture as it surrounds AD replication in particular. 

Other documents
------------------------

[http://samba.org/ftp/unpacked/samba_4_0_test/source/auth/kerberos/kerberos-notes.txt Notes on how Samba4 uses kerberos, and some of it's requirements]

Mailing list discussions
------------------------

An extensive [http://lists.samba.org/archive/samba-technical/2005-November/044119.html discussion on kerberos and Samba4] looking at how and why Samba4 uses it's own KDC, and why we can't just be 'normal'. 

Another [http://lists.samba.org/archive/samba-technical/2008-June/059553.html discussion on Samba4 kerberos architecture] 

These are listed so that hopefully, in future the next rounds of such discussions don't have to go over the same ground. 

Source code tools
------------------------

The [http://build.samba.org Samba Build farm] tests Samba across a number of platforms, and provides code coverage statistics on our test-suite (now obsolete).