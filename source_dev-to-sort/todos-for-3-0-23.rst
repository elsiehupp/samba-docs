TODOs for 3.0.23
    <namespace>0</namespace>
<last_edited>2007-02-26T01:44:35Z</last_edited>
<last_editor>WhitÐµcraig</last_editor>

There are a lot of upcoming changes:

* Convert from hand coded MS-RPC marshalling/unmarshalling to using the libndr layer from the Samba 4 branch. `Samba3/libndr|Partially done`.
* Fix winbind offline mode bugs.
* Add AD Site support to winbindd.
* Addition of --with-dnsupdate for DDNS registration during domain join