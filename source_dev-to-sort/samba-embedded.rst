Samba Embedded
    <namespace>0</namespace>
<last_edited>2009-09-15T20:50:32Z</last_edited>
<last_editor>Kai</last_editor>

This page is currently work in progress, but will contain information on building Samba for embedded devices.

=================
Current Status
=================

So far neither Samba 3 nor Samba4 are optimized for embedded systems.

=================
Work Items
=================

This is a loose collection of work items that will make Samba easier to run on embedded systems.

Modular Design 
------------------------

It should be possible to only build required parts of Samba at compile time. will be a major pice of work, likely multiple man months..

UClib compilation 
------------------------

I have not tried this yet.