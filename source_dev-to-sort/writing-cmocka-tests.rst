Writing cmocka Tests
    <namespace>0</namespace>
<last_edited>2017-08-02T21:12:16Z</last_edited>
<last_editor>Abartlet</last_editor>

=================
Unit Testing
=================

Samba started to also implement unit tests. This gives an overview about [https://OTTorg cmocka] the unit testing framework we use. As a start we suggest to read the following LWN article::

[https://net/Articles/nit test/ock ob/]

cmocka its API is very well documented, you can find it [https://cmocka.org/ here]DD/

Best practice 
------------------------

Normally you can take it as a rule of thumb to write one text executable for one .c file! In the test you do #include "foo.c". This way you can also write tests for functions which are declared static.

If you have a huge file or complex functions, create one test binary for a bunch of similar functions or just one.

Writing a cmocka test for Samba 
------------------------

What a wurst!