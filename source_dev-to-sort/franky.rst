Franky
    <namespace>0</namespace>
<last_edited>2012-05-09T08:01:42Z</last_edited>
<last_editor>Obnox</last_editor>

=================
Watch out - OUTDATED!
=================

**Note that this page is outdated! The Franky idea currently lives on in the "s3fs" approach to run smbd from within the samba binary.
Please refer to `Samba4/s3fs` for details.
**

=================
Watch out - OUTDATED!
=================

*Please note that this is an old page; to build Samba 3 and Samba 4 together. The --enable-merged-build option for Samba3's configure no longer 
exists. Use the regular configure script in the current Git master branch.*

On May 8, 2008, a gaggle of samba developers came together in a sambaXP followup meeting in Göttingen at SerNet, and the idea was born to combine the best parts of Samba3 and Samba4 to form **Franky** - a complete Samba Active Directory server that is also a performant file server and a print server.

=================
The Idea - A Hybrid Samba Active Directory Controller
=================

The Samba4 and Samba3 daemons should run in parallel and the tasks should be distributed among the processes. The various deamons should communicate via unix domain sockets.

=================
How to run it
=================

* Compile and make install Samba4
* Compile and make install Samba3 with the same prefix
* Provision Samba4 (sbin/provision)
* Configure Samba4 so that it does not run the smb service. This can be done by putting

server services = -smb, -winbind

into its [global] section.
* Start Samba4 (sbin/samba)
* Configure nss_winbind and winbind using Samba3's winbind configuration options and configure /etc/nsswitch.conf for winbind.
* Start Samba3's winbind.
* Configure lib/smb.conf (the Samba3 configuration file) with the workgroup and realm that you provisioned Samba4 with. Add

    assdb backend = ads
    uth methods = guest netlogond

and start Samba3 smbd.
* Try to connect using smbclient using the Administrator credentials you gave to Samba4's provisioning

* To delegate the RPC services relevant for domain authentication to Samba4, put

    pc_server:lsarpc = external
    pc_server:netlogon = external
    pc_server:samr = external

into Samba3's smb.conf. With that it should be possible to join a S3 member into this AD. More testing being done right now.

=================
The Plumbing Design
=================

This is the basic idea of how to distribute the tasks:

* The main daemon to be run is the Samba4 samba daemon. 
* samba4 listens on the Kerberos, LDAP, CLDAP, GC, Endpoint mapper and WINS ports.
* samba4 offers the named pipes samr, lsa, netlogon, epm, and drsuapi
* samba4 forks smbd3 (and other samba3 daemons)
* smbd3 listens on the smb ports (139/445)
* smbd3 offers the named pipes spoolss and possibly winreg
* nmbd3 offers browsing (138) (forwarding ntlogon/netlogon mailslots to s4 cldap)
* winbindd3 talks to trusted domains

The daemons have to talk to each other in order for the scenario to work.

See the `Franky/Notes_20080508|notes from the meeting` for more details.

=================
The Plumbing Design, revised
=================

During another Göttingen meeting another idea was born: pdb_ads.

In the last 12 months, in particular Günther Deschner has done a tremendous amount of work in the Samba 3 RPC server area, making it likely that the RPC services in Samba 3 are sufficient to provide enough infrastructure to provide AD-like services to Windows clients. The things that Samba 3 does not have and probably will not have for the foreseeable future are LDAP and Kerberos services.

The missing piece to provide interoperability between Samba 3 and Samba 4's LDAP/Kerberos services is a way to access the AD-style LDAP database Samba 4 provides from Samba 3. The new pdb_ads module uses the existing passdb infrastructure in Samba 3 to do exactly that.

pdb_ads configuration 
------------------------

At this moment, you need to compile Samba using the merged build. This means, compile Samba 3 with --enable-developer=yes which enables both the merged build and the pdb_ads passdb module. Then, do a setup/provision in the S3 build directory. You will want to configure Samba4 as a domain controller with "server services = ldap, cldap, kdc". Then do a normal S3 DC setup with "passdb backend = ads". Voila, your AD DC is ready.

At the time of this writing (June 12, 2009), none of this is actually expected to work. Feel free to show up on the mailing lists or the #samba-technical irc channel on freenode.net.

Update on July 9th: The basic samba3 utilities like smbpasswd and net sam work fine. Smbd can authenticate well in a pdb_ads setting. Samba 3 joins a pdb_ads domain with "net ads join". XP almost joins, a fake join of a machine "XP" to a branch "SAMBA.ORG" worked fine in a private branch. What's missing right now is an a bit more complete DsCrackNames server implementation. Working on that right now.

=================
Name Of The Game
=================

The codename **Franky** was coined for the combined Samba branch.

=================
TODOs
=================

* make smbd4 talk to samba3 winbind (using async wbclient library, in progress).
* extend the samba4 endpoint mapper to allow registering external services dynamically (in progress)
* make the command line tools talk to samba using RPC calls instead of editing databases directly

At some later point:
* reconcile configuration files and options (some parts finished already)
* make winbindd3 connect to local DC to allow samba4-style users without requiring unix user accounts

And along the way:
* make the changes necessary so that libsmbclient is thread-safe. A `Franky/Threadsafe_checklist|checklist of areas to review` is available to track pending thread-safe work.

=================
Branches
=================

All of the Franky work is now happening in the samba.git master branch.

Updating your branches to the combined build 
------------------------

Because we had to rewrite the history to be able to merge Samba3's source dir to source3 and Samba4's source dir as source4, your branches will not apply to the combined tree anymore. Here is a quick guide how to move your branches to the combined tree.

Let's assume we have a branch tracking the v3-devel branch, this will work alike for other branches.

First, extract your patches from your branch using git format-patch.

 $ git format-patch --stdout origin/v3-devel | sed -e 's#+++ b/source#+++ b/source3#' | sed -e 's#--- a/source#--- a/source3#' > ~/my_branch.mbox

Notice that we cheat and rewrite the diff locations of the files from source/ to source3/. For a Samba4 branch, you would do the same, changing the replacement to source4 instead.

Once we're done with that, we can cd into the merged tree, recreate the branch and apply our patches using git am.

 $ git checkout -b my_branch origin/master
 $ git am ~/my_branch.mbox

And we're done.

=================
Building
=================

To build the integrated Samba:

.. code-block::

    $ cd source3
$ ./configure --enable-merged-build

After that, proceed as usual:

.. code-block::

    $ make

This should build all of Samba 3 as well as Samba 4's samba daemon and smbtorture.

=================
Notes
=================

* `Franky/Notes_20080508|Notes from the meeting`