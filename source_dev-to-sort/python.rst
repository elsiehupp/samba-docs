Python
    <namespace>0</namespace>
<last_edited>2018-01-26T07:24:22Z</last_edited>
<last_editor>Abartlet</last_editor>

`Python/Debugging|Debugging`

=================
Available modules
=================

LDB 
------------------------

LDB is the database engine used in Samba4. For further information see [http://ldb.samba.org/].

Installing in debian/ubuntu:

 apt-get install python-ldb

tdb 
------------------------

samba.registry 
------------------------

samba.param (loadparm) 
------------------------

samba.credentials 
------------------------

samba.tests 
------------------------

`Writing_Python_Tests|Tests written in Python` should be under this namespace.  This includes both unit and integration tests.

samba.dcerpc 
------------------------

All RPC protocols have python bindings avaiable generated by `PIDL` and exposed as **samba.dcerpc.*protocol***

... 
------------------------

There is some other modules, we need to describe them there.

===============================
Python3
===============================

Samba and the libraries `TDB` [https://talloc.samba.org talloc] `LDB|ldb` have both Python and `Python3` modules.

More detail on the progress to shipping `Python3|Samba with Python3` is on that page