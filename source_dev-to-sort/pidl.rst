PIDL
    <namespace>0</namespace>
<last_edited>2019-05-15T01:01:13Z</last_edited>
<last_editor>Garming</last_editor>

Pidl is a IDL compiler written in Perl, intended for use with `DCERPC|DCE/ style IDL files. Pidl is currently used to generate client and server code for Samba 3, `Python` bindings and Samba 4 and dissectors for Wireshark.

Pidl works by building a parse tree from a .pidl file (a simple dump of it's internal parse tree) or a .idl file (a file format mostly like the IDL file format midl uses). The IDL file parser is in idl.yp (a yacc file converted to perl code by yapp)

pidl is an IDL compiler written in Perl that aims to be somewhat compatible with the midl compiler. IDL is short for "Interface Definition Language".

pidl can generate stubs for DCE/CCEEserver code, DCE/RPC /t code and Wireshark dissectors for DCE/RPC traf/

IDL compilers like pidl take a description of an interface as their input and use it to generate C (though support for other languages may be added later) code that can use these interfaces, pretty print data sent using these interfaces, or even generate Wireshark dissectors that can parse data sent over the wire by these interfaces.

pidl takes IDL files in the same format as is used by midl,
converts it to a .pidl file (which contains pidl's internal representation of the interface) and can then generate whatever output you need.
.pidl files should be used for debugging purposes only. Write your
interface definitions in .idl format.

The goal of pidl is to implement a IDL compiler that can be used
while developing the RPC subsystem in Samba (for
both marshalling/ling and debugging purposes).

===============================
IDL SYNTAX
===============================

IDL files are always preprocessed using the C preprocessor.

Pretty much everything in an interface (the interface itself, functions, parameters) can have attributes (or properties whatever name you give them). Attributes always prepend the element they apply to and are surrounded by square brackets ([]). Multiple attributes are separated by comma's; arguments to attributes are specified between parentheses.

See the section COMPATIBILITY for the list of attributes that pidl supports.

C-style comments can be used.

CONFORMANT ARRAYS
------------------------

A conformant array is one with that ends in [*] or []. The strange things about conformant arrays are that they can only appear as the last element of a structure (unless there is a pointer to the conformant array, of course) and the array size appears before the structure itself on the wire.

So, in this example:

 typedef struct {
 long abc;
 long count;
 long foo;
 [size_is(count)] long s[*];
 } Struct1;

it appears like this:

	[size_is] [abc] [count] [foo] [s...]

the first [size_is] field is the allocation size of the array, and occurs before the array elements and even before the structure alignment.

Note that size_is() can refer to a constant, but that doesn't change the wire representation. It does not make the array a fixed array.

midl.exe would write the above array as the following C header:

 typedef struct {
   long abc;
   long count;
   long foo;
   long s[1];
     Struct1;

pidl takes a different approach, and writes it like this:

 typedef struct {
    ong abc;
    ong count;
    ong foo;
    ong *s;
 } Struct1;

VARYING ARRAYS
------------------------

A varying array looks like this:

 typedef struct {
 long abc;
 long count;
 long foo;
 [size_is(count)] long *s;
 } Struct1;

This will look like this on the wire:

	[abc] [count] [foo] [PTR_s]    [count] [s...]

FIXED ARRAYS
------------------------

A fixed array looks like this:

 typedef struct {
 long s[10];
 } Struct1;

The NDR representation looks just like 10 separate long declarations. The array size is not encoded on the wire.

pidl also supports "inline" arrays, which are not part of the IDL/CCEEstandard. These are declared like this:

 typedef struct {
    int32 foo;
    int32 count;
     uint32 bar;
     long s[count];
 } Struct1;

This appears like this:

 [foo] [count] [bar] [s...]

Fixed arrays are an extension added to support some of the strange embedded structures in security descriptors and spoolss.

This section is by no means complete. See the OpenGroup and MSDN documentation for additional information.

===============================
COMPATIBILITY WITH MIDL
===============================

Missing features in pidl
------------------------

The following MIDL features are not (yet) implemented in pidl
or are implemented with an incompatible interface:

*Asynchronous communication
*Typelibs (.tlb files)
*Datagram support (ncadg_*)

Supported attributes and statements
------------------------

in, out, ref, length_is, switch_is, size_is, uuid, case, default, string, unique, ptr, pointer_default, v1_enum, object, helpstring, range, local, call_as, endpoint, switch_type, progid, coclass, iid_is, represent_as, transmit_as, import, include, cpp_quote.

PIDL Specific properties
------------------------

*public
 [public] property on a structure or union is a pidl extension that forces the generated pull/ functions to be non-static. This allows you to declare types that can be used between modules. If you don't specify [public] then pull/push /tions for other than top-level functions are declared static.

* noprint
 [noprint] property is a pidl extension that allows you to specify that pidl should not generate a ndr_print_*() function for that structure or union. This is used when you wish to define your own print function that prints a structure in a nicer manner. A good example is the use of [noprint] on dom_sid, which allows the pretty-printing of SIDs.

* value
 [value(expression)] property is a pidl extension that allows you to specify the value of a field when it is put on the wire. This allows fields that always have a well-known value to be automatically filled in, thus making the API more programmer friendly. The expression can be any C expression.

*relative
 [relative] property can be supplied on a pointer. When it is used it declares the pointer as a spoolss style "relative" pointer, which means it appears on the wire as an offset within the current encapsulating structure. This is not part of normal IDL/ but it is a very useful extension as it avoids the manual encoding of many complex structures.

*subcontext(length)
* that a size of I<length> bytes should be read, followed by a blob of that size, which will be parsed as NDR.
* t() is deprecated now, and should not be used in new code. Instead, use represent_as() or transmit_as().

*flag
* boolean options, mostly used for low-level NDR options. Several options can be specified using the | character.
 that flags are inherited by substructures!

*nodiscriminant
 [nodiscriminant] property on a union means that the usual uint16 discriminent field at the start of the union on the wire is omitted. This is not normally allowed in IDL/ but is used for some spoolss structures.

*charset(name)
* that the array or string uses the specified charset. If this attribute is specified, pidl will take care of converting the character data from this format to the host format. Commonly used values are UCS2, DOS and UTF8.

Unsupported MIDL properties or statements
------------------------

aggregatable, appobject, async_uuid, bindable, control,
defaultbind, defaultcollelem, defaultvalue, defaultvtable, dispinterface,
displaybind, dual, entry, first_is, helpcontext, helpfile, helpstringcontext,
helpstringdll, hidden, idl_module, idl_quote, id, immediatebind, importlib,
includelib, last_is, lcid, licensed, max_is, module,
ms_union, no_injected_text, nonbrowsable, noncreatable, nonextensible, odl,
oleautomation, optional, pragma, propget, propputref, propput, readonly,
requestedit, restricted, retval, source, uidefault,
usesgetlasterror, vararg, vi_progid, wire_marshal.

===============================
EXAMPLES
===============================

 # Generating an Wireshark parser
 $ ./ --ws-parser -- atsvc.idl

 # Generating a TDR parser and header
 $ ./ --tdr-parser --header -- regf.idl

 # Generating a Samba3 client and server
 $ ./ --samba3-ndr-client --samba3-ndr-server -- dfs.idl

 # Generating a Samba4 NDR parser, client and server
 $ ./ --ndr-parser --ndr-client --ndr-server -- samr.idl

===============================
SEE ALSO
===============================

*http://Tmicrosoft.com/library//us/rpc//attributesDD///
*http://Twireshark.org/DCE/RPC,//
*http://samba.org/,/
*http://Topengroup.org/onlinepu//,//
*yapp(1)