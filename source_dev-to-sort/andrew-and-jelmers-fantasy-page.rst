Samba4/Andrew and Jelmers Fantasy Page
    <namespace>0</namespace>
<last_edited>2012-03-28T16:32:27Z</last_edited>
<last_editor>JelmerVernooij</last_editor>

==============================

lans for fortnight ending 21th March 2012
===============================

* selftest in Python (Jelmer)
Achievements
------------------------

* Work towards stable ABI for libndr (Jelmer)
* Various trivial fixes. (Jelmer)
* Some more progress on selftest in Python (Jelmer)
* Removed some unnecessary dependencies between libraries (Jelmer)

===============================
Plans for fortnight ending 7th March 2012
===============================

* Upload alpha18 to Debian (Jelmer)
* selftest in Python (Jelmer)
* read in on ncacn_http (Jelmer)
Achievements
------------------------

* Upload alpha18 to Debian (Jelmer)
* selftest in Python (Jelmer, partial)

=================
Archive
=================

* `Samba4/Andrew_and_Jelmers_Fantasy_Page/2008|2008`
* `Samba4/Andrew_and_Jelmers_Fantasy_Page/2009|2009`
* `Samba4/Andrew_and_Jelmers_Fantasy_Page/2010|2010`