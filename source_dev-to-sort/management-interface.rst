Management Interface
    <namespace>0</namespace>
<last_edited>2014-06-02T02:27:57Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

Introduction 
------------------------

the current `Samba3` implementation lacks a defined interface for managing sessions, shares, printers or parameters.
Each third party management software has to parse and write the smb.conf file on it's own. Many important informations 
can only be gathered by parsing the output of tools like smbstatus.

Today, Samba management looks like this:

`Image:management1.jpg`

Management library / RPC Interface 
------------------------

`Samba3` already has the concept of RPC pipes.A lot of administrative functions are already implemented.
* A possible solution would be to backport the Samba4 libnet library to `samba3` and use it for management stuff.

`Samba3` management with a central library (like libnet):

`Image:management2.jpg`

Management tasks == 
to group the various admin functions we should work on a scope definition for the 'day to day' admin jobs.
**Here is a first proposal :**

Process control
------------------------

* start,stop,restart services (smbd,winbindd,nmbd)
* kill sessions

Status & Debugging
------------------------

* open sessions
* open files (per user/per share)
* locking infos
* connected users
* logs & events (read/receive)
* health (e.g. winbind -p)
* smbcontrol (set debug level ...)

Configuration
------------------------

* global smb.conf parameters (get/set/list)
* share mgmt (create/delete/list)
* share acl (get/set)
* share parameters (get/set/list)
* printer mgmt (create/delete/list)
* mapping of synonyms (guest only=only guest)
* DFS roots / links 
* handling of static include files
* variable path to config file

Performance
------------------------

* CPU
* Memory (per user)
* Disk
* Network usage
* profiling data
* system load

User/Group Mgmt === 
* List user/groups
* Add  user/groups
* Delete user/groups
* group memberships (list,change)