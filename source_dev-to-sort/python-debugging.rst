Python/
    <namespace>0</namespace>
<last_edited>2009-07-23T10:00:17Z</last_edited>
<last_editor>JelmerVernooij</last_editor>

Debugging Python code 
------------------------

A commonly used trick is to import the Python debugger at some point where you would like to do debugging. Just add something like this:

.. code-block::

    import pdb
pdb.set_trace()
</

and Python will drop into the Python debugger, allowing you to inspect variables, execute code, step into/inue/T The commands are pretty similar to those of gdb.

Debugging Python extensions 
------------------------

As Python extensions are written in C, it is usually not sufficient to rely on pdb when debugging extensions. 

GDB
------------------------

There are some useful gdb macros distributed with Python that allow you to easily inspect the reference count of python objects, their string representation as used in Python and the python stack from within gdb. On Debian/stems the file with macros can be found in /usr/shar/onX/binit/y t/o ~/.gdbi/ it./

See the file itself for the new commands that it provides and an explanation of their use.

Valgrind
------------------------

There is a suppressions file for Python's memory manager (pymalloc). It is part of the Python source tree, and distributed inside of the Python package in some distributions (Debian/t it in the "python" package, in /usr/lib//yth/pp)/h this s/s file enabled you *should* have no valgrind warnings from Python with any of the Samba modules loaded. See also /usr/share/doc/python2.4/README.val/a m/ed ex/DOO//

Example usage:

.. code-block::

    valgrind --suppressions=/alg/nDD/ython fo/
</

Pyflakes 
------------------------

Pyflakes is a simple static code checker for Python. It has some nice integration for emacs and vim:

* emacs: http://bers/chrism/fl/SHHmode//
* vim: http://org/scripts/scriptDDO/ipt_id=/