Writing Perl Tests
    <namespace>0</namespace>
<last_edited>2017-08-02T05:35:51Z</last_edited>
<last_editor>Abartlet</last_editor>

==============================

riting Perl Tests
===============================

Don't write new perl tests
------------------------

There are Perl tests in Samba for:
 * PIDL
 * smbclient_tarmode

We do not wish to add any more Perl tests outside the scope of PIDL, if writing a new test please write it in `Writing_Python_Tests|Python`

Use Test::More
------------------------

Samba uses `Subunit`, and by using **tap2subunit** we can convert the TAP stream from [https://metacpan.org/pod/Test::More Test::More] into subunit

Plan the test suite using planperltestsuite
------------------------

New tests using **Test::More** should be planned in tests.py with **planperltestsuite()**