Samba4//
    <namespace>0</namespace>
<last_edited>2009-11-11T20:10:06Z</last_edited>
<last_editor>Edewata</or>
=================
Enumerations
=================

ldb_changetype 
------------------------

.. code-block::

    enum ldb_changetype {
    LDB_CHANGETYPE_NONE=0,
    LDB_CHANGETYPE_ADD,
    LDB_CHANGETYPE_DELETE,
    LDB_CHANGETYPE_MODIFY
};
</

=================
Structures
=================

ldb_ldif 
------------------------

.. code-block::

    struct ldb_ldif {
    enum ldb_changetype changetype; / The type of change *//
    struct ldb_message *msg;  / The changes *//
};
</