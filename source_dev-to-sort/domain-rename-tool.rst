Domain rename tool
    <namespace>0</namespace>
<last_edited>2019-07-28T11:20:16Z</last_edited>
<last_editor>Hortimech</last_editor>


Background 
------------------------

{{Imbox
| type = warning
| text = Although Samba has a domain rename tool, it currently does not support renaming a production domain for long-term use.

The tool is intended to handle two specific use cases:
* Running a temporary alternate domain, in the event of a catastrophic failure of the regular domain. The idea is you can switch over to the alternate/renamed domain with minimal effort. You can then run the two domains at the same time, without them interferring with each other. The renamed/alternate domain provides the core Samba network services, while the original domain DCs can be trouble-shooted.
* `Create a samba lab-domain|Producing a realistic lab domain`, i.e. you rename the domain and scrub its (at this point in time, only the most critical) secrets to create a pre-production environment for testing.

The domain rename is done in two steps:
* samba-tool domain backup rename: this takes a clone of a running DC's database, renames it in the process, and produces a backup file.
* samba-tool domain backup restore: this takes the backup file and populates the files needed to run a fresh new samba DC.

The rename is implemented this way because the mechanics behind the domain rename are very similar to `Back up and Restoring a Samba AD DC|taking an online backup` of a domain. Renaming of the domain is done during the step of cloning a new database in order to minimize consistency errors and to perform a more efficient and effective rename.

Example commands 
------------------------

To run the rename command, you need to specify the new domain NetBIOS name (e.g. 'NEWDOMAIN' below), the new DNS realm for the domain (e.g. 'new.renamed.com'), and specify a running DC in the domain you want to clone. This command will create a backup-file, which will get placed in the targetdir you specified. The command would look something like:

.. code-block::

    sudo samba-tool domain backup rename NEWDOMAIN new.renamed.com --server=<DC> --targetdir=<backup-dir> -UAdministrator

The backup file generated will have a name like <backup-dir>/samba-backup-new.renamed.com-<timestamp>.tar.bz2. This file gets passed into the next step, which is restoring the cloned/renamed database back to disk. The restored domain will have a single new DC (whose name is specified by --newservername). Other DCs can subsequently be joined to the new DC to repopulate the samba network. The targetdir specifies the new location for all the samba files (smb.conf, sam.ldb, etc). The restore command would look something like:

.. code-block::

    sudo samba-tool domain backup restore --newservername=<new-DC-name> --targetdir=<restore-dir> --backup-file=<backup-file>

These commands generally need to be run as root in order to preserve/restore the NTACLs of the sysvol files correctly, as well as the UID of other internal files that samba uses.

Before starting ``samba``, check the restored smb.conf to make sure its settings are correct. You may need to manually update some settings. 

Note that when ``samba`` starts, it runs ``samba_dnsupdate`` automatically. This may throw up a few error logs initially as it updates the DNS records.

What changes in a rename 
------------------------

The rename command takes a clone of an existing domain. So only the cloned domain is modified - the original domain is not touched at all.

The cloned Samba database is modified so that the new domain will have a different NetBIOS name and a different DNS realm. The following are modified:
* Every single DN.
* The domain partition object and its NetBIOS name.
* The DNS zone objects, as well as the dnsRoot attributes.

Note that the backup-file still contains all the DCs from the original domain. When the restore command is run, a new fresh DC is added and the old DCs are removed. By the time the new DC is added, the domain has been completely renamed, so the new DC ends up with the correct dnsHostname/servicePrincipalName values automatically.

What doesn't change 
------------------------

userPrincipalName
------------------------

The userPrincipalName is not updated - if this has been set previously, then it will still use user@old-realm. If you use the --keep-dns-realm option when renaming the domain, then users can still login using their full userPrincipalName name. Otherwise, they can login using user@new-realm (providing the Kerberos configuration handles the new realm).

GPOs
------------------------

The GPO attributes (i.e. gPCFileSysPath and gPLink) aren't updated at all. The sysvol files for the backed-up domain are kept, but these retain the old realm's filepath, e.g. sysvol/samba.example.com/Policies. A default sysvol directory is also created using the new realm's filepath, e.g. sysvol/new.renamed.com/Policies.

Updating the GPOs to handle the change in domain name is not a trivial process. The best way to handle a long-term domain rename would be to export the GPO files manually and then re-import them. In the short-term the renamed domain should be able to continue using GPOs that refer to the old realm. 

`Category:Backup` `Category:Active Directory`