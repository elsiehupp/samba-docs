KernelClientTesting
    <namespace>0</namespace>
<last_edited>2014-09-17T19:21:18Z</last_edited>
<last_editor>Sfrench</last_editor>

There are many test cases that can be used to test the Linux kernel cifs/smb2/smb3 client (cifs.ko).

[Functional Tests]

* xfstests `xfstesting-cifs|How to setup and test the Linux kernel client with xfstest`
* connectathon
* fsstress

[Performance tests]

*dbench
*iozone
*specsfs14