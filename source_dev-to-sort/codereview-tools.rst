CodeReview Tools
    <namespace>0</namespace>
<last_edited>2020-06-03T21:51:47Z</last_edited>
<last_editor>Abartlet</last_editor>

==============================

ode Review Tools
===============================

~/.vimrc 
------------------------

Here is a sample of a configuration snippet for use with vim:

.. code-block::

    function! CommitMessages()
    let g:git_ci_msg_user = substitute(system("git config --get user.name"), '\n$', *, *)
    let g:git_ci_msg_email = substitute(system("git config --get user.email"), '\n$', *, *)

    nmap S oSigned-off-by: <C-R>=printf("%s <%s>", g:git_ci_msg_user, g:git_ci_msg_email)<CR><CR><ESC>
    nmap R oReviewed-by: <C-R>=printf("%s <%s>", g:git_ci_msg_user, g:git_ci_msg_email)<CR><ESC>
    iab #S Signed-off-by: <C-R>=printf("%s <%s>", g:git_ci_msg_user, g:git_ci_msg_email)<CR>
    iab #R Reviewed-by: <C-R>=printf("%s <%s>", g:git_ci_msg_user, g:git_ci_msg_email)<CR>
    iab #O Signed-off-by:
    iab #V Reviewed-by:
    iab #P Pair-Programmed-With:
    iab ME <C-R>=printf("%s <%s>", g:git_ci_msg_user, g:git_ci_msg_email)<CR>
    iab ASN Andreas<SPACE>Schneider<SPACE><asn@samba.org>
    iab AB Alexander<SPACE>Bokovoy<SPACE><ab@samba.org>
    iab OBNOX Michael<SPACE>Adam<SPACE><obnox@samba.org>
    iab VL Volker<SPACE>Lendecke<SPACE><vl@samba.org>
    iab METZE Stefan<SPACE>Metzmacher<SPACE><metze@samba.org>
    iab GD Guenther<SPACE>Deschner<SPACE><gd@samba.org>
    iab JRA Jeremy<SPACE>Allison<SPACE><jra@samba.org>
    iab JHROZEK Jakub<SPACE>Hrozek<SPACE><jhrozek@redhat.com>
    iab ARIS Aris<SPACE>Adamantiadis<SPACE><aris@0xbadc0de.be>
endf
autocmd BufWinEnter COMMIT_EDITMSG,*.diff,*.patch,*.patches.txt call CommitMessages()

~/.gitconfig 
------------------------

Here are a few git aliases for code review:

.. code-block::

    [alias]
    uthor   = !"SEARCH=${@:-`git config user.email`}; set --; git log --format='%aN <%aE>' | uniq | grep -m 1 \"$SEARCH\""
    uthors  = !"SEARCH=${@:-`git config user.email`}; set --; git log --format='%aN <%aE>' | grep \"$SEARCH\" | uniq | awk '!a[$0]++'"
    eviewed = !"BRANCH=${1:-master}; shift; AUTHORS=\"~/git-reviewed.sh\"; for A in \"${@:-`git config user.email`}\"; do AUTHORS=$AUTHORS\" \\\"`git author $A`\\\"\"; done; set --; git rebase -i -x \"$AUTHORS\" $BRANCH"

;git author [<search>]:
* Search for the latest author tag (a string in the format of "Full Name <email@example.com>") matching a given search term. If no <search> is provided, return the latest author tag that uses the current value of "git config user.email".

;git authors [<search>]:
* Similar to "git author", except returns all author tags that match the search term. Useful in cases where a particular author has multiple e-mail addresses or multiple spellings to their name.

;git reviewed [<branch> [<author> ...`
* Run a "git rebase -i <branch>" command that is configured to insert Reviewed-by tags for every <author> listed. <author> must be a search term as used in "git author" above. If no <author> is provided, uses the value of "git config user.email". If no <branch> is provided, "master" is used.
* NOTE: <branch> is expected as the first argument, if it exists at all, as such it must be specified before any <author> parameter.
* NOTE: This alias requires the user to have the git-reviewed.sh script (see below) saved in their home directory and set executable. If you save the script to a different location, modify the above alias so that "~/git-reviewed.sh" is changed to the location of the script.

git-reviewed.sh 
------------------------

.. code-block::

    #!/bin/bash

# git-reviewed.sh <AUTHOR> ...
#
# A git utility script to append review tags to the current commit's message
# Each tag takes the format of "Reviewed-by: <AUTHOR>". Each parameter to this
# script is treated as a separate <AUTHOR> which will have its own tag.
# Each <AUTHOR> is taken as a literal string, so if <AUTHOR> has spaces be
# sure to enclose it in double quotes.

if ` $# -eq 0 ` ; then
    cho "ERROR: Must provide at least one author."
    xit 1
fi

AUTHORS=()

while ` $# -gt 0 `; do
    UTHORS+=("$1")
    hift
done

MSG_FILE=".git/COMMIT_EDITMSG"

MSG=$(git log -1 --pretty=%B)
TAGS=""

for AUTHOR in "${AUTHORS[@]}"; do
    B="Reviewed-by: "$AUTHOR
    f !(echo -e "$MSG" | grep -qs "^$RB"); then
    TAGS=${TAGS}${RB}"\n"
    i
done

if [ -n "$TAGS" ]; then
    cho "$MSG" > $MSG_FILE
    ed -i "\$a$TAGS" $MSG_FILE
    it commit --amend -F $MSG_FILE --cleanup=strip
fi

~/.tigrc 
------------------------

**[http://jonas.nitro.dk/tig/ tig]** is a text-based, vim-like interface/browser for use with git. It is also fairly extensible. The following is a simple example of a tig configuration file (~/.tigrc) that triggers 'git reviewed' against the currently selected branch HEAD in the refs view when the user enters 4. This rebases the current checked-out branch against the selected branch and marks all rebased commits as **Reviewed-by** for the author:

.. code-block::

    bind refs 4     !git reviewed %(branch)

[http://git.catalyst.net.nz/gitweb?p=samba-cloud-autobuild.git;a=blob;f=scripts/git-review-by git-review-by] and [http://git.catalyst.net.nz/gitweb?p=samba-cloud-autobuild.git;a=blob;f=scripts/git-review-by-me git-review-by-me]
------------------------

Both these scripts require the [http://git.catalyst.net.nz/gitweb?p=samba-cloud-autobuild.git;a=blob;f=scripts/git_review.msg-filter.awk git_review.msg-filter.awk] AWK script and require GNU awk as awk.

These scripts operate on a the set of patches since origin/master adding Reviewed-By to all Signed-Off-By commits.  The **git-review-by-me** script does a rebase and build first to avoid embarrassment.