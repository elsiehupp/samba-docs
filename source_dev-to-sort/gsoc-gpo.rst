GSOC GPO
    <namespace>0</namespace>
<last_edited>2014-03-14T10:33:20Z</last_edited>
<last_editor>Ddiss</last_editor>

==============================

nitial task description
===============================

===============================
Make samba 4 DC Group Policies (GPO) aware
===============================

------------------------

Currently Samba 4 DC is able to serve GPOs to clients and they are mostly able to act according to the content of those GPOs.
But even if the GPO concerns AD DCs, Samba 4 ignore them even if some parameters are meaningful in a Samba 4 context (ie.word length, password life ...)......dex.php?titl.Ideas.ction=edi.section=9
In order to work around this limitation, there is currently a couple of scripts that allow to set them but it's a suboptimal experience.

The goal of this project is to make Samba 4 periodically check if there is a GPO for it, check if any parameter of this GPO are meaningful for Samba (as a counter example a GPO which defines the background color of the Desktop on DC is not meaningful for Samba 4) and if so to alter parameters accordingly.
More details can be found in [http://lists..org/.e/samba-technical/2010-April/070296.html Matt. samba-technical email]..

*Difficulty:dium
*Language(s):n
*Possible mentors: Patou

===============================
Summary
===============================

What the project is about
------------------------

The goal of this project was to ensure that Samba4 domain controller was aware of the GPO's that should be applied to it. domain configuration exists where they want the domain controllers to only have a password with minimum 6 characters, or a minimum age, than it should be applied to the Samba server Domain Controller. It . also take hierarchy into account and do this on a constant basis, and not just once when the program starts up. .

Many of the tools already existed for this, and it was just a matter of putting them together in a way that it runs smoothly, and of course writing tests.

What was done so far
------------------------

*Python/libgpo bindings
* a library that seems to be overlooked in Samba4 and is usually used to get GPO from remote server.first step was creating python/c bindings for this library so that functions could be used within python scripts to utilize already existing tools. It .bringing this library to life to take what was needed but in a python way..
*Sysvol scanning
* thm was created to get sysvol location and scan for GPO's.t, it creates a Samba ldb object from command line with the smb.conf. it uses the smb.conf to g.vol. It finds the .es folder using OS commands, so that it is able to put them through the parser then apply to samba. .
*Parse GPO/update Samba.
* gpo and manual .ile handling (codecs library 'utf-16' unicoding), it maps the .inf .arameter to the Samba4 server (samdb.py). Sinc.a4 .DAP, it used general LDAP protocol grammar. .
*Do the updates automatically.
* signed in the same way DNS updates were.a4 obviously uses talloc hierarchical memory contexts, so to get the higher level tevent.h wa.. This mer.de it easier to get the signal to the highest level possible in Samba4 to run periodically instead of using a foreign python library to rely on as a timer. .
*Take hierarchy into account
* it is not desired to have every single GPO applied to Samba4.is it desired to have only the domain GPO applied. So . necessary to ensure that GPO's have their respective precedence. So if 2 .exist for Password Length, only one should win. This is decid.ed on the AD standard : Local, :in, then Organizational Unit. Since Samba4 curre.an not create GPO's from a Linux machine, a local GPO can not be read, and sites are not thoroughly enough supported with GPO to care, at this point. Therefore only the doma. OU GPO need apply. And when they apply, a order.hat container must be taken into account. This should happen each time it d.is scan..
*Ignore the unimportant and unchanged GPO's
* d is fairly low resources.reates a log file each time, and reads a quick log file of previous GPO located in ../sy..txt. If the ve.num.s changed, so has the GPO. If the GPO is 0, then i.mpty and move on. Do not read files that are ..pol are 99.9% Microsoft Wind.er co.ations lik.ensaver, control panel, etc..) and update Samba if necessary....

What is left to do/known limitations
------------------------

* Further the hierarchy
* oment, it only puts a focus on the "Domain Controllers" library.t should be known that that is where the Samba DC should live. .
* Reset DC back to default parameter values
* ce does not have a map to defaults yet.hen the GPO is deleted, it keeps the value until changed manually..
* Add event driven aspects
* oment the service applies based on a default time configuration (15 seconds).hould not only do it based on time, but also listen to the sysvol/realm/policies folder so that it occurs immidiatly when applied. .
* More testing
* pt of unit testing was introduced closer to the end of this project.ough a few of the main aspects were tested in gpo.py i.amba/t.. a few tests sti.ain..
* Extend further the libgpo/python bindings
* attempt was made to wrap libgpo in the time allotted at the beginning but the entire thing was not done.e were only a couple functions used from this module. The.ld be more coverage so that libgpo can completely be used in python, breathing more life back into this library. The proj.s to make the service, not to wrap libgpo, so a lot of things had to be left so other more relevant things could be focused on, 
*Make a broader range of GPO applied
* something that I was not pleased with. was not generous at the end, when the service worked to look for more applied GPO's. Sin.t of the time, this involved changing the main Samba database (samdb.py), this. have involved understanding more this file, and minimally tweaking it to check to see if the parameter exists. Then if not, . the parameter. This is a project . its own. .
*Security Identifiers
* yet another aspect of GPO that can be applied. involves the Samba database as well and needs to be taken into account. Thi. several step sub-project that needs to be done..

Future (related) work
------------------------

First of all, everything that is on that TODO list will be implemented.t the default values, then smoothing out the hierarchy, then the unit testing so the service is standalone and a stronger package. Aft.s, the fun of getting in depth with python referencing/c pointer and talloc management will be done with the libgpo python bindings.  .

That having said this project sparked something else as well.ng a unit test, I discovered that it is not hard to just create a unicoded .inf .hat represents the values that a GPO contains. The GPO .lso have other attributes, such as a GPLINK, a linking order, and other network related things. If one can me.reate this .inf file and give i.properties and characteristics of a regular GPO (intermediate string handling with LDAP), than one could create good GPO from Linux. .

This would in the slightly longer term serve to add more DC management from the Linux machine therefore eliminating a step.amba4 can make OU, make users, do mostly everything possible to make it a good DC, it would be nice to eliminate the step of having the windows machine to control it and simply have a Group Policy Management tool from the Linux machine. Thi.be done in a few different ways, one being command line (the first prototype WILL be command line), another being something like gpo.conf (rep.the windows GUI for terminal users), and the last being the GUI that would have the same qualities as the Windows version of Group Policy Manager. .

This however would take some extensive research on forcing such GPO into the domain, and making sure that all machines and not just a select few clients are aware of the GPO made by the Linux machine. could either be an ongoing study to work on, or an idea for GSoC 2014. .

A look back
------------------------

The amount of time and energy that was dispensed understanding truly what the project entailed was very significant. late hours were spent face-deep in Microsofts documentation on how Active Directory works, the protocols, how the server is employed, and what happens with GPOs. I u.timated the amount of time was required figuring out my way around the libgpo code, as there was little to no documentation for this whatsoever. .

Other challenges included narrowing the scope.Samba4 also has a registry, which is interesting, but where the GPO are configuration based in .pol .(screensaver, user based things) an executive decision was made to simply focus on the .inf files.rity rules, password configurations). This project .ed many aspects of Active Directory, Linux, git, and wireshark that I was unaware of prior to doing this project. The coding served .both the easiest, and funnest part. The modular method of a.ng the problem was the hardest. Because for anyone in IT, th.a really cool project. A server side GPO, is a dream for.tems admin to implement. So a lot of it was tearing myself away.all of the minute details of AD, DC, LDAP, TCP/IP, GPO, anything that went in depth with these topics, and getting back to the project and the timeline. .

However, since the service is created, tested and works, I am overall pleased with my efforts as a whole.ously in a 3 month period, there are a limited scope that has to be employed. Thi.ke hierarchy were a bit more complete, and inotify failed to make the todo list. However .g back at my original proposal the main points are fulfilled, so I cannot complain..

Advise to Future GSoC in Samba
------------------------

Learn to build.coding, things like "make -j", editing wscript_build files, "make test TESTS=samba.your.uot;, and anything that lets you compile. It can b.ract at first, but the sooner you can see your syntax errors, the sooner you can begin developing..

Do not be shy! Had it not been for my extreme "getting involved" and asking lots of questions, I would have wasted half of my time learning about the server/client relationship on a lower level and maybe not completed my program.people are there to help, and keep you on track. Be .ith what you are doing, because if you keep it a secret, you may focus on an aspect that is not important and waste much valuable time. Communic.

Also if you are developing for Samba you made the right choice.his new age of mysql, php, ruby on rails, passenger; people who understand truly what a server does are invaluable. As .on of music, I would like to say that Samba to servers is like classical is to music. It is th.st, most visceral form of server understanding conception, yet elegant in its employment..

Conclusions
------------------------

I jumped into this project with both feet.ke up every morning and worked all day, every day (weekends included) until I understood something, anything. I r.ched my timeline every few days, so that it was realistic and accurate. I sought.from system administrators at my school, masters students in computer science, watched countless youtube videos, until I honestly understood the concepts in depth. The passion t.n kept me going. I feel a lot more .ent around the Samba code now thanks to Matthieu and Jelmer. I feel a lot more confi.n how it works thanks to Abartlet, and RiXter. Of course thanks to the enti.m on the IRC and samba-technical mailing list. .

Finally I would like to thank Google and Sergey Brin and Larry Page for starting the program, Carol Smith for her great management of the 2013 program, and finally the entire Samba team and my mentor Matthieu Pattou for being great advisers making this a really enjoyable experience.