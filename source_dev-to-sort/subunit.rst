Subunit
    <namespace>0</namespace>
<last_edited>2017-08-02T05:25:46Z</last_edited>
<last_editor>Abartlet</last_editor>

Samba extensively uses [http://launchpad.net/subunit Subunit] to describe test results. 

At the moment the main testrunner and the testsuites will output subunit. 

Samba uses subunit version 1.  We do not use subunit version 2, which is a binary protocol. 

There are a still things we would still like to achieve:

* Ability to run single tests (--load-list) in smbtorture
* Subunit formatting compatible with the main Subunit protocol