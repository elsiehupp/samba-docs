Samba4/LDB/Paged Searches
    <namespace>0</namespace>
<last_edited>2009-10-13T20:48:14Z</last_edited>
<last_editor>Edewata</last_editor>

=================
Source Code
=================

The source code is located at [http://gitweb.samba.org/?p=samba.git;a=blob;f=source4/lib/ldb/modules/paged_searches.c source4/lib/ldb/modules/paged_searches.c].

=================
Structures
=================

.. code-block::

    struct ps_context {
    struct ldb_module *module;
    struct ldb_request *req;

    bool pending;

    char **saved_referrals;
    int num_referrals;

    struct ldb_request *down_req;
};

=================
Methods
=================

ps_init() 
------------------------

This method checks the root DSE of the backend LDAP server to see whether it supports paged results control (1.2.840.113556.1.4.319).

ps_search() 
------------------------

This method intercepts each search request and adds paged results control.

ps_callback() 
------------------------

This method handles the search results and the paged results control.