Samba4/LDB/Map
    <namespace>0</namespace>
<last_edited>2009-10-13T20:44:47Z</last_edited>
<last_editor>Edewata</last_editor>

=================
Structures
=================

ldb_map_attribute 
------------------------

.. code-block::

    struct ldb_map_attribute {
    const char *local_name; /* local name */

    enum ldb_map_attr_type { 
        MAP_IGNORE, /* Ignore this local attribute. Doesn't exist remotely.  */
        MAP_KEEP,   /* Keep as is. Same name locally and remotely. */
        MAP_RENAME, /* Simply rename the attribute. Name changes, data is the same */
        MAP_CONVERT, /* Rename + convert data */
        MAP_GENERATE /* Use generate function for generating new name/data. 
            Used for generating attributes based on 
            multiple remote attributes. */
    } type;
	
    /* if set, will be called for search expressions that contain this attribute */
    int (*convert_operator)(struct ldb_module *, TALLOC_CTX *ctx, struct ldb_parse_tree **ntree, const struct ldb_parse_tree *otree);

    union { 
        struct {
            const char *remote_name;
        } rename;
		
        struct {
            const char *remote_name;

            /* Convert local to remote data */
            ldb_map_convert_func convert_local;

            /* Convert remote to local data */
            /* an entry can have convert_remote set to NULL, as long as there as an entry with the same local_name 
             * that is non-NULL before it. */
            ldb_map_convert_func convert_remote;
        } convert;
	
        struct {
            /* Generate the local attribute from remote message */
            struct ldb_message_element *(*generate_local)(struct ldb_module *, TALLOC_CTX *mem_ctx, const char *remote_attr, const struct ldb_message *remote);

            /* Update remote message with information from local message */
            void (*generate_remote)(struct ldb_module *, const char *local_attr, const struct ldb_message *old, struct ldb_message *remote, struct ldb_message *local);

            /* Name(s) for this attribute on the remote server. This is an array since 
             * one local attribute's data can be split up into several attributes 
             * remotely */
            const char *remote_names[LDB_MAP_MAX_REMOTE_NAMES];

            /* Names of additional remote attributes
             * required for the generation.	 NULL
             * indicates that `local_attr' suffices. */
            /*
#define LDB_MAP_MAX_SELF_ATTRIBUTES 10
            const char *self_attrs[LDB_MAP_MAX_SELF_ATTRIBUTES];
            */
        } generate;
    } u;
};

Map from local to remote attribute.

ldb_map_objectclass 
------------------------

.. code-block::

    struct ldb_map_objectclass {
    const char *local_name;
    const char *remote_name;
    const char *base_classes[LDB_MAP_MAX_SUBCLASSES];
    const char *musts[LDB_MAP_MAX_MUSTS];
    const char *mays[LDB_MAP_MAX_MAYS];
};

Map from local to remote objectClass.

ldb_map_context 
------------------------

.. code-block::

    struct ldb_map_context {
    struct ldb_map_attribute *attribute_maps;
    /* NOTE: Always declare base classes first here */
    const struct ldb_map_objectclass *objectclass_maps;

    /* Remote (often operational) attributes that should be added
     * to any wildcard search */
    const char * const *wildcard_attributes;

    /* ObjectClass (if any) to be added to remote attributes on add */
    const char *add_objectclass;

    /* struct ldb_context *mapped_ldb; */
    struct ldb_dn *local_base_dn;
    struct ldb_dn *remote_base_dn;
};

map_reply 
------------------------

.. code-block::

    struct map_reply {
    struct map_reply *next;
    struct ldb_reply *remote;
    struct ldb_reply *local;
};

map_context 
------------------------

.. code-block::

    struct map_context {

    struct ldb_module *module;
    struct ldb_request *req;

    struct ldb_dn *local_dn;
    const struct ldb_parse_tree *local_tree;
    const char * const *local_attrs;
    const char * const *remote_attrs;
    const char * const *all_attrs;

    struct ldb_message *local_msg;
    struct ldb_request *remote_req;

    struct map_reply *r_list;
    struct map_reply *r_current;
};