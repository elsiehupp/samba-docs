Samba4/Andrew and Jelmers Fantasy Page/2009/
    <namespace>0</namespace>
<last_edited>2010-09-30T08:19:36Z</last_edited>
<last_editor>JelmerVernooij</last_editor>

==================================

Plans for fortnight ending 19 December 2009
==================================

*Password work completed (Matthias)
Achievements
------------------------*DRS pair programming with Tridge (Andrew)
**Working on linked attribute replication with AD
**Rework duplicate code into utility functions

===============================
Plans for fortnight ending 5 December 2009
===============================

Achieved so far
------------------------*Alpha release (Andrew)
*Fixed nasty "primaryGroupToken" crash bug (Andrew)

===============================
Plans for fortnight ending 21 November 2009
===============================

*Fix up group membership (Andrew)
**The PAC should not include builtin groups, but the local token must

===============================
Plans for fortnight ending 7 November 2009
===============================

*Fix up Binary+DN format DNs after the Vampire sprint. (Andrew)
**The code developed at the interop event with Microsoft needs some rough edges filed off...

Achieved so far
------------------------*Finished Dynamic creation of partitions (Andrew)
*Posted implemention of Binary+DN changes, awaiting review (Andrew)
*Reviewed patches by mdw for const and passwords (Andrew)
*Reviewed karminim's prefixmap patches (Andrew)

===============================
Plans for fortnight ending 23 October 2009
===============================

*Finish Dynamic creation of partitions (Andrew)

Achieved so far
------------------------*Dynamic creation of partitions (Andrew)
**Merged many of the pre-requisite patches that are required towards

===============================
Plans for fortnight ending 9 October 2009
===============================

Achieved so far
------------------------*Dynamic creation of partitions (Andrew)
**Continued work started at plugfest.  
*Reproduced DRS replication (Andrew)
**i.e., the things tridge achived at the plugfest. 
**Published generalised versions of tridge's helper scripts
*Pair-programming with Tridge on merging his DRS work (Andrew)

===============================
Plans for fortnight ending 26 September 2009
===============================

*Implement clever nTSecurityDescriptor update (Matthieu)
*Merge Calin's work into Samba-GTK. (Jelmer)
*Test and Debianize SWAT. (Jelmer)

Achieved so far
------------------------*Merged outstanding patches. (Jelmer)
*CIFS plugfest (Andrew)
**Merged to common code parts required for a lmhosts implementation in Samba4
**Discussions around LDAP and Kerberos backends for Samba4
**[http://OTTsamba.org/people/2/cifsDD/DDAA/en/EEblog article]
*Kerberos Salting
**Reworked 'join domain' code to always use the python 'set secrets' code
**This ensures we then set saltPrincipal, which was previously incorrect
*Microsoft interop (Andrew)
**Ran Microsoft's LDAP testsuite against Samba4
**Added objectClass hierarchy restrictions
**Added allowed RDN restrictions
**Started 'dynamic partitions' work
**Don't allow creation of 'isDefunct' objectClasses
**Add new module to handle 'lazyCommit' control (ignored for now)
**Handle NULL RDN
**Merge 'relax' control to allow us to specify objectGUID in provision, but ban their specification normally
**Learnt how the online interop environment will be set up
***Should allow us to run the tests remotely.
**Assistance where required for the DRS replication challenge tridge was running

===============================
Plans for fortnight ending 12 September 2009
===============================

*Demonstrate Samba<->Samba replication over DRS (Andrew, tridge)
*Finally import LDB index patches
*More work on the SAMLDB module (Matthias)

Achieved so far
------------------------*Worked with tridge to: (Andrew)
**Add support for linked attribute replication over DRS
**Fix LDB to be more robust in handling errors in callback-based modules
**Fix failures on older python installs for the 'dcerpc' tests
**Rework LDB and Samba4's modules to correctly handle two-stage commits
*Investigated LDB index performance and proposed patches to fix it
*Implement correct behavior with supportedEnc field in GetDomainInfo rpc (Matthieu)
*Refactor rebuildextendeddn so it can be integrated in main repo (Matthieu)

===============================
Plans for fortnight ending 29 August 2009
===============================

*Finish basic functions for update script (ie. allow updating at least the schema and adding simple objects) (Matthieu)
*Push rebuildextendeddn.py to the central repo (Matthieu)
*Return full ctr6 structure in dcesrv_drsuapi_DsGetNCChanges (Anatoliy)
*Start digging in linked attributes (Anatoliy)
*Test case for "urgent replication" (Kamen)
*Test case for DsGetNCChanges() (Kamen)
Achieved so far
------------------------*Explanation of Zahari's ACL problem (Andrew)
*Add and improve ldb python wrappers to assist test and conversion script development (Andrew)
*Fix 'show_deleted' module not to linearise the search filter (should improve performance) (Andrew)

===============================
Plans for fortnight ending 15 August 2009
===============================

*Really start working on a tool for provision update (mainly due to schema update) (Matthieu)
*Investigate and fix issues with Windows 2008 and Samba4 (as a Windows 2008 level DC) (Andrew)
Achieved so far
------------------------*Review of Matthias's 'Computer information in AD' patch (Andrew) 
**Matthias was finally able to merge his patch!
*More questions to Microsoft (AES key use) (Andrew)
*Create a script (rebuildextendeddn.py) to (re)build extended, usefull for upgrading a long time running setup (Matthieu)

===============================
Plans for fortnight ending 1 August 2009
===============================

*Continue investigation on bug 6273 (unable to access windows 2008 share from XP/ (Matthieu)
*Start working on a tool for provision update (mainly due to schema update) (Matthieu)
*Display specifiers (Andrew, Matthias)
*Prepare for an alpha with vampire capability (Andrew)
*Add flag to ldb to force canonical form (Andrew)
*Investigate file server bugs (Andrew)
*Investigate domain trusts again (Andrew)
Achieved so far
------------------------*Computer informations in AD (Matthias)
*Nested groups (Matthias)
*Forwarded question to Microsoft for their comment in Windows 2008 access issue (Andrew)
*Review of Matthias's 'Computer information in AD' patch (Andrew)
*Fixed Zahari's segfault in his python wrapper for libnet_ChangePassword (Andrew)
*Implemented 'net export keytab' to extract a keytab from a Samba4 DC (Andrew)
*Fixed a number of trivial failures in Samba4's 'make test' (Andrew)
**This should make real bugs easier to see
*Fix provision on FreeBSD (Andrew)
*Find core problem for bug 6273, proposed a patch (Matthieu)

===============================
Plans for fortnight ending 18 July 2009
===============================

*Prepare for an alpha with vampire capability (Andrew)
*Add flag to ldb to force canonical form (Andrew)
**This is things such as making large 32 bit integers negative, sids always to binary etc
*Research possibilities how to use Kerberos from within Python code (Zahari)
*Catch up with Andrew Tridgell on replication (Anatoliy, Kamen)
*Communicate with Microsoft to establish the correct nTSecurityDescriptors for the partitions in a clean installation, how is the defaultSecurityDescriptor used, how the default DACL of a security token is created, and the function of the extended rights (Nadya)
*Finish debugging the descriptor inheritance (Nadya)
*Define tests for descriptor inheritance to be added to unit tests (Nadya)
*Improve Netlogon dissector in order to drill down on bugs 6272 and 6273 (Matthieu)
*Investigate the problems with Windows 2008 as a SMB client for Windows XP bug 6272 (Matthieu)

Achieved so far
------------------------*Found the problem for bug 6272, issued a patch that should be integrated by Heimdal (Matthieu)
*Netlogon dissector of wireshark is now able to decrypt schannel encrypted dialogs, patch sent to samba-technical for comments (Matthieu) 
*Found and fixed python and ldb/ issues shown up by nTsecurityDescriptor test by Zahari (Andrew)
*Fixed Windows7 Join against Samba4 (Andrew)
**It was failing for the 'add' case.
*Finalize schemaUpdateNow patch and test(Anatoliy)
**It does not break possibleInferiors test and the schema update is ok now
**We should focus on schema consistency checker at some point
*Make Samba4 report Windows 2008 functional level by default (Andrew)
*Update to current Heimdal again (as patches have been accepted) (Andrew)
*Sort out issues with various tests (schemaUpdateNow etc) and get outstanding patches applied (Andrew)
*Working with NTP.org community to finally integrate the MS-SNTP signing of NTP replies (Andrew)
*Discussions with Microsoft to get 'Display specifiers' released under an acceptable licence (Andrew)
**This should allow an import into Samba4

===============================
Plans for fortnight ending 4 July 2009
===============================

*Sort out nTsecurityDescriptor problems from Zahari (Andrew)
*Work with summer of code students (Andrew)

Achieved so far
------------------------*Worked with tridge to show DRS replication from windows works again (Andrew)
*Applied patch queue from Matthias (Andrew)

===============================
Plans for fortnight ending 20 June 2009
===============================

*Improve automated setup of OpenLDAP backend (Andrew)
*Finish subunit separation (Jelmer)
*Maybe WMI..
Achieved so far
------------------------*Samba4 alpha (Andrew)
*Heimdal merge (Andrew)
*Fixing Python rpcecho test and Python ldb test
*Work with Don Davis on Samba4's Kerberos lib requirements (Andrew)

===============================
Plans for fortnight ending 6 June 2009
===============================

*rpcecho.python test (Jelmer)
*Attempt Heimdal merge (Andrew)
*More work on Kerberos requirements (Andrew)
Achived so far
------------------------*Documentation of Kerberos requirements (in particular requiremnts that a MIT Kerberos swich would require) (Andrew with Don Davis)
*Fix SAMR tests (Andrew)
*Fix build with older libnet on Fedora 10
*LDB performance issues with many users (Andrew and Tridge)
*Unique indexes in LDB (Andrew and Tridge)
*Fixed one-level indexes in LDB (Andrew and Tridge)
*Worked with Howard Chu to chase down nasty crash bugs in OpenLDAP under Samba4's 'make test'

===============================
Plans for fortnight ending 23 May 2009
===============================

*Rework Samba4 DC to support only one realm at a time (Andrew)
**This is not related to trusted domains, but to how we look at our database
*Fix krbtgt expiry causing kpasswd account to be disabled (Andrew)
Achieved so far
------------------------*'make test' failures with OpenLDAP backend (Andrew)
**Reproduced on current code
**Fedora 11 VM prepared and supplied to Howard Chu for further investigation
*str_list code (Andrew)
**str_list_make_v3 added to Samba3 while I was away
**Investigate why this 'v3' version is required
**Add unit tests for all aspects of 'common' str_list behaviour
**Attempt (but not committed) to re-merge all the str_list code

===============================
Plans for fortnight ending 9 May 2009
===============================

Achieved so far
------------------------*Documentation build system improvements (Jelmer)
**Changed the docs build system to use dblatex rather than db2latex
**Remove cruft from docs
===============================
Plans for fortnight ending 25 April 2009
===============================

*SambaXP conference
**Samba4 status report presentation
**Samba4 and Microsoft presentation
Achieved so far
------------------------*libcli/ merge (without ldb and Samba3 server-side components) (Andrew)
*Fix RPC python tests (Andrew, Jelmer)

===============================
Plans for fortnight ending 11 April 2009
===============================

Achieved so far
------------------------*Use Full WSPP Microsoft schema in Samba4 (Andrew and Tridge)
**Required a lot of work to make ldb more efficient with a full set of schema
**Create and test possibleInferiors attribute for AD schema
**Integrate work by Sreepathi Pai to convert the WSPP schema into LDIF for the provision
*Prepare merge of charcnv code
**Required cutting down patch from all code to just sharing a common API

===============================
Plans for fortnight ending 28 March 2009
===============================

*Improve the implementation of netr_DsRGetDCNameEx2 (Andrew)
*Include full AD schema when permitted by Microsoft to do so (Andrew)
*libcli/ merge between Samba3 and Samba4 (Andrew)
*charcvn merge between Samba3 and Samba4 (Andrew)
*libregistry merge (Jelmer)
*Samba3 DCE/CCEEasync (Jelmer)
*WMI (Jelmer)
*Fix kpasswd when the krbtgt account has expired (Andrew)
Achived so far
------------------------*Pair programming of restoring minschema to operation
*Implementation (with Tridge) of UID handling for recursion to a new event context in the VFS layer (Andrew)

===============================
Plans for fortnight ending 14 March 2009
===============================

*Improve the implementation of netr_DsRGetDCNameEx2 (Andrew)
*Include full AD schema when permitted by Microsoft to do so (Andrew)
Achieved so far
------------------------*Proposal for fixes for the 'wrong UID' problem with recursion to a new event context in the VFS layer
*Improve performance of Samba will a full schema (Andrew)
===============================
Plans for fortnight ending 27 February
===============================

Achieved so far
------------------------*Release of alpha7 (Andrew)
*Work on the trusted domains and IPA proposal (Andrew)
*Remove dependency of GENSEC on the Samba4 auth subsystem (Andrew)
*Travel plans for SambaXP (Andrew)
*Work with Microsoft on importing the full AD schema
===============================
Plans for fortnight ending 13 February 2009
===============================

*Prepare alpha7
*Prepare proposal for linking IPA with AD via Samba4 (Andrew)
*Windows7 join to Samba4
**Work to add the AES schannel type
**Fix Samba4 to accept Windows 7 joins
Achieved so far
------------------------*Phone call with Microsoft over Windows 7 and Samba issues (Andrew)
*Initial work on IPA proposal (Andrew)
**See http://Tsamba.org/indexDDO/ba4/Proposal_f/AD_tru/

===============================
Plans for fortnight ending 24 January 2009
===============================

*More work reintegrating WMI (Jelmer)
*Finish full epmapper implementation (Jelmer)
*Fix random failures of samba4.ldb.python tests (Jelmer)
*Use subunit in submissions to the buildfarm (Jelmer)
Achieved so far
------------------------*Alpha 6 ! (Andrew, Jelmer)
===============================
Plans for fortnight ending 10 January 2009=