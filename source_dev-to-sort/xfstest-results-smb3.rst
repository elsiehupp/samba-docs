Xfstest-results-smb3
    <namespace>0</namespace>
<last_edited>2020-06-15T03:58:26Z</last_edited>
<last_editor>Sfrench</last_editor>

xfstest results mounted to Samba with vers=3.1.1 with 5.7 kernel (mount options "noperm,mfsymlinks")
------------------------

cifs/001 1s
*generic/001 12s
*generic/002 1s
*generic/003	[not run] atime related mount options have no effect on cifs
*generic/004	[not run] O_TMPFILE is not supported
*generic/005 1s
*generic/006 41s
*generic/007 68s
*generic/008	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/008.out.bad)
    --- tests/generic/008.out	2020-01-24 17:11:18.672862181 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/008.out.bad	2020-06-14 13:46:29.063693642 -0500
    @@ -1,227 +1,10 @@
     QA output created by 008
     zero 0, 1
    -wrote 1024/1024 bytes at offset 0
    -XXX Bytes, X ops; XX:XX:XX.X (XXX YYY/sec and XXX ops/sec)
    -wrote 1024/1024 bytes at offset 1024
    -XXX Bytes, X ops; XX:XX:XX.X (XXX YYY/sec and XXX ops/sec)
    -00000000:  00 41 41 41 41 41 41 41 41 41 41 41 41 41 41 41  .AAAAAAAAAAAAAAA
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/008.out /home/smfrench/xfstests-dev/results//generic/008.out.bad'  to see the entire diff)
*generic/009	[failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/009.out.bad)
    --- tests/generic/009.out	2020-01-24 17:11:18.672862181 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/009.out.bad	2020-06-14 13:46:30.487699289 -0500
    @@ -1,75 +1,61 @@
     QA output created by 009
     	1. into a hole
    -0: [0..127]: hole
    -1: [128..383]: unwritten
    -2: [384..639]: hole
     1aca77e2188f52a62674fe8a873bdaba
     	2. into allocated space
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/009.out /home/smfrench/xfstests-dev/results//generic/009.out.bad'  to see the entire diff)
*generic/010 0s
*generic/011 34s
*generic/012	[not run] xfs_io fcollapse  failed (old kernel/wrong fs?)
*generic/013 13s
*generic/014 74s
*generic/015	[not run] Filesystem cifs not supported in _scratch_mkfs_sized
*generic/016	[not run] xfs_io fcollapse  failed (old kernel/wrong fs?)
*generic/017	[not run] xfs_io fcollapse  failed (old kernel/wrong fs?)
*generic/018	[not run] defragmentation not supported for fstype "cifs"
*generic/019 not run: require //localhost/scratch to be valid block disk
*generic/020 - output mismatch (see /home/smfrench/xfstests-dev/results//generic/020.out.bad)
    --- tests/generic/020.out	2020-01-24 17:11:18.672862181 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/020.out.bad	2020-06-14 13:48:33.735902962 -0500
    @@ -1,4 +1,5 @@
     QA output created by 020
    +touch: cannot touch '/mnt-local-xfstest/test/syscalltest': Permission denied
     *** list non-existant file
        *** print attributes
     getfattr: <TESTFILE>: No such file or directory
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/020.out /home/smfrench/xfstests-dev/results//generic/020.out.bad'  to see the entire diff)
*generic/021	[not run] xfs_io fcollapse  failed (old kernel/wrong fs?)
*generic/022	[not run] xfs_io fcollapse  failed (old kernel/wrong fs?)
*generic/023 2s
*generic/024 0s
*generic/025	[not run] kernel doesn't support renameat2 syscall
*generic/026	[not run] ACLs not supported by this filesystem type: cifs
*generic/027	[not run] Filesystem cifs not supported in _scratch_mkfs_sized
*generic/028 5s
*generic/029 0s
*generic/030 0s
*generic/031	[not run] xfs_io fcollapse  failed (old kernel/wrong fs?)
*generic/032 11s
*generic/033 0s
*generic/034	[not run] require //localhost/scratch to be valid block disk
*generic/035	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/035.out.bad)
    --- tests/generic/035.out	2020-01-24 17:11:18.676861985 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/035.out.bad	2020-06-14 13:48:55.103890746 -0500
    @@ -1,3 +1,7 @@
     QA output created by 035
     overwriting regular file:
    +t_rename_overwrite: rename("/mnt-local-xfstest/test/26025/file1", "/mnt-local-xfstest/test/26025/file2"): Permission denied
    +rm: cannot remove '/mnt-local-xfstest/test/26025/file2': No such file or directory
     overwriting directory:
    +nlink is 2, should be 0
    +rmdir: failed to remove '/mnt-local-xfstest/test/26025': Directory not empty
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/035.out /home/smfrench/xfstests-dev/results//generic/035.out.bad'  to see the entire diff)
*generic/036 10s
*generic/037 - output mismatch (see /home/smfrench/xfstests-dev/results//generic/037.out.bad)
    --- tests/generic/037.out	2020-01-24 17:11:18.676861985 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/037.out.bad	2020-06-14 13:49:07.895878002 -0500
    @@ -1,4 +1,5 @@
     QA output created by 037
    +touch: cannot touch '/mnt-local-xfstest/test/syscalltest': Permission denied
     "GOOD"
     "GOOD"
     "GOOD"
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/037.out /home/smfrench/xfstests-dev/results//generic/037.out.bad'  to see the entire diff)
*generic/038	[not run] FITRIM not supported on /mnt-local-xfstest/scratch
*generic/039	[not run] require //localhost/scratch to be valid block disk
*generic/040	[not run] require //localhost/scratch to be valid block disk
*generic/041	[not run] require //localhost/scratch to be valid block disk
*generic/042	[not run] cifs does not support shutdown
*generic/043	[not run] cifs does not support shutdown
*generic/044	[not run] cifs does not support shutdown
*generic/045	[not run] cifs does not support shutdown
*generic/046	[not run] cifs does not support shutdown
*generic/047	[not run] cifs does not support shutdown
*generic/048	[not run] cifs does not support shutdown
*generic/049	[not run] cifs does not support shutdown
*generic/050	[not run] cifs does not support shutdown
*generic/051	[not run] cifs does not support shutdown
*generic/052	[not run] cifs does not support shutdown
*generic/053	[not run] ACLs not supported by this filesystem type: cifs
*generic/054	[not run] cifs does not support shutdown
*generic/055	[not run] cifs does not support shutdown
*generic/056	[not run] require //localhost/scratch to be valid block disk
*generic/057	[not run] require //localhost/scratch to be valid block disk
*generic/058	[not run] xfs_io finsert  failed (old kernel/wrong fs?)
*generic/059	[not run] require //localhost/scratch to be valid block disk
*generic/060	[not run] xfs_io finsert  failed (old kernel/wrong fs?)
*generic/061	[not run] xfs_io finsert  failed (old kernel/wrong fs?)
*generic/062	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/062.out.bad)
    --- tests/generic/062.out	2020-01-24 17:11:18.680861789 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/062.out.bad	2020-06-14 13:49:12.779872114 -0500
    @@ -1,20 +1,47 @@
     QA output created by 062
    +touch: cannot touch '/mnt-local-xfstest/test/syscalltest': Permission denied
     *** create test bed
    +mknod: /mnt-local-xfstest/scratch/dev/b: Operation not permitted
    +mknod: /mnt-local-xfstest/scratch/dev/c: Operation not permitted
    +mknod: /mnt-local-xfstest/scratch/dev/p: Operation not permitted
    +find: '/mnt-local-xfstest/scratch/dir0740': Permission denied
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/062.out /home/smfrench/xfstests-dev/results//generic/062.out.bad'  to see the entire diff)
*generic/063	[not run] xfs_io finsert  failed (old kernel/wrong fs?)
*generic/064	[not run] xfs_io finsert  failed (old kernel/wrong fs?)
*generic/065	[not run] require //localhost/scratch to be valid block disk
*generic/066	[not run] require //localhost/scratch to be valid block disk
*generic/067	[not run] require //localhost/scratch to be valid block disk
*generic/068	[not run] cifs does not support freezing
*generic/069 - output mismatch (see /home/smfrench/xfstests-dev/results//generic/069.out.bad)
    --- tests/generic/069.out	2020-01-24 17:11:18.684861593 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/069.out.bad	2020-06-14 13:49:17.175866349 -0500
    @@ -5,5 +5,7 @@
     *** checking file with 20 integers
     *** checking file with 300 integers
     *** checking file with 40000 integers
    +bad data, offset = 82036maybe corrupt O_APPEND to /testfile.42614!
     *** checking file with 3000000 integers
    +bad data, offset = 4maybe corrupt O_APPEND to /testfile.42615!
     *** checking file with 12345 integers
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/069.out /home/smfrench/xfstests-dev/results//generic/069.out.bad'  to see the entire diff)
*generic/070 - output mismatch (see /home/smfrench/xfstests-dev/results//generic/070.out.bad)
    --- tests/generic/070.out	2020-01-24 17:11:18.684861593 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/070.out.bad	2020-06-14 13:49:32.051843652 -0500
    @@ -1 +1,2 @@
     QA output created by 070
    +touch: cannot touch '/mnt-local-xfstest/test/syscalltest': Permission denied
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/070.out /home/smfrench/xfstests-dev/results//generic/070.out.bad'  to see the entire diff)
*generic/071 0s
*generic/072	[not run] xfs_io fcollapse  failed (old kernel/wrong fs?)
*generic/073	[not run] require //localhost/scratch to be valid block disk
*generic/074 14s
*generic/075 [failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/075.out.bad)
    --- tests/generic/075.out	2020-01-24 17:11:18.684861593 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/075.out.bad	2020-06-14 13:49:47.079815941 -0500
    @@ -4,15 +4,4 @@
     ------------------------

-----------------------

     fsx.0 : -d -N numops -S 0
     ------------------------

-----------------------

    -
    ------------------------

------------------------

    -fsx.1 : -d -N numops -S 0 -x
    ------------------------

------------------------

    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/075.out /home/smfrench/xfstests-dev/results//generic/075.out.bad'  to see the entire diff)
*generic/076	[not run] require //localhost/scratch to be local device
*generic/077	[not run] ACLs not supported by this filesystem type: cifs
*generic/078	[not run] kernel doesn't support renameat2 syscall
*generic/079	[not run] file system doesn't support chattr +ia
*generic/080 3s
*generic/081	[not run] require //localhost/scratch to be valid block disk
*generic/082	[not run] disk quotas not supported by this filesystem type: cifs
*generic/083	[not run] Filesystem cifs not supported in _scratch_mkfs_sized
*generic/084 5s
*generic/085	[not run] require //localhost/scratch to be valid block disk
*generic/086 1s
*generic/087	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/087.out.bad)
    --- tests/generic/087.out	2020-01-24 17:11:18.684861593 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/087.out.bad	2020-06-14 13:49:58.159792594 -0500
    @@ -1,7 +1,7 @@
     QA output created by 087
     t a 600 file owned by (99/99) as user/group(99/99)  PASS
     T a 600 file owned by (99/99) as user/group(99/99)  PASS
    -t a 600 file owned by (99/99) as user/group(100/99)  PASS
    -T a 600 file owned by (99/99) as user/group(100/99)  PASS
    +t a 600 file owned by (99/99) as user/group(100/99)  FAIL
    +T a 600 file owned by (99/99) as user/group(100/99)  FAIL
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/087.out /home/smfrench/xfstests-dev/results//generic/087.out.bad'  to see the entire diff)
*generic/088	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/088.out.bad)
    --- tests/generic/088.out	2020-01-24 17:11:18.684861593 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/088.out.bad	2020-06-14 13:49:58.519791795 -0500
    @@ -1,9 +1,10 @@
     QA output created by 088
    +-r-xr-xr-x 1 root root 0 Jun 14 13:49 TEST_DIR/t_access
     access(TEST_DIR/t_access, 0) returns 0
     access(TEST_DIR/t_access, R_OK) returns 0
     access(TEST_DIR/t_access, W_OK) returns 0
    -access(TEST_DIR/t_access, X_OK) returns -1
    +access(TEST_DIR/t_access, X_OK) returns 0
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/088.out /home/smfrench/xfstests-dev/results//generic/088.out.bad'  to see the entire diff)
*generic/089	[failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/089.out.bad)
    --- tests/generic/089.out	2020-01-24 17:11:18.684861593 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/089.out.bad	2020-06-14 13:49:58.675791448 -0500
    @@ -1,19 +1,3 @@
     QA output created by 089
    -completed 50 iterations
    -completed 50 iterations
    -completed 50 iterations
    -completed 10000 iterations
    -directory entries:
    -t_mtab
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/089.out /home/smfrench/xfstests-dev/results//generic/089.out.bad'  to see the entire diff)
*generic/090	[not run] require //localhost/scratch to be valid block disk
*generic/091	[failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/091.out.bad)
    --- tests/generic/091.out	2020-01-24 17:11:18.688861397 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/091.out.bad	2020-06-14 13:49:59.479789653 -0500
    @@ -1,7 +1,28 @@
     QA output created by 091
     fsx -N 10000 -l 500000 -r PSIZE -t BSIZE -w BSIZE -Z -R -W
    -fsx -N 10000 -o 8192 -l 500000 -r PSIZE -t BSIZE -w BSIZE -Z -R -W
    -fsx -N 10000 -o 32768 -l 500000 -r PSIZE -t BSIZE -w BSIZE -Z -R -W
    -fsx -N 10000 -o 8192 -l 500000 -r PSIZE -t BSIZE -w BSIZE -Z -R -W
    -fsx -N 10000 -o 32768 -l 500000 -r PSIZE -t BSIZE -w BSIZE -Z -R -W
    -fsx -N 10000 -o 128000 -l 500000 -r PSIZE -t BSIZE -w BSIZE -Z -W
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/091.out /home/smfrench/xfstests-dev/results//generic/091.out.bad'  to see the entire diff)
*generic/092	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/092.out.bad)
    --- tests/generic/092.out	2020-01-24 17:11:18.688861397 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/092.out.bad	2020-06-14 13:49:59.871788775 -0500
    @@ -3,4 +3,4 @@
     XXX Bytes, X ops; XX:XX:XX.X (XXX YYY/sec and XXX ops/sec)
     0: [0..10239]: data
     0: [0..10239]: data
    -1: [10240..20479]: unwritten
    +1: [10240..14335]: hole
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/092.out /home/smfrench/xfstests-dev/results//generic/092.out.bad'  to see the entire diff)
*generic/093	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/093.out.bad)
    --- tests/generic/093.out	2020-01-24 17:11:18.688861397 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/093.out.bad	2020-06-14 13:50:00.447787475 -0500
    @@ -1,15 +1,19 @@
    +touch: cannot touch '/mnt-local-xfstest/test/syscalltest': Permission denied
     QA output created by 093
     
     **** Verifying that appending to file clears capabilities ****
    -file = cap_chown+ep
    +Failed to set capabilities on file '/mnt-local-xfstest/test/093.file' (Operation not supported)
    +The value of the capability argument is not permitted for a file. Or the file is not a regular (non-symlink) file
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/093.out /home/smfrench/xfstests-dev/results//generic/093.out.bad'  to see the entire diff)
*generic/094	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/094.out.bad)
    --- tests/generic/094.out	2020-01-24 17:11:18.688861397 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/094.out.bad	2020-06-14 13:50:01.003786219 -0500
    @@ -1,3 +1,13 @@
     QA output created by 094
     fiemap run with sync
    +Can't fibmap file: Invalid argument
    +ERROR: found an allocated extent where a hole should be: 0
    +map is 'HPPHHDHPHHPHHDDPDHPHDHPHDPDPP'
    +logical: [       0..      13] phys:        0..      13 flags: 0x001 tot: 14
    +Problem comparing fiemap and map
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/094.out /home/smfrench/xfstests-dev/results//generic/094.out.bad'  to see the entire diff)
*generic/095 4s
*generic/096	[not run] Filesystem cifs not supported in _scratch_mkfs_sized
*generic/097	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/097.out.bad)
    --- tests/generic/097.out	2020-01-24 17:11:18.688861397 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/097.out.bad	2020-06-14 13:50:05.699775352 -0500
    @@ -1,22 +1,21 @@
     QA output created by 097
    +touch: cannot touch '/mnt-local-xfstest/test/syscalltest': Permission denied
     
     create file foo
     
     *** Test out the trusted namespace ***
     
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/097.out /home/smfrench/xfstests-dev/results//generic/097.out.bad'  to see the entire diff)
*generic/098 0s
*generic/099	[not run] ACLs not supported by this filesystem type: cifs
*generic/100 36s
*generic/101	[not run] require //localhost/scratch to be valid block disk
*generic/102	[not run] Filesystem cifs not supported in _scratch_mkfs_sized
*generic/103 - output mismatch (see /home/smfrench/xfstests-dev/results//generic/103.out.bad)
    --- tests/generic/103.out	2020-01-24 17:11:18.688861397 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/103.out.bad	2020-06-14 13:50:44.939669195 -0500
    @@ -1,2 +1,3 @@
     QA output created by 103
    +touch: cannot touch '/mnt-local-xfstest/test/syscalltest': Permission denied
     Silence is golden.
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/103.out /home/smfrench/xfstests-dev/results//generic/103.out.bad'  to see the entire diff)
*generic/104	[not run] require //localhost/scratch to be valid block disk
*generic/105	[not run] ACLs not supported by this filesystem type: cifs
*generic/106	[not run] require //localhost/scratch to be valid block disk
*generic/107	[not run] require //localhost/scratch to be valid block disk
*generic/108	[not run] require //localhost/scratch to be valid block disk
*generic/109 51s
*generic/110	[not run] Reflink not supported by test filesystem type: cifs
*generic/111	[not run] Reflink not supported by test filesystem type: cifs
*generic/112 [failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/112.out.bad)
    --- tests/generic/112.out	2020-01-24 17:11:18.688861397 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/112.out.bad	2020-06-14 13:51:38.099486713 -0500
    @@ -4,15 +4,4 @@
     ------------------------

-----------------------

     fsx.0 : -A -d -N numops -S 0
     ------------------------

-----------------------

    -
    ------------------------

------------------------

    -fsx.1 : -A -d -N numops -S 0 -x
    ------------------------

------------------------

    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/112.out /home/smfrench/xfstests-dev/results//generic/112.out.bad'  to see the entire diff)
*generic/113 9s ...  3s
*generic/114	[not run] device block size: 4096 greater than 512
*generic/115	[not run] Reflink not supported by test filesystem type: cifs
*generic/116	[not run] Reflink not supported by test filesystem type: cifs
*generic/117 21s ... - output mismatch (see /home/smfrench/xfstests-dev/results//generic/117.out.bad)
    --- tests/generic/117.out	2020-01-24 17:11:18.692861201 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/117.out.bad	2020-06-14 13:51:56.783413443 -0500
    @@ -1,4 +1,5 @@
     QA output created by 117
    +touch: cannot touch '/mnt-local-xfstest/test/syscalltest': Permission denied
     
     Running fsstress in serial:
     fsstress iteration: 0
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/117.out /home/smfrench/xfstests-dev/results//generic/117.out.bad'  to see the entire diff)
*generic/118	[not run] Reflink not supported by test filesystem type: cifs
*generic/119	[not run] Reflink not supported by test filesystem type: cifs
*generic/120	[not run] atime related mount options have no effect on cifs
*generic/121	[not run] Dedupe not supported by test filesystem type: cifs
*generic/122	[not run] Dedupe not supported by test filesystem type: cifs
*generic/123	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/123.out.bad)
    --- tests/generic/123.out	2020-01-24 17:11:18.692861201 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/123.out.bad	2020-06-14 13:51:57.807409302 -0500
    @@ -1,7 +1,3 @@
     QA output created by 123
    -Permission denied
    -Permission denied
    -Permission denied
    -Permission denied
    -foo
    -bar
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/123.out /home/smfrench/xfstests-dev/results//generic/123.out.bad'  to see the entire diff)
*generic/124 5s
*generic/125 61s
*generic/126	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/126.out.bad)
    --- tests/generic/126.out	2020-01-24 17:11:18.692861201 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/126.out.bad	2020-06-14 13:52:03.483386125 -0500
    @@ -8,12 +8,12 @@
     r a 004 file owned by (99/99) as user/group(12/100)  PASS
     r a 040 file owned by (99/99) as user/group(200/99)  PASS
     r a 400 file owned by (99/99) as user/group(99/500)  PASS
    -r a 000 file owned by (99/99) as user/group(99/99)  FAIL
    +r a 000 file owned by (99/99) as user/group(99/99)  PASS
     w a 000 file owned by (99/99) as user/group(99/99)  FAIL
    -x a 000 file owned by (99/99) as user/group(99/99)  FAIL
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/126.out /home/smfrench/xfstests-dev/results//generic/126.out.bad'  to see the entire diff)
*generic/127 - output mismatch (see /home/smfrench/xfstests-dev/results//generic/127.out.bad)
    --- tests/generic/127.out	2020-01-24 17:11:18.692861201 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/127.out.bad	2020-06-14 13:55:42.050271006 -0500
    @@ -4,10 +4,70 @@
     === FSX Light Mode, Memory Mapping
------------------------

     All 100000 operations completed A-OK!
     === FSX Standard Mode, No Memory Mapping
------------------------

    -All 100000 operations completed A-OK!
    +ltp/fsx -q -l 262144 -o 65536 -S 191110531 -N 100000 -R -W fsx_std_nommap
    +copy range: 0x17cf1 to 0x203e3 at 0x8c59
    +do_copy_range:: Input/output error
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/127.out /home/smfrench/xfstests-dev/results//generic/127.out.bad'  to see the entire diff)
*generic/128 0s ... - output mismatch (see /home/smfrench/xfstests-dev/results//generic/128.out.bad)
    --- tests/generic/128.out	2020-01-24 17:11:18.692861201 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/128.out.bad	2020-06-14 13:55:42.350269253 -0500
    @@ -1 +1,3 @@
     QA output created by 128
    +su: warning: cannot change directory to /home/fsgqa: No such file or directory
    +Error: we shouldn't be able to ls the directory
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/128.out /home/smfrench/xfstests-dev/results//generic/128.out.bad'  to see the entire diff)
*generic/129 71s
*generic/130 2s
*generic/131	[not run] Require fcntl advisory locks support
*generic/132 10s
*generic/133 6s
*generic/134	[not run] Reflink not supported by test filesystem type: cifs
*generic/135 output mismatch (see /home/smfrench/xfstests-dev/results//generic/135.out.bad)
    --- tests/generic/135.out	2020-01-24 17:11:18.696861006 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/135.out.bad	2020-06-14 13:57:12.601723568 -0500
    @@ -8,6 +8,6 @@
     0000000 5656 5656 5656 5656 5656 5656 5656 5656
     *
     0004096
    -0000000 7878 7878 7878 7878 7878 7878 7878 7878
    +0000000 5656 5656 5656 5656 5656 5656 5656 5656
     *
     0002048
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/135.out /home/smfrench/xfstests-dev/results//generic/135.out.bad'  to see the entire diff)
*generic/136	[not run] Dedupe not supported by test filesystem type: cifs
*generic/137	[not run] Reflink not supported by test filesystem type: cifs
*generic/138	[not run] Reflink not supported by test filesystem type: cifs
*generic/139	[not run] Reflink not supported by test filesystem type: cifs
*generic/140	[not run] Reflink not supported by test filesystem type: cifs
*generic/141 0s
*generic/142	[not run] Reflink not supported by test filesystem type: cifs
*generic/143	[not run] Reflink not supported by test filesystem type: cifs
*generic/144	[not run] Reflink not supported by test filesystem type: cifs
*generic/145	[not run] Reflink not supported by test filesystem type: cifs
*generic/146	[not run] Reflink not supported by test filesystem type: cifs
*generic/147	[not run] Reflink not supported by test filesystem type: cifs
*generic/148	[not run] Reflink not supported by test filesystem type: cifs
*generic/149	[not run] Reflink not supported by test filesystem type: cifs
*generic/150	[not run] Reflink not supported by test filesystem type: cifs
*generic/151	[not run] Reflink not supported by test filesystem type: cifs
*generic/152	[not run] Reflink not supported by test filesystem type: cifs
*generic/153	[not run] Reflink not supported by test filesystem type: cifs
*generic/154	[not run] Reflink not supported by test filesystem type: cifs
*generic/155	[not run] Reflink not supported by test filesystem type: cifs
*generic/156	[not run] Reflink not supported by test filesystem type: cifs
*generic/157	[not run] Reflink not supported by test filesystem type: cifs
*generic/158	[not run] Dedupe not supported by test filesystem type: cifs
*generic/159	[not run] file system doesn't support chattr +i
*generic/160	[not run] file system doesn't support chattr +i
*generic/161	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/162	[not run] Dedupe not supported by scratch filesystem type: cifs
*generic/163	[not run] Dedupe not supported by scratch filesystem type: cifs
*generic/164	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/165	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/166	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/167	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/168	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/169 0s
*generic/170	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/171	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/172	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/173	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/174	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/175	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/176	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/177	[not run] require //localhost/scratch to be valid block disk
*generic/178	[not run] Reflink not supported by test filesystem type: cifs
*generic/179	[not run] Reflink not supported by test filesystem type: cifs
*generic/180	[not run] Reflink not supported by test filesystem type: cifs
*generic/181	[not run] Reflink not supported by test filesystem type: cifs
*generic/182	[not run] Dedupe not supported by test filesystem type: cifs
*generic/183	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/184	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/184.out.bad)
    --- tests/generic/184.out	2020-01-24 17:11:18.708860417 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/184.out.bad	2020-06-14 13:57:20.545674008 -0500
    @@ -1 +1,3 @@
     QA output created by 184 - silence is golden
    +mknod: /mnt-local-xfstest/test/null: Operation not permitted
    +chmod: cannot access '/mnt-local-xfstest/test/null': No such file or directory
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/184.out /home/smfrench/xfstests-dev/results//generic/184.out.bad'  to see the entire diff)
*generic/185	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/186	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/187	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/188	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/189	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/190	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/191	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/192	[not run] atime related mount options have no effect on cifs
*generic/193	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/193.out.bad)
    --- tests/generic/193.out	2020-01-24 17:11:18.712860221 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/193.out.bad	2020-06-14 13:57:22.001664901 -0500
    @@ -3,62 +3,54 @@
     testing ATTR_UID
     
     user: chown root owned file to qa_user (should fail)
    -chown: changing ownership of 'test.root': Operation not permitted
     user: chown root owned file to root (should fail)
    -chown: changing ownership of 'test.root': Operation not permitted
     user: chown qa_user owned file to qa_user (should succeed)
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/193.out /home/smfrench/xfstests-dev/results//generic/193.out.bad'  to see the entire diff)
*generic/194	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/195	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/196	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/197	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/198 1s
*generic/199	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/200	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/201	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/202	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/203	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/204	[not run] Filesystem cifs not supported in _scratch_mkfs_sized
*generic/205	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/206	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/207 0s
*generic/208 201s
*generic/209 [failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/209.out.bad)
    --- tests/generic/209.out	2020-01-24 17:11:18.720859829 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/209.out.bad	2020-06-14 14:00:45.484337084 -0500
    @@ -1,2 +1,2 @@
     QA output created by 209
    -test ran for 30 seconds without error
    +reader found old byte at pos 4096
    \ No newline at end of file
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/209.out /home/smfrench/xfstests-dev/results//generic/209.out.bad'  to see the entire diff)
*generic/210 0s
*generic/211 1s
*generic/212 0s
*generic/213	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/213.out.bad)
    --- tests/generic/213.out	2020-01-24 17:11:18.720859829 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/213.out.bad	2020-06-14 14:00:47.060326473 -0500
    @@ -1,4 +1,3 @@
     QA output created by 213
     We should get: fallocate: No space left on device
     Strangely, xfs_io sometimes says "Success" when something went wrong, FYI
    -fallocate: No space left on device
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/213.out /home/smfrench/xfstests-dev/results//generic/213.out.bad'  to see the entire diff)
*generic/214 0s
*generic/215 2s
*generic/216	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/217	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/218	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/219	[not run] disk quotas not supported by this filesystem type: cifs
*generic/220	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/221 1s
*generic/222	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/223	[not run] can't mkfs cifs with geometry
*generic/224	[not run] Filesystem cifs not supported in _scratch_mkfs_sized
*generic/225	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/225.out.bad)
    --- tests/generic/225.out	2020-01-24 17:11:18.724859633 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/225.out.bad	2020-06-14 14:00:52.304291139 -0500
    @@ -1,3 +1,13 @@
     QA output created by 225
     fiemap run without preallocation, with sync
    +Can't fibmap file: Invalid argument
    +ERROR: found an allocated extent where a hole should be: 0
    +map is 'HDHDDDDHDDDHHDHDHHHDHHDHDHDHHDDHHHHDHDDHDHDHDHHHDDDDHDHHHHDDHHDHDHDDH'
    +logical: [       0..      20] phys:        0..      20 flags: 0x001 tot: 21
    +Problem comparing fiemap and map
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/225.out /home/smfrench/xfstests-dev/results//generic/225.out.bad'  to see the entire diff)
*generic/226	[not run] Filesystem cifs not supported in _scratch_mkfs_sized
*generic/227	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/228	 0s
*generic/229	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/230	[not run] disk quotas not supported by this filesystem type: cifs
*generic/231	[not run] disk quotas not supported by this filesystem type: cifs
*generic/232	[not run] disk quotas not supported by this filesystem type: cifs
*generic/233	[not run] disk quotas not supported by this filesystem type: cifs
*generic/234	[not run] disk quotas not supported by this filesystem type: cifs
*generic/235	[not run] disk quotas not supported by this filesystem type: cifs
*generic/236 ++++++++[failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/236.out.bad)
    --- tests/generic/236.out	2020-01-24 17:11:18.728859437 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/236.out.bad	2020-06-14 14:00:55.196271633 -0500
    @@ -1,2 +1,3 @@
     QA output created by 236
    -Test over.
    +ctime: 1592161254 -> 1592161254 
    +Fatal error: ctime not updated after link
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/236.out /home/smfrench/xfstests-dev/results//generic/236.out.bad'  to see the entire diff)
*generic/237	[not run] ACLs not supported by this filesystem type: cifs
*generic/238	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/239 31s ...  34s
*generic/240	[not run] fs block size must be larger than the device block size.  fs block size: 1024, device block size: 4096
*generic/241 72s ...  72s
*generic/242	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/243	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/244	[not run] disk quotas not supported by this filesystem type: cifs
*generic/245 1s ...  0s
*generic/246 0s ...  0s
*generic/247 13s ...  4s
*generic/248 0s ...  0s
*generic/249 0s ...  1s
*generic/250	[not run] require //localhost/scratch to be valid block disk
*generic/251	[not run] FITRIM not supported on /mnt-local-xfstest/scratch
*generic/252	[not run] require //localhost/scratch to be valid block disk
*generic/253	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/254	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/255	[failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/255.out.bad)
    --- tests/generic/255.out	2020-01-24 17:11:18.732859241 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/255.out.bad	2020-06-14 14:02:49.351481872 -0500
    @@ -7,9 +7,6 @@
     2: [384..639]: extent
     2f7a72b9ca9923b610514a11a45a80c9
     	3. into unwritten space
    -0: [0..127]: extent
    -1: [128..383]: hole
    -2: [384..639]: extent
     1aca77e2188f52a62674fe8a873bdaba
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/255.out /home/smfrench/xfstests-dev/results//generic/255.out.bad'  to see the entire diff)
*generic/256	[not run] Filesystem cifs not supported in _scratch_mkfs_sized
*generic/257 3s ...  2s
*generic/258 1s ... [failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/258.out.bad)
    --- tests/generic/258.out	2020-01-24 17:11:18.736859046 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/258.out.bad	2020-06-14 14:02:51.435462215 -0500
    @@ -1,5 +1,6 @@
     QA output created by 258
     Creating file with timestamp of Jan 1, 1960
     Testing for negative seconds since epoch
    -Remounting to flush cache
    -Testing for negative seconds since epoch
    +Timestamp wrapped: 1592161371
    +Timestamp wrapped
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/258.out /home/smfrench/xfstests-dev/results//generic/258.out.bad'  to see the entire diff)
*generic/259	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/260	[not run] FITRIM not supported on /mnt-local-xfstest/scratch
*generic/261	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/262	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/263 47s ... [failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/263.out.bad)
    --- tests/generic/263.out	2020-01-24 17:11:18.736859046 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/263.out.bad	2020-06-14 14:02:52.271454334 -0500
    @@ -1,3 +1,28 @@
     QA output created by 263
     fsx -N 10000 -o 8192 -l 500000 -r PSIZE -t BSIZE -w BSIZE -Z
    -fsx -N 10000 -o 128000 -l 500000 -r PSIZE -t BSIZE -w BSIZE -Z
    +Seed set to 1
    +main: filesystem does not support fallocate mode FALLOC_FL_COLLAPSE_RANGE, disabling!
    +main: filesystem does not support fallocate mode FALLOC_FL_INSERT_RANGE, disabling!
    +main: filesystem does not support clone range, disabling!
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/263.out /home/smfrench/xfstests-dev/results//generic/263.out.bad'  to see the entire diff)
*generic/264	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/265	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/266	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/267	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/268	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/269	[not run] Filesystem cifs not supported in _scratch_mkfs_sized
*generic/270	[not run] disk quotas not supported by this filesystem type: cifs
*generic/271	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/272	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/273	[not run] Filesystem cifs not supported in _scratch_mkfs_sized
*generic/274	[not run] Filesystem cifs not supported in _scratch_mkfs_sized
*generic/275	[not run] Filesystem cifs not supported in _scratch_mkfs_sized
*generic/276	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/277	[not run] file system doesn't support chattr +A
*generic/278	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/279	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/280	[not run] disk quotas not supported by this filesystem type: cifs
*generic/281	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/282	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/283	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/284	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/285	[failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/285.out.bad)
    --- tests/generic/285.out	2020-01-24 17:11:18.744858654 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/285.out.bad	2020-06-14 14:02:56.751412152 -0500
    @@ -1 +1,3 @@
     QA output created by 285
    +seek sanity check failed!
    +(see /home/smfrench/xfstests-dev/results//generic/285.full for details)
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/285.out /home/smfrench/xfstests-dev/results//generic/285.out.bad'  to see the entire diff)
*generic/286	[failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/286.out.bad)
    --- tests/generic/286.out	2020-01-24 17:11:18.744858654 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/286.out.bad	2020-06-14 14:02:57.131408578 -0500
    @@ -1 +1,5 @@
     QA output created by 286
    +ERROR: [error:38] SEEK_DATA failed due to Input/output error:Input/output error
    +/mnt-local-xfstest/test/seek_copy_testfile /mnt-local-xfstest/test/seek_copy_testfile.dest differ: char 1, line 1
    +TEST01: file bytes check failed
    +(see /home/smfrench/xfstests-dev/results//generic/286.full for details)
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/286.out /home/smfrench/xfstests-dev/results//generic/286.out.bad'  to see the entire diff)
*generic/287	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/288	[not run] FITRIM not supported on /mnt-local-xfstest/scratch
*generic/289	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/290	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/291	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/292	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/293	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/294	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/294.out.bad)
    --- tests/generic/294.out	2020-01-24 17:11:18.748858457 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/294.out.bad	2020-06-14 14:02:58.267397896 -0500
    @@ -1,5 +1,6 @@
     QA output created by 294
    -mknod: SCRATCH_MNT/294.test/testnode: File exists
    +mknod: SCRATCH_MNT/294.test/testnode: Operation not permitted
    +mknod: SCRATCH_MNT/294.test/testnode: Read-only file system
     mkdir: cannot create directory 'SCRATCH_MNT/294.test/testdir': File exists
     touch: cannot touch 'SCRATCH_MNT/294.test/testtarget': Read-only file system
     ln: creating symbolic link 'SCRATCH_MNT/294.test/testlink': File exists
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/294.out /home/smfrench/xfstests-dev/results//generic/294.out.bad'  to see the entire diff)
*generic/295	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/296	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/297	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/298	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/299	[not run] require //localhost/scratch to be valid block disk
*generic/300	[not run] require //localhost/scratch to be valid block disk
*generic/301	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/302	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/303	[not run] Reflink not supported by test filesystem type: cifs
*generic/304	[not run] Dedupe not supported by test filesystem type: cifs
*generic/305	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/306	[failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/306.out.bad)
    --- tests/generic/306.out	2020-01-24 17:11:18.752858261 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/306.out.bad	2020-06-14 14:03:00.051381131 -0500
    @@ -1,25 +1,5 @@
     QA output created by 306
    -== try to create new file
    -touch: cannot touch 'SCRATCH_MNT/this_should_fail': Read-only file system
    -== pwrite to null device
    -wrote 512/512 bytes at offset 0
    -XXX Bytes, X ops; XX:XX:XX.X (XXX YYY/sec and XXX ops/sec)
    -== pread from zero device
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/306.out /home/smfrench/xfstests-dev/results//generic/306.out.bad'  to see the entire diff)
*generic/307	[not run] ACLs not supported by this filesystem type: cifs
*generic/308 0s
*generic/309 1s
*generic/310 102s
*generic/311	[not run] require //localhost/scratch to be valid block disk
*generic/312	[not run] Filesystem cifs not supported in _scratch_mkfs_sized
*generic/313 4s ... - output mismatch (see /home/smfrench/xfstests-dev/results//generic/313.out.bad)
    --- tests/generic/313.out	2020-01-24 17:11:18.756858065 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/313.out.bad	2020-06-14 14:04:48.282385940 -0500
    @@ -1,2 +1,6 @@
     QA output created by 313
     Silence is golden
    +ctime not updated after truncate down
    +mtime not updated after truncate down
    +ctime not updated after truncate up
    +mtime not updated after truncate up
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/313.out /home/smfrench/xfstests-dev/results//generic/313.out.bad'  to see the entire diff)
*generic/314	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/314.out.bad)
    --- tests/generic/314.out	2020-01-24 17:11:18.756858065 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/314.out.bad	2020-06-14 14:04:48.406384818 -0500
    @@ -1,2 +1,2 @@
     QA output created by 314
    -drwxr-sr-x subdir
    +drwxr-xr-x subdir
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/314.out /home/smfrench/xfstests-dev/results//generic/314.out.bad'  to see the entire diff)
*generic/315 0s
*generic/316	[failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/316.out.bad)
    --- tests/generic/316.out	2020-01-24 17:11:18.756858065 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/316.out.bad	2020-06-14 14:04:50.002370442 -0500
    @@ -45,137 +45,31 @@
     	1. into a hole
     1aca77e2188f52a62674fe8a873bdaba
     	2. into allocated space
    -0: [0..127]: extent
    -1: [128..383]: hole
    -2: [384..639]: extent
    -2f7a72b9ca9923b610514a11a45a80c9
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/316.out /home/smfrench/xfstests-dev/results//generic/316.out.bad'  to see the entire diff)
*generic/317	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/317.out.bad)
    --- tests/generic/317.out	2020-01-24 17:11:18.756858065 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/317.out.bad	2020-06-14 14:04:50.466366265 -0500
    @@ -2,19 +2,19 @@
     From init_user_ns
       File: "$SCRATCH_MNT/file1"
       Size: 0            Filetype: Regular File
    -  Mode: (0644/-rw-r--r--)         Uid: (qa_user)  Gid: (qa_user)
    +  Mode: (0755/-rwxr-xr-x)         Uid: (0)  Gid: (0)
     From user_ns
       File: "$SCRATCH_MNT/file1"
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/317.out /home/smfrench/xfstests-dev/results//generic/317.out.bad'  to see the entire diff)
*generic/318	[not run] ACLs not supported by this filesystem type: cifs
*generic/319	[not run] ACLs not supported by this filesystem type: cifs
*generic/320	[not run] Filesystem cifs not supported in _scratch_mkfs_sized
*generic/321	[not run] require //localhost/scratch to be valid block disk
*generic/322	[not run] require //localhost/scratch to be valid block disk
*generic/323	 122s
*generic/324	[not run] defragmentation not supported for fstype "cifs"
*generic/325	[not run] require //localhost/scratch to be valid block disk
*generic/326	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/327	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/328	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/329	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/330	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/331	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/332	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/333	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/334	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/335	[not run] require //localhost/scratch to be valid block disk
*generic/336	[not run] require //localhost/scratch to be valid block disk
*generic/337 0s ... - output mismatch (see /home/smfrench/xfstests-dev/results//generic/337.out.bad)
    --- tests/generic/337.out	2020-01-24 17:11:18.764857673 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/337.out.bad	2020-06-14 14:07:00.017224449 -0500
    @@ -1,4 +1,5 @@
     QA output created by 337
    +touch: cannot touch '/mnt-local-xfstest/test/syscalltest': Permission denied
     # file: SCRATCH_MNT/testfile
     user.J3__T_Km3dVsW_="hello"
     user.WvG1c1Td="qwerty"
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/337.out /home/smfrench/xfstests-dev/results//generic/337.out.bad'  to see the entire diff)
*generic/338	[not run] require //localhost/scratch to be valid block disk
*generic/339 16s
*generic/340 1s
*generic/341	[not run] require //localhost/scratch to be valid block disk
*generic/342	[not run] require //localhost/scratch to be valid block disk
*generic/343	[not run] require //localhost/scratch to be valid block disk
*generic/344 3s
*generic/345 2s
*generic/346 5s
*generic/347	[not run] require //localhost/scratch to be valid block disk
*generic/348	[not run] require //localhost/scratch to be valid block disk
*generic/352	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/349 2s ...  2s
*generic/350 2s ...  2s
*generic/351	[not run] xfs_io finsert  failed (old kernel/wrong fs?)
*generic/353	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/354 2s
*generic/355	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/355.out.bad)
    --- tests/generic/355.out	2020-01-24 17:11:18.772857281 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/355.out.bad	2020-06-14 14:07:30.432962783 -0500
    @@ -1,14 +1,14 @@
     QA output created by 355
     Check that suid/sgid bits are cleared after direct write
     == with no exec perm
    -before: -rwSr-Sr--
    -after:  -rw-r-Sr--
    +before: -rwxr-xr-x
    +after:  -rwxr-xr-x
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/355.out /home/smfrench/xfstests-dev/results//generic/355.out.bad'  to see the entire diff)
*generic/356	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/357	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/358	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/359	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/360 0s
*generic/361	[not run] require //localhost/scratch to be valid block disk
*generic/362	[not run] this test requires richacl support on $SCRATCH_DEV
*generic/363	[not run] this test requires richacl support on $SCRATCH_DEV
*generic/364	[not run] this test requires richacl support on $SCRATCH_DEV
*generic/365	[not run] this test requires richacl support on $SCRATCH_DEV
*generic/366	[not run] this test requires richacl support on $SCRATCH_DEV
*generic/367	[not run] this test requires richacl support on $SCRATCH_DEV
*generic/368	[not run] this test requires richacl support on $SCRATCH_DEV
*generic/369	[not run] thgeneric/442	[not run] require //localhost/scratch to be valid block diskis test requires richacl support on $SCRATCH_DEV
*generic/370	[not run] this test requires richacl support on $SCRATCH_DEV
*generic/371	[not run] Filesystem cifs not supported in _scratch_mkfs_sized
*generic/372	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/373	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/374	[not run] Dedupe not supported by scratch filesystem type: cifs
*generic/375	[not run] ACLs not supported by this filesystem type: cifs
*generic/376	[not run] require //localhost/scratch to be valid block disk
*generic/377 0s ... - output mismatch (see /home/smfrench/xfstests-dev/results//generic/377.out.bad)
    --- tests/generic/377.out	2020-01-24 17:11:18.780856890 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/377.out.bad	2020-06-14 14:07:33.604935621 -0500
    @@ -1,4 +1,5 @@
     QA output created by 377
    +touch: cannot touch '/mnt-local-xfstest/test/syscalltest': Permission denied
     xattr: user.foo
     xattr: user.hello
     xattr: user.ping
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/377.out /home/smfrench/xfstests-dev/results//generic/377.out.bad'  to see the entire diff)
*generic/378	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/378.out.bad)
    --- tests/generic/378.out	2020-01-24 17:11:18.780856890 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/378.out.bad	2020-06-14 14:07:33.820933771 -0500
    @@ -1,3 +1,3 @@
     QA output created by 378
    -Permission denied
    -Permission denied
    +You should not see this
    +You should not see this
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/378.out /home/smfrench/xfstests-dev/results//generic/378.out.bad'  to see the entire diff)
*generic/379	[not run] disk quotas not supported by this filesystem type: cifs
*generic/380	[not run] disk quotas not supported by this filesystem type: cifs
*generic/381	[not run] disk quotas not supported by this filesystem type: cifs
*generic/382	[not run] disk quotas not supported by this filesystem type: cifs
*generic/383	[not run] disk quotas not supported by this filesystem type: cifs
*generic/384	[not run] disk quotas not supported by this filesystem type: cifs
*generic/385	[not run] disk quotas not supported by this filesystem type: cifs
*generic/386	[not run] disk quotas not supported by this filesystem type: cifs
*generic/387	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/388	[not run] require //localhost/scratch to be local device
*generic/389	[not run] O_TMPFILE is not supported
*generic/390	[not run] cifs does not support freezing
*generic/391 3s
*generic/392	[not run] cifs does not support shutdown
*generic/393 1s
*generic/394 0s
*generic/395	[not run] No encryption support for cifs
*generic/396	[not run] No encryption support for cifs
*generic/397	[not run] No encryption support for cifs
*generic/398	[not run] No encryption support for cifs
*generic/399	[not run] No encryption support for cifs
*generic/400	[not run] disk quotas not supported by this filesystem type: cifs
*generic/401	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/401.out.bad)
    --- tests/generic/401.out	2020-01-24 17:11:18.788856498 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/401.out.bad	2020-06-14 14:07:40.180879383 -0500
    @@ -1,9 +1,9 @@
     QA output created by 401
    +mknod: /mnt-local-xfstest/scratch/find-by-type/c: Operation not permitted
    +mknod: /mnt-local-xfstest/scratch/find-by-type/b: Operation not permitted
    +mknod: /mnt-local-xfstest/scratch/find-by-type/p: Operation not permitted
     . d
     .. d
    -b b
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/401.out /home/smfrench/xfstests-dev/results//generic/401.out.bad'  to see the entire diff)
*generic/402	[not run] filesystem cifs timestamp bounds are unknown
*generic/403	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/403.out.bad)
    --- tests/generic/403.out	2020-01-24 17:11:18.788856498 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/403.out.bad	2020-06-14 14:07:40.560876135 -0500
    @@ -1,2 +1,205 @@
     QA output created by 403
    +touch: cannot touch '/mnt-local-xfstest/test/syscalltest': Permission denied
    +setfattr: /mnt-local-xfstest/scratch/file: Operation not supported
    +/mnt-local-xfstest/scratch/file: trusted.small: Operation not supported
    +setfattr: /mnt-local-xfstest/scratch/file: Operation not supported
    +setfattr: /mnt-local-xfstest/scratch/file: Operation not supported
    +setfattr: /mnt-local-xfstest/scratch/file: Operation not supported
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/403.out /home/smfrench/xfstests-dev/results//generic/403.out.bad'  to see the entire diff)
*generic/404	[not run] xfs_io finsert  failed (old kernel/wrong fs?)
*generic/405	[not run] require //localhost/scratch to be valid block disk
*generic/406 1s
*generic/407	[not run] Reflink not supported by test filesystem type: cifs
*generic/408	[not run] Dedupe not supported by test filesystem type: cifs
*generic/409	[not run] require //localhost/scratch to be local device
*generic/410	[not run] require //localhost/scratch to be local device
*generic/411	[not run] require //localhost/scratch to be local device
*generic/412 0s
*generic/413	[not run] mount //localhost/scratch with dax failed
*generic/414	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/415	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/416	[not run] Filesystem cifs not supported in _scratch_mkfs_sized
*generic/417	[not run] cifs does not support shutdown
*generic/418	[not run] require //localhost/test to be valid block disk
*generic/419	[not run] No encryption support for cifs
*generic/420 0s
*generic/421	[not run] No encryption support for cifs
*generic/422 1s
*generic/423	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/423.out.bad)
    --- tests/generic/423.out	2020-01-24 17:11:18.796856105 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/423.out.bad	2020-06-14 14:07:43.508850958 -0500
    @@ -1,11 +1,30 @@
     QA output created by 423
     Test statx on a fifo
    +mkfifo: cannot create fifo '/mnt-local-xfstest/test/423-fifo': Operation not permitted
    +/mnt-local-xfstest/test/423-fifo: No such file or directory
    +stat_test failed
     Test statx on a chardev
    +mknod: /mnt-local-xfstest/test/423-null: Operation not permitted
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/423.out /home/smfrench/xfstests-dev/results//generic/423.out.bad'  to see the entire diff)
*generic/424	[not run] file system doesn't support any of /usr/bin/chattr +a/+c/+d/+i
*generic/425	[not run] xfs_io fiemap -a failed (old kernel/wrong fs/bad args?)
*generic/426	[not run] cifs does not support NFS export
*generic/427	[not run] Filesystem cifs not supported in _scratch_mkfs_sized
*generic/428 0s
*generic/429	[not run] No encryption support for cifs
*generic/430 0s
*generic/431 0s
*generic/432 1s
*generic/433 0s
*generic/434	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/434.out.bad)
    --- tests/generic/434.out	2020-01-24 17:11:18.800855909 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/434.out.bad	2020-06-14 14:07:46.764823174 -0500
    @@ -1,5 +1,7 @@
     QA output created by 434
     Create the original files
    +mknod: /mnt-local-xfstest/test/test-434/dev1: Operation not permitted
    +mkfifo: cannot create fifo '/mnt-local-xfstest/test/test-434/fifo': Operation not permitted
     Try to copy when source pos > source size
     d41d8cd98f00b204e9800998ecf8427e  TEST_DIR/test-434/copy
     Try to copy to a read-only file
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/434.out /home/smfrench/xfstests-dev/results//generic/434.out.bad'  to see the entire diff)
*generic/435	[not run] No encryption support for cifs
*generic/436	[failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/436.out.bad)
    --- tests/generic/436.out	2020-01-24 17:11:18.800855909 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/436.out.bad	2020-06-14 14:07:47.548816488 -0500
    @@ -1,2 +1,3 @@
     QA output created by 436
    -Silence is golden
    +seek sanity check failed!
    +(see /home/smfrench/xfstests-dev/results//generic/436.full for details)
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/436.out /home/smfrench/xfstests-dev/results//generic/436.out.bad'  to see the entire diff)
*generic/437 1s
*generic/438 139s
*generic/439 0s
*generic/440	[not run] No encryption support for cifs
*generic/441	[not run] require //localhost/scratch to be valid block disk
*generic/442	[not run] require //localhost/scratch to be valid block disk
*generic/443 0s
*generic/444	[not run] ACLs not supported by this filesystem type: cifs
*generic/445 1s
*generic/446 4s
*generic/447	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/448 1s
*generic/449	[not run] ACLs not supported by this filesystem type: cifs
*generic/450	[not run] Only test on sector size < half of block size
*generic/451 30s
*generic/452 1s
*generic/453	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/453.out.bad)
    --- tests/generic/453.out	2020-01-24 17:11:18.804855713 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/453.out.bad	2020-06-14 14:10:46.067326072 -0500
    @@ -1,6 +1,8 @@
     QA output created by 453
     Format and mount
     Create files
    +/home/smfrench/xfstests-dev/tests/generic/453: line 47: /mnt-local-xfstest/scratch/test-453/urk��moo: No such file or directory
     Test files
    +Key urk��moo does not exist for FAKESLASH test??
     Uniqueness of inodes?
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/453.out /home/smfrench/xfstests-dev/results//generic/453.out.bad'  to see the entire diff)
generic/454 1s ... - output mismatch (see /home/smfrench/xfstests-dev/results//generic/454.out.bad)
    --- tests/generic/454.out	2020-01-24 17:11:18.804855713 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/454.out.bad	2020-06-14 14:10:46.503322501 -0500
    @@ -1,6 +1,16 @@
     QA output created by 454
    +touch: cannot touch '/mnt-local-xfstest/test/syscalltest': Permission denied
     Format and mount
     Create files
    +setfattr: /mnt-local-xfstest/scratch/test-454/attrfile: Invalid argument
     Test files
    +/mnt-local-xfstest/scratch/test-454/attrfile: user.linedraw_\012╔═══════════╗\012║ metatable ║\012╟───────────╢\012║ __index   ║\012╚═══════════╝\012.txt: No such attribute
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/454.out /home/smfrench/xfstests-dev/results//generic/454.out.bad'  to see the entire diff)
*generic/455	[not run] This test requires a valid $LOGWRITES_DEV
*generic/456	[not run] require //localhost/scratch to be valid block disk
*generic/457	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/458	[not run] Reflink not supported by scratch filesystem type: cifs
*generic/459	[not run] require //localhost/scratch to be valid block disk
*generic/460 1s ...  1s
*generic/461	[not run] cifs does not support shutdown
*generic/462	[not run] mount //localhost/scratch with dax failed
*generic/463	[not run] Reflink not supported by test filesystem type: cifs
*generic/464	 51s
*generic/465 1s ...  2s
*generic/466	[not run] require //localhost/scratch to be valid block disk
*generic/467	[not run] cifs does not support NFS export
*generic/468	[not run] cifs does not support shutdown
*generic/469 1s ... [failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-*dev/results//generic/469.out.bad)
    --- tests/generic/469.out	2020-01-24 17:11:18.808855517 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/469.out.bad	2020-06-14 14:11:42.382867287 -0500
    @@ -1,9 +1,49 @@
     QA output created by 469
     fsx --replay-ops fsxops.0
    -fsx -y --replay-ops fsxops.0
    -fsx --replay-ops fsxops.1
    -fsx -y --replay-ops fsxops.1
    -fsx --replay-ops fsxops.2
    -fsx -y --replay-ops fsxops.2
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/469.out /home/smfrench/xfstests-dev/results//generic/469.out.bad'  to see the entire diff)
*generic/470	[not run] This test requires a valid $LOGWRITES_DEV
*generic/471	[not run] xfs_io pwrite  -V 1 -b 4k -N failed (old kernel/wrong fs?)
*generic/472	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/472.out.bad)
    --- tests/generic/472.out	2020-01-24 17:11:18.808855517 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/472.out.bad	2020-06-14 14:11:43.418858891 -0500
    @@ -1,4 +1,32 @@
     QA output created by 472
    +rm: cannot remove '/mnt-local-xfstest/scratch/dir0740': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/options.h': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/fgetline.h': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/error.h': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/options.c': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/fgetline.c': Permission denied
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/472.out /home/smfrench/xfstests-dev/results//generic/472.out.bad'  to see the entire diff)
*generic/474	[not run] cifs does not support shutdown
*generic/475	[not run] require //localhost/scratch to be valid block disk
*generic/476  [too slow to run]
*generic/477     [not run] cifs does not support NFS export
*generic/478     - output mismatch (see /home/smfrench/xfstests-dev/results//generic/478.out.bad)
    --- tests/generic/478.out   2020-01-24 17:11:18.808855517 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/478.out.bad        2020-06-14 15:50:16.440827399 -0500
    @@ -1,13 +1,13 @@
     QA output created by 478
     get wrlck
     lock could be placed
    -get wrlck
    -get wrlck
     lock could be placed
     get wrlck
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/478.out /home/smfrench/xfstests-dev/results//generic/478.out.bad'  to see the entire diff)
*generic/479     [not run] cifs does not support mknod/mkfifo
*generic/480     [not run] require //localhost/scratch to be valid block disk
*generic/481     [not run] require //localhost/scratch to be valid block disk
*generic/482     [not run] This test requires a valid $LOGWRITES_DEV
*generic/483     [not run] require //localhost/scratch to be valid block disk
*generic/484     [not run] require //localhost/scratch to be valid block disk
*generic/485     [not run] xfs_io finsert  failed (old kernel/wrong fs?)
*generic/486     - output mismatch (see /home/smfrench/xfstests-dev/results//generic/486.out.bad)
    --- tests/generic/486.out   2020-01-24 17:11:18.812855321 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/486.out.bad        2020-06-14 15:50:18.436809212 -0500
    @@ -1,2 +1,5 @@
     QA output created by 486
    +touch: cannot touch '/mnt-local-xfstest/test/syscalltest': Permission denied
    +Operation not supported
    +error at line 66
     Attribute "world" has a NNNN byte value for SCRATCH_MNT/hello
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/486.out /home/smfrench/xfstests-dev/results//generic/486.out.bad'  to see the entire diff)
*generic/487     [not run] This test requires a valid $SCRATCH_LOGDEV
*generic/488     [not run] Filesystem cifs not supported in _scratch_mkfs_sized
*generic/489     [not run] require //localhost/scratch to be valid block disk
*generic/490     [failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/490.out.bad)
    --- tests/generic/490.out   2020-01-24 17:11:18.812855321 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/490.out.bad        2020-06-14 15:50:19.620798446 -0500
    @@ -1 +1,3 @@
     QA output created by 490
    +seek sanity check failed!
    +(see /home/smfrench/xfstests-dev/results//generic/490.full for details)
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/490.out /home/smfrench/xfstests-dev/results//generic/490.out.bad'  to see the entire diff)
*generic/491     [not run] cifs does not support freezing
*generic/492     [not run] xfs_io label  failed (old kernel/wrong fs?)
*generic/493     [not run] Dedupe not supported by scratch filesystem type: cifs
*generic/494     - output mismatch (see /home/smfrench/xfstests-dev/results//generic/494.out.bad)
    --- tests/generic/494.out   2020-01-24 17:11:18.812855321 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/494.out.bad        2020-06-14 15:50:20.720788459 -0500
    @@ -1,6 +1,36 @@
     QA output created by 494
    +rm: cannot remove '/mnt-local-xfstest/scratch/dir0740': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/options.h': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/fgetline.h': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/error.h': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/options.c': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/fgetline.c': Permission denied
*generic/495     - output mismatch (see /home/smfrench/xfstests-dev/results//generic/495.out.bad)
    --- tests/generic/495.out   2020-01-24 17:11:18.812855321 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/495.out.bad        2020-06-14 15:50:21.116784866 -0500
    @@ -1,4 +1,32 @@
     QA output created by 495
    +rm: cannot remove '/mnt-local-xfstest/scratch/dir0740': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/options.h': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/fgetline.h': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/error.h': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/options.c': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/fgetline.c': Permission denied
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/495.out /home/smfrench/xfstests-dev/results//generic/495.out.bad'  to see the entire diff)
*generic/496     [not run] fallocated swap not supported here
*generic/497     [not run] xfs_io fcollapse  failed (old kernel/wrong fs?)
*generic/498     [not run] require //localhost/scratch to be valid block disk
*generic/499     [not run] xfs_io fcollapse  failed (old kernel/wrong fs?)
*generic/500     [not run] require //localhost/scratch to be valid block disk
*generic/501     [not run] Reflink not supported by scratch filesystem type: cifs
*generic/502     [not run] require //localhost/scratch to be valid block disk
*generic/503     [not run] xfs_io fcollapse  failed (old kernel/wrong fs?)
*generic/504 0s
*generic/505     [not run] cifs does not support shutdown
*generic/506     [not run] cifs does not support shutdown
*generic/507     [not run] file system doesn't support chattr +AsSu
*generic/508     [not run] lsattr not supported by test filesystem type: cifs
*generic/509     [not run] O_TMPFILE is not supported
*generic/510     [not run] require //localhost/scratch to be valid block disk
*generic/511     [not run] Filesystem cifs not supported in _scratch_mkfs_sized
*generic/512     [not run] require //localhost/scratch to be valid block disk
*generic/513     [not run] Reflink not supported by scratch filesystem type: cifs
*generic/514     [not run] Reflink not supported by scratch filesystem type: cifs
*generic/515     [not run] Reflink not supported by scratch filesystem type: cifs
*generic/516     [not run] Dedupe not supported by test filesystem type: cifs
*generic/517     [not run] Dedupe not supported by scratch filesystem type: cifs
*generic/518     [not run] Reflink not supported by scratch filesystem type: cifs
*generic/519     [not run] FIBMAP not supported by this filesystem
*generic/520     [not run] require //localhost/scratch to be valid block disk
*generic/523     - output mismatch (see /home/smfrench/xfstests-dev/results//generic/523.out.bad)
    --- tests/generic/523.out   2020-01-24 17:11:18.816855125 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/523.out.bad        2020-06-14 15:50:26.452736636 -0500
    @@ -1,6 +1,5 @@
     QA output created by 523
    +touch: cannot touch '/mnt-local-xfstest/test/syscalltest': Permission denied
     set attr
    +setfattr: /mnt-local-xfstest/scratch/moofile: Invalid argument
     check attr
    -# file: SCRATCH_MNT/moofile
    -user.boo/hoo="woof"
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/523.out /home/smfrench/xfstests-dev/results//generic/523.out.bad'  to see the entire diff)
*generic/524 73s ...  1s
*generic/525     - output mismatch (see /home/smfrench/xfstests-dev/results//generic/525.out.bad)
    --- tests/generic/525.out   2020-01-24 17:11:18.816855125 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/525.out.bad        2020-06-14 15:50:28.144721408 -0500
    @@ -1,2 +1,2 @@
     QA output created by 525
    -7ffffffffffffffe:  61  a
    +pread: Invalid argument
*generic/526     [not run] require //localhost/scratch to be valid block disk
*generic/527     [not run] require //localhost/scratch to be valid block disk
*generic/528 0s
*generic/529     [not run] ACLs not supported by this filesystem type: cifs
*generic/530     [not run] cifs does not support shutdown
*generic/531     - output mismatch (see /home/smfrench/xfstests-dev/results//generic/531.out.bad)
    --- tests/generic/531.out   2020-01-24 17:11:18.816855125 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/531.out.bad        2020-06-14 15:50:29.256711420 -0500
    @@ -1,2 +1,26 @@
     QA output created by 531
    +open?: No such file or directory
    +open?: No such file or directory
    +open?: No such file or directory
    +open?: No such file or directory
    +open?: No such file or directory
    +open?: No such file or directory
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/531.out /home/smfrench/xfstests-dev/results//generic/531.out.bad'  to see the entire diff)
*generic/532 0s
*generic/533 0s ... - output mismatch (see /home/smfrench/xfstests-dev/results//generic/533.out.bad)
    --- tests/generic/533.out   2020-01-24 17:11:18.816855125 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/533.out.bad        2020-06-14 15:50:29.892705712 -0500
    @@ -1,4 +1,5 @@
     QA output created by 533
    +touch: cannot touch '/mnt-local-xfstest/test/syscalltest': Permission denied

     create file foo.533

    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/533.out /home/smfrench/xfstests-dev/results//generic/533.out.bad'  to see the entire diff)
*generic/534     [not run] require //localhost/scratch to be valid block disk
*generic/535     [not run] require //localhost/scratch to be valid block disk
*generic/536     [not run] cifs does not support shutdown
*generic/537     [not run] FSTRIM not supported
*generic/538     [not run] Need device logical block size(4096) < fs block size(1024)
*generic/539     [failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/539.out.bad)
    --- tests/generic/539.out   2020-01-24 17:11:18.816855125 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/539.out.bad        2020-06-14 15:50:31.584690552 -0500
    @@ -1,2 +1,4 @@
     QA output created by 539
     Silence is golden
    +seek sanity check failed!
    +(see /home/smfrench/xfstests-dev/results//generic/539.full for details)
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/539.out /home/smfrench/xfstests-dev/results//generic/539.out.bad'  to see the entire diff)
*generic/540     - output mismatch (see /home/smfrench/xfstests-dev/results//generic/540.out.bad)
    --- tests/generic/540.out   2020-01-24 17:11:18.816855125 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/540.out.bad        2020-06-14 15:50:32.500682358 -0500
    @@ -1,14 +1,69 @@
     QA output created by 540
    +rm: cannot remove '/mnt-local-xfstest/scratch/dir0740': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/options.h': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/fgetline.h': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/error.h': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/options.c': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/fgetline.c': Permission denied
*generic/541     - output mismatch (see /home/smfrench/xfstests-dev/results//generic/541.out.bad)
    --- tests/generic/541.out   2020-01-24 17:11:18.816855125 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/541.out.bad        2020-06-14 15:50:33.188676210 -0500
    @@ -1,16 +1,71 @@
     QA output created by 541
    +rm: cannot remove '/mnt-local-xfstest/scratch/dir0740': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/options.h': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/fgetline.h': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/error.h': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/options.c': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/fgetline.c': Permission denied
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/541.out /home/smfrench/xfstests-dev/results//generic/541.out.bad'  to see the entire diff)
*generic/542     [not run] Reflink not supported by scratch filesystem type: cifs
*generic/543     [not run] Reflink not supported by scratch filesystem type: cifs
*generic/544     [not run] Reflink not supported by scratch filesystem type: cifs
*generic/545     [not run] file system doesn't support chattr +i
*generic/546     [not run] Reflink not supported by scratch filesystem type: cifs
*generic/547     [not run] require //localhost/scratch to be valid block disk
*generic/548     [not run] No encryption support for cifs
*generic/549     [not run] No encryption support for cifs
*generic/550     [not run] No encryption support for cifs
*generic/551 319s ...  266s
*generic/552     [not run] require //localhost/scratch to be valid block disk
*generic/553     [not run] xfs_io chattr +i failed (old kernel/wrong fs?)
*generic/554     - output mismatch (see /home/smfrench/xfstests-dev/results//generic/554.out.bad)
    --- tests/generic/554.out   2020-01-24 17:11:18.820854930 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/554.out.bad        2020-06-14 15:55:01.602619107 -0500
    @@ -1,3 +1,60 @@
     QA output created by 554
    +rm: cannot remove '/mnt-local-xfstest/scratch/dir0740': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/options.h': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/fgetline.h': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/error.h': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/options.c': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/fgetline.c': Permission denied
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/554.out /home/smfrench/xfstests-dev/results//generic/554.out.bad'  to see the entire diff)
*generic/555     [not run] xfs_io chattr +ia failed (old kernel/wrong fs?)
*generic/556     [not run] cifs does not support casefold feature
*generic/557     [not run] require //localhost/scratch to be valid block disk
*generic/558     [not run] cifs does not have a fixed number of inodes available
*generic/559     [not run] duperemove utility required, skipped this test
*generic/560     [not run] duperemove utility required, skipped this test
*generic/561     [not run] duperemove utility required, skipped this test
*generic/562     [not run] Reflink not supported by scratch filesystem type: cifs
*generic/563     [not run] cgroup2 not mounted on /sys/fs/cgroup
*generic/564     [not run] cifs does not support mknod/mkfifo
*generic/565 0s
*generic/566     [not run] disk quotas not supported by this filesystem type: cifs
*generic/567 0s
*generic/568 0s ... - output mismatch (see /home/smfrench/xfstests-dev/results//generic/568.out.bad)
    --- tests/generic/568.out   2020-01-24 17:11:18.820854930 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/568.out.bad        2020-06-14 15:55:03.794604659 -0500
    @@ -1,4 +1,4 @@
     QA output created by 568
     wrote 2/2 bytes at offset block_size - 1
     XXX Bytes, X ops; XX:XX:XX.X (XXX YYY/sec and XXX ops/sec)
    -OK: File did not grow.
    +ERROR: File grew from 0 B to 8192 B when writing to the fallocated range.
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/568.out /home/smfrench/xfstests-dev/results//generic/568.out.bad'  to see the entire diff)
*generic/569     - output mismatch (see /home/smfrench/xfstests-dev/results//generic/569.out.bad)
    --- tests/generic/569.out   2020-01-24 17:11:18.820854930 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/569.out.bad        2020-06-14 15:55:04.638599105 -0500
    @@ -1,4 +1,33 @@
     QA output created by 569
    +rm: cannot remove '/mnt-local-xfstest/scratch/dir0740': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/options.h': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/fgetline.h': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/error.h': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/options.c': Permission denied
    +rm: cannot remove '/mnt-local-xfstest/scratch/samefile-2.14/fgetline.c': Permission denied
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/569.out /home/smfrench/xfstests-dev/results//generic/569.out.bad'  to see the entire diff)
*generic/570     [not run] require //localhost/scratch to be valid block disk
*generic/571     [not run] Require fcntl advisory locks support
*generic/572     [not run] fsverity utility required, skipped this test
*generic/573     [not run] fsverity utility required, skipped this test
*generic/574     [not run] fsverity utility required, skipped this test
*generic/575     [not run] fsverity utility required, skipped this test
*generic/576     [not run] fsverity utility required, skipped this test
*generic/577     [not run] fsverity utility required, skipped this test
*generic/578     [not run] Reflink not supported by test filesystem type: cifs
*generic/579     [not run] fsverity utility required, skipped this test
*generic/580     [not run] No encryption support for cifs
*generic/581     [not run] No encryption support for cifs
*generic/582     [not run] No encryption support for cifs
*generic/583     [not run] No encryption support for cifs
*generic/584     [not run] No encryption support for cifs
*generic/585     [not run] kernel doesn't support renameat2 syscall
*generic/586     - output mismatch (see /home/smfrench/xfstests-dev/results//generic/586.out.bad)
    --- tests/generic/586.out   2020-06-14 15:13:59.924160846 -0500
    +++ /home/smfrench/xfstests-dev/results//generic/586.out.bad        2020-06-14 15:55:13.522540923 -0500
    @@ -1,2 +1,66 @@
     QA output created by 586
    +[0]: sbuf.st_size=1048576, expected 2097152.
    +[1]: sbuf.st_size=1048576, expected 2097152.
    +[2]: sbuf.st_size=1048576, expected 2097152.
    +[3]: sbuf.st_size=1048576, expected 2097152.
    +[4]: sbuf.st_size=1048576, expected 2097152.
    +[5]: sbuf.st_size=1048576, expected 2097152.
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/586.out /home/smfrench/xfstests-dev/results//generic/586.out.bad'  to see the entire diff)
*generic/587     [not run] disk quotas not supported by this filesystem type: cifs
*generic/588     [not run] require //localhost/scratch to be valid block disk
*generic/589     [not run] require //localhost/scratch to be local device
*generic/590      7s
*generic/591      0s [TBD: check if this is really skipped]
*generic/592     [not run] No encryption support for cifs
*generic/593     [not run] No encryption support for cifs
*generic/594     [not run] disk quotas not supported by this filesystem type: cifs
*generic/595     [not run] No encryption support for cifs
*generic/596     [not run] accton utility required, skipped this test
*generic/597     [not run] 123456-fsgqa user not defined.
*generic/598     [not run] 123456-fsgqa user not defined.
*generic/599     [not run] cifs does not support shutdown
*generic/600     [not run] disk quotas not supported by this filesystem type: cifs
*generic/601     [not run] disk quotas not supported by this filesystem type: cifs