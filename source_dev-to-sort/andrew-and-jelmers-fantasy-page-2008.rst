Samba4/Andrew and Jelmers Fantasy Page/2008
    <namespace>0</namespace>
<last_edited>2010-09-30T08:18:32Z</last_edited>
<last_editor>JelmerVernooij</last_editor>

==============================

lans for fortnight ending 27 December 2008
===============================

*Trusted domains (Andrew)
**Reproduce metze's sucess trusting a Win2k3 domain 
**Reproduce metze's issue being trusted by a Samba3 domain
*Make preperations for a alpha release
**Fixing build farm failures (Andrew and Jelmer)
**Testing a 'real' deployment (Andrew)
**Write release notes (Jelmer)
Achieved so far
------------------------

*Proper Extended DN support (Andrew)
**Pushed into the master branch
*Shared object files for gen_ndr files between Samba 3 and Samba 4 (Jelmer)
*rewrote SWIG-based Python modules in manual C (Jelmer)
*made Samba 4 in merged build use shared libraries when possible (Jelmer)
*fixed several issues building the standalone libraries (Jelmer)
*prepared Debian package of tevents and packaged new versions of talloc, tdb and ldb (Jelmer)

===============================
Plans for fortnight ending 13 December 2008
===============================

Achieved so far
------------------------

*Added interactive mode to setup/provision (Jelmer)
*Proper Extended DN support (Andrew)
**Published final patch to list for review
*Use Microsoft's full AD Schema in Samba4 (Andrew)
**Conversion script taken on by Sreepathi Pai
**Working with Microsoft to correct errors in the schema

===============================
Plans for fortnight ending 29 November 2008
===============================

Archived so far
------------------------

*Proper Extended DN support (Andrew)
**continued work on implementation and testing

===============================
Plans for fortnight ending 15 November 2008
===============================

*Research to check about transitive trusts between AD and MIT realms (Andrew)
*Proper Extended DN support (Andrew)
**Needed for Samba3 domain members in a Samba4 domain.
*Make a Samba4 release
**Needed for OpenChange, and to give users a solid alpha to test
Achieved so far
------------------------

*Increase to tridge's blood pressure (Andrew)
**Tridge and I worked to learn python and start an 'upgrade_samba4' script to assist users who have to re-provision but do not wish to loose data.
*Proper Extended DN support (Andrew)
**Posted initial implementation to mailing list for comment

===============================
Plans for fortnight ending 1 November 2008
===============================

*Finish 'unicode' password issues with integration of new charset (Andrew)
**The character set conversion needs to change invalid sequences to a known 'bad' value
*Proper Extended DN support (Andrew)
**Needed for Samba3 domain members in a Samba4 domain.
*Unique Index support (Andrew)
**Needed to ensure we don't have more than one 'Administrator' in a domain (for example)
*Allow registration in endpoint mapper (Jelmer)
*ncacn_http (Jelmer)
*Research to check about transitive trusts between AD and MIT realms (Andrew)
Achieved so far
------------------------

*Fix kpasswd server to not 'exit(10)' the whole of Samba (Andrew)
**Found by Apple at the CIFS plugfest
*Reconciled more library code between Samba 3 and 4 (Jelmer)
** lib/util
** librpc/gen_ndr
** librpc/ndr
*Repel pstring to nsswitch/ (Jelmer)
*Move crypt() replacement to libreplace (Jelmer)
*Enable merged-build automatically in developer builds (Jelmer)
*Merged Matthias' registry server improvements (Jelmer)
*Split up selftest code into a Samba4-specific and a generic part (Jelmer)
*Fix blackbox tests on IPv6-only hosts (Jelmer)
*[http://people.samba.org/people/2008/10/22#a-year-since-microsofts-appeal-failed Blog posting] about interopability with Microsoft (Andrew)
**Now found on [http://lwn.net/Articles/304499/ LWN] and [http://linux.slashdot.org/linux/08/10/23/1441200.shtml Slashdot]

===============================
Plans for fortnight ending 18 October 2008
===============================

*Use separate structure for gensec settings (Jelmer)
*Share DEBUG() code between Samba 3 and Samba 4 (Jelmer)
**In preparation of merging my libutil-share branch
*More work getting WMI back to work (Jelmer)
Achieved so far
------------------------

*Implement a 'unicode' password pass-down mechanism in LDB
**This fixes domain trust problems where member servers select a compleatly random password
**We still need to fix this for kerberos hash types (awating charset work by tridge)

===============================
Plans for fortnight ending 4 October 2008
===============================

*Implement a 'unicode' password pass-down mechanism in LDB, or otherwise avoid UCS2 -> UTF8 -> UCS2 problems
*Trusted domain support (LSA and KDC portions) (Andrew)
Achieved so far
------------------------

*Separate out and add tests for Subunit (Jelmer)
*Remove global_loadparm use in a couple more places (Jelmer)
*Restructure some of the installation bits together with Matthias (Jelmer)

===============================
Plans for fortnight ending 20 september 2008
===============================

*wmi integration (Jelmer)
*hdb_samba4 (Jelmer)
*eliminate last EJS (minschema.js, samba3sam.js (Jelmer))
*Trusted domain support (LSA and KDC portions) (Andrew)
Achieved so far
------------------------

*Committed merged build patch to Samba 3 (Jelmer)
*Made Samba 3 and Samba 4 use the same copy of tdb, talloc, compression, replace, nss_wrapper, socket_wrapper, popt (Jelmer)
*Committed WMI support to the repository (doesn't compile completely yet though) (Jelmer)
*Fixed samba3sam.js and removed remaining JavaScript support. (Jelmer)
*Implemented WSGI standard (http://www.python.org/dev/peps/pep-0333/) support in web_server.

===============================
Plans for fortnight ending 6 september 2008
===============================

*wmi integration (Jelmer)
*upload samba-gtk into Debian (Jelmer)
*hdb_samba4 (Jelmer)
*send out patch for merged franky build (Jelmer)
*Use franky build for personal Samba4 development (Andrew)
*eliminate last EJS (minschema.js, samba3sam.js (Jelmer))
*Trusted domain support (LSA and KDC portions) (Andrew)
Achieved so far
------------------------

*Update NTP patch (Andrew)
*Respond to comments and suggestions on RPMs for Fedora (Andrew)
*(partial) Trusted domain support (LSA and KDC portions) (Andrew)
*PAC Verification support over NETLOGON (Andrew)
*Sent out franky merged build patch, more prerequisites fixed for Franky (Jelmer)

===============================
Plans for fortnight ending 23 august 2008
===============================

Achieved so far
------------------------

* slacking off (Jelmer)
* Lots of questions to Microsoft on trusted domains and PAC validation (Andrew)
* Build indexes and attributes directly from the schema, not a hard-coded list (Andrew)
* Generate the cn=Aggregate schema in Samba4, rather than in minschema.js
**This prepares us for adding arbitrary schema into Samba4
* Integrate patches for multi-master OpenLDAP configuration (Andrew)
** This allows a Samba4 provision-backend to create a multi-master backend, without hand-manipulation by the admin
* Start of work on trusted domains
**In our KDC, start with a special case for handling the trusted domains principals
**In the drsblobs.idl, parse the trustAuthIncoming and trustAuthOutgoing blobs

===============================
Plans for fortnight ending 9 August 2008
===============================

*Fix AES compatability with Windows 2008/Vista. (Andrew)
**It turns out that Metze was starting to chase the same bug
**The fix is to implement gss_wrap_ex() - ie AEAD, the signing of headers in DCE/RPC packets. 
**Earlier 'use Heimdal for SPNEGO' work is forming a very useful basis for this work
*Look at smartcard login again (Andrew)
**Bugs in Dogtag have been allegedly fixed.
*Trusted domains (Andrew)
**Add support for trusted domains in our KDC
Achieved so far
------------------------

===============================
Plans for fortnight ending 26 July 2008
===============================

Achieved so far
------------------------

*Fix LDAP backend to be secure (not anonymous access) (Andrew)
*Partially Fix vista join bugs due to AES and GSSAPI CFX (Andrew with Tridge)
**Session keys for smb signing are original length (ie, 32 in this case)
**Session keys for SAMR encryption are 16 (ie, truncated)
**Still need to fix GSSAPI encryption for the AES case (it uses AEAD, as seen in NTLM2)
*Phone calls with Microsoft (Andrew)
**I now have a regular phone hookup with Microsoft to go over pending issues in the WSPP process
*Fix 'file not found' errors from clients (Andrew with Tridge)
**Due to an uninitialised variable, introduced in some recent SMB2 work
**shows up on systems with extended attributes (typically those using SeLinux, such as Fedora)
**Perhaps a good reason to push out a new alpha soon

===============================
Plans for fortnight ending 12 July 2008
===============================

*wmi integration (Jelmer)
*upload openchange and samba-gtk into Debian (Jelmer)
*hdb_samba4 (Jelmer)
*eliminate last EJS (minschema.js, samba3sam.js)
*Improve LDAP backend from a technology preview to a deployable system (Andrew)
Achieved so far
------------------------

*Continue packaging of OpenChange and Samba4 for Fedora
*Start work on smart card login (Andrew)
**Including setting up DogTag certificate system (Andrew)
**At least to the stage of the first crashes...
*Rework schema handling to know about auxillary classes (Andrew)
**Try to do this in common between ad2OLschema and the kludge_acl and objectclass modules.

===============================
Plans for fortnight ending 28 June 2008
===============================

*external Heimdal use (Andrew)
Achievements
------------------------

*Created Samba 4 and OpenChange RPM packages (Andrew)
*test TEST_LDAP=yes (Andrew)
*Fixed Franky build for odd make versions (Jelmer)

===============================
Plans for fortnight ending 14 June 2008
===============================

*Linked attributes for 'net vampire' (Andrew)
*AES Key support (check with docs and Win2008 on format) in samdb (Andrew)
*Work to make ldb merge easier for Simo (andrew)
*Any work required to merge NTP patch with ntp.org distribution (Andrew)
*Work with alpha testers on any issues that come up in production deployments of Samba4 (Andrew)
Achieved so far
------------------------

*Samba4 alpha4 release (andrew)
**without LDB merge, which seems a while off yet
*Sync ldap.py test with it's (now obsolete) ldap.js predecessor (andrew)
*Add python bindings for NetBIOS (Jelmer)
*Improve portability of Franky build (Jelmer)
*Asked Microsoft about AES key formats (Andrew)
**Just getting the data from Win2008 failed due to other reasons
*Continued the battle with Microsoft over NTP documentation (Andrew)
*Worked on package of Heimdal for Fedora (Andrew)
**As a preview to packaging Samba4 for Fedora

===============================
Plans for fortnight ending 31 May 2008
===============================

*Linked attributes for 'net vampire' (andrew)
*Make a Samba 4.0 alpha4 release if the ldb branch gets merged
http://packages.debian.org/testing/python/python-wmi (Jelmer)
Achieved so far
------------------------

*Implement NTP signing (andrew)
**Patch posted to ntp.org for consideration: [http://bugs.ntp.org/1028 NTP bugzilla item with patch]
**ntp_signd now started by default in samba4
*Finish CLDAP and NBT netlogon parsing. (Andrew) 
**Including expected value tests (critical to ensuring we return the *right* answer)
**This should help things like Group Policy, which rely on this 'DC ping' functionality
*Merge Simo's ldb branch with current v4-0-test (abartlet)
**Should make Simo's merge task easier. 
*Removed smbpython and restructured Python modules hierarchy to not clutter Python namespace (Jelmer)
*Merged improvements made by Wilco and Jelmer to the registry during SambaXP (Jelmer)
*Added documentation to most Python modules and improved descriptions. (Jelmer)
*Fixed memory bug in autogenerated DCE/RPC Python bindings (Jelmer)
*Several test infrastructure improvements. (Jelmer)
**Print full test path for easy inclusion in knownfail lists
**Make test case name part of test name to allow a test to have different results against different test cases
**Set PYTHONPATH during test runs
*Removed unused old EJS DCE/RPC bindings and testscripts (Jelmer)
*Make it easier to use various libraries externally without including all of Samba 4's build system (Jelmer)
*Updated Samba 4, OpenChange and Samba-Gtk Debian packages, now passes lintian. (Jelmer)
** Announced on the OpenChange website (http://www.openchange.org/index.php?option=com_content&task=blogsection&id=7&Itemid=77)
*Added Python bindings for IRPC / Messaging interfaces (Jelmer)
**Rewrote smbstatus in Python
*Added mechanism for doing "raw" DCE/RPC requests from Python (Jelmer)
**Also initial work on a script that should attempt to figure out IDL by probing
*Exposed more DCE/RPC internals from Python bindings (Jelmer)
*Initial work on [http://www.python.org/dev/peps/pep-0333/ WSGI] implementation in web_server/ (Jelmer)
*Added combined buildsystem for `Franky`

===============================
Plans for fortnight ending 17 May 2008
===============================

*Fix our CLDAP netlogon processing to match description in [MS-ADTS] 7.3.3 (andrew)
**Use this to fix and test group policy handling on Win2000 and WinXP clients
Achieved so far
------------------------

*Partial security=server implementation, awaiting VFS proxy merge for testing (Andrew)
*Removed a large number of dead build farm hosts in response to automated mails (Andrew)
*Brought back old (D)COM code and made it compile again (Jelmer)
*Merged GNU make branch (Jelmer)
**Now allows using system Python with Samba Python modules
*Finished Samba 4 Debian package together with Christian (Jelmer)
*Updated Debian packages for OpenChange and Samba-Gtk (Jelmer)
*Most of the parsing work towards the CLDAP/NBT netlogon consolidation (Andrew)

===============================
Plans for fortnight ending 3 May 2008
===============================

*Build Farm improvements
**See if we can use SQLite to get a bit more done
**make build farm summary page use sqlite
**host list, by last reported time
**last reported time on host individual page
*Finish security=server re-implementation in Samba4
*Finish ncacn_http implementation
Achieved so far
------------------------

*Very useful Visit to Sam's home company for 2 days
**Chat with principals to encourage them
**Jelmer prepared WAFS branch for merging
***Looks like further development will be upstream, which is great
**Jelmer did some initial work on tests for proxy code
**Andrew Started work on 'security=server' re-implementation for Samba4
***This will allow WAFS to hijack an unsigned connection as a man in the middle attack. 
**Andrew fixed 'make test' to fail if PIDL tests fail
*Build Farm
**make build farm send e-mails to dead hosts (based on SQLite database)
===============================
Achievements for fortnight ending ending 19 April 2008
===============================

SambaXP
------------------------

*Successfully gave 3 presentations
**Samba4 status report (Both)
**Samba4 and the LDAP backend / Little barber shop of horrors (Andrew)
**RPC Scripting using Python (Jelmer)
*Worked with Sam Liddicott
**He has implemented the start of a WAFS (latency reducing) proxy for Samba4
**Organised to visit his companies office
*Improved code coverage to give better 'headline' figure for presentation (Andrew)
**Working with Kai's winbind work to run metze's structure based tests
**Kai worked on blackbox tests
**Required fixing up parts of winbind (untested code is broken code, Andrew)
*Fixed bugs in Pidl reported by Volker (Jelmer)
*Added knownfailure support in test code (Jelmer)
*Split out policy library into separate git repository (Jelmer)
*Worked with Wilco on more registry tests (Jelmer)
*Fixed several Python usability bits (Jelmer)
*Fixed duplication in blackbox tests (Jelmer)
*Initial work on ncacn_http support (Jelmer)
*Discussions with Guenther, Michael about reconciling registry, libsmbdotconf and smbdotconf in Samba 3 and 4 (Jelmer)