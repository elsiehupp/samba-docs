Samba4/LDB
    <namespace>0</namespace>
<last_edited>2010-01-08T02:31:08Z</last_edited>
<last_editor>Edewata</last_editor>

=================
API
=================

* `Samba4/CLI | CLI`
** `Samba4/CLI/NTSTATUS | NTSTATUS`
* `Samba4/LDB/API | LDB`
* `Samba4/LDB/DN | DN`
* `Samba4/LDB/Message | Message`
* `Samba4/LDB/Request | Request`
* `Samba4/LDB/Reply | Reply`
* `Samba4/LDB/Module | Module`
* `Samba4/LDB/Schema | Schema`
* `Samba4/LDB/Map | Map`
* `Samba4/LDB/Filter | Parse Tree`
* `Samba4/LDB/Control | Control`
* `Samba4/LDB/LDIF | LDIF`

=================
Modules
=================

* `Samba4/LDB/Object Class | Object Class`
* `Samba4/LDB/Paged Searches | Paged Searches`
* `Samba4/LDB/Partition | Partition`