GSoC 2011 Samba-gtk Summary
    <namespace>0</namespace>
<last_edited>2011-06-02T13:53:25Z</last_edited>
<last_editor>Dhananjaysathe</last_editor>

**Samba-Gtk Google Summer of Code (GSoC ) 2011 Outline Plan** 

Project Details 
------------------------

* Name of Student : Dhananjay Sathe 
* Proposal Name : Samba Control Centre. [http://socghop.appspot.com/gsoc/project/google/gsoc2011/dhananjaysathe/22004]
* Mentor : Jelmer Vernooij  [http://www.samba.org/~jelmer/]

Project Outline 
------------------------

The following is the basic outline of what I plan to do as a part of my Google Summer of Code project .
----

* In the first phase :-
**One of the prerequisites for the implementation of my projects happens to be the ability to add local and remote shares (folders/printers etc).This functionality is currently not present in Samba-Gtk. Thus my initial primary focus shall be to add this feature functionality to the present tool set.This involves creating the required back-end and the Gtk+ interface akin to the present tools present in Samba-Gtk.<br />
**Unlike currently available GUI tools that entail the direct manipulation of the smb.conf file to configure samba and also in step with the rest of the Samba-Gtk tools this will be done using the samba-python bindings via the dcerpc bindings (in this case specifically the srvsvc methods), thus eliminating a range of complications and also keeping it in step with the Samba-4 recommendations.Additionally this approach allows for configuration of samba3 based hosts and even Windows hosts.<br /><br />
*In the second phase I would like to implement a simple questions driven set of interactive wizard driven GUI tools with features such as tool-tips, easy self explanatory text fields, etc on the lines of tools such as those found for say Dropbox or Ubuntu-One .To achieve the basic configuration functions such as adding or removing users, groups and shares. Thus giving an uncomplicated and easier interface to enable new users to get through these tasks in a breeze and providing for a better user experience. 

Contact Details 
------------------------

Feel free to contact me with any comments,suggestions and queries at :
* Email / gtalk : dhananjaysathe@gmail.com
* IRC handle/nick : dsathe 
( Will be available  on #samba and #samba-technical on freenode) [http://www.samba.org/samba/irc.html]
* Jabber : dhananjaysathe@jabber.org

References 
------------------------

*[http://socghop.appspot.com/gsoc/project/google/gsoc2011/dhananjaysathe/22004 My GSoC Proposal]
*[http://www.samba.org/~jelmer/ Jelmer Vernooij]

External links 
------------------------

* [http://www.samba.org/ Samba.org Homepage]
* [http://wiki.samba.org/index.php/SambaGtk Samba-Gtk Homepage ]
* [http://www.samba.org/samba/irc.html Samba IRC Channels ]