Autobuild
    <namespace>0</namespace>
<last_edited>2020-06-03T22:24:33Z</last_edited>
<last_editor>Abartlet</last_editor>

Samba's Continuous Integration (CI) process is called *Autobuild*. [https://git.samba.org/?p=samba.git;a=blob;f=script/autobuild.py autobuild.py] is a python script that verifies Samba builds, installs, and passes all automated tests successfully. Autobuild is generally invoked for CI automatically (e.g. via git hooks) rather than run manually.

Autobuild is more than just a wrapper for 'make test'. It runs additional tests that <tt>make test</tt> doesn't cover, such as the CTDB tests.
Autobuild also builds various parts of the Samba project in different ways, for example using the LDB system library as well building the in-tree LDB source code.
Running tests against these different build permutations ensures the main supported Samba configurations always work.

A successful autobuild is a gating step in delivering code to the master Samba branch.
Autobuild is run on a host called <tt>sn-devel</tt> that only Samba Team members have access to.
The script automatically rebases the commits under test to be on current master, so that if another change is merged into master first, testing restarts. 
Therefore, the Samba master branch **always passes** all tests.

Note that because autobuild is the gating step in code delivery, Samba Team members have the habit of (somewhat confusingly) using the term 'autobuild' to refer to delivering code, e.g. "I've pushed your code to autobuild".

Autobuild on sn-devel to push code (eg to master)
------------------------

[https://www.samba.org/samba/team Samba team members] have a git alias <tt>git autobuild</tt> configured that pushes the current branch to a magic location on the <tt>sn-devel</tt> server.  If the scheduled run of <tt>script/autobuild.py</tt> passes on that server, the code will be marked and pushed to master via the <tt>--pushto</tt> and <tt>--mark</tt> parameters.  

***Bold text:** the actual process is a bit more complex than this*

Methods of running Autobuild
------------------------

While <tt>autobuild.py</tt> can run on any Linux machine, the official results are only accepted on the <tt>sn-devel</tt> host.
However, only Samba Team members can access <tt>sn-devel</tt>, and it's only a single host with limited CPU resources.
So there are other methods that allow developers to get CI results in a similar way:

* **Gitlab**. `Samba on GitLab|Samba CI on gitlab` is the preferred way for developers to run autobuild. Some tests can run against public 'runners', which gitlab provides for free. However, other tests need a specially configured CI box, due to memory or ext4 filesystem requirements. [https://git.samba.org/?p=samba.git;a=blob;f=.gitlab-ci.yml .gitlab-ci.yml] implements Samba CI on Gitlab.

* **Manually**. Autobuild can also be run manually. A typical invocation of autobuild is:
<pre>script/autobuild.py --testbase=/tmp</pre>
* This can be run on any Linux system with the `Build_Samba_from_Source|right packages` installed. The current reference system has recently been switched over to Ubuntu 18.04 (for a long time it was Ubuntu 14.04).

* **Openstack**. <tt>autobuild.py</tt> should operate on a virtual machine in any [https://www.openstack.org/ OpenStack] cloud.

Autobuild tests *everything* in the Samba codebase. However, the <tt>autobuild.py</tt> script defines a series of tasks, which allows many tests to be run in parallel. Because of this parallelization,  a gitlab CI run usually takes about an hour to complete.

Potential improvements to Autobuild
------------------------

Autobuild tests everything in the Samba codebase, which means it takes several hours to run. The autobuild.py script defines a series of independent tasks, which means some tests can be run in parallel, but only the cloud-based runners behind [Samba on GitLab|GitLab CI] fully takes advantage of that, as ``sn-devel`` runs on one physical server.

The other problem is sometimes tests fail for unknown reasons at random times, i.e. the tests ‘flap’. A bot runs on ``sn-devel`` four times per day and e-mails the [https://lists.samba.org/archive/samba-cvs/ samba-cvs mailing-list] if any failures are detected. This allows developers to see which tests, perhaps newly introduced, caused the failure.

Tests can be marked as ‘flapping’ (either in [https://git.samba.org/?p=samba.git;a=blob;f=selftest/flapping selftest/flapping] or [https://git.samba.org/?p=samba.git;a=tree;f=selftest/flapping.d selftest/flapping.d]), which means the test case still gets run, but the test result is essentially ignored. Obviously, this practice is discouraged as it reduces the usefulness of the test.

More work could be done to investigate these intermittent test failures and fix them.

`Category:Testing`