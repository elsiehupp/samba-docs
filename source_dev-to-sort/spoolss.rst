Spoolss
    <namespace>0</namespace>
<last_edited>2010-04-14T21:45:00Z</last_edited>
<last_editor>GlaDiaC</last_editor>

=================
Spoolss / TODO list
=================

* torture and win32 tests to validate winreg and spoolss PrinterData consistency
* torture and win32 tests for XcvData
* torture and win32 tests for truncated devicemodes
* torture and win32 tests for printer ports
* torture and win32 tests for ChangeNotify
* torture and win32 tests for PRINTER_INFO_STRESS (level 0)
* torture and win32 tests for alignement in particular of EnumPrinterDataEx
* fix subkey implementation when subkeys are created via PrinterData functions
* check for / key and valuenames
* fix marshalling of spoolss_PrinterDriverInfo101

=================
Detach registry from spoolss implementation
=================

In order to better separate and detach the printing code (spoolss server) and eventually to be able to move out spoolss out of smbd entirely into a separate process, we need to remove various dependencies, including:

* winreg server dynamically loads ntprinters.tdb in order to expose spoolss PrinterData registry content
* RAP server directly accesses printing and spoolss functions in order to do LANMAN printing

The plan is to talk to a (local) winreg server in order to access the printing data backend via standardized winreg client API.

Global HKLM hive 
------------------------

First we need to have a global hive opened for the spoolss server:

* use rpc_pipe_open_internal() to access winreg server from within spoolss server
* use rpccli_winreg_OpenHKLM() to open HKLM hive and preserve policy handle globally

Spoolss server calls 
------------------------

Then for each spoolss server call, notably:
* SetPrinterData{Ex}
* GetPrinterData{Ex}
* EnumPrinterData{Ex}
* DeletePrinterData{Ex}
* EnumPrinterKey
* DeletePrinterKey 

we need to open the printer specific key and set,get or enum the values directly via winreg API:

* use rpccli_winreg_OpenKey(SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Print\\Printers\\printername) to open printer specific key
* use rpccli_winreg_SetValue() to set value
* use rpccli_winreg_QueryValue() to query value
* use rpccli_winreg_EnumValue() to enumerate values
etc.
* use rpccli_winreg_Close() to close the printer specific key

Eliminate printing registry backend 
------------------------

The dynamic retrieval of PrinterData content currently loaded from ntprinters.tdb needs to be removed.

Migration 
------------------------

It is planned to have no in-place migration of data stored in ntprinters.tdb into registry.tdb. Instead we will provide a tool/at does this migration offline.

RAP callout to spoolss 
------------------------

As the RAP calls for user enumeration currently callout into the samr server, the RAP printing calls need to callout into the spoolss server.

Registry links 
------------------------

Original:

    OFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Print\\Printers
    YSTEM\\CurrentControlSet001

Links:

    YSTEM\\CurrentControlSet -> SYSTEM\\CurrentControlSet001
    YSTEM\\CurrentControlSet001\\Control\\Print\\Printers -> SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Print\\Printers

http://TTsepago.de/helge/20/reeDD/DDAA/AA/ry/nks-reg_link//

References 
------------------------

* [http://Tmicrosoft.com/enDDAASS/ry/cc244870%/TT13%29/ MS-RPRN Print System Remote Protocol Specification]
* [http://Tmicrosoft.com/enDDAASS/ry/cc244870%/TT13%29/#id134 MS-RPRN footnote 134: Persist:stract Data Model in the Registry]
* [http://Tmicrosoft.com/enDDAASS/ry/cc244877(/13)DDOO/DAASSHHRRP: Windows:gistry Protocol Specification]

--`User:8:25, 5 Ma:CST)