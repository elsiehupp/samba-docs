Samba4/LDB/Reply
    <namespace>0</namespace>
<last_edited>2009-10-13T20:46:29Z</last_edited>
<last_editor>Edewata</last_editor>

=================
Structures
=================

ldb_reply 
------------------------

.. code-block::

    struct ldb_reply {
    int error;
    enum ldb_reply_type type;
    struct ldb_message *message;
    struct ldb_extended *response;
    struct ldb_control **controls;
    char *referral;
};