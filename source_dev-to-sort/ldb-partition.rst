Samba4/LDB/Partition
    <namespace>0</namespace>
<last_edited>2009-10-13T21:08:24Z</last_edited>
<last_editor>Edewata</last_editor>

=================
Source Code
=================

The source code is located at [http://gitweb.samba.org/?p=samba.git;a=blob;f=source4/dsdb/samdb/ldb_modules/partition.c source4/dsdb/samdb/ldb_modules/partition.c].

=================
Structures
=================

dsdb_partition 
------------------------

.. code-block::

    struct dsdb_partition {
    struct ldb_module *module;
    struct dsdb_control_current_partition *ctrl;
};

part_request 
------------------------

.. code-block::

    struct part_request {
    struct ldb_module *module;
    struct ldb_request *req;
};

partition_context 
------------------------

.. code-block::

    struct partition_context {
    struct ldb_module *module;
    struct ldb_request *req;
    bool got_success;

    struct part_request *part_req;
    int num_requests;
    int finished_requests;
};

=================
Methods
=================

partition_init() 
------------------------

This method reads the partition configuration from the SAM database, connects to the backends, registers the partitions, load modules, and register controls.

The configuration is stored in the @PARTITION entry in the following attributes:
* partition: Base DN and URL of the partition.
* replicateEntries: Entries to replicate to all partitions.
* modules: Base DN and modules for the partition.

This method registers the following controls:
* domain scope (1.2.840.113556.1.4.1339)
* search options (1.2.840.113556.1.4.1340)

partition_add() 
------------------------

This method intercepts search requests and redirects the request to the appropriate partitions.

partition_modify() 
------------------------

This method intercepts search requests and redirects the request to the appropriate partitions.

partition_delete() 
------------------------

This method intercepts search requests and redirects the request to the appropriate partitions.

partition_rename() 
------------------------

This method intercepts search requests and redirects the request to the appropriate partitions.

partition_search() 
------------------------

This method intercepts search requests, removes domain scope and search options controls, and redirects the request to the appropriate partitions.