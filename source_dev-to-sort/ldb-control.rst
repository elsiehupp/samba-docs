Samba4/ol/
    <namespace>0</namespace>
<last_edited>2009-10-13T20:43:48Z</last_edited>
<last_editor>Edewata</last_editor>

=================
Structures
=================

ldb_control 
------------------------

.. code-block::

    struct ldb_control {
    const char *oid;
    int critical;
    void *data;
};
</

=================
Methods
=================

ldb_request_get_control() 
------------------------

.. code-block::

    struct ldb_control *ldb_request_get_control(struct ldb_request *req, const char *oid);
</

This method checks if a control with the specified "oid" exists and returns it.

Returns: NULL if not found.

ldb_reply_get_control() 
------------------------

.. code-block::

    struct ldb_control *ldb_reply_get_control(struct ldb_reply *rep, const char *oid);
</

This method checks if a control with the specified "oid" exists and returns it.

Returns: NULL if not found.

save_controls() 
------------------------

.. code-block::

    int save_controls(struct ldb_control *exclude, struct ldb_request *req, struct ldb_control ***saver);
</

This method saves the current controls list into the "saver" and replaces the one in req with a new one excluding the "exclude" control.

Returns: 0 on error.

check_critical_controls() 
------------------------

.. code-block::

    int check_critical_controls(struct ldb_control **controls);
</

This method checks if there's any control marked as critical in the list.

Return: True if any, False if none.

ldb_request_add_control() 
------------------------

.. code-block::

    int ldb_request_add_control(struct ldb_request *req, const char *oid, bool critical, void *data);
</

This method adds a control into in the request.

ldb_parse_control_strings() 
------------------------

.. code-block::

    struct ldb_control **ldb_parse_control_strings(struct ldb_context *ldb, void *mem_ctx, const char **control_strings);
</

This method parses controls from the format used on the command line and in ejs.