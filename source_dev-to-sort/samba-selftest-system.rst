The Samba Selftest System
    <namespace>0</namespace>
<last_edited>2020-06-18T20:57:47Z</last_edited>
<last_editor>Abartlet</last_editor>

==============================

resentation Introduction and overview
===============================

Andrew Bartlett's [https://ftp.samba.org/pub/samba/slides/linux.conf.au-2013-abartlet-ci-miniconf-samba.pdf Two years with Samba's autobuild] presentation at Linux.conf.au 2012 is a good place to start understanding our automated test and CI infrastructure.  [https://www.youtube.com/watch?v=EZIUxFgneoo Youtube].

Also see Andreas Schneider's [https://www.samba.org/~asn/sambaxp-2015-andreas_schneider-selftest.pdf SambaXP 2015 presentation] [https://sambaxp.org/archive_data/SambaXP2015-AUDIO1/thu/track3/sambaxp2015-thu-track3-Andreas_Schneider-HowToUseTheSambaSelftestSuite.mp3 Audio]

===============================
`Autobuild`
===============================

`Autobuild` is the top *Pure Samba* layer in the selftest system.

===============================
Public CI using GitLab CI
===============================

There's now also a Samba CI instance on gitlab, available for Samba team members and outside contributors: `Samba on GitLab|Samba CI on gitlab`.  This is the next layer up, controlled by the ``.gitlab-ci.yml`` file in the repository, this runs `Autobuild`.

===============================
`Understanding make test|make test`
===============================

The next layer is ``make test`` which invokes ``selftest.pl`` and filters the results via `subunit`.

===============================
Subunit
===============================

Samba's selftest system is based on `Subunit` output streams, to describe tests that pass, fail and are flapping.