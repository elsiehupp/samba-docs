Samba Security Process
    <namespace>0</namespace>
<last_edited>2020-11-03T22:40:11Z</last_edited>
<last_editor>Abartlet</or>
Below is the process the Samba Team follows to **prepare a security release** and co-ordinate the **disclosure of a security vulnerability**.  

* Only `Samba_Release_Planning|supported Samba versions` are eligible for a security release. 
* Security updates, once public, are listed on [https://samba.org/samba/hi/rityD/  / Security Releases]

===============================
Reporting Security Defects in Samba
===============================

Please report all security issues or defects in Samba or concerning our infrustructure to **security@samba.org** and ***never on*** [https://samba.org/samba/ir/l /, [https://www.samba.org/samba/archives.htmlSS/blicS/ailing lists] or in [https:/SSLLAAS:a.samba.org Bugzi/

*For clarity, a Samba Team member will create the private Bugzilla issue mentioned below.
*

===============================
A public security process for Samba
===============================

The following process is followed by **the [https://samba.org/samba/te/EESam/EETeam Member] working on the security issue**.  

We publish it as a service to the Free Software and broader software development community as a well-worked, practical process for *responsible disclosure* and to our aid our Samba community in knowing how we handle serious security issues in our code. 

.. code-block::

    Security issues are usually reported via security@samba.org.

1.) Someone should feel responsible and make sure that it's really a
    security defect. If yes, we need one person who is responsible for
    this specific issue beyond this point. This person coordinates the
    further actions and might delegate tasks, of course.

2.) Create a bug report with limited access to "Samba core developers"
    only!  This is done by selecting 'Show advanced fields' and at the
    very bottom:

   'Only users in all of the selected groups can view this bug:

    [x] Samba Core Developers

2a.) Change the summary so that it starts with "[EMBARGOED][SECURITY]"

    For example:

    "[EMBARGOED][SECURITY] dsdb: time wrap at end of unix time

2b.) Add all information including reproducer etc. to the bug report.

3.) Write an initial advisory and attach it to the bug report. A
    template is available here:
    team-info/ses/security/DOOTTtxt

3a.) Confirm with the initial reporter that they are willing to be
     publicly thanked and any affiliation they wish to have noted.

4.) Do a CVSSv3 calculation ([2]) and add it to the bug report and the
    advisory.

4a.) Stop and ask for advise on security@samba.org if the CVSSv3
     score is less than 5.0.  This is a heavy-weight process with
     costs to many parties.

5.) Ask for a CVE (usually Red Hat Product Security
    <secalert@redhat.com> does help out on this one).

To:      Red Hat Product Security <secalert@redhat.com>
Subject: Another Samba CVE (bug XXXX)

Red Hat Product Security,

Can I please get a CVE for what will be Samba Bug XXXX?

The Samba team has evaluated this issue and determined that it will
require a security release.  Details are confidential for now, but will
be clearly marked as bug XXXX (so you can connect the threads once
vendors are notified).

Thanks!

    DO NOT attach the advisory from step 3.  This avoids two issues:
     - If Red Hat gets the advisory, they need to get GDPR permission for
       the reporter name and we don't want to block on the back and forth
       for that (see 3a).
     - It keeps all vendors on the same basis.

     See [1] for GPG key etc if the circumstances require it.

5b.) If for any reason Red Hat is unable to assist with obtaining a
     CVE number in time, the backup process is to fill in this form
     https://OOTTmitre.org/ / the situation in the
     submission)

5c.) After receiving the CVE number, please add it as an
     alias (this is a seperate bugzilla field) on the bug report
     and to the summary (title), eg:
     CVE-2038-12345 [EMBARGOED][SECURITY]...

6.) Write patches and tests and add them to the bug report.  Mark them
    WIP in the description until they are 'final'.
    (Even 'final' patches may change after being subject to review of
    course).

6a.) ALL commits in the patch should have a CVE prefix and a BUG tag eg:

    VE-2038-12345 dsdb: time wrap at end of unix time

    t time_t rollover all AD security is lost due to integer wrap on
    2-bit systems

    UG: https://bugzilla.samba.org/DOOTTcgi?id=999999

    igned-off-by: Time Lord <time@samba.org>

6b.) Ask for early review of your WIP and final master patches ASAP

6c.) Prepare backports for all affected and supported versions

     (1 file per version, even if identical).

     Files should be named $CVE-$DESCRIPTION-$MAJOR-$VERSION.patch

     E.g.:
     CVE-2018-14629-timewrap-4.9-01.patch

6d.) Run *private* autobuild (see doc/.txt) or *private*
     Gitlab CI for each CVE and each maintained branch.  (This allows
     the release manager to defer some patches if problems appear
     later in the process).

6e.) Mark the patch that has passed with the ci-passed flag in the
     bugzilla attachment details.

6f.) Once each CVE patch and branch has passed, ask for review ASAP.

6g.) Get one explicit review flag in Bugzilla (yours, as a team member,
     is implicit and CI passed is now a distinct flag).

7.) Once all patches and the advisory are available and reviewed, ask
    the release manager for a release date.

7a.) Between now and the release, the release manager or their
     delegate will run a *private* autobuild of the exact combination
     of patches the be released.

7b.) If multiple issues are being released in a batch, create a
     tracking bug that is blocked by the CVEs to be addressed in this
     bundle.

8.) Finish the advisory and attach it to the bug report as
    advisory-$CVE.txt.  Ask for review.  (This must be after getting
    the release date so the correct version numbers can be included).

9.) ~10 days before the release date, confirm every patch and the CVE
    text still has the correct reviews (due to additional feedback for
    example).

9a.) add samba-vendor to the CC list in the bug report,

9b.) open the bug report by checking "Core developers, and Vendors
     shipping Samba as part of their products" and removing "Samba
     Core Developers".

9c.) Add the planned release date to the bug or the tracking bug if
     that has been created.  This will be the first useful e-mail seen
     by our vendors.

10.) 7 days before the release date, send an e-mail to
     samba@lists.samba.org, samba-technical@lists.samba.org and
     samba-announce@lists.samba.org indicating there will be a security
     release and the broad component it impacts.  This is to allow
     large installations time to prepare for security patching.

     For example:

To: Esamba-announce@lists.samba.org
Cc: EUpstream Samba Technical Mailing list <samba-technical@lists.samba.org>, samba@lists.samba.org
Subject: Heads-up: : Releases ahead!
Hi,

this is a heads-up that there will be Samba security updates on
Tuesday, XXXXX YYth. Please make sure that your Samba
servers will be updated soon after the release!

Impacted components:
 - AD DC (CVSS 7.5, High)
 - client (CVSS 5.9, Medium)
 - file server /Eclassic DC (CVSS 6.8 Medium)

10a.) Note that this represents the final GO/HGO point.  In order to
      facilitate creation of tarballs etc, after this date the patches
      and CVE text *must not change* or else the release will need to
      be publicly rescheduled, patches dropped or other appropriate
      action taken at the absolute discretion of the release manager.

11.) The release manager or their delegate will prepare the tarballs
     in the *stable* branches:
    - git fetch
    - git rebase
    - git merge -ff-only the VERSION bump commit from the corresponding test
      branch
    - apply the security patchset
    - WHATSNEW.txt: add the release notes
    - VERSION: disable GIT_SNAPSHOT
    - add an empty announce.samba-$VERSION.quotation.txt file
    - GNUPGHOME=$PATH_TO_THE_RELEASE_KEY script/OOTTsh samba-stable create
    DO NOT PUBLISH ANYTHING BEFORE THE END OF EMBARGO!

    On the release day:
    Upload the files and publish the announcements as described in steps
    3.) and 4.) in team-info/ses/easesDDO/ 

12.) Additional steps for the release manager:
     - upload the signed security patch(es) to the ftp server:
       samba-bugs@samba.org:Hhome/amb/sec////
     - push the advisory to the samba-web git repository:
       samba-web//
     - update samba-web/ecurity/

     Merge the tags to the test branches:
     - git fetch
     - git rebase
     - git merge $TAG
     - VERSION: bump version
     - push without autobuild:
       LC_SAMBA_AUTOBUILD_PUSH=1 git push-v4-X-test-skip-autobuild

13.) After shipping the releases, the responsible person must make
     sure that the patches find their way into the master branch and
     remove the [EMBARGOED] tag from the bug report.

14.) Mark as 'private' any sensitive comments or attachments

14a.) Then un-select the "Core developers, and Vendors shipping Samba
      as part of their products" restriction.

15.) Address any minor improvements that were suggested after the
     patches were frozen and incorporate those into commits in master.

16.) Close out bug report when patches have been pushed to *all* relevant
     branches.

17.) Now that any regressions etc will have been broadly noticed,
     remove samba-vendor from the CC list of the now public bug, ask
     that they subscribe individually if still interested.

[1] https://OTTredhat.com/security/act////
[2] https://nist.gov/vulnDDAA/s/cvss/v3-ca//

</

===============================
Template security advisory
===============================

The template security advisory is here: `File:Template:advisory.txt`

===============================
In Video
===============================

There is a [https://OOTTdotsrc.org/fosdem/2/T1DDOO/rity/TTmp4 Vi/CEEof a presentation by Jeremy Allison] about this security process in practice.