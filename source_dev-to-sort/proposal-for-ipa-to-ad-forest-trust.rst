Samba4/Proposal for IPA to AD forest trust
    <namespace>0</namespace>
<last_edited>2009-02-27T12:26:05Z</last_edited>
<last_editor>Abartlet</last_editor>

==============================

orest trust Design
===============================

An implementation of the requirements for an `Samba4/Proposal_for_IPA_to_AD_trust|IPA to AD trust`

The most complete and sensible trust mechanism for this task is a 'Forest Trust'.  This modal provides for mismatching schema, and mismatching functional levels, both of which are highly desirable (to avoid forced upgrades with the AD infrastructure).

`image:ASSHHtrust.png|left`

`image:HHsplit.png|right`

Components
------------------------

The design consists of these parts:

===============================
KDC
===============================

------------------------

A single KDC, representing the IPA realm.  The KDC technology needs to support the all the different AD extensions (referrals to the other side of the forest trust in particular), and of course be in sync with all the other password handling systems.

Because Microsoft's extensions to Kerberos are not particularly invasive (they are pervasive, but perhaps surprisingly do not disrupt traditional Kerberos behaviour very much), no duplication of KDC services is required.   Furthermore, creating two separate realms would simply create the complexity of both the Forest trust and the MIT trust proposals, as both a forest trust (to AD) and an MIT trust (to IPA) would be required, and IPA users would have two different principals, just as the MIT trust proposal would create.

===============================
LDAP (port 389) server
===============================

------------------------

Samba4's LDAP server providing service on port 389

===============================
LDAP Backend for IPA clients
===============================

------------------------

Some means of showing the existing schema to IPA clients, but not in conflict with the above.

Most likely this would be the existing Fedora DS server providing FreeIPA clients the existing service as currently provided, but on a different port (perhaps ldaps, or just another?)

Given the design with DNS and Kerberos, it is not possible simply to use a different IP address, unless that IP is not announced in the _ldap._tcp SRV record.

===============================
DNS
===============================

------------------------

A single DNS zone, matching the IPA realm, with the extra AD details embedded as per the Samba4 provision.

Reasoning=
===============================

------------------------

Because this design calls for a single realm, and a single set of replicated KDCs, it makes best sense to match this with a single DNS zone.  This also ensures that hosts trivially map to a realm, and that the SRV records for locating the KDC function easily and correctly.

Consequences=
===============================

------------------------

Note that because this is a single DNS zone, the _ldap._tcp SRV records must point at the AD server and the AD LDAP emulator (Samba4) on port 389, not FreeIPA.  All servers (there can be multiple) pointed at by this record must run Samba4 on that port.

This is because non-Microsoft AD clients (at least) expect to use the _ldap SRV record, despite the separate tree under _msdcs.  (Unsure of the MS client behaviour, but given the trouble the other clients will cause, it does not really matter).

===============================
LSA Server
===============================

------------------------

Most of the LSA service must be available to trusted domain clients.  Fortunately, if implemented fully for the standalone domain case, this should also cover most of the trusted domain cases too.  Additional calls would clearly include the create/query trusted domain calls. 

This is the most vital pipe for interdomain trust relations. 

===============================
NETLOGON Server
===============================

------------------------

Not only required for NTLM authentication (but clearly the most vital function it performs), the NETLOGON server also provides DC location and many other services.  We should try to implement the full pipe here, save the NT4 SamSync services (old BDC replication). 

===============================
Other Samba4 DC services
===============================

------------------------

CIFS, CLDAP, and other RPC services to be provided by Samba4, as normal for a standalone DC

===============================
Virtual Directory
===============================

------------------------

A mapping between the ultimate LDAP backend and the AD schema that Samba4 needs.  If some composite schema is used, perhaps the IPA clients 

===============================
IPA servers
===============================

------------------------

SSSD or some other service to present IPA and AD users in a consistent namespace, client side.   These need to contact the AD domains directly (using Samba3's winbind most probably), or via an RPC proxy on the servers.  

The IPA clients and servers also need a modified GSSAPI able to both read the PAC, but also to stash it for use (as Samba3 does with winbind, but only for CIFS so far) as the authenticated provider of groups.

Alternately, perhaps the same RPC proxy could do a s2u4self call to do a separate lookup against AD.

By whatever method, the clients will be responsible to doing lookups that eventually target the AD and IPA servers simultaneously, to gain an accurate real-time view of the whole enterprise.

(By way of example, this is just as a Samba3 client in pure AD would have to contact all trusted domains and forests. )

===============================
IPA Clients
===============================

------------------------

The IPA clients will need to be modified to us a Kerberos library that understands the AD referrals, so that access to servers in the AD realm, and access by AD users operates correctly across the trust.

The SSSD or similar will need to handle caching of the PAC for the login session, to get correct groups etc.

Advantages
------------------------

This is a clean, complete solution.  The user data is not synchronised, nor are shadow user entries created, but simply accessed in real time from it's native source.  The PAC is used by both AD and IPA clients, and avoids extra lookup traffic.

===============================
Example Operations
===============================

Desktop Logon with Windows client (in AD) using IPA user
------------------------

The desktop logon is perhaps one of the more complex things that the forest trust must handle, because of the number of protocols that it must cross.

These protocols include:
*Kerberos
*CLDAP
*SAMR
*DRSUAPI
*LSARPC

While complex, the 'IPA user logs on to windows client' senerio is very important, partly because if this works, then pretty much everything else must already be in pretty good shape.

On the flip side, it invokes very few (no?) calls that are not already very well known and understood. 

The process (for a WinXP client in this case) is roughly like this:

#Client locates it's own DC in Active Directory with CLDAP
#Client connects to NETLOGON (via the endpoint mapper) to call NetrLogonGetDomainInfo
#Client locates the target domain (ie, IPA) with DNS
#:up is for kerberos._tcp.Default-First-Site-Name._sites.dc._msdcs.{IPA Realm}: type SR:N
#Client connects to IPA KDC to get it's own ticket
#Client then obtains the cross-realm and service tickets so as to get a ticket to the local workstation name (ie, itself)
#Client locates the target domain (ie, IPA) with DNS (again...)
#:up is for ldap._tcp.Default-First-Site-Name._sites.dc._msdcs.{IPA Realm}: type SR:N
#A directed CLDAP ping is used to confirm connectivity to the host
#Client connects to DRSUAPI (via the endpoint mapper) to call DsCrackNames
#Client downloads Group Policy over Kerberos-secured LDAP and SMB from AD

Desktop Login with AD user on IPA Workstathion
------------------------

`image:ASSHHtrust-ipa-login.png|left`
#SSSD on IPA client determins that this user is in AD by query to it's KDC, directory.
#SSSD on IPA client performas a NETLOGON NetrLogonGetDomainInfo call to IPA's NETLOGON server, to determine the AD location details for this user
#The Kerberos libs do a lookup for the correct KDC to contact
#The Kerberos libs make a kinit for the AD user
#A ticket for the AD user to access the local workstation is obtained, by cross-realm to the IPA realm
#The user's groups are obtained from the PAC, and interpreted by SSSD
#The user's home directory is mounted using the user's Kerberos ticket

File share connection from Windows client, Windows user to IPA Server
------------------------

Samba is running on an IPA-enabled Server, offering access to files.  

A Windows user (with a valid Kerberos ticket, including PAC, from their local AD) wants to access the file server from their windows desktop

# Client connects to the file server
# Client and server negotiate kerberos authentication
# Client asks local KDC for ticket to file server
# KDC returns a referall to the IPA realm's KDC
#:obtained by looking all over the global catalog, then determining that the host probably belongs to IPA, and returning a referall to match
# Client obtains a cross-realm ticket to the IPA realm
# Client obtains a ticket to the file server
# Server uses the PAC to determine the user's groups
# Server uses idmap stored in the IPA directory or in AD to map to unix groups

===============================
Notes
===============================

------------------------

Proxied lookupSids operations (for example, to display the contents of an ACL) may require Samba to consult a combined SSSD/Winbind, which might then reach back to AD to resolve SIDs to names.

A varient on idmap_ad might be used to store the IDMAP, perhaps split between AD and IPA for relevant users.

Remote desktop connection from IPA client, IPA User to windows server
------------------------

#Client has a valid ticket to the IPA realm (including an AD PAC)
#Client make connection to RDP server using rdesktop
#Client and server negotiate Kerberos
#Client attempts to get ticket from IPA KDC for user to access server
#IPA KDC refers user to the AD realm
#Client gets a cross-realm ticket to the AD realm
#Client gets a ticket for the target server from the 'right' AD KDC
#Client authenticates with Kerberos to the RDP server

===============================
Notes
===============================

------------------------

All Kerberos tickets issued by the IPA KDC include the PAC, so this is simply passed along pretty much as if this were a pure AD setup.

Any calls (LDAP, LSA) the RDP server needs to make to the home AD server arrive on the emulated AD interface of the IPA server.

===============================
Missing components
===============================

------------------------

At this stage, rdesktop does not support Kerberos authentication.  However, the specifications for this protocol are online, so this should not be too hard.