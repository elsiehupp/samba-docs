GSOC cockpit samba ad dc
    <namespace>0</namespace>
<last_edited>2021-05-15T17:13:54Z</last_edited>
<last_editor>Fraz</last_editor>

=================
Cockpit Samba AD DC Plugin Documentation
=================
=================

What is Cockpit?
=================
According to Cockpit’s documentation, it’s an "*easy-to-use, integrated, glanceable, and open web-based interface for your servers*" (https://cockpit-project.org/). 

Cockpit helps you manage your Linux servers using a user interface that runs in a browser. It also lets developers develop their own plugins using an API cockpit provides for interacting with the server. This project sought to incorporate tasks done using the samba-tool command line utility for Samba AD DC in an intuitive UI for easier usage and administration.

=================
Installation
=================
Fedora 32 Installation 
------------------------

Prerequisite: Cockpit
------------------------

# Download the repository https://download.opensuse.org/repositories/home:/Hezekiah/Fedora_32/home:Hezekiah.repo
# Place it to /etc/yum.repos.d/
# Run the following command
#:<pre># dnf install cockpit-samba-ad-dc</pre>
# If you already have Cockpit on your server, point your web browser to: https://ip-address-of-machine:9090
# Use your system user account and password to log in.

Ubuntu 20.04 Installation 
------------------------

===============================
Prerequisite: Cockpit
===============================

------------------------

# Update the apt package index and install packages to allow apt to use a repository over HTTPS:
#:<pre> $ sudo apt update
 $ sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common</pre>
# Add Cockpit-Samba-AD-DC GPG key:
#:<pre> $ curl -fsSL https://download.opensuse.org/repositories/home:/Hezekiah/xUbuntu_20.04/Release.key | sudo apt-key add - </pre>
# Add the repository to the sources.list file
#:<pre>$ sudo add-apt-repository "deb https://download.opensuse.org/repositories/home:/Hezekiah/xUbuntu_20.04 ./"</pre>
# Update the apt package index, and download the latest version of Cockpit-Samba-AD-DC plugin.
#:<pre>$ sudo apt-get update
$ sudo apt-get install cockpit-samba-ad-dc</pre>
#If you already have Cockpit on your server, point your web browser to: https://ip-address-of-machine:9090
#Use your system user account and password to log in.

Debian 10 Installation 
------------------------

Prerequisite: Cockpit
===============================

------------------------

# Update the apt package index and install packages to allow apt to use a repository over HTTPS:
#:<pre>$ sudo apt update
$ sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common</pre>
# Add Cockpit-Samba-AD-DC GPG key:
#:<pre>$ curl -fsSL https://download.opensuse.org/repositories/home:/Hezekiah/Debian_10/Release.key | sudo apt-key add -</pre>
# Add the repository to the sources.list file
#:<pre>$ sudo add-apt-repository "deb https://download.opensuse.org/repositories/home:/Hezekiah/Debian_10 ./"</pre>
# Update the apt package index, and download the latest version of Cockpit-Samba-AD-DC plugin.
#:<pre>$ sudo apt-get update
$sudo apt-get install cockpit-samba-ad-dc</pre>
# If you already have Cockpit on your server, point your web browser to: https://ip-address-of-machine:9090
# Use your system user account and password to log in

=================
Using the Plugin
=================
Provisioning an AD DC Domain 
------------------------

The plugin first checks the Samba configuration file (smb.conf) to make sure that the server is set as an Active Directory Domain Controller (AD DC). If the server is not, the user is prompted to setup one.  

`File:Cockpit-plugin_provision_AD_DC.png|960px` 

Once provisioned, a page with a list of actions you can perform with the plugin appears.

`File:Cockpit-samba-ad-dc_main_page.png|960px`

Computer Management 
------------------------

Using the plugin you can:

* Create a computer
* Delete a computer
* Display a Computer Active Directory object
* List all computers
* Move a computer to an organizational unit/container.
 `File:Cockpit-plugin_computer_magement.png|900px`

Contact Management 
------------------------

* Creating a Contact
* Deleting a Contact
* Listing all Contacts
* Showing a Contact

`File:Cockpit-plugin_contact_management.png|920px`

Delegation Management 
------------------------

Use the plugin to:

* Add a service principal as msDS-AllowedToDelegateTo
* Delete a service principal as msDS-AllowedToDelegateTo
* Show the delegation setting of an account
* Set/unset UF_TRUSTED_FOR_DELEGATION for an account
* Set/unset UF_TRUSTED_TO_AUTHENTICATE_FOR_DELEGATION (S4U2Proxy) for an account

`File:Cockpit-plugin_delegation_management.png|950px`

DNS Management 
------------------------

*Creating a DNS record
* Deleting a DNS record
* Cleanup DNS records
* Query for server information
* Creating zones
* Deleting zones
* Showing zone information

`File:Cockpit-plugin_DNS_management.png|960px`

Domain Management 
------------------------

Using the plugin, you can:

* Promote an existing domain member or NT4 PDC to an AD DC
* Get basic info about a domain
* Demote a Domain Controller
* Join domain as either member or backup domain controller
* Upgrade from Samba classic (NT4-like) database to Samba AD DC database
* Create a domain or forest trust
* Delete a domain trust
* List domain trusts
* Show trusted domain details
* Validate a domain trust
* Manage forest trust namespaces
* Copy a running DC's current DB into a backup tar file
* Backup the local domain directories safely into a tar file
* Copy a running DC's DB to backup file, renaming the domain in the process
* Restore the domain's DB from a backup-file

`File:Cockpit-plugin_domain_management.png|960px`

Forest Management 
------------------------

* Show
* DSHeuristics

FSMO Management 
------------------------

* Seize Roles
* Show Roles
* Transfer Roles

Group Policy Object (GPO) Management 
------------------------

* Create an empty GPO
* Deleting a GPO
* Backing up a GPO
* Delete GPO link from a container
* Downloading a GPO
* Get inheritance flag for a container
* List GPO Links for a container
* List GPOs for an account
* List all GPOs
* List all linked containers for a GPO
* Restore a GPO to a new container
* Set inheritance flag on a container
* Add or update a GPO link to a container
* Show information for a GPO

Group Management 
------------------------

* Create a new AD group
* Delete an AD group
* List all groups
* List all members of an AD group
* Move a group to an organizational unit/container
* Remove members from an AD group
* Display a group AD object

`File:Cockpit-plugin_Group_management.png|960px`

Organization Unit (OU) Management 
------------------------

* Create an organizational unit
* Delete an organizational unit
* List all organizational units
* List all objects in an organizational unit
* Move an organizational unit
* Rename an organizational unit

`File:Cockpit-plugin_OU_management.png|960px`

Sites Management 
------------------------

Use the plugin to:

* Create a new site
* Delete an existing site
* Create a new subnet
* Delete an existing subnet
* Assign a subnet to a site

`File:Cockpit-plugin_sites_management.png|960px`

SPN Management 
------------------------

With the plugin, you can:

* Create a new spn
* Delete a spn
* List spns of a given user

`File:Cockpit-plugin_SPN_Management.png|960px`

User Management 
------------------------

Using the plugin you can:

* Create a new user
* Delete a user
* Disable a user
* Enable a user
* List all users
* Move a user to an organizational unit/container
* Change password for a user account
* Set or reset the password of a user account
* Set the expiration of a user account
* Display a user AD object

`File:Cockpit-plugin_user_management.png|960px`

Server Time 
------------------------

Retrieve the time on a server
`File:Cockpit-plugin_time.png|960px`

DS ACLs manipulation 
------------------------

Using the plugin, you can:

* Get access list on a directory object
* Modify access list on a directory object

`File:Cockpit-plugin_DS_ACLs_manipulation.png|960px`

NT ACLs manipulation 
------------------------

Use the plugin to:

* Change the domain SID for ACLs
* Get ACLs of a file
* Get DOS info of a file from xattr
* Set ACLs on a file
* Check sysvol ACLs match defaults
* Reset sysvol ACLs to defaults

`File:Cockpit-plugin_NT_ACLs_manipulation.png|960px`

=================
Project Design
=================

The plugin uses:

* React - A Javascript library for building user interfaces (https://reactjs.org/)
* Cockpit - https://cockpit-project.org/
* PatternFly - https://www.patternfly.org/v4/
* Open Build Service - https://openbuildservice.org/
* Webpack - https://webpack.js.org/

The plugin is built on top of the Cockpit's Starter Kit (https://github.com/cockpit-project/starter-kit).
Starter Kit makes it easy to get started building plugins for Cockpit. This blog post (https://cockpit-project.org/blog/cockpit-starter-kit.html) gives more explanation on why developers might need the Starter Kit when building plugins for Cockpit.

=================
Building the Source Code
=================
Make sure you have npm available (usually from your distribution package). These commands check out the source and build it into the dist/ directory:

 git clone https://gitlab.com/HezekiahM/samba-ad-dc.git
 cd samba-ad-dc
 make

*make install* compiles and installs the package in */usr/share/cockpit/*.

For development, you usually want to run your module straight out of the git tree. To do that, link that to the location were cockpit-bridge looks for packages:

 mkdir -p ~/.local/share/cockpit
 ln -s `pwd`/dist ~/.local/share/cockpit/starter-kit
After changing the code and running make again, reload the Cockpit page in your browser.

You can also use watch mode to automatically update the webpack on every code change with

 npm run watch
or

 make watch

To contibute to the project, clone the repository, make your changes in a feature branch and make a merge request to the master branch.

=================
Automated Releases
=================

The plugin utilises Open Build Service for automated releases currently for these distributions:
* Fedora 32
* Ubuntu 20.04
* Debian 10

The OBS package can be found here: https://build.opensuse.org/package/show/home:Hezekiah/samba-ad-dc. A webhook in gitlab pushes any code changes in the master branch to OBS repo where it gets built and released for the various distributions.

----
`Category:Active Directory`