Documenting new features
    <namespace>0</namespace>
<last_edited>2018-09-13T05:22:07Z</last_edited>
<last_editor>Timbeale</last_editor>

The best place to document Samba features is in the code itself, i.e. by writing great man-pages and samba-tool help. Writing a good WHATSNEW.txt description is also a great way to let existing Samba users know about a new feature. However, documenting new and updated features on the Samba wiki is also useful, and can be a helpful way for a new Samba user to work out what Samba can and cannot do. 

Documenting user features 
------------------------

* There's a `Template: |template` to help you get started. Just create a new page and save it with the following content::

.. code-block::

    {{subst: }}
* he magic of mediawiki templating, this will create a skeleton page.
* Replace the *<blah>* markers in each section with your own content.
* Add <nowiki>`Category:;/nowiki> tags as appropriate, e.g. `:Category:rectory]:gory:Printing`, etcDD: list of:categories, see `Special:Categories`.:
* Link to the new page from appropriate places on the wiki, e.g. `User Documentation`.
* [Password Settings Objects|PSOs|an example` of what your finished page might look like.

This structure won't work in all cases, but try to use it as a guide so that wiki pages have a consistent look to users.

You may want to go into a lot more detail about the new feature, or have several different pages explaining different aspects of the feature. In which case, you could use the above template to provide a brief overview of the feature, and then simply link to the other pages to drill-down in more detail.

Documenting other work 
------------------------

The above template won't make so much sense if you're working on something that's not really user-facing, e.g. Python-3, WAF, selftest. However, if you've put in a lot of work on the changes, then it's still worth documenting it for other developers. Just use a free-form wiki page, but make sure to link to your new page from relevant places, e.g. `Developer Documentation`.