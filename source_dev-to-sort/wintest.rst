WinTest
    <namespace>0</namespace>
<last_edited>2016-04-01T01:56:28Z</last_edited>
<last_editor>Jasonblewis</last_editor>

Testing against Windows with WinTest 
------------------------

WinTest is a framework for testing Samba against Windows virtual machines. It aims to provide a reliable, repeatable test system that any Samba developer can setup.

The core of WinTest is based on [http://www.noah.org/wiki/pexpect pexpect], a python expect system that provides a flexible way of controlling command line tools.

Some background information on WinTest is available in [http://blog.tridgell.net/?p=91 this blog post]

Setting up your environment for WinTest 
------------------------

To run WinTest, you need a Linux host that can control Windows VMs. Any scriptable VM system that supports snapshots should work. There are currently [https://git.samba.org/?p=samba.git;a=tree;f=wintest/conf;hb=HEAD five example config] files for WinTest.
If you build a config for a different VM system, please contribute an example config file.
* [https://git.samba.org/?p=samba.git;a=blob;f=wintest/conf/tridge.conf;hb=HEAD tridge.conf] for using VirtualBox.
* [https://git.samba.org/?p=samba.git;a=blob;f=wintest/conf/abartlet.conf;hb=HEAD abartlet.conf] for using virsh to control KVM.
* [https://git.samba.org/?p=samba.git;a=blob;f=wintest/conf/abartlet-jesse.conf;hb=HEAD abartlet-jesse.conf] for using virsh to control KVM.
* [https://git.samba.org/?p=samba.git;a=blob;f=wintest/conf/zahari-esxi.conf;hb=HEAD zahari-esxi.conf] using VMware ESXi via ssh.
* [https://git.samba.org/?p=samba.git;a=blob;f=wintest/conf/bbaumbach.conf;hb=HEAD bbaumbach.conf] using VirtualBox in headless mode, since some systems have problems running VMs with "su" in default GUI mode.

The current [https://git.samba.org/?p=samba.git;a=blob;f=wintest/test-s4-howto.py;hb=HEAD test script] can use and test the following VMs:
* Windows7
* WindowsXP
* Windows2003
* Windows2008
* Windows2008R2

You don't need to setup all of these VMs to run the test. The script will look for what VMs you have defined in your config file and will only run tests against those VMs.

Setting up a VM
------------------------

Each of the VMs needs to be setup with the following properties:

* The Telnet server needs to be enabled.
* netdom.exe needs to be installed for VMs that will join Samba as a workstation.
* dcdiag.exe needs to be installed for VMs that are DCs.
* The machine needs to have a snapshot with it fully booted.

It is also a good idea to do the following:
* Ensure that in the snapshot the machine is activated, and windows update is disabled.
* Disable the firewall, although the script will try to do that if Telnet is working.

Make sure that you test that you can Telnet into the machine. You may need to add "Authenticated Users" to the [http://support.microsoft.com/kb/250908 TelnetClients group].

Running The Tests 
------------------------

You need to run the tests as root. After you have created a config file (based on one of the examples), you should run it like this:

    udo wintest/test-s4-howto.py --conf wintest/conf/tridge.conf --rebase

This will run the full set of tests, using the parameters from your config file. See the config file for details on where it gets the source tree from and where it builds it. On my laptop the full test suite currently takes about 30 minutes.

Your may find this [http://samba.org/tridge/wintest-output.txt sample output] useful to see what happens when you run WinTest.

DNS Setup 
------------------------

Without setting a special DNS backend, the test will setup the Samba internal DNS. Bind9 with the DLZ backend can be used instead, by running Wintest with --dns-backend=BIND9_DLZ.

The tests aim to run Samba in a manner as close to real production use as possible. To make this possible, it will modify your /etc/resolv.conf file to point at the DNS server starts. The DNS server config is setup to automatically forward DNS requests for non-WinTest hosts to your original nameserver, so it should not impact on normal usage of your machine. The test restores your resolv.conf to its original value when it is complete.

Network Setup 
------------------------

The script will create an IP alias on whatever network interface you set up in your config file. Samba and the DNS server will be setup to listen on that IP alias. The Windows VMs will be automatically modified to set up their networking to point at the IP alias.

The idea behind this arrangement is that you can run this script on your primary development workstation (eg. your laptop), and you will be able to keep using the machine for normal work while the test is running.

What is Tested? 
------------------------

The main things that are tested are:
* Joining Windows XP and Windows 7 to a Samba4 domain as member servers
* Joining Windows 2003, Windows 2008, and Windows 2008R2 as DCs in a Samba domain
* Joining Windows 2008R2 as a RODC to a Samba domain
* Joining Samba as a DC in a Windows 2003, Windows 2008, and Windows 2008R2 domain
* Joining Samba as a RODC in a Windows 2008R2 domain
* DRS replication between Windows and Samba for all the above DC arrangements
* Dynamic DNS with TSIG/GSS, using bind9 or the internal DNS
* Dynamic DNS

The list of tests is likely to grow rapidly as WinTest is developed further.

Samba3 Testing 
------------------------

There is a skeleton of a [http://samba.org/ftp/unpacked/samba_4_0_test/wintest/test-s3.py Samba3 test] in WinTest, but it needs to be developed further. It would be great if someone could volunteer to expand it to be a useful test of Samba3 against Windows.

Buildfarm Integration 
------------------------

We hope in the future to integrate WinTest with [http://build.samba.org the build farm], providing web based access to regular testing of Samba against Windows. The main thing that needs to be done is to convert WinTest to use the subunit python testing framework that the build farm uses.