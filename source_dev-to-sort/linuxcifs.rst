LinuxCIFS
    <namespace>0</namespace>
<last_edited>2018-05-24T23:05:27Z</last_edited>
<last_editor>Sfrench</last_editor>

Introduction 
------------------------

The CIFS VFS is a virtual file system for Linux to allow access to modern SMB3 servers (Windows, NetApp, EMC, Samba, Macs and Azure) as well as older servers and storage appliances compliant with the SNIA CIFS Specification version 1.0 or later. Popular older servers such as Samba 3, Windows 2000, Windows XP and many others supported CIFS by default, but modern servers and network appliances now support SMB3 or later.  The CIFS VFS supports both.

CIFS VFS is designed to take advantage of advanced network file system features such as locking, Unicode (advanced internationalization), hardlinks, RDMA, DFS (hierarchical, replicated name space), distributed caching and uses native TCP names (rather than RFC1001, Netbios names).  Unlike some other network file systems all key network function including authentication is provided in kernel (and changes to mount and/or a mount helper file are not required in order to enable the CIFS VFS).

The CIFS VFS was added to mainline Linux kernels in 2.5.42. While only peripherally related to SAMBA, the cifs client userspace tools are hosted in a separate tree on git.samba.org.

The development tree for future improvements to the cifs kernel client is [http://git.samba.org/?p=sfrench/cifs-2.6.git;a=shortlog;h=refs/heads/for-next]. A list of the cifs kernel client improvements by kernel release is included on the `LinuxCIFSKernel` page.

The current support for CIFS, SMB2, SMB2.1 and SMB3 dialects are stable and tested, and use of either CIFS or SMB3 (vers=1 or vers=3.0 on mount) is recommended depending on security requirements (SMB3 is much stronger security), server type and workload.  The status of implementation of optional SMB3 protocol features in the kernel client can be found at the `SMB3_kernel_status` page.  Instructions for how to build a backported version of the current cifs/smb3 driver for older kernels can be found at the `LinuxSMB3_build_backport` page.

For developers interested in the extensions being made to the current (SMB3.11) protocol for better POSIX/Linux support, see https://wiki.samba.org/index.php/SMB3-Linux

Userspace Utilities 
------------------------

Most of the cifs code is implemented as part of the Linux kernel. There are some userspace pieces that are maintained separately as part of the **cifs-utils** project. Information on that is available on the `LinuxCIFS_utils` page.

Mailing List 
------------------------

Discussion of the Linux CIFS Client takes place primarily on the [http://vger.kernel.org/vger-lists.html#linux-cifs linux-cifs] mailing list::

If you're planning to ask for help, please read over the `LinuxCIFS_troubleshooting` page first.  To check the linux-cifs mailing list archives see https://www.spinics.net/lists/linux-cifs/

`Category:t`