Samba codebase organization
    <namespace>0</namespace>
<last_edited>2020-06-12T22:01:18Z</last_edited>
<last_editor>Abartlet</last_editor>

===============================

Introduction
===============================

{{:Samba codebase organization overview}}

The following sections break down the codebase layout in more detail. This is not intended to be a comprehensive directory, and just covers the major components.

=================
Top-Level libraries
=================

At the time of the merge, all code was located in either the ``source3`` or ``source4`` directory. Over time, as duplicate code between the two branches becomes merged or used in common, the code is moved out into the top-level of the source-code tree.

The major libraries components at the top-level are:

* [https://git.samba.org/?p=samba.git;a=tree;f=third_party Third-party] libraries Samba needs some specific libraries to build. Some of these are included in the Samba source tree to aid in building on older and non-Linux platforms.

* [https://git.samba.org/?p=samba.git;a=tree;f=lib General purpose libraries] Samba, being like any large program written in C, has a number of internal helper functions that do not implement the protocols but are required to share code and make the rest of Samba possible.

* The sub-projects of ``talloc``, ``tdb``, ``tevent`` and ``ldb`` and live here, in the ``lib`` directory.

* [https://git.samba.org/?p=samba.git;a=tree;f=librpc Common RPC client library (librpc)] is the common (between and ``source3`` and ``source4``) parts of Samba’s RPC client implementation.

* [https://git.samba.org/?p=samba.git;a=tree;f=libcli Common client library (libcli)] is the common (between and ``source3`` and ``source4``) parts of Samba’s client implementation for our protocols.

* [https://git.samba.org/?p=samba.git;a=tree;f=auth Common authentication library (auth)] is the common (between and ``source3`` and ``source4``) parts of Samba’s authentication implementation.

* `PIDL|PIDL` is Samba’s code auto-generation system for generating C code and C-Python bindings from IDL.

* [https://git.samba.org/?p=samba.git;a=tree;f=python python] contains Samba’s Python library. It is not generally used in the file server, but is critical for the AD DC.

* [https://git.samba.org/?p=samba.git;a=tree;f=ctdb CTDB] Samba’s clustered database (which enables the clustered file server).

=================
Source3
=================

The [https://git.samba.org/?p=samba.git;a=tree;f=source3 source3] directory is home to code primarily used by the file server and domain member. ``source3`` contains the following major components:

* [https://git.samba.org/?p=samba.git;a=tree;f=source3/smbd The SMB file server (smbd)] is the file server that most people think of when they think of Samba.
* [https://git.samba.org/?p=samba.git;a=tree;f=source3/nmbd The NBT name server (nmbd)] provides NetBIOS over TCP/IP (NBT) for those who want it.
* [https://git.samba.org/?p=samba.git;a=tree;f=source3/winbindd Winbindd] provides the connection between Samba and the AD Domain to which it is joined, for authentication and name lookup. It also manages the IDMAP, being the mapping between unix UID/GID values and Windows SID values. ``winbindd`` is used in both Domain member and AD Domain Controller modes.
* [https://git.samba.org/?p=samba.git;a=tree;f=source3/librpc RPC client library (librpc)] contains the parts of Samba’s RPC client implementation that are specific to the source3 subtree.
* [https://git.samba.org/?p=samba.git;a=tree;f=source3/libsmb SMB client library (libsmb)] contains the parts of Samba’s SMB client implementation used in the source3 subtree.
* [https://git.samba.org/?p=samba.git;a=tree;f=source3/auth Authentication server (auth)] contains the parts of Samba’s NTLM authentication server used in the source3 subtree. A shim module connects this to the ``source4`` authentication code when Samba is an AD DC.
* [https://git.samba.org/?p=samba.git;a=tree;f=source3/passdb Password database (passdb)] contains the NTLM password database used in the ``source3`` subtree. A shim module connects this to the ``sam.ldb`` data store when Samba is an AD DC.
* [https://git.samba.org/?p=samba.git;a=tree;f=source3/rpc_server RPC server] contains the ``source3`` RPC server. However, most parts of this are not used in the AD DC, but instead are redirected to the equivalent parts of ``source4/rpc_server``. When used, this provides the classic or NT4-like DC either as a DC or to service the SAM on each member or standalone server (each Windows machine has a database under its own name, which Samba does too).
* [https://git.samba.org/?p=samba.git;a=tree;f=source3/printing Print server] functionality is located in the ``printing`` directory. Also relevant is the [https://git.samba.org/?p=samba.git;a=tree;f=source3/rpc_server/spoolss source3/rpc_server/spoolss] code.

=================
Source4
=================

The [https://git.samba.org/?p=samba.git;a=tree;f=source4 source4] directory is home to code primarily used by the Active Directory Domain Controller. ``source4`` contains the following major components:

* [https://git.samba.org/?p=samba.git;a=tree;f=source4/setup Active Directory Database templates] located in ``setup``. These templates fill out the basic structure of an Active Directory DC in the ``sam.ldb``. This includes the full schema definition.
* [https://git.samba.org/?p=samba.git;a=tree;f=source4/heimdal Heimdal] is an (old) branch/fork of Heimdal with some changes. An attempt is made to sync this Samba fork with a tree called lorikeet-heimdal (which is a true branch/fork of Heimdal). Patches applied here should first be incorporated upstream, however this has not always happened.
* [https://git.samba.org/?p=samba.git;a=tree;f=source4/lib General purpose libraries (lib)] that have not yet been migrated to the top level.
* [https://git.samba.org/?p=samba.git;a=tree;f=source4/libcli Client library (libcli)] contains the parts of Samba’s client implementation for our protocols specific to the ``source4`` codebase.
* [https://git.samba.org/?p=samba.git;a=tree;f=source4/librpc RPC client library (librpc)] contains the parts of Samba’s RPC client implementation specific to the codebase.
* [https://git.samba.org/?p=samba.git;a=tree;f=source4/libgpo libgpo] contains Group Policy Object support.
* [https://git.samba.org/?p=samba.git;a=tree;f=source4/torture smbtorture] binary, used for testing Samba and Windows. For historical reasons there are two ``smbtorture`` frameworks, the ``source4`` framework is the one being extended at this time, but some tests will remain in [https://git.samba.org/?p=samba.git;a=tree;f=source4/torture source3/torture].
* Old NTVFS file server and VFS layer. The attempt at a new file server architecture is preserved in the following directories. These demonstrated a new VFS layer that is organised around the SMB and NTFS semantics rather than the POSIX semantics that Samba used in ``smbd`` at the time (``smbd`` now uses a hybrid approach).
* * [https://git.samba.org/?p=samba.git;a=tree;f=source4/ntvfs source4/ntvfs]
* * [https://git.samba.org/?p=samba.git;a=tree;f=source4/smb_server source4/smb_server]
* AD Services. The core AD DC is implemented in the named folders for each component:
* * [https://git.samba.org/?p=samba.git;a=tree;f=source4/ldap_server source4/ldap_server]
* * [https://git.samba.org/?p=samba.git;a=tree;f=source4/cldap_server source4/cldap_server] (Connectionless LDAP)
* * [https://git.samba.org/?p=samba.git;a=tree;f=source4/rpc_server source4/rpc_server]
* * [https://git.samba.org/?p=samba.git;a=tree;f=source4/dns_server source4/dns_server]
* * [https://git.samba.org/?p=samba.git;a=tree;f=source4/nbt_server source4/nbt_server] (NetBIOS over TCP/IP)
* * [https://git.samba.org/?p=samba.git;a=tree;f=source4/kdc source4/kdc]
* [https://git.samba.org/?p=samba.git;a=tree;f=source4/auth Authentication server (auth)] contains parts of Samba’s authentication server used in the AD DC. A shim module connects ``smbd`` to this authentication code when Samba is an AD DC.
* The Directory Services DB (DSDB), which provides the main implementation behind the sam.ldb database (covered in more detail below).

Directory Services DB (DSDB) 
------------------------

The code that implements the main AD database is located in [https://git.samba.org/?p=samba.git;a=tree;f=source4/dsdb source4/dsdb]. The ``dsdb`` directory contains the following notable components:

* [https://git.samba.org/?p=samba.git;a=tree;f=source4/dsdbsamdb/ldb_modules LDB modules] The LDB library provides a generic framework where custom plug-in modules can be added to modify the database’s behaviour. DSDB uses the LDB library framework and defines its own set of plug-in modules (located in ``dsdb/samdb/ldb_modules``) that are specific to Active Directory. The result is a database that provides the full AD semantics.
* [https://git.samba.org/?p=samba.git;a=tree;f=source4/dsdb/schema Schema handling] The ``sam.ldb`` database follows and conforms to the AD schema. The handling for loading and using the full AD schema is located in ``source4/dsdb/schema``.
* [https://git.samba.org/?p=samba.git;a=tree;f=source4/dsdbrepl Replication handling (part)] Some of the code related to handling AD’s DRS replication is located in ``source4/dsdb/repl``.
* [https://git.samba.org/?p=samba.git;a=tree;f=source4/dsdb/kcc KCC The Knowledge Consistency Checker (KCC)] is a process that ensures that a valid replication graph is maintained and other periodic cleanup work is done. Parts of the implementation are located in ``source4/dsdb/kcc``, mostly for historical reasons. Other KCC handling is also located in [https://git.samba.org/?p=samba.git;a=tree;f=python/samba/kcc python/samba/kcc].

=================
Infrastructure components
=================

The source-code tree contains the following components that are used to build and test Samba.

* [https://git.samba.org/?p=samba.git;a=tree;f=selftest Selftest] is a bespoke framework for unit and integration testing. The tests themselves are located in many different parts of the source tree.
* * [https://git.samba.org/?p=samba.git;a=tree;f=python/samba/tests python/samba/tests] contains many of the Python unit tests.
* * [https://git.samba.org/?p=samba.git;a=tree;f=selftest/selftest.pl selftest/selftest.pl] is the runner for ``make test``.
* * [https://git.samba.org/?p=samba.git;a=tree;f=selftest/target selftest/target] is Perl code that constructs Samba test environments.
* * [https://git.samba.org/?p=samba.git;a=tree;f=selftest/tests.py selftest/tests.py] declares all the Samba tests (both unit and integration) and the test environment they should run in. Note that ``make test`` is also spread across [https://git.samba.org/?p=samba.git;a=blob;f=source3/selftest/tests.py source3] and [https://git.samba.org/?p=samba.git;a=blob;f=source4/selftest/tests.py source4] as well.

* [https://git.samba.org/?p=samba.git;a=tree;f=wintest Wintest] is a system that sits outside Samba’s selftest. Wintest builds and installs Samba and runs some limited testing against Windows automatically. Note that this system is not currently maintained and in-use.

* [https://git.samba.org/?p=samba.git;a=tree;f=buildtools Build system]. the code in ``buildtools`` uses the ``wscript`` files in each directory of the source tree in order to build Samba.

* [https://git.samba.org/?p=samba.git;a=tree;f=docs-xml Documentation]. Samba’s manpages are constructed from XML and are located in the ``docs-xml`` directory. In particular the [https://www.samba.org/samba/docs/current/man-html/smb.conf.5.html smb.conf] manpage is constructed from a whole sub-directory of files in here.

* Note that the internal list of valid parameters in Samba is created from the XML documentation of each configuration parameter, ensuring the code and documentation is always consistent. Documented defaults are also checked for consistency in the automated test-suites.

=================
Autogenerated code
=================

Note that significant amounts of Samba’s codebase is autogenerated from IDL (Interface Definition Language) files. This code is spread across source-code tree (i.e. ``source3``, ``source4``, and top-level libraries).

PIDL generates pull (serialize, or pack) and push (deserialize, or unpack) functions for all the structures described using IDL, and structures marked ``[public]`` are exposed in public functions in C and Python. This is very helpful for parsing not just DCE/RPC packets but any other regularly structured buffer. The IDL files are located:

* [https://git.samba.org/?p=samba.git;a=tree;f=librpc/idl librpc/idl]
* [https://git.samba.org/?p=samba.git;a=tree;f=source3/librpc/idl source3/librpc/idl]
* [https://git.samba.org/?p=samba.git;a=tree;f=source4/librpc/idl source4/librpc/idl]

For complex structures that don’t quite fit into IDL, a marker ``[nopull]``, ``[nopush]``, or ``[noprint]`` can be specified. Hand-written parsers can then be written to handle these structures. These manual parsers are located in:

* [https://git.samba.org/?p=samba.git;a=tree;f=librpc/ndr librpc/ndr]
* [https://git.samba.org/?p=samba.git;a=tree;f=source4/librpc/ndr source3/librpc/ndr]
* [https://git.samba.org/?p=samba.git;a=tree;f=source4/librpc/ndr source4/librpc/ndr]

See the `PIDL|PIDL page` for specifics on PIDL syntax and examples.