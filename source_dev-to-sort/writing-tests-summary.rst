Writing Tests summary
    <namespace>0</namespace>
<last_edited>2020-06-18T20:50:32Z</last_edited>
<last_editor>Abartlet</last_editor>


Most changes to Samba should have a test to demonstrate the bug being fixed, or test the feature being added.  Most tests are run using 'make test' from a Samba source tree.  

See `Writing_Tests|writing and running Samba tests` but in particular:
* `Writing Torture Tests`:re in the *source4/torture* directory and provides direct C protocol tests.
* `Writing Python Tests`:rotocol under test is `DCERPC`, then `PIDL` will have already auto-generated `Python` bindings.  Likewise LDAP is easily accessed via `LDB`.
* `Writing cmocka Tests`: unit tests of C functions.
* `LDB`:r LDB are in ``lib/ldb/tests`` and are run from ``make test`` within ``lib/ldb``
* `Running_CTDB_tests|CTDB`:r CTDB are written as shell scripts under ``ctdb/tests`` and are run from ``make test`` within ``ctdb``