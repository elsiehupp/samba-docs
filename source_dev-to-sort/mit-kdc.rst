Samba4/MIT KDC
    <namespace>0</namespace>
<last_edited>2009-10-12T21:28:17Z</last_edited>
<last_editor>Bradh</last_editor>

A proposal to allow Red Hat to integrate Samba4 and their choice of MIT KDC for the FreeIPA project

[http://freeipa.org/page/Main_Page FreeIPA] is an integration of [http://web.mit.edu/Kerberos/ MIT Kerberos] and the [http://directory.fedoraproject.org/ Fedora DS/389] projects into a useful identity management solution for unix-like systems.  It wishes to interoperate with Windows domains and clients, and so plans to [http://www.freeipa.org/page/IPA_and_AD use Samba4] in [http://www.freeipa.org/page/IPAv3_development_status IPAv3].  

One of the biggest challenges the FreeIPA project faces is their requirement that the existing integrated Heimdal KDC and Kerberos library used by Samba4 be replaced with MIT Kerberos.  At the time of writing, the [http://k5wiki.kerberos.org/wiki/Projects/Samba4_Port proposal for Samba4 to use MIT kerberos] simply called for every part of Heimdal to be replaced with MIT Kerberos.  However, progress (beyond scoping documentation) on this task has been very slow up to this point. 

At the [http://people.samba.org/people/2009/10/05#cifs-2009-conference Samba4 LDAP and Kerberos backends BOF], the Samba Team proposed an alternative solution, with less impact on the Samba Team, while still allowing Red Hat to use MIT Kerberos for the KDC. 

The proposal is that Samba4 provide an IRPC interface to Heimdal's HDB and WinDC layer.  The advantage of this is that the exact same database operations are run regardless of the KDC implementation in use. 

This fits in with ideas Love (Heimdal maintainer) has suggested where he wants to separate the database and cryptographic operations from the KDC processing. 

These functions will be provided:
 - fetch entry (send ASN.1 encoded HDB entry)
 - fetch key or encrypt/decrypt with key (principal/kvno/enctype)
 - provide PAC
 - verify PAC
 - authorize access
 - auth status (record success or failure)
 - check constrained delegation
 - check pkinit ms upn match

(i.e. pretty much all the interfaces that hdb-samba4.c and pac-glue.c expose)

This will allow an MIT DAL backend to be written to that interface.  This interface will not be an guaranteed ABI, but will hopefully not change too often. 

The MIT KDC can then listen and respond to packets (Samba4's KDC process simply will not open those ports), but the behaviour will be very much of the Samba4 KDC (as the real power is in the database and the transformations it performs).   This will avoid much duplication of work.

Samba4 will still use Heimdal internally, including as a helper to provide this interface.   If Heimdal adopts an similar interface for it's own reasons, this may eventually be dropped in favour of the native Heimdal backend/frontend interface.

By separating the processes, there is no need to modify the MIT KDC's main loop, or packet processing routines - only a 'DAL' backend.  Also, the symbols from Samba4 need not exist in the same process space as the MIT KDC, avoiding any symbol collisions that might otherwise occur. 

--abartlet 2009-10-08