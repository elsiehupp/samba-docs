Samba4/Proposal for IPA to AD trust
    <namespace>0</namespace>
<last_edited>2009-02-15T23:07:53Z</last_edited>
<last_editor>Abartlet</last_editor>

==============================

urpose
===============================

To link FreeIPA to AD in a way that minimises replication of data.   

===============================
Key assumptions
===============================

IPA and AD are both seperate DNS domains, seen by each other in the same company, that administrators which to join in such a way that users and services are easily accessed on both sides of the trust, using Kerberos.

That Kerberos is the only authentication protocol in use (that fallback to NTLM has been disabled or is unwanted)

===============================
Background
===============================

See the `Samba4/Linking_AD_and_unix_directories|discussion of various trust types available in AD`

===============================
Designs
===============================

There are two feasable designs for the IPA to AD trust:
* `Samba4/Proposal_for_IPA_to_AD_forest_trust|Forest Trusts`
* `Samba4/Proposal_for_IPA_to_AD_MIT_trust|MIT Trusts`