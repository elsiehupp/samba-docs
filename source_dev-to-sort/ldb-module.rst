Samba4/LDB/Module
    <namespace>0</namespace>
<last_edited>2009-11-17T23:00:47Z</last_edited>
<last_editor>Edewata</last_editor>

=================
Structures
=================

ldb_module_ops 
------------------------

.. code-block::

    struct ldb_module_ops {
    const char *name;
    int (*init_context) (struct ldb_module *);
    int (*search)(struct ldb_module *, struct ldb_request *); /* search */
    int (*add)(struct ldb_module *, struct ldb_request *); /* add */
    int (*modify)(struct ldb_module *, struct ldb_request *); /* modify */
    int (*del)(struct ldb_module *, struct ldb_request *); /* delete */
    int (*rename)(struct ldb_module *, struct ldb_request *); /* rename */
    int (*request)(struct ldb_module *, struct ldb_request *); /* match any other operation */
    int (*extended)(struct ldb_module *, struct ldb_request *); /* extended operations */
    int (*start_transaction)(struct ldb_module *);
    int (*prepare_commit)(struct ldb_module *);
    int (*end_transaction)(struct ldb_module *);
    int (*del_transaction)(struct ldb_module *);
    int (*sequence_number)(struct ldb_module *, struct ldb_request *);
    void *private_data;
};

ldb_module 
------------------------

.. code-block::

    struct ldb_module {
    struct ldb_module *prev, *next;
    struct ldb_context *ldb;
    void *private_data;
    const struct ldb_module_ops *ops;
};

=================
Operations
=================

ldb_module_new() 
------------------------

 struct ldb_module *ldb_module_new(TALLOC_CTX *memctx,
     struct ldb_context *ldb,
     const char *module_name,
     const struct ldb_module_ops *ops);

ldb_module_get_name() 
------------------------

 const char * ldb_module_get_name(struct ldb_module *module);

ldb_module_get_ctx() 
------------------------

 struct ldb_context *ldb_module_get_ctx(struct ldb_module *module);

ldb_module_get_private() 
------------------------

 void *ldb_module_get_private(struct ldb_module *module);

ldb_module_set_private() 
------------------------

 void ldb_module_set_private(struct ldb_module *module, void *private_data);

ldb_next_request() 
------------------------

 int ldb_next_request(struct ldb_module *module, struct ldb_request *request);

ldb_next_start_trans() 
------------------------

 int ldb_next_start_trans(struct ldb_module *module);

ldb_next_end_trans() 
------------------------

 int ldb_next_end_trans(struct ldb_module *module);

ldb_next_del_trans() 
------------------------

 int ldb_next_del_trans(struct ldb_module *module);

ldb_next_prepare_commit() 
------------------------

 int ldb_next_prepare_commit(struct ldb_module *module);

ldb_next_init() 
------------------------

 int ldb_next_init(struct ldb_module *module);

ldb_default_modules_dir() 
------------------------

 const char *ldb_default_modules_dir(void);

ldb_register_module() 
------------------------

 int ldb_register_module(const struct ldb_module_ops *);

ldb_module_send_entry() 
------------------------

 int ldb_module_send_entry(struct ldb_request *req,
    struct ldb_message *msg,
    struct ldb_control **ctrls);

ldb_module_send_referral() 
------------------------

 int ldb_module_send_referral(struct ldb_request *req, char *ref);

ldb_module_done() 
------------------------

 int ldb_module_done(struct ldb_request *req,
     struct ldb_control **ctrls,
     struct ldb_extended *response,
     int error);

ldb_mod_register_control() 
------------------------

 int ldb_mod_register_control(struct ldb_module *module, const char *oid);