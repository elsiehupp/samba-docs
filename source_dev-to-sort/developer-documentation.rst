Developer Documentation
    <namespace>0</namespace>
<last_edited>2021-04-28T05:08:30Z</last_edited>
<last_editor>Abartlet</last_editor>

=================
How can I contribute
=================

* `Contribute|Contribution HowTo`

* `Using_Git_for_Samba_Development|Using Git for Samba Development`

* `CodeReview|Doing code review`

* `Roadmap|Active projects`

=================
Release planning
=================

* `Samba_Release_Planning|Samba release planning` (including the `Samba_Release_Planning#Release_Branch_Checkin_Procedure| Policy and steps required for checkins to branches`)

* `Samba_Features_added/changed_(by_release)|Samba Features added/changed (by release)`

* `Roadmap|Samba Next Goals`

=================
Source code
=================

Samba development is stored in the [https://www.git-scm.com/ Git] SCM system.  See `Creating a Samba patch series|Using Git for Samba Development` for more detail on how Samba uses Git.

Two web interfaces are:
* [https://git.samba.org/?p=samba.git;a=heads Samba's own GitWeb] ([https://git.samba.org/?p=samba.git;a=shortlog;h=refs/heads/master latest changes in the master branch])
* [https://gitlab.com/samba-team/samba Official Samba GitLab Mirror]

Samba is no longer actively developed on [https://github.com GitHub] but for reference and to keep old code from being mistakenly examined, the [https://github.com/samba-team/samba Offical Samba Team GitHub Mirror] is kept up to date.

Samba codebase organization 
------------------------

{{ codebase organization overview}}

The Samba codebase is broken down in detail in the **`Samba_codebase_organization|Samba codebase organization page`**.

=================
Debugging / Testing
=================

* `The_Samba_Selftest_System|The Samba Selftest System`

* `Samba on GitLab|Samba CI on gitlab`

`Writing_Tests|Writing and running Samba tests`
------------------------

* `Writing_Torture_Tests|Writing Torture Tests`

* `Writing_Python_Tests|Writing Python Tests`

* `Writing_cmocka_Tests|Writing cmocka Tests`

* `Writing_Shell_Tests|Writing Shell Tests`

* `Writing_Perl_Tests|Writing Perl Tests`

* `WinTest|Testing against Windows using WinTest`

* `Understanding_make_test|Understanding make test`

Debugging help 
------------------------

* `Keytab_Extraction|Keytab extraction`

* `Wireshark_Keytab|Wireshark with keytab to decrypt encrypted traffic`

* `KernelClientTesting|How to test the Linux kernel client against Samba`

* `Saving_RPC_FAULTs|Saving packets that failed to parse to test IDL fixes using ndrdump`

* `Python/Debugging|Debugging in Python`

Development testing 
------------------------

* `Testing removal of ifdef or configure checks|Testing removal of #ifdef or configure checks`
* `Fuzzing|Information on Samba's fuzzing infrastructure`

=================
Bug reporting = 

* `Bug_Reporting|Bug reporting HowTo`

* `Capture_Packets|Packet capturing`

=================
Security
=================

* `Samba_Security_Documentation|Samba Security Documentation`
* `Samba_Protocol_Security|Samba Protocol Security Layers`

=================
Presentations
=================

* `Presentations|Presentations about Samba, including slides and papers`

=================
VFS
=================

* [http://www.samba.org/~sharpe/The-Samba-VFS.pdf Writing a Samba VFS]
* `The new version of Writing a Samba VFS` (for Samba 3.6 and earlier)
* `Writing a Samba VFS Module` (for Samba 4.0 and above)

=================
Clustered Samba
=================

* `CTDB_and_Clustered_Samba|CTDB and Clustered Samba`

* `New_clustering_features_in_SMB3_and_Samba|New clustering features in SMB3 and Samba`

=================
Building Samba
=================

* `BuildsystemUseAndWhy|Building Samba 4` (Which build system to use and why)

* `Waf|Using waf to Build Samba`

=================
Building packages
=================

* `Samba4/Debian|Building Debian Packages of Samba 4`

=================
Google Summer of Code
=================

* [http://www.google-melange.com/ Google Summer of Code]

* `SoC|Samba SoC page`

* `SoC/Ideas|SoC Samba project ideas`

=================
Historical Documentation on the Development of Samba4
=================

* `Samba4/DRS_TODO_List|Samba4 DRS ToDo List`

* `Samba/Status|Samba 4 Status`

* `Franky|Franky`: A Hybrid Samba Active Directory Controller (outdated!)

* `Samba4/s3fs|An Explanation of the s3fs Architecture for Using smbd in the AD Server`

* `Development Resources`

* `Samba4/Tests|Test Status`

* `SambaGtk|Gtk+ Frontends`

* `Samba4/ActiveDirectory|Active Directory Plans`

* `Samba4 AD Plugfest 2010 TODO list|Samba 4 AD Plugfest 2010 TODO List`

* [http://download.samba.org/pub/samba/rc/ Development Releases of Samba4 (technology previews, alphas, betas, release candidates)]

=================
Prehistoric documentation from the Samba3 development era
===============================

* [https://samba-team.gitlab.io/samba/htmldocs/Samba-Developers-Guide/index.html Samba Developers Guide]

=================
Academic Writings etc.
=================

* [https://www.samba.org/samba/news/articles/gensec-white-paper.pdf GENSEC white paper (2005)]

* [https://www.samba.org/samba/news/articles/abartlet_thesis.pdf Thesis on Samba (2005)]

* [https://www.samba.org/~metze/presentations/2007/thesis/StefanMetzmacher_Bachelorthesis_ENG_Draft-9811557.pdf Thesis on Samba replication (2007)]

* [https://www.samba.org/samba/news/articles/samba3-4_integration.pdf Integration of Samba 3 & Samba 4 (2005)]

* [http://is.muni.cz/th/359290/fi_b/thesis.pdf Thesis on Talloc (2012)]