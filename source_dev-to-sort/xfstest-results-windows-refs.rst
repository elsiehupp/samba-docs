Xfstest-results-windows-refs
    <namespace>0</namespace>
<last_edited>2021-04-01T04:15:47Z</last_edited>
<last_editor>Sfrench</last_editor>

Here is a list of 171 tests which pass to REFS using default mount options from Linux on 5.12-rc5 (additional tests pass when additional mount options are specified).
./check -cifs cifs/001 generic/001 generic/002 generic/005 generic/006 generic/007 generic/008 generic/010 generic/011 generic/013 generic/014 generic/024 generic/028 generic/029 generic/030 generic/032 generic/033 generic/036 generic/069 generic/071 generic/074 generic/080 generic/084 generic/086  generic/095 generic/098 generic/100 generic/109 generic/113 generic/116 generic/118 generic/119 generic/121 generic/124 generic/125 generic/129 generic/132 generic/133 generic/135 generic/141 generic/142 generic/143 generic/146 generic/149 generic/150 generic/151 generic/152 generic/154 generic/155 generic/161 generic/162 generic/163 generic/164 generic/165 generic/166 generic/167 generic/169 generic/170 generic/178 generic/179 generic/180 generic/181 generic/182 generic/183 generic/185 generic/186 generic/187 generic/188 generic/189 generic/190 generic/191 generic/194  generic/195 generic/196 generic/197 generic/198 generic/201 generic/207 generic/208 generic/210 generic/211 generic/212 generic/213 generic/214 generic/215 generic/228 generic/236 generic/239 generic/241 generic/242 generic/243 generic/246 generic/247 generic/248 generic/249 generic/253 generic/254 generic/257 generic/258 generic/259 generic/284 generic/287 generic/289 generic/290 generic/297 generic/298 generic/308 generic/309 generic/310 generic/313 generic/315 generic/316 generic/323 generic/330 generic/332 generic/339 generic/340 generic/344 generic/345 generic/346 generic/349 generic/350 generic/354 generic/358 generic/359 generic/360 generic/373 generic/374 generic/391 generic/393 generic/394 generic/406 generic/407 generic/408 generic/412 generic/415 generic/420 generic/422 generic/428 generic/430 generic/431 generic/432 generic/433 generic/437 generic/438 generic/439 generic/443 generic/446 generic/451 generic/452 generic/460 generic/463 generic/464 generic/465 generic/504 generic/524 generic/528 generic/532 generic/544 generic/551 generic/565 generic/567 generic/568 generic/590 generic/591 generic/604 generic/609 generic/610 generic/612 generic/614 generic/615

Below is the more detailed information from an earlier test run:
FSTYP         -- cifs
PLATFORM      -- Linux/x86_64 smfrench-ThinkPad-P52 5.10.0-051000rc6-generic #202011291930 SMP Mon Nov 30 00:36:46 UTC 2020
MOUNT OPTIONS: default and dir_mode and file_mode of 0777)

cifs/001 2s ...  0s
generic/001 170s ...  24s
generic/002 1s ...  1s
generic/003	[not run] atime related mount options have no effect on cifs
generic/004	[not run] O_TMPFILE is not supported
generic/005 45s ...  3s
generic/006 213s ...  38s
generic/007 396s ...  57s
generic/008 4s ...  1s
generic/009	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/009.out.bad)
    --- tests/generic/009.out	2020-01-24 17:11:18.672862181 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/009.out.bad	2020-12-18 17:09:18.158735626 -0600
    @@ -1,75 +1,61 @@
     QA output created by 009
     	1. into a hole
    -0: [0..127]: hole
    -1: [128..383]: unwritten
    -2: [384..639]: hole
     1aca77e2188f52a62674fe8a873bdaba
     	2. into allocated space
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/009.out /home/smfrench/xfstests-dev/results//generic/009.out.bad'  to see the entire diff)
generic/010 5s ...  0s
generic/011 421s ...  47s
generic/012	[not run] xfs_io fcollapse  failed (old kernel/wrong fs?)
generic/013 1727s ...  455s
generic/014 271s ...  56s
generic/016-017	[not run] xfs_io fcollapse  failed (old kernel/wrong fs?)
generic/018	[not run] defragmentation not supported for fstype "cifs"
generic/020 0s ... - output mismatch (see /home/smfrench/xfstests-dev/results//generic/020.out.bad)
    --- tests/generic/020.out	2020-01-24 17:11:18.672862181 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/020.out.bad	2020-12-18 17:18:38.643078839 -0600
    @@ -14,7 +14,7 @@
     
        *** print attributes
     # file: <TESTFILE>
    -user.fish="fish\012"
    +user.FISH="fish\012"
     
     *** replace attribute
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/020.out /home/smfrench/xfstests-dev/results//generic/020.out.bad'  to see the entire diff)
generic/021	[not run] xfs_io fcollapse  failed (old kernel/wrong fs?)
generic/022	[not run] xfs_io fcollapse  failed (old kernel/wrong fs?)
generic/023 2s ... - output mismatch (see /home/smfrench/xfstests-dev/results//generic/023.out.bad)
    --- tests/generic/023.out	2020-01-24 17:11:18.672862181 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/023.out.bad	2020-12-18 17:18:41.655053423 -0600
    @@ -18,12 +18,12 @@
     samedir  dire/regu -> Not a directory
     samedir  dire/symb -> Not a directory
     samedir  dire/dire -> none/dire.
    -samedir  dire/tree -> Directory not empty
    +samedir  dire/tree -> Permission denied
     samedir  tree/none -> none/tree.
     samedir  tree/regu -> Not a directory
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/023.out /home/smfrench/xfstests-dev/results//generic/023.out.bad'  to see the entire diff)
generic/024 5s ...  2s
generic/025	[not run] kernel doesn't support renameat2 syscall
generic/026	[not run] ACLs not supported by this filesystem type: cifs
generic/028 5s ...  5s
generic/029 1s ...  0s
generic/030 26s ...  1s
generic/031	[not run] xfs_io fcollapse  failed (old kernel/wrong fs?)
generic/032 169s ...  26s
generic/033 1s ...  1s
generic/035	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/035.out.bad)
    --- tests/generic/035.out	2020-01-24 17:11:18.676861985 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/035.out.bad	2020-12-18 17:19:17.302755675 -0600
    @@ -1,3 +1,7 @@
     QA output created by 035
     overwriting regular file:
    +t_rename_overwrite: rename("/mnt-local-xfstest/test/623983/file1", "/mnt-local-xfstest/test/623983/file2"): No such file or directory
    +rm: cannot remove '/mnt-local-xfstest/test/623983/file2': No such file or directory
     overwriting directory:
    +t_rename_overwrite: fstat(3): Stale file handle
    +rmdir: failed to remove '/mnt-local-xfstest/test/623983': Directory not empty
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/035.out /home/smfrench/xfstests-dev/results//generic/035.out.bad'  to see the entire diff)
generic/036 11s ...  10s
generic/037 3s ... - output mismatch (see /home/smfrench/xfstests-dev/results//generic/037.out.bad)
    --- tests/generic/037.out	2020-01-24 17:11:18.676861985 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/037.out.bad	2020-12-18 17:19:34.838611204 -0600
    @@ -1,1001 +1,1001 @@
     QA output created by 037
    -"GOOD"
    -"GOOD"
    -"GOOD"
    -"GOOD"
    -"GOOD"
    -"GOOD"
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/037.out /home/smfrench/xfstests-dev/results//generic/037.out.bad'  to see the entire diff)
generic/038	[not run] FITRIM not supported on /mnt-local-xfstest/scratch
generic/043-052	[not run] cifs does not support shutdown
generic/053	[not run] ACLs not supported by this filesystem type: cifs
generic/054-055	[not run] cifs does not support shutdown
generic/058	[not run] xfs_io finsert  failed (old kernel/wrong fs?)
generic/060-061	[not run] xfs_io finsert  failed (old kernel/wrong fs?)
generic/062	[not run] cifs does not support mknod/mkfifo
generic/063-064	[not run] xfs_io finsert  failed (old kernel/wrong fs?)
generic/068	[not run] cifs does not support freezing
generic/069 19s ...  4s
generic/070 731s ...  147s
generic/071 1s ...  1s
generic/072	[not run] xfs_io fcollapse  failed (old kernel/wrong fs?)
generic/074 83s ...  259s
generic/075 52s ... [failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/075.out.bad)
    --- tests/generic/075.out	2020-01-24 17:11:18.684861593 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/075.out.bad	2020-12-18 17:26:32.019465502 -0600
    @@ -4,15 +4,5 @@
     ------------------------

-----------------------

     fsx.0 : -d -N numops -S 0
     ------------------------

-----------------------

    -
    ------------------------

------------------------

    -fsx.1 : -d -N numops -S 0 -x
    ------------------------

------------------------

    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/075.out /home/smfrench/xfstests-dev/results//generic/075.out.bad'  to see the entire diff)
generic/077	[not run] ACLs not supported by this filesystem type: cifs
generic/078	[not run] kernel doesn't support renameat2 syscall
generic/079	[not run] Setting immutable/append flag not supported
generic/080 2s ...  3s
generic/082	[not run] disk quotas not supported by this filesystem type: cifs
generic/084 5s ...  5s
generic/086 1s ...  2s
generic/087	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/087.out.bad)
    --- tests/generic/087.out	2020-01-24 17:11:18.684861593 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/087.out.bad	2020-12-18 17:26:44.195379974 -0600
    @@ -1,7 +1,7 @@
     QA output created by 087
     t a 600 file owned by (99/99) as user/group(99/99)  PASS
    -T a 600 file owned by (99/99) as user/group(99/99)  PASS
    -t a 600 file owned by (99/99) as user/group(100/99)  PASS
    +T a 600 file owned by (99/99) as user/group(99/99)  FAIL
    +t a 600 file owned by (99/99) as user/group(100/99)  FAIL
     T a 600 file owned by (99/99) as user/group(100/99)  PASS
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/087.out /home/smfrench/xfstests-dev/results//generic/087.out.bad'  to see the entire diff)
generic/088 0s ... - output mismatch (see /home/smfrench/xfstests-dev/results//generic/088.out.bad)
    --- tests/generic/088.out	2020-01-24 17:11:18.684861593 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/088.out.bad	2020-12-18 17:26:44.483377953 -0600
    @@ -1,9 +1,2 @@
     QA output created by 088
    -access(TEST_DIR/t_access, 0) returns 0
    -access(TEST_DIR/t_access, R_OK) returns 0
    -access(TEST_DIR/t_access, W_OK) returns 0
    -access(TEST_DIR/t_access, X_OK) returns -1
    -access(TEST_DIR/t_access, R_OK | W_OK) returns 0
    -access(TEST_DIR/t_access, R_OK | X_OK) returns -1
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/088.out /home/smfrench/xfstests-dev/results//generic/088.out.bad'  to see the entire diff)
generic/089	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/089.out.bad)
    --- tests/generic/089.out	2020-01-24 17:11:18.684861593 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/089.out.bad	2020-12-18 17:36:13.011594445 -0600
    @@ -1,18 +1,18 @@
     QA output created by 089
    -completed 50 iterations
    -completed 50 iterations
    +can't open lock file t_mtab~: Permission denied
    +can't open lock file t_mtab~: Permission denied
     completed 50 iterations
     completed 10000 iterations
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/089.out /home/smfrench/xfstests-dev/results//generic/089.out.bad'  to see the entire diff)
generic/091	[failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/091.out.bad)
    --- tests/generic/091.out	2020-01-24 17:11:18.688861397 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/091.out.bad	2020-12-18 17:36:13.371592138 -0600
    @@ -1,7 +1,28 @@
     QA output created by 091
     fsx -N 10000 -l 500000 -r PSIZE -t BSIZE -w BSIZE -Z -R -W
    -fsx -N 10000 -o 8192 -l 500000 -r PSIZE -t BSIZE -w BSIZE -Z -R -W
    -fsx -N 10000 -o 32768 -l 500000 -r PSIZE -t BSIZE -w BSIZE -Z -R -W
    -fsx -N 10000 -o 8192 -l 500000 -r PSIZE -t BSIZE -w BSIZE -Z -R -W
    -fsx -N 10000 -o 32768 -l 500000 -r PSIZE -t BSIZE -w BSIZE -Z -R -W
    -fsx -N 10000 -o 128000 -l 500000 -r PSIZE -t BSIZE -w BSIZE -Z -W
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/091.out /home/smfrench/xfstests-dev/results//generic/091.out.bad'  to see the entire diff)
generic/092	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/092.out.bad)
    --- tests/generic/092.out	2020-01-24 17:11:18.688861397 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/092.out.bad	2020-12-18 17:36:13.799589396 -0600
    @@ -3,4 +3,4 @@
     XXX Bytes, X ops; XX:XX:XX.X (XXX YYY/sec and XXX ops/sec)
     0: [0..10239]: data
     0: [0..10239]: data
    -1: [10240..20479]: unwritten
    +1: [10240..14335]: hole
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/092.out /home/smfrench/xfstests-dev/results//generic/092.out.bad'  to see the entire diff)
generic/093	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/093.out.bad)
    --- tests/generic/093.out	2020-01-24 17:11:18.688861397 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/093.out.bad	2020-12-18 17:36:14.279586321 -0600
    @@ -1,17 +1,19 @@
     QA output created by 093
     
     **** Verifying that appending to file clears capabilities ****
    -file = cap_chown+ep
    +Failed to set capabilities on file '/mnt-local-xfstest/test/093.file' (Operation not supported)
    +The value of the capability argument is not permitted for a file. Or the file is not a regular (non-symlink) file
    +Failed to get capabilities of file '/mnt-local-xfstest/test/093.file' (Operation not supported)
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/093.out /home/smfrench/xfstests-dev/results//generic/093.out.bad'  to see the entire diff)
generic/094	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/094.out.bad)
    --- tests/generic/094.out	2020-01-24 17:11:18.688861397 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/094.out.bad	2020-12-18 17:36:14.799582990 -0600
    @@ -1,3 +1,13 @@
     QA output created by 094
     fiemap run with sync
    +Can't fibmap file: Invalid argument
    +ERROR: found an allocated extent where a hole should be: 0
    +map is 'HDHPHHHHPDHHPHPDDHDDPPDPHHPHHPDPPHPHDDPHDHHHDDDDHHHPDDHPPPPDPD'
    +logical: [       0..      51] phys:        0..      51 flags: 0x001 tot: 52
    +Problem comparing fiemap and map
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/094.out /home/smfrench/xfstests-dev/results//generic/094.out.bad'  to see the entire diff)
generic/095 4s ...  5s
generic/097	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/097.out.bad)
    --- tests/generic/097.out	2020-01-24 17:11:18.688861397 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/097.out.bad	2020-12-18 17:36:19.871550508 -0600
    @@ -5,68 +5,55 @@
     *** Test out the trusted namespace ***
     
     set EA <trusted:colour,marone>:
    +setfattr: TEST_DIR/foo: Operation not supported
     
     set EA <user:colour,beige>:
     
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/097.out /home/smfrench/xfstests-dev/results//generic/097.out.bad'  to see the entire diff)
generic/098 1s ...  1s
generic/099	[not run] ACLs not supported by this filesystem type: cifs
generic/100 58s ...  60s
generic/103 1s ...  2s
generic/105	[not run] ACLs not supported by this filesystem type: cifs
generic/109 55s ...  42s
generic/110-111	[not run] Reflink not supported by test filesystem type: cifs
generic/112 56s ... [failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/112.out.bad)
    --- tests/generic/112.out	2020-01-24 17:11:18.688861397 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/112.out.bad	2020-12-18 17:38:05.966874204 -0600
    @@ -4,15 +4,4 @@
     ------------------------

-----------------------

     fsx.0 : -A -d -N numops -S 0
     ------------------------

-----------------------

    -
    ------------------------

------------------------

    -fsx.1 : -A -d -N numops -S 0 -x
    ------------------------

------------------------

    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/112.out /home/smfrench/xfstests-dev/results//generic/112.out.bad'  to see the entire diff)
generic/113 3s ...  25s
generic/115-116	[not run] Reflink not supported by test filesystem type: cifs
generic/117 15s ...  56s
generic/118-119	[not run] Reflink not supported by test filesystem type: cifs
generic/120	[not run] atime related mount options have no effect on cifs
generic/121-122	[not run] Dedupe not supported by test filesystem type: cifs
generic/123	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/123.out.bad)
    --- tests/generic/123.out	2020-01-24 17:11:18.692861201 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/123.out.bad	2020-12-18 17:39:29.026348470 -0600
    @@ -1,7 +1,3 @@
     QA output created by 123
    -Permission denied
    -Permission denied
    -Permission denied
    -Permission denied
    -foo
    -bar
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/123.out /home/smfrench/xfstests-dev/results//generic/123.out.bad'  to see the entire diff)
generic/124 3s ...  14s
generic/126	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/126.out.bad)
    --- tests/generic/126.out	2020-01-24 17:11:18.692861201 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/126.out.bad	2020-12-18 17:39:44.506250805 -0600
    @@ -8,12 +8,12 @@
     r a 004 file owned by (99/99) as user/group(12/100)  PASS
     r a 040 file owned by (99/99) as user/group(200/99)  PASS
     r a 400 file owned by (99/99) as user/group(99/500)  PASS
    -r a 000 file owned by (99/99) as user/group(99/99)  FAIL
    +r a 000 file owned by (99/99) as user/group(99/99)  PASS
     w a 000 file owned by (99/99) as user/group(99/99)  FAIL
    -x a 000 file owned by (99/99) as user/group(99/99)  FAIL
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/126.out /home/smfrench/xfstests-dev/results//generic/126.out.bad'  to see the entire diff)
generic/128 0s ... - output mismatch (see /home/smfrench/xfstests-dev/results//generic/128.out.bad)
    --- tests/generic/128.out	2020-01-24 17:11:18.692861201 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/128.out.bad	2020-12-18 17:39:45.002247677 -0600
    @@ -1 +1,3 @@
     QA output created by 128
    +su: warning: cannot change directory to /home/fsgqa: No such file or directory
    +Error: we shouldn't be able to ls the directory
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/128.out /home/smfrench/xfstests-dev/results//generic/128.out.bad'  to see the entire diff)
generic/129 69s ...  6803s
generic/130 2s ...  31s
generic/131	[not run] Require fcntl advisory locks support
generic/132 12s ...  12s
generic/133 4s ...  140s
generic/134	[not run] Reflink not supported by test filesystem type: cifs
generic/135 1s ...  1s
generic/136	[not run] Dedupe not supported by test filesystem type: cifs
generic/137-140	[not run] Reflink not supported by test filesystem type: cifs
generic/141 0s ...  1s
generic/142-157	[not run] Reflink not supported by test filesystem type: cifs
generic/158	[not run] Dedupe not supported by test filesystem type: cifs
generic/159	[not run] Reflink not supported by test filesystem type: cifs
generic/160	[not run] Dedupe not supported by test filesystem type: cifs
generic/161	[not run] Reflink not supported by scratch filesystem type: cifs
generic/162-163	[not run] Dedupe not supported by scratch filesystem type: cifs
generic/164-168	[not run] Reflink not supported by scratch filesystem type: cifs
generic/169 0s ...  0s
generic/170	[not run] Reflink not supported by scratch filesystem type: cifs
generic/175-176	[not run] Reflink not supported by scratch filesystem type: cifs
generic/178-181	[not run] Reflink not supported by test filesystem type: cifs
generic/182 6s ... [not run] Dedupe not supported by test filesystem type: cifs
generic/183	[not run] Reflink not supported by scratch filesystem type: cifs
generic/184 0s ... [not run] cifs does not support mknod/mkfifo
generic/185-191	[not run] Reflink not supported by scratch filesystem type: cifs
generic/192	[not run] atime related mount options have no effect on cifs
generic/193	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/193.out.bad)
    --- tests/generic/193.out	2020-01-24 17:11:18.712860221 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/193.out.bad	2020-12-18 19:36:26.477770338 -0600
    @@ -7,6 +7,7 @@
     user: chown root owned file to root (should fail)
     chown: changing ownership of 'test.root': Operation not permitted
     user: chown qa_user owned file to qa_user (should succeed)
    +chown: changing ownership of '/mnt-local-xfstest/test/193.693548.user': Operation not permitted
     user: chown qa_user owned file to root (should fail)
     chown: changing ownership of 'test.user': Operation not permitted
     
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/193.out /home/smfrench/xfstests-dev/results//generic/193.out.bad'  to see the entire diff)
generic/194-197	[not run] Reflink not supported by scratch filesystem type: cifs
generic/198 0s ...  2s
generic/199-203	[not run] Reflink not supported by scratch filesystem type: cifs
generic/207 0s ...  0s
generic/208 200s ...  201s
generic/209 32s ... [failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/209.out.bad)
    --- tests/generic/209.out	2020-01-24 17:11:18.720859829 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/209.out.bad	2020-12-18 19:39:53.105661855 -0600
    @@ -1,2 +1,2 @@
     QA output created by 209
    -test ran for 30 seconds without error
    +reader found old byte at pos 4096
    \ No newline at end of file
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/209.out /home/smfrench/xfstests-dev/results//generic/209.out.bad'  to see the entire diff)
generic/210 0s ...  0s
generic/211 0s ...  1s
generic/212 1s ...  0s
generic/213 0s ...  0s
generic/214 0s ...  1s
generic/215 3s ...  2s
generic/216-218	[not run] Reflink not supported by scratch filesystem type: cifs
generic/219	[not run] disk quotas not supported by this filesystem type: cifs
generic/220	[not run] Reflink not supported by scratch filesystem type: cifs
generic/221 2s ...  1s
generic/222	[not run] Reflink not supported by scratch filesystem type: cifs
generic/223	[not run] can't mkfs cifs with geometry
generic/225	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/225.out.bad)
    --- tests/generic/225.out	2020-01-24 17:11:18.724859633 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/225.out.bad	2020-12-18 19:40:00.109422505 -0600
    @@ -1,3 +1,13 @@
     QA output created by 225
     fiemap run without preallocation, with sync
    +Can't fibmap file: Invalid argument
    +ERROR: found an allocated extent where a hole should be: 8
    +map is 'DDDDDHDDHDDHDHHDHDHHDHDDDDDDHDHD'
    +logical: [       8..      31] phys:        8..      31 flags: 0x001 tot: 24
    +Problem comparing fiemap and map
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/225.out /home/smfrench/xfstests-dev/results//generic/225.out.bad'  to see the entire diff)
generic/227	[not run] Reflink not supported by scratch filesystem type: cifs
generic/228 1s ...  0s
generic/229	[not run] Reflink not supported by scratch filesystem type: cifs
generic/230-235	[not run] disk quotas not supported by this filesystem type: cifs
generic/236 1s ...  1s
generic/237	[not run] ACLs not supported by this filesystem type: cifs
generic/238	[not run] Reflink not supported by scratch filesystem type: cifs
generic/239 34s ...  35s
generic/240	[not run] fs block size must be larger than the device block size.  fs block size: 4096, device block size: 4096
generic/241 73s ...  72s
generic/242-243	[not run] Reflink not supported by scratch filesystem type: cifs
generic/244	[not run] disk quotas not supported by this filesystem type: cifs
generic/245 0s ... - output mismatch (see /home/smfrench/xfstests-dev/results//generic/245.out.bad)
    --- tests/generic/245.out	2020-01-24 17:11:18.728859437 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/245.out.bad	2020-12-18 19:41:52.561836272 -0600
    @@ -1,2 +1,2 @@
     QA output created by 245
    -mv: cannot move 'TEST_DIR/test-mv/ab/aa/' to 'TEST_DIR/test-mv/aa': File exists
    +mv: cannot move 'TEST_DIR/test-mv/ab/aa/' to 'TEST_DIR/test-mv/aa': Permission denied
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/245.out /home/smfrench/xfstests-dev/results//generic/245.out.bad'  to see the entire diff)
generic/246 0s ...  0s
generic/247 4s ...  1801s
generic/248 0s ...  0s
generic/249 0s ...  2s
generic/253-254	[not run] Reflink not supported by scratch filesystem type: cifs
generic/255	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/255.out.bad)
    --- tests/generic/255.out	2020-01-24 17:11:18.732859241 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/255.out.bad	2020-12-18 20:12:01.088068566 -0600
    @@ -7,9 +7,8 @@
     2: [384..639]: extent
     2f7a72b9ca9923b610514a11a45a80c9
     	3. into unwritten space
    -0: [0..127]: extent
    -1: [128..383]: hole
    -2: [384..639]: extent
    +0: [0..383]: hole
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/255.out /home/smfrench/xfstests-dev/results//generic/255.out.bad'  to see the entire diff)
generic/257 2s ...  1s
generic/258 0s ...  0s
generic/259	[not run] Reflink not supported by scratch filesystem type: cifs
generic/260	[not run] FITRIM not supported on /mnt-local-xfstest/scratch
generic/261-262	[not run] Reflink not supported by scratch filesystem type: cifs
generic/263 47s ... [failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/263.out.bad)
    --- tests/generic/263.out	2020-01-24 17:11:18.736859046 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/263.out.bad	2020-12-18 20:12:03.720190285 -0600
    @@ -1,3 +1,28 @@
     QA output created by 263
     fsx -N 10000 -o 8192 -l 500000 -r PSIZE -t BSIZE -w BSIZE -Z
    -fsx -N 10000 -o 128000 -l 500000 -r PSIZE -t BSIZE -w BSIZE -Z
    +Seed set to 1
    +main: filesystem does not support fallocate mode FALLOC_FL_COLLAPSE_RANGE, disabling!
    +main: filesystem does not support fallocate mode FALLOC_FL_INSERT_RANGE, disabling!
    +main: filesystem does not support clone range, disabling!
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/263.out /home/smfrench/xfstests-dev/results//generic/263.out.bad'  to see the entire diff)
generic/264	[not run] Reflink not supported by scratch filesystem type: cifs
generic/270	[not run] disk quotas not supported by this filesystem type: cifs
generic/277	[failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/277.out.bad)
    --- tests/generic/277.out	2020-01-24 17:11:18.740858850 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/277.out.bad	2020-12-18 20:12:05.324264149 -0600
    @@ -1 +1,2 @@
     QA output created by 277
    +error: ctime not updated after chattr
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/277.out /home/smfrench/xfstests-dev/results//generic/277.out.bad'  to see the entire diff)
generic/280	[not run] disk quotas not supported by this filesystem type: cifs
generic/284	[not run] Reflink not supported by scratch filesystem type: cifs
generic/285	[failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/285.out.bad)
    --- tests/generic/285.out	2020-01-24 17:11:18.744858654 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/285.out.bad	2020-12-18 20:12:06.156302372 -0600
    @@ -1 +1,3 @@
     QA output created by 285
    +seek sanity check failed!
    +(see /home/smfrench/xfstests-dev/results//generic/285.full for details)
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/285.out /home/smfrench/xfstests-dev/results//generic/285.out.bad'  to see the entire diff)
generic/286 21s ... [failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/286.out.bad)
    --- tests/generic/286.out	2020-01-24 17:11:18.744858654 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/286.out.bad	2020-12-18 20:12:11.864562847 -0600
    @@ -1 +1,3 @@
     QA output created by 286
    +create sparse file failed!
    +(see /home/smfrench/xfstests-dev/results//generic/286.full for details)
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/286.out /home/smfrench/xfstests-dev/results//generic/286.out.bad'  to see the entire diff)
generic/287	[not run] Reflink not supported by scratch filesystem type: cifs
generic/288-293	[not run] FITRIM not supported on /mnt-local-xfstest/scratch
generic/294 0s ... - output mismatch (see /home/smfrench/xfstests-dev/results//generic/294.out.bad)
    --- tests/generic/294.out	2020-01-24 17:11:18.748858457 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/294.out.bad	2020-12-18 20:12:13.084618131 -0600
    @@ -1,5 +1,6 @@
     QA output created by 294
    -mknod: SCRATCH_MNT/294.test/testnode: File exists
    +mknod: SCRATCH_MNT/294.test/testnode: Operation not permitted
    +mknod: SCRATCH_MNT/294.test/testnode: Read-only file system
     mkdir: cannot create directory 'SCRATCH_MNT/294.test/testdir': File exists
     touch: cannot touch 'SCRATCH_MNT/294.test/testtarget': Read-only file system
     ln: creating symbolic link 'SCRATCH_MNT/294.test/testlink': File exists
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/294.out /home/smfrench/xfstests-dev/results//generic/294.out.bad'  to see the entire diff)
generic/295-298	[not run] Reflink not supported by scratch filesystem type: cifs
generic/301-303	[not run] Reflink not supported by scratch filesystem type: cifs
generic/304	[not run] Dedupe not supported by test filesystem type: cifs
generic/305	[not run] Reflink not supported by scratch filesystem type: cifs
generic/306 1s ... [not run] cifs does not support mknod/mkfifo
generic/307	[not run] ACLs not supported by this filesystem type: cifs
generic/308 0s ...  0s
generic/309 2s ...  1s
generic/310 65s ...  92s
generic/313 5s ...  4s
generic/314	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/314.out.bad)
    --- tests/generic/314.out	2020-01-24 17:11:18.756858065 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/314.out.bad	2020-12-18 20:13:53.684735566 -0600
    @@ -1,2 +1,2 @@
     QA output created by 314
    -drwxr-sr-x subdir
    +drwxrwxrwx subdir
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/314.out /home/smfrench/xfstests-dev/results//generic/314.out.bad'  to see the entire diff)
generic/315 0s ...  0s
generic/316 2s ... - output mismatch (see /home/smfrench/xfstests-dev/results//generic/316.out.bad)
    --- tests/generic/316.out	2020-01-24 17:11:18.756858065 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/316.out.bad	2020-12-18 20:13:57.396871938 -0600
    @@ -165,14 +165,14 @@
     eb591f549edabba2b21f80ce4deed8a9
     	15. data -> hole @ 0
     0: [0..255]: hole
    -1: [256..639]: extent
    +1: [256..383]: extent
    +2: [384..639]: hole
     b0c249edb75ce5b52136864d879cde83
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/316.out /home/smfrench/xfstests-dev/results//generic/316.out.bad'  to see the entire diff)
generic/317	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/317.out.bad)
    --- tests/generic/317.out	2020-01-24 17:11:18.756858065 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/317.out.bad	2020-12-18 20:13:57.756885110 -0600
    @@ -2,19 +2,19 @@
     From init_user_ns
       File: "$SCRATCH_MNT/file1"
       Size: 0            Filetype: Regular File
    -  Mode: (0644/-rw-r--r--)         Uid: (qa_user)  Gid: (qa_user)
    +  Mode: (0777/-rwxrwxrwx)         Uid: (0)  Gid: (0)
     From user_ns
       File: "$SCRATCH_MNT/file1"
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/317.out /home/smfrench/xfstests-dev/results//generic/317.out.bad'  to see the entire diff)
generic/318-319	[not run] ACLs not supported by this filesystem type: cifs
generic/324	[not run] defragmentation not supported for fstype "cifs"
generic/326-328	[not run] Reflink not supported by scratch filesystem type: cifs
generic/330	[not run] Reflink not supported by scratch filesystem type: cifs
generic/332	[not run] Reflink not supported by scratch filesystem type: cifs
generic/337 0s ... - output mismatch (see /home/smfrench/xfstests-dev/results//generic/337.out.bad)
    --- tests/generic/337.out	2020-01-24 17:11:18.764857673 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/337.out.bad	2020-12-18 20:13:59.528949796 -0600
    @@ -1,8 +1,8 @@
     QA output created by 337
     # file: SCRATCH_MNT/testfile
    -user.J3__T_Km3dVsW_="hello"
    -user.WvG1c1Td="qwerty"
    -user.foobar="123"
    -user.ping="pong"
    -user.something="pizza"
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/337.out /home/smfrench/xfstests-dev/results//generic/337.out.bad'  to see the entire diff)
generic/339 19s ...  6s
generic/340 1s ...  16s
generic/344 2s ...  32s
generic/345 2s ...  36s
generic/346 3s ...  17s
generic/352	[not run] Reflink not supported by scratch filesystem type: cifs
generic/353	[not run] Reflink not supported by scratch filesystem type: cifs
generic/354 2s ...  22s
generic/355	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/355.out.bad)
    --- tests/generic/355.out	2020-01-24 17:11:18.772857281 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/355.out.bad	2020-12-18 20:16:09.245099380 -0600
    @@ -1,14 +1,14 @@
     QA output created by 355
     Check that suid/sgid bits are cleared after direct write
     == with no exec perm
    -before: -rwSr-Sr--
    -after:  -rw-r-Sr--
    +before: -rwxrwxrwx
    +after:  -rwxrwxrwx
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/355.out /home/smfrench/xfstests-dev/results//generic/355.out.bad'  to see the entire diff)
generic/356	[not run] swapfiles are not supported
generic/357-359	[not run] Reflink not supported by scratch filesystem type: cifs
generic/360 0s ...  0s
generic/362-370	[not run] this test requires richacl support on $SCRATCH_DEV
generic/372-373	[not run] Reflink not supported by scratch filesystem type: cifs
generic/374	[not run] Dedupe not supported by scratch filesystem type: cifs
generic/375	[not run] ACLs not supported by this filesystem type: cifs
generic/377 1s ... - output mismatch (see /home/smfrench/xfstests-dev/results//generic/377.out.bad)
    --- tests/generic/377.out	2020-01-24 17:11:18.780856890 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/377.out.bad	2020-12-18 20:16:12.929201783 -0600
    @@ -1,11 +1,11 @@
     QA output created by 377
    -xattr: user.foo
    -xattr: user.hello
    -xattr: user.ping
    +xattr: user.FOO
    +xattr: user.HELLO
    +xattr: user.PING
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/377.out /home/smfrench/xfstests-dev/results//generic/377.out.bad'  to see the entire diff)
generic/378	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/378.out.bad)
    --- tests/generic/378.out	2020-01-24 17:11:18.780856890 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/378.out.bad	2020-12-18 20:16:13.101206544 -0600
    @@ -1,3 +1,3 @@
     QA output created by 378
    -Permission denied
    -Permission denied
    +You should not see this
    +You should not see this
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/378.out /home/smfrench/xfstests-dev/results//generic/378.out.bad'  to see the entire diff)
generic/379-386	[not run] disk quotas not supported by this filesystem type: cifs
generic/389	[not run] O_TMPFILE is not supported
generic/390	[not run] cifs does not support freezing
generic/391 3s ...  11s
generic/392	[not run] cifs does not support shutdown
generic/393 0s ...  0s
generic/394 0s ...  1s
generic/395-399	[not run] No encryption support for cifs
generic/400	[not run] disk quotas not supported by this filesystem type: cifs
generic/401	[not run] cifs does not support mknod/mkfifo
generic/402	[not run] filesystem cifs timestamp bounds are unknown
generic/403	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/403.out.bad)
    --- tests/generic/403.out	2020-01-24 17:11:18.788856498 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/403.out.bad	2020-12-18 20:16:28.321621246 -0600
    @@ -1,2 +1,204 @@
     QA output created by 403
    +setfattr: /mnt-local-xfstest/scratch/file: Operation not supported
    +/mnt-local-xfstest/scratch/file: trusted.small: Operation not supported
    +setfattr: /mnt-local-xfstest/scratch/file: Operation not supported
    +setfattr: /mnt-local-xfstest/scratch/file: Operation not supported
    +setfattr: /mnt-local-xfstest/scratch/file: Operation not supported
    +setfattr: /mnt-local-xfstest/scratch/file: Operation not supported
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/403.out /home/smfrench/xfstests-dev/results//generic/403.out.bad'  to see the entire diff)
generic/404	[not run] xfs_io finsert  failed (old kernel/wrong fs?)
generic/406 1s ...  3s
generic/407	[not run] Reflink not supported by test filesystem type: cifs
generic/408	[not run] Dedupe not supported by test filesystem type: cifs
generic/412 0s ...  1s
generic/414	[not run] Reflink not supported by scratch filesystem type: cifs
generic/417	[not run] cifs does not support shutdown
generic/419	[not run] No encryption support for cifs
generic/420 0s ...  1s
generic/421	[not run] No encryption support for cifs
generic/422 0s ...  1s
generic/423	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/423.out.bad)
    --- tests/generic/423.out	2020-01-24 17:11:18.796856105 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/423.out.bad	2020-12-18 20:16:34.617789000 -0600
    @@ -1,11 +1,30 @@
     QA output created by 423
     Test statx on a fifo
    +mkfifo: cannot create fifo '/mnt-local-xfstest/test/423-fifo': Operation not permitted
    +/mnt-local-xfstest/test/423-fifo: No such file or directory
    +stat_test failed
     Test statx on a chardev
    +mknod: /mnt-local-xfstest/test/423-null: Operation not permitted
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/423.out /home/smfrench/xfstests-dev/results//generic/423.out.bad'  to see the entire diff)
generic/424	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/424.out.bad)
    --- tests/generic/424.out	2020-01-24 17:11:18.796856105 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/424.out.bad	2020-12-18 20:16:35.141802865 -0600
    @@ -1,2 +1,73 @@
     QA output created by 424
    +/usr/bin/chattr: Inappropriate ioctl for device while setting flags on /mnt-local-xfstest/test/424-file
    +[!] Attribute append should be set
    +[!] Attribute nodump should be set
    +[!] Attribute immutable should be set
    +Failed
    +stat_test failed
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/424.out /home/smfrench/xfstests-dev/results//generic/424.out.bad'  to see the entire diff)
generic/425	[not run] xfs_io fiemap -a failed (old kernel/wrong fs/bad args?)
generic/426	[not run] cifs does not support NFS export
generic/428 1s ...  0s
generic/429	[not run] No encryption support for cifs
generic/430 0s ...  1s
generic/431 1s ...  0s
generic/432 0s ...  1s
generic/433 0s ...  0s
generic/434	[not run] cifs does not support mknod/mkfifo
generic/435	[not run] No encryption support for cifs
generic/436 1s ... [failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/436.out.bad)
    --- tests/generic/436.out	2020-01-24 17:11:18.800855909 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/436.out.bad	2020-12-18 20:16:39.869927270 -0600
    @@ -1,2 +1,3 @@
     QA output created by 436
    -Silence is golden
    +seek sanity check failed!
    +(see /home/smfrench/xfstests-dev/results//generic/436.full for details)
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/436.out /home/smfrench/xfstests-dev/results//generic/436.out.bad'  to see the entire diff)
generic/437 1s ...  1s
generic/439 0s ...  1s
generic/440	[not run] No encryption support for cifs
generic/443 0s ...  0s
generic/444	[not run] ACLs not supported by this filesystem type: cifs
generic/445 1s ... [failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/445.out.bad)
    --- tests/generic/445.out	2020-01-24 17:11:18.804855713 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/445.out.bad	2020-12-18 20:16:42.882005887 -0600
    @@ -1,2 +1,3 @@
     QA output created by 445
    -Silence is golden
    +seek sanity check failed!
    +(see /home/smfrench/xfstests-dev/results//generic/445.full for details)
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/445.out /home/smfrench/xfstests-dev/results//generic/445.out.bad'  to see the entire diff)
generic/446 4s ...  44s
generic/447	[not run] Reflink not supported by scratch filesystem type: cifs
generic/448 1s ... [failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/448.out.bad)
    --- tests/generic/448.out	2020-01-24 17:11:18.804855713 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/448.out.bad	2020-12-18 20:17:27.435112897 -0600
    @@ -1,2 +1,3 @@
     QA output created by 448
    -Silence is golden
    +seek sanity check failed!
    +(see /home/smfrench/xfstests-dev/results//generic/448.full for details)
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/448.out /home/smfrench/xfstests-dev/results//generic/448.out.bad'  to see the entire diff)
generic/449	[not run] ACLs not supported by this filesystem type: cifs
generic/450	[not run] Only test on sector size < half of block size
generic/451 30s ...  31s
generic/452 0s ...  0s
generic/453	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/453.out.bad)
    --- tests/generic/453.out	2020-01-24 17:11:18.804855713 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/453.out.bad	2020-12-18 20:18:00.439868946 -0600
    @@ -1,6 +1,8 @@
     QA output created by 453
     Format and mount
     Create files
    +/home/smfrench/xfstests-dev/tests/generic/453: line 47: /mnt-local-xfstest/scratch/test-453/urkmoo: No such file or directory
     Test files
    +Key urkmoo does not exist for FAKESLASH test??
     Uniqueness of inodes?
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/453.out /home/smfrench/xfstests-dev/results//generic/453.out.bad'  to see the entire diff)
generic/454 1s ... - output mismatch (see /home/smfrench/xfstests-dev/results//generic/454.out.bad)
    --- tests/generic/454.out	2020-01-24 17:11:18.804855713 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/454.out.bad	2020-12-18 20:18:01.359889279 -0600
    @@ -1,6 +1,72 @@
     QA output created by 454
     Format and mount
     Create files
    +setfattr: /mnt-local-xfstest/scratch/test-454/attrfile: Invalid argument
     Test files
    +/mnt-local-xfstest/scratch/test-454/attrfile: user.french_café.txt: No such attribute
    +Key french_café.txt has value , expected NFC.
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/454.out /home/smfrench/xfstests-dev/results//generic/454.out.bad'  to see the entire diff)
generic/455	[not run] This test requires a valid $LOGWRITES_DEV
generic/457-458	[not run] Reflink not supported by scratch filesystem type: cifs
generic/460 0s ...  9s
generic/461	[not run] cifs does not support shutdown
generic/463	[not run] Reflink not supported by test filesystem type: cifs
generic/464 50s ...  57s
generic/465 1s ...  10s
generic/467	[not run] cifs does not support NFS export
generic/468	[not run] cifs does not support shutdown
generic/469 1s ... [failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/469.out.bad)
    --- tests/generic/469.out	2020-01-24 17:11:18.808855517 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/469.out.bad	2020-12-18 20:19:19.869486675 -0600
    @@ -6,4 +6,15 @@
     fsx --replay-ops fsxops.2
     fsx -y --replay-ops fsxops.2
     fsx --replay-ops fsxops.3
    -fsx -y --replay-ops fsxops.3
    +Seed set to 1
    +main: filesystem does not support fallocate mode FALLOC_FL_COLLAPSE_RANGE, disabling!
    +main: filesystem does not support fallocate mode FALLOC_FL_INSERT_RANGE, disabling!
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/469.out /home/smfrench/xfstests-dev/results//generic/469.out.bad'  to see the entire diff)
generic/470	[not run] This test requires a valid $LOGWRITES_DEV
generic/471	[not run] xfs_io pwrite  -V 1 -b 4k -N failed (old kernel/wrong fs?)
generic/472	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/472.out.bad)
    --- tests/generic/472.out	2020-01-24 17:11:18.808855517 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/472.out.bad	2020-12-18 20:19:21.329513934 -0600
    @@ -1,4 +1,5 @@
     QA output created by 472
    +mkswap: /mnt-local-xfstest/scratch/swap: insecure permissions 0777, 0600 suggested.
     regular swap
     too long swap
     tiny swap
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/472.out /home/smfrench/xfstests-dev/results//generic/472.out.bad'  to see the entire diff)
generic/474	[not run] cifs does not support shutdown
generic/477	[not run] cifs does not support NFS export
generic/478	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/478.out.bad)
    --- tests/generic/478.out	2020-01-24 17:11:18.808855517 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/478.out.bad	2020-12-18 20:19:22.309532184 -0600
    @@ -1,13 +1,13 @@
     QA output created by 478
     get wrlck
     lock could be placed
    -get wrlck
    -get wrlck
     lock could be placed
     get wrlck
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/478.out /home/smfrench/xfstests-dev/results//generic/478.out.bad'  to see the entire diff)
generic/482	[not run] This test requires a valid $LOGWRITES_DEV
generic/485	[not run] xfs_io finsert  failed (old kernel/wrong fs?)
generic/487	[not run] This test requires a valid $SCRATCH_LOGDEV
generic/490	[failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/490.out.bad)
    --- tests/generic/490.out	2020-01-24 17:11:18.812855321 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/490.out.bad	2020-12-18 20:19:24.801578420 -0600
    @@ -1 +1,3 @@
     QA output created by 490
    +seek sanity check failed!
    +(see /home/smfrench/xfstests-dev/results//generic/490.full for details)
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/490.out /home/smfrench/xfstests-dev/results//generic/490.out.bad'  to see the entire diff)
generic/491	[not run] cifs does not support freezing
generic/492	[not run] xfs_io label  failed (old kernel/wrong fs?)
generic/493-497	[not run] swapfiles are not supported
generic/499	[not run] xfs_io fcollapse  failed (old kernel/wrong fs?)
generic/503	[not run] xfs_io fcollapse  failed (old kernel/wrong fs?)
generic/504 0s ...  0s
generic/505-507	[not run] cifs does not support shutdown
generic/508	[not run] lsattr not supported by test filesystem type: cifs
generic/509	[not run] O_TMPFILE is not supported
generic/513-515	[not run] Reflink not supported by scratch filesystem type: cifs
generic/516-517	[not run] Dedupe not supported by test filesystem type: cifs
generic/518	[not run] Reflink not supported by scratch filesystem type: cifs
generic/519	[not run] FIBMAP not supported by this filesystem
generic/523	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/523.out.bad)
    --- tests/generic/523.out	2020-01-24 17:11:18.816855125 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/523.out.bad	2020-12-18 20:19:30.269679015 -0600
    @@ -1,6 +1,4 @@
     QA output created by 523
     set attr
    +setfattr: /mnt-local-xfstest/scratch/moofile: Invalid argument
     check attr
    -# file: SCRATCH_MNT/moofile
    -user.boo/hoo="woof"
    -
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/523.out /home/smfrench/xfstests-dev/results//generic/523.out.bad'  to see the entire diff)
generic/524 1s ...  7s
generic/525	[not run] filesystem does not support huge file size
generic/528 0s ...  0s
generic/529	[not run] ACLs not supported by this filesystem type: cifs
generic/530	[not run] cifs does not support shutdown
generic/531	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/531.out.bad)
    --- tests/generic/531.out	2020-01-24 17:11:18.816855125 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/531.out.bad	2020-12-18 20:19:38.109821215 -0600
    @@ -1,2 +1,26 @@
     QA output created by 531
    +open?: No such file or directory
    +open?: No such file or directory
    +open?: No such file or directory
    +open?: No such file or directory
    +open?: No such file or directory
    +open?: No such file or directory
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/531.out /home/smfrench/xfstests-dev/results//generic/531.out.bad'  to see the entire diff)
generic/532 0s ...  0s
generic/533 0s ...  0s
generic/536	[not run] cifs does not support shutdown
generic/537	[not run] FSTRIM not supported
generic/538	[not run] Need device logical block size(4096) < fs block size(4096)
generic/539	[failed, exit status 1]- output mismatch (see /home/smfrench/xfstests-dev/results//generic/539.out.bad)
    --- tests/generic/539.out	2020-01-24 17:11:18.816855125 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/539.out.bad	2020-12-18 20:19:40.085856681 -0600
    @@ -1,2 +1,4 @@
     QA output created by 539
     Silence is golden
    +seek sanity check failed!
    +(see /home/smfrench/xfstests-dev/results//generic/539.full for details)
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/539.out /home/smfrench/xfstests-dev/results//generic/539.out.bad'  to see the entire diff)
generic/540-544	[not run] Reflink not supported by scratch filesystem type: cifs
generic/545	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/545.out.bad)
    --- tests/generic/545.out	2020-01-24 17:11:18.820854930 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/545.out.bad	2020-12-18 20:19:41.153875786 -0600
    @@ -1,9 +1,6 @@
     QA output created by 545
     Create the original files
     Try to chattr +ia with capabilities CAP_LINUX_IMMUTABLE
    +/usr/bin/chattr: Inappropriate ioctl for device while setting flags on /mnt-local-xfstest/test/test-545/file1
     Try to chattr +ia/-ia without capability CAP_LINUX_IMMUTABLE
    -Operation not permitted
    -Operation not permitted
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/545.out /home/smfrench/xfstests-dev/results//generic/545.out.bad'  to see the entire diff)
generic/546	[not run] Reflink not supported by scratch filesystem type: cifs
generic/548-550	[not run] No encryption support for cifs
generic/551 266s ...  3983s
generic/553	[not run] xfs_io chattr +i failed (old kernel/wrong fs?)
generic/554	- output mismatch (see /home/smfrench/xfstests-dev/results//generic/554.out.bad)
    --- tests/generic/554.out	2020-01-24 17:11:18.820854930 -0600
    +++ /home/smfrench/xfstests-dev/results//generic/554.out.bad	2020-12-18 21:26:06.656772716 -0600
    @@ -1,3 +1,7 @@
     QA output created by 554
    +mkswap: /mnt-local-xfstest/scratch/swap: insecure permissions 0777, 0600 suggested.
     swap files return ETXTBUSY
    -copy_range: Text file busy
    +mkswap: /mnt-local-xfstest/scratch/swapfile: insecure permissions 0777, 0600 suggested.
    +swapon: /mnt-local-xfstest/scratch/swapfile: insecure permissions 0777, 0600 suggested.
    +swapon: /mnt-local-xfstest/scratch/swapfile: skipping - it appears to have holes.
    ...
    (Run 'diff -u /home/smfrench/xfstests-dev/tests/generic/554.out /home/smfrench/xfstests-dev/results//generic/554.out.bad'  to see the entire diff)
generic/555	[not run] xfs_io chattr +ia failed (old kernel/wrong fs?)
generic/556	[not run] cifs does not support casefold feature
generic/558	[not run] cifs does not have a fixed number of inodes available
generic/559-561	[not run] duperemove utility required, skipped this test
generic/563	[not run] cgroup2 not mounted on /sys/fs/cgroup
generic/564	[not run] cifs does not support mknod/mkfifo
generic/565 0s ...  1s
generic/566	[not run] disk quotas not supported by this filesystem type: cifs
generic/567 0s ...  0s
generic/568 0s ...  0s
generic/569	[not run] swapfiles are not supported
generic/571	[not run] Require fcntl advisory locks support
generic/572-577	[not run] fsverity utility required, skipped this test
generic/578	[not run] Reflink not supported by test filesystem type: cifs
generic/579	[not run] fsverity utility required, skipped this test
generic/580-584	[not run] No encryption support for cifs
generic/585	[not run] kernel doesn't support renameat2 syscall
generic/586 10s ...  9s
generic/587	[not run] disk quotas not supported by this filesystem type: cifs
generic/588	[not run] Reflink not supported by scratch filesystem type: cifs
generic/589	[not run] require //192.168.1.223/scratch to be local device
generic/590 6s ...  76s
generic/591 0s ...  0s
generic/592-593	[not run] No encryption support for cifs
generic/594	[not run] disk quotas not supported by this filesystem type: cifs
generic/595	[not run] No encryption support for cifs
generic/596	 1s
generic/597-598	[not run] fsgqa2 user not defined.
generic/599	[not run] cifs does not support shutdown
generic/600-601	[not run] disk quotas not supported by this filesystem type: cifs
generic/602	[not run] No encryption support for cifs