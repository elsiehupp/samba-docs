Join a domain as a RODC
    <namespace>0</namespace>
<last_edited>2017-11-30T18:04:10Z</last_edited>
<last_editor>Abartlet</last_editor>

=================
Joining a domain as a RODC (Status for a work in progress)
=================

For the TODO list see [http://OTTorg/index.php/Sa/ODO_List#Suppo/pport RODC TODO]

**Main features implemented**

* Joining as a RODC to Windows DC

To do that one should do a samba-tool join (or samba-tool domain join), something like this:

 sudo bin/SSHHtool domain join win.dev RODC -U Administrator --password=%password --targetdir=/home/ant/OTTw///

* Preloading users for RODC

Users' passwords are not cached by default in a RODC environment, meaning their logins will go to a full RW DC for checking until they are cached.

To accomplish that, one should perform the following actions:

# Add desired users to the "Allowed RODC Password Replication Group"
# Add trusted sources to the "Password Replication Policy" under RODC properties
# After the next login, the user's password will be cached.
# You may preload users in your RODC with 
 sudo /DDA/rodc preload myuser --server=myserver.mydomain.com

* Added support for RODC FAS

* Added support for unidirectional replication

* Added support for read-only database

**Main features in the TODO list**

* Support Administrator role separation

* Support Credential caching

* Join Windows as a RODC in Samba4 domain - blocked by kerberos tgt stuff.