SambaHK
    <namespace>0</namespace>
<last_edited>2015-09-05T23:07:52Z</last_edited>
<last_editor>Mmuehlfeld</or>
=================
Samba Hong Kong
=================

Samba Hong Kong 是成立於 2004 年四月，當年 Tridge 來港介紹 Samba4 ；因此成立本會。

=================
Samba Hong Kong 宗旨
=================

Samba Hong Kong 的宗旨是向香港、台灣及中國大陸推廣 Samba 伺服器的運作，（亦有介紹不同的 Open Source 應用方案。）

=================
Samba Hong Kong 的網上資源
=================

我們定期舉行 Open Classroom 的活動。以技術推廣和介紹為主。而當中的內容，我們更制作了不同的錄影。（多謝會員 Reno 提供）

 http://room.samba.hk

新聞組。（多謝會員 Lisa 提供）

 news://Tsamba.hk/sambaDDO/ - 有關 samba.hk servers 或內部的問題
 news://Tsamba.hk/sambaDDO/ - 報到 samba.hk 和其他 events
 news://Tsamba.hk/sambaDDO/ - 關於 samba.hk 義工和幫手
 news://Tsamba.hk/sambaDDO/ssroom - openclassroom 問題
 news://Tsamba.hk/sambaDDO/rner - 自己實習遇到的事宜

=================
主網站
=================

http://samba.hk

姊妹網站 
------------------------

http://openworkshop.org