Create a samba lab-domain
    <namespace>0</namespace>
<last_edited>2018-11-05T02:12:22Z</last_edited>
<last_editor>Timbeale</last_editor>

=================
Overview
=================

Having a lab domain gives you a realistic environment for pre-production testing, without impacting your live network. It allows you to test new Samba releases prior to deploying the upgrade across your network. A lab domain provides a sandbox for trying out new Samba functionality, or running tools like `traffic replay`, which simulates Samba network load.

In the past, setting up a Samba lab domain has been difficult. The main problem is that running a second Samba domain with the same NETBIOS name and DNS realm will cause chaos on your production network if traffic from the two domains ever mixes. So you have to completely isolate *all* traffic from the lab domain from the rest of your network, which has proven very tricky to get right.

You can now avoid this headache by using the `domain rename tool`, which essentially creates a clone of your production domain with a different NETBIOS name and DNS realm.

The basic steps involved in creating a lab domain are:
* Generate a 'rename' backup of one of your production DCs.
* Restore the backup-file onto the lab DC.
* Reset any passwords needed for testing the lab domain.
* Start ``samba`` on the lab DC.
* Join subsequent DCs to the lab DC as needed, to scale the lab network.

The rest of this page walks through these steps in more detail.

=================
Steps Involved
=================

Generate a renamed backup 
------------------------

The first step is to take a 'rename' backup of one of your production DCs. This clones the production DC's database, renaming all the objects in the database as it goes. The resulting database files are then written to a .tar.bz2 backup-file. Note that the backup command does not have any side-effects on the production DC, apart from the extra overhead on the server (which is similar load to joining a new DC).

The rename command takes the format:

<pre>samba-tool domain backup rename <new-domain-netbios> <newdomain-dns-realm> --server=<dc-to-backup> --targetdir=<output-dir> --no-secrets -UAdministrator</pre>

You need to specify a new NETBIOS name and a new DNS realm for the lab domain. The targetdir is just the location where the generated backup-file will be written. For example, the actual command you run might look something like:

<pre>sudo samba-tool domain backup rename LABDOMAIN lab.example.com --server=PROD-DC --targetdir=/root/samba-backups/ --no-secrets -UAdministrator</pre>

Note that the ``--no-secrets`` option excludes sensitive password information (i.e. attributes like unicodePwd, lmPwdHistory, etc) for all the users in the domain. However, the backup-file still contains sensitive information, such as all the user account-names in your network, so it's still not the type of thing you can post on a public mailing-list.

You should be able to run the backup command on the machine you'll use as your lab DC (providing it has connectivity with your production domain). In this case, it's a good idea to have an smb.conf file that closely matches your production DC and pass this to the backup command (i.e. using the ``--configfile=smb.conf`` option). This ensures the backup will contain an smb.conf that closely matches your domain.

Otherwise, if you're running the backup command on another machine (e.g. the production DC itself), then copy the generated backup-file onto your lab DC machine, ready for the next step.

Restore the backup-file onto the lab DC 
------------------------

Next, decide on a directory location on your lab DC where you want the Samba database files to reside. Note that you can't use default installation location (e.g. <tt>/usr/local/samba/</tt>) because the directory has to be empty (or not exist yet). However, you can specify a sub-directory underneath (e.g. <tt>/usr/local/samba/labdom/</tt>), if you wanted to. We'll refer to this directory a lot in the next few commands, so let's set it as a bash variable.

<pre>export RESTORE_DIR=/usr/local/samba/labdom</pre>

Now extract the Samba database files from the backup-file using the 'restore' command, which has the format:

<pre>samba-tool domain backup restore --targetdir=<output-dir> --newservername=<new-dc-name> --backup-file=<tar-file></pre>

You pass it the backup-file generated in the previous step, along with the server-name the new lab DC will use. Note that the lab-DC cannot use the same server-name as a DC in your production network. The actual command you run might look something like:

<pre>sudo samba-tool domain backup restore --targetdir=$RESTORE_DIR --newservername=LAB-DC1 --backup-file=/root/samba-backups/samba-backup-lab.example.com-2018-08-09T13-03-38.817855.tar.bz2</pre>

You now have all the database files on disk that you need for the new lab domain. All the production user and machine accounts exist, but the lab database contains none of the passwords. The DN of every object in the database has been renamed, to reflect the change in domain name. Any traces of the production DCs have been removed from the lab database, and the lab domain now only contains the one DC you specified with ``--newservername``.

Reset passwords 
------------------------

During the backup/restore, the password for the Administrator's user account is reset to a randomly-generated password. You probably want to change this to something easier to remember (but not the same as your production domain). You can just update the database locally on disk, by running:

<pre>sudo samba-tool user setpassword Administrator --newpassword=<password> -H $RESTORE_DIR/private/sam.ldb</pre>

For testing user authentication, etc, you may want to either add extra 'test' user/machine accounts or 'commandeer' some of the accounts copied from the production domain. The accounts copied from the production domain will not have passwords set, so you could reset passwords for selected accounts at this point too. Or you could do this later, once Samba is actually running on the lab DC.

Starting up Samba 
------------------------

Before you start the ``samba`` executable on the lab DC, double-check that the settings in the smb.conf look correct. e.g.

<pre>sudo cat $RESTORE_DIR/etc/smb.conf</pre>

Once you're happy, to start ``samba`` on the lab DC you need to specify the smb.conf you just restored. This will ensure Samba loads the correct database files for the lab domain. e.g.

<pre>sudo samba --configfile=$RESTORE_DIR/etc/smb.conf</pre>

Note that you may get a few DNS errors logged when you first start ``samba``. This due to ``samba_dnsupdate`` running automatically and adding DNS records for the new domain.

Once ``samba`` is running, you can then perform some basic checks that your lab DC is operating correctly, e.g.

<pre>ldbsearch -H ldap://LAB-DC1 -UAdministrator</pre>

Update Site Subnets 
------------------------

The new lab domain will contain all of the `Active Directory Sites|AD Sites` in your production domain, but none of the production DCs. However, the Subnets that these Sites use will likely no longer make sense for your lab domain. Depending on how closely you want to mimic your production domain, you can either:
* Retain the Sites, but change the Subnets to match IP subnets that your lab domain will use.
* Remove the production Sites and Subnets from the lab domain, i.e. either use just a single Site for the lab domain, or add your own lab-specific Sites.

To list the Sites in your lab domain, and the Subnets they relate to, use:
<pre>ldbsearch -H ldap://LAB-DC1 -UAdministrator "(objectclass=site)" --cross-ncs cn siteObjectBL</pre>

Note that the restored DC will be added to the 'Default-First-Site-Name' site (creating the site, if it doesn't already exist). You can specify a different site to use in the restore command, using the <tt>--site</tt> option.

To add or remove Sites and Subnets, use one of the following commands:

.. code-block::

    samba-tool sites remove <site-name>
samba-tool sites remove subnet remove <subnet>
samba-tool sites create <site-name> 
samba-tool sites remove subnet <subnet> <site-name>
(Along with the DB and credentials, i.e. ``-H ldap://LAB-DC1 -UAdministrator``).

For example, the below bash code will generate commands to remove all Subnets from the lab domain.

.. code-block::

    SERVER=LAB-DC1
kinit Administrator
SUBNETS=$(bin/ldbsearch -H ldap://$SERVER "(objectclass=subnet)" --cross-ncs -k yes | grep "cn: " | sed "s/cn: //")
for subnet in $SUBNETS ; do echo "samba-tool sites subnet remove $subnet -H ldap://$SERVER -k yes"; done

Join subsequent lab DCs 
------------------------

Once you're happy that your new lab DC is operating correctly, you can scale the size of your lab network by adding in more DCs. On the new DC machine, simply join it to the existing lab DC, e.g.

<pre>samba-tool domain join lab.example.com DC --server=LAB-DC1 -UAdministrator</pre>

See `Joining a Samba DC to an Existing Active Directory` for more details.

Note that the server-names that you give to your lab DCs should not match any DCs in your production domain. Otherwise the lab DC's NETBIOS name could interfere with the operation of your production domain.

Troubleshooting problems 
------------------------

See `Back_up_and_Restoring_a_Samba_AD_DC#Troubleshooting|Backup and restoring a Samba AD` for debugging suggestions.

`Category:Backup` `Category:Active Directory`

Custom testenv 
------------------------

Note that if you really wanted, alternatively you could load the lab domain into the selftest environment used for Samba's automated testing. The testenv will run the Samba server locally against the lab domain database, using socket wrapping that prevents packets escaping onto the network. See `customdc testenv` for more details.