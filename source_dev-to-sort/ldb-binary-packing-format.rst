LDB binary packing format
    <namespace>0</namespace>
<last_edited>2019-05-29T03:38:34Z</last_edited>
<last_editor>Garming</last_editor>

Overview 
------------------------

**Added in Samba version:** *<The next Samba release containing the feature>*

*<What does the feature do, Why might users care about it, What is the Microsoft-equivalent that a Windows admin might be familiar with, etc>*

How to configure it
------------------------

*<Just reference the relevant samba-tool/smb.conf options (your man-page/help documentation should already be clear enough)>*

Known issues and limitations
------------------------

*<Any outstanding bugs, configurations not supported, etc>*

Troubleshooting
------------------------

*<What debug level do you need to run to see messages of interest. Are there any other ways to verify the feature is doing what it should>*

For Developers 
------------------------

https://wiki.samba.org/index.php/LDB_binary_packing_format_(developers)

`Category:New Feature`