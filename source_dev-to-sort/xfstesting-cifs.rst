Xfstesting-cifs
    <namespace>0</namespace>
<last_edited>2021-03-16T20:44:06Z</last_edited>
<last_editor>Sfrench</last_editor>

`Category:CIFSclient`

Please update if you see corrections ...

=================
Setting up the tests
=================
The xfstest suite has been updated to make it easier to test cifs (and smb3). 

First clone the tree
------------------------

   git clone git://git.kernel.org/pub/scm/fs/xfs/xfstests-dev.git
and also if you do not have xfsprogs headers installed on your distro already you can build it yourself
   git clone git://git.kernel.org/pub/scm/fs/xfs/xfsprogs-dev.git
and then do make followed by:
   sudo make install-qa
Build the source
------------------------

For up-to-date build instructions see the official setup guide in the source tree, xfstests-dev/README

 sudo yum install gcc uuid-devel libtool e2fsprogs-devel automake libuuid-devel libattr-devel libacl-devel libaio-devel fio gdbm-devel dbench quota-devel
or on ubuntu the following packages

 apt-get install gcc xfslibs-dev uuid-dev libtool e2fsprogs automake libuuid1 libuuidm-ocaml-dev attr libattr1-dev libacl1-dev libaio-dev gawk xfsprogs libgdbm-dev liburing-dev quota fio dbench sudo libtool-bin e2fsprogs automake gcc make gawk uuid-runtime python sqlite3

And then actually build the test package:

 make
 sudo make install-qa
 cd ../xfstests
 ./configure
 make

Add the secondary test userid (fsgqa) and group if it does not already exist, and add the user to the server you wish to mount to (in our example we used user "test" with password "testpasswd" but you can specify any that you want on the mount.  Make sure that you create the share on the test server and change the SERVER and TEST, SCRATCH to the appropriate UNC name for your test and scratch export.
 sudo useradd fsgqa

Create mountpoints for testing 
------------------------

Testing needs two mountpoints, one for the test share and another for a scratch device/share. The example config is set up to use /mnt/test and /mnt/scratch for these.

    udo mkdir /mnt/test
    udo mkdir /mnt/scratch

Setup the configuration files 
------------------------

Download the the skeleton config file from `Media:Xfstests.local.config.txt|local config` to the xfstests directory and name it local.config.

    get https://wiki.samba.org/images/9/99/Xfstests.local.config.txt -O local.config

Edit it and update SERVER, TEST, SCRATCH, USERNAME and PASSWORD to match your system.

The configuration file comes with predefined sections for several common testscases, such as SMB3, SMB2, CIFS(a.k.a SMB1), and special sections when testing against
Samba servers. You can delete those sections you don't need.

Exclusion files 
------------------------

There are many tests that do not work yet with some of the dialects or just take too long to run over a network. To handle this we provide prepackaged exclusion files to be used with xfstests.
Copy these files to the tests/cifs subdirectory:

    get https://wiki.samba.org/images/d/db/Xfstests.exclude.very-slow.txt -O tests/cifs/exclude.very-slow.txt
    get https://wiki.samba.org/images/b/b0/Xfstests.exclude.incompatible-smb3.txt -O tests/cifs/exclude.incompatible-smb3.txt
    get https://wiki.samba.org/images/9/9f/Xfstests.exclude.incompatible-smb2.txt -O tests/cifs/exclude.incompatible-smb2.txt

Server Configuration Hints 
------------------------

If running to Samba, you will typically want to check the share settings in the server's smb.conf configuration file in order to enable "ea support = yes" (to enable xattr support) and if running with btrfs "vfs objects = btrfs" (to allow tests for reflink support to run)

Run the tests 
------------------------

Make sure you have at least 40GB disk space free on your test target.

Run the SMB3 tests:
 sudo ./check -s smb3 -E tests/cifs/exclude.incompatible-smb3.txt -E tests/cifs/exclude.very-slow.txt

Run the SMB2 tests:
 sudo ./check -s smb2 -E tests/cifs/exclude.incompatible-smb2.txt -E tests/cifs/exclude.very-slow.txt

or run specific test cases (the following is a recommended list for regression testing for mounts with defaults)
 ./check -cifs  cifs/001 generic/001 generic/002 generic/005 generic/006 generic/007 generic/010 generic/011 generic/013 generic/014 generic/023 generic/024 generic/028 generic/029 generic/030 generic/036 generic/069 generic/074 generic/075 generic/084 generic/091 generic/095 generic/098 generic/100  generic/109 generic/112 generic/113 generic/124 generic/127 generic/129 generic/130 generic/132 generic/133 generic/135 generic/141 generic/169 generic/198  generic/207 generic/208 generic/210 generic/211 generic/212 generic/221 generic/239 generic/241 generic/245 generic/246 generic/247 generic/248 generic/249 generic/257 generic/263 generic/285 generic/286 generic/308 generic/309 generic/310 generic/315 generic/323 generic/339 generic/340 generic/344 generic/345 generic/346 generic/354 generic/360 generic/393 generic/394

Current Test Results
------------------------

Detail on current testing of default dialect to Samba can be found here `xfstest-results-smb3|Detailed test results and timing for Linux kernel client mounted with SMB3.1.1 to Samba` and for Windows running REFS can be found here `xfstest-results-windows-refs|Detailed test results and timing for Linux kernel client mounted with SMB3.1.1 to Windows`

<ol>
==============================

ailing test cases
===============================

------------------------

#cifs/001 (cifs only failure, since this copy offload feature requires SMB2/SMB3)
#020 "Extended attribute support and case preserving EAs" (EA keys are not case preserving)
#035 stale file handle
#081 (SMB3 only)
#087 and 088 ownership bugs 
#089 "can't open lock file t_mtab" (CIFS only)
#105 "chown: changing ownership of subdir: permission denied" (CIFS only until SMB3 adds ACL support)
#120
#123 "Permission denied" (SMB only)
#126 and 193 fail chown (need to configure usermapping on cifs client?)
#131 No advisory locks for CIFS/SMB3
#184 "mknod" (Will work if 'sfu' mount option enabled since it does a mknod)
#192 (SMB3 only) and 313 file mtime/ctime timestamp failure
#209 "aio readahead" file corruption (SMB3 only)
#Various testcases, including 237, 314, shared051,  also require posix acl support (which we don't emulate)
#258 "Testing for negative seconds since the epoch" fails to Samba works to Windows (not client problem - Samba server bug)
#Adding quota support would be required for test 270
#Other cifs failures: tests 294, 306, 313, 314, 317, 318, 319

===============================
Test detail by server type and protocol
===============================

------------------------

#To current Windows (SMB3 or later):  
cifs/001 generic/001 generic/002 generic/005 generic/006 generic/007 generic/008 generic/010 generic/011 generic/013 generic/014 generic/024 generic/028 generic/029 generic/030 generic/032 generic/033 generic/036 generic/069 generic/070 generic/071 generic/074 generic/080 generic/084 generic/086 generic/095 generic/098 generic/098 generic/100 generic/103 generic/109 generic/113 generic/117 generic/124 generic/129 generic/130 generic/132 generic/133 generic/135 generic/141 generic/169 generic/198 generic/207 generic/208 generic/210 generic/211 generic/212 generic/213 generic/214 generic/215 generic/221 generic/228 generic/236 generic/239 generic/241 generic/246 generic/247 generic/248 generic/249 generic/257 generic/258 generic/308 generic/309 generic/310 generic/313 generic/315 generic/339 generic/340 generic/344 generic/345 generic/346 generic/354 generic/360 generic/391 generic/393 generic/394 generic/406 generic/412 generic/420 generic/422 generic/428 generic/430 generic/431 generic/432 generic/433 generic/437 generic/439 generic/443 generic/446 generic/451 generic/452 generic/460 generic/464 generic/465 generic/504 generic/524 generic/528 generic/532 generic/533 generic/551 generic/565 generic/567 generic/568 generic/586 generic/590 generic/591 generic/596

[Need to recheck the list below to Windows]
generic/023 generic/075 generic/088 generic/089 generic/091 generic/126 generic/127 generic/184 generic/192 generic/245 generic/263 generic/313
# To current Windows (using CIFS dialect instead of SMB3):  
# To current Samba (CIFS with POSIX extensions). Detailed data on which tests run and which fail:
#:Ran:
#::cifs/001 generic/001 generic/002 generic/005 generic/006 generic/007 generic/010 generic/011 generic/013 generic/014 generic/023 generic/024 generic/028 generic/029 generic/030 generic/035 generic/036 generic/053 generic/067 generic/069 generic/074 generic/075 generic/080 generic/084 generic/087 generic/088 generic/089 generic/091 generic/095 generic/098 generic/100 generic/105 generic/109 generic/112 generic/113 generic/120 generic/123 generic/124 generic/125 generic/126 generic/127 generic/128 generic/129 generic/130 generic/131 generic/132 generic/133 generic/135 generic/141 generic/169 generic/184 generic/192 generic/193 generic/198 generic/207 generic/208 generic/209 generic/210 generic/211 generic/212 generic/215 generic/221 generic/236 generic/237 generic/239 generic/241 generic/245 generic/246 generic/247 generic/248 generic/249 generic/257 generic/258 generic/263 generic/285 generic/286 generic/294 generic/306 generic/307 generic/308 generic/309 generic/310 generic/313 generic/314 generic/317 generic/318 generic/319 generic/323
#:Not run:
#::generic/003 generic/004 generic/008 generic/009 generic/012 generic/015 generic/016 generic/017 generic/018 generic/019 generic/020 generic/021 generic/022 generic/025 generic/026 generic/027 generic/031 generic/032 generic/033 generic/034 generic/037 generic/038 generic/039 generic/040 generic/041 generic/042 generic/043 generic/044 generic/045 generic/046 generic/047 generic/048 generic/049 generic/050 generic/051 generic/052 generic/054 generic/055 generic/056 generic/057 generic/058 generic/059 generic/060 generic/061 generic/062 generic/063 generic/064 generic/065 generic/066 generic/068 generic/070 generic/071 generic/072 generic/073 generic/076 generic/077 generic/078 generic/079 generic/081 generic/082 generic/083 generic/085 generic/086 generic/090 generic/092 generic/093 generic/094 generic/096 generic/097 generic/099 generic/101 generic/102 generic/103 generic/104 generic/106 generic/107 generic/108 generic/110 generic/111 generic/114 generic/115 generic/116 generic/117 generic/118 generic/119 generic/121 generic/122 generic/134 generic/136 generic/137 generic/138 generic/139 generic/140 generic/142 generic/143 generic/144 generic/145 generic/146 generic/147 generic/148 generic/149 generic/150 generic/151 generic/152 generic/153 generic/154 generic/155 generic/156 generic/157 generic/158 generic/159 generic/160 generic/161 generic/162 generic/163 generic/164 generic/165 generic/166 generic/167 generic/168 generic/170 generic/171 generic/172 generic/173 generic/174 generic/175 generic/176 generic/177 generic/178 generic/179 generic/180 generic/181 generic/182 generic/183 generic/185 generic/186 generic/187 generic/188 generic/189 generic/190 generic/191 generic/194 generic/195 generic/196 generic/197 generic/199 generic/200 generic/201 generic/202 generic/203 generic/204 generic/205 generic/206 generic/213 generic/214 generic/216 generic/217 generic/218 generic/219 generic/220 generic/222 generic/223 generic/224 generic/225 generic/226 generic/227 generic/228 generic/229 generic/230 generic/231 generic/232 generic/233 generic/234 generic/235 generic/238 generic/240 generic/242 generic/243 generic/244 generic/250 generic/251 generic/252 generic/253 generic/254 generic/255 generic/256 generic/259 generic/260 generic/261 generic/262 generic/264 generic/265 generic/266 generic/267 generic/268 generic/269 generic/270 generic/271 generic/272 generic/273 generic/274 generic/275 generic/276 generic/277 generic/278 generic/279 generic/280 generic/281 generic/282 generic/283 generic/284 generic/287 generic/288 generic/289 generic/290 generic/291 generic/292 generic/293 generic/295 generic/296 generic/297 generic/298 generic/299 generic/300 generic/301 generic/302 generic/303 generic/304 generic/305 generic/311 generic/312 generic/315 generic/316 generic/320 generic/321 generic/322 generic/324 generic/325 generic/326 generic/327 generic/328 generic/329 generic/330 generic/331 generic/332 generic/333 generic/334 generic/335 generic/336 generic/337 shared/001 shared/002 shared/003 shared/006 shared/032 shared/051 shared/272 shared/289 shared/298
#:Failures: 
#::cifs/001 generic/035 generic/087 generic/088 generic/089 generic/105 generic/120 generic/126 generic/184 generic/193 generic/237 generic/258 generic/294 generic/306 generic/313 generic/314 generic/317 generic/318 generic/319
#:Failed 19 of 88 tests
# To current Samba (CIFS, POSIX extensions disabled).  Detailed data on which tests run and which fail. Running as nonroot user on mount<br />
#:Ran:
#::generic/001 generic/002 generic/005 generic/006 generic/007 generic/011 generic/013 generic/014 generic/020 generic/023 generic/024 generic/028 generic/070 generic/074 generic/075 generic/088 generic/089 generic/091 generic/112 generic/113 generic/123 generic/125 generic/126 generic/127 generic/133 generic/184 generic/192 generic/193 generic/198 generic/207 generic/208 generic/209 generic/210 generic/211 generic/212 generic/215 generic/221 generic/236 generic/239 generic/245 generic/246 generic/247 generic/248 generic/249 generic/257 generic/258 generic/263 generic/285 generic/286 generic/308 generic/309 generic/310 generic/313 generic/314 generic/323 generic/324 generic/325 shared/006 shared/032 shared/051 shared/272 shared/289 shared/298
#:Not run: 
#::generic/003 generic/004 generic/008 generic/009 generic/010 generic/012 generic/015 generic/016 generic/017 generic/018 generic/019 generic/021 generic/022 generic/025 generic/026 generic/027 generic/053 generic/062 generic/068 generic/069 generic/076 generic/077 generic/079 generic/083 generic/093 generic/097 generic/099 generic/100 generic/105 generic/117 generic/120 generic/124 generic/128 generic/129 generic/130 generic/131 generic/132 generic/135 generic/141 generic/169 generic/204 generic/213 generic/214 generic/219 generic/223 generic/224 generic/225 generic/226 generic/228 generic/230 generic/231 generic/232 generic/233 generic/234 generic/235 generic/237 generic/240 generic/241 generic/251 generic/255 generic/256 generic/260 generic/269 generic/270 generic/273 generic/274 generic/275 generic/277 generic/280 generic/288 generic/294 generic/299 generic/300 generic/306 generic/307 generic/311 generic/312 generic/315 generic/316 generic/317 generic/318 generic/319 generic/320 generic/321 generic/322<br />
#:Failures:
#::generic/088 generic/089 generic/123 generic/126 generic/184 generic/192 generic/193 generic/209 generic/215 generic/236 generic/258 generic/285 generic/286 generic/308 generic/309 generic/310 generic/313 generic/314 generic/323 generic/324 generic/325 shared/006 shared/032 shared/051 shared/272 shared/289 shared/298
#:Failed 27 of 63 tests

===============================
test cases that need new cifs features to run
===============================

------------------------

Various testcases, including 237, 314, shared051 would require posix acl support (which we don't emulate).
Adding quota support would be required for test 270.  Adding "cifsacl" mount option for SMB3 would presumably be required to pass the permissions test cases.
===============================
test cases that need xfstest itself to be updated to run over cifs/smb3
===============================

------------------------

Includes any with SCRATCH_DEV requirement (patches to xfstest exist to get these to run over a network file system)