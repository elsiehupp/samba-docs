Samba4/for IPA to AD MIT trust
    <namespace>0</namespace>
<last_edited>2009-02-15T23:08:21Z</last_edited>
<last_editor>Abartlet</last_editor>

==============================

IT Trusts design
===============================

An implementation of the requirements for an `Samba4/for_IPA_to_AD_trust|IPA to AD trust`

Components
------------------------

===============================
KDC
===============================

------------------------

The KDC will need to be modified at least to give referrals to the AD domain for user and machine requests.

===============================
LDAP server
===============================

------------------------

Operating as current FreeIPA versions do.

===============================
IPA servers
===============================

------------------------

Servers will use SSSD to lookup user identity information in AD, via a RPC proxy at the IPA server

===============================
IPA clients
===============================

------------------------

Clients will need a Kerberos library capable of following referrals to find the AD servers.

The SSSD will need to lookup user identity information in AD, via a RPC proxy at the IPA server

===============================
Windows server sync
===============================

------------------------

Something like AD-sync will need to operate, to keep shadow copies of all IPA users in AD.  However, AD users will not need to be in IPA (this may lead to all users being in AD at some sites)

Advantages
------------------------

Much less work than implementing Forest trusts

Disadvantages
------------------------

Little change from the current situation with AD-sync, as all users in FreeIPA must be copied into AD (except for their passwords), so that AD can create a PAC for these users (without a PAC, windows clients and servers do not know what groups a user is in, and will reject the Kerberos ticket).

This copy of the user would also have it's own principal, which can also cause confusion, as the user now exists twice.  IPA clients reading both AD and IPA directories would need to filter out these duplicate users.