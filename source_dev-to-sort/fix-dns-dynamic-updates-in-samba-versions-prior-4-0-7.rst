Fix DNS dynamic updates in Samba versions prior 4.0.7
    <namespace>0</namespace>
<last_edited>2015-01-03T17:56:38Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

Samba versions prior 4.0.7 are affected by [https://bugzilla.samba.org/show_bug.cgi?id=9559 Bug #9559]. If you installation is running an affected version or you have upgraded from one, you may have problems with dynamic DNS updates with Windows XP sp3 and Windows 7. To fix the problem you need to follow this steps:

* Update Samba to a version later than 4.0.7

* kinit to avoid samba-tool asking for a password

 # kinit administrator

* Search for all the DNS entries that contain the broken records using:

 # samba-tool dns query SERVER DOMAIN @ ALL

* You´ll find entries like:

 Name=WORKSTATION, Records=0, Children=0

* For every entry found like the one above you´ll need to issue the following commands

 # samba-tool dns add SERVER DOMAIN WORKSTATION A IP -k yes
 # samba-tool dns delete SERVER DOMAIN WORKSTATION A IP -k yes

* On windows workstations you can run 

 > ipconfig /registerdns

* And dynamic updates should work ok