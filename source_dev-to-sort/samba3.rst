Samba3
    <namespace>0</namespace>
<last_edited>2020-02-29T14:01:00Z</last_edited>
<last_editor>Hortimech</last_editor>

Samba 3 is now EOL.

The last version was 3.6.25 and this was declared end of life on the 4th of March 2015.