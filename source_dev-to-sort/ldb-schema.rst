Samba4/LDB/Schema
    <namespace>0</namespace>
<last_edited>2009-10-13T20:46:55Z</last_edited>
<last_editor>Edewata</last_editor>

=================
Structures
=================

ldb_schema_syntax 
------------------------

.. code-block::

    struct ldb_schema_syntax {
    const char *name;
    ldb_attr_handler_t ldif_read_fn;
    ldb_attr_handler_t ldif_write_fn;
    ldb_attr_handler_t canonicalise_fn;
    ldb_attr_comparison_t comparison_fn;
};

Attribute handler structure.

* name: The attribute name
* ldif_read_fn: convert from ldif to binary format
* ldif_write_fn: convert from binary to ldif format
* canonicalise_fn: canonicalise a value, for use by indexing and dn construction
* comparison_fn: compare two values

ldb_schema_attribute 
------------------------

.. code-block::

    struct ldb_schema_attribute {
    const char *name;
    unsigned flags;
    const struct ldb_schema_syntax *syntax;
};

=================
Operations
=================

ldb_schema_attribute_by_name() 
------------------------

 const struct ldb_schema_attribute *ldb_schema_attribute_by_name(struct ldb_context *ldb,
     const char *name);