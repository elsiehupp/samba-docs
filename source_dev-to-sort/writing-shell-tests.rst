Writing Shell Tests
    <namespace>0</namespace>
<last_edited>2017-08-02T06:30:41Z</last_edited>
<last_editor>Abartlet</last_editor>

==============================

riting shell tests
===============================

Avoid writing new shell-based tests
------------------------

New tests should be written in `Writing_Python_Tests|Python`.

This particularly includes:
* `Writing_Python_Tests#Testing_Samba_binaries|Black-box tests of Samba binaries` using **samba.tests.BlackboxTestCase**
* `Writing_Python_Tests#Testing_samba-tool|tests of **samba-tool**` using **samba.tests.samba_tool.base.SambaToolCmdTest**.

Avoid bashisms
------------------------

On many systems ([https://wiki.ubuntu.com/DashAsBinSh Debian and Ubuntu in particular])
 /bin/sh
is actually **dash** not **[https://www.gnu.org/software/bash/ Bash]**.  Therefore use only [http://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_title.html POSIX sh] and avoid Bash specific syntax.

Use helper functions
------------------------

If your test is in testprogs/blackbox you can use:
 . `dirname $0`/subunit.sh
 . `dirname $0`/common_test_fns.inc

otherwise you will need to use a ../ path to find these.

===============================
testit
===============================

------------------------

testit is the primary test command, testing one particular command for success.

 testit "change dc password" $samba4srcdir/scripting/devel/chgtdcpass -s $PROVDIR/etc/smb.conf || failed=`expr $failed + 1`

exit code
------------------------

The script should exit with the number of failures

 exit $failed