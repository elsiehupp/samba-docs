Samba4/LDB/Message
    <namespace>0</namespace>
<last_edited>2009-11-11T22:25:52Z</last_edited>
<last_editor>Edewata</last_editor>

=================
Structures
=================

ldb_val 
------------------------

.. code-block::

    struct ldb_val {
    uint8_t *data; /*!< result data */
    size_t length; /*!< length of data */
};

ldb_message_element 
------------------------

.. code-block::

    struct ldb_message_element {
    unsigned int flags;
    const char *name;
    unsigned int num_values;
    struct ldb_val *values;
};

Results are given back as arrays of ldb_message_element.

ldb_message 
------------------------

.. code-block::

    struct ldb_message {
    struct ldb_dn *dn;
    unsigned int num_elements;
    struct ldb_message_element *elements;
};

An ldb_message represents all or part of a record. It can contain an arbitrary number of elements.

=================
Methods
=================

ldb_msg_new() 
------------------------

.. code-block::

    struct ldb_message *ldb_msg_new(TALLOC_CTX *mem_ctx);

Create an empty message

Parameters:
* mem_ctx: the memory context to create in. You can pass NULL to get the top level context, however the ldb context (from ldb_init()) may be a better choice

ldb_msg_find_element() 
------------------------

.. code-block::

    struct ldb_message_element *ldb_msg_find_element(const struct ldb_message *msg, 
						 const char *attr_name);

Find an element within an message

ldb_val_equal_exact() 
------------------------

.. code-block::

    int ldb_val_equal_exact(const struct ldb_val *v1, const struct ldb_val *v2);

Compare two ldb_val values

Parameters:
* v1: first ldb_val structure to be tested
* v2: second ldb_val structure to be tested

Returns: 1 for a match, 0 if there is any difference

ldb_msg_find_val() 
------------------------

.. code-block::

    struct ldb_val *ldb_msg_find_val(const struct ldb_message_element *el, 
				 struct ldb_val *val);

Find a value within an ldb_message_element.

Parameters:
* el: the element to search
* val: the value to search for

Note: This search is case sensitive

ldb_msg_add_empty() 
------------------------

.. code-block::

    int ldb_msg_add_empty(struct ldb_message *msg,
		const char *attr_name,
		int flags,
		struct ldb_message_element **return_el);

Add a new empty element to a ldb_message.

ldb_msg_add() 
------------------------

.. code-block::

    int ldb_msg_add(struct ldb_message *msg, 
		const struct ldb_message_element *el, 
		int flags);

ldb_msg_add_value() 
------------------------

.. code-block::

    int ldb_msg_add_value(struct ldb_message *msg, 
		const char *attr_name,
		const struct ldb_val *val,
		struct ldb_message_element **return_el);

ldb_msg_add_steal_value() 
------------------------

.. code-block::

    int ldb_msg_add_steal_value(struct ldb_message *msg, 
		      const char *attr_name,
		      struct ldb_val *val);

ldb_msg_add_steal_string() 
------------------------

.. code-block::

    int ldb_msg_add_steal_string(struct ldb_message *msg, 
			     const char *attr_name, char *str);

ldb_msg_add_string() 
------------------------

.. code-block::

    int ldb_msg_add_string(struct ldb_message *msg, 
		       const char *attr_name, const char *str);

ldb_msg_add_fmt() 
------------------------

.. code-block::

    int ldb_msg_add_fmt(struct ldb_message *msg, 
		    const char *attr_name, const char *fmt, ...) PRINTF_ATTRIBUTE(3,4);

ldb_msg_element_compare() 
------------------------

.. code-block::

    int ldb_msg_element_compare(struct ldb_message_element *el1, 
			    struct ldb_message_element *el2);

ldb_msg_element_compare_name() 
------------------------

.. code-block::

    int ldb_msg_element_compare_name(struct ldb_message_element *el1, 
				 struct ldb_message_element *el2);

ldb_msg_find_ldb_val() 
------------------------

.. code-block::

    const struct ldb_val *ldb_msg_find_ldb_val(const struct ldb_message *msg, const char *attr_name);

ldb_msg_find_attr_as_int() 
------------------------

.. code-block::

    int ldb_msg_find_attr_as_int(const struct ldb_message *msg, 
			     const char *attr_name,
			     int default_value);

ldb_msg_find_attr_as_uint() 
------------------------

.. code-block::

    unsigned int ldb_msg_find_attr_as_uint(const struct ldb_message *msg, 
				       const char *attr_name,
				       unsigned int default_value);

ldb_msg_find_attr_as_int64() 
------------------------

.. code-block::

    int64_t ldb_msg_find_attr_as_int64(const struct ldb_message *msg, 
				   const char *attr_name,
				   int64_t default_value);

ldb_msg_find_attr_as_uint64() 
------------------------

.. code-block::

    uint64_t ldb_msg_find_attr_as_uint64(const struct ldb_message *msg, 
				     const char *attr_name,
				     uint64_t default_value);

ldb_msg_find_attr_as_double() 
------------------------

.. code-block::

    double ldb_msg_find_attr_as_double(const struct ldb_message *msg, 
				   const char *attr_name,
				   double default_value);

ldb_msg_find_attr_as_bool() 
------------------------

.. code-block::

    int ldb_msg_find_attr_as_bool(const struct ldb_message *msg, 
			      const char *attr_name,
			      int default_value);

ldb_msg_find_attr_as_string() 
------------------------

.. code-block::

    const char *ldb_msg_find_attr_as_string(const struct ldb_message *msg, 
					const char *attr_name,
					const char *default_value);

ldb_msg_find_attr_as_dn() 
------------------------

.. code-block::

    struct ldb_dn *ldb_msg_find_attr_as_dn(struct ldb_context *ldb,
				       TALLOC_CTX *mem_ctx,
				       const struct ldb_message *msg,
				       const char *attr_name);

ldb_msg_sort_elements() 
------------------------

.. code-block::

    void ldb_msg_sort_elements(struct ldb_message *msg);

ldb_msg_copy_shallow() 
------------------------

.. code-block::

    struct ldb_message *ldb_msg_copy_shallow(TALLOC_CTX *mem_ctx, 
					 const struct ldb_message *msg);

ldb_msg_copy() 
------------------------

.. code-block::

    struct ldb_message *ldb_msg_copy(TALLOC_CTX *mem_ctx, 
				 const struct ldb_message *msg);

ldb_msg_canonicalize() 
------------------------

.. code-block::

    struct ldb_message *ldb_msg_canonicalize(struct ldb_context *ldb, 
					 const struct ldb_message *msg);

ldb_msg_diff() 
------------------------

.. code-block::

    struct ldb_message *ldb_msg_diff(struct ldb_context *ldb, 
				 struct ldb_message *msg1,
				 struct ldb_message *msg2);