Samba4/LDB/Filter
    <namespace>0</namespace>
<last_edited>2010-01-19T17:50:32Z</last_edited>
<last_editor>Edewata</last_editor>

=================
Enumerations
=================

.. code-block::

    enum ldb_parse_op { LDB_OP_AND=1, LDB_OP_OR=2, LDB_OP_NOT=3,
		    LDB_OP_EQUALITY=4, LDB_OP_SUBSTRING=5,
		    LDB_OP_GREATER=6, LDB_OP_LESS=7, LDB_OP_PRESENT=8,
		    LDB_OP_APPROX=9, LDB_OP_EXTENDED=10 };

=================
Structures
=================

ldb_parse_tree 
------------------------

.. code-block::

    struct ldb_parse_tree {
    enum ldb_parse_op operation;
    union {
        struct {
            struct ldb_parse_tree *child;
        } isnot;
        struct {
            const char *attr;
            struct ldb_val value;
        } equality;
        struct {
            const char *attr;
            int start_with_wildcard;
            int end_with_wildcard;
            struct ldb_val **chunks;
        } substring;
        struct {
            const char *attr;
        } present;
        struct {
            const char *attr;
            struct ldb_val value;
        } comparison;
        struct {
            const char *attr;
            int dnAttributes;
            char *rule_id;
            struct ldb_val value;
        } extended;
        struct {
            unsigned int num_elements;
            struct ldb_parse_tree **elements;
        } list;
    } u;
};