A Good Git Commit Message
    <namespace>0</namespace>
<last_edited>2020-12-22T08:50:03Z</last_edited>
<last_editor>Kseeger</last_editor>


Good commit messages are very important. Commits should always be separated in meaningful pieces.
Please make sure that your commit messages include at least the following information:

* A **short** summary of the fix/feature including the Samba version and component followed by a blank line.
* A more verbose description.
* A bug number if available (required for `Samba_Release_Planning#Release_Branch_Checkin_Procedure|backports to a released branch`, so please file a bug first)
* The description for the release notes starting with "RN: ". Please make sure that **administrators** understand the issue.
* Your `CodeReview#commit_message_tags|Signed-off-by tag` per the [https://www.samba.org/samba/devel/copyright-policy.html Samba copyright policy]

Example:

 s3:smbd: Add new funky shiny and groovy feature

 The funky shiny and groovy feature has been added to
 be able to do whatever you like. A typical usecase is the
 following.

 BUG: https://bugzilla.samba.org/show_bug.cgi?id=1
 RN: Fix problem xyz.

 Signed-off-by: Author <author@example.org>