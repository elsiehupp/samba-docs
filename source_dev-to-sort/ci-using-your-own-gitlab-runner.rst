CI using Your own gitlab runner
    <namespace>0</namespace>
<last_edited>2018-07-03T09:42:12Z</last_edited>
<last_editor>NoelPower</last_editor>

Register your development machine as a runner for your own gitlab project
------------------------

To increase speed of your CI runs you can run your own CI on your own machine for your gitlab samba project clone 
You can register your own development machine as a runner for your pipelines on your own clone of the samba gitlab repo.

These instructions are for SUSE so you will need to adjust the commands (e.g. apt-get instead of zypper etc.) as appropriate for your linux distro.

* Install docker

.. code-block::

     zypper in docker

IMPORTANT you need to ensure the device mapper storage driver is enabled, to do this you need to edit /etc/docker/daemon.json. If it does not yet exist, create it and add

.. code-block::

    {
    storage-driver": "devicemapper"
}

* Install gitlab runner

.. code-block::

     sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
 sudo chmod +x /usr/local/bin/gitlab-runner

* Optionally, you can install and run as a service

.. code-block::

    sudo useradd --system --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

gitlab-runner will run as root, but will execute jobs as user specified by the install command. You may need to add this user to the 'docker' group. Easiest way to test if docker is working is to issue 
'docker info' as the gitlab-runner 'user'

* Register your runner

.. code-block::

    # gitlab-runner register
Running in system-mode.                            

Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/)::
https://gitlab.com

Please enter the gitlab-ci token for this runner:
<you can find your token in your project's Settings ==> CI/CD ==> Runners settings>

Please enter the gitlab-ci description for this runner:
[hostname]: <your hostname>

Please enter the gitlab-ci tags for this runner (comma separated):

Whether to run untagged builds [true/false]:
[false]: ==> false

Whether to lock the Runner to current project [true/false]:
[true]: true

Registering runner... succeeded                     runner=M6jmUiFA
Please enter the executor: ssh, virtualbox, docker-ssh+machine, kubernetes, docker-ssh, parallels, shell, docker, docker+machine::
docker, private, shared

Please enter the default Docker image (e.g. ruby:::
registry.gitlab.com/samba-team/samba:

Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!

By default iirc gitlab-runner will just execute one job per pipeline at a time, to increase that you will need to edit /etc/gitlab-runner/config.toml and change the 'concurrent' value to something your machine can handle.

To manage your runner (and CI) you need to refer to the gitlab-runner help, additional options regarding CI and managing your runner specifically for your project can be accessed from your project settings available from the sidebar e.g. https://gitlab.com/{$USER}/samba/settings/ci_cd

Note: you will need to change some defaults, from your project settings (navigate Settings | CI/CD from the sidebar), expand the 'General Pipeline settings' and change

.. code-block::

       Timeout: 10h

   Custom CI config path: .gitlab-ci-private.yml

If you don't change the custom CI config path then by default the CI will run a reduced set of tests.

Credit for most the content above should go to Samuel Cabrero