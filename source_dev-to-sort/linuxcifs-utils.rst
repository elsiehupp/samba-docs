LinuxCIFS utils
    <namespace>0</namespace>
<last_edited>2021-04-13T00:11:37Z</last_edited>
<last_editor>Pshilovsky</last_editor>

=================
Description
=================

The in-kernel CIFS filesystem is generally the preferred method for mounting SMB/CIFS shares on Linux.

The in-kernel CIFS filesystem relies on a set of user-space tools. That package of tools is called **cifs-utils**. Although not really part of Samba proper, these tools were originally part of the Samba package. For several reasons, shipping these tools as part of Samba was problematic and it was deemed better to split them off into their own package.

See `LinuxCIFS` for description of the kernel client.

=================
News
=================
* April, 2021: Release 6.13
** CVE-2021-20208: cifs.upcall kerberos auth leak in container
** https://lists.samba.org/archive/samba-technical/2021-April/136467.html
* December, 2020: Release 6.12
** get/setcifsacl tools are improved to support changing owner, group and SACLs
** mount.cifs is enhanced to use SUDO_UID env variable for cruid
** smbinfo is re-written in Python language
** https://lists.samba.org/archive/samba-technical/2020-December/136156.html
* September, 2020: Release 6.11
** CVE-2020-14342: mount.cifs: fix shell command injection
** https://lists.samba.org/archive/samba-technical/2020-September/135747.html
* December 16, 2019: Release 6.10
** smb3 alias/fstype is added
** smb2-quota tool is added to display quota information
** smb2-secdesc UI tool to view security descriptors is added
** smbinfo is enhanced with capabilities to dump session keys and get/set compression of files
** smbinfo bash completion is supported
** getcifsacl tool is improved to support multiple files
** https://lists.samba.org/archive/samba-technical/2019-December/134662.html
* April 5, 2019: Release 6.9
** smbinfo utility is added to query various kinds of information from the server (objectId, snapshots, different FileInfo* classes and other metadata)
** server IP change is supported by expiring DNS key resolver entries
** get/setcifsacl tools are improved to handle unexpected behavior
** share snapshot are allowed to be specified by a GMT token or SMB 100-nanoseconds time
** various new mount option are documented: bsize, handletimeout, handlecache, rdma, max_credits and others
** https://lists.samba.org/archive/samba-technical/2019-April/133233.html
* March 9, 2018: Release 6.8
** man pages updates (auto-negotiate protocol version by default) and cleanups (moving to .rst format)
** setcifsacl: fix security descriptor buffer size mismatch
** cifscreds: fix a segfault for incorrect usage
** minor mount.cifs fixes
** https://lists.samba.org/archive/samba-technical/2018-March/126227.html
* March 2, 2017: Release 6.7
** fixes for regressions from cifs.upcall overhaul
** mount.cifs cleanups
** https://lists.samba.org/archive/samba-technical/2017-March/119036.html
* September 3, 2016: Release 6.6
** cleanup/overhaul of cifs.upcall krb5 credcache handling
** https://lists.samba.org/archive/samba-technical/2016-September/115974.html
* February 22, 2016: Release 6.5
** mount.cifs: ignore x- mount options
** minor build fixes
** minor manpage fix
** https://lists.samba.org/archive/samba-technical/2016-February/112372.html
* July 11, 2014: Release 6.4
** allow PAM directory to be configurable
** better determination of default keytab file
** better cifscreds error handling
** uppercase devicename when retrying mount
** https://lists.samba.org/archive/samba-technical/2014-July/101132.html
* January 9, 2014: Release 6.3
** fixes for various bugs turned up by Coverity
** clean unused cruft out of upcall binary
** add new pam_cifscreds PAM module for establishing NTLM creds on login
** https://lists.samba.org/archive/samba-technical/2014-January/097124.html
* October 4, 2013: Release 6.2
** setcifsacl can now work without a plugin
** systemd-ask-password is found using $PATH now
** cifs.upcall now works with KEYRING: credcaches
** https://lists.samba.org/archive/samba-technical/2013-October/095287.html
* July 2, 2013: Release 6.1
** minor bugfixes
** allow cifs.upcall to use dedicated keytab
** https://lists.samba.org/archive/samba-technical/2013-July/093601.html
* March 25, 2013: Release 6.0
** minor bugfixes and documentation updates
** support for NFS-style device names removed
** https://lists.samba.org/archive/samba-technical/2013-March/091169.html
* January 7, 2013: Release 5.9
** new plugin architecture for the ID mapping tools
** DOMAIN\username@password format for username= arguments is removed
** full RELRO (vs. partial) is now enabled on all binaries
** https://lists.samba.org/archive/samba-technical/2013-January/089761.html
* November 11, 2012: Release 5.8
** warning message added about deprecation of NFS-style mount syntax
** many fixes for cifs.idmap, getcifsacl and setcifsacl
** https://lists.samba.org/archive/samba-technical/2012-November/088686.html
* October 09, 2012: Release 5.7
** Fixes for mounting with '/' in usernames with sec=krb5
** Support for DIR: type krb5 ccaches with cifs.upcall
** support for "nofail" option in mount.cifs
** https://lists.samba.org/archive/samba-technical/2012-October/087501.html
* July 26, 2012: Release 5.6
** -Werror has been removed by default from CFLAGS
** PIE and RELRO are enabled by default at build time
** better integration with systemd by allowing the use of /bin/systemd-ask-password if available
** better checks and warnings from cifscreds when used in environments that do not have a session keyring
** https://lists.samba.org/archive/samba-technical/2012-July/085743.html
* May 30, 2012: Release 5.5
** a bunch of fixes for compile time warnings and build breaks
** some fixes in the libcap capabilities dropping code
** remove unneeded mount.smb2 multicall code and other prep work for smb2 support
** manpage updates for kernel-level behavior changes
** https://lists.samba.org/archive/samba-technical/2012-May/084102.html
* April 18, 2012: Release 5.4
** the "rootsbindir" can now be specified at configure time
** mount.cifs now supports the -s option by passing "sloppy" to the kernel in the options string
** cifs.upcall now properly respects the domain_realm section in krb5.conf
** unprivileged users can no longer mount onto dirs into which they can't chdir (fixes CVE-2012-1586)
** http://article.gmane.org/gmane.linux.kernel.cifs/5912
* January 28, 2012: Release 5.3
** admins can now tell cifs.upcall to use an alternate krb5.conf file
** on remount, mount.cifs no longer adds a duplicate mtab entry
** the cifscreds utility has seen a major overhaul to allow for multiuser mounts without krb5 auth
** https://lists.samba.org/archive/samba-technical/2012-January/081381.html
* December 09, 2011: Release 5.2
** cifs.idmap can now map uid/gid to SID in addition to the other way around
** getcifsacl/setcifsacl are now installed by default in /usr/bin instead of /usr/sbin. The manpages are now in section 1.
** cifs.upcall has a new scheme for picking the SPN on krb5 mounts. The hostname is now always lowercased. If we fail to get a ticket using an unqualified name, it now attempts to guess the domain name.
** A lot of manpage updates, additions and corrections
* September 23, 2011: Release 5.1
** fix for a minor security issue that can corrupt the mtab
** new getcifsacl/setcifsacl tools that allow you to fetch and set raw Windows ACLs via an xattr.
** a lot of manpage patches
* June 1, 2011: Release 5.0
**mount.cifs always uses the original device string to ensure that umounts by unprivileged users are not problematic
**there is a new cifs.idmap program for handling idmapping upcalls
** a lot of manpage patches
* March 4, 2011: Release 4.9
** Some distros (namely Fedora) are moving to having /etc/mtab be a symlink to /proc/mounts. We automatically skip trying to alter the mtab if it's a symlink. 
** fix for a bug that could prevent root from mounting onto a directory to which he did not have explicit execute permission.
** fix for a bug that caused the mount helper to pass in a corrupt address when someone specified an IPv6 address with a scopeid.
** mount.cifs bugfix for an uninitialized variable that could cause a segfault
* January 21, 2011: Release 4.8.1
* January 15, 2011: Release 4.8
* October 19, 2010: Release 4.7
* July 30, 2010: Release 4.6
* May 21, 2010: Release 4.5
* April 28, 2010: Release 4.4
* April 9, 2010: Release 4.3
* April 2, 2010: Release 4.2
* March 23, 2010: Release 4.1
* March 3, 2010: Release 4.0 -- first official cifs-utils release
* February 26, 2010: Release 4.0rc1
* February 14, 2010: The git repo has moved to a new location. See the `#Development|Development` section below
* February 9, 2010: Release 4.0-alpha1. In order to smooth the transition from shipping these tools as part of samba, the first release will be 4.0.

=================
Download
=================

A historical set of cifs-utils releases is available in the [https://download.samba.org/pub/linux-cifs/cifs-utils releases directory].

=================
Documentation
=================

* [http://pserver.samba.org/samba/ftp/cifs-cvs/linux-cifs-client-guide.pdf  Documentation (CIFS Users Guide)]
* <del>[http://www.snia.org/tech_activities/CIFS/CIFS-TR-1p00_FINAL.pdf SNIA CIFS Specification  ]</del> broken link

=================
Development
=================

The source code for cifs-utils is managed via git. An example checkout from the main git repo:

<pre>$ git clone git://git.samba.org/cifs-utils.git</pre>

gitweb access is also available [http://git.samba.org/?p=cifs-utils.git;a=summary here].

=================
Contact
=================

Questions, suggestions, concerns, and patches should be sent to [http://vger.kernel.org/vger-lists.html#linux-cifs linux-cifs@vger.kernel.org]. Security issues should be sent to [mailto:security@samba.org security@samba.org] to avoid immediate public disclosure.