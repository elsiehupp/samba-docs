Samba4/Andrew and Jelmers Fantasy Page/2010
    <namespace>0</namespace>
<last_edited>2012-04-01T19:37:22Z</last_edited>
<last_editor>Mdw</last_editor>

==============================

lans for fortnight ending 11th October 2010
===============================

* Work on supporting waf in Heimdal (Jelmer)
* Resolve circular links issues in waf for Debian package (Jelmer)
* Integrate land.py and autobuild.py (Jelmer)
Achievements
------------------------

* Removed circular dependencies (Jelmer)
===============================
Plans for fortnight ending 27th September 2010
===============================

Achievements
------------------------

*Merge some of Wilco's registry patches (Jelmer)
*Avoid aborting on environment setup problems without printing proper subunit errors (Jelmer)
*Release alpha13 (Jelmer)
*Several selftest improvements (running individual tests) (Jelmer)
*Uploaded newer versions to Sid (Jelmer)
*Support for aborting selftest early on failure (Tridge, Jelmer)

===============================
Plans for fortnight ending 19th July 2010
===============================

*Start hacking around [http://msdn.microsoft.com/en-us/library/cc224123%28v=PROT.13%29.aspx MS-BKRP] aka protected storage (Matthieu)
Achievements
------------------------

* Made a page about `UpgradeprovisionPlans | upgradeprovision plans` (Matthieu)
* Finished all the patches about upgradeprovision to keep up with the pace of current development (Matthieu)
* Add options to net to manipulate service principal names (a bit like addspn): net spn (Matthieu)

===============================
Plans for fortnight ending 5th July 2010
===============================

*Make upgradeprovision able to change synchronize msds-keyversionnumber (Matthieu)
*Start hacking around [http://msdn.microsoft.com/en-us/library/cc224123%28v=PROT.13%29.aspx MS-BKRP] aka protected storage (Matthieu)
*Review Andrews s3compat auth patches. (Jelmer)
*Infrastructure for testing net from within Python. (Jelmer)
Achievements
------------------------

*Make upgradeprovision is now able to change synchronize msds-keyversionnumber (Matthieu)
===============================
Plans for fortnight ending 19 June 2010
===============================

*Upload Debian packages (Jelmer)
*Fix build against system Heimdal (Jelmer)
Achievements
------------------------

*More Python cleanups (Jelmer)
*Fix Samba 4 build to install everything necessary for OpenChange again (Jelmer)
*Review Matthieu's patches (Jelmer)
*Push major update for upgradeprovision both in terms of update capacity and reliability (Matthieu)
*Use standard python logging infrastructure in Python code (Jelmer)
*Re-upload Debian packages based on waf build (Jelmer)
*Started `MergeRequests` page (Jelmer)

===============================
Plans for fortnight ending 5 June 2010
===============================

*Make the whole redesign of upgradeprovision go in Master (Matthieu)
*Develop more unit tests around upgradeprovision (Matthieu)
Achievements
------------------------

===============================
Plans for fortnight ending 22 May 2010
===============================

*Make client DFS referral support for sysvol go in Master (Matthieu)
*Develop torture test for DFS (at least for the domain referral part) (Matthieu)
*Make the whole redesign of upgradeprovision go in Master (Matthieu)
*Develop more unit tests around upgradeprovision (Matthieu)
*Merged some waf patches from Thomas. (Jelmer)
*Cherry-picked some of the patches I pair-programmed with Matthieu during SambaXP (Jelmer)
Achievements
------------------------

*DFS responses to client referral request are ok for Domain, DC and SYSVOL/NETLOGON (Matthieu)
*Torture tests ok (Matthieu)
*Update Debian packages for talloc, tdb, ldb and tevent. (Jelmer)

===============================
Plans for fortnight ending 24 Apr 2010
===============================

SambaXP!
Achievements
------------------------

*Played with the waf build (Jelmer)
*Pair-programmed on upgradeprovision unit tests (Jelmer, Matthieu)
*Initial Python bindings for libpolicy (Jelmer) 
===============================
Plans for fortnight ending 10 Apr 2010
===============================

Slacking
===============================
Plans for fortnight ending 27 Mar 2010
===============================

Slacking.
===============================
Plans for fortnight ending 13 Mar 2010
===============================

Slacking.
===============================
Plans for fortnight ending 27 Feb 2010
===============================

Achievements
------------------------

*More work to automate the correct setup of BIND for DNS (Andrew)
*Work with tridge to demonstrate 'waf' as a build system for Samba (Andrew)
===============================
Plans for fortnight ending 13 Feb 2010
===============================

Achievements
------------------------

*Work with Tridge on Samba HOWTO (Andrew)
*More work to automate the correct setup of BIND for DNS (Andrew)
*Improve Samba4 RPC proxy to handle a non-zero if_version (Andrew)
**This is needed to ensure we proxy the full if_version from an RPC bind to the endpoint mapper and subsequent bind on another RPC server. 
**Add testsuite to ensure the RPC proxy (rpc_server/remote) does not bitrot further. 

===============================
Plans for fortnight ending 30 January 2010
===============================

Achievements
------------------------

*Successful presentation at linux.conf.au Sysadmin mini-conf (Andrew)
*Holiday on South Island of NZ (Andrew)

===============================
Plans for fortnight ending 16 January 2010
===============================

Achievements
------------------------

*Samba4 Alpha 11 release (Andrew)
*DRS pair programming with Tridge (Andrew)
*Preparation for linux.conf.au SysAdmin mini-conf presentation

===============================
Plans for fortnight ending 2 January 2010
===============================

Achievements
------------------------

*DRS pair programming with Tridge (Andrew)
**Success with replicating with Windows 2008 again (mostly Tridge)
*Christmas Holidays (Andrew)

=================
Older entries
=================

For the 2009 entries, see `Samba4/Andrew_and_Jelmers_Fantasy_Page/2009`.

For the 2008 entries, see `Samba4/Andrew_and_Jelmers_Fantasy_Page/2008`.