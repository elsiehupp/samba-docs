Better Posix AD
    <namespace>0</namespace>
<last_edited>2018-01-27T01:49:10Z</last_edited>
<last_editor>Abartlet</last_editor>

==============================

ackground
===============================

When `Setting_up_Samba_as_an_Active_Directory_Domain_Controller|Setting up Samba as an AD DC` for Linux and other POSIX clients some things are not as simple as they could be. 

===============================
IDMAP
===============================

A user on a POSIX system needs a uid and gid value.  Many POSIX clients will use the uidNumber and gidNumber values, so we should have those filled in by default.

Possible solutions
------------------------

Samba should set a uidNumber and gidNumber on the directory entry when the user or group is created.  Additionally the schema should be extended to indicate that the uidNumber is actually IDMAP_BOTH, that is able to be expressed as a GID for ACLs.

This should be allocated via winbind, so that the administrator has control, however a new default should be created to use the SSSD idmap algorithm.

For RID based algorithms the base values for the domain should be stored the in trustPosixOffset value of the domain trust entry so that they run on each host autonomously.  For counter based allocation, the allocation of the uid/gid should be deferred to the PDC FSMS role holder (or similar).

===============================
SSH public keys
===============================

Schema for the ssh public key (sshPublicKey or nsSSHPublicKey) should be included in the default install.  However don't use the default schema as it has the SSH public key as MUST and it should be MAY. 

===============================
Sudo schema
===============================

Include the FreeIPA sudo schema.

===============================
SSSD HBAC access control
===============================

Include the schema so that SSSD can use HBAC rules stored in a Samba AD.

===============================
GPO
===============================

There is initial support for the GPO going in to Samba 4.8, but this work need to continue.

===============================
GUI and command line tools for POSIX
===============================

The `Installing_RSAT|RSAT` tools in modern windows versions have lost the NIS page so uidNumber is not easy to see and change.  We need web GUI tools where Samba with POSIX attributes is a first class citizen.

===============================
Client support for server POSIX smarts
===============================

sssd and winbindd need to be able to know, ideally by default, that the server is a POSIX enbled AD (eg server-side idmap, less compromises),