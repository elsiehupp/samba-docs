BuildsystemUseAndWhy
    <namespace>0</namespace>
<last_edited>2020-04-23T05:21:30Z</last_edited>
<last_editor>Abartlet</last_editor>

The waf build
-------------

Samba 4.0 and above ships with a new build system, based on waf.  A background to this build system can be found at `Waf`

This is the build system that is used when you run 

.. code::

    ./configure && make 

in the top level of a Samba 4.x release tree.

For the vast majority of our users, this is the build system you should use.  It supports parallel and incremental builds, and builds the whole Samba suite, the file server, the print server, the NT4 domain controller, winbind, the AD Domain Controller, the client libraries and the python libraries.  

A key feature for many of our distributors and OEMs is that despite the range of additional features, the resulting binaries and libraries are substantially smaller, because we use shared libraries extensively.

For distributions that have a requirement to use the system-supplied Kerberos library, we support building against a Heimdal or system MIT Kerberos library, provided the version is recent enough (otherwise we will use our internal version of Heimdal).  Please note that builds with MIT krb5 support will not have AD DC features.

By the time of the first release candidate, we will finish renaming the binaries that we ship so that where we provide a tool under a name that was used in Samba 3.x, it continues to behave in the same way it always has.  This will ensure that our change in build system does not impact on our user's ability to use Samba as they always have.

For developers, this build system backs a comprehensive 'make test', which provides `Code Coverage` of around 48% of our code by line:

This build system also implements important features such as ABI checking (which protects you as users from accidental changes to our published libraries), symbol versions and dependency checked incremental rebuilds after header-file changes.

The waf build also assists developers by providing fully-linked binaries that run from bin/ without needing to set LD_LIBRARY_PATH. 

For users who do not have python installed on their systems, we provide a install_with_python.sh script, which will install a local copy of python sufficient to run the build system, without impacting on the rest of the system.  

Within this requirement, we expect that this build will run on all our supported platforms, and will actively deal with any portability issues that users can bring to our attention. 

For all these reasons, we highly recommend this new build system to all our users, for whatever purpose you want to put Samba to.

Optional Libraries
------------------

To assist users and distributors to build Samba with the full feature set, by the first release candidate the build system will abort if our dependent libraries and their header files are not found on the target system.  This will mean for example, that xattr, acl and ldap headers must be installed for the default build to complete.  The configure system will check for these headers, and the error message will indicate:
* the required header and library
* the option (such as --without-acl-support) that can be specified to skip this requirement, and
* the feature (ACL support optional for file servers, mandatory for AD Domain Controller) being skipped. 

This will assist users and in particular distributors in building fully functional packages, while allowing those on systems truly without these facilities to continue to build Samba after careful consideration.

----

taken from 
 https://lists.samba.org/archive/samba-technical/2012-May/084080.html