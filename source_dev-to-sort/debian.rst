Samba4/Debian
    <namespace>0</namespace>
<last_edited>2018-03-15T13:18:02Z</last_edited>
<last_editor>Bjacke</last_editor>

Build a Debian .deb package from git.debian.org sources 
------------------------

The Debian packaging for Samba 4 is maintained in the git 
repository of the Debian Samba packaging team. 

See instructions at: https://salsa.debian.org/samba-team/samba/blob/master/debian/README.source.md