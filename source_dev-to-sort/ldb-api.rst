Samba4/LDB/API
    <namespace>0</namespace>
<last_edited>2009-09-24T17:19:36Z</last_edited>
<last_editor>Edewata</last_editor>

=================
Initialization
=================

ldb_global_init() 
------------------------

 int ldb_global_init(void);

Initialise ldbs' global information

This is required before any other LDB call

Return: 0 if initialisation succeeded, -1 otherwise

ldb_init() 
------------------------

 struct ldb_context *ldb_init(TALLOC_CTX *mem_ctx, struct tevent_context *ev_ctx);

Initialise an ldb context

This is required before any other LDB call.

Parameters:
* mem_ctx: pointer to a talloc memory context. Pass NULL if there is no suitable context available.

Return: pointer to ldb_context that should be free'd (using talloc_free()) at the end of the program.

=================
Connection
=================

ldb_connect() 
------------------------

 int ldb_connect(struct ldb_context *ldb, const char *url, unsigned int flags, const char *options[]);

Connect to a database.

This is typically called soon after ldb_init(), and is required prior to any search or database modification operations.

The URL can be one of the following forms:
* tdb://path
* ldapi://path
* ldap://host
* sqlite://path

Parameters:
* ldb: the context associated with the database (from ldb_init())
* url: the URL of the database to connect to, as noted above
* flags: a combination of LDB_FLG_* to modify the connection behaviour
* options: backend specific options - passed uninterpreted to the backend

Return: result code (LDB_SUCCESS on success, or a failure code)

It is an error to connect to a database that does not exist in readonly mode
(that is, with LDB_FLG_RDONLY). However in read-write mode, the database will be
created if it does not exist.

=================
Root DSE
=================

ldb_get_root_basedn() 
------------------------

 struct ldb_dn *ldb_get_root_basedn(struct ldb_context *ldb);

Return: an automatic basedn from the rootDomainNamingContext of the rootDSE.
This value have been set in an opaque pointer at connection time

ldb_get_config_basedn() 
------------------------

 struct ldb_dn *ldb_get_config_basedn(struct ldb_context *ldb);

Return: an automatic basedn from the configurationNamingContext of the rootDSE.
This value have been set in an opaque pointer at connection time

ldb_get_schema_basedn() 
------------------------

 struct ldb_dn *ldb_get_schema_basedn(struct ldb_context *ldb);

Return: an automatic basedn from the schemaNamingContext of the rootDSE.
This value have been set in an opaque pointer at connection time

ldb_get_default_basedn() 
------------------------

 struct ldb_dn *ldb_get_default_basedn(struct ldb_context *ldb);

Return: an automatic baseDN from the defaultNamingContext of the rootDSE. This value have been set in an opaque pointer at connection time.

=================
Attributes
=================

ldb_attr_cmp() 
------------------------

 #define ldb_attr_cmp(a, b) strcasecmp(a, b)

Compare two attributes

This function compares to attribute names. Note that this is a case-insensitive comparison.

Parameters:
* a: the first attribute name to compare
* b: the second attribute name to compare

Return 0 if the attribute names are the same, or only differ in case; non-zero if there are any differences

attribute names are restricted by rfc2251 so using strcasecmp and toupper here is ok. return 0 for match

ldb_attr_casefold() 
------------------------

 char *ldb_attr_casefold(TALLOC_CTX *mem_ctx, const char *s);

ldb_attr_dn() 
------------------------

 int ldb_attr_dn(const char *attr);

=================
LDAP Operations
=================

ldb_search() 
------------------------

 int ldb_search(struct ldb_context *ldb, TALLOC_CTX *mem_ctx,
     struct ldb_result **result, struct ldb_dn *base,
     enum ldb_scope scope, const char * const *attrs,
     const char *exp_fmt, ...) PRINTF_ATTRIBUTE(7,8);

Search the database

This function searches the database, and returns records that match an LDAP-like search expression

Parameters:
* ldb the: context associated with the database (from ldb_init())
* mem_ctx: the memory context to use for the request and the results
* result: the return result
* base: the Base Distinguished Name for the query (use ldb_dn_new() for an empty one)
* scope: the search scope for the query
* attrs: the search attributes for the query (pass NULL if none required)
* exp_fmt: the search expression to use for this query (printf like)

Return: result code (LDB_SUCCESS on success, or a failure code)

Note: Use talloc_free() to free the ldb_result returned.

ldb_add() 
------------------------

 int ldb_add(struct ldb_context *ldb, 
     const struct ldb_message *message);

Add a record to the database.

This function adds a record to the database. This function will fail if a record with the specified class and key already exists in the database. 

Parameters:
* ldb: the context associated with the database (from ldb_init())
* message: the message containing the record to add.

Return: result code (LDB_SUCCESS if the record was added, otherwise  a failure code)

ldb_modify() 
------------------------

 int ldb_modify(struct ldb_context *ldb, 
     const struct ldb_message *message);

Modify the specified attributes of a record

This function modifies a record that is in the database.

Parameters:
* ldb: the context associated with the database (from ldb_init())
* message: the message containing the changes required.

Return: result code (LDB_SUCCESS if the record was modified as requested, otherwise a failure code)

ldb_rename() 
------------------------

 int ldb_rename(struct ldb_context *ldb, struct ldb_dn *olddn, struct ldb_dn *newdn);

Rename a record in the database

This function renames a record in the database.

Parameters:
* ldb: the context associated with the database (from ldb_init())
* olddn: the DN for the record to be renamed.
* newdn: the new DN 

Return: result code (LDB_SUCCESS if the record was renamed as requested, otherwise a failure code)

ldb_delete() 
------------------------

 int ldb_delete(struct ldb_context *ldb, struct ldb_dn *dn);

Delete a record from the database

This function deletes a record from the database.

Parameters:
* ldb: the context associated with the database (from ldb_init())
* dn: the DN for the record to be deleted.

Return: result code (LDB_SUCCESS if the record was deleted, otherwise a failure code)