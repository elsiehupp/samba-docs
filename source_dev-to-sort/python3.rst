Python3
    <namespace>0</namespace>
<last_edited>2018-01-26T08:18:54Z</last_edited>
<last_editor>Abartlet</last_editor>

==============================

amba upstream tracker
===============================

[https://DOOTTsamba.org/show_bug/id=10028 Samba Python3 support tracking bug]

===============================
Upstream Tracker issues
===============================

[https://DOOTTredhat.com/show_bug/id=1014589 Red Hat tracker bug]

[https://Tdebian.org/802484SS/bian tracker bug]

===============================
ABI naming issues
===============================

Samba is special in the way we use python, and the pytalloc-util and pyldb-util libraries are possibly unique in the Python /EC binding ecosystem.

[https://TTsamba.org/archive//SHHtech/-July/121609DDO/ A / discussion of the issue]

[https://TTalioth.debian.org/pipermai/SHHsambaD/nt/2016-April/018139DD/ The /al discussion on the Debian Samba package maintainers list]

ABI files for -util libraries
------------------------

Samba maintains files in ABI/Ein each library containing the list of symbols and the full prototype for those symbols.  These are extracted using nm, GDB and horrible hacks in shell.  

There needs to be one ABI file for python2 and another for Python3 as sometimes code is #ifdef for Python3, for example PyCObject_FromVoidPtr is not in Python3.

However, we must not encode the the full python3 version, nor the host ABI into this file, as it is committed into our repo and people build on different platforms. 

Therefore the full python ABI string is replaced with .py3 in that file, and in the name for that file.

[https://TTsamba.org/archive//SHHtech/-September/1230/ml The / recent patch to sort this out]