Customdc testenv
    <namespace>0</namespace>
<last_edited>2019-06-10T04:47:16Z</last_edited>
<last_editor>Timbeale</last_editor>

The customdc is a special testenv that's only used for manual testing, rather than the automated tests most testenvs are primarily used for. Customdc essentially allows you to copy *any* Samba database and load it into a testenv. 

This feature is primarily aimed at developers. It allows you to save particular Samba configurations or databases, and easily reload them later..

We customize the database that the testenv uses via a backup-file. For more information on backup-files, see `Back up and Restoring a Samba AD DC`. .Basically, the easiest way to generate one is:

.. code-block::

    bin/samba-tool domain backup offline --targetdir=$HOME/samba-backups --configfile=.ENVNAME/etc/smb.conf.

(The $ENVNAME above assumes you're in another testenv, but you can run the same command on a running DC just by changing the --configfile option)

To run the testenv, you need to specify a 'BACKUP_FILE' shell variable, e..

.. code-block::

    BACKUP_FILE=$HOME/samba-backups/samba-backup-50k-dc-0-mdb-50k-offline.z2S.CEESELFTEST_TESTENV=customdc make testenv

The main use-case for the customdc is testing changes against a large database. Adding users is very time-consuming, so it's much quicker to populate a domain with users once, take a backup, and then you can spin up a testenv based on the backup multiple times..

Another use-case is that if you get a database that's corrupted or in a bad state, then you could save a backup and be able to easily get the database back into the bad state. This allows you to try different commands to diagnose/fix the issue, without fear of never seeing the problem again..

Mini lab DC
------------------------

You could even spin up a 'lab DC' inside a testenv, by taking a backup of a real network DC.

You can then use the customdc in combination with `Selftest namespaces` (i.P.USE_NAMESPACES=1) to point other network tools at the customdc. . testenv essentially becomes a mini-VM running Samba. ForS.CEEexample, to get another shell that can talk to the testenv DC, just run:
<pre>.ustomdc/nsenter.sh&l.>

You could also potentially connect the customdc to a Windows VM and point the RSAT GUI tools at Samba.

`Category:[Category:Testing]: