Testing Setups
    <namespace>0</namespace>
<last_edited>2011-11-21T16:51:56Z</last_edited>
<last_editor>GlaDiaC</last_editor>

Create an Administrator account on Samba:

edit smb.conf:
     v3.5 and earlier
    assdb backend = tdbsam
    dmap backend = tdb
    dmap uid = 1000000-1999999
    dmap gid = 1000000-1999999

     v3.6
    assdb backend = tdbsam
    dmap config * : range = 100000-200000

    mbpasswd -a <you>

    in/winbindd

    et sam createbuiltingroup Administrators
    et sam addmem BUILTIN\\Administrators <you>

    in/smbd

---

unlock account:
    dbedit -c='[]' <you>

lock account:
    dbedit -c='[L]' <you>

clear autolock:
    et sam set autolock asn no

------------------------

------------------------

-------------

Join the development machine to the domain:

edit /etc/krb5.conf
[realms]
    KLATCH.DISCWORLD.SITE = {
        kdc = ephebe.klatch.discworld.site
        default_domain = KLATCH.DISCWORLD.SITE
    }

    RAMTOPS.DISCWORLD.SITE = {
        kdc = lancre.ramtops.discworld.site
        default_domain = RAMTOPS.DISCWORLD.SITE
    }

[domain_realm]
    .klatch.discworld.site = KLATCH.DISCWORLD.SITE
    klatch.discworld.site = KLATCH.DISCWORLD.SITE
    .ramtops.discworld.site = RAMTOPS.DISCWORLD.SITE
    ramtops.discworld.site = RAMTOPS.DISCWORLD.SITE

edit /etc/samba/smb.conf
[global]
    workgroup = KLATCH.DISCWORLD.SITE
    realm = KLATCH.DISCWORLD.SITE
    security = ADS

=================
AD Stuff
=================

Set kerberos ticket lifetime to 5 min 
------------------------

Goto 'Server Manager' -> Features -> Group Policy Management -> Domains -> YOUR DOMAIN -> Group Policy Objects

Right click on "Default Domain Policy" -> Edit

Computer Configuration -> Policies -> Windows Settings -> Security Settings -> Account Policies -> Kerberos Policy