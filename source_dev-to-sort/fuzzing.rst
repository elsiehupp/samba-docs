Fuzzing
    <namespace>0</namespace>
<last_edited>2020-10-24T20:25:56Z</last_edited>
<last_editor>Abartlet</last_editor>

==============================

ss-fuzz reproduction steps
===============================

Samba is integrated with [https://google.github.io/oss-fuzz/ oss-fuzz].  To obtain access to the fuzzing results you must be a **member of the [https://samba.org/samba/team Samba team]** and listed in the [https://github.com/google/oss-fuzz/blob/master/projects/samba/project.yaml oss-fuzz **project.yml** file].  The fuzzing results are on [https://oss-fuzz.com the oss-fuzz ClusterFuzz web interface].  See the [https://google.github.io/oss-fuzz/getting-started/new-project-guide/#reviewing-results oss-fuzz new project guide on reviewing results] for more details. 

fuzz_ndr_X
------------------------

If the failure is in a tool named fuzz_ndr_* then it is possible to translate this failure into an ndrdump command.  The $PIPE below is eg drsuapi from fuzz_ndr_drsuapi_TYPE_OUT, the $FILE is the file provided by oss-fuzz.

 [https://git.samba.org/?p=samba.git;a=blob;f=lib/fuzzing/decode_ndr_X_crash lib/fuzzing/decode_ndr_X_crash] -p $PIPE $FILE

This may allow the failure to be debugged without needing a recompile to a fuzzing build and makes it easy to incorporate into the testsuite.

Other fuzzers (or where ndrdump does not reproduce)
------------------------

For other fuzzers, build Samba for fuzzing.  See [https://gitlab.com/samba-team/samba/blob/master/lib/fuzzing/README.md lib/fuzzing/README.md] for instructions on how to build Samba for fuzzing.

If neither AFL or hongfuzz is available locally, **you can still build using --enable-afl** and this will produce a normal binary.

Either way, the result of these builds are a series of fuzz_ binaries in bin/ that will take the failing input in stdin.

===============================
oss-fuzz public information
===============================

While newly discovered issues are embargoed, the following information is public:
* [https://oss-fuzz-build-logs.storage.googleapis.com/badges/samba.svg build status badge]
* [https://oss-fuzz-build-logs.storage.googleapis.com/index.html#samba current oss-fuzz Samba build logs]
* [https://bugs.chromium.org/p/oss-fuzz/issues/list?q=label:Proj-samba bugs past the auto-disclosure deadline]
* [https://oss-fuzz.com/coverage-report/job/libfuzzer_asan_samba/latest oss-fuzz code coverage metrics]

===============================
Developing new fuzzers
===============================

Please do help develop new fuzzers!  The Samba Team can collaborate with you in private in the early stages if your work is finding security-releavent issues easily before we merge the fuzzer for running by the public and on oss-fuzz.

See this `fuzzing/example|example of how to add a new fuzz target`.

===============================
Fixing fuzzing bugs
===============================

Determine security impact
------------------------

If there is security impact from the fuzzing result, please follow the `Samba Security Process`

Add test cases
------------------------

Remember to add a testcase to the appropriate part of samba if possible when fixing the bug.

===============================
ndrdump reproducable failures
===============================

------------------------

Currently failures that can be reproduced with ndrdump are being added to [https://git.samba.org/?p=samba.git;a=blob;f=python/samba/tests/blackbox/ndrdump.py python/samba/tests/blackbox/ndrdump.py]

===============================
Other failures
===============================

------------------------

Please find an appropriate part of the existing testsuite to extend, and build a framework if needed.  This may see excessive, but where one bug is found, others will follow and it makes it easier in the long run.

Credit
------------------------

Make sure to credit the tool, fuzz target and ideally the fuzz target author (particularly for novel or important findings).  For issues found by [https://google.github.io/oss-fuzz/architecture/ OSS-Fuzz] they ask to for 
 Credit to OSS-Fuzz
to be in the commit message.