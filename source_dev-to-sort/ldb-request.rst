Samba4/LDB/Request
    <namespace>0</namespace>
<last_edited>2010-02-02T17:45:03Z</last_edited>
<last_editor>Edewata</last_editor>

=================
Enumerations
=================

ldb_request_type 
------------------------

.. code-block::

    enum ldb_request_type {
    LDB_SEARCH,
    LDB_ADD,
    LDB_MODIFY,
    LDB_DELETE,
    LDB_RENAME,
    LDB_EXTENDED,
    LDB_REQ_REGISTER_CONTROL,
    LDB_REQ_REGISTER_PARTITION
};

ldb_scope 
------------------------

.. code-block::

    enum ldb_scope {
    LDB_SCOPE_DEFAULT=-1, 
    LDB_SCOPE_BASE=0, 
    LDB_SCOPE_ONELEVEL=1,
    LDB_SCOPE_SUBTREE=2};

=================
Structures
=================

ldb_search 
------------------------

.. code-block::

    struct ldb_search {
    struct ldb_dn *base;
    enum ldb_scope scope;
    struct ldb_parse_tree *tree;
    const char * const *attrs;
    struct ldb_result *res;
};

ldb_add 
------------------------

.. code-block::

    struct ldb_add {
    const struct ldb_message *message;
};

ldb_modify 
------------------------

.. code-block::

    struct ldb_modify {
    const struct ldb_message *message;
};

ldb_delete 
------------------------

.. code-block::

    struct ldb_delete {
    struct ldb_dn *dn;
};

ldb_rename 
------------------------

.. code-block::

    struct ldb_rename {
    struct ldb_dn *olddn;
    struct ldb_dn *newdn;
};

ldb_extended 
------------------------

.. code-block::

    struct ldb_extended {
    const char *oid;
    void *data;
};

ldb_register_control 
------------------------

.. code-block::

    struct ldb_register_control {
    const char *oid;
};

ldb_register_partition 
------------------------

.. code-block::

    struct ldb_register_partition {
    struct ldb_dn *dn;
};

ldb_request 
------------------------

.. code-block::

    struct ldb_request {

    enum ldb_request_type operation;

    union {
        struct ldb_search search;
        struct ldb_add    add;
        struct ldb_modify mod;
        struct ldb_delete del;
        struct ldb_rename rename;
        struct ldb_extended extended;
        struct ldb_register_control reg_control;
        struct ldb_register_partition reg_partition;
    } op;

    struct ldb_control **controls;

    void *context;
    ldb_request_callback_t callback;

    int timeout;
    time_t starttime;
    struct ldb_handle *handle;
};

=================
Operations
=================

ldb_request() 
------------------------

 int ldb_request(struct ldb_context *ldb, struct ldb_request *request);

ldb_request_done() 
------------------------

 int ldb_request_done(struct ldb_request *req, int status);

ldb_request_is_done() 
------------------------

 bool ldb_request_is_done(struct ldb_request *req);