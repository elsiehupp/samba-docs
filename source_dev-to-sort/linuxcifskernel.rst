LinuxCIFSKernel
    <namespace>0</namespace>
<last_edited>2021-05-25T22:57:53Z</last_edited>
<last_editor>Sfrench</last_editor>

=================
Linux Kernel SMB3/CIFS Client
=================
The Linux cifs kernel client has been included in the kernel since kernel version 2.5.42.  For improved security and performance, SMB3 is the now the default dialect (SMB3.1.1/SMB3.02/SMB3/SMB2.1 dialects are requested by default). CIFS protocol (and other old dialects) can be selected (by specifying "vers=1.0" or "vers=2.02" in the mount options for example). To require that mount fails unless the server supports the most secure dialect, SMB3.11, specify "vers=3.1.1".

If you want to try current SMB3 support on an older kernel, full backports of all SMB3/CIFS fixes to earlier kernel versions can be found at: https://github.com/smfrench/smb3-cifs-linux-stable-backports.git

Changes by release 
------------------------

5.13-rc kernel (module version 2.32 63 changesets so far) 
------------------------

Handle caching (deferred close) added, greatly improving performance of workloads like "open/write/close/open/read/close."
Improvements to directory caching.
Significant read ahead performance improvements (e.g. copying large file from server) with addition of new mount parameter "rasize=" to allow setting more I/O to be sent for readahead (allowing more parallelism on the wire). This is especially helpful when using multichannel or when there are longer latencies on the network or server disk.
Support for shutdown ioctl (e.g. "godown" utility).
Support for fcollapse, finsert support added.
Strongest encryption ("gcm256") now enabled by default (will be negotiated automatically if server requires it).
Debugging of encrypted wireshark traces improved (for gcm256 and also for multiuser mounts).
Mount error handling improved.

5.12 kernel (module version 2.31 51 changesets) 
------------------------

Add support for two new mount parmaters that allow controlling metadata cache timeouts differently for directories and files ("acdirmax" and "acregmax"), unlike "actimeo" which sets metadata cache timeouts for both. Key fixes for setting mode bits and ownership with using the "cifsacl" and "modefromsid" mount options. Add 3.1.1 dialect to list of dialects requested when specifying "vers=3" (ie version three or later dialect) on mount ("vers=3.0" or "vers=3.0.2" or "vers=3.1.1" can be specified on mount if a specific dialect is wanted, rather than generally wanting SMB3 or later dialects).  Support for mounting shares with space in the share name.  Important fix for reconnect to server when the server's ip address changing while we are mounted to it.  Add support for stronger encryption (AES-GCM-256).

5.11 kernel (module version 2.30 80 changesets) 
------------------------

Add support for the new mount API.  See https://www.kernel.org/doc/html/latest/filesystems/mount_api.html  This will allow improved error handling, as well as allowing remounting a share (without requiring unmount/mount) to change various mount parameters which is less disruptive.  Add support for the Witness protocol which allows server notification to the client on server changes (e.g. the server changing its IP address).  See https://www.youtube.com/watch?v=tShA4G2ecRw and MS-SWN for more information.  Add support for getting/setting auditing information from the ACL (the SACL).  The smbinfo utility has also been updated.

5.10 kernel (module version 2.29 43 changesets) 
------------------------

Add support for recognizing WSL reparse points for special file types. Fix for esize= mounts (encrypt offload) data corruption. Add support for returning owner information when mounted with SMB3.1.1 POSIX extensions. Fix use of idsfromsid mount option to Azure.

5.9 kernel (module version 2.28 30 changesets) 
------------------------

Fix idsfromsid mount option when creating directories.  Various DFS (global namespace) fixes.

5.8 kernel (module version 2.28 61 changesets) 
------------------------

Huge improvement in large i/o performance with multichannel.  Add new mount parm "nodelete"
Add support for "idsfromsid" on create and chown - which allows POSIX ownership (uid/gid) to be saved in the ACL (Owner SIDs).
Add support for query info using the new POSIX info level in the SMB3.1.1 POSIX Extensions (this was the last important missing piece for the experimental SMB3.1.1 POSIX extensions implementation in the Linux client). Sparse file fixes.  Improve read performance when using 64K pages.  "smbdirect" (SMB3 over RDMA) support configured on by default.

5.7 kernel (module version 2.26, 49 changesets) 
------------------------

Add support for POSIX readdir. Various improvements for setting mtime. RDMA (smbdirect) improvements. Performance improvement for signed connections (when multiple requests are sent at the same time).  Experimental support for swap over SMB3 mounts added.

5.6 kernel (module version 2.25, released 3/29/2020, 59 changesets) 
------------------------

Add support for fallocate mode 0 for non-sparse files. Allow setting owner info, DOS attributes, creation time from user space backup/restore tools.  readdir performance optimization (reduce roundtrips 20% by using compounding of query directory).  "modefromsid" mount parm improved (sets mode bits in ACL at create time, and cache attributes more correctly).  ioctl for change notify added (so user space tools can wait on directory and file change notifications).

5.5 kernel (module version 2.24, released January 26th, 2020. 61 changesets) 
------------------------

New mount parm "modefromsid" (partially available in 5.4) to allow setting mode bits very accurately in a special ACE.  Add SMB3 multichannel support (improves performance). Add support for flock (whole file locks). Add support for root file system on cifs (diskless boot over cifs) so can now boot from cifs (partially available in 5.4).  Add performance optimization to query attributes on close.  Performance optimization for readdir of directories with reparse points.

5.4 kernel (module version 2.23, 75 changesets) 
------------------------

Allow decryption for large reads to be offloaded to improve encrypted read performance (new mount parm "esize=").  Add various new performance related mount options: "nolease" and "cache=ro" and "cache=singleclient" and "signloosely"  Improve debugging by allowing displaying max requests in flight. Permit dumping encryption keys (via "smbinfo" tool) so wireshark traces of encrypted SMB3 traffic can be more easily read.  Allow passthrough IOCtl to call SMB3 SetInfo.

5.3 kernel (module version 2.22, 55 changesets) 
------------------------

Open now about 10% faster (cut from 3 network roundtrips to 2). Cross-mount server side copy (copy offload) now allowed. Server side copy improved (copy_file). Performance of large file I/O over encrypted shares increased dramatically (more than doubled in many cases for large reads and writes) with addition of "GCM" encryption support (and GCM is negotiated by default).  Remove unneeded ifdef for CONFIG_CIFS_ACL (always enabled now).  New netname negotiate context sent when sessions are established with a server (may be useful for load balancers in the future).  SMBDIRECT (SMB3 over RDMA) is no longer experimental.

5.2 kernel (module version 2.20 64 changesets) 
------------------------

Sparse file support improved (fiemap now can query allocated ranges which can help common Linux commands like "cp", and support added for SEEK_HOLE and SEEK_DATA).
"FSCTL passthrough" improved - now can support cases where user space tools need to write data over an SMB3 fsctl.
Improvements to smbdirect (SMB3 over RDMA) support.

5.1 kernel (86 changesets) 
------------------------

"FSCTL passthrough" improved allowing user space tools like smbinfo in cifs-utils to query any available info via SMB3 fsctl or SMB3 query info requests.
New mount parm "handletimeout" added to allow persistent/resilient handle timeouts to be configurable.
Added ability for fallocate "zero range" to expand a file.
Performance improved - FILE_ALL_INFO cached for shared root file handle. default inode block size report now as 1MB. Guest mounts now supported on SMB3.1.1 dialect.  Many dynamic trace points added for easier debugging and analysis. Many important fixes for "crediting" (SMB3 flow control). New mount parm: bsize. Prior to kernel version 5.1, the blocksize was always reported  as  16K  instead of 1MB (and was not configurable) which can hurt the performance of tools like cp and scp.

5.0 kernel (module version 2.17 82 changesets) 
------------------------

SMB3.1.1 dialect requested by default (added to the list of negotiated dialects)
Reconnect possible to server even if its IP address changes (requires the user to install the updated cifs-utils package as well).
Major improvements to DFS caching and reconnect (global name space).  Failover is now possible to alternate targets.
Performance improvements (compounding) for get/set extended attributes.
Many critical bug fixes for 'crediting' (SMB3 flow and control) including reducing reconnects, and fixing large file copy in cases where the network connection is slow or interrupted.
Add key for OFD lock support.

4.20 Kernel (70 changesets) 
------------------------

Large performance improvement in some common operations: stat, create, unlink, mkdir, rmdir, setinfo (due to adding more support for the SMB3 "compounding" feature).
I/O size that client requests has increased (rsize/wsize) from 1MB to 4MB which improves performance to many servers.
Fix Kerberos mounts to Azure (cloud).
Improve performance of direct i/o read and write.
Add passthrough ioctl to allow querying additional metadata information (ie send any SMB3 infolevel) from server (this features is needed for the newly updated **smbinfo** (see its man page in the cifs-utils package for more information) tool to allow it to query SMB3 metadata, ACLs, quotas better and display this information to users).  Add additional tracepoints for improved debugging.  Add pseudo-file /proc/fs/cifs/open_files to allow displaying the list of open files on cifs/smb3 mounts.

4.19 Kernel (69 changesets, module version 2.13) 
------------------------

Allow cifs.ko to be built with insecure dialects disabled (vers=1.0 and vers=2.0 not allowed).
Add support for snapshot mounts (specifying "snapshot=<time>" on the mount allows a previous version of the share to be mounted, read-only).
Bug fixes including improving statfs output; support for ACLs when using vers=3.1.1;
Allow displaying count of "slow" responses (greater than 1 second).  Add additional commonly requested tracepoints (e.g. for reconnections, session expiration, slow requests).
Improve performance of statfs (add support for compounding of query_fs calls).  CONFIG_CIFS_STATS now enabled by default (e.g. "cat /proc/fs/cifs/Stats" shows more information by default)

4.18 Kernel (90 changesets, module version 2.12) 
------------------------

Add "smb3" alias to cifs.ko ("insmod smb3" and "mount -t smb3 ...") and make it easier to disable less secure dialects, disable cifs dialect if mounting with "-t smb3"
Add support for improved tracing (ftrace, trace-cmd)
Cache root file handle (with a directory lease), reduce redundant opens, improving performance. RDMA improvements for direct i/o
Experimental posix extensions for SMB3.1.1 (mount option "posix")

4.17 Kernel (56 changesets) 
------------------------

SMB3.1.1 support (most secure authentication) improved and no longer marked experimental.  SMB3.1.1 Encryption.  SMB3.1.1 Preauth integrity
Add signing support for SMB Direct.
Bug fixes.

4.16 Kernel (66 changesets) 
------------------------

RDMA support (SMB Direct, limited to one adapter). sendfile (and splice write) performance improvement. Bug fixes.

4.15 Kernel (6 changesets) 
------------------------

Bug fixes.

4.14 Kernel (37 changesets) 
------------------------

Add support for getting and setting xattrs over SMB3. Multidialect support now the default (SMB2.1 and later, SMB3, SMB3.02). Bugfixes (including more improvements to reconnection scenarios.  Fixes for "validate negotiate" SMB3 security feature).

4.13 Kernel (24 changesets) 
------------------------

Default **dialect changed to SMB3** (from CIFS).  SMB3 support added for "cifsacl" mount option (and can now emulate retrieving mode from ACL in SMB3). Bug fixes.

4.12 Kernel (36 changesets) 
------------------------

SMB3 mapping of illegal (in SMB3/Windows but legal in POSIX) characters improved.  Improved AIO support.  Add support for enumerating snapshots. Misc. bug fixes.

4.11 Kernel (47 changesets) VERY IMPORTANT update 
------------------------

SMB3 Reconnect improvements (persistent and durable handle fixes). Much higher reliability when server crashes while I/O in flight or cached. Server side copy enabled in more cases (clone file range and "cp --reflink" now support the commonly implemented "copychunk" API not just servers which support "duplicate extents").  SMB3 DFS support added (global name space).  SMB3 Encryption support added (can mount encrypted shares).

4.10 Kernel (14 changesets) 
------------------------

Misc. bug fixes

4.9 Kernel (32 changesets) 
------------------------

Various reconnect improvements (e.g. sends "echo" request sooner after socket reconnect to better tell when server comes back up). Add way to query file attributes (cifs.dosattrib) and creation time (cifs.creationtime) via Linux xattrs.  New mount option "idsfromsid" to let cifs.ko retrieve the uid and gid owner fields from special sids (in the ACL).
Can override number of credits (new mount option "max_credits").  Various bug fixes (including DFS fix).

4.8 Kernel (14 changesets) 
------------------------

Allow mounts with prefixpath where top of share unaccessible.  Fix for file create when existing directory of the same name exists. Add mfsymlink support for SMB2.0 (symlink emulation, also used by the Mac). Various bug fixes.

4.7 Kernel (44 changesets) 
------------------------

Fix badlock regression for guest mounts (mount with -o guest can fail to Samba servers when patched for badlock). Cifs.ko version updated to 2.09.  Minor fixes: including NetApp DFSpathname issue and SPNEGO fix.  Persistent handle reconnect fixes

4.6 Kernel 
------------------------

Minor bug fixes

4.5 Kernel 
------------------------

Minor bug fixes.  clone_file_range added to vfs, cifs support for clone_file_range changed ("cp --reflink ..." now requires duplicate_extents support in server e.g. Windows 2016 REFS, and the older ioct, used e.g. by the "cloner" tool, can use the more broadly supported "CopyChunk").  Allow file open O_DIRECT with cache=loose.  Make echo interval tunable (to improve server failure detection in unusual network configurations).  First phase of SMB3 per-share encryption support begun (not complete in 4.5).

4.4 Kernel 
------------------------

Allow copy offload across shares.   Add resilient and persistent handle mount options ("-o resilienthandles" and "-o persistenthandles") and support for the create context (durable v2)

4.3 Kernel 
------------------------

Minor bug fixes (including Mac authentication issue when timestamps differ too much on server/client).  Add krb5 support for smb3 (mount sec=krb5 for SMB3 e.g. vers=3.02, not just for cifs).  Cifs.ko version updated to 2.08.  Added ioctl to query detailed fs info on mounted share

4.2 Kernel 
------------------------

Partial support for SMB 3.1.1 (Windows 10) dialect support (will improve security when complete).  Faster copy offload (REFLINK, duplicate_extents) added for Windows Server 2016 when target file system is REFS (cp --reflink to REFS exported shares is much faster).

4.1 Kernel 
------------------------

Mapchars fix, fix to allow Unicode surrogate pairs (improved character
conversion for some Asian languages), DFS fix, inode number reuse fix

    .0 Kernel 
------------------------

Fix copy offload (clone range) bug, and find_writable_file bug
    .19 Kernel (cifs.ko module version 2.06) 
------------------------

Fix Oplock bug, inode caching bug and ioctl clone bug
Fix conflicts between SecurityFlags (which allowed CONFIG_MUST_LANMAN and CONFIG_MUST_PLNTXT to get cleared when set to non-default values)
    .18 Kernel 
------------------------

Add mfsymlink (emulated symlink) support for SMB3 mounts
Add optional support for "sfu" style FIFO and device file emulation for SMB3 (when "sfu" is specific on an SMB3 mount)
By default support mapchars ('SFM' style remapping, ie the same way the Mac remaps them) for the seven reserved characters (*, ?, : etc.) when mounting with SMB3.
Workaround Unix Extensions problem on cifs mounts to Mac servers.
    .17 Kernel 
------------------------

SMB3 large read/write support much faster (including SMB3 multicredit support)
Many SMB3 stability fixes
directio allowed on "cache=strict" mounts
Added fallocate/sparse file support for SMB3
Fixed SMB2.1 mounts to MacOS servers
    .16 Kernel 
------------------------

Allow multiple mounts to the same server with different dialects
Rewrote authentication support to improve GSSAPI support
Fix mapchars (which allows use of reserved characters like ':' ion paths) to work over SMB3, not just cifs, mounts
    .15 Kernel 
------------------------

Bug fixes (aio/write, append, xattrs, metadata caching)
    .14 
------------------------

Bug Fixes (including SMB3 large write fix, and fix DFS refererals when mounted to Samba with Unix Extensions). Security fix for illegal aio write request
    .13 
------------------------

Added support for setting/getting **per-file compression** ("chattr +c /mnt/filename")
CIFS and SMB2/SMB3 CopyChunk (**server side copy offload support** added)
Support for O_DIRECT on "cache=none" mounts (**specsfs support**)
Secure negotiate support (**SMB3 downgrade attack protection**)
    .12 
------------------------

Multiuser SMB3 signing now possible
SMB2/SMB3 symlinks
SMB2.1 Lease (client caching)
    .11
**SMB3 signing** (not multiuser)
**Authentication overhaul
SMB2 Durable handle** (improved data integrity)
SMB3.02 dialect added
    .10 
------------------------

cifs.ko module size reduced
nosharesock mount option added
    .9 
------------------------

Bug fixes (rename fix, dfs security fix)
    .8 
------------------------

**default authentication mechanism strengthened**, moved to ntlmv2 (ntlmv2 in ntlmssp)
**smb2.02 support added**
    .7 
------------------------

**SMB2.1 support added**
    .5 
------------------------

"cache=" mount option needed (making forcedirectio and strictcache mount options obsolete)
    .4 (May 2012) 
------------------------

**Faster read ahead performance'.*  Also can allow many more requests in flight for cifs dialect (if server sets maximum multiplex requests above the default, client will now honor it even if more than 50 at one time). Support for "sloppy" mount option.

`Category:CIFSclient`