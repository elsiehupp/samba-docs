Samba 4.0 Whitepaper
    <namespace>0</namespace>
<last_edited>2012-12-15T03:37:23Z</last_edited>
<last_editor>Doublez13</last_editor>

===============================

Introduction
===============================

This document aims at describing the configurations supported for Samba 4.0, particularly the role of Active Directory Domain Controller. Samba 4.0 contains the software components that were previously released in the Samba 3 software and adds "active directory domain controller" as a new server role. The existing Samba 3 configurations are essentially still supported by the Samba 4.0 software.

Samba 3.6 like setups 
------------------------

The classical configurations in the style of Samba 3.6 include standalone fileservers, domain member servers (with security = domain and security = ads), and NT4-style domain controllers. These setups use (combinations of) the individually started daemons smbd, nmbd,
and winbindd and they are still supported with the following exceptions:

* The "security = server" functionality has been removed.
* The "security = share" functionality has been removed.

Administration
------------------------

The management of these classical setups happens with the well known tools "net", "smbpasswd", "smbcontrol" and several more.

New Features
------------------------

New features of SMB file serving include

* Support for SMB 2.0 durable handles
* Support for SMB 2.1 (with the omission of the capabilities leases and branch cache)
* Basic support for SMB 3.0 including negotiation, signing, and encryption.

Clustered Setups with CTDB 
------------------------

Clustered variants of the classical setups are supported with the CTDB software as usual.

Active Directory Compatible Server 
------------------------

Samba 4.0 for the first time features an Active Directory Compatible Domain Controller.

The one setup as Active Directory Compatible Server supported out of the box with Samba 4.0 is this:

* There is only a single domain in the forest.
* There are no cross-forest-trusts (more explicitly, samba can be trusted but can not trust)
* Samba is the only domain controller in its domain.

These limitations are being worked on and will be removed in later 4.X releases.

The support for multiple domain controllers in a domain requires two flavours of replication:

* Directory replication (for the user database)
* File system replication (for the sysvol and netlogon shares)

Of these two Windows protocols, the directory replication is available in Samba, but the file system replication is still being developed.

Note: Homogeneous Samba 4.0 Multi-DC-Domains
------------------------

Hence one can set up homogeneous Samba 4.0 Active Directory multi-DC domains, i.e. domains with multiple Samba 4.0 domain controllers and no Windows domain controllers. For this kind of setup, one needs to set up an external substitute for the file system replication, for instance with some rsync-based shell scripts. One has to do this very carefully though, since the there is not concept of sysvol master role.

Administration
------------------------

The Active Directory part is administered with the new "samba-tool" command.