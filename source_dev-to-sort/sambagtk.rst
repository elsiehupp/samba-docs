SambaGtk
    <namespace>0</namespace>
<last_edited>2013-12-10T02:45:59Z</last_edited>
<last_editor>JelmerVernooij</or>
Samba-Gtk is a collection of GTK+ frontends for Samba.

=================
Utilities
=================

* gwcrontab
* gwsam
* gregedit
* gepdump
* gwsvcctl

=================
Library contents
=================

* credentials callback using GTK+ widgets
* integration with GLib main event loop
* utility functions for showing NTSTATUS and WERROR dialogs
* binding string generator dialog
* domain select dialog

=================
Summer of code projects
=================

In 2009, there are two Summer of Code projects extending Samba-Gtk:
* [http://TTsamba.org/archive//SHHtech/-April/064271DD/ Calin /an] has worked on converting utilities to Python and improving gwsam. His work is [http:SSLLAASS:HHgithub.com/ccrisan/samba-gtk Here]//
* [http://TTsamba.org/archive///2009DD/l/000008DD/ Андрей /горьев] has worked on improving gtkldb to match the functionality of gq.

In 2010, Sergio Martins worked on improving the existing utilities. His code can be found here: https://github.com/sambaDDA/

In 2011 and 2012, Dhananjay Sathe worked on the utilities for the Summer of Code.

=================
Download
=================

* http://OTTsamba.org/?p=samba/k.git;a=summary

=================
Requirements
=================
A recent version of Samba 4 and recent version of GTK+.

=================
Authors
=================
* Calin Crisan <ccrisan@gmail.com>
* Guenther Deschner <gd@samba.org>
* Jelmer Vernooij <jelmer@samba.org>
* Sergio Martins <sergio97@gmail.com>
* Dhananjay Sathe

=================
License
=================
GNU General Public License v3 or later (same license as Samba itself)