Samba/Status
    <namespace>0</namespace>
<last_edited>2017-01-04T16:57:00Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

=================
Samba Project Status
=================

The intent of this page is to provide an overview of the status of various components, capabilities and features which are a part of the Samba project.  The hope is that this page will be a collection point where teams, developers and interested individuals can provide updates as features evolve and mature.  This overview is more aimed at administrators and people who intend to install and *use* Samba than it is a technical rundown of features and development milestones.  Hopefully, this document will help answer the question, *"Has Samba matured to a point where I can put it to use in my environment, and have it actually help me accomplish what I need to do?"*

Development Status 
------------------------

Samba 4.0 has been released and is at version 4.0.9.   Please see the `Samba_Release_Planning|Samba Release Planning` page for detailed information on releases.  It is also possible to download development copies of the Samba software directly from the "git" repository.  Detailed instructions on how to build and set up a Samba4 server can be found in the `Setting_up_Samba_as_an_Active_Directory_Domain_Controller|Samba AD DC HOWTO`.

For detailed development status and a current list of bugs, please:
* see the Samba Bugzilla tracking site at: https://bugzilla.samba.org/.
* join the #samba-technical IRC channel on irc.freenode.net
* subscribe to the Samba (https://lists.samba.org/mailman/listinfo/samba) or Samba Technical (https://lists.samba.org/mailman/listinfo/samba-technical) mailing lists.

Roadmap (Where is Samba headed, what are we interested in doing?) == 
See the `Roadmap` for a list of `Roadmap|current projects being worked on` (as opposed to the desired features listed below, some of which are complete and others are not in the near-term roadmap)

Functional Areas 
------------------------

In order to understand the overall status of the Samba4 project, it can be helpful to think of Samba as covering three main functional areas.  There is a fair amount of overlap between these areas, both in terms of function and in the underlying libraries, but the areas are:

* **Domain Support** (AD functionality):  The core functionality in this area is largely feature complete and stable.  There are several sites reporting that they have been using Samba4 as their primary production Domain Controller, in some cases for close to two years.  (Think of the "80/20" rule.)
* **File Server**:  The Samba4 project actually started out as the "NTVFS Rewrite". (Tridge [http://ftp.samba.org/pub/samba/slides/samba4_tridge.pdf Slide set] from 2003?)  This area has been the focus of much of the current development activity, and has been one of the primary blockers to going into "Beta" (or RC) status.  Due to the rework of the underlying architecture and code, Samba4 has been significantly behind Samba3 functionality for some time.  The "`Samba4/s3fs|s3fs`" effort is a way to move the current Samba3 level of functionality into Samba4.
* **Tools**:  There are a wide variety of tools, utilities and scripts that are built to support the creation, debugging and administration of a Samba4 based domain.  These tools have seen a great deal of rapid development, reflecting the needs of testing and deployment in a wide range of installations.  Most of these tools are command line based, or loadable libraries to be used by third party tools and scripting languages.  An interesting note is that, the easiest approach to GUI based management of the domain is through `Installing_RSAT|Microsoft Remote Administration Tools`.

At a simplistic level, the AD functionality is the most stable of these areas.  Prior to the introduction of the s3fs project, it was recommended for installations to use Samba4 as the Domain Controller, and then use the older Samba3.x servers as members of the domain, functioning as the main file servers.

Feature Status on the AD DC 
------------------------

The following table is a high level breakdown of capabilities and features included in **Samba4**, Samba's AD DC, along with some information on the completeness or usability of each capability.  Please note that this is not by any means a *complete* list of capabilities!  In particular, features of Samba when acting as a file server or domain member server are very different.  

Please see the end of the table for a rough description of the columns and their intent.

{| border="1" cellpadding="2" cellspacing="1"
|-
!Feature/Capability
!Overall Status
!Dependencies
!Notes
!Contact
!Last Update
|-
| Domain Trusts
| Development
|planned winbind changes;<br/>NTLM logins across trust
| Working for DRS replication; NT4 level trusts to a S4 domain working, but not the other way around.  In a cross forest trust, Samba server "completely trusts" the relationship -- ie. no SID filtering.  NEEDS TESTING.  (see also FreeIPA effort)
| Charles Tryon
| 29 May, 2012
|-
|`Winbindd|Winbindd`
| Stable
|
| Does not know how to talk to multiple domains (needed for NTLM logins across domain trusts)
|
| 29 May, 2012
|-
| Domain Logins (Users)
| Stable
|
|
|
|
|-
| Machine Accounts
| Stable
|
|
|
|
|-
| Windows 7 Support
| Stable
|
|Windows 7 clients able to join and participate in domain without any "hacks" or special registry modifications.
|
| 29 May, 2012
|-
| Internal DNS server
| Working
|
| Basic DNS operations working.  Internal server is now the default during provisioning, though BIND (v9.9) can also be configured. See the Samba `The_Samba_AD_DNS_Back_Ends|DNS` page for more information on selecting the internal DNS vs. BIND.
|
| 05 June 2013
|-
| Admin Tools
| Development
|
| samba-tool covers most command line functions; GUI administration through standard Windows AD, DNS and other administrative tools.
|
| 01 May, 2012
|-
|Group Policy Management
|Stable
|
|
|
|
|-
| NTVFS
| Development
|
| The original core Samba4 file server. Supports POSIX backend; Stackable and async; Does not yet support many of the common smbd attributes.  Still in development, but replaced by s3fs as the default file server. *To a large extent, this feature has been deprecated in terms of use for file shares.*
|
| 30 May, 2010
|-
|  `Samba4/s3fs|s3fs `
| Working
|
| s3fs has now replaced NTVFS as the default in new provisioned databases.  Use the --use-ntvfs option to provision to change this.  Still working on issues where it fails to correctly set ACLs for group policy objects based on POSIX attributes. Startup issues if previous shutdown wasn't clean.
|
| 29 May, 2012
|-
| `Samba3/SMB2|SMB2/SMB3`
| Working
|
| SMB2 / SMB3 Protocol support.  Default to SMB3 for max SMB support.  This is implemented in both Samba3 and Samba4 through "s3fs". Please see the `Samba3/SMB2` page for more details.
|Stefan (metze) Metzmacher
| May, 2014
|-
|Printing
|Development
|
| Not supported in the "samba" executable.  Should be supported at 4.0, with "s3fs" handling spools traffic.
|
| 12 June, 2012
|-
|`Samba4/DRS_TODO_List|DRS` Replication
|Testing
|
|DRS replication may fail.  (The team is working on getting the To Do list updated.)
|
|05 June, 2012
|-
| RODC<br/>(Read Only DC)
| Development
|
| major gap on RODCs at the moment is that we need to record the attributes that we replicate to the RODC.
|
| 29 May, 2012
|-
|Exchange support
|Development
|
| Very much a work in progress
|
|13 July 2012
|}

Columns:
------------------------

* **Feature:**  A short name of the feature or capability, such as an interface or AD feature.  There may be some overlap between capabilities, and some sort of simple hierarchy.
* **Overall Status:** A high level indication of the state the project is in.  A single word is sufficient, but it can be a little longer.  This list includes (but is not limited to):

* * **Stable** -- the feature may be going through some tweaking, but generally speaking, it's expected to work, at least at a "Alpha" level.
* * **Working** -- largely feature complete and working, but still dealing with non-trivial issues.
* * **Testing** -- the feature is generally complete, but needs a lot more testing before people can assume it's working.  There may be some serious known bugs.
* * **Development** -- the feature is being worked on, but still has lots of holes.
* * **Nearing completion** -- developers have a clear idea where the feature is going, and have implemented the bulk of it, but it's not really complete.
* * **In Planning** -- developers are actively hacking at a design.  They have a good idea of a roadmap, but very little work has been done yet.
* * **Refactoring** -- Maybe this worked before, but there have been serious deficiencies discovered, and developers are in the middle of tearing out the guts...  :-(
* * **Planned** -- Recognized as needed, but no one has had time to sit down and begin to lay out a solution.

* * **Out of Scope** -- "In your dreams," right?
* **Dependencies:**  What other projects, interfaces, features, documentation or resources is this feature dependent on or waiting for?
* **Notes:**  This overlaps somewhat with the "Dependencies" section, but should list high level **issues** which are either holding up progress, or that still prevent the feature from being useful.  This may also give known **workarounds** (e.g., *"Fine grained control over shares is not supported by S4 -- use a parallel S3 server instead."*)
* **Contact Person:**  This is not an exhaustive list of developers or testers involved.  It could be a name or two of key person(s) directing this feature, or point person to ask questions.  This could simply be a person who is interested in the feature, and acting to keep this status line up to date.
* **Last Update:**  When was the last time someone updated this feature?  This should serve as an indicator as to how current (read: useful) this information is.

**Time Line??**  *Notice that the list does NOT include a column for "When will this be DONE"! *

--`User:Bilbo|Bilbo` 01:50, 31 May 2012 (UTC)