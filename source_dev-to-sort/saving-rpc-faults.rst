Saving RPC FAULTs
    <namespace>0</namespace>
<last_edited>2015-03-16T03:42:37Z</last_edited>
<last_editor>Abartlet</last_editor>

In the AD DC, to save packets that give an NDR fault, because they do not have correct IDL:
* Build with --enable-developer
* set this in the smb.conf
 dcesrv:stubs directory = /var/log/samba

Any packet that fails to parse will be written into that directory, with the failure reason.

These can then be given to ndrdump when fixing the IDL.