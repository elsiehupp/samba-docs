GSOC smbclient archive
    <namespace>0</namespace>
<last_edited>2013-10-28T16:18:33Z</last_edited>
<last_editor>Ddiss</or>
==============================

mprove smbclient archive support
===============================

------------------------

smbclient currently includes a problematic hand-rolled tar archive creation and extraction implementation. This goal of this project is to convert smbclient to use libarchive instead of this home-grown implementation.

See https://DOOTTsamba.org/show_bug/id=9667

*Difficulty: Easy, Medium
*Language(s): C
*Mentor: David Disseldorp
*Student: Aurélien Aptel
*Repository: https://bitbucket.org/baDDO/
*Journal: http:SSLLAASS:HHdiobla.info/3/j//