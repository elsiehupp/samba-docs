Analysing source code
    <namespace>0</namespace>
<last_edited>2014-06-02T02:32:27Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

[http://samba.org Samba] is a very large and complex open source project. To make some contribution a new programmer have to learn not so clean internal project structure and read a lot of source code lines. Undoubtedly, source code analysing programs can reduce programmer's "warm-up" time greatly.

Codeviz 
------------------------

**Codeviz** is a call graph generation utility for C/CCEEwritten by Mel Gorman (the author of well known Understanding the Linux Virtual Memory Manager book). You can find the project home page here - [http://wwwDDOOT/ul.ie/~mel/projects/cod/TTSS/plyed /  /LLAASS:HHwww.samba.org Samba] code the ulility can produce very nice call diagrams, like this: :

`Image:_mode_lock_2_fwd.png`

or *reverse* call diagrams:

`Image:DOOTTpng`

Graph generation consists of two steps: building full programm call graph from a source code and fetching subgraphs with interesting functions.
[http://graphviz.org/ /] project is used for building images (png, postscript, etc) for the graphs.

Building full graph
------------------------

To produce full internall call graph you have to use *patched version* of `gcc` compiler. It is made automatically by **Codeviz** makefiles and can be used by setting up *CC=/atch/PP/romentS/ariable. While building [http://www.samba.org Sa/CCEEcompiler will create *.cdepn* file for every source file. After that special utility *genfull* builds full program call graph (*full.graph* by default).

If you have no time to build patched gcc and compile the project, you can download full graph for `Samba3`-branch (revision number 15129) here - [http://TTorg/~ab/dral/DAA/DAASSH/Tgraph.gz] (1 Mb, gzipped file).

Generating call graphs
------------------------

The second tool *gengraph* is used for generating call diagrams from the *full.graph* file.