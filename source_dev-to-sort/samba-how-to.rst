Samba How to
    <namespace>0</namespace>
<last_edited>2020-06-13T02:51:33Z</last_edited>
<last_editor>Abartlet</last_editor>

=================
Development Practices = 

Typical development process 
------------------------

The typical development process on Samba looks like this:

* A developer has a problem to solve. This might be fixing a bug, or implementing some previously unsupported Windows Server functionality.
* The developer would then write test cases that demonstrate the problem. For server-side behaviour, these test cases would pass when run against a Windows DC, but fail against a Samba DC.
* The finished tests are integrated into Samba’s self-test and marked as known failures initially. There are a couple of benefits to this approach:
* * It’s standard practice in [https://en.wikipedia.org/wiki/Test-driven_development Test-Driven Development (TDD)] to help prove that the new test-case works correctly.
* * It means ``git bisect`` can be run over the codebase, which can help to identify any degradations introduced to Samba. After any given commit, the Samba code will always compile, and will always pass all tests.
* The developer then writes the code to fix the bug or implement the desired functionality. The known failure status for the new tests is removed, the new tests are re-run, and this time they should all pass.
* The developer should then run the full Continuous Integration (CI) test suite over their changes, to verify they haven’t broken any existing functionality. The [https://gitlab.com/samba-team/devel/samba/pipelines Gitlab CI] provides a convenient way to do this, although there are several other approaches. Developers outside the samba team can submit patches to run against the Gitlab CI by following the [https://wiki.samba.org/index.php/Contribute#Subsequent_Merge_Requests_.28and_complex_first_requests.29 Merge Requests process on the Contribute page].
* The developer should end up with a coherent set of patches that add the new functionality, along with tests that prove the new functionality works correctly. They then submit the patch-set as explained by `Contribute|Contribute page`.
* The code is reviewed by [https://www.samba.org/samba/team/ Samba Team members]. While any developer can potentially contribute changes to the Samba codebase, only Samba Team members have the access rights to actually deliver code changes to the master code branch. Usually the reviewers provide some feedback on how the patches could be further improved.
* Once the reviewer is happy, the code must then pass a final CI test run before it’s incorporated into the main Samba codebase.

The following sections cover the Continuous Integration and Code Review process in more detail, as these steps are particularly important to maintaining the quality of the Samba codebase.

Continuous Integration 
------------------------

`Autobuild|Samba's autobuild system` is the core of our CI system.

Code Review 
------------------------

See `CodeReview| Samba's Code Review Guidelines`

=================
Coding Style
=================

When contributing to Samba, it is expected that you will follow the [https://gitlab.com/samba-team/samba/-/blob/master/README.Coding.md Samba coding style guidelines]. The guidelines are about reducing the number of unnecessary reformatting patches and making things easier for developers to work together.

Highlights of what is covered in this document:

* [https://gitlab.com/samba-team/samba/-/blob/master/README.Coding.md#quick-start Quick Start] explains the basic rules of code formatting.
* [https://gitlab.com/samba-team/samba/-/blob/master/README.Coding.md#editor-hints Editor Hints] provides basic editor settings which deal with appropriate whitespace use, etc.
* [https://gitlab.com/samba-team/samba/-/blob/master/README.Coding.md#faq-statement-reference FAQ & Statement Reference] gives examples of correct vs. incorrect code formatting.

=================
Samba codebase organization
=================

{{:Samba codebase organization overview}}

The Samba codebase is broken down in detail in the **`Samba_codebase_organization|Samba codebase organization page`**.