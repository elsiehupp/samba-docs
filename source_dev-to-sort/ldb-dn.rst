Samba4/LDB/DN
    <namespace>0</namespace>
<last_edited>2009-10-13T20:45:31Z</last_edited>
<last_editor>Edewata</last_editor>

=================
Structures
=================

ldb_dn_component 
------------------------

.. code-block::

    struct ldb_dn_component {

    char *name;
    struct ldb_val value;

    char *cf_name;
    struct ldb_val cf_value;
};

ldb_dn_ext_component 
------------------------

.. code-block::

    struct ldb_dn_ext_component {

    char *name;
    struct ldb_val value;
};

ldb_dn 
------------------------

.. code-block::

    struct ldb_dn {

   struct ldb_context *ldb;

   /* Special DNs are always linearized */
   bool special;
   bool invalid;

   bool valid_case;

   char *linearized;
   char *ext_linearized;
   char *casefold;

   unsigned int comp_num;
   struct ldb_dn_component *components;

   unsigned int ext_comp_num;
   struct ldb_dn_ext_component *ext_components;
};

=================
Operations
=================

ldb_dn_get_linearized() 
------------------------

 const char *ldb_dn_get_linearized(struct ldb_dn *dn);

Get the linear form of a DN (without any extended components)

Parameters:
* dn:o linearize

ldb_dn_alloc_linearized() 
------------------------

 char *ldb_dn_alloc_linearized(TALLOC_CTX *mem_ctx, struct ldb_dn *dn);

Allocate a copy of the linear form of a DN (without any extended components) onto the supplied memory context 
    Parameters:
* dn:o linearize
* mem_ctx:ontext to return result on

ldb_dn_get_extended_linearized() 
------------------------

 char *ldb_dn_get_extended_linearized(void *mem_ctx, struct ldb_dn *dn, int mode);

Get the linear form of a DN (with any extended components)

Parameters:
* mem_ctx:ontext to return result on
* dn:o linearize
* mode: extended DN to return (0 is HEX representation of binary form, 1 is a string form)

ldb_dn_get_extended_component() 
------------------------

 const struct ldb_val *ldb_dn_get_extended_component(struct ldb_dn *dn, const char *name);

ldb_dn_set_extended_component() 
------------------------

 int ldb_dn_set_extended_component(struct ldb_dn *dn, const char *name, const struct ldb_val *val);

ldb_dn_remove_extended_components() 
------------------------

 void ldb_dn_remove_extended_components(struct ldb_dn *dn);

ldb_dn_has_extended() 
------------------------

 bool ldb_dn_has_extended(struct ldb_dn *dn);

ldb_dn_extended_add_syntax() 
------------------------

 int ldb_dn_extended_add_syntax(struct ldb_context *ldb, unsigned flags, const struct ldb_dn_extended_syntax *syntax);

ldb_dn_new() 
------------------------

 struct ldb_dn *ldb_dn_new(TALLOC_CTX *mem_ctx, struct ldb_context *ldb, const char *dn);

Allocate a new DN from a string

Parameters:
* mem_ctx:ontext to return resulting ldb_dn structure on
* dn:DN 

The DN will not be parsed at this time.  Use ldb_dn_validate to tell if the DN is syntacticly correct

ldb_dn_new_fmt() 
------------------------

 struct ldb_dn *ldb_dn_new_fmt(TALLOC_CTX *mem_ctx, struct ldb_context *ldb, const char *new_fmt, ...UTE(3,4);

Allocate a new DN from a printf style format string and arguments

Parameters:
* mem_ctx:ontext to return resulting ldb_dn structure on
* new_fms:DN as a format string (plus arguments)

The DN will not be parsed at this time. ldb_dn_validate to tell if the DN is syntacticly correct

ldb_dn_from_ldb_val() 
------------------------

 struct ldb_dn *ldb_dn_from_ldb_val(void *mem_ctx, struct ldb_context *ldb, const struct ldb_val *strdn);

Allocate a new DN from a struct ldb_val (useful to avoid buffer overrun)

Parameters:
* mem_ctx:ontext to return resulting ldb_dn structure on
* dn:DN 

The DN will not be parsed at this time. ldb_dn_validate to tell if the DN is syntacticly correct

ldb_dn_validate() 
------------------------

 bool ldb_dn_validate(struct ldb_dn *dn);

Determine if this DN is syntactically valid 

Parameters:
* dn:o validate

ldb_dn_escape_value() 
------------------------

 char *ldb_dn_escape_value(TALLOC_CTX *mem_ctx, struct ldb_val value);

ldb_dn_get_casefold() 
------------------------

 const char *ldb_dn_get_casefold(struct ldb_dn *dn);

ldb_dn_alloc_casefold() 
------------------------

 char *ldb_dn_alloc_casefold(TALLOC_CTX *mem_ctx, struct ldb_dn *dn);

ldb_dn_compare_base() 
------------------------

 int ldb_dn_compare_base(struct ldb_dn *base, struct ldb_dn *dn);

ldb_dn_compare() 
------------------------

 int ldb_dn_compare(struct ldb_dn *edn0, struct ldb_dn *edn1);

ldb_dn_add_base() 
------------------------

 bool ldb_dn_add_base(struct ldb_dn *dn, struct ldb_dn *base);

ldb_dn_add_base_fmt() 
------------------------

 bool ldb_dn_add_base_fmt(struct ldb_dn *dn, const char *base_fmt, ...UTE(2,3);

ldb_dn_add_child() 
------------------------

 bool ldb_dn_add_child(struct ldb_dn *dn, struct ldb_dn *child);

ldb_dn_add_child_fmt() 
------------------------

 bool ldb_dn_add_child_fmt(struct ldb_dn *dn, const char *child_fmt, ...UTE(2,3);

ldb_dn_remove_base_components() 
------------------------

 bool ldb_dn_remove_base_components(struct ldb_dn *dn, unsigned int num);

ldb_dn_remove_child_components() 
------------------------

 bool ldb_dn_remove_child_components(struct ldb_dn *dn, unsigned int num);

ldb_dn_copy() 
------------------------

 struct ldb_dn *ldb_dn_copy(TALLOC_CTX *mem_ctx, struct ldb_dn *dn);

ldb_dn_get_parent() 
------------------------

 struct ldb_dn *ldb_dn_get_parent(TALLOC_CTX *mem_ctx, struct ldb_dn *dn);

ldb_dn_canonical_string() 
------------------------

 char *ldb_dn_canonical_string(TALLOC_CTX *mem_ctx, struct ldb_dn *dn);

ldb_dn_canonical_ex_string() 
------------------------

 char *ldb_dn_canonical_ex_string(TALLOC_CTX *mem_ctx, struct ldb_dn *dn);

ldb_dn_get_comp_num() 
------------------------

 int ldb_dn_get_comp_num(struct ldb_dn *dn);

ldb_dn_get_component_name() 
------------------------

 const char *ldb_dn_get_component_name(struct ldb_dn *dn, unsigned int num);

ldb_dn_get_component_val() 
------------------------

 const struct ldb_val *ldb_dn_get_component_val(struct ldb_dn *dn, unsigned int num);

ldb_dn_get_rdn_name() 
------------------------

 const char *ldb_dn_get_rdn_name(struct ldb_dn *dn);

ldb_dn_get_rdn_val() 
------------------------

 const struct ldb_val *ldb_dn_get_rdn_val(struct ldb_dn *dn);

ldb_dn_set_component() 
------------------------

 int ldb_dn_set_component(struct ldb_dn *dn, int num, const char *name, const struct ldb_val val);

ldb_dn_is_valid() 
------------------------

 bool ldb_dn_is_valid(struct ldb_dn *dn);

ldb_dn_is_special() 
------------------------

 bool ldb_dn_is_special(struct ldb_dn *dn);

ldb_dn_check_special() 
------------------------

 bool ldb_dn_check_special(struct ldb_dn *dn, const char *check);

ldb_dn_is_null() 
------------------------

 bool ldb_dn_is_null(struct ldb_dn *dn);