Samba3/SMB2/ReauthTests
    <namespace>0</namespace>
<last_edited>2012-04-26T09:42:56Z</last_edited>
<last_editor>Metze</last_editor>

=================
TODO
=================

* test rename simple file
** as user1: create filename1 => handle1
** as user1: ask for the security descriptor => sd1
** reauth as user2 (maybe anonymous)
** as user2: rename on handle1 to filename2
** reauth as user1
** as user1: ask for the security descriptor => sd2
** compare sd1 and sd2
** as user1: close handle1
** as user1: open filename2 => handle2
** as user1: ask for the security descriptor => sd3
** compare sd1 and sd3
** reauth as user2 (maybe anonymous)
** as user2: set delete on close on handle2
** reauth as user1
** as user1: close handle2
** as user1: open filename2 => should fail