Samba4/LDB/Object Class
    <namespace>0</namespace>
<last_edited>2009-10-14T06:48:40Z</last_edited>
<last_editor>Edewata</last_editor>

=================
Source Code
=================

The source code is located at [http://gitweb.samba.org/?p=samba.git;a=blob;f=source4/dsdb/samdb/ldb_modules/objectclass.c source4/dsdb/samdb/ldb_modules/objectclass.c].

=================
Structures
=================

oc_context 
------------------------

.. code-block::

    struct oc_context {

    struct ldb_module *module;
    struct ldb_request *req;

    struct ldb_reply *search_res;

    int (*step_fn)(struct oc_context *);
};

=================
Methods
=================

objectclass_add() 
------------------------

This method intercepts an add request, searches for the parent entry, sorts the object classes, validates the RDN attribute, validates the entry against parent object class, adds some attributes, then performs the actual add operation.

objectclass_modify() 
------------------------

objectclass_rename() ==