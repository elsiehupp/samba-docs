Samba, Active Directory & LDAP
    <namespace>0</namespace>
<last_edited>2009-05-14T20:17:58Z</last_edited>
<last_editor>5mi11er</last_editor>

==============================

Integrating Samba, Active Directory and LDAP
===============================

Abstract
------------------------

I have stumbled onto a nice way to configure Samba to authenticate against AD, but use the UID/GID information from OpenLDAP.

History: how I got here
------------------------

It is so frustrating to me that Microsoft's Authentication mechanism is totally incompatible with mechanisms available with OpenLDAP.  When attempting to integrate Windows and Linux environments together, eventually you realize that there's currently no getting around having to store two password fields, one for windows type authentication, another for linux type authentication.

For me, I distrust Microsoft to the degree that I don't want to modify the AD schema to allow AD to store Linux information, and there's enough magic going on that I don't want to try to fool M$ into thinking that OpenLDAP is actually AD. I know both those options are technically possible, but, if I still have to store two password fields, then I might as well just deal with having to administer both AD and OpenLDAP; after all, most of the linux information is exclusive of the windows information and vice versa.  The few overlapping areas aren't really of great concern to me; do I really care if their phone/address information is accurate? No, I care if they have appropriate access rights.

The Problem
------------------------

So, I created OpenLDAP instance(s) to centralize Linux logins, and it is also used for authentication of internal web services like Open Fire IM, a Xoops Forum, Confluence, etc. AD is used for windows authentication.

Now we go to configure Samba, and we run straight into the problem.  On the one hand, OpenLDAP contains the user's uid and gid numbers that we want Samba to recognize, but we can't authenticate a windows share using OpenLDAP, we have to use AD.  But, an unmodified AD schema has no way to store the appropriate uid/gid numbers.

One potential solution is for Linux to run Winbind, which has the ability to map AD Security Identifiers (SID's) to uids and gids, but keeping such a table accurate would create time consuming and tedious administration tasks. It appears, we've got a catch 22 situation with no clean solution.

The Solution I stumbled upon
------------------------

So, I began by configuring Kerberos, Samba and Winbind according to the Samba wiki but ignoring the PAM configuration because I don't want Linux login's to authenticate against AD.  After setting things up as shown in the configuration areas below, but with winbind also configured, I discovered that when users mapped the Samba home directory, the uid/gid numbers that were being used were, in fact, coming from the OpenLDAP server, and was NOT the automatically generated SID/UID/GID mapping created by Winbind!

The username was identical in AD and OpenLDAP, and the OpenLDAP information in the nsswitch.conf file was listed first, so that's what was being used; I didn't need Winbind at all. So, I just removed the Winbind option from the /etc/nsswitch.conf and /etc/samba/smb.conf files altogether.

So, as long as the AD "user logon name", and OpenLDAP uid strings match for all the users that need linux logins and Samba mappings, you don't need to use winbind at all.

Why it works
------------------------

Samba will authenticate against AD, and then utilize the normal 'getent' system calls to gather the uid/gid numbers, and those will come from OpenLDAP, and/or the local system files as configured within the nsswitch.conf file.

Prerequisites
------------------------

* Linux server configured to use OpenLDAP for authentication
** OpenLDAP server on the network
** OpenLDAP client configured properly on the target server
** Nss_LDAP configured properly on the target server
** PAM configured properly on the target server
* Samba installed on the target server
* Kerberos installed on the target server

Kerberos Configuration
------------------------

/etc/krb5.conf

.. code-block::

    default = FILE:/var/log/krb5libs.log
 kdc = FILE:/var/log/krb5kdc.log
 admin_server = FILE:/var/log/kadmind.log

[libdefaults]
 default_realm = DOMAIN.COM
 dns_lookup_realm = false
 dns_lookup_kdc = false
 ticket_lifetime = 24h
 forwardable = yes

[realms]
 DOMAIN.COM = {
    dc = dc1.domain.com:88
    dmin_server = dc1.domain.com:749
    efault_domain = domain.com
 }

[domain_realm]
 .domain.com = DOMAIN.COM
 domain.com = DOMAIN.COM

Configure nsswitch
------------------------

/etc/nsswitch.conf

.. code-block::

    passwd:       files ldap
shadow:       files ldap
group:        files ldap

Samba Configuration
------------------------

/etc/samba/smb.conf

.. code-block::

    [global]
        workgroup = DOMAIN
        server string = Samba Server Version %v

        security = ADS
        realm = DOMAIN.COM

        encrypt passwords = yes
        log level = 3
        log file = /var/log/samba/%U.log
        max log size = 50
        template shell = /bin/bash

[homes]
        comment = Home Directories
        browseable = no
        writable = yes
        force create mode = 0660
        force directory mode = 0770

Establish AD Connection
------------------------

AD needs to have the machine register as a member of the AD domain, first initialize the Kerberos connection:

.. code-block::

    kinit <domain administrator@domain.com>

You'll need to provide the appropriate password, then:
<pre>net ads join "Computers"</pre>

If successful it should return a notification that the computer was connect to Active Directory.

Start Samba Services
------------------------

<pre>Service smbd start</pre>

Map a drive to the samba share
------------------------

As an appropriate user logged into the AD domain, just type this at a windows command prompt:
<pre>net use <drive letter>: \\<server>\homes</pre>