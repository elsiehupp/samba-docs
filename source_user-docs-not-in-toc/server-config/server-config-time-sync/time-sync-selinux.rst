Time Synchronisation - SELinux Labeling and Policy
    <namespace>0</namespace>
<last_edited>2016-09-03T23:55:13Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

Set policy for Windows client time sync:

.. code::

    # chcon -u system_u -t ntpd_t /usr/local/samba/var/lib/ntp_signd

Make the policy permanent

.. code::

    # semanage -a -t ntpd_t "/usr/local/samba/var/lib/ntp_signd"

Verify the change

.. code::

    # cat /etc/selinux/targeted/contexts/files/file_contexts.local

You should see a line like:

.. code::

    /usr/local/samba/var/lib/ntp_signd    system_u:object_r:ntpd_t:s0

The below policy file is based on 4.1.6 on RHEL 6.5 and should be for reference only. Always run the below egrep command, after you have a base policy file created. Then stop / start ntpd and Samba AD services on your system and add to the base policy until competed.

.. code::

    egrep "samba|ntpd" /var/log/audit/audit.log | audit2allow

If all is fine, you'll see this for each "allow" line below in the policy file.

.. code::

    #!!! This avc is allowed in the current policy

    allow ntpd_t self:capability sys_admin;

    etc.

<tt>samba4.te</tt> policy: 

.. code::

    module samba4 1.0;

    require {

      type ntpd_t;

      type usr_t;

      type initrc_t;

      type fs_t;

      type setfiles_t;

      type lib_t;

      type unconfined_t;

      type locate_t;

      class dir write;

      class dir search;

      class dir open;

      class dir read;

      class dir getattr;

      class dir remove_name;

      class dir add_name;

      class dir relabelto;

      class unix_stream_socket connectto;

      class sock_file write;

      class sock_file create;

      class sock_file unlink;

      class filesystem associate;

      class capability sys_admin;

    }

        #============= initrc_t =========

    allow initrc_t ntpd_t:dir { write remove_name add_name };

    allow initrc_t ntpd_t:sock_file create;

    allow initrc_t ntpd_t:sock_file unlink;

    #============= ntpd_t =========

    allow ntpd_t usr_t:sock_file write;

    allow ntpd_t initrc_t:unix_stream_socket connectto;

    allow ntpd_t fs_t:filesystem associate;

    allow ntpd_t lib_t:sock_file write;

    allow ntpd_t unconfined_t:unix_stream_socket connectto;

    allow ntpd_t self:sock_file write;

    allow ntpd_t self:capability sys_admin;

    #============= locate_t =========

    allow locate_t ntpd_t:dir { read getattr open search };

    #============= setfiles_t =========

    allow setfiles_t ntpd_t:dir relabelto;  

Check and load policy:

.. code::

    $ checkmodule -M -m -o samba4.mod samba4.te 
    $ semodule_package -o samba4.pp -m samba4.mod
    $ semodule -i samba4.pp