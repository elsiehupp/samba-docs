BIND9 DLZ AppArmor and SELinux Integration
    <namespace>0</namespace>
<last_edited>2019-10-23T15:31:32Z</last_edited>
<last_editor>Hortimech</last_editor>

=================
Interaction with AppArmor or SELinux
=================

If you are getting an error from samba_dnsupdate and nsupdate **return dns_tkey_negotiategss: TKEY is unacceptable** try the following:

If you are using AppArmor or SELinux, you have to ensure that the BIND process has read access to the following files: 
* /usr/local/samba/private/dns.keytab
* /usr/local/samba/private/named.conf
as well read-write access to the
* /usr/local/samba/private/dns/
directory and it's own zone file(s).

The Samba provision tries to setup the permissions correctly for these files, but you may find you need to make changes in your AppArmor or SELinux configuration if you are running either of those. If you are using AppArmor, then the 'aa-logprof' command may help you add any missing permissions you need to add after you start Samba and BIND for the first time after configuring them.

Permissions, SELinux Labeling and Policy

These instructions are intended for RHEL6, but may serve as a guide for other distributions/versions.

There is still more work to be done in regards of creating a Samba 4 specific SELinux policy but for now you should be able to have everything working without disabling SELinux.

For all the commands below, make sure you have set the following environment variable:
 MYREALM="samdom.example.com"

Set Permissions (SELinux):

 chown named:named /usr/local/samba/private/dns
 chgrp named /usr/local/samba/private/dns.keytab
 chmod g+r /usr/local/samba/private/dns.keytab
 chmod 775 /usr/local/samba/private/dns

Label files (SELinux):

 chcon -t named_conf_t /usr/local/samba/private/dns.keytab
 chcon -t named_conf_t /usr/local/samba/private/named.conf.update
 chcon -t named_var_run_t /usr/local/samba/private/dns
 chcon -t named_var_run_t /usr/local/samba/private/dns/${MYREALM}.zone

Set Label Persistence (SELinux):

 semanage fcontext -a -t named_conf_t /usr/local/samba/private/dns.keytab
 semanage fcontext -a -t named_conf_t /usr/local/samba/private/named.conf
 semanage fcontext -a -t named_conf_t /usr/local/samba/private/named.conf.update
 semanage fcontext -a -t named_var_run_t /usr/local/samba/private/dns
 semanage fcontext -a -t named_var_run_t /usr/local/samba/private/dns/${MYREALM}.zone
 semanage fcontext -a -t named_var_run_t /usr/local/samba/private/dns/${MYREALM}.zone.jnl

AppArmor Configuration :

Add the following to the end of /etc/apparmor.d/local/usr.sbin.named (create it if it doesn't already exist).

 # Samba DLZ and Active Directory Zones (default source installation)
 /usr/local/samba/lib/** rm,
 /usr/local/samba/private/dns.keytab rk,
 /usr/local/samba/private/named.conf r,
 /usr/local/samba/private/dns/** rwk,
 /usr/local/samba/etc/smb.conf r,

.. note:

    From Samba 4.8.0 ``/usr/local/samba/private`` is replaced by ``/usr/local/samba/bind-dns``

Additionally, it was found that on some distributions, additional paths may be required; consult your AppArmor logs for more information. 

Examples:

It was found that on Ubuntu 14.04.1 LTS, BIND was trying to create files such as /var/tmp/DNS_110, and so a further entry was required:

 /var/tmp/** rwmk,

It was found that on Ubuntu 16.04 LTS, /dev/urandom is used, so a further entry was required:

 /dev/urandom rw,