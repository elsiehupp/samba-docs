Using BIND DLZ backend with secured / signed DNS updates
    <namespace>0</namespace>
<last_edited>2021-05-01T08:45:17Z</last_edited>
<last_editor>Hortimech</last_editor>

===============================

Introduction
===============================

In the default configuration of Distributed ISC Bind  in many Distributions you will find that the secured updates do not work with Samba 4.  You will receive errors in /var/log/messages indicating update '<name of client>' denied.

This is because the rpm/deb/pkg has been compiled with the 'disable-isc-spnego' flag and/or without bind-dlz support at all.
In order to fix this you will need to recompile/rebuild the distribution rpm / deb / pkg.

=================
Common Bind9 Compile flags for BIND DLZ for bind 9.9
=================

Samba4 and up  require for BIND DLZ at least --with-dlz-ldap , --with-dlz-filesystem=yes

Common used configure flags for BIND9.8.6 and up:

.. code-block::

    CONFIGURE_OPTIONS="\
...
--with-openssl \
--enable-threads \
--with-gssapi=yes \
--with-libtool \
--with-libxml2 \
--with-dlopen=yes \
--with-dlz-mysql \
--with-dlz-bdb \
--with-dlz-ldap \
--with-dlz-filesystem=yes \
--with-dlz-bdb=yes \
--enable-filter-aaaa \
--enable-rrl \
--with-ecdsa \
--enable-threads \
--with-idnlib='-L/usr/lib -R/usr/lib -lidn -lidn2' \
...

The parameters for "-L" "-R" can differ from Distribution needs.

 ... we ask herein  i386 / x64_64 and  Child Distribution Maintainers Members to update the package specs 
 for Bind 9.8.5 and up to cover this for Samba4 in permanent.

=================
RHEL / CENTOS / FC + clones -  ReBuild Distributed ISC Bind RPM
=================

First make sure you have your RPM build environment setup and then install the source rpm for bind.  The instructions that follow are for CENTOS 6.4 with 9.8.2-0.17.rc1.el6_4.6 being the latest version at the time of writing:

    pm -i http://vault.centos.org/6.4/updates/Source/SPackages/bind-9.8.2-0.17.rc1.el6_4.6.src.rpm

Then locate and edit the SPEC file, if your rpm build directory is ~/rpmbuild then it will be ~/rpmbuild/SPECS/bind.spec

Now locate and remove the line that reads:

    -disable-isc-spnego

On the example rpm provided it is line 361.

Now recompile your rpm:

    pmbuild -bb ~/rpmbuild/SPECS/bind.spec

Once finished you should find the replacement rpms in the RPMS/{arch} path of your build root.  Replace {arch} with the relevant architecture of your machine (e.g. x86_64 or i686). Install them over the top of your existing rpms and updates should all be working again. Remember if you update with yum it may replace your copy of bind so you will either want to exclude bind* in your yum configuration or use priorities and add these rpms to a local repository.

=================
OpenSuSE using ISC BIND backend
=================

In the default configuration of Bind in the OpenSuSE  distribution you will find that the secured updates do not work with Samba 4.1 and up. 
In order to fix this, you will need to recompile the https://build.opensuse.org/package/show/openSUSE:Factory/bind.

 `User:Remsnet` Published  https://github.com/remsnet/OpenSuSE-Samba-DC/blob/master/bind-9.9.4-P1.spec

    ith Build Instructions at https://github.com/remsnet/OpenSuSE-Samba-DC/blob/master/Samba4-DC-DLZ.Readme

=================
RHEL/CENTOS/FC + clones - RPM  Build New ISC Bind 9.9 / 9.10
=================

 Benjamin Kraft publishes on his [http://bkraft.fr/blog/bind_9_10_1_and_bind_9_9_6_and_bind_9_8_8/]  Bind9 Security Fixes Page.
 Cleanly Patching Bind9 has almost been a task for experts ...

 `User:Remsnet` Published https://github.com/remsnet/CentOS-Bind-DLZ SPEC File for SRPMS to Build a Clean and updated Bind9 with DLZ .

=================
Debian / Ubuntu + clones - Build New ISC Bind  9.9 / 9.10
=================

.. note:

    This should no longer be needed on the latest Debian based distro's, it was only required if using Bind9 9.8.x on Wheezy & Jessie.

While following the official Samba4 HOWTO I found that bind would not start, giving me the following error (taken from my syslog):

 Loading ‘AD DNS Zone’ using driver dlopen
 unsupported DLZ database driver ‘dlopen’.  AD DNS Zone not loaded.

If you want some technical background as to what dlopen is, read this blog post, but in short, Samba4 needs some features only available in Bind 9.8 and above.
If you get the error I described above, you either have an earlier version or your binary version of Bind 9.8.3 or above was not compiled with support for dlz drivers.

* To fix it I backported Bind9 from SID on i386/x86_64.
* To Fix it in debian RPi , you need to rebuild / recompile 

* **Note**: when Bind 9.8 gets backported it will become much easier to install, you will only have to follow the steps described here. For now, follow the instructions below.
Let’s remove the old version of bind first:

 $ sudo apt-get remove bind9

Install required packages(wheezy):

 $ apt-get update
 $ sudo apt-get install devscripts build-essential libkrb5-dev debhelper libssl-dev libtool bison libdb-dev libldap2-dev libxml2-dev libpcap2-dev hardening-wrapper libgeoip-dev dpkg-dev

Download bind9 .dsc file (check here for the latest link to the .dsc file):

 $ mkdir -p /usr/src/BUILD/bind9
 $ cd /usr/src/BUILD/bind9

 $ dget -x http://ftp.de.debian.org/debian/pool/main/b/bind9/bind9_9.9.5.dfsg-7.dsc

Now unpack bind, Configure and then compile Bind9 source code:

 $ tar xvzf bind9_9.9.5.dfsg.orig.tar.gz
 $ cd bind9-9.9.5.dfsg
 $ fakeroot ./configure --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info \
         --sysconfdir=/etc/bind --localstatedir=/var --enable-threads --enable-largefile --with-libtool \
         --enable-shared --enable-static --with-openssl=/usr --with-gssapi=/usr --with-gnu-ld \
         --with-dlz-postgres=no --with-dlz-mysql=no --with-dlz-bdb=yes --with-dlz-filesystem=yes \
         --with-dlz-ldap=yes  --with-dlz-stub=yes --with-dlopen=yes \
          --with-geoip=/usr --enable-ipv6 CFLAGS=-fno-strict-aliasing 

* **Note**: If you are using bind9 9.8.1 and below, you may find a compilation error which can be fixed with the patch described here. You can apply the patch manually: all you have to do is to edit the file contrib/dlz/drivers/sdlz_helper.c and to remove the “#ifdef DLZ” line and the “#endif” line at the end of the file.

* **Note**: If you are building Bind 9.8.0, you must use '--with-dlz-dlopen=yes' instead of '--with-dlopen=yes'.

Now let’s compile and install bind9:

 $ make install

Last step, we need to manually create the /var/cache/bind directory:

 $ sudo mkdir /var/cache/bind

Verify Bind Compile Options with: 

 $ named -V

Start the service bind (wheezy):
 # /etc/init.d/bind9 start

Start the service bind (jessie):
 # service bind9 start

* Do NOT run bind chrooted with a samba AD DC and make sure every thing has the correct privileges ..
* make sure you run the provisioning steps again with bind9 running.
* bind9 should start just fine -  server/bind logs should show that the samba-dlz zones loaded ok.