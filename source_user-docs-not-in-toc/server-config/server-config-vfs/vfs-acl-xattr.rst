Using the acl xattr VFS Module
    <namespace>0</namespace>
<last_edited>2017-11-01T16:47:54Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

===============================

Introduction
===============================

The ``acl_xattr`` Virtual File System (VFS) module enables you to use the fine-granular Windows Access Control Lists (ACL) on a share. For further details, see `Setting up a Share Using Windows ACLs`.

Samba stores the Windows ACLs in the ``security.NTACL`` extended attribute of a file or directory. Therefore, the file system of the share must support extended attributes. For details, see your operating system's and file system's documentation.

.. note:

    On a Samba Active Directory (AD) domain controller (DC), the ``acl_xattr`` module is automatically globally enabled and cannot be deactivated. You must not add it to your ``smb.conf`` file manually.

=================
Enabling the ``acl_xattr`` Module
=================

You can enable the ``acl_xattr`` module either globally in the the ``[global]`` section or for individual shares in the share's section. To enable the module:

* Edit your ``smb.conf`` file. Depending on where to enable the module, add the following entry either to the ``[global]`` or to the share's section:

 vfs objects = acl_xattr

* Reload the Samba configuration:

 # smbcontrol all reload-config

=================
Additional Resources
=================

For additional information, see the ``vfs_acl_xattr`` man page.

----
`Category:Active Directory`
`Category:NT4 Domains`
`Category:Domain Members`
`Category:Virtual File System Modules`