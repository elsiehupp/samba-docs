Using the streams xattr VFS Module
    <namespace>0</namespace>
<last_edited>2017-11-01T16:47:34Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

===============================

Introduction
===============================

The ``streams_xattr`` Virtual File System (VFS) module enables applications to store information in Alternative Data Streams (ADS). Certain applications, such as the Microsoft Edge browser, require ADS to operate correctly. For example, if you use Edge to download a file to a Samba share that has no ADS support enabled, the download will fail.

Samba stores ADS in the ``user.DosStream.*ADS_name*`` extended attribute of a file or directory. Therefore, the file system of the share must support extended attributes. For details, see your operating system's and file system's documentation.

=================
Enabling the ``streams_xattr`` Module
=================

You can enable the ``streams_xattr`` module either globally in the the ``[global]`` section or for individual shares in the share's section. To enable the module:

* Edit your ``smb.conf`` file. Depending on where to enable the module, add the following entry either to the ``[global]`` or to the share's section:

 vfs objects = streams_xattr

* Reload the Samba configuration:

 # smbcontrol all reload-config

=================
Additional Resources
=================

For additional information, see the ``vfs_streams_xattr`` man page.

----
`Category:Active Directory`
`Category:NT4 Domains`
`Category:Domain Members`
`Category:Virtual File System Modules`