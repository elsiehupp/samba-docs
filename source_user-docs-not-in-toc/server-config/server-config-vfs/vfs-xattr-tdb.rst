Using the xattr tdb VFS Module
    <namespace>0</namespace>
<last_edited>2020-02-28T18:30:43Z</last_edited>
<last_editor>Abartlet</last_editor>

===============================

Introduction
===============================

The ``xattr_tdb`` Virtual File System (VFS) module stores extended attributes in a Trivial Database (TDB) file. This enables you to use extended attributes on operating systems or file systems that do not support extended attributes.

You can use this module with other VFS modules, such as `Using_the_acl_xattr_VFS_Module|acl_xattr` or `Using_the_streams_xattr_VFS_Module|streams_xattr`, that store information in extended attributes.

{{Imbox
| type = warning
| text = Storing extended attributes in a TDB file is inefficient, unsafe (unless absolutely all access is via Samba) and does not scale. Do not use this module in production.

=================
Enabling the ``xattr_tdb`` Module
=================

You can enable the ``xattr_module`` module either globally in the the ``[global]`` section or for individual shares in the share's section. To enable the module:

* Edit your ``smb.conf`` file. Depending on where to enable the module, add the following entry either to the ``[global]`` or to the share's section:

 vfs objects = xattr_tdb

* Optionally, set the path to the TDB file. For example:
 xattr_tdb = /usr/local/samba/var/locks/xattr.tdb

* Reload the Samba configuration:

 # smbcontrol all reload-config

=================
Additional Resources
=================

For additional information, see the ``vfs_xattr_tdb`` man page.

----
`Category Directory`
`Category Domains`
`Category Members`
`Category: File System Modules`