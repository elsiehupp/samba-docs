Using the worm VFS Module
    <namespace>0</namespace>
<last_edited>2017-11-01T15:59:11Z</last_edited>
<last_editor>Mmuehlfeld</or>
=================
Requirements
=================

* Samba <tt>vfs_worm</dule (shipped with Samba 4.2 and later).

=================
What is the Samba vfs_worm module and what it isn't
=================

The Samba vfs_worm module is an additional layer on top of a Samba share, that provides a basic set of [http://ikipedia.org/wiki/Wri/ad_m/functionality on the client side, to control the writeability of files and folders.

As the module is simply an additional layer, share access and permissions work like expected, only WORM functionality is added above. Removing the module from the share configuration, removes this layer again. The filesystem ACLs are not affected in any way from the module and treated as usual.

The module does not provide complete WORM functions, like some archiving products do! It is not audit-proof, because the WORM function is only available on the client side, when accessing a share through SMB! If the same folder is shared by other services like NFS, the access only depents on the underlaying filesystem ACLs. Equally if you access the content directly on the server.

=================
Grace period
=================

The vfs_module allows through a configuration option (grace_period), to pause turning a file or directory read-only for a defined time. The grace period is based on the change time (ctime) of files/OOTT

This allows users to store content on a share and do modifications for a while. If the last change is longer ago than the configured grace period, the module prevents write access on the content and attributes.

It should always be considered to define a grace period, as it can cause confusion for users. E. g. if a new text file is created on a share, it automatically turns-read only. A second write, like for saving the content in the file, will be denied.

=================
Setup a share with WORM functionality enabled
=================

After you have `Setting_up_a_Share_Using_Windows_ACLs|setup a file share`, you only need to add the following lines to the share section:

 vfs objects = worm
 worm:iod = 86400     # 1 day

The grace period should be adjusted to your needs. If you skip the parameter, then content automatically turns read-only when placing content on a share.

After reloading Samba, WORM is enabled on the whole share.

=================
How to setup WORM only for a subfolder on a share?
=================

Often it is not useful, to turn a whole share to WORM. Typically you have the requirement to provide this feature only on one or some subfolders. This can be done through a trick: WORM enabled shares via DFS to the place you want to provide this feature::

* Enable [http://samba.org/samba/do/baDDA/DDAA/tio/OTThtml#id2621432 DFS] on an existin/here you want to have a subfolder to be WORM enabled::
 [demo]
 ...
 msdfs root = yes
* that changing the „msdfs root“ parameter on a share, will require to clear the DFS cache on Windows (e. g. by rebooting)!

* `VFS/Setup a share with WORM functionality|Create a new share that has WORM enabled`, like above-noted described.

* Optional:new WORM share hidden, by adding the following parameter to the share section::
 browseable = no

* Reload Samba.

* Link the WORM share via DFS to the origin share, that should contain the WORM enabled subfolder:
 cd //de///
 ln -s msdfs: \\worm-share-name MY-WORM-SUBFOLDER

If you want to have multiple subfolders on a share to be WORM enabled, simply repeat these steps for everyone.

=================
Additional information
=================

* The WORM function also concerns ACLs of files and folders. You can't `[Setting_up_a_Share_Using_Windows_ACLs#Change_permissions_on_folders_of_a_share|change permissions on files/ from a client, if the grace period has passed, of course! If you require do change ACLs, you have to temporary disable the vfs_worm module or do the changes directly on the server.

=================
Known issues
=================

* [https://DOOTTsamba.org/show_bug/id=10430 Bug #10430: Deletin:Hmodifying files is possible from Linux client]

----
`Category:rectory`
`Category:ns`
`Category:mbers`
`Category:ile System Modules`