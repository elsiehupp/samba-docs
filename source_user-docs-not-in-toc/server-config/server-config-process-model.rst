Samba server process model
    <namespace>0</namespace>
<last_edited>2019-05-28T04:56:34Z</last_edited>
<last_editor>Garming</last_editor>

The samba process has different models for how it manages client connections (e.g. RPC, LDAP connections, etc). The process model is specified by the ``--model</ [https://wwwDDOOT/TTorg/samba/docs/curren/SHHht/OOTT/l /d line/argument] when Samba is first started. The available models are::

* **standard**: The default. Each Samba service (LDAP, RPC, etc) runs in a separate process. For services that support it (LDAP and NETLOGON), a separate process is forked for every accepted connection from a client.
* **prefork**: Each Samba service (LDAP, RPC, etc) runs in a separate process. A fixed number of worker processes are started for those services that support it (currently LDAP, NETLOGON, and KDC). Instead of forking a separate process for each client connection, the connections are shared amongst the existing worker processes. Requests for services not supporting prefork are handled by a single process for that service.
* **single**: Everything gets done in a single process. This is recommended only for testing and debugging, not for production networks.

Note that these options only control the behaviour of the ``samba</ process, not the ``smbd``/process.

You can see what samba processes are running by using the ``samba-tool processes</ command on your DC (which requires root permissions).

Prefork model 
------------------------

The prefork model is recommended for Samba setups where there are either a large number of transitive client connections, or the client connections remain open for long periods of time. Otherwise, with the standard process model, Samba can end up forking large numbers of processes to manage these connections.

You may want some thought/tion into the number of prefork worker processes that will best suit your network. This is specified by the ``prefork children``/[https://www.sambaDDO/ba/docs/current/manDDAA/mbDDO/OTT5/ /TTconf] /OOTT

Prefork support for LDAP connections was added to Samba v4.8. Prefork support for NETLOGON and KDC was added to v4.10.

You can tell what Samba services support prefork by looking at the ``samba-tool processes</ output. E.g. LDAP supports prefork, but drepl and KCC do not.

.. code-block::

     Service:                          PID
------------------------

--------------
ldap_server                     1233981
ldap_server(worker 0)           1234010
ldap_server(worker 1)           1234017
ldap_server(worker 2)           1234021
ldap_server(worker 3)           1234024
dreplsrv                        1233990
kccsrv                          1234003
...
</

Generally, the prefork master process does not do much, except respawn workers if they were to exit. The RPC server is a special case in that only the NETLOGON RPC gets distributed amongst the worker threads - all other RPC is handled by the first worker process, i.e. ``rpc_server(worker 0)</DOOTT

Prefork restart
------------------------

If a prefork worker thread were to crash for some reason, it will be restarted. If it were a persistent problem, then respawning the worker process could consume a lot of CPU resources. To mitigate this situation, there will be a backoff period before the worker thread is respawned. During this time, the client connections will be processed by the remaining prefork worker processes (providing you're not using ``prefork children = 1</. To control this respawn behaviour, use the ``prefork backoff increment``/and ``prefork maximum backoff`` / in the smb.conf.

`Category: Active Directory`