Ldapsam Editposix
    <namespace>0</namespace>
<last_edited>2017-05-07T12:59:02Z</last_edited>
<last_editor>Hortimech</last_editor>

(First submitted by idra (a) samba.org)

Samba and the Editposix/Trusted Ldapsam extension 
------------------------

The ldapsam: ditposix extension is based on the ldapsam:trusted optimization.
The ldapsam:trusted optimization was developed as a performance optimization for a server that uses ldap as user and group account storage. This optimization _requires_ that all samba users and group accounts store their posix account information in the ldap tree.

The ldapsam: ditposix extension was created with the aim of making it easier to configure samba for use with an ldap server, by providing means to add the posix accounts and groups on the LDAP server without needing to use external scripts.

To further help admins we introduced the <b>net sam provision</b> command that helps with creating the basic accounts and groups need to make smbd run.

A running winbind daemon is required to use ldapsam: ditposix EVEN ON A SAMBA PDC.

You must also use the ldap idmap backend.

Basic LDAP configuration 
------------------------

After the basic slapd installation you should have the following LDAP configuration:

.. code-block::

    dn: dc=samba,dc=org
    objectClass: top
    objectClass: dcObject
    objectClass: organization
    o: samba.org
    dc: samba

    dn: cn=admin,dc=samba,dc=org
    objectClass: simpleSecurityObject
    objectClass: organizationalRole
    cn: admin
    description: LDAP administrator
    userPassword:: e1NTSEF9aGxQZ0g2bDF4aGJLWFIydENuV1BabXFXY3Y2TEdHUzI
===============================

You will now need to Import the samba schema into LDAP:

On Debian using the distro Samba packages, you will need to do this:

 cp /usr/share/doc/samba/examples/LDAP/samba.ldif.gz /etc/ldap/schema/
 gunzip /etc/ldap/schema/samba.ldif.gz
 ldapadd -Q -Y EXTERNAL -H ldapi:/// -f /etc/ldap/schema/samba.ldif

Next create a file called samba_indices.ldif and add the contents as shown below:

nano /etc/ldap/schema/samba_indices.ldif

 dn: olcDatabase={1}mdb,cn=config
 changetype: modify
 add: olcDbIndex
 olcDbIndex: ou eq
 olcDbIndex: mail eq
 olcDbIndex: surname eq
 olcDbIndex: givenname eq
 olcDbIndex: loginShell eq
 olcDbIndex: uniqueMember eq,pres
 olcDbIndex: sambaSID eq
 olcDbIndex: sambaPrimaryGroupSID eq
 olcDbIndex: sambaGroupType eq
 olcDbIndex: sambaSIDList eq
 olcDbIndex: sambaDomainName eq
 olcDbIndex: default sub
 olcDbIndex: nisMapName eq
 olcDbIndex: nisMapEntry eq
 -
 add: olcAccess
 olcAccess: to attrs=loginShell by dn="cn=admin,dc=samba,dc=org" write by self write by * read
 olcAccess: to attrs=sambaNTPassword,sambaLMPassword,sambaPwdLastSet,sambaPwdMustChange by dn="cn=admin,dc=samba,dc=org" write by self write by * none

Now import it using the following command:

 ldapmodify -Q -Y EXTERNAL -H ldapi:/// -f /etc/ldap/schema/samba_indices.ldif

Now you need to add some OU's to the ldap database:

 nano samba.local.ldif

 dn: ou=users,dc=samba,dc=org
 objectClass: top
 objectClass: organizationalUnit
 ou: users

 dn: ou=groups,dc=samba,dc=org
 objectClass: top
 objectClass: organizationalUnit
 ou: groups

 dn: ou=idmap,dc=samba,dc=org
 objectClass: top
 objectClass: organizationalUnit
 ou: idmap

 dn: ou=computers,dc=samba,dc=org
 objectClass: top
 objectClass: organizationalUnit
 ou: computers

Now add the ldif with:

 cat samba.local.ldif | ldapadd -D cn=admin,dc=samba,dc=org -W -x -H ldapi:///
 Enter LDAP Password: 

Configuring smb.conf 
------------------------

you need to properly configure smb.conf before running any daemon
or command.

Here is the bare minimum options to set in smb.conf:

 [global]
   workgroup = SAMBA
   domain logons = yes
   os level = 65
   time server = yes
   passdb backend = ldapsam
   ldapsam: ditposix = yes
   ldapsam:trusted = yes
   ldap admin dn = cn=admin,dc=samba,dc=org
   ldap suffix = dc=samba,dc=org
   ldap group suffix = ou=groups
   ldap machine suffix = ou=computers
   ldap user suffix = ou=users
   ldap ssl = off
   idmap config *: backend = tdb
   idmap config *: range = 3000-7999
   idmap config SAMBA: backend = ldap
   idmap config SAMBA: range = 10000-19999
   idmap config SAMBA: ldap_url = ldap://localhost/
   idmap config SAMBA: ldap_base_dn = ou=idmap,dc=samba,dc=org
   idmap config SAMBA: ldap_user_dn = cn=admin,dc=samba,dc=org
   ldap delete dn = yes
   ldap password sync = yes
   winbind use default domain = yes
   wins support = yes
   template shell = /bin/bash
   template homedir = /home/%U
   obey pam restrictions = yes
   logon home
=================
   logon path
=================
   dns proxy = no
   printing = bsd
   printcap name = /dev/null
   store dos attributes = yes
   map read only = no
   map archive = no
   dos filetime resolution = yes
   fake directory create times = yes
   csc policy = disable

A quick setup 
------------------------

Do not run any daemon until told, use a fresh install or
wipe out any previously created tdb before starting.

To quickly set up and test this feature follow these steps as root:

Configure and run your ldap server, you may use the above base ldif
as a starting point. Make sure the defined ldap admin works
correctly.

Configure smb.conf

Add the ldap admin password to the required samba secrets databases:
     smbpasswd -w secret
     net idmap set secret '*' secret
     net idmap set secret 'SAMBA' secret

Start winbindd only.
     /etc/init.d/winbindd start

Now provision the domain in ldap:

     net sam provision

This should result in:

 Checking for Domain Users group.
 Adding the Domain Users group.
 Checking for Domain Admins group.
 Adding the Domain Admins group.
 Check for Administrator account.
 Adding the Administrator user.
 Checking for Guest user.
 Adding the Guest user.
 Checking Guest's group.
 Adding the Domain Guests group.

If the the command returns:
"Unable to allocate a new gid to create Domain Admins group" then make sure
idmap is properly configured and the idmap range is large enough to hold the
predicted number of unique users+groups you will have in your system life time.
On a new system you should never get this error, if you have to
increase the idmap range in a production system, just increase the highest value,
NEVER change the lowest value.

Give a password to the newly created Administrator user enabling it.
     smbpasswd Administrator
    ew SMB password:
    etype new SMB password:

Now start also nmbd and smbd:
     /etc/init.d/nmbd start
     /etc/init.d/smbd start

Grant yourself some privileges:
     net rpc rights grant Administrator SeAddUsersPrivilege -U Administrator
     net rpc rights grant Administrator SeMachineAccountPrivilege -U Administrator

Managing your DB 
------------------------

At this point you will be able to connect from any windows client or use any samba tool to manage your users groups and machines with the Administrator user.

There is one problem (or not, depending on your view), you cannot log into the PDC via ssh or at the console etc using a Samba user stored in ldap.