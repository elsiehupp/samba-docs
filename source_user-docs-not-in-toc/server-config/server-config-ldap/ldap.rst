Samba & LDAP
    <namespace>0</namespace>
<last_edited>2018-11-09T19:40:19Z</last_edited>
<last_editor>Hortimech</last_editor>

389 Directory Server
------------------------

389 Directory Server has a wiki page on integrating with Samba. See 
[http://www.port389.org/docs/389ds/howto/howto-samba.html#sts=Samba%20&%20389%20Directory%20Server%C2%A0Integration]

OpenLDAP
------------------------

OpenLDAP is a widely used LDAP server in Linux environments. It can do a lot of things for all sorts of needs. Whilst Samba, running in 'classic' mode, can use it as a storage backend for accounts and mappings from Windows (TM) SIDs to unix UID/GID, it cannot be used if Samba is running as an AD DC. OpenLDAP can also be used to control mail delivery and other things. You can find OpenLDAP at `http://www.openldap.org`

OpenDS/OpenDJ
------------------------

OpenDJ is a extension of the Sun Microsystems' initiated OpenDS project as LDAPv3 compliant directory service developed for the Java platform. OpenDJ comes with a plugin which syncronizes Samba password attributes with the LDAP password attribute - more information can be found `http://blog.profiq.cz/2011/08/15/opendj-integration-with-samba/ on the plugin page`]. OpenDJ is available at: :SLLAASSHHSSLLAAS:DOOTTforgerock.org/`.

GUI Tools
------------------------

LAM [http://lam.sf.net/] - A web frontend to maintain accounts for unix and samba in an ldap server

LUMA [http://luma.sourceforge.net/] - LDAP browser, utility and more (Python/QT)

phpldapadmin [http://phpldapadmin.sf.net/]

Webmin `http://www.webmin.com`

ldapadmin `http://ldapadmin.sf.net` - Ldap Admin is free Win32 application for managing LDAP directories.

Apache Directory Studio `http://directory.apache.org/studio/` - Apache Directory Studio is an Eclipse based LDAP Browser and Directory client.  It includes a LDAP Browser, LDIF Editor, and Schema Editor

Integration into the system
------------------------

===============================
Introduction
===============================

------------------------

It is not that easy to integrate an LDAP directory into a *NIX system. The most anoying thing is that samba relies on the host OS and thus needs uid/gid for a user. You can store these information in your passwd, but for a consistent setup it is recommended to store these things in your LDAP server too.

Given these you have to configure your OS to use an LDAP server as passwd backend and then let samba use the same as own password storage. This is valid for user, group and machine accounts.

===============================
Setting up PAM and NSS to use LDAP
===============================

------------------------

It is quite simple but need changes at two places. You have to change nsswitch.conf and add the keyword 'ldap' to the passwd: and group: :OTT Then you have to install the library libnss_ldap from your package system.

In the newer releases (as of 03/2006) pam_unix2 can use ldap servers for lookup. You just have to enable it  in /etc/security/pam_unix2.conf. But it needs the /etc/ldap.conf file to get all needed parameters for ldap lookups.

===============================
Let Samba use LDAP
===============================

------------------------

Just specify 
* passdb backend = ldapsam:AASS:HH<your-hostname>
* ldap suffix = 

and optionally
* ldap admin dn

then run smbpasswd -w <ldap-secret> to let samba know the password for the admin dn
Mapping the well known Windows groups to unix groups=
===============================

------------------------

net groupmap add --unixgroup=<unix-group> --ntgroup="<windows-group>"

===============================
Tools to manage accounts
===============================

------------------------

A good tool set for managing accounts are the smbldap-tools. These tools allow the management of most components which are needed for the accounts. Especially user, machine and group management. The real fortunate is that these tools can be set as scripts in smb.conf parameters.
Preparation=
===============================

------------------------

Before you can use the smbldap-tools you have to configure /etc/smbldap-tools/smbldap.conf and /etc/smbldap-tools/smbldap_bind.conf. All entries have to fit your ldap DIT.

After you have done the correct settings, you have to run smbldap-populate. This is mandantory since the base DIT has to be set up properly.
Add the first user=
===============================

------------------------

smbldap-useradd -a <username>
Add the first machine=
===============================

------------------------

smbldap-useradd -w <machine>

===============================
Debugging your setup
===============================

------------------------

The essential thing to get your setup working is **not** to do all stuff at the same time. It is better to have a basic but working setup. This setup can be migrated to your desired setup later. Doing it this way ensure that the basic setup works.

===============================
Traps
===============================

------------------------

* Many errors occure due to simple typing errors of the dn or DIT components. IE look at how your user accounts are created: are they uid=<user> or cn=<user>? This little difference leads to misfunction of your setup. 
* Machine accounts also needs unix uid/gid. This is needed cause samba looks up those accounts like a normal user account. 
* Your Unix machine can't look up the uid/gid from your ldap server: this can be really simply be debugged with the 'getent passwd' and 'getent group' command (if you have getent)

----
--`User:` 15:09,  April 2006 (CDT)

----
`Category Domains`