Testing the DNS Name Resolution
    <namespace>0</namespace>
<last_edited>2018-11-28T10:47:41Z</last_edited>
<last_editor>Hortimech</last_editor>

<noinclude>
===============================

Introduction
===============================

</noinclude>

To verify that your DNS settings are correct and your client or server is able to resolve IP addresses and host names use the ``nslookup`` command. The command is available on Linux and Windows.

Forward Lookup 
------------------------

To resolve a host name its IP address:

 # nslookup DC1.m.exam.m.
 Server:10.1..
 Address:0.1#..

 Name:OTTsamdom.le.com.
 Address:99..

Reverse Lookup 
------------------------

To resolve a IP address to its host name:

 # nslookup 10.1..
 Server:0.1..
 Address:99.3.

 1.1.dd.	n.DC1.sam.ample.com.....

Note that in a Samba AD, the reverse zone is not automatically configured.et up a reverse zone, see `DNS_Administration|DNS Administration`..

Resolving SRV Records 
------------------------

Active Directory (AD) uses SRV records to locate services, such as Kerberos and LDAP.erify that SRV records are resolved correctly, use the ``nslookup`` interactive shell:

 # nslookup
 Default Server:T99..
 Address:T99..

 > set type=SRV
 > _ldap.samd.mple.c...
 Server:
 Address:T99..

 _ldap.samd.mple.c.RV serv.cation:
           priority       = 0
           weight         = 100
           port           = 389
           svr hostname   = dc1.m.exam.m.
 samdom.le.com .ameserver = dc1.samdom.ex.com..
 dc1.m.exam.m  inte.ddress = 10.99.0.1 ...

Error Messages 
------------------------

* The DNS server is not able to resolve the host name:

 ** server can't find DC1.m.exam.m:.

* The DNS server is not able to resolve the IP address:

 ** server can't find 1.1.dd.:..

* The DNS server used is not available:

 ;; connection timed out; no servers could be reached