Verifying and Creating a DC DNS Record
    <namespace>0</namespace>
<last_edited>2017-07-16T16:57:17Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

===============================

Introduction
===============================

If you join a domain controller (DC) to an Active Directory (AD), certain DNS records must exist in the AD DNS zone to enable the DC to work and replicate correctly.

You must create the DC's A and objectGUID CNAME record manually, if you join:
* a Samba DC, that runs 4.6 or earlier, to an existing AD
* a Windows DC to an existing Samba AD

=================
Domain Controller A Record
=================

To verify that the A record for domain controller (DC) ``DC2`` using the IP address ``10.99.0.2`` is created in the ``samdom.example.com`` DNS zone, run on a domain member:

 # host -t A <u>DC2</u>.samdom.example.com.
 DC2.samdom.example.com has address 10.99.0.2

If the record is missing, the following error is displayed:

 # host -t A <u>DC2</u>.samdom.example.com.
 Host DC2.samdom.example.com. not found: 3(NXDOMAIN)

To manually add the A record for ``DC2`` to the DNS server running on ``DC1``, run:

 # samba-tool dns add DC1 samdom.example.com <u>DC2</u> A <u>10.99.0.2</u> -Uadministrator
 Password for [SAMDOM\administrator]: 
 Record added successfully

The directory replication automatically distributes the new record to all DCs.

=================
The objectGUID CNAME Record
=================

Determining a DCs objectGUID 
------------------------

To determine the objectGUID value of the joined domain controller ``DC2``, run on an existing DC:

 # ldbsearch -H /usr/local/samba/private/sam.ldb '(invocationId=*)' --cross-ncs objectguid
 # record 1
 dn: CN=NTDS Settings,CN=<u>DC2</u>,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com
 objectGUID: <u>df4bdd8c-abc7-4779-b01e-4dd4553ca3e9</u>

 # record 2
 dn: CN=NTDS Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=samdom,DC=example,DC=com
 objectGUID: 4a6bd92a-6612-4b15-aa8c-9ec371e8994f

 # returned 2 records
 # 2 entries
 # 0 referrals

Locate the objectGUID of the DC in the output.

Verifying and Creating the objectGUID Record 
------------------------

To verify that the CNAME objectGUID record for domain controller (DC) ``DC2`` exists in the ``_msdcs.samdom.example.com`` DNS zone, run on a domain member:

 # host -t CNAME <u>df4bdd8c-abc7-4779-b01e-4dd4553ca3e9</u>._msdcs.samdom.example.com.
 df4bdd8c-abc7-4779-b01e-4dd4553ca3e9._msdcs.samdom.example.com is an alias for DC2.samdom.example.com.

If the record is missing, the following error is displayed:

 # host -t CNAME <u>df4bdd8c-abc7-4779-b01e-4dd4553ca3e9</u>._msdcs.samdom.example.com.
 Host df4bdd8c-abc7-4779-b01e-4dd4553ca3e9._msdcs.samdom.example.com. not found: 3(NXDOMAIN)

To manually add the objectGUID CNAME record to the DNS server running on ``DC1``, use the objectGUID determined in section `#Determining_a_DCs_objectGUID|Determining a DCs objectGUID` and run:

 # samba-tool dns add DC1 _msdcs.samdom.example.com <u>df4bdd8c-abc7-4779-b01e-4dd4553ca3e9</u> CNAME <u>DC2</u>.samdom.example.com -Uadministrator
 Password for [SAMDOM\administrator]: 
 Record added successfully

The directory replication automatically distributes the new record to all DCs.