Linux and Unix DNS Configuration
    <namespace>0</namespace>
<last_edited>2018-11-28T10:49:48Z</last_edited>
<last_editor>Hortimech</last_editor>

<noinclude>
__TOC__

===============================

Introduction
===============================

</noinclude>
Active Directory (AD) uses DNS in the background, to locate other DCs and services, such as Kerberos. Thus AD domain members and servers must be able to resolve the AD DNS zones.

The following describes how to manually configure Linux clients to use DNS servers. If you are running a DHCP server providing DNS settings to your client computers, configure your DHCP server to send the IP addresses of your DNS servers.

Configuring the /etc/resolv.conf 
------------------------

Set the DNS server IP and AD DNS domain in your ``/etc/resolv.conf``. For example:

 nameserver 10.99.0.1
 search samdom.example.com

Some utilities, such as NetworkManager can overwrite manual changes in that file. See your distribution's documentation for information about how to configure name resolution permanently.

Testing DNS resolution 
------------------------

{{:he_DNS_Name_Resolution}}

----
`Category:rectory`
`Category:mbers`