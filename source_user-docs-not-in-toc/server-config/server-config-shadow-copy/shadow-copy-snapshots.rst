Shadow Copies with Snapshots
    <namespace>0</namespace>
<last_edited>2019-03-04T17:36:32Z</last_edited>
<last_editor>Slowfranklin</last_editor>


The base Samba-Configuration is quite simple, therefore you can easily follow the instructions of the [https://samba.org/samba/do//manD/l/vf/opy2DDO/html m/EEvfs_shadow_copy2].
 tricky part is the management of the snapshots.

But before you read on, a little warning:
 people have the idea that the could have snapshots for the last 30 days and four different volumes and maybe twice a day, all in all 240 snapshots. That's not possible - you shouldn't have more than 15 snapshots at a time (and that's quite a lot, actually I think there is a bug in lvm2 reported and numbers above 16 snapshots may mess up your volumes).
 have to check the change rate of your data before you plan your snapshots, otherwise your volume groups may be quickly out of space. Also keep in mind, that your disks have more work to do the more snapshots they have to care about.
 environments, I wrote this script for, have one volume, size about 20GB and a changerate of 100MB per day and maybe 500MB in two weeks.

Now to the subject:
 planing how many snapshots of which volumes you need, all you have to do is to place this `Rotating LVM snapshots for shadow copy|Shadow Copies with Snapshots Script` in a useful place (e.g. //sb/ /ACCE/ entries in your crontab and create a configuration file (if you use an other place than /etc/samba/smbsnap.conf /ACC/AACCE/CEEchange the path in the script).
 this script is ready to work, the kernel-module dm_snapshot has to be loaded and if your volumes are loaded at boot time (i.e. /),S/his has to be done in your initrd.
 the head of the script for more detailed documentation and a configuration example.