Samba4/HOWTO/Setup a Single Sign-On Website
    <namespace>0</namespace>
<last_edited>2016-10-22T22:50:24Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

Goal
------------------------

This Howto aims to show a clean way to setup a website that provides:

* SSL encryption (HTTPS) by using a self-signed certificate
* single sign-on from within your Samba4 domain
* optional login from outside (user/password prompt)
* full Kerberos 5 authentication security 

The type of setup shown here is very minimal. It is intended to get you a basic idea of how the process works.

Usecase
------------------------

You may provide a secured intranet website for your clients, hosting private content on a per-user basis.

It´s also possible to develop a web based application for domain management, using Kerberos/LDAP and Samba´s Python API. More information on this topic may be provided in another document.

Requirements
------------------------

* Samba4 setup as domain controller
* a working DNS configuration
* a working Kerberos configuration

It's recommended to follow the setup process described at `Setting_up_Samba_as_an_Active_Directory_Domain_Controller`.

Setup 
------------------------

Apache2
------------------------

You need a web server that hosts your site. Apache2 is widely spread these days and available as software package in (almost) all linux-distributions.

To install apache2, mod_ssl and mod_auth_kerb run:

**Debian/Ubuntu**

.. code-block::

      # apt-get install apache2 libapache2-mod-auth-kerb
     a2enmod ssl auth_kerb

Setup a minimal ssl-site

NOTE:t need to use a secured site to get this example working, but in production environments it's highly recommended for security reasons.
A minimal configuration might look like this:
   
----
<tt>**file:HHetc/apache2/sites-available/default-ssl**</tt>

.. code-block::

      <IfModule mod_ssl.c>
    VirtualHost _default_:
      ServerAdmin webmaster@localhost
      DocumentRoot /var/www
      
      <Directory />
          Options FollowSymLinks
          AllowOverride None
      </Directory>
      
      <Directory /var/www/>
          Options Indexes FollowSymLinks MultiViews
          AllowOverride None
          Order allow,deny
          allow from all
      </Directory>   
      
      #########################################################
      # add a private directory using kerberos authentication #
      #########################################################
      
      <Directory /var/www/private>
          AuthType Kerberos
          AuthName "Intranet Login"
          KrbMethodNegotiate on
          KrbMethodK5Passwd on
          KrbVerifyKDC on
          KrbSaveCredentials off
          # our keytab
          Krb5Keytab  /etc/apache2/http.keytab
          # specify your realm (upper case - like the krb5.conf)
          KrbAuthRealms YOUR.REALM
          Require valid-user
      </Directory>
      # rest of file
      ...
----

Active Directory
------------------------

Windows Client(s)
------------------------

Troubleshooting ==