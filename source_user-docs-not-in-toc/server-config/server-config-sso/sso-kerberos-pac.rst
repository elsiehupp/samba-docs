Kerberos PAC
    <namespace>0</namespace>
<last_edited>2020-04-23T05:39:05Z</last_edited>
<last_editor>Abartlet</or>
==============================

ocumentation
===============================

In Kerberos, the ticket and PAC (Privilege Account Certificate), described in [https://t.com/en-us/open/ows_protocol/HHpac/166/SHHc863-41/9c23-e/2 MS-PAC], is passed from the KDC to the target server in the Kerberos ticket. 

A too-large PAC
------------------------

There are limits on how big a PAC can be, [https://soft.com/en-us/help/oblemsDDAASS/SSHH/AASSHH/tion-when-a-user-belongs-to-many-grou particularly when connecting to a Windows server].  The MaxTokenSize parameter on windows may be important in obtaining full interoperability for a larger PAC.

Other operating systems and servers may have limits on other places - in SMBv1 Samba must collect parts of the Kerberos ticket in multiple SPNEGO round-trips to obtain the full PAC, for example.  

The `tokenGroups#Limits on final group membership size|Windows simits on final group membership size` may also apply.

===============================
Obtaining the current Kerberos PAC for a user
===============================

Running
 net ads kerberos pac dump -U$USERNAME

will show the current PAC, eg:

 The Pac:     pac_data_ctr->pac_data: struct PAC_DATA
        num_buffers              : 0x00000005 (5)
        version                  : 0x00000000 (0)
        buffers: ARRAY(5)
            buffers: struct PAC_BUFFER
                type                     : PAC_TYPE_LOGON_INFO (1)
                _ndr_size                : 0x000001d0 (464)
                info                     : *
                    info                     : union PAC_INFO(case 1)
                    logon_info: struct PAC_LOGON_INFO_CTR
                        info                     : *
                            info: struct PAC_LOGON_INFO
                                info3: struct netr_SamInfo3
                                    base: struct netr_SamBaseInfo
                                        logon_time               : Thu Apr 23 05:02:46 AM 2020 UTC
                                        logoff_time              : Thu Sep 14 02:48:05 AM 30828 UTC
                                        kickoff_time             : Thu Sep 14 02:48:05 AM 30828 UTC
                                        last_password_change     : Thu Apr 23 03:20:23 AM 2020 UTC
                                        allow_password_change    : Fri Apr 24 03:20:23 AM 2020 UTC
                                        force_password_change    : Thu Jun  4 03:20:23 AM 2020 UTC
                                        account_name: struct lsa_String
                                            length                   : 0x001a (26)
                                            size                     : 0x001a (26)
                                            string                   : *
                                                string                   : 'Administrator'
                                        full_name: struct lsa_String
                                            length                   : 0x0000 (0)
                                            size                     : 0x0000 (0)
                                            string                   : *
                                                string                   : *
                                        logon_script: struct lsa_String
                                            length                   : 0x0000 (0)
                                            size                     : 0x0000 (0)
                                            string                   : *
                                                string                   : *
                                        profile_path: struct lsa_String
                                            length                   : 0x0000 (0)
                                            size                     : 0x0000 (0)
                                            string                   : *
                                                string                   : *
                                        home_directory: struct lsa_String
                                            length                   : 0x0000 (0)
                                            size                     : 0x0000 (0)
                                            string                   : *
                                                string                   : *
                                        home_drive: struct lsa_String
                                            length                   : 0x0000 (0)
                                            size                     : 0x0000 (0)
                                            string                   : *
                                                string                   : *
                                        logon_count              : 0x0007 (7)
                                        bad_password_count       : 0x0000 (0)
                                        rid                      : 0x000001f4 (500)
                                        primary_gid              : 0x00000201 (513)
                                        groups: struct samr_RidWithAttributeArray
                                            count                    : 0x00000006 (6)
                                            rids                     : *
                                                rids: ARRAY(6)
                                                    rids: struct samr_RidWithAttribute
                                                        rid                      : 0x00000201 (513)
                                                        attributes               : 0x00000007 (7)
                                                               1: SE_GROUP_MANDATORY       
                                                               1: SE_GROUP_ENABLED_BY_DEFAULT
                                                               1: SE_GROUP_ENABLED         
                                                               0: SE_GROUP_OWNER           
                                                               0: SE_GROUP_USE_FOR_DENY_ONLY
                                                               0: SE_GROUP_INTEGRITY       
                                                               0: SE_GROUP_INTEGRITY_ENABLED
                                                               0: SE_GROUP_RESOURCE        
                                                            0x00: SE_GROUP_LOGON_ID         (0)
                                                    rids: struct samr_RidWithAttribute
                                                        rid                      : 0x00000200 (512)
                                                        attributes               : 0x00000007 (7)
                                                               1: SE_GROUP_MANDATORY       
                                                               1: SE_GROUP_ENABLED_BY_DEFAULT
                                                               1: SE_GROUP_ENABLED         
                                                               0: SE_GROUP_OWNER           
                                                               0: SE_GROUP_USE_FOR_DENY_ONLY
                                                               0: SE_GROUP_INTEGRITY       
                                                               0: SE_GROUP_INTEGRITY_ENABLED
                                                               0: SE_GROUP_RESOURCE        
                                                            0x00: SE_GROUP_LOGON_ID         (0)
                                                    rids: struct samr_RidWithAttribute
                                                        rid                      : 0x0000023c (572)
                                                        attributes               : 0x00000007 (7)
                                                               1: SE_GROUP_MANDATORY       
                                                               1: SE_GROUP_ENABLED_BY_DEFAULT
                                                               1: SE_GROUP_ENABLED         
                                                               0: SE_GROUP_OWNER           
                                                               0: SE_GROUP_USE_FOR_DENY_ONLY
                                                               0: SE_GROUP_INTEGRITY       
                                                               0: SE_GROUP_INTEGRITY_ENABLED
                                                               0: SE_GROUP_RESOURCE        
                                                            0x00: SE_GROUP_LOGON_ID         (0)
                                                    rids: struct samr_RidWithAttribute
                                                        rid                      : 0x00000206 (518)
                                                        attributes               : 0x00000007 (7)
                                                               1: SE_GROUP_MANDATORY       
                                                               1: SE_GROUP_ENABLED_BY_DEFAULT
                                                               1: SE_GROUP_ENABLED         
                                                               0: SE_GROUP_OWNER           
                                                               0: SE_GROUP_USE_FOR_DENY_ONLY
                                                               0: SE_GROUP_INTEGRITY       
                                                               0: SE_GROUP_INTEGRITY_ENABLED
                                                               0: SE_GROUP_RESOURCE        
                                                            0x00: SE_GROUP_LOGON_ID         (0)
                                                    rids: struct samr_RidWithAttribute
                                                        rid                      : 0x00000207 (519)
                                                        attributes               : 0x00000007 (7)
                                                               1: SE_GROUP_MANDATORY       
                                                               1: SE_GROUP_ENABLED_BY_DEFAULT
                                                               1: SE_GROUP_ENABLED         
                                                               0: SE_GROUP_OWNER           
                                                               0: SE_GROUP_USE_FOR_DENY_ONLY
                                                               0: SE_GROUP_INTEGRITY       
                                                               0: SE_GROUP_INTEGRITY_ENABLED
                                                               0: SE_GROUP_RESOURCE        
                                                            0x00: SE_GROUP_LOGON_ID         (0)
                                                    rids: struct samr_RidWithAttribute
                                                        rid                      : 0x00000208 (520)
                                                        attributes               : 0x00000007 (7)
                                                               1: SE_GROUP_MANDATORY       
                                                               1: SE_GROUP_ENABLED_BY_DEFAULT
                                                               1: SE_GROUP_ENABLED         
                                                               0: SE_GROUP_OWNER           
                                                               0: SE_GROUP_USE_FOR_DENY_ONLY
                                                               0: SE_GROUP_INTEGRITY       
                                                               0: SE_GROUP_INTEGRITY_ENABLED
                                                               0: SE_GROUP_RESOURCE        
                                                            0x00: SE_GROUP_LOGON_ID         (0)
                                        user_flags               : 0x00000000 (0)
                                               0: NETLOGON_GUEST           
                                               0: NETLOGON_NOENCRYPTION    
                                               0: NETLOGON_CACHED_ACCOUNT  
                                               0: NETLOGON_USED_LM_PASSWORD
                                               0: NETLOGON_EXTRA_SIDS      
                                               0: NETLOGON_SUBAUTH_SESSION_KEY
                                               0: NETLOGON_SERVER_TRUST_ACCOUNT
                                               0: NETLOGON_NTLMV2_ENABLED  
                                               0: NETLOGON_RESOURCE_GROUPS 
                                               0: NETLOGON_PROFILE_PATH_RETURNED
                                               0: NETLOGON_GRACE_LOGON     
                                        key: struct netr_UserSessionKey
                                            key: ARRAY(16): <REDACTED SECRET VALUES>
                                        logon_server: struct lsa_StringLarge
                                            length                   : 0x0008 (8)
                                            size                     : 0x000a (10)
                                            string                   : *
                                                string                   : 'ADDC'
                                        logon_domain: struct lsa_StringLarge
                                            length                   : 0x0010 (16)
                                            size                     : 0x0012 (18)
                                            string                   : *
                                                string                   : 'ADDOMAIN'
                                        domain_sid               : *
                                            domain_sid               : S-1-5-21-4023018537-2373006774-1847616786
                                        LMSessKey: struct netr_LMSessionKey
                                            key: ARRAY(8): <REDACTED SECRET VALUES>
                                        acct_flags               : 0x00000010 (16)
                                               0: ACB_DISABLED             
                                               0: ACB_HOMDIRREQ            
                                               0: ACB_PWNOTREQ             
                                               0: ACB_TEMPDUP              
                                               1: ACB_NORMAL               
                                               0: ACB_MNS                  
                                               0: ACB_DOMTRUST             
                                               0: ACB_WSTRUST              
                                               0: ACB_SVRTRUST             
                                               0: ACB_PWNOEXP              
                                               0: ACB_AUTOLOCK             
                                               0: ACB_ENC_TXT_PWD_ALLOWED  
                                               0: ACB_SMARTCARD_REQUIRED   
                                               0: ACB_TRUSTED_FOR_DELEGATION
                                               0: ACB_NOT_DELEGATED        
                                               0: ACB_USE_DES_KEY_ONLY     
                                               0: ACB_DONT_REQUIRE_PREAUTH 
                                               0: ACB_PW_EXPIRED           
                                               0: ACB_TRUSTED_TO_AUTHENTICATE_FOR_DELEGATION
                                               0: ACB_NO_AUTH_DATA_REQD    
                                               0: ACB_PARTIAL_SECRETS_ACCOUNT
                                               0: ACB_USE_AES_KEYS         
                                        sub_auth_status          : 0x00000000 (0)
                                        last_successful_logon    : NTTIME(0)
                                        last_failed_logon        : NTTIME(0)
                                        failed_logon_count       : 0x00000000 (0)
                                        reserved                 : 0x00000000 (0)
                                    sidcount                 : 0x00000000 (0)
                                    sids                     : NULL
                                resource_groups: struct PAC_DOMAIN_GROUP_MEMBERSHIP
                                    domain_sid               : NULL
                                    groups: struct samr_RidWithAttributeArray
                                        count                    : 0x00000000 (0)
                                        rids                     : NULL
                _pad                     : 0x00000000 (0)
            buffers: struct PAC_BUFFER
                type                     : PAC_TYPE_LOGON_NAME (10)
                _ndr_size                : 0x00000024 (36)
                info                     : *
                    info                     : union PAC_INFO(case 10)
                    logon_name: struct PAC_LOGON_NAME
                        logon_time               : Thu Apr 23 05:02:46 AM 2020 UTC
                        size                     : 0x001a (26)
                        account_name             : 'Administrator'
                _pad                     : 0x00000000 (0)
            buffers: struct PAC_BUFFER
                type                     : PAC_TYPE_UPN_DNS_INFO (12)
                _ndr_size                : 0x0000008e (142)
                info                     : *
                    info                     : union PAC_INFO(case 12)
                    upn_dns_info: struct PAC_UPN_DNS_INFO
                        upn_name_size            : 0x004a (74)
                        upn_name                 : *
                            upn_name                 : 'Administrator@addom.samba.example.com'
                        dns_domain_name_size     : 0x002e (46)
                        dns_domain_name          : *
                            dns_domain_name          : 'ADDOM.SAMBA.EXAMPLE.COM'
                        flags                    : 0x00000001 (1)
                               1: PAC_UPN_DNS_FLAG_CONSTRUCTED
                _pad                     : 0x00000000 (0)
            buffers: struct PAC_BUFFER
                type                     : PAC_TYPE_SRV_CHECKSUM (6)
                _ndr_size                : 0x00000010 (16)
                info                     : *
                    info                     : union PAC_INFO(case 6)
                    srv_cksum: struct PAC_SIGNATURE_DATA
                        type                     : 0x00000010 (16)
                        signature                : DATA_BLOB length=12
 [0000] 5C 5C 54 AF 6C EA E2 4D   65 B0 A9 4C               \\T.l..M e..L
                _pad                     : 0x00000000 (0)
            buffers: struct PAC_BUFFER
                type                     : PAC_TYPE_KDC_CHECKSUM (7)
                _ndr_size                : 0x00000010 (16)
                info                     : *
                    info                     : union PAC_INFO(case 7)
                    kdc_cksum: struct PAC_SIGNATURE_DATA
                        type                     : 0x00000010 (16)
                        signature                : DATA_BLOB length=12
 [0000] 20 D5 27 D6 CA 0E 9F 64   C4 AD 22 84                .'....d ..".
                _pad                     : 0x00000000 (0)

This example is for the same user as in `tokenGroups|the examples about tokenGroups`.  While the information transferred is the same, the unprocessed PAC is much more fiddly to manually parse.