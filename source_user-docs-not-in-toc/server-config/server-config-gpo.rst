GPO Backup and Restore
    <namespace>0</namespace>
<last_edited>2019-03-21T01:32:19Z</last_edited>
<last_editor>Garming</last_editor>

Overview 
------------------------

**Added in Samba version *4.10*

GPO creation and management has a number of issues in Samba still. Synchronization of GPO often causes problems with access permissions due to missing AD file replication protocols. In order to start from scratch, building a GPO can be incredibly time consuming (as it mostly requires a GUI editor) and so allowing a backup to be restored (to a new GPO) makes this a lot easier. This makes it much easier to duplicate GPO and make slight variations, automate GPO settings and also inspect what settings are being used without a Windows machine. In particular, it allows the import of GPO across domains by generalizing a GPO and returning an XML entity file that can be edited and fed back into the tool.

The backup process reads the GPO configuration files and stores them as generalized XML files, as well as storing a binary duplicate in case the restoration from XML fails. It's possible to directly edit these files although corruption is quite easy in this way.

In Windows, it is possible to backup (and restore) a GPO using the following Powershell commands: Backup-GPO, Restore-GPO or alternatively by using the GUI group policy editor. There are a few problems with using these tools. The output is a mix of storage formats, dependencies and has general inconsistencies which make it difficult for an admin to understand what is happening and in particular, manually edit the contents of their GPOs in the case of errors. Windows supports importing GPO using migration tables, but there are a number of file formats, where information is stored. These files and GPO settings are not supported by these tables for replacing names and other AD domain-specific data when performing migrations. By having simple XML, and a human readable formats which are easily machine parseable and allows substitution, it makes the process much more transparent (and hopefully, eventually far more complete).

How to configure it
------------------------

1. Setup your GPO using the usual tools (Windows Group Policy Management)

2. Determine the GPO GUID name e.g. from LDAP, samba-tool gpo listall 

3. Run the samba-tool gpo backup command

<pre>samba-tool gpo backup <GPO GUID></pre>

4. Run the samba-tool gpo restore command

<pre>samba-tool gpo restore <new GPO name> <path generated from backup></pre>

 Generalizing a GPO (to use in a different domain) ==== 

<pre>samba-tool gpo backup --generalize --entities=<output path></pre>

The generated entities are in the standard XML DTD header format, and are used likewise.

There are currently three types of entities: user ID (SID, usernames), ACL (SDDL), share paths (\\unc paths)

== Example (entities) output ===
===============================

------------------------

.. code-block::

    <!ENTITY SAMBA__NETWORK_PATH__82419dafed126a07d6b96c66fc943735__ "\\realm.com">
<!ENTITY SAMBA__NETWORK_PATH__0484cd41ded45a0728333a9c5e5ef619__ "\\test">
<!ENTITY SAMBA____SDDL_ACL____4ce8277be3f630300cbcf80a80e21cf4__ "D:KA;;;BA)(A;CIIO;KA;;;CO)(A;CI;KR;;;S-1-0-0)">
<!ENTITY SAMBA____USER_ID_____d0970f5a1e19cb803f916c203d5c39c4__ "*S-1-5-21-3075996733-1131693954-1131023892-501">
<!ENTITY SAMBA____USER_ID_____7b7bc2512ee1fedcd76bdc68926d4f7b__ "Guest">

In the backup folder, the generated XML files will now contain XML entities to replace certain strings from the directory. If you are migrating a GPO across a domain, you must inspect these entities and edit them for appropriate replacements. 

When generalizing a GPO, these are handled as special cases and are not complete, although Windows and migrating using their equivalent migration tables also has problems. In some cases, you may wish to manually replace strings with entities which works as you would expect (this isn't possible for certain file types on Windows, but is possible with this tool).

===============================
 Restoring with an entities file
------------------------

.. code-block::

    samba-tool gpo restore --entities=<new entities file>

As you can manually edit the contents of GPO files (and replace contents with entities), do note that settings may not be applied as you expect, so you should verify after you restore.

Known issues and limitations
------------------------

In Samba 4.10, there are these bugs currently open (no backports yet):

* [https://bugzilla.samba.org/show_bug.cgi?id=13825 BUG #13825]: :TINI files causes backup to fail
* [https://bugzilla.samba.org/show_bug.cgi?id=13806 BUG #13806]: :TINI failing with GPO names with accents
* [https://bugzilla.samba.org/show_bug.cgi?id=13627 BUG #13627]:  import doesn't enable the GPO immediately

This tool is largely independent to many Samba components. Building from source and running the latest Git master to backup and restore fixes these bugs correctly.

 GPE.INI files causes backup to fail =
===============================

------------------------

Currently, if a GPE.INI file is created via RSAT, the backup no longer works. The only likely workaround would be to remove the GPE.INI file temporarily and copy it to its final location manually.

It would be recommended to use Samba master until this patch is backported.

 GPT.INI failing with GPO names with accents =
===============================

------------------------

Currently, the only workaround for this bug appears to be avoiding setting a display name with non-ASCII characters. Removing the display name from the GPT.INI manually may also be an option (but note, this hasn't been tested).

In master, the name is interpreted as Latin-1 and is eventually removed entirely in any restored GPO (as the name appears to be sourced from LDAP and not this file).

 GPO import doesn't enable the GPO immediately =
===============================

------------------------

Currently, the gPCMachineExtensionNames and gPCUserExtensionNames values on a newly restored GPO are not being set. To workaround, querying over LDAP from the DC where the backup was made, copy these values to the new GPO.

In master, this is now automatically done by the tool (but a new backup must be generated).

Troubleshooting
------------------------

Manually inspecting output files and the resulting GPO in RSAT is probably the best way to ensure that the GPO work as intended. The generalization function for migrating GPO across domains is still incomplete as there are only a subset of files that support this feature in the tool.

Report any errors to Samba bugzilla, there are probably a number of files (and associated file formats) which have yet to be attempted with this tool.

 Known missing formats (and settings) =
===============================

------------------------

* aas files (software installation)
* Domain generalization of: xml files e.g. preferences, shortcuts
* admx status unknown

For Developers == 

How to test it
------------------------

.. code-block::

    make test TESTS=samba_tool.gpo

GPO import and export has a fallback mode to restore from binary files instead of from XML if any errors occur in the restore. Currently there isn't any way to turn this off, and so what should probably fail often doesn't.

Where the code is located
------------------------

The core parsing logic for GPO is in the python gp_parse module. 

The outer logic lives in netcmd/gpo.py (where the actual regex matching used to identify which parser is used) and the tests are all in the samba_tool gpo.py tests.

Note: The parser for PReg .pol files is auto-generated from IDL.

Reference Docs
------------------------

[MS-GPOD] and [MS-GPOD] describe an overview of how the overall Group Policy system works. Individual references to which file formats correspond to which parsers lives in the comments of each parser class in gp_parse.

`Category Feature`