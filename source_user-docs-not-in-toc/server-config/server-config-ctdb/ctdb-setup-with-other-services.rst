Setting up CTDB with other services
    <namespace>0</namespace>
<last_edited>2019-10-15T01:36:46Z</last_edited>
<last_editor>MartinSchwenke</last_editor>

===============================

Introduction
===============================

CTDB can be used to cluster services other than Samba and NFS.  CTDB comes with event scripts for handling some of these services.  Configuring some of these is described below.

**Note:** These services are not as well tested as the core Samba and NFS services.  Feel free to ask questions on mailing lists and/or provide patches.

=================
Prerequisites
=================

* `Basic CTDB configuration`
* `Setting up a cluster filesystem`
* `Configuring the CTDB recovery lock` (recommended)
* `Adding public IP addresses` (or some other failover/load balancing scheme)

=================
Common instructions
=================
nsswitch.conf and PAM configuration 
------------------------

Some Unix or Linux services need to be configured to authenticate users against the same backend being used for SMB.  In this case Samba's windbind component will need to be used.

For details, see `Setting up Samba as a Domain Member#Configuring the Name Service Switch | Configuring the Name Service Switch`
and  `Authenticating Domain Users Using PAM`.

On Red Hat style systems, a command like the following can be useful:

    authconfig --update --nostart \
      --enablewinbindauth --enablewinbind --enablekrb5 \
      --krb5kdc=<kdc>.<domain> \
      --krb5realm=<domain>

substituting appropriate values from ``&lt;kdc&gt;`` and ``&lt;domain&gt;``.

Restart CTDB 
------------------------

After a configuring CTDB to manage a new service it may be necessary to restart CTDB.

=================
FTP using vsftpd
=================

``vsftpd`` is an FTP server available on various Linux distributions and other platforms.

Configure CTDB to manage vsftpd 
------------------------

The vsftpd event script must be enabled:

    tdb event script enable legacy 40.vsftpd

CTDB will manage and start/stop/restart the vsftpd services, so the operating system should be configured so these are not started/stopped automatically.

Samba configuration 
------------------------

Users will require a shell and home directory to log in to the FTP server.

The following Samba global configuration parameters can be used to provide these:

    emplate shell = /bin/bash
    emplate homedir = /<shared storage>/homedir/%D/%U

A more secure solution could use something like ``rssh`` and a home directory that uses a ``chroot``:

    emplate shell = /usr/bin/rssh
    emplate homedir = /var/local/vsftp-chroot

The home directory must exist or the user will not be able to log in with FTP.

=================
Antivirus with ClamAV
=================

ClamAV is a free and open source antivirus solution.

Configure CTDB to manage clamd 
------------------------

The clamd event scripts must be enabled:

    tdb event script enable legacy 31.clamd

CTDB will manage and start/stop/restart the vsftpd services, so the operating system should be configured so these are not started/stopped automatically.

The ``CTDB_CLAMD_SOCKET`` script variable must be set in either ``script.options`` or ``31.clamd.options`` file.  For example:

    TDB_CLAMD_SOCKET=/var/run/clamd.socket

=================
iSCSI with tgt
=================

[http://stgt.sourceforge.net/ tgt] is a Linux SCSI target framework.

Integration with CTDB 
------------------------

Since the iSCSI Target is not clustered nor integrated with CTDB in the same sense Samba is, this implementation will only create a HA solution for iSCSI where each public address is assigned its own iSCSI target name and the LUNs that are created are only accessible through one specific target (i.e. one public address at a time).

**Note:** This feature ONLY works when public addresses are used. It is not supported and does not work with LVS.

Required software
------------------------

* On Debian style systems the relevant package is simply called ``tgt``
* On Red Hat style systems the relevant package is called ``scsi-target-utils``

CTDB will manage and start/stop/restart the required ``tgtd``, so the operating system should be configured so these are not started/stopped automatically.

CTDB configuration
------------------------

The ``CTDB_START_ISCSI_SCRIPTS`` script variable must be set in either ``script.options`` or ``70.iscsi.options`` file.  For example:

    TDB_START_ISCSI_SCRIPTS=/clusterfs/iscsi

This is a directory on shared storage where the scripts to start and configure the iSCSI service are held. There is one script for each public address named ``&lt;public address&gt;.sh``.

The iscsi event script must be enabled:

    tdb event script enable legacy 70.iscsi

Create a LUN backing file on the cluster filesystem
------------------------

Before exporting a LUN a backing file must be created in the cluster filesystem. This should not be a sparse file.  
While it is much quicker to create a sparse file if you want a 100GB file, SCSI has no concept of "disk full" so if you run out of backing space for the sparse file, the SCSI initiators will be "surprised" and "unhappy".

A 100MB LUN backing file for public IP address 10.1.1.1 could be created as follows:

    d if=/dev/zero of=/clusterfs/iscsi/10.1.1.1.lun bs=1K count=100K

Example 10.1.1.1.sh script
------------------------

    !/bin/sh

     This example shell script is used to configure an iSCSI target hosted on public IP address 10.1.1.1.
      #create a target
    gtadm --lld iscsi --op new --mode target --tid 1 -T iqn.2007-11.com.ctdb:iscsi.target.10.1.1.1
      #attach a lun
    gtadm --lld iscsi --op new --mode logicalunit --tid 1 --lun 1 -b /gpfs/iscsi/10.1.1.1.lun
      # no security, allow everyone to access this lun
    gtadm --lld iscsi --op bind --mode target --tid 1 -I ALL

``iqn.2007-11.com.ctdb:iscsi.target.10.1.1.1`` in the example above is the iSCSI name that is assigned to the target.  This is just an example, pick your own name!

See the documentation for the ``tgtadm`` command for more information.