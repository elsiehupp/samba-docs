Configuring the CTDB recovery lock
    <namespace>0</namespace>
<last_edited>2020-09-27T03:12:15Z</last_edited>
<last_editor>MartinSchwenke</last_editor>

=================
Goal
=================

Configure CTDB to use a recovery lock to avoid a "split-brain" situation.

=================
Prerequisites
=================

* `Setting up a cluster filesystem` (by default - see below)

=================
CTDB configuration
=================

The recovery lock, configured via the ``recovery lock`` option in the ``cluster`` section, provides important split-brain prevention and is usually configured to point to a lock file in the cluster filesystem.  See the RECOVERY LOCK section in [http://ctdb.samba.org/manpages/ctdb.7.html ctdb(7)] for more details.

For example:

    cluster]
    recovery lock = /clusterfs/.ctdb/reclock

If the clustered filesystem does not support locking on default, CTDB allows to replace this mechanism with a binary that would interact with the clustered filesystem directly. This binary will be launched on each node and the node that can lock the file (or alternative operation) will output "0" (holding lock), the other nodes will output "1" (contended). See the RECOVERY LOCK section in [http://ctdb.samba.org/manpages/ctdb.7.html ctdb(7)] for more info about  this mechanism. In the utils directory in the [https://github.com/samba-team/samba/tree/master/ctdb/utils Samba Github] repository additional examples can be found.