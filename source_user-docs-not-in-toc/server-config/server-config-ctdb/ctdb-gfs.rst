GFS CTDB HowTo
    <namespace>0</namespace>
<last_edited>2018-09-17T06:26:30Z</last_edited>
<last_editor>MartinSchwenke</last_editor>

===============================

Introduction
===============================

This is a step by step HowTo for setting up clustered Samba with CTDB on top of GFS2.

Warning: Out of date!!! 
------------------------

Please note that this guide is very old and doesn't contain the latest instructions for configuration CTDB and Samba.  For the parts covering CTDB and Samba specific configuration please follow the various sections under `CTDB and Clustered Samba`.

=================
Platform
=================

This HowTo covers Fedora Core 13 (or later), or Red Hat Enterprise Linux 6 (or later), on x86_64 platforms.The Red Hat cluster manager supports a minimum of two machines in a clustered environment. All nodes are required to run the exact same software versions unless otherwise mentioned.

All of the cluster nodes over which you plan to use GFS2 must be able to mount the same shared storage (Fibre Channel, iSCSI, etc.).

Fence device: Any installation must use some sort of fencing mechanism. GFS2 requires fencing to prevent data corruption. http://sources.redhat.com/cluster/wiki/FAQ/Fencing has more info about fencing. A Fence Device is not required for test clusters.

<font color="red">
/* This list of required and optional packages for clustered samba is not complete. Please help me in filling this in. I usually configure a yum repo and do a 'yum -y install cman lvm2-cluster gfs2-utils' for my cluster and it pulls everything in automatically. */
</font>

Required Packages 
------------------------

* cman (Red Hat cluster manager)
* clusterlib
* corosync
* corosynclib
* openais
* openaislib
* fence-agents
* gfs2-utils (Utilities to manage and administer a gfs2 filesystem.)
* samba
* samba-winbind-clients
* samba-common
* ctdb
* cifs-utils
* lvm2-cluster (Clustered Logical Volume Manager (clvm) to manage shared logical volumes.)
* ...

Optional Packages 
------------------------

* mrxvt (Multi-tabbed shell for simultaneous administration of cluster nodes.)

Depending on what method you use to access shared storage, additional packages might be needed. For example, you'll need <tt>aoetools</tt> if you plan to use ATAoE.

It is recommended that you use the <tt>yum</tt> package management tool to update/install these packages so you have all the necessary dependencies resolved automatically.
Something like, "<tt>yum install cman lvm2-cluster gfs2-utils ctdb samba</tt>"

=================
Configuration
=================

All configuration steps in this section are to be performed on all of the cluster nodes unless otherwise specified. Setting up one machine and copying the configs over to the other nodes is one way to do this if you're not using a cluster shell (like <tt>cssh</tt> or <tt>mrxvt</tt>) that can broadcast keystrokes to all the tabs/windows, each of which is connected to a different cluster node.

Cluster configuration 
------------------------

To set up a cluster, all you need is a cluster configuration xml file located at /etc/cluster/cluster.conf. A simple configuration for a 3 node cluster is as shown below.

Example:

<tt>

.. code-block::

    &lt;?xml version="1.0"?&gt;
&lt;cluster name="csmb" config_version="1"&gt;
    lt;clusternodes&gt;
    &lt;clusternode name="clusmb-01" nodeid="1"&gt;&lt;/clusternode&gt;
    &lt;clusternode name="clusmb-02" nodeid="2"&gt;&lt;/clusternode&gt;
    &lt;clusternode name="clusmb-03" nodeid="3"&gt;&lt;/clusternode&gt;
    lt;/clusternodes&gt;
&lt;/cluster&gt;
</tt>

The <tt>&lt;cluster name&gt;</tt> attribute is your name for the cluster. This needs to be unique among all the clusters you may have on the same network. As we will see later when configuring GFS2, this cluster name will also associate the filesystem with it.

For each <tt>&lt;clusternode&gt;</tt>, you need to specify a <tt>"name"</tt> which is also the hostname of the corresponding machine and the <tt>"nodeid"</tt>, which is numeric and unique to the nodes.

<u>Note:</u> For a two-node cluster, there's a special attribute that needs to be part of the cluster.conf. Add <tt>&lt;cman two_node="1" expected_votes="1"/&gt;</tt> within the <tt>&lt;cluster&gt;&lt;/cluster&gt;</tt> tags.

This example below adds a power fencing (apc) device to the configuration:

<tt>

.. code-block::

    &lt;?xml version="1.0"?&gt;
&lt;cluster name="csmb" config_version="1"&gt;
    lt;clusternodes&gt;
    &lt;clusternode name="clusmb-01" nodeid="1"&gt;
      &lt;fence&gt;
        &lt;method name="single"&gt;
        &lt;device name="clusmb-apc" switch="1" port="1"/&gt;
        &lt;/method&gt;
      &lt;/fence&gt;
    &lt;/clusternode&gt;
    &lt;clusternode name="clusmb-02" nodeid="2"&gt;
      &lt;fence&gt;
        &lt;method name="single"&gt;
        &lt;device name="clusmb-apc" switch="1" port="2"/&gt;
        &lt;/method&gt;
      &lt;/fence&gt;
    &lt;/clusternode&gt;
    &lt;clusternode name="clusmb-03" nodeid="3"&gt;
      &lt;fence&gt;
        &lt;method name="single"&gt;
        &lt;device name="clusmb-apc" switch="1" port="3"/&gt;
        &lt;/method&gt;
      &lt;/fence&gt;   
    &lt;/clusternode&gt;
    lt;/clusternodes&gt;
    lt;fencedevices&gt;
    &lt;fencedevice name="single" agent="fence_apc" ipaddr="192.168.1.102" login="admin" passwd="password"/&gt;
    lt;/fencedevices&gt;
&lt;/cluster&gt;
</tt>

Each <tt>&lt;clusternode&gt;</tt> has an associated <tt>&lt;fence&gt;</tt> attribute and a separate <tt>&lt;fencedevices&gt;</tt> tag within the root <tt>&lt;cluster&gt;</tt> tag that defines the fencing agent. http://sources.redhat.com/cluster/wiki/FAQ/Fencing has more information on configuring different fencing agents.

The <tt>cluster.conf</tt> manual page has more information on the various options available.

<tt><b>lvm2-cluster</b></tt> Configuration 
------------------------

Before using <tt>lvm2-cluster(clvmd)</tt> to manage your shared storage, make sure you edit the lvm configuration file at <tt>/etc/lvm/lvm.conf</tt> to change the <tt>locking_type</tt> attribute to <tt>"locking_type=3"</tt>. Type 3 uses built-in clustered locking.

GFS2 Configuration 
------------------------

When using <tt>clvm</tt>, you must bring up the cluster first (so <tt>clvmd</tt> is running) before you can create logical volumes and gfs2 filesystems. Please skip this step and move on to the next step <b>CTDB Configuration</b>. We will come back to this step later.

Use the lvm tools like <tt>pvcreate, vgcreate, lvcreate</tt> to create two logical volumes <tt>ctdb_lv</tt> and <tt>csmb_lv</tt> on your shared storage. <tt>ctdb_lv</tt> will store the shared ctdb state and needs to be 1GB in size. <tt>csmb_lv</tt> will hold the user data that will be exported via a samba share so size it accordingly. Note that creation of clustered volume groups and logical volumes is to be done on only one of the cluster nodes. After creation, a "<tt>service clvmd restart</tt>" on all the nodes will refresh this new information and all the nodes will be able to see the logical volumes you just created.

You will be configuring gfs2 filesystems on both these volumes.
The only thing you need to do for gfs2 is to run <tt>mkfs.gfs2</tt> to make the filesystem. <u>Note:</u> <tt>mkfs.gfs2</tt> is to be run on one cluster node only!

In this example, we've used a 100GB (<tt>/dev/csmb_vg/csmb_lv</tt>) gfs2 filesystem for the samba share and the 1GB (<tt>/dev/csmb_vg/ctdb_lv</tt>) gfs2 filesystem for ctdb state.

First, we will create the filesystem to host the samba share.

<tt>mkfs.gfs2 -j3 -p lock_dlm -t csmb:gfs2 /dev/csmb_vg/csmb_lv</tt>

<tt>-j</tt> : Specifies the number of journals to create in the filesystem. One journal per node, and we have 3 nodes.

<tt>-p</tt> : <tt>lock_dlm</tt> is the locking protocol gfs2 uses for inter-node communication.

<tt>-t</tt> : Is the lock table name and is of the format <tt>cluster_name:fs_name</tt>. Recall that our cluster name is <tt>"csmb"</tt> (from <tt>cluster.conf</tt>) and we use <tt>"gfs2"</tt> as the name for our filesystem.

The output of this command looks something like this:

<tt>.. code-block::

    This will destroy any data on /dev/csmb_vg/csmb_lv.
    t appears to contain a gfs2 filesystem.

Are you sure you want to proceed? [y/n] y

Device:                    /dev/csmb_vg/csmb_lv
Blocksize:                 4096
Device Size                100.00 GB (26214400 blocks)
Filesystem Size:           100.00 GB (26214398 blocks)
Journals:                  3
Resource Groups:           400
Locking Protocol:          "lock_dlm"
Lock Table:                "csmb:gfs2"
UUID:                      94297529-ABG3-7285-4B19-182F4F2DF2D7</tt>

Next, we will create the filesystem to host ctdb state information.

<tt>mkfs.gfs2 -j3 -p lock_dlm -t csmb:ctdb_state /dev/csmb_vg/ctdb_lv</tt>

Note the different lock table name to distinguish this filesystem from the one created above and obviously the different device used for this filesystem.
The output will look something like this:

<tt>.. code-block::

    This will destroy any data on /dev/csmb_vg/ctdb_lv.
    t appears to contain a gfs2 filesystem.

Are you sure you want to proceed? [y/n] y

Device:                    /dev/csmb_vg/ctdb_lv
Blocksize:                 4096
Device Size                1.00 GB (262144 blocks)
Filesystem Size:           1.00 GB (262142 blocks)
Journals:                  3
Resource Groups:           4
Locking Protocol:          "lock_dlm"
Lock Table:                "csmb:ctdb_state"
UUID:                      BCDA8025-CAF3-85BB-B062-CC0AB8849A03</tt>

CTDB Configuration 
------------------------

The CTDB config file is located at <tt>/etc/sysconfig/ctdb</tt>. Five mandatory fields for ctdb operation that need to be configured are:

<tt>

.. code-block::

    CTDB_NODES=/etc/ctdb/nodes
CTDB_PUBLIC_ADDRESSES=/etc/ctdb/public_addresses
CTDB_RECOVERY_LOCK="/mnt/ctdb/.ctdb.lock"
CTDB_MANAGES_SAMBA=yes
CTDB_MANAGES_WINBIND=yes
</tt>

Not that there should be no spaces around the equals signs, since this config file is sourced by a shell script...

* <tt>CTDB_NODES</tt> specifies the location of the file which contains the list of ip addresses of the cluster nodes.
* <tt>CTDB_PUBLIC_ADDRESSES</tt> specifies the location of the file where a list of ip addresses can be used to export the samba shares exported by this cluster.
* <tt>CTDB_RECOVERY_LOCK</tt> specifies a lock file that <tt>ctdb</tt> uses internally for recovery and this file must reside on shared storage such that all the cluster nodes have access to it. In this example, we've used the gfs2 filesystem that will be mounted at <tt>/mnt/ctdb</tt> on all nodes. This is different from the gfs2 filesystem that will host the samba share that we plan to export. This <tt>reclock</tt> file is used to prevent split-brain scenarios. With newer versions of CTDB (>= 1.0.112), specifying this file is optional, but it is strongly recommended that it is substituted with another split-brain prevention mechanism.
* <tt>CTDB_MANAGES_SAMBA=yes</tt>. Enabling this allows ctdb to start and stop the samba service as it deems necessary to provide service migration/failover etc.
* <tt>CTDB_MANAGES_WINBIND=yes</tt>. If running on a member server, you will need to set this too.

The contents of the <tt>/etc/ctdb/nodes</tt> file:

<tt>.. code-block::

    192.168.1.151
192.168.1.152
192.168.1.153
</tt>

This simply lists the cluster nodes' ip addresses. In this example, we assume that there is only one interface/IP on each node that is used for both cluster/ctdb communication and serving clients. If you have two interfaces on each node and wish to dedicate one set of interfaces for cluster/ctdb communication, use those ip addresses here and make sure the hostnames/ip addresses used in the cluster.conf file are the same.

It is critical that this file is identical on all nodes because the ordering is important and ctdb will fail if it finds different information on different nodes.

The contents of the <tt>/etc/ctdb/public_addresses</tt> file:
<tt>

.. code-block::

    192.168.1.201/24 eth0
192.168.1.202/24 eth0
192.168.1.203/24 eth0
</tt>

We're using three addresses in our example above which are currently unused on the network. Please choose addresses that can be accessed by the intended clients.

These are the IP addresses that you should configure in DNS for the name of the clustered samba server and are the addresses that CIFS clients will connect to. By using different public_addresses files on different nodes it is possible to partition the cluster into subsets of nodes

For more information on ctdb configuration, look at: http://ctdb.samba.org/configuring.html

Samba Configuration 
------------------------

The <tt>smb.conf</tt> located at <tt>/etc/samba/smb.conf</tt> in this example looks like this:

<tt>.. code-block::

    [global]
	guest ok = yes
	clustering = yes
	netbios name = csmb-server
[csmb]
	comment = Clustered Samba 
	public = yes
	path = /mnt/gfs2/share
	writeable = yes</tt>
We export a share with name <tt>"csmb"</tt> located at <tt>/mnt/gfs2/share</tt>. Recall that this is different from the GFS2 shared filesystem that we used earlier for the ctdb lock file at <tt>/mnt/ctdb/.ctdb.lock</tt>. We will create the <tt>"share"</tt> directory in <tt>/mnt/gfs2</tt> when we mount it for the first time. <tt>clustering = yes</tt> instructs samba to use CTDB. <tt>netbios name = csmb-server</tt> explicitly sets all the nodes to have a common NetBIOS name.

<tt>smb.conf</tt> should be identical on all the cluster nodes.
</ul>
</li>

=================
Bringing up the cluster, clvmd (optional), gfs2, ctdb and samba
=================

Bringing up the Cluster 
------------------------

The <tt>cman init script</tt> is the best and recommended way of bringing up the cluster.
<tt>"service cman start"</tt> on all the nodes will bring up the various components of the cluster.

<u> Note for Fedora installs:</u> NetworkManager is enabled and run by default. This must be disabled in order to run cman.
   
<tt># service NetworkManager stop</tt>
   
<tt># chkconfig NetworkManager off</tt>

You might have to configure network interfaces manually using <tt>system-config-network</tt>. Also see <tt>man ifconfig</tt> for more information.

<tt>.. code-block::

    [root@clusmb-01 ~]# service cman start
Starting cluster:
   Checking Network Manager...                             [  OK  ]
   Global setup...                                         [  OK  ]
   Loading kernel modules...                               [  OK  ]
   Mounting configfs...                                    [  OK  ]
   Starting cman...                                        [  OK  ]
   Waiting for quorum...                                   [  OK  ]
   Starting fenced...                                      [  OK  ]
   Starting dlm_controld...                                [  OK  ]
   Starting gfs_controld...                                [  OK  ]
   Unfencing self...                                       [  OK  ]
   Joining fence domain...                                 [  OK  ]</tt>

Once this starts up successfully, you can verify that the cluster is up by issuing the <tt>"cman_tool nodes"</tt> command.

<tt>.. code-block::

    [root@clusmb-01 ~]# cman_tool nodes
Node  Sts   Inc   Joined               Name
   1   M     88   2010-10-07 15:44:27  clusmb-01
   2   M     92   2010-10-07 15:44:27  clusmb-02
   3   M     88   2010-10-07 15:44:27  clusmb-03</tt>

The output should be similar on all the nodes. The status field <tt>"Sts"</tt> value of <tt>'M'</tt> denotes that the particular node is a member of the cluster.

You can have a cluster node automatically join the cluster on reboot by having init start it up.

<tt>"chkconfig cman on"</tt>

Bringing up clvmd 
------------------------

<tt>"service clvmd start"</tt> on all the nodes should start the <tt>clvmd</tt> daemon and activate any clustered logical volumes that you might have. Since this is the first time you're bringing up this cluster, you still need to create the logical volumes required for its operation. At this point, you can go back to the previous skipped section <b>GFS2 Configuration</b> to create logical volumes and gfs2 filesystems as described there.

You can have a cluster node automatically start <tt>clvmd</tt> at system boot time by having init start it up. Make sure you have <tt>"cman"</tt> configured to start on boot as well.

<tt>"chkconfig clvmd on"</tt>

Bringing up GFS2 
------------------------

<tt>"mount -t gfs2 /dev/csmb_vg/csmb_lv /mnt/gfs2"</tt> and <tt>"mount -t gfs2 /dev/csmb_vg/ctdb_lv /mnt/ctdb"</tt> on all nodes will mount the gfs2 filesystems we created on <tt>/dev/csmb_vg/csmb_lv</tt> and <tt>/dev/csmb_vg/ctdb_lv</tt> at the mount points <tt>/mnt/gfs2</tt> and <tt>/mnt/ctdb</tt> respectively. Recall that the <tt>"/mnt/gfs2"</tt> mountpoint was configured in <tt>/etc/samba/smb.conf</tt> and <tt>"/mnt/ctdb"</tt> was configured in <tt>/etc/sysconfig/ctbd</tt>

Once mounted, from a single node, create the share directory <tt>/mnt/gfs2/share</tt>, set it's permissions according to your requirements and copy on all the data that you wish to export. Since gfs2 is a cluster filesystem, you'll notice that all the other nodes in the cluster also have the same contents in the <tt>/mnt/gfs2/</tt> directory.

It is possible to make gfs2 mount automatically during boot using <tt>"chkconfig gfs2 on"</tt>. Make sure you have <tt>"cman"</tt> and <tt>"clvmd"</tt> also set to start up on boot.
Like all other filesystems that can be automounted, you will need to add these lines to <tt>/etc/fstab</tt> before gfs2 can mount on its own:
<tt>.. code-block::

    /dev/csmb_vg/csmb_lv	/mnt/gfs2	gfs2	defaults	0 0
/dev/csmb_vg/ctdb_lv	/mnt/ctdb	gfs2	defaults	0 0</tt>

Bringing up CTDB/Samba 
------------------------

<tt>"service ctdb start"</tt> on all the nodes will bring up the <tt>ctdbd</tt> daemon and startup <tt>ctdb</tt>. Currently, it can take upto a minute for ctdb to stabilize. <tt>"ctdb status"</tt> will show you how ctdb is doing:

<tt>.. code-block::

    [root@clusmb-01 ~]# ctdb status
Number of nodes:3
pnn:0 192.168.1.151     OK (THIS NODE)
pnn:1 192.168.1.152     OK
pnn:2 192.168.1.153     OK
Generation:1410259202
Size:3
hash:0 lmaster:0
hash:1 lmaster:1
hash:2 lmaster:2
Recovery mode:NORMAL (0)
Recovery master:0</tt>

Since we configured ctdb with <tt>"CTDB_MANAGES_SAMBA=yes"</tt>, <tt>ctdb</tt> will also start up the <tt>samba</tt> service on all nodes and export all configured samba shares.

When you see that all nodes are <tt>"OK"</tt>, it's safe to move on to the next step.

=================
Using the Clustered Samba server
=================

Clients can connect to the samba share that we just exported by connecting to one of the ip addresses specified in <tt>/etc/ctdb/public_addresses</tt>

Example:

<tt>mount -t cifs //192.168.1.201/csmb /mnt/sambashare -o user=testmonkey</tt>

	or

<tt>smbclient //192.168.1.201/csmb</tt>

For Cluster/GFS2 related questions please email linux-cluster@redhat.com and for Samba/CTDB related questions please email samba-technical@samba.org