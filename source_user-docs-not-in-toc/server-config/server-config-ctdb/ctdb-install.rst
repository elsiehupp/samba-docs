Getting and Building CTDB
    <namespace>0</namespace>
<last_edited>2016-10-21T00:38:23Z</last_edited>
<last_editor>MartinSchwenke</last_editor>

=================
Getting CTDB Source code
=================

If you `Obtaining_Samba|download source code for Samba &gt;= 4.2.0` then CTDB is included in the ``ctdb`` subdirectory.

=================
Binary packages
=================

* Your operating system may include pre-built packages for Samba and CTDB

* If you use pre-built packages then you need to ensure that Samba was built with cluster support

*  You can get information about smbd's clustering features:

   # smbd -b | grep -i 'ctdb\|cluster'
     CLUSTER_SUPPORT
     CTDB_SOCKET
   Cluster support features:
     CLUSTER_SUPPORT
     CTDB_SOCKET: /var/run/ctdb/ctdbd.socket
     CTDB_PROTOCOL: 1

*  Note that output may vary slightly, but the ``CLUSTER_SUPPORT`` feature must be present.

* CTDB will usually be in a separate package to other Samba components called ``ctdb``

=================
Building from source code
=================

CTDB should be built with Samba.  Add the following options to the ``./configure`` command.

; ``--with-cluster-support`` : This enables clustering support in Samba and includes CTDB in the build
; ``--with-shared-modules=idmap_rid,idmap_tdb2,idmap_ad`` : Clustered Samba needs an IDMAP facility so it is worth building a few

Other than that, just `Build_Samba_from_Source|follow instructions for building Samba`.

=================
Legacy CTDB versions
=================

You should use a version of CTDB that comes with `Samba_Release_Planning|a supported Samba version`.

However, [https://download.samba.org/pub/ctdb/ legacy, standalone versions of CTDB are still available] (including `CTDB2releaseNotes|2.x`).  These versions were used with Samba versions older than 4.2.0.

The code is also available from the CTDB git repository at git://git.samba.org/ctdb.git.  See also the [https://git.samba.org/?p=ctdb.git;a=summary GIT web interface].