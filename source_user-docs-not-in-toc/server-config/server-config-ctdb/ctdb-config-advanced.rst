Advanced CTDB configuration
    <namespace>0</namespace>
<last_edited>2018-09-17T06:18:39Z</last_edited>
<last_editor>MartinSchwenke</last_editor>

===============================

Introduction
===============================

This section describes advanced CTDB configuration topics that do not fit anywhere else.

=================
CTDB port
=================

CTDB uses `Basic CTDB configuration#nodes file|private IP addresses` to communicate between nodes.  Connections are made to IANA assigned TCP port 4379 on each node.

Although not recommended, it is possible to configure a different port to be used for CTDB traffic.  This is done by adding a ctdb entry to the ``/etc/services`` file.

For example, add the following line to ``/etc/services`` to change CTDB to use port 9999:

    tdb  9999/tcp

**Note:** All nodes in the cluster **must** use the same port.

=================
Event scripts
=================

CTDB runs event scripts when certain events occur.  These event scripts usually reside in the ``events/`` subdirectory of the `Basic CTDB configuration#CTDB configuration directory|CTDB configuration directory`.  This is often ``/etc/ctdb/events``.

Event scripts support health monitoring, service management, IP failover, internal CTDB operations and features.  They handle events such as ``startup``, ``shutdown``, ``monitor``, ``releaseip`` and ``takeip``.

Please see the event scripts that installed by CTDB for examples of how to configure other services to be aware of the HA features of CTDB.

Also see ``ctdb/config/events/README`` in the Samba source tree for additional documentation on how to write and modify event scripts.

Samba &le; 4.8 
------------------------

Event scripts reside in the ``events.d/`` subdirectory.