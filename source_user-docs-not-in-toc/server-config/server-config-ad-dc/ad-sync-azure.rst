Azure AD Sync
    <namespace>0</namespace>
<last_edited>2021-05-03T14:57:38Z</last_edited>
<last_editor>Maurer-itsd</last_editor>


===============================
Azure AD Connect cloud sync - Agent installation and configuration
===============================

Requirements:
* samba4 AD-DC (tested with 4.12.11)
* samba-tool domain functionalprep --function-level=2012_R2 run successfully
* samba-tool domain schemaupgrade --schema=2012_R2 run successfully
* Windows Server 2016 in english language. If, for example, an attempt is made to install the agent on a German-language system, the process terminates when the services are started.
* .NET-Framework Version 4.7.1
* aad-connect user, member of the Enterprise Admin group. (You will need it for the configuration wizard to connect to the local Active Directory).

Install the Agent:
* Sign in to the server you'll use with enterprise admin permissions
* Sign in to the Azure portal, and then go to Azure Active Directory
* In the left menu, select Azure AD Connect
* Select Manage cloud sync > Review all agents
* Download the Azure AD Connect provisioning agent from the Azure portal

`File:g` 

With agent version 1.1.281.0+, by default, when you run the agent configuration wizard, you are prompted to setup Group Managed Service Account (GMSA). In this scenario you can skip the GMSA installation:
* Run the agent installer as Administrator to install the new agent binaries. Close the agent configuration wizard which opens up automatically after the installation is successful. 
* Use the **'Run**' menu item to open the registry editor (regedit.exe) 
* Locate the key folder HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Azure AD Connect Agents\Azure AD Connect Provisioning Agent 
* Right-click and select "New -> DWORD Value" 
* Provide the name: UseCredentials 
* Double-click on the Value Name and enter the value data as 1.

`File:g`

* After this operation, start the configuration wizard to enter data to Microsoft 365 and for the connection to the local Active Directory (aad-connect user)

`File:g`

Verify Agent Installation on Azure portal:
* Sign in to the Azure portal
* Select Azure Active Directory > Azure AD Connect. In the center, select Manage cloud sync.

`File:g`

* Select Review all agents

`File:g`

On the On-premises provisioning agents screen, you see the agents you installed. Verify that the agent in question is there and is marked **'active**'

`File:g`

Verify Agent Installation on the local server:
* Open Services as Administrator by either navigating to it or by going to Start > Run > Services.msc.
Make sure Microsoft Azure AD Connect Agent Updater and Microsoft Azure AD Connect Provisioning Agent are there and their status is **'Running**'.

`File:g`

After you install the Azure AD Connect provisioning agent, you need to log in to the Azure portal and configure it.