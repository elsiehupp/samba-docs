Using the samba backup script
    <namespace>0</namespace>
<last_edited>2018-08-09T22:52:19Z</last_edited>
<last_editor>Timbeale</last_editor>

===============================

Introduction
===============================

* 
| type = important
| text = This page describes creating a backup on Samba v4.0-v4.8. Creating backups has been greatly improved on Samba v4.9 onwards - see `Back_up_and_Restoring_a_Samba_AD_DC|here` for more details

The contents of this document are meant to be used with samba 4.x.

This is a tutorial about backup and restore of a Samba AD DC. It is hoped that it will be useful, but it comes WITHOUT ANY WARRANTY!

**Until this line is removed, consider this document as a DRAFT, that may contain faults or is missing something!**

=================
General
=================

* 
| type = important
| text = WARNING, if you run more than one DC:store a DC from a backup. You'll corrupt the directory as the replication meta data will get out of sync!

If at least one other DC is still running, simply `Joining_a_Samba_DC_to_an_Existing_Active_Directory|join the new machine as a DC` again. Everything will be synchronized from the other running DC(s) - except SysVol replication, which isn't implemented yet.

* 
| type = note
| text = Only use a backup on the DC it came from, never use it to upgrade your DC by moving it to a new DC. In this instance, join a new DC instead.

If every DC in the domain is broken, you should post on the [mailto:SSHHtechnical@lists.samba.org samba-technical] mailing list on http:SSLLAASS:HHlists.samba.org to discuss the best way to fix this.

How LDB and TDB files can be backed-up/restored 
------------------------

If plan to modify the 'samba_backup' script or create your own solution, you'll find some useful information here:
* Whilst you must never normally copy a running database, you can backup *.ldb & *.tdb files online with 'tdbbackup'
* Files created by tdbbackup, are full and normal TDB files. tdbbackup does the extraction safely.
* Backups of *.ldb files can be restored to their old names by simply removing the '.bak' suffix.

About the samba_backup script 
------------------------

Samba provides a very basic shell script to backup its databases (source4/scripting/bin/samba_backup in the source tarball). This requires that your whole samba installation is in one place (such as /usr/local/samba/). If you have used configure options to store pieces of samba in different locations, you can adjust the script or use it as a base to write your own script.

Because the script is very basic and if you plan to use it as is, there is one thing to be aware of:

* The script doesn't backup extended ACLs. So you will lose the permissions e. g. on the SysVol share. If you have a tar version that supports the --xattrs option (see the tar manpage), you should add this option to all 'tar' commands inside the script. This enables tar to keep extended ACLs in the archives.

=================
Backup
=================

The Samba backup script isn't installed by 'make install'. You need to you copy it from the sources directory (source4/scripting/bin/samba_backup) to somewhere like /usr/sbin, and set secure permissions:
 # cp .../source4/scripting/bin/samba_backup /usr/sbin
 # chown root:AASSHHusr/sbin/samba_backup
 # chmod 750 /usr/sbin/samba_backup

Adjust the following variables inside the script to your needs:
 FROMWHERE=/usr/local/samba
 WHERE=/usr/local/backups
 DAYS=90

Create the destination folder, you have configured in the $WHERE variable and set permissions:
 # mkdir /usr/local/backups
 # chmod 750 /usr/local/backups

Start the backup script for a first test:
 # /usr/sbin/samba_backup

If the script exits without an error, you should find three files in the destination folder:
* etc.{Timestamp}.tar.bz2
* samba4_private.{Timestamp}.tar.bz2
* sysvol.{Timestamp}.tar.bz2

If your test backup succeeded, you should add a cron-job for daily backup:
 # crontab -e

Add the following line to backup daily at 2am:
 0 2 * * *       /usr/sbin/samba_backup

*Note:when running the script via cron, all required binaries are in directories that are part of the $PATH variable. If the correct path isn't defined system-wide, you can either set the variable in your crontab or at the beginning of the script after the shebang (#!) line.*

=================
Restore
=================

The following restore guide assumes that you backed-up your databases with the 'samba_backup' script. If you have your own script, adjust the steps.

**Very important notes:
* **Never do a restore and a version change at the same time! Always restore on a system using the same Samba version as the one you created the backup on!**
* **Always Restore on a system with the same IP and Hostname. Otherwise you will run into Kerberos and DNS issues.**
* **Recommended:on the same OS as you created the backup on.**
**The most important thing in a restore situation is to bring your system back to a running state. Once everything is up and tested, you can then do any required changes. Never try to make changes together with a restore!**

If your whole system is broken, you will first have to setup the whole machine as described in the HowTo (`Setting_up_Samba_as_an_Active_Directory_Domain_Controller|Active Directory Controller`).

Remove the folders, that we will restore (samba must not be running!):
 # rm -rf /usr/local/samba/etc
 # rm -rf /usr/local/samba/private
 # rm -rf /usr/local/samba/var/locks/sysvol

Now unpack your latest working backup files to their old location:
 # cd /usr/local/backups
 # tar -jxf etc.{Timestamp}.tar.bz2 -C /usr/local/samba/
 # tar -jxf samba4_private.{Timestamp}.tar.bz2 -C /usr/local/samba/
 # tar -jxf sysvol.{Timestamp}.tar.bz2 -C /usr/local/samba/

Rename *.ldb.bak files in the 'private' directory back to *.ldb. This can be done with GNU find and Bash:
 # find /usr/local/samba/private/ -type f -name '*.ldb.bak' -print0 | while read -d $'\0' f ; do mv "$f" "${f%.bak}" ; done

If your backup doesn't contain extended ACLs (see section `#About the samba_backup script|About the samba_backup script`, you have to run:
 # samba-tool ntacl sysvolreset

If you use Bind as DNS backend, you have to fix the hardlinks for the DNS databases:
 # samba_upgradedns --dns-backend=BIND9_DLZ

See `BIND9_DLZ_DNS_Back_End#New_added_DNS_entries_are_not_resolvable|DNS Backend BIND - New added DNS entries are not resolvable`.

Now you can start samba and test if your restore was successful.

**Hint:t it is possible to restore single databases out of your backups, some databases may be linked to others, so be carefully that you don't get an inconsistent system! If you are unsure if your broken database relies on others, you should ask on the [mailto:sambaDDA:ical@lists.samba.org samba-technical] mailing list on http:/SSLLAAS:OOTTsamba.org, or restore everything.

----
`Category:rectory` `Category:Backup`: