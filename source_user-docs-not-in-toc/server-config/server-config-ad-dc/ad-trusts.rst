Active Directory Trusts
    <namespace>0</namespace>
<last_edited>2016-11-22T09:52:05Z</last_edited>
<last_editor>Slowfranklin</last_editor>

==========================

Support for Active Directory Trusts
==========================

External trusts between individual domains work in both ways (inbound and outbound). The same applies to root domains of a forest trust.

The transitive routing into the other forest is fully functional for kerberos, but not yet supported for NTLM . FIXMEFIXMEFIXME: what does this mean from a functional perspective?

While a lot of things are working fine, there are currently a few limitations:

* Both sides of the trust need to fully trust each other!
* No SID filtering rules are applied at all!
* This means DCs of domain A can grant domain admin rights in domain B.
* It's not possible to add users/groups of a trusted domain into domain groups.

`Category:Active Directory`