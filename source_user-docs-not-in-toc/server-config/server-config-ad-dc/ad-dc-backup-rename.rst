Domain backup and rename
    <namespace>0</namespace>
<last_edited>2019-05-01T05:21:08Z</last_edited>
<last_editor>Timbeale</last_editor>

Overview 
------------------------

**Added in Samba version:OTT9

Samba backups provide a way to recover your Samba network, in the unlikely event that a catastrophic failure of the domain occurs. For more details on the different types of Samba backups and how to use them, see `Back up and Restoring a Samba AD DC`.

One of the backup options involves renaming the domain (see `Domain rename tool`). Note that this works completely differently to the Windows tool for renaming domains, i.e. ``rendom``. Windows renames each DC one at a time in situ. Whereas the Samba approach is to take a renamed copy of the domain database, create a single new DC that re-seeds the domain, and start re-adding DCs again from scratch. On Windows there is an overlap where some DCs are using the original domain and some are using the renamed domain, but on Samba there can be no overlap.

How to configure it
------------------------

Use the ``samba-tool domain backup`` set of commands to create a backup-file. There are three flavours of backup to choose from:offline, and rename. Then use ``samba-tool domain backup restore`` to restore the backup-file. Generally, you'll need to run the backup/restore commands as root. See `Back up and Restoring a Samba AD DC` for more details.

Known issues and limitations
------------------------

The most important concept to grasp is you're taking a backup of your entire *domain*, not an individual DC. So restoring the domain is essentially completely replacing the existing domain DCs.

You can't restore the domain to the default install location on disk (i.e. <tt>/usr/local/samba</tt>). You have to specify a new DC during the restore, i.e. one that is not already in the domain. See our `Back_up_and_Restoring_a_Samba_AD_DC#Recommended_strategy|recommended strategy` for how to work around these problems.

See bug #13621 in Samba bugzilla for current issues in v4.9.0.

Troubleshooting
------------------------

See `Back_up_and_Restoring_a_Samba_AD_DC#Troubleshooting|Troubleshooting`.

For Developers == 

How to test it
------------------------

The main samba-tool backup/restore tests are run using:

.. code-block::

    make test TESTS="domain_backup"

However, a lot of the backup/restore functionality is tested by running testenvs created by doing a backup and then a restore. These testenvs are:c, renamedc, labdc, and offlinebackupdc (which reflect the different backup use-cases). These testenvs are created by backing up a dedicated backupfromdc. A small subset of tests are then run against these testenvs to verify that they're functional. New tests were added to specifically sanity-check the restored DB is consistent with the original (ldapcmp_restore) and that you can do a basic join to the new testenvs (join_ldapcmp).

You can also run the backup/restore commands manually within a testenv. Note:ad_dc (or similar) testenv, *not* the ad_dc_ntvfs testenv, as we don't support backups with an NTVFS file system. Once you have a backup-file, you can also restore it outside of the testenv (you'll need to use sudo in order to restore the sysvol ACLs).

Where the code is located
------------------------

The backup/restore code is located in netcmd/domain_backup.py. It uses parts of the existing join.py and provision framework. A new python library (ntacls.py) was also added for backing up the sysvol files correctly (along with their corresponding NTACLs).

Reference Docs
------------------------

The backup and restore functionality are specific to Samba, so there are no external reference docs.

`Category:re`
`Category:
`Category:rectory`