Samba AD on CentOS7
    <namespace>0</namespace>
<last_edited>2015-12-15T13:32:08Z</last_edited>
<last_editor>Hflautert</last_editor>

This page describe Samba4 Active Directory installation on CentOS 7. The environment is composed by 2 DCs with replication, and one fileserver.

===============================
Enviroment
===============================

 dc1.domain.com.br
 dc2.domain.com.br
 fileserver.domain.com.br

===============================
Installation
===============================

The installation is based on **CentOS7 Minimal ISO**, you should update the system and install basic tools.
 yum update
 yum install -y vim wget
Pre-Setup
------------------------

This pre-setup can be done in all 3 servers, basically it will setup the Sernet Samba Repository and install samba from repo.
 wget https://raw.githubusercontent.com/hflautert/lotus7/master/samba4/pre_install_samba4.sh
 chmod +x pre_install_samba4.sh
 ./pre_install_samba4.sh

===============================
DC1 Setup
===============================

------------------------

First of all we need a domain, so lets provision it.
 samba-tool domain provision --use-rfc2307 --interactive

===============================
DC2 Setup
===============================

------------------------

Join as DC.
 samba-tool domain join domain.com.br DC -Uadministrator --realm=domain.com.br
Fix DNS if it was not created automatically:
 samba-tool dns add dc1 domain.com.br dc2 A <IP-OF-DC2> -Uadministrator
After couple minutes, we can see the replication:
 samba-tool drs showrepl
* You can safely ignore it:SLLAASS:HHwiki.samba.org/index.php/FAQ#Message:_Warning:_No_NC_r:for_Conn:TT21 Warning: No NC replicated for Connection!]:

Post Setup
------------------------

This post setup will configure the services to startup and disable Selinux and Firewall, during my tests firewalld did not save the allowed ports, even with permanent flag, so I´v decided to disable to avoid problems.
 wget https://raw.githubusercontent.com/hflautert/lotus7/master/samba4/post_install_samba4.sh
 chmod +x post_install_samba4.sh
 ./post_install_samba4.sh

===============================
Fileserver Setup
===============================

The pre-setup is the same, but we are not going provision neither join, the fileserver will be just a member of domain.
CentOS bring us a useful tool to do it, actually we are going to setup authentication and join the domain in one shot.
Prep, auth and join
------------------------

Lets start testing our DNS:
 host -t SRV _ldap._tcp.domain.com.br.
 _ldap._tcp.domain.com.br has SRV record 0 100 389 dc1.domain.com.br.
Setup auth and join the domain:
 authconfig-tui
`file:DAASSHHtui.png`

`file:DAASSHHtui2.png`

Ignore DNS error, you have to create DNS host A manually, at this point you can use windows interface to do that.

Hide domain name:
 authconfig --enablewinbindusedefaultdomain --update
Se domain info and users:
 net ads info
 wbinfo -u
 wbinfo -g

Grant rights to **domain admins** change disk permissions:
 net rpc rights grant 'domain admins' SeDiskOperatorPrivilege -U'administrator' -I fileserver.domain.com.br

Services configuration
------------------------

Here there is a TODO, you can map what you need to set on Selinux, or disable it:
 setenforce 0
 sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config

Setup samba start mode
 sed -i 's/SAMBA_START_MODE="none"/SAMBA_START_MODE="classic"/g' /etc/default/sernet-samba

Add following lines at end of [global] section on /etc/samba/smb.conf
 vfs objects = acl_xattr
 map acl inherit = Yes
 store dos attributes = Yes

Restart services
 systemctl restart sernet-samba-smbd
 systemctl restart sernet-samba-nmbd
 systemctl restart sernet-samba-winbindd

Configurte startup
 chkconfig sernet-samba-nmbd on
 chkconfig sernet-samba-smbd on
 chkconfig sernet-samba-winbindd on

Shares
------------------------

Add share on smb.conf
 [share]
   path = /storage/share
   read only = no

Reload samba:
 systemctl reload sernet-samba-smbd

Restric permissions:
 chmod -Rf 770 /storage/share
 chown administrator:ain admins" /storage/share

Set ACL to domain admins can change permissions.
 setfacl -m default:ot;do:s":rwx /sto:SSHHshare

===============================
Useful
===============================

`Installing RSAT`

`Rsync_based_SysVol_replication_workaround`

[https://www.forensit.com/uk/domain-migration.html User Profile Wizard]

===============================
Troubleshooting
===============================

All my troubles were related to:
*Check network-firewall.
*Check DNS.
*Check Time and Date.