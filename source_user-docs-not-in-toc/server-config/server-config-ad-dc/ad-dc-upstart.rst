Managing the Samba AD DC Service Using Upstart
    <namespace>0</namespace>
<last_edited>2018-03-21T09:46:57Z</last_edited>
<last_editor>Grimtech</last_editor>

__TOC__

=================
Downloading an Upstart Configuration File
=================

To retrieve the upstart config file, run:

 $ wget -O /etc/init/samba-ad-dc.conf "<nowiki>https://salsa.debian.org/samba-team/samba/raw/jessie/debian/samba-ad-dc.upstart</nowiki>"

{{Imbox
| type = warning
| text = This procedure links to unverified, external content.
