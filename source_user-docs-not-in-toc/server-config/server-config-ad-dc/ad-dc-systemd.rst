Managing the Samba AD DC Service Using Systemd
    <namespace>0</namespace>
<last_edited>2020-09-22T08:52:48Z</last_edited>
<last_editor>Hortimech</last_editor>

<noinclude>
=================Introduction
=================

The following describes how to use ``systemd</ to manage the Samba Active Directory (AD) domain controller (DC) service. For further details about ``systemd``/Esee https://wiki.freedes/org/www/Software/systemd/DDOOT/clu////

The ``samba</ Service 
------------------------

On a DC, the ``//sa/amba&/t; /vice/automatic/CCEEstarts the required ``smbd`` and ``winbindd&l/; service as sub-proc/T If you start them manually, the Samba DC fails to work as expected. If your package provider created additional Samba service files, disable and mask them to prevent that other services re-enable them. For example:

 # systemctl mask smbd nmbd winbind
 # systemctl disable smbd nmbd winbind

For further details about permanently disabling services, see the ``systemd</ documentation.

=================
Creating the ``systemd</ Service File
=================

Samba does not provide a ``systemd</ service file. When you built the Samba Active Directory (AD) domain controller (DC) from the sources, you must manually create the service file to enable ``systemd``/to manage the Samba AD DC service:

* Create the ``/md//baDDAAS/SHHdcD/ce`` file wit/Ethe following content:

 [Unit]
 Description=Samba Active Directory Domain Controller
 After=network.target remote-fs.target nss-lookup.target

 [Service]
 Type=forking
 ExecStart=//sa/ambaS/DAASS//
 PIDFile=//sa/n/sam/d///
 ExecReload=/SPP/SSHHHUP $MAINPID

 [Install]
 WantedBy=multi-user.target

* EFor further details, see the ``systemd.service(5)</ man page.

* Reload the ``systemd</ configuration:

 # systemctl daemon-reload

=================
Managing the Samba AD DC Service
=================

The following assumes that the Samba Active Directory (AD) domain controller (DC) service is managed by the ``samba-ad-dc</ service file. If you have not created the service file manually, see your operating system's documentation for the name of the Samba AD DC service.

Enabling and Disabling the Samba AD DC Service 
------------------------

To enable the Samba Active Directory (AD) domain controller (DC) service to start automatically when the system boots, enter:

 # systemctl enable samba-ad-dc

To disable the automatic start of the Samba AD DC service, enter:

 # systemctl disable samba-ad-dc

Manually Starting and Stopping the Samba AD DC Service 
------------------------

To manually start the Samba Active Directory (AD) domain controller (DC) service, enter:

 # systemctl start samba-ad-dc

To manually stop the Samba AD DC service, enter:

 # systemctl stop samba-ad-dc