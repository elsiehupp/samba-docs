Upgrading a Samba AD DC
    <namespace>0</namespace>
<last_edited>2019-09-19T09:12:50Z</last_edited>
<last_editor>Hortimech</last_editor>

__NOTOC__
Run the following steps when you are updating a Samba Active Directory (AD) domain controller (DC). 

.. note:

    For upgrading a Samba NT4-style PDC, a Samba domain member, or a standalone installation, please see `Updating_Samba#The_Update_Process|updating samba`.

=================
Before you upgrade
=================

* On at least one AD DC, create a backup-file of your domain using the `Back_up_and_Restoring_a_Samba_AD_DC|samba-tool domain backup tool`.

* Read the [https://www.samba.org/samba/history/ release notes] of skipped versions. They contain important information, such as new features, changed parameter, and bug fixes. In case you switch to new major release, read the release notes of the initial version (x.y.0) and the ones from minor versions up to the new version you will update to. For example, if you update from 4.4.4 to 4.6.2, read the 4.5.0, 4.6.0, 4.6.1, and 4.6.2 release notes.

* Get the latest version of Samba:
** If you compile Samba from the sources, use the same ``configure`` options as used for your previous version. For more information, see `Build_Samba_from_Source#Viewing_Built_Options_of_an_Existing_Installation|Build Samba From the Sources`.
** If you update using packages, read the distribution documentation for information how to update.

* Avoid making changes to the AD directory during the upgrade. Administrators should refrain from adding users or changing group memberships while the upgrade is taking place. While it's difficult to prevent users from changing their passwords, you can schedule the Samba upgrade for a low-use time of day.

=================
Upgrading in-place
=================

Run the following steps:

* Stop all Samba services.

* Install the latest packages over your existing ones.

* Start the ``samba`` daemon.

* Run the `#Samba_AD_DC_Database_Check|Samba AD DC database check` to identify (and fix) any potential problems with the database.

* Check your Samba log files for errors.

* Test the Samba services work on your updated installation, e.g. verify users can login successfully via the upgraded DC.

This approach works best for moving between minor Samba releases (e.g. from v4.10.5 to v4.10.6).

=================
Updating Multiple Samba Domain Controllers
=================

If you are updating multiple Samba AD DCs, it's recommended to mitigate the risk of data loss by following this process: 

* Update one Samba AD DC that does not hold any `Transferring_and_Seizing_FSMO_Roles#Displaying_the_Current_FSMO_Role_Owners|flexible single master operations (FSMO) role`, or else the DC with the least roles.
** For upgrading to a new release, most users should simply follow the '`#Upgrading in-place|in-place upgrade`' steps above.
** Advanced users upgrading to a new *major* release may want to consider using the '`#Rejoining the upgraded DC|DC rejoin`' steps below.

* Start Samba on the updated DC.

* Verify that the directory replication between all DCs is working correctly using: ``samba-tool drs showrepl``

* Test the installation to ensure that the new version works correctly.

* Upgrade all other Samba DCs one at a time, with those holding FSMO roles last, and always verify that the replication is working correctly.

This method ensures that if any of the DCs have issues with the new version, you can find out with a joined DC rather than the master. If the issue can't be fixed easily, you can return to an earlier version on the joined DC without network downtime or risk of data loss.

.. note:

    Note that some samba-tool commands allow you to modify the database directly on disk. However, it's best to avoid doing this in the middle of a upgrade to a new major release. Wait until after you've restarted the samba executable before making any other changes to the local database. 

=================
Upgrade A DC and join it to the domain again
=================

{{Imbox
| type = warning
| text = Joining a DC to domain can be a troublesome process for some users, and it can be difficult to recover your DC if something goes wrong. Only consider using this approach if you are an advanced Samba user, and are running a modern Samba installation (i.e. v4.7 release or later, with a minimum 2008R2 base schema). Only use this approach for major Samba version upgrades (e.g. v4.10 to v4.11).

Upgrading a DC and Joining it to the domain again has `Downgrading_an_Active_Directory_DC#In-place_upgrades_vs_DC_rejoins|some benefits` for your AD database over in-place upgrades. This approach requires having at least two active DCs in your domain (which is also recommended for redundancy, in case of failure). This is the most reliable approach for `Downgrading an Active Directory DC|AD database compatibility`. The steps are similar to upgrading in place, with a few extra steps to rejoin the DC. The process is:

* Check the outbound replication on the DC is up-to-date. You can either `Verifying_the_Directory_Replication_Statuses|verify the replication status` or force a replication to the peer DC using the 'samba-tool drs replicate' command.

* If the DC holds any FSMO roles, transfer them to another DC before continuing.

* Demote the DC:

 # samba-tool domain demote -U administrator
 Using DC1.samdom.example.com as partner server for the demotion
 Password for [SAMDOM\administrator]:
 Deactivating inbound replication
 Asking partner server DC1.samdom.example.com to synchronize from us
 Changing userControl and container
 Removing Sysvol reference: CN=DC2,CN=Enterprise,CN=Microsoft System Volumes,CN=System,CN=Configuration,DC=samdom,DC=example,DC=com
 Removing Sysvol reference: CN=DC2,CN=samdom.example.com,CN=Microsoft System Volumes,CN=System,CN=Configuration,DC=samdom,DC=example,DC=com
 Removing Sysvol reference: CN=DC2,CN=Domain System Volumes (SYSVOL share),CN=File Replication Service,CN=System,DC=samdom,DC=example,DC=com
 Removing Sysvol reference: CN=DC2,CN=Topology,CN=Domain System Volume,CN=DFSR-GlobalSettings,CN=System,DC=samdom,DC=example,DC=com
 Demote successful

* Stop all Samba services.

* Remove the existing Samba version, ensure that all traces of Samba are removed.

* Install the latest Samba packages.

* If this is the first DC you have tried to upgrade:
** First, using your upgraded samba package, take an `Back_up_and_Restoring_a_Samba_AD_DC#Online_DC_backup|online backup` of another DC in the network. This mimics the rejoin process - if it doesn't work successfully, ***don't*** continue. Use the in-place upgrade instead.
** As an additional sanity-check, you could try joining a temporary DC first, e.g. using a temporary VM. If you can't spin up a VM, use ``--option="netbios name = tmpdc" --target-dir=~/tmp-join/`` to achieve the same result. Once you've rejoined your production DC successfully, you can demote this temporary DC (and if you used the --targetdir option, then remove the directory too).

* Run the ``samba-tool domain join <realm> dc`` command to rejoin the upgraded DC to the domain. This uses DRS replication to completely rebuild the DC's local database. 
** Try to use the same command options as you did when you originally joined the DC.

* Start the ``samba`` daemon.

* Run the `#Samba_AD_DC_Database_Check|Samba AD DC database check`.

* Check your Samba log files for errors.

* Test your updated installation, e.g. verify users can login successfully.

=================
Downgrading
=================

If you need to downgrade between major Samba releases (e.g. v4.11 to v4.10) please read `Downgrading an Active Directory DC` carefully.

=================
Samba AD DC Database Check
=================

The ``samba-tool dbcheck`` utility enables you to detect and fix problems in the Samba AD database. It's worth running dbcheck on each DC *after* you have upgraded the Samba packages and restarted the samba executable. Use the ``--cross-ncs`` option to make sure you check each partition in the database.

For example, a previous Samba version may have stored an attribute incorrectly and the updated version fixes the problem. However, you must run dbcheck manually on each DC in order to find and fix any such problems. 

For more details on running the tool, refer to the `dbcheck` page.

`Category:Active Directory`