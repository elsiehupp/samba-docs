Group membership in AD
    <namespace>0</namespace>
<last_edited>2020-04-23T05:39:52Z</last_edited>
<last_editor>Abartlet</last_editor>

==========================================
The User token and Group memberships in AD
==========================================

All users accessing a Samba server, indeed any server or service in an AD domain, have a list of groups associated with them.  This is often referred to as the `Kerberos PAC`, which is actually the surrounding structure encrypted and signed within a Kerberos ticket.  The user group information is in that ticket, but not trivially accessible.

Group membership in AD is recursive, and group-based
----------------------------------------------------

Most users in an AD will have a number of attribute values describing the groups they are a member of.  These are 
 memberOf
and
 primaryGroupID.  

However, this is not the total group membership, because those groups are also members of other groups.

To show this, the naive simple group membership search shows:

 ldbsearch -H ldap://$SERVER -s base -b "CN=Administrator,CN=Users,DC=addom,DC=samba,DC=example,DC=com" memberOf primaryGroupID -U$USERNAME
gives
 dn: CN=Administrator,CN=Users,DC=addom,DC=samba,DC=example,DC=com
 primaryGroupID: 513
 memberOf: CN=Domain Admins,CN=Users,DC=addom,DC=samba,DC=example,DC=com
 memberOf: CN=Schema Admins,CN=Users,DC=addom,DC=samba,DC=example,DC=com
 memberOf: CN=Enterprise Admins,CN=Users,DC=addom,DC=samba,DC=example,DC=com
 memberOf: CN=Group Policy Creator Owners,CN=Users,DC=addom,DC=samba,DC=example,DC=com
 memberOf: CN=Administrators,CN=Builtin,DC=addom,DC=samba,DC=example,DC=com

Note how, compared to the examples below, how this is missing
 SID_BUILTIN_ADMINISTRATORS    = S-1-5-32-544

This can be discovered by recursion, but this is slow and may be error-prone if the domain is part of a larger forest or there are inter-forest trusts. 

Finally, it should of course be remembered that the group membership is actually controlled by the **member** attribute on the groups, and the **primaryGroupID** attribute on the user, not the **memberOf** backlink.  Only the **member** attributes are available for modification.

===============================
Accessing the final group membership
===============================

tokenGroups is an operational (that is, calculated and hidden) LDAP attribute.  It is present both on an individual user (accessible by other users, eg a service account or administrator account) and on the rootDSE for the current user. 

===============================
Limits on final group membership size
===============================

It is well-known that the final token [https://support.microsoft.com/help/328889/logging-on-a-user-account-that-is-a-member-of-more-than-1010-groups-ma cannot contain more than 1010 groups] against Windows. 

See also `Kerberos_PAC#A_too-large_PAC|PAC size limits`.

ldbsearch for the tokenGroups attribute on authenticated connection
------------------------

To confirm the final group membership of a the current user, run:

 ldbsearch -H ldap://$SERVER -s base -b "" tokenGroups -U$USERNAME

This will give an output like this:

 # record 1
 dn: 
 tokenGroups: S-1-5-21-4023018537-2373006774-1847616786-500
 tokenGroups: S-1-5-21-4023018537-2373006774-1847616786-513
 tokenGroups: S-1-5-21-4023018537-2373006774-1847616786-512
 tokenGroups: S-1-5-21-4023018537-2373006774-1847616786-572 
 tokenGroups: S-1-5-21-4023018537-2373006774-1847616786-518
 tokenGroups: S-1-5-21-4023018537-2373006774-1847616786-519
 tokenGroups: S-1-5-21-4023018537-2373006774-1847616786-520
 tokenGroups: S-1-1-0
 tokenGroups: S-1-5-2
 tokenGroups: S-1-5-11
 tokenGroups: S-1-5-32-544
 tokenGroups: S-1-5-32-545
 tokenGroups: S-1-5-32-554

Compared with the below, this includes SIDs which are only added to the user token on the final server, like 
 SID_NT_AUTHENTICATED_USERS    = "S-1-5-11"
 SID_BUILTIN_PREW2K            = "S-1-5-32-554"
A SID indicating the user of NTLM may also be added.

ldbsearch base search on the user object
------------------------

**tokenGroups** is also provided on the user object

 ldbsearch -H ldap://$SERVER -s base -b "<SID=S-1-5-21-4023018537-2373006774-1847616786-500>" tokengroups -U$USERNAME%$PASSWORD
gives
 # record 1
 dn: CN=Administrator,CN=Users,DC=addom,DC=samba,DC=example,DC=com
 tokenGroups: S-1-5-21-4023018537-2373006774-1847616786-512
 tokenGroups: S-1-5-21-4023018537-2373006774-1847616786-572
 tokenGroups: S-1-5-32-544
 tokenGroups: S-1-5-21-4023018537-2373006774-1847616786-518
 tokenGroups: S-1-5-21-4023018537-2373006774-1847616786-519
 tokenGroups: S-1-5-21-4023018537-2373006774-1847616786-520
 tokenGroups: S-1-5-21-4023018537-2373006774-1847616786-513
 tokenGroups: S-1-5-32-545

In this example the **<SID=** form of the DN is used, but the string can also be used, eg:

 ldbsearch -H ldap://$SERVER -s base -b "CN=Administrator,CN=Users,DC=addom,DC=samba,DC=example,DC=com" tokengroups -U$USERNAME%$PASSWORD

wbinfo --user-sids
------------------------

If you know the USER's SID, then if winbindd knows the group mebership (ie, there has been a login previously, either over NTLM/Kerberos to smbd, or wbinfo -a, you can obtain the same SID list with (eg):

 wbinfo --user-sids=S-1-5-21-4023018537-2373006774-1847616786-500
giving:
 S-1-5-21-4023018537-2373006774-1847616786-500
 S-1-5-21-4023018537-2373006774-1847616786-513
 S-1-5-21-4023018537-2373006774-1847616786-519
 S-1-5-21-4023018537-2373006774-1847616786-520
 S-1-5-21-4023018537-2373006774-1847616786-518
 S-1-5-21-4023018537-2373006774-1847616786-512
 S-1-5-21-4023018537-2373006774-1847616786-572
 S-1-5-32-544
 S-1-5-32-545