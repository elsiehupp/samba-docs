Managing the Samba AD DC Service Using an Init Script
    <namespace>0</namespace>
<last_edited>2020-09-22T13:36:59Z</last_edited>
<last_editor>Hortimech</last_editor>

<noinclude>
=================
Introduction
=================

The following describes how to use an init script to manage the Samba Active Directory (AD) domain controller (DC) service. Depending on your operating system, the location of the init script, its content, and the procedures how to manage the service can be different. For details, see your operating system's documentation.

.. warning:

   If you operating system uses a different system to manage services, such as ``systemd``, see `Managing_the_Samba_AD_DC_Service|Managing the Samba AD DC Service`.
</noinclude>

=================
Creating the Init Script
=================

Red Hat Enterprise Linux 6 
------------------------

.. note:

    On Red Hat Enterprise Linux later than version 6, use ``systemd`` to manage the Samba service. For details, see `Managing_the_Samba_AD_DC_Service_Using_Systemd|Managing the Samba AD DC Service Using Systemd`..

* Create the ``/etc/init.ba-ad-dc`` file with the following content:

 #!/bin/bash
 #
 # samba-ad-dc	This shell script takes care of starting and stopping
 # 		samba AD daemons.
 #
 # chkconfig: - 58 74
 # description: Samba Active Directory Domain Controller

 ### BEGIN INIT INFO
 # Provides: samba-ad-dc
 # Required-Start: $network $local_fs $remote_fs
 # Required-Stop: $network $local_fs $remote_fs
 # Should-Start: $syslog $named
 # Should-Stop: $syslog $named
 # Short-Description: start and stop samba-ad-dc
 # Description: Samba Active Directory Domain Controller
 ### END INIT INFO

 # Source function library.
 . /etc/init.d/fu.s
     # Source networking configuration.
 . /etc/sysconfig/network
     prog=samba
 prog_dir=/usr/local/samba/sbin/
 lockfile=/var/lock/subsys/$prog
     start() {
 	[ "$NETWORKING" = "no" ] && exit 1
 	echo -n $"Starting Samba AD DC: "
 	daemon $prog_dir/$prog -D
 	RETVAL=$?
 	echo
 	[ $RETVAL -eq 0 ] && touch $lockfile
 	return $RETVAL
 }

    top() {
 	[ "$EUID" != "0" ] && exit 4
 	echo -n $"Shutting down Samba AD DC: "
 	killproc $prog_dir/$prog
 	RETVAL=$?
 	echo
 	[ $RETVAL -eq 0 ] && rm -f $lockfile
 	return $RETVAL
 }
     case "$1" in
 start)
 	start
 	;;
 stop)
 	stop
 	;;
 status)
 	status $prog
 	;;
 restart)
 	stop
 	start
 	;;
 *)
 	echo $"Usage: $0 {start|stop|status|restart}"
 	exit 2
 esac

* Make the script executeable:

 # chmod 755 /etc/init.ba-ad-dc

Debian 
------------------------

* Create the ``/etc/init.ba-ad-dc`` file with the following content:

 #!/bin/sh

 ### BEGIN INIT INFO
 # Provides:          samba-ad-dc
 # Required-Start:    $network $local_fs $remote_fs
 # Required-Stop:     $network $local_fs $remote_fs
 # Default-Start:     2 3 4 5
 # Default-Stop:      0 1 6
 # Short-Description: start Samba daemons for the AD DC
 ### END INIT INFO

 #
 # Start/stops the Samba daemon (samba).
 # Adapted from the Samba 3 packages.
 #

 PATH=/usr/local/samba/sbin:Husr/local/samba/bin:$PATH:

 PIDDIR=/usr/local/samba/var/run
 SAMBAPID=$PIDDIR/samba.

 # clear conflicting settings from the environment
 unset TMPDIR

 # See if the daemon and the config file are there
 test -x /usr/local/samba/sbin/samba -a -r /usr/local/samba/etc/smb. || exit 0

 . /lib/lsb/init-functions

 case "$1" in
         start)
                 SERVER_ROLE=`samba-tool testparm --parameter-name="server role"  2>/dev/null | tail -1`
                 if [ "$SERVER_ROLE" != "active directory domain controller" ]; then
                     exit 0
                 fi

                 # CVE-2013-4475
                 KEYFILE=/usr/local/samba/private/tls/key.
                 if [ -e $KEYFILE ]; then
                     KEYPERMS=`stat -c %a $KEYFILE`
                     if [ "$KEYPERMS" != "600" ]; then
                         echo "wrong permission on $KEYFILE, must be 600"
                         echo "samba will not start (CVE-2013-4475)"
                         echo "Removing all tls . files will cause an auto-regeneration with the correct permissions.&quo.
                         exit 1
                     fi
                fi

                log_daemon_msg "Starting Samba AD DC daemon" "samba"
                # Make sure we have our PIDDIR, even if it's on a tmpfs
                install -o root -g root -m 755 -d $PIDDIR
                    if ! start-stop-daemon --start --quiet --oknodo --exec /usr/local/samba/sbin/samba -- -D; then
                    log_end_msg 1
                    exit 1
                fi

                log_end_msg 0
                ;;
        stop)
                log_daemon_msg "Stopping Samba AD DC daemon" "samba"

               start-stop-daemon --stop --quiet --pidfile $SAMBAPID
                # Wait a little and remove stale PID file
                sleep 1
                if [ -f $SAMBAPID ] && ! ps h `cat $SAMBAPID` > /dev/null
                then
                    # Stale PID file (samba was succesfully stopped),
                    # remove it (should be removed by samba itself IMHO.
                    rm -f $SAMBAPID
                fi

               log_end_msg 0

                ;;
        restart|force-reload)
                $0 stop
                sleep 1
                $0 start
                ;;
        status)
                status_of_proc -p $SAMBAPID /usr/local/samba/sbin/samba samba
                exit $?
                ;;
        *)
                echo "Usage: /etc/init.ba-ad-dc {start|stop|restart|force-reload|status}"
                exit 1
                ;;
 esac

 exit 0

* If necessary, update the locations to the ``samba`` service, the ``samba-tool`` utility, and the ``smb.lt;/code> file in the ``/etc/init.d/sa.-dc`` file..

* Make the script executeable:

 # chmod 755 /etc/init.ba-ad-dc

=================

Managing the Samba AD DC Service
=================

The following assumes that the Samba Active Directory (AD) domain controller (DC) service is managed by the ``/etc/init.ba-ad-dc`` init script. .If you have not created the script manually, see your operating system's documentation for the name of the Samba AD DC service..

.. note:

    Depending on your operating system, there can be different ways to enable or disable a service. See your operating system's documentation for details..

Enabling and Disabling the Samba AD DC Service 
------------------------

To enable the Samba Active Directory (AD) domain controller (DC) service to start automatically when the system boots, enter:

Red Hat Enterprise Linux 6 
------------------------

 # chkconfig samba-ad-dc enable

To disable the automatic start of the Samba AD DC service, enter:

 # chkconfig samba-ad-dc disable

Debian 
------------------------

 # update-rc. samba-ad-dc defaults

To disable the automatic start of the Samba AD DC service, enter:

 # update-rc. -f samba-ad-dc remove

Manually Starting and Stopping the Samba AD DC Service 
------------------------

To manually start the Samba Active Directory (AD) domain controller (DC) service, enter:

 # service start samba-ad-dc

To manually stop the Samba AD DC service, enter:

 # service stop samba-ad-dc