Using Samba with JumpCloud Authentication
    <namespace>0</namespace>
<last_edited>2018-02-21T09:04:49Z</last_edited>
<last_editor>Fraz</last_editor>

It took me a while to figure out how to set this up, so I thought I'd make this wiki page to help others do the same.
* [https://jumpcloud.com/daas-product/samba-authentication Samba File Server and NAS Authentication]

Setting up JumpCloud 
------------------------

Log into JumpCloud as an administrator and create a new user to function as a service user to connect with. Then, on the left, go to 'Directories' and go to the JumpCloud LDAP directory. In the LDAP directory, click the checkbox that says 'Configure Samba Authentication', and set the domain to whatever you want to use as the domain name. Once you're done with that, make a new user group for users allowed to authenticate in Samba, and check 'Enable Samba Authentication' on that group. Add any users you want to be allowed to authenticate in Samba to this group.

Setting up Samba 
------------------------

Paste the below into your /etc/samba/smb.conf, changing values as necessary. I put the values you need to change in caps. You can find your organization's ID

.. code-block::

    [global]
        log file = /var/log/samba/%m
        log level = 1
        workgroup = DOMAIN_SET_IN_JUMPCLOUD
        netbios name = DOMAIN_SET_IN_JUMPCLOUD
        server role = standalone server
        passdb backend = ldapsam:AASS:HHldap.jumpcloud.com:389:
        ldap ssl = start tls
        ldap suffix = o=JUMPCLOUD_ORG_ID,dc=jumpcloud,dc=com
        ldap admin dn = uid=SERVICE_USER'S_USERNAME,ou=Users,o=JUMPCLOUD_ORG_ID,dc=jumpcloud,dc=com
        ldap user suffix = ou=Users
        ldap passwd sync = yes
Once you do that, run 'smbpasswd -W' a root and enter the password for your service user, then (re)start your Samba server. You should now be able to use ``smbclient -L`` and authenticate using credentials for a user in the group you created earlier.

Note that, although I haven't tried it, I am pretty sure you cannot use Samba as a domain controller with this configuration