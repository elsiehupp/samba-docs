Authenticating Freeradius against Active Directory
    <namespace>0</namespace>
<last_edited>2020-04-15T08:53:53Z</last_edited>
<last_editor>Thctlo</last_editor>

===============================

Introduction
===============================

This has been tested on:

* Samba 4.7.6 AD DC (2 of them), compiled from source, on centos 7

* Freeradius 3.0.13 + samba 4.6.2 as domain member, packages straight from centos repo.

* Freeradius 3.0.14 and samba 4.7.x

* Debian Buster, Samba 4.10.7, Freeradius 3.0.17/3.0.19

=================
Configuration
=================
**With Samba 4.2.x and up, use  the following setting on the freeradius server and on all the Samba AD-DC's:**

Add to the [global] section:

 ntlm auth = mschapv2-and-ntlmv2-only

Ensure the server is added to AD with net ads join.

You need to configure Freeradius to use mschapv2 with ntlmv1 disabled globally by setting this in /mods-available/mschap:

 mschap {

 .....

 ntlm_auth = "/path/to/ntlm_auth **--allow-mschapv2** --request-nt-key 
 --username=%{mschap:User-Name} --domain=WINDOWSDOMAIN 
 --challenge=%{%{mschap:Challenge}:-00} 
 --nt-response=%{%{mschap:NT-Response}:-00}"

OR (if your Freeradius supports it)

 winbind_username = "%{mschap:User-Name}"
 winbind_domain = "%{mschap:NT-Domain}"

The former should work without modification to freeradius, the latter requires freeradius to be built with winbind auth. For example: on Centos you will have to rebuild the rpm and add the winbind libraries to the ./configure path.

This is all that is required to change from the "standard", well documented freeradius/AD integration for the integration.

If there are going to be password changes with freeradius ("your password has expired" - type - scenarios) You should probably also configure in /mods-available/mschap:

 passchange {
 ntlm_auth = "/path/to/ntlm_auth 
 --helper-protocol=ntlm-change-password-1 **--allow-mschapv2**
 ntlm_auth_username = "username: %{mschap:User-Name}
 ntlm_auth_domain = "nt-domain: %{mschap:NT-Domain}"

With the settings above it works correctly, so even if it is unnecessary, it doesn't break anything. It hasn't been tested without this option while denying ntlmv1 overall on the AD DC, but it is thought that it will work without it.

=================
Testing
=================
If everything works as intended, you should see in the AD DC audit log something like this:
 {"timestamp": "some-date0", "type": "Authentication", "Authentication": 
 {"version": {"major": 1, "minor": 0}, "status": "NT_STATUS_OK", 
 "localAddress": "ipv4:xxx.xxx.xxx.xxx", "remoteAddress": 
 "ipv4:xxx.xxx.xxx.xxx:58046", "serviceDescription": "SamLogon", 
 "authDescription": "network", "clientDomain": "WINDOWSDOMAIN", 
 "clientAccount": "some-user", "workstation": "\\\\SOME-HOST", 
 "becameAccount": "some-user", "becameDomain": "WINDOWSDOMAIN", 
 "becameSid": "SOME-SID", "mappedAccount": "some-user", "mappedDomain": 
 "WINDOWSDOMAIN", "netlogonComputer": "SOME-HOST", 
 "netlogonTrustAccount": "SOME-HOST$", "netlogonNegotiateFlags": 
 "0x610FFFFF", "netlogonSecureChannelType": 2, "netlogonTrustAccountSid": 
 "somesid, *"passwordType": "MSCHAPv2"*}}

Without the "--allow-mschapv2" setting, you would see "passwordType":"NTLMv1".

The information for this wiki page was supplied by Kacper Wirski

----
`Category:Active Directory`