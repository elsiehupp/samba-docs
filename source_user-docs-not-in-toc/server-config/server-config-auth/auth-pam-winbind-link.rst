Pam winbind Link
    <namespace>0</namespace>
<last_edited>2017-09-29T14:01:56Z</last_edited>
<last_editor>Hartnegg</last_editor>

===============================

Introduction
===============================

To enable users to authenticate to an NT4 or Active Directory (AD) domain, PAM must be able to locate the ``pam_winbind.so`` library provided by Samba. Create a link in the PAM modules directory to enable PAM to use Winbind.

.. note:

    Do not copy the library to the directory. Otherwise you must replace it manually after every Samba update.

=================
Determining the Platform
=================

To determine the operating system's platform:

 # uname -m

=================
Verifying If PAM Support Is Enabled
=================

To verify if Samba was build with PAM support, enter:

 # smbd -b | grep "WITH_PAM"
    WITH_PAM

If the ``WITH_PAM`` option is not shown in the command's output, Samba was built without PAM support, because:
* The ``configure`` script failed to locate the PAM headers.
* The ``--without-pam`` option was passed to the ``configure`` script.

To enable PAM support:
* Install the PAM headers. For example, on Red Hat Enterprise Linux, the headers are provided in the ``pam-devel`` package.
* Rebuild Samba without passing the ``--without-pam`` option to the ``configure`` script. For details about building Samba, see `Build_Samba_from_Source|Build Samba from Source`.

=================
Locating the ``pam_winbind.so`` Library
=================

If you built Samba, the location of the ``pam_winbind.so`` library was set using the ``configure`` script's ``--with-pammodulesdir`` parameter. The default location is ``/usr/local/samba/lib/security/pam_winbind.so``.

To locate the library, run:

 # find / -type f -name pam_winbind.so
 /usr/local/samba/lib/security/pam_winbind.so

Link the library from this directory to your operating system's PAM module directory.

=================
Operating System-specific Examples
=================

Red Hat-based Operating Systems 
------------------------

x86_64
------------------------

 # ln -s /usr/local/samba/lib/security/pam_winbind.so /lib64/security/

i686
------------------------

 # ln -s /usr/local/samba/lib/security/pam_winbind.so /lib/security/

Debian-based Operating Systems 
------------------------

x86_64
------------------------

 # ln -s /usr/local/samba/lib/security/pam_winbind.so /lib/x86_64-linux-gnu/security/

i686
------------------------

 # ln -s /usr/local/samba/lib/security/pam_winbind.so /lib/i386-linux-gnu/security/

SUSE-based Operating Systems 
------------------------

x86_64
------------------------

 # ln -s /usr/local/samba/lib/security/pam_winbind.so /lib64/security/

i686
------------------------

 # ln -s /usr/local/samba/lib/security/pam_winbind.so /lib/security/