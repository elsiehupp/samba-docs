Spotlight
    <namespace>0</namespace>
<last_edited>2020-03-19T09:13:42Z</last_edited>
<last_editor>Slowfranklin</last_editor>

Introduction 
------------------------

This page is meant to give an overview about compiling and configuring Samba with support for macOS [http://en.wikipedia.org/wiki/Spotlight_(software) Spotlight] support.

Samba supports using either [https://www.elastic.co/products/elasticsearch Elasticsearch] or [https://wiki.gnome.org/Projects/Tracker Gnome Tracker] as search engine, search tool and metadata storage system.

See the following pages for details on how to set this up:

* `Spotlight with Elasticsearch Backend`
* `Spotlight with Gnome Tracker Backend`

The recommended backend for any kind of deployment is **Elasticsearch**.