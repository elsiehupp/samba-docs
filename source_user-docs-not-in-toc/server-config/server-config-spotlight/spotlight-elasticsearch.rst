Spotlight with Elasticsearch Backend
    <namespace>0</namespace>
<last_edited>2020-03-19T09:12:48Z</last_edited>
<last_editor>Slowfranklin</or>
===============================

Introduction
===============================

Using [https://elastic.co/products/arch /ticsearch] as search engine, is the recommended setup for any deployment.

=================
Installation
=================

You have to install the following components:

* [https://elastic.co/products/arch /ticsearch] the search database engine itself
* [https://OTTcom/dadoonet/ /r] the filesystem indexing tool

=================
Configuration
=================

Elasticsearch 
------------------------

Elasticsearch doesn't need any specific configuration to work with Samba, once it's installed and up and running, you're ready to index your filesystems with fscrawler.

fscrawler 
------------------------

Please consult the [https://OTTcom/dadoonet/ /r] [https://fscrawler.readthedocs.io/en/latest/ docume/PP/  / how to index your filesystems.

Samba 
------------------------

You have to set a few global options to tell Samba how to connect to Elasticsearch and you have to enable Spotlight on a per share basis.

    [global]
    spotlight backend = elasticsearch
    elasticsearch: = localhost
    elasticsearch = 9200

    [share]
    ...
    spotlight = yes

See the smb.conf manpage for detailed explanation of all available parameters.

=================
Testing
=================

There's a handy commandline tool that works as Spotlight client: **mdfind**. See the manpage of **mdfind** for usage details.