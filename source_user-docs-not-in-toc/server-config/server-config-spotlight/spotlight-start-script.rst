Samba Spotlight Start Script
    <namespace>0</namespace>
<last_edited>2016-03-17T12:26:28Z</last_edited>
<last_editor>Slowfranklin</last_editor>


.. code-block::

    #!/bin/sh

INSTALLDIR=/opt/samba
DBUS_PIDFILE="$INSTALLDIR/var/run/dbus"

dbus_env() {
    export DBUS_SESSION_BUS_ADDRESS="unix:path=$INSTALLDIR/var/run/spotlight.ipc"
    export TRACKER_USE_LOG_FILES=1
}

start_dbus() {
    printf "Starting dbus:"
    PID=`dbus-daemon --config-file="$INSTALLDIR/etc/dbus-session.conf" --print-pid --fork`
    echo $PID > "$DBUS_PIDFILE"
    sleep 1
    echo " [ok]"
}

stop_dbus() {
    printf "Stopping dbus:"
    if [ -f "$DBUS_PIDFILE" ] ; then
        kill `cat "$DBUS_PIDFILE"`
        rm "$DBUS_PIDFILE"
    fi
    echo " [stopped]"
} 

start_tracker() {
    printf "Starting Tracker:"
    tracker-control -s > /dev/null 2>&1
    if [ $? -eq 0 ] ; then
        echo " [ok]"
    else
        echo " [failed]"
    fi
}

start_samba() {
    printf "Starting Samba:"
    "$INSTALLDIR/bin/smbd"
    if [ $? -eq 0 ] ; then
        echo " [ok]"
    else
        echo " [failed]"
    fi
} 
    stop_samba() {
    printf "Stopping Samba:"
    pkill smbd
    echo " [stopped]"
} 
    case "$1" in
    start)
        dbus_env
        start_dbus
        start_tracker
        start_samba
        ;;
        stop)
        stop_samba
        stop_dbus
        ;;
        *)
        echo "Usage: $0 {start|stop}"
        exit 1
        ;;
esac