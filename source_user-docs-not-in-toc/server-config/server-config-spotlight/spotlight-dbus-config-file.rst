Spotlight DBUS Configuration File
    <namespace>0</namespace>
<last_edited>2016-04-07T11:54:50Z</last_edited>
<last_editor>Slowfranklin</last_editor>


.. code-block::

    <!DOCTYPE busconfig PUBLIC "-//freedesktop//DTD D-Bus Bus Configuration 1.0//EN"
 "http://www.freedesktop.org/standards/dbus/1.0/busconfig.dtd">
<busconfig>
    !-- Our well-known bus type, don't change this -->
    type>session</type>

    !-- If we fork, keep the user's original umask to avoid affecting
       the behavior of child processes. -->
    keep_umask/>

    listen>unix:AASSHHvar/run/samba/spotlight.ipc</listen>

    standard_session_servicedirs />

    policy context="default">
    <allow user="*"/>
    <allow own="*"/>
    <allow send_destination="*" eavesdrop="true"/>
    <allow receive_sender="*"/>
    <allow eavesdrop="true"/>
    /policy>

    !-- the memory limits are 1G instead of say 4G because they can't exceed 32-bit signed int max -->
    limit name="max_incoming_bytes">1000000000</limit>
    limit name="max_outgoing_bytes">1000000000</limit>
    limit name="max_message_size">1000000000</limit>
    limit name="service_start_timeout">120000</limit>  
    limit name="auth_timeout">240000</limit>
    limit name="max_completed_connections">100000</limit>  
    limit name="max_incomplete_connections">10000</limit>
    limit name="max_connections_per_user">100000</limit>
    limit name="max_pending_service_starts">10000</limit>
    limit name="max_names_per_connection">50000</limit>
    limit name="max_match_rules_per_connection">50000</limit>
    limit name="max_replies_per_connection">50000</limit>

</busconfig>