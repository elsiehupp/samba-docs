Enabling the Sysvol Share on a Windows DC
    <namespace>0</namespace>
<last_edited>2017-05-18T14:01:17Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

===============================

Introduction
===============================

When you join Windows Server as a domain controller (DC), Windows tries to replicate the content of the ``Sysvol`` share from an existing DC. Because Samba currently does not support the DFS-R protocol, the content is not replicated and therefore Windows does not enable the ``Sysvol`` share..

This documentation describes how to enable the share manually after you joined the DC to the domain. However, this procedure does not enable Sysvol replication. .For a replication workaround between Samba and Windows DCs, see `Robocopy_based_SysVol_replication_workaround|Robocopy-based Sysvol Replication`..

=================
Verifying if the Sysvol Share Exists
=================

* Log in to the Windows DC using a domain administrator account.

* Open ``\\*windows_server_name*\``.

If the ``Sysvol`` share exists, it is displayed in the list of shares on this server.

=================
Enabling the Sysvol Share on a Windows DC
=================

To enable the ``Sysvol``share:

* Log in to the Windows DC using a domain administrator account.

* Save the following content as plain text in a file named, for example, ``Win-Create-Sysvol-Share.t;/code>:

 Windows Registry Editor Version 5.

 [HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Netlogon\Parameters]

 "SysvolReady"=dword:

* 
| type = important
| text = Use a text editor that stores files in plain text, such as ``Editor`` or ``Notepad``.

* Double-click the file to import the setting to the Windows registry. 

* Reboot the server to take the changes effect.