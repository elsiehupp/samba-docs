The SYSTEM Account
    <namespace>0</namespace>
<last_edited>2017-05-06T20:33:41Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

=================
The ``SYSTEM`` Account
=================

The ``SYSTEM`` account uses the ``S-1-5-18`` security ID (SID). Because the SID does not contain the domain SID, the account only exists locally in a Windows and Samba installation. The ``SYSTEM`` account is also named ``LocalSystem`` or ``NT AUTHORITY\SYSTEM``.

In Windows, ``SYSTEM`` is used, for example, by local services on the Windows host to access files on the local file system. Because the ``SYSTEM`` account exists in every Windows installation, has no password set, and in most cases has ``Full Control`` permissions on local NTFS file systems, it would be a security issue, if this account could be used to authenticate to network resources and access files. If local services that use the ``SYSTEM`` account access network resources, the local machine's network account (``*domain*\*computername$*``) is used to authenticate to the network. 

How the ``SYSTEM`` Account Is Used by a Windows Service 
------------------------

The following example describes how a Windows Active Directory (AD) domain member downloads and applies group policy objects (GPO):
# The local ``Group Policy Client`` service starts. The service is executed locally using the ``SYSTEM`` account.
# The service authenticates to the domain controller's ``Sysvol`` share using local machine's account within the domain. For example, ``*domain*\*computername$*``.
# If authentication was successful, the services downloads the ``Computer Configuration`` part of the GPOs.
# On the domain member, the service updates the registry and file system using the ``SYSTEM`` account.

.. note:

    The ``SYSTEM`` account is never sent to a remote host to authenticate and for this reason never used to access a remote file system.

=================
Using the ``SYSTEM`` Account in File System ACLs on a Samba Server
=================

On Windows operating systems using the defaults, the ``SYSTEM`` account has ``Full Control`` permissions granted on the local NTFS system drive. Additionally, documentation often advices to add the account to the file system access control lists (ACL) to enable local services, that are using this account, to access files.

To be consistent with Windows, the internal ``SYSTEM`` account also exists in Samba and you can use it when `Setting_up_a_Share_Using_Windows_ACLs#Setting_ACLs_on_a_Folder|setting file system permissions using Windows ACLs`. However, on a Unix host that runs Samba, the ``SYSTEM`` account is neither used by Samba, nor available to the operating system. Therefore, you cannot run local services on the Samba host using the ``SYSTEM`` account.

From the perspective of a Samba server you can omit the ``SYSTEM`` account in file system ACLs. However, certain Windows services validate ACLs on shares and expect defined ACLs, even if they are not explicitely used. If ``SYSTEM`` is not listed in the remote server's ACLs, using the share can fail, even if the user is allowed to access the required content. For example, this applies to:

* the ``Sysvol`` share
* user roaming profile shares

.. warning:

   For compatibility with Windows, add the ``SYSTEM`` account to file system ACLs.

=================
Further Resources
=================

For further details about the ``SYSTEM`` account and how it is used in Windows, see the following Microsoft documentation:
* [https://support.microsoft.com/en-us/help/120929/how-the-system-account-is-used-in-windows How the System account is used in Windows]
* [https://msdn.microsoft.com/en-us/library/windows/desktop/ms684190(v=vs.85).aspx LocalSystem Account]
* [https://support.microsoft.com/en-us/help/243330/well-known-security-identifiers-in-windows-operating-systems Well-known security identifiers in Windows operating systems]