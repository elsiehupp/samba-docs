Setting up a Windows failover cluster
    <namespace>0</namespace>
<last_edited>2020-11-11T14:04:50Z</last_edited>
<last_editor>Aaptel</last_editor>


* .. warning:

   This page is a draft

Introduction 
------------------------

Setting up a Windows cluster requires at least three virtual machines and two networks.

These instructions assume KVM and libvirt are used. Windows version is Server 2019.

 <nowiki>
				+-------------+
				| SMB  Client |
				+-------------+
				       |
				192.168.150.201
				       |
	+------------------------

------------------------

---------------+
	|			Clients network				|
	+------------------------

------------------------

---------------+
		|                      |                       |
	 192.168.150.10		192.168.150.21		192.168.150.22
		|                      |                       |
	+---------------+       +------------+          +------------+
	|   AD + iSCSI  |       |   Node 1   |          |   Node 2   |
	| target server |       +------------+          +------------+
	+---------------+              |                       |
                                       |                       |
                                192.168.160.21          192.168.160.22
                                       |                       |
	+------------------------

------------------------

---------------+
	|		        Cluster network                         |
	+------------------------

------------------------

---------------+
</nowiki>

{| class="wikitable"
!VM name
!Hostname
!colspan=2|IP addresses
|-
|win2k19-fover-ad
|fover-ad
|192.168.150.10
|
|-
|win2k19-fover-n1
|fover-n1
|192.168.150.21
|192.168.160.21
|-
|win2k19-fover-n2
|fover-n2
|192.168.150.22
|192.168.160.22
|}

Setup libvirt networks 
------------------------

The first step is to create the public and private networks in libvirt. Save the following fragment to a file named ``cluster-public.xml``:

 <nowiki>
<network>
    name>cluster-public</name>
    forward mode='nat'>
    <nat>
      <port start='1024' end='65535'/>
    </nat>
    /forward>
    domain name='cluster-public'/>
    dns>
    <forwarder domain='fover.net' addr='192.168.150.10'/>
    /dns>
    ip address='192.168.150.1' netmask='255.255.255.0'>
    <dhcp>
      <range start='192.168.150.200' end='192.168.150.250'/>
    </dhcp>
    /ip>
</network>
</nowiki>

Save the following fragment to ``cluster-private.xml``:
 <nowiki>
<network>
    name>cluster-private</name>
    domain name='fover.net'/>
    dns>
    <forwarder domain='fover.net' addr='192.168.160.10'/>
    /dns>
    ip address='192.168.160.1' netmask='255.255.255.0'>
    <dhcp>
      <range start='192.168.160.200' end='192.168.160.250'/>
    </dhcp>
    /ip>
</network>
</nowiki>

Now define and start the networks:

 <nowiki>
# virsh net-define cluster-public.xml
# virsh net-define cluster-private.xml
# virsh net-start cluster-public
# virsh net-start cluster-private
</nowiki>

Setup AD virtual machine 
------------------------

After installing the operating system, **set the host name** and **IP addresses**. It is useful to rename the network adapters to know to which network are connected.

{| class="wikitable"
!
!Public
!Private
|-
!IP Address
|192.168.150.10
|192.168.160.10
|-
!Netmask
|255.255.255.0
|255.255.255.0
|-
!Gateway
|192.168.150.1
|
|-
!DNS 1
|192.168.150.1
|
|-
!DNS 2
|
|
|}

Proceed to install the **Active Directory Domain Services** and **iSCSI target server** roles:

`File:Setting_up_a_Windows_failover_cluster_ad_install_roles.png`

Once roles are installed, provision the Active Directory domain following the Wizard:

`File:Setting_up_a_Windows_failover_cluster_ad_domain_provision.png`

Setup cluster nodes 
------------------------

In both nodes:

<ol>
<li> Install the operating system</li>
<li> Set the hostname</li>
<li> Set the IP addresses</li>
{| class="wikitable"
!
!colspan="2"|fover-n1
!colspan="2"|fover-n2
|-
!
!Public
!Private
!Public
!Private
|-
!IP Address
|192.168.150.21
|192.168.160.21
|192.168.150.22
|192.168.160.22
|-
!Netmask
|255.255.255.0
|255.255.255.0
|255.255.255.0
|255.255.255.0
|-
!Gateway
|192.168.150.1
|
|192.168.150.1
|
|-
!DNS 1
|192.168.150.10
|
|192.168.150.10
|
|-
!DNS 2
|
|
|
|
|}
<li>Join the computer to the domain</li>
<li>Install the "File Server" role and the "Failover Clustering" feature.
</ol>

Setup the iSCSI target server 
------------------------

In fover-ad:

# Open the Server Manager console and go to "File and Storage Services" -> "iSCSI"
# Click TASKS -> New iSCSI virtual disk
# Follow the "New iSCSI Virtual Disk Wizard"
## iSCSI Virtual Disk Location
##; Select C:
## iSCSCI Virtual Disk Name
##; Name: quorum
##; Description: quorum witness disk</il>
## iSCSI Virtual Disk Size
##; 128 MB, Dynamically expanding
## iSCSI target
##; Select New iSCSI target
## Target Name and Access
##; Name: fover
##; Description: Fover cluster iSCSI target server
## Access Server
### Click add
### Select Query initiator computer for ID
### Type the node 1 host name (fover-n1.fover.net)
### Click OK
### Click add
### Select Query initiator computer for ID
### Type the node 2 host name (fover-n2.fover.net)
### Click OK
## Enable authentication
##; Do not enable
## Confirm

`File:Setting_up_a_Windows_failover_cluster_ad_iscsi_target_server.png`

Setup the iSCSI initiators 
------------------------

In each node:
# Open the Server Manager
# Click Tools -> iSCSI initiator
# It will ask to start the service, click yes
# In the "Targets" tab, type "fover-ad.fover.net" and click Quick Connect
# In the "Volumes and Devices" tab, click Auto Configure
# Click OK

`File:Setting_up_a_Windows_failover_cluster_node_iscsi_initiator.png`

Initialize the quorum disk 
------------------------

In one of the nodes:
# Open the Server Manager
# Go to File and Storage Services -> Disks
# Right click the 128MB disk, select bring online
# Right click the 128MB disk, select Initialize
# Right click the 128MB disk, select New Volume
# Follow the "New Volume Wizard", do not assign a letter

`File:Setting_up_a_Windows_failover_cluster_node_quorum_disk.png`

Create the cluster 
------------------------

# Open the Server Manager
# Go to Tools -> Failover Cluster Manager
# In the right column, click Validate Configuration
# Follow the Validate Configuration Wizard
## Select Servers
##: Type the name of the first node, fover-n1.fover.net and click add
##: Type the name of the second node, fover-n2.fover.net and click add
## Testing options
##: Select Run all tests
## Check the results
##: Everything should succeed, you may get a warning if the nodes do not have the same updates installed
## Select the "Create the cluster now using the validated nodes" and click finish
# Follow the "Create Cluster Wizard"
## Access point for administering the cluster
##: Type a name for the cluster: "cluster"
##: Type an address associated to the cluster name: "192.168.150.30"
## Confirmation page
##: Select "Add all elegible storage to the cluster"
## Confirm

Check the Quorum is properly configured. You should see the 128MB disk assigned to "Disk Witness in Quorum"

`File:Setting_up_a_Windows_failover_cluster_node_disk_witness.png`

A new computer account "cluster" will be created in AD, and a new A record added to the DNS zone. This new computer account needs permission to create and modify computer objects in the AD computers organizational unit. To grant this permission:

# In fover-ad, open the Active Directory Users and Computers management tool
# Click View, enable Advanced features
# Expand the domain, right click in the "Computers" OU and select Properties
# Select the Security tab, click add
# Click "Object Types", Select Computers, click OK
# Type "cluster", click OK
# Click cluster, and grant "Write, Create all child objects and Delete all child objects"
# Click OK

`File:Setting_up_a_Windows_failover_cluster_ad_grant_permissions.png`

Create the clustered file server 
------------------------

Create a new iSCSI disk for the file server
------------------------

# In AD, open the Server Manager
# Go to File and Storage Services -> iSCSI
# Click Tasks -> New iSCSI virtual disk
# Follow the New iSCSI Virtual Disk Wizard:
## iSCSI Virtual Disk Location
##: Click Select by volume and select C:
## iSCSCI Virtual Disk Name
##: Name: fs
##: Description: Disk for file server role
## iSCSI Virtual Disk Size
##: 2 GB, Dynamically expanding
## iSCSI Target
##: Select Existing iSCSI target and select 'fover'
## Click create

`File:Setting_up_a_Windows_failover_cluster_ad_fs_disk.png`

Initialize the iSCSI disk
------------------------

# In one of the nodes, open Server Manager
# Go to Tools -> iSCSI Initiator
# Go to the Volumes and Devices tab and click Auto Configure
# The new disk will appear in the list. Click OK to close the iSCSI initiator
# Go to "File and Storage Services" -> Disks
# If the new disk does not appear, click Tasks -> Refresh
# Right click the 2GB Disk, Bring online
# Right click the 2GB Disk, Initialize
# Right click the 2GB Disk, New Volume
# Follow the "New Volume Wizard", assign a drive letter

`File:Setting_up_a_Windows_failover_cluster_ad_fs_volume.png`

Add the iSCSI disk to the cluster storage
------------------------

# In one of the nodes, open the Failover Cluster Manager
# In the left column, expand the cluster, expand Storage and select Disks
# In the right column, click Add Disk
# Select the disk and click OK
# The disk is listed as "Available Storage"

`File:Setting_up_a_Windows_failover_cluster_node_fs_disk_assigned.png`

Create the role
------------------------

# In one of the nodes, open the Failover Cluster Manager
# In the left column, expand the cluster and select Roles
# In the right column, click "Configure Role"
# Follow the "High Availabiliry wizard"
## Select Role
##: Select "File Server"
## File Server Type
##: Select "File Server for general use"
## Client Access Point
##: Name: fs
##: IP Address: 192.168.150.31
## Select Storage
##: Select the disk
## Confirm

`File:Setting_up_a_Windows_failover_cluster_node_fs_role.png`

Create a high available share
------------------------

# In one of the nodes, open the Failover Cluster Manager
# In the left column, expand the cluster and select Roles
# Click the File Server (fs), and in the right column click "Add File Share"
# Follow the "New Share Wizard"
## Select Profile
##: Select "SMB Share - Quick"
## Share Location
##: Select by Volume, and select E: (The letter assigned to the volume created in previous steps)
## Share Name
##: Name: share1
## Other Settings
##: Enable Continous availability
## Permissions
##: Click Customize permissions and grant Domain Users write access to the share
## Confirm

`File:Setting_up_a_Windows_failover_cluster_node_fs_share.png`