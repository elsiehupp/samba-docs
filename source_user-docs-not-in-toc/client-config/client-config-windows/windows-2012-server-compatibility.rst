Windows 2012 Server compatibility
    <namespace>0</namespace>
<last_edited>2021-04-13T00:58:26Z</last_edited>
<last_editor>Abartlet</last_editor>

__TOC__

===============================

Introduction
===============================

There are a number of different ways that Samba can be considered compatible with Windows and so this page attempts to try to explain some of them (and which ones might be regarded as important). A number of these details will obviously apply more generally to other Windows versions.

=================
SMB protocol features
=================

As Windows 2012 (and 2012 R2) ships with a particular version of SMB, clients which expect to negotiate a certain version may see differences between Windows and Samba. SMB allows for many optional features which are negotiated and servers generally support multiple versions of SMB for interoperability with different clients. This means that servers and clients will speak a wide variety of flavours of SMB, meaning interoperability issues with Samba are generally limited to individual applications and use-cases which have stricter requirements on their SMB connections (encryption and supported ciphers, resilient handles).

=================
RPC server features
=================

This is similar to SMB, many calls or structures have been deprecated over time. In many cases Samba does not implement every call, or has calls which do nothing. There may even be entire RPC pipes which are unimplemented, although their functionality is reproduced in some other way e.g. eventlog6 logging.

`Category:rectory`

=================
Active Directory Domain member
=================

Joining Windows as a domain member to a Samba domain
------------------------

The process for this is described in the following page:

* `a Windows Client or Server to a Domain`

This is generally expected to work without any special effort (compared to a Windows domain), with the supported versions listed here:

* `a Windows Client or Server to a Domain#Supported Windows Versions|Supported Windows Versions for joining a domain`

Joining Samba as a domain member to a Windows domain
------------------------

The instructions for joining any Active Directory domain remain the same between a Windows AD and a Samba AD.

* `up Samba as a Domain Member`

=================
Active Directory Domain controller
=================

Overview 
------------------------

When considering the compatibility of domain controllers, there are least three initial aspects that must be considered:

* The schema level

* The domain (or forest) preparation level

* The functional level

On the Windows platform, all three of these are resolved by a tool called adprep.exe. In previous versions, this was run manually by administrators, but in newer versions (2012+), this is automatically run by domain controller promotion on Windows. Unfortunately, adprep itself and the newer methods they use to invoke it are both incompatible with Samba. However, there is still a work-around for Samba, which is explained more `Windows 2012 Server compatibility#adprep_workaround|below`.

In Samba, trust support isn't yet complete and so in general terms, the domain is often considered the forest.

 Schema level =
===============================

------------------------

With new versions of Windows, applications may require new objects whose schema description is not stored in Active Directory. Newer domain controllers might want to provide extra functionality which is specific to that controller and is not broadly available (or advertised) across the domain. Similarly newer versions available to client machines may also wish to record additional details and provide additional functionality. Therefore, the most basic requirement for these applications is an updated schema which supports classes and attributes that allow this information to be stored in the directory.

In practice, domain controllers seem to rely on both the schema level and the preparation level before they will even interact with domain controllers running other versions. Updating schema versions should be backwards-compatible with every version of Windows (for instance, 2012 R2 schema should work on 2008 DCs), as they do not imply functionality across the domain (only behaviour local to the instance).

From Samba 4.11, our default schema is 2012 R2. Prior to Samba 4.11, our default schema was 2008 R2. For those running 2008 R2 schema and want to upgrade to 2012 R2 schema, you may run the 'samba-tool domain schemaupgrade' command. 

* ma Version Support`

Although a newer schema will work on an older DC, Windows doesn't appear to support running an older schema on a newer DC. For example, when joining a newer DC (e.g. 2012 R2) to an older domain (e.g. 2008 R2) Windows requires that the domain is upgraded to the newer schema (2012 R2). Samba has no such requirement in order to join a Windows domains (and likely isn't ever going to).

 Preparation level =
===============================

------------------------

Where schema defines what objects can be created in the domain, the preparation step ensures that any directory differences required by a newer functional level are enforced. For instance, certain containers or users might be necessary in newer functional versions (of Active Directory).

For example, the 2008 R2 schema added the msDS-PasswordSettingsContainer schema object for the `Password_Settings_Objects|PSO` feature. Just updating the schema level means that the DC now understands this type of object, were it to exist. However, upgrading the preparation level would involve actually creating the 'CN=Password Settings Container' msDS-PasswordSettingsContainer object, which is required in order for the PSO feature to actually operate.

Joining a Windows 2012 R2 DC to a 2008 domain requires not only that the schema level is 2012 R2, but also that the preparation level is too. (If Windows DCs are in the domain being upgraded, this should be done remotely by adprep during the schema upgrade process).

 Functional level =
===============================

------------------------

Functional level basically describes what version of Active Directory you are running (which in Windows is tied to the OS version, but with newer OS able to run at earlier functional levels). Setting the functional level simply sets a number stored in the directory which DCs will read and alter their behaviour accordingly. All the work required to exhibit the functional level behaviour is either done beforehand (as schema or preparation), or done as a runtime switch on this number. Altering this number without using the correct tools is **NOT** recommended.

{{Imbox
| type = warning
| text = Samba does not currently support any attempts to update the functional level

The samba-tool command to raise your functional level is not safe to use against Samba (and probably not even safe against Windows either). The tool is currently incomplete in that it does not check for the appropriate schema version or preparation level before deciding to change the functional level. Please do not run this! If changing the functional level is necessary, using Windows to perform the change is required.
* the Functional Levels`

 FSMO roles =
===============================

------------------------

This section lists the overall status of our FSMO roles. Although Samba does not implement all of them entirely, such operations that go through the FSMO may be quite rare, or in the case of only Samba, perhaps impossible. Not many people would be running (large) mixed domains in the long term and if they are, their domain should be primarily Windows or primarily Samba to avoid issues in compatibility but also with their expectations.

; Schema master (forest):ndles the schema master role but certain aspects of Windows may assume that the schema master is running Windows (or rather all components of Active Directory and their associated management protocols). This causes issues with adprep and schema upgrades trying to run against Samba still. If you are updating the schema, use Samba tools against Samba schema masters and Windows tools against Windows schema masters. In general, Samba does not implement as many checks to schema as Windows does, and so it is probably easier to corrupt your database by incorrectly updating schema with Samba.

; Domain naming master (forest):y no behaviour is implemented in regards to this role in Samba. Partition creation is not functional in Samba (or is only partly functional).

; PDC emulator (domain):the functionality should be implemented at a 2008 R2 level. However, notably we currently do not implement certain aspects of authentication and password changes (including urgent replication) which would normally forward to the PDC emulator in Samba. In this case, the issue not with the PDC (which presumably does nothing special but respond as per normal) but it is that we don't respect the PDC emulator in the rest of the domain. Normally the PDC is considered authoritative, but in Samba, the replication must propagate naturally before certain logins may work. As long as the domain is at 2008 R2 functional level (regardless of the OS versions), this should function mostly as expected.

; RID master (domain):plements the RID master role and the associated forwarding to the RID master to ensure every DC in the domain does not run out of RIDs (and pre-allocates before it does so). This role should be the same regardless of functional level, but it was implemented for 2008 R2. Certainly at 2008 R2 functional level (regardless of OS versions), this should work as expected.

; Infrastructure master (domain + domain DNS + forest DNS):y no behaviour appears to be implemented for these roles in Samba. This role is normally responsible for maintaining certain cross realm links and aspects of the recycling bin (neither have official support in Samba).

 adprep workaround =
===============================

------------------------

As we've covered, when joining a newer Windows DC (e.g. 2012R2) to an older Windows domain (e.g. 2008R2), the schema level and preparation level of the domain need to be upgraded. Windows now runs adprep automatically as part of the join process and upgrades the domain (note that the domain's functional level remains unchanged).

As a `Joining_a_Windows_Server_2012_/_2012_R2_DC_to_a_Samba_AD|workaround` in the past, administrators have been able to use this adprep behaviour to upgrade Samba and successfully join Windows 2012 to Samba (even though Samba didn't support the 2012 schema at the time). Unfortunately, adprep doesn't run directly against a Samba DC. However, if another Windows DC is present, adprep can upgrade that DC, and the updated domain database then gets replicated to the Samba DC as well. The process looks like:

# A 2008R2 version of Windows is joined to Samba.
# All the FSMO roles are transferred to the Windows 2008R2 DC.
# The Windows 2012 DC joins the 2008R2 DC and adprep automatically updates the domain's schema and preparation levels.
# The 2008R2 DC can now be removed and/or FSMO roles transferred back to Samba.

Compatibility 
------------------------

This sections covers the compatibility between joining Windows and Samba in more detail, but a high level summary is:

{|class="wikitable" style="text-align:quot;
|-
!| DC
!colspan="4"| Domain (functional level)
|-
!
!| Win2008 R2
!| Win2012 R2
!| Win2016
!| Samba
|-
!| Win2008 R2
| -
| OK
| OK
| OK
|-
!| Win2012 R2
| OK*
| -
| OK
| OK*
|-
!| Win2016
| OK*
| OK*
| -
| Not supported
|-
!| Samba
| OK
| Not supported
| Not supported
| -
|}

<nowiki>*</nowiki> Requires that the domain schema level and preparation level are upgraded first.

Joining a Windows DC to a Samba domain
------------------------

 2008 and 2008 R2 Windows DC =
===============================

------------------------

This should work without any special preparation.

 2012 and 2012 R2 Windows DC =
===============================

------------------------

Yes, this works but not yet by default. In order for this to work, the Samba domain needs to have the latest 2012 schema levels, as well as the preparation level. 

In Samba v4.11, the default schema is now 2012 R2. So if your domain was provisioned on v4.11 or later, then only the functional preparation step needs to be done.

If your domain was provisioned prior to v4.11, you'll need to upgrade the schema first by running the 'samba-tool domain schemaupgrade' command. Although the schemaupgrade command exists from v4.8 onwards, we recommend that you upgrade to v4.11 first, as important fixes were added in v4.11.

.. warning:

   This tool may [https://bugzilla.samba.org/show_bug.cgi?id=13705 error out] when a live server is running, but it has actually succeeded. There is also a [https://bugzilla.samba.org/show_bug.cgi?id=14029 bug in 4.11.0rc1] in the LDAP server preventing the actual Windows domain join.

.. code-block::

    samba-tool domain functionalprep --function-level=[2012|2012_R2]

Note that the schemaupgrade and functionalprep commands only need to be run on a single DC. The schema and preparation changes will then propagate out to the rest of the DCs via replication.

 2016 and above =
===============================

------------------------

Not yet complete. All the infrastructure is in place, but the work needs to be done/funded. This may still be possible using the `Windows 2012 Server compatibility#adprep_workaround|adprep workaround`.

Joining a Samba DC to a Windows domain
------------------------

 Pre-2008R2 functional level =
===============================

------------------------

There are some known problems joining a Windows domain running 2003 or older (see below), but there are some work-arounds and so this is actually possible.

Note that Samba only supports schema upgrades from 2008R2 domains onwards. If you originally created your domain using a Windows DC older than 2008R2, then samba-tool will not be able to upgrade your domain schema for you.

If you are migrating from Windows to Samba, then we recommend that you use Windows adprep to upgrade your domain schema and preparation level to 2008 R2 before decommissioning your Windows DCs.

===============================
 Pre-2003 functional level
------------------------

The issue with joining a Windows 2000 functional level server is that the DNS entries are still located on the main partition and not in DC=DomainDNSZones.

Before joining a Samba AD DC successfully, you need to upgrade the functional level to 2003 and then upgrade DNS zones to migrate the DNS entries from main partition to DC=DomainDNSZones and DC=ForestDNSZones. See [https://www.itprotoday.com/windows-78/q-how-can-i-create-domaindnszones-directory-partition creating domain DNS zones] for more details.

If you have not migrated the DNS entries first, then joining the Samba DC will fail. Once the DNS entries have been migrated, the join should complete successfully.

 2008 or 2008R2 functional level =
===============================

------------------------

Generally speaking, this should work. At times there have been replication issues (and workarounds for them), but stable versions of Samba should join without issue.
* a Samba DC to an Existing Active Directory`

One such bug is [https://bugzilla.samba.org/show_bug.cgi?id=14046 14046] - note that this should only affect Samba v4.10 (built with Python 3 support).

 2012 functional level or above =
===============================

------------------------

This is currently not possible as it would require Samba to implement 2012 functional level (at least enough to operate most features).

Attempting to join will trigger this error message:

<pre>DsAddEntry failed with status WERR_ACCESS_DENIED info (8567,
'WERR_DS_INCOMPATIBLE_VERSION')

In reality, Samba could in fact join and pretend it ran the correct functional level, but this has security consequences and is not generally considered safe. The advice is to downgrade the forest (and domain) functional level on the Windows DC to 2008 R2 (and turn off all the associated features in 2012) before joining Samba.

 Troubleshooting =
===============================

------------------------

For further details, see `Samba_AD_DC_Troubleshooting|Samba AD DC Troubleshooting`.

Trusted domain environments
------------------------

Samba still has a number of limitations to its trusted domain support. In a mixed environment, expect Samba to allow or disallow operations differently from Windows. More information needs to be provided here on exact scenarios.

Other missing features
------------------------

Some noticeable omissions in our feature set for 2008 R2 are:

* Recycling bin
* DFSR (sysvol replication)
* Various pieces of trust/forest support
* DCOM / WMI (and associated DCE/RPC pipe support)
* `ADWS / AD Powershell compatibility`

For 2012 and 2012 R2:

* Group managed service accounts 
* Newer Kerberos features including claims based access control, authentication policy silos, FAST armoring

For 2016:
* Temporary (time-limited) group memberships