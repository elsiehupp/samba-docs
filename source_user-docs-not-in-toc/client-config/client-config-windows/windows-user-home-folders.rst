Windows User Home Folders
    <namespace>0</namespace>
<last_edited>2021-02-26T12:45:44Z</last_edited>
<last_editor>Hortimech</or>
===============================

Introduction
===============================

Home folders contain files of an individual account. Using Samba, you can share the directories to enable network users to store own files on their home folder on the file server.

This documentation does not use the Samba built-in ``[homes]</ section that dynamically shares the user's home directory using the ``\\server\*user_name*\``/path. While this can be helpful in certain scenarios, it has some disadvantages:
* Windows does not support this feature, and certain settings, such as folder redirection in an Active Directory (AD), require a workaround instead and you cannot use the official solution.
* You must create each new user's home directory manually.
* Whilst The ``[homes]</ feature is supported on a Samba Active Directory (AD) domain controller (DC), it will not work for Windows users home directories. It will work for Unix home directories, but this setup is not shown here.

In the following, the directory containing the home folders are shared using the ``users</ share name. Each user's home directory is created as a subdirectory on the ``\\server\users\``/share, such as, ``\\server\users\*user_name*``. /s is the same format used in a Microsoft Windows environment and requires no additional work to set up.

=================
Setting up the Share on the Samba File Server
=================

Using Windows ACLs 
------------------------

Setting extended access control lists (ACL) on the share that hosts home directories enables you to create new users in the ``Active Directory Users and Computers</ application without manually creating the user's home folder and setting permissions.

To create a share, for example, ``users</ for hosting the user home folders on a Samba file server: 

* Create a new share. For details, see `Setting up a Share Using Windows ACLs`. Set the following permissions:

* Share permissions::
* :"wikitable"
!Principal
!Access
|-
|Domain Users
|Change
|-
|Domain Admins
|Full Control
|}

* File system permissions on the root of the ``users</ share::

* :"wikitable"
!Principal
!Access
!Applies to
|-
|Domain Users*
|Read & execute
|This folder only
|-
|CREATOR OWNER
|Full control
|Subfolders and files only
|-
|Domain Admins
|Full control
|This folder, subfolders and files
|}

* :;/; You can alternatively set other groups, to enable the group members to store their user profile on the share. When using different groups, apply the permissions as displayed for ``Domain Users``/in the previous example.

* : that permission inheritance is disabled on the root of the share. If any permission entry in the ``Advanced Security Settings</ window displays a path in the ``Inherited from``/column, click the ``Disable inheritance`` /OTT On Windows 7, unselect the ``Include inheritable permissions from this object's parent`` check / to set the same setting.

* :r_File_:s.png`

* a Samba share, you can omit the ``SYSTEM</ account in the file system ACLs. For details, see `The SYSTEM Account`.

These settings enable members of the ``Domain Admins</ group to set the user home folder in the ``Active Directory Users and Computers``/application, that automatically creates the home folder and sets the correct permissions.

Using POSIX ACLs 
------------------------

Instead of using Windows access control lists (ACL), you can set up a share using POSIX ACLs on your Samba server. However, when using POSIX ACL to set permissions, you must create the home directory for each new user manually and set permissions.

.. note:

    When setting up the share on a Samba Active Directory (AD) domain controller (DC), you cannot use POSIX ACLs. On an Samba DC, only shares using extended ACLs are supported. For further details, see `Setting_up_a_Share_Using_Windows_ACLs#Enable_Extended_ACL_Support_in_the_smb.conf_File|Enable Extended ACL Support in the smb.conf File`. To set up the share on a Samba AD DC, see `#Using_Windows_ACLs|Setting up the Home Folder Share on the Samba File Server - Using Windows ACLs`.

For example, to create the ``users</ share: 

* Add the following share configuration section to your ``smb.conf</ file:

    users]
          path = //us///
          read only = no
          force create mode = 0600
          force directory mode = 0700

* EFor details about the parameters used, see the descriptions in the smb.conf(5) man page.

* EDo not use ``homes</ as name of the share. For further details, see `#Introduction|Introduction`.

* Create the directory and set the correct permissions:

 # mkdir -p //us///
 # chgrp -R "*Domain Users*" //us///
 # chmod 2750 //us///

* EIn a domain, the ``Domain Users</ group is a group, all domain user accounts are member of. Alternatively, or if you are running a non-domain environment, you can set it to any group that exists locally. However, user accounts must be member of this group to access the share.

* Reload Samba:

 # smbcontrol all reload-config

=================
Creating the Home Folder for a New User
=================

Using Windows ACLs 
------------------------

If you are using the ``Active Directory Users and Computers</ application, the user's home directory is automatically created and the correct permissions applied when you set the path to the user folder in the application.

If you are not using ``Active Directory Users and Computers</ you must create the folder manually and set the correct permissions. For example:

* Log in to a Windows machine using an account that has permissions to create new folders on the ``\\server\users\</ share.

* Navigate to the ``\\server\users\</ share.

* Create a new home folder for the user.

* Add the user to the access control list (ACL) of the folder and grant ``Full control</ to the user. For details, see `Setting_up_a_Share_Using_Windows_ACLs#Setting_ACLs_on_a_Folder|Setting ACLs on a Folder`.

Using POSIX ACLs 
------------------------

When you set up the ``users</ share using POSIX access control lists (ACL), you must create the home folder for each new user manually. To create the home folder for the ``demo``/user:

* Create the directory:

 # mkdir //us////

* Set the following permissions to only enable the ``demo</ user to access the directory:

 # chown *user_name* //us////
 # chmod 700 //us////

=================
Assigning a Home Folder to a User
=================

In an Active Directory 
------------------------

Using ``Active Directory Users and Computers</ 
===============================

------------------------

In an Active Directory, you can use the ``Active Directory Users and Computers</ Windows application to set the path to the user home folder and the assigned drive letter. If you do not have the Remote Server Administration Tools (RSAT) installed, see `Installing RSAT|Installing RSAT`.

To assign the ``\\server\users\demo\</ path as home folder to the ``demo``/account:

* Log in to a computer using an account that is able to edit user accounts.

* Open the ``Active Directory Users and Computers</ application.

* Navigate to the directory container that contains the ``demo</ account.

* Right-click to the ``demo</ user account and select ``Properties``/

* Select the ``Profile</ tab.

* Select ``Connect</ the drive letter Windows assigns the mapped home folder to, and enter the path to the home folder into the ``To``/field.

* DUC_Set: r.png`.

* Click ``OK</DOOTT

If a warning is displayed when saving the settings that the home folder was not created:
* the permissions on the ``users</ share were incorrectly set when you set up the share using Windows access control lists (ACL). To fix the problem, set the permissions described in `#Using_Windows_ACLs|Using Windows ACLs`.
* you set up the share using POSIX ACL. To fix the problem, create the directory manually. See `#Using_POSIX_ACLs_2|Creating the Home Folder for a New User - Using POSIX ACLs`.

Using a Group Policy Preference
------------------------

Using group policy preferences, you can assign settings to organizational units (OU) or to a domain. This enables you, for example, to automatically assign home folder paths to all users in the OU or domain. If you move the account to a different OU or domain, the setting is removed or updated. Using this way, you do not have to assign manually the setting to each user account.

To create a group policy object (GPO) for the domain that automatically assigns the ``\\server\users\*user_name*</ path as home folder to each user:

* Log in to a computer using an account that is allowed you to edit group policies, such as the AD domain ``Administrator</ account.

* Open the ``Group Policy Management Console</DOOTT If you are not having the Remote Server Administration Tools (RSAT) installed on this computer, see `Installing RSAT|Installing RSAT`.

* Right-click to your AD domain and select ``Create a GPO in this domain, and Link it here</DOOTT

* PMC_Cre:OTTpng`

* Enter a name for the GPO, such as ``Home folders on *server*</DOOTT The new GPO is shown below the domain entry.

* Right-click to the newly-created GPO and select ``Edit</ to open the ``Group Policy Management Editor``/

* Navigate to the ``User Configuration</ &rarr; ``Preferences``/&rarr; ``Windows Settings`` /; ``Drive Maps`` entryDDO/

* Right-click to the ``Drive Maps</ entry and select ``New``/&rarr; ``Mapped Drive``./

* Set the following:
* On the ``General</ tab::
* : <cod: </
* :n: <cod:ver\users\%LogonUser%</
* ::automatically replaces the ``%LogonUser%</ variable when a user logs in
* : ``Reconnect</
* : EnterSS: string. For example: ``Home:SSHHcode>
* SelectS: drive letter the home folder is mapped to.
* On the ``Common</ tab::
* : ``Run in logged-on user's security context (user policy option)</

* PME_Hom:operties.png`

* Click ``OK</DOOTT

* Close the ``Group Policy Management Editor</DOOTT The GPOs are automatically saved on the ``Sysvol``/share on the domain controller (DC).

* Close the ``Group Policy Management Console</DOOTT

The policy is applied to users in the OU or domain, the policy is assigned to, during the next log in.

Using ``ldbedit</ on a Domain Controller
------------------------

On a domain controller (DC), for example, to assign the ``\\server\users\demo</ path as home folder to the ``demo``/account and set the assigned drive letter to ``H:ASSHHcode>

* Edit the ``demo</ser account:

 # ldbedit -H //sa/e/sam/  /countNa/

* The accounts attributes are displayed in an editor. Append the following attributes and values to the end of the list:

 homeDrive: H::
 homeDirectory: \\server\users\demo\

* Save the changes.

The setting is applied the next time the user logs in.

In an NT4 Domain 
------------------------

In an Samba NT4 domain, to set ``\\server\users\%U</ as path to the home folder and to map the drive to the ``H:ASSHHcode> drive letter::

* Add the following parameters to the ``[global]</ section in your ``smb.conf``/file:

 logon drive = H:
 logon home = \\server\users\%U

* EDuring logging in to the domain member, Samba automatically replaces the ``%U</ variable with the session user name. For further details, see the ``Variable Substitutions``/section in the ``smb.conf(5)`` /CCEEpage.

* Reload Samba:

 # smbcontrol all reload-config

In a Non-domain Environment 
------------------------

Using a Windows Professional or Higher Edition
------------------------

If your Samba server and clients are not part of a domain, set the user home folder mapping in the local user account's properties:

* Log on to the Windows machine using an account that is member of the local ``Administrators</ group.

* Open the ``lusrmgr.msc</ (Local User and Groups) application.
* EThe ``lusrmgr.msc</ application is not available in Windows Home editions.

* Click ``Users</ in the navigation on the left side.

* Right-click the account you want to assign a home folder to, and select ``Properties</

* Navigate to the ``Profile</ tab.

* Select ``Connect</ the drive letter Windows assigns the mapped home folder to, and enter the path to the home folder into the ``To``/field.

* Click ``OK</DOOTT

You must set the mapping for each user on every Windows client manually.

Using Windows Home Edition
------------------------

Windows Home editions do not provide the necessary application to set the user home folder mapping in the local account properties. Instead each user must map the drive manually:

* Log on to the Windows machine as the user that should get the home folder mapped

* Open a command prompt.

* For example, to map the ``\\server\users\demo\</ folder to the ``H:ASSHHcode> drive letter, enter::

 > net use H: \\server\users\demo\ /t:yes:

The user home folder is automatically connected when the user logs in. To stop the automatic mapping, disconnect the drive. For example:

 > net use H: /

----
`Category Directory`
`Category Members`
`Category Serving`
`Category Domains`
`Category:  Server`