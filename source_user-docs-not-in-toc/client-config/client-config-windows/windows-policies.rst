Samba and Windows Policies
    <namespace>0</namespace>
<last_edited>2017-02-26T21:05:26Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

Great Resources for Implementing Policies on Samba
------------------------

Mike Petersen's website [http://HHservices.com/custom_poleditDDO/

Troubleshooting Policies
------------------------

See Bugzilla Entry #3042 [https://a.org/show_bug.cgi/

System Policy HowTos
------------------------

`Implementing System Policies with Samba`

`Creating Custom Policy Templates`