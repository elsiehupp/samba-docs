Sysvolreset
    <namespace>0</namespace>
<last_edited>2021-01-29T15:48:48Z</last_edited>
<last_editor>Hortimech</last_editor>

Samba comes with some subcommands of 'samba-tool ntacl', designed to configure permissions on the sysvol folder.

Advice via mailing list (as of May 2018)
------------------------

(courtesy of Rowland Penny)

If you have added any custom GPOs and given Domain Admins a gidNumber attribute, never ever use sysvolcheck or sysvolreset, this because this turns the windows group into a Unix group.
*(You are now probably thinking 'what?', a group is just a group, right ? Well, no, a Windows group can do something that no Unix group can, it can own files and directories and guess what needs to own files and directories in sysvol ??)*

Some steps to follow for sysvol troubleshooting
------------------------

(courtesy of L.P.H. van Belle <vmime.5a7ac078.6b03.2ff673485d0a7bf8@ms249-lin-003.rotterdam.bazuin.nl>)

I suggest start with this one. 
wget https :// raw.githubusercontent.com/thctlo/samba4/master/samba-check-set-sysvol.sh
This checks and set the rights to be known to be right. ( aka works great for me ) ;-) 

Then follow these steps.

* login as dom\administrator. 
* start computer manager, connect to dc.
* klik Shared Folders, Shares, sysvol. 
Option 1, this is the default. Everyone with Full control, Change and Read.
Option 2, Everyone: Read.
		Verified users:  Full, Change, Read.
		SYSTEM Full, Change, Read.
		DOMAIN\Adminstrators ( or DOMAIN\Domain Admins ) Full, change read.

The result of both settings are ( share wize)  the same.
Except in option2, you must be verified before you can write anywere.

* Tab Security.
		Verified users:  Read+exec, Show folder content, Read.
		SYSTEM: full ( everything on )
		DOMAIN\Adminstrators: ( or DOMAIN\Domain Admins ) full ( everything on
		DOMAIN\Serer operators: Read+exec, Show folder content, Read.
Once this is set, klik advanced, klik change below. 
Check, replace all underlying and replace.. 

! Note, always this order, first share security then folder security. 
That helps preventing making error or resetting rights. 

* Do the same for Netlogon. Same settings as sysvol, since its a sub folder of sysvol.

* These steps are imo only done once, ( ! Or if you get errors again due to a reset or change in windows clients ) 
Now first goto the GroupPolicyObjects, ( not the linked once's )
Klik on every GPO object there, if you get any message, press ok, then its reset. 

Now, you need to check the GPO Objects that are assigned/linked to OU and/or groups.

Just start in the top, and klik every object.
All my "normal" GPO Objects only have Authenticated Users.
My "special" GPO Object have different settings. 

For example, i've disabled all USB/Mobile/CD access on the pc's by GPO's 
A user policy set in the Standard User. I've created 2 groups, per type. 
For example.
USB-Read, if i look here you see only USB-Allow-Read group. Now klik the Delegation Tab.
That shows me: 
 Authenticated User Ready (by security filter)
 DOM\Domain Admins
 DOM\Enterprise Admins
 Server logon
 SYSTEM

What you dont see is the underlying ACL, klik  Advanced.
Here you see, ... The "Reset to default" button. 
Reset it. 

Now remember here, after doing this, no samba-tool sysvolreset..
If you do, repeat the above again. Everything! 

User GPO's, only a group with the user is fine, and needs "apply GPO"
A computer GPO, needs Domain computers with apply GPO AND the users group. 

I've setup all "problem" shares, due to user NT Authority\SYSTEM problems. 
Google for it, you see lots of it in the samba list.
My shares layout that used it. ( on mulple servers ) 
 DC: Sysvol and Netlogon
 Members: users and profiles
 Print server: print$ and printers

So in short, all shares were the "computer$" my access as user system or things like that. 

If you see errors on a computer in the eventlogs with:
 computer$ can't access .... Bla bla....   On GPO.ini.
This if often a forgoten "DOM\Domain Computers" in the GPO object with read and/or writes rights missing.
People test this and the computer$ can access the GPO.ini without problems, so why the event log.
Because of "SYSTEM" or an other user that is haveing user/group/SID problems with linux acls. 

I hope i explained good enough why i use and set ignore systemacl.