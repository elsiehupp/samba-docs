Mounting samba shares from a unix client
    <namespace>0</namespace>
<last_edited>2018-03-30T09:27:54Z</last_edited>
<last_editor>Fraz</last_editor>

Mounting samba shares from a unix client
------------------------

===============================
General description
===============================

------------------------

Using a share from a samba server within a unix filesystem depends on a lot of single components. You need at least the smbfs (which is no longer maintained) or the modern cifs kernel modules. Although older documentation says to use the smbfs, it has many restrictions and the cifs should be favoured. That said, I'll describe only the cifs module in this document.

Since the cifs filesystem is included in the standard Linux kernel, it is simple to build it either as module or built in. If you build the cifs module (e.g. to get a newer version with bugfixes) and do not install it in the default location, then you may need to load it prior to mount a share or let modprobe load it via modprobe.conf in order to get the newer cifs module rather than the one shipped with your distribution.

The basic mount command is *mount -t cifs //<server>/<share> <mountpoint>*. Each component needs its own attention.

The *mount* utility calls a mount helper, usually *mount.cifs* which calls into the kernel.
The mount helper *mount.cifs* is the user space helper and needed to parse tcp/ip names and retrieve userid and password, and also does simple formatting of the mount options.

Server's listen for incoming client connections via TCP/IP and thus have ip addresses, and usually tcp host names configured for them, but users often refer to the server by its "netbios name" (RFC1001 name).  To mount using the cifs client, a tcp name (rather than netbios name) must be specified for the server.   To resolve the *<server>* to a ip address, you need either a DNS server which knows the ip address or your client needs the nss module wins. It is a shared library which must be in the path of your ldd. Usually under /usr/lib. You also have to add the *wins* option to *hosts* in your /etc/nsswitch.conf.   The smbclient utility can also be used to identify the tcp name or ip address of a server (identified by its netbios name).

*mountpoint* must be a directory elsewhere in the filesystem which must exist.

===============================
Common mount.cifs options
===============================

------------------------

Now an explaination of the options for mount -t cifs as described in man mount.cifs
*mount.cifs* takes a lot of options, some of them are really confusing. I'll try to mention the most important here.

* *user=<user>* or *username=<user>*: Thats the user <user> which is used to authenticate against the server. If this is not given, then the environment variable USER is used. This option can also take the form  "user%password"  or  "workgroup/user"  or  "workgroup/user%password" to allow the password and workgroup to be specified as part of the username.
* *pass=<password>* or *password=<password>*: specifies the CIFS password. If this option is not given then the environment variable PASSWD is used. If the password is not specified directly or indirectly via an argument to  mount  mount.cifs will prompt for a password, unless the guest option is specified.
* *credentials=<filename>*:specifies a file that contains a username and/or password. This is preferred over having passwords in plaintext in a shared file, such as /etc/fstab. Be  sure to protect any credentials file properly. The format of the file is:
 username=value
 password=value
* *uid=<uid-spec>*: sets  the  uid  that  will own all files on the mounted filesystem. It may be specified as either a username or a numeric uid. This parameter is ignored when the target server supports the CIFS Unix extensions (which samba does per default unless you disable it).
* *gid=<group-spec>*: sets the gid that will own all files on the mounted filesystem. It may be specified as either a groupname or a numeric gid. This parameter is ignored when the target server supports the CIFS Unix extensions.
* *domain=<domain|workgroup>*: sets the domain (workgroup) of the user
* *ip=<ip-addr>*: sets the destination host or IP address.
* *guest*: mount the share as guest and don't prompt for a password.

There are other options but they are not that important to be usefull beside performance and really specific things.

===============================
NetBios name resolution with WINS
===============================

------------------------

All networking programs at some point need to resolve a host name to an IP-Address. Usually this is done with a call to the function *gethostbyname*. This call is a library function and thus is handled in user space. Glibc systems such as Linux have the feature of using more than one lookup method to fulfill such a query. This is known as nsswitch (Name Service Switch). The config file is usually found in /etc/nsswitch.conf.

If a program wants to resolve the ip address of a host named 'wiki' it simply calls the function *gethostbyname*. The lookups done are the responsibility of the underlying glibc then. Glibc reads the */etc/nsswitch.conf* and loads the library mentioned in the *hosts* line. Thats the key point: if there is a *wins* entry, glibc loads the libnss_wins.so which enables the NetBios Name Lookup. You got it then, you can resolve a Windows Name to an ip address.

So its simple: install the libnss_wins.so, add to your /etc/nsswitch.conf at line *hosts* the wins entry and the windows name resolution works. Your network setup should be ok for this, but thats not a matter of samba.

===============================
Step by Step list to get it working
===============================

------------------------

Lets assume you have one of those ARM powered NAS boxes such as Buffalo et al make under your TV set and you want to access it from a desktop Linux box somewhere in your house.

You will need the IP address of said box! Often you can find this out via the web interface of your router.

As root edit /etc/fstab using your favourite editor

e.g. sudo gedit /etc/fstab

At the end of the file add a line that looks like this:-

<pre>//<IP address of NAS box 192.168.x.y>/<shared folder>	/<mount point>	cifs	user=<user on NAS>,pass=<passwd on NAS> 0 0</pre>

If you never set any users up on the NAS then it would look like this

<pre>//<IP address of NAS box 192.168.x.y>/<shared folder>	/<mount point>		cifs	guest,_netdev,uid=<your user name on Linux box> 0 0</pre>

Here is what one of my entries looks like

<pre>//192.168.2.100/share	/mnt/oneterra		cifs	guest,_netdev,uid=mike 0 0</pre>

Now when I browse over to /mnt/oneterra I see all my files on my 1TB NAS! If I had left out the uid=myloginname I would only have read-only access, the _netdev is to tell Linux to wait for the network to be up before attempting the mount

For more try this link [http://opensuse.swerdna.org/susesambacifs.html]

TODO: make a step by step list to solve most of the problems

Here's the verbatim commands I used (SuSE 10.0 to Buffalo Link Station).  Notice I knew the IP address, in my case the SMB name didn't work and I didn't bother making the Network Name "NetBios Name" Work as suggested above.
 su (make sure you do this as root)
 mkdir /mnt/linky_share
 mount -t cifs -o user=luke //192.168.1.104/share /mnt/linky_share
 <enter>
 it then prompts for your Fileshare password.
 voila! now you can treat it as your file system.

 #note I thought "mount -t cifs -o guest" should work but I got a permission error even though
 # I have shares that don't have access restriction...

===============================
Mounting shares at logon time
===============================

------------------------

If you want to automatically mount shares at user logon time the only way I found is doing it with the pam_mount module. It is the only way to do an authenticated mount without requiring the user to store the password in a file or entering it on a terminal.

===============================
For more information
===============================

------------------------

The Linux CIFS client (http://linux-cifs.samba.org) user's guide can be found at:
http://pserver.samba.org/samba/ftp/cifs-cvs/linux-cifs-client-guide.pdf
or
http://pserver.samba.org/samba/ftp/cifs-cvs/linux-cifs-client-guide.odt

`Category:CIFSclient`