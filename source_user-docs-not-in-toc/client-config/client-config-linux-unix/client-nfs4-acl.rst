NFS4 ACL overview
    <namespace>0</namespace>
<last_edited>2021-03-23T19:04:54Z</last_edited>
<last_editor>Bjacke</last_editor>

===============================

Introduction
===============================

Windows ACLs are very different from POSIX draft ACLs, this is why people are often not happy with the mapping that Samba can make between Windows ACLs and POSIX draft ACLs. Those mappings implemented by Samba or described [http://www.citi.umich.edu/projects/nfsv4/rfc/draft-ietf-nfsv4-acl-mapping-05.txt here] are often insufficient and lossy because POSIX draft ACLs don't allow setting the fine-grained permissions that Windows ACLs do and the inheritance model is also working quite different and cannot be mapped from the one to the other. 

If you use the Samba vfs module acl_xattr, you can use the full Windows ACL features but you will not be able to manage the ACLs on the OS of the Samba host directly and you will not have the permissions enforced by the filesystem. Samba is doing permission management in userspace if you use acl_xattr. As a result of that you will not be able to share the files differently than via Samba because you have no (or partly) permissions on the files in the filesystem itself.

You might have noticed the word *draft* between the two words POSIX ACLs. This is the correct naming because POSIX ACLs had actually never been finally standardized and the draft was even withdrawn back in 1997 in favor of what's described in the following.

With the introduction of [https://tools.ietf.org/html/rfc7530 NFS4] a new ACL system was introduced and standardized, which is essentially a clone of the Windows ACL system. So ironically in the Unix world they are called NFS4 ACLs these days, even though they are Windows ACLs actually. Because we are talking about the Unix filesystems here we will also call them NFS4 ACLs further on here. With those NFS4 ACLs you will be able to manage the Windows ACLs natively in the filesystem of the Samba host. As a result of the native support of the ACL model you will also be able to share the files with the permissions set on the files via other protocols and have permission enforcement locally on the file server.

=================
Operating system support
=================

In the following there is an overview of which operating systems and filesystem implementations do support NFS4 ACLs.

Linux 
------------------------

Linux is the only one of the major Unix flavors that does not have any native NFS4 ACL support upstream in the kernel yet. There was a proposed implementation called [https://github.com/andreas-gruenbacher/richacl RichACLs]. RichACLs are essentially like NFS4 ACLs with the additional feature of file masks. Even though the RichACL implementation was in a good shape in 2015 already, it never was brought upstream into the kernel because the Linux VFS maintainers believe that POSIX draft ACLs are sufficient and NFS4 ACLs are not needed. This is a pity for all of the the Linux community actually.

* [https://lwn.net/Articles/738235/ lwn: Maintai:t 2017: SPDX, crossDDAAS: m development, and conclusion]
* [https://lwn.net/Articles/661357/ lwn: A netwo:tem wish list]
* [https://lwn.net/Articles/661357/ lwn: Rich ac:ol lists]

cifs vfs
------------------------

The Linux cifs kernel module supports native Windows ACLs and dedicated tools from the cifs-utils package can be used to view and manage these ACLs. In the future the NT ACLs might get exposed via the more common system.nfs4_acl EAs, see [https://bugzilla.samba.org/show_bug.cgi?id=14499 Bug 14499].

Spectrum Scale / GPFS
------------------------

This proprietary cluster filesystem of IBM has it's own NFS4 ACL support implemented.

NFS4 client
------------------------

The Linux NFS4 client does support NFS4 ACLs. They can be viewed and managed through the [http://www.citi.umich.edu/projects/nfsv4/linux/ nfs4-acl-tools] package, which is part of most Linux distributions. The NFS4 ACLs are revealed here though the ``system.nfs4_acl`` xattr.

openZFS
------------------------

As of 2020 the Linux port of ZFS does *not* have NFS4 ACL support but there might be some support for that coming up [https://github.com/openzfs/zfs/issues/4966 here].

AIX 
------------------------

AIX supports NFS4 ACLs.

JFS2
------------------------

JFS2, which is the default filesystem here, supports NFS4 ACLs if it's set up accordingly.

Solaris 
------------------------

Solaris has native NFS4 ACL support if you use ZFS.

FreeBSD 
------------------------

FreeBSD supports NFS4 ACLs. As of 2020 they missed to implement DACL_PROTECTED though, so that this implementation is incomplete. As a workaround there is a parametric option ``map_dacl_protected`` for the zfsacl vfs module to make this work on FreeBSD in most cases. FreeBSD will hopefully fix this soon.