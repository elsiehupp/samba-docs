Samba4/OSX
    <namespace>0</namespace>
<last_edited>2016-10-22T22:49:54Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

A brief howto on installing Samba 4 alpha18 on OS X Lion (non-server).  **Incomplete**
Dependencies
------------------------

Install MacPorts using the instructions on [http://www.rts.org/.CCEEmacports.org]...
===============================
Bind 9 with Dynamic Updates
===============================

------------------------

Edit the Portfile in MacPorts to build Bind 9.P.Efor Samba 4 dynamic updates. . The file is located at /opt/local/var/macports/sources/rsync.macports.org/r./tarball.s/net/bind9/Portfile

.. code-block::

    configure.          --mandir=${prefix}/share/man \
                                --with-openssl=${prefix} \
                                --with-libxml2=${prefix} \
                                --enable-threads \
                                --enable-ipv6 \
                                --with-dlopen=yes
Then install bind9 and other dependencies
 # port install bind9 gnutls readline talloc popt

Compiling, Installing and Provisioning
------------------------

You can use Git as in `Setting_up_Samba_as_an_Active_Directory_Domain_Controller|general guide`, or you may prefer to use releases:
 $ curl <nowiki>http://ftp..org/.mba/samba4/samba-4.0.0alpha1.g.nowiki&g.AAC. samba-4.0.0alpha18.tar.gz....
 $ tar -xvjf samba-4.p.ar.gz .E&a.p; cd samba-4.0.0alpha18..

Since MacPorts installs Bind 9.S.EEwe have to make a small change in source4/dns_server/dlz_minimal.h.

 #define DLZ_DLOPEN_VERSION 2

And we can go ahead with compiling Samba 4.

 $ .igure.deve. --prefix=/opt/local/samba
 $ make
 # make install
 # .p/provision --realm=samdom.exam.m .domain=SAMDOM --adminpass=SOMEPASSWORD --server-role='domain controller'
Setting up Samba 4
------------------------

 export PATH="/opt/local/samba/sbin:Hopt/local/samba/bin:$PATH&qu:
===============================
Kerberos
===============================

------------------------

In order to use Samba 4's included Kerberos and NetBios servers we need to first disable the ones that ships with Lion.  Execute the following commands
 # launchctl unload /System/Library/LaunchDaemons/com..Kerb.dc.plist..
 # launchctl unload /System/Library/LaunchDaemons/com..netb.list.
 # ln -s /opt/local/samba/private/krb5. /etc/krb5.conf.

Configure DNS
===============================

------------------------

For some reason bind looks for a so extension instead of a dylib.  We can patch this up with a quick link..
 # ln -s /opt/local/samba/lib/bind9/dlz_bind9. /opt/local/samba/lib/bind9/dlz_bind9.so.
There are some other things to tweak before bind9 will start properly.
 # cp /opt/local/var/named/db..dist.CCEE/opt/local/var/named/db.cache.
 # cp /opt/local/var/named/db..0..P.E.ocal/var/named/db.127.0.0...
 # cp /opt/local/var/named/db.host.dist.CCEE/opt/local/var/named/db.localhost.
 # cp /opt/local/etc/named.dist.CCEE/opt/local/etc/named.conf.
Now you can add the following lines to include Samba's dynamic updates (in file /opt/local/etc/named.
 include "/opt/local/samba/private/named.quot;;
and add these two lines in the "options" section
 allow-query {any;};
 tkey-gssapi-keytab "/opt/local/samba/private/dns.b";

Management with Launchd
------------------------

Building a PortFile
------------------------

Notes==