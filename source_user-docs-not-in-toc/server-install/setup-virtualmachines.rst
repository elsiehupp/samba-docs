Samba4/VirtualMachines
    <namespace>0</namespace>
<last_edited>2011-11-15T11:44:26Z</last_edited>
<last_editor>Kelly</last_editor>

=================
Virtual Machines
=================

Oracle VirtualBox 
------------------------

This guide is to ensure that your virtualbox setup is going to work well with Samba. There are a few nuances with some of the settings that may cause problems further down the track, and getting it right from the start can be a huge time saver.

If you already have a virtual machine set up using VirtualBox, you must ensure that it is powered down before you can edit the settings. The settings button will be greyed out for any machine in a saved/running state.

Downloading/Installing
------------------------

The VirtualBox website has the installs available for download, or instructions on installing for debian/ubuntu and rpm based distros.
[http://www.virtualbox.org/wiki/Linux_Downloads]

Creating a new virtual machine
------------------------

To run from the command prompt:
	$ virtualbox

Alternatively there may be an icon available within your GUI from the applications menu, likely under System Tools.

The “Oracle VM VirtualBox Manager” window is the main screen of the application. It displays a list of available virtual machines on the left (empty if you have not created any), and information about the currently selected machine on the right.  There are buttons at the top of the screen for adding, removing, manipulating or starting virtual machines. 

# Click the “new” button, then Next.
# Insert a name for your virtual machine (something that makes sense for what its function shall be), and select the operating system that it will be running (the OS selection determines the logo displayed for ease of identification and use). Click Next
# Use the slider to select Base Memory Size, or insert text directly into the text box to the right of the slider. Something between 512 and 1024 should suffice. More is better, keeping in mind that you may want to run multiple virtual machines at once and how much memory you have available on your system. Click Next
# Ensure “Boot Hard Disk” is selected, and “Create new hard disk”. Click Next, then Next again as you enter the Create New Virtual Disk Wizard
# Select Dynamically expanding storage as this will create a small file that only expands to use real disk space as it is needed, up to your determined upper limit which is yet to be set. Click Next
# The Location is the file name and location that you wish to use to save your virtual disk to. It defaults to the name of your virtual machine at “$home/VirtualBox VMs/”. Use the slider to set the upper limit size for your virtual disk, or insert text directly into the text box to the right of the slider. 20GB should suffice. Click Next
# A summary of your new disk will be displayed. Click Finish.
# A summary of your new virtual machine will be displayed. Click Finish.

Setting up the virtual machine
------------------------

Looking at the “Oracle VM VirtualBox Manager” window, you should now see your new virtual machine listed to the left of the screen. If you click it you can see information about it to the right of the screen. Now we need to ensure that it is set up sanely to work with Samba.

# Ensure that your new virtual machine is selected, click the “Settings” button at the top of the window. A smaller window is displayed with a list of settings categories to the left, and changeable attributes to the right.
# Select the “System” category. This allows you to modify the base memory used if required, and also the boot order. Assuming that you will be using an ISO file as your windows installation media, change the boot order to boot from CD/DVD-ROM, then Hard Disk (select the drive you wish to manipulate, then use the up and down arrows accordingly). Deselect any other drives as they are unnecessary and only increase boot time while seeking media.
# Select the “Storage” category. This displays a Storage Tree to the left where we can see what disks are currently setup for the environment, and a list of attributes for those disks to the right.
## Set up the CD/DVD-ROM to point to your ISO
### Select the Empty CD/DVD icon within the Storage Tree
### Within the Attributes section of the screen, click the CD/DVD icon/button to select where the CD/DVD media will be found
### Choose a virtual CD/DVD disk file
### Locate your ISO file and click the “Open” button at the bottom-right of the window.
## Change your Hard Disk to be IDE
### Select the IDE Controller from within the Storage Tree
### Click the “Add Hard Disk” button to the right of the IDE Controller, or click the “Add Attachment” button below the Storage Tree, then “Add Hard Disk”.
### Click “Choose existing disk”
### Locate your virtual disk (default at $home/VirtualBox Vms/*nameOfVirtualMachine.vdi*), then click the “Open” button at the bottom-right of the window.
## Remove the SATA drive – you will notice some red text on your window reading “Invalid settings detected” so long as you have both the SATA and the IDE drive pointing to the same virtual disk.
### Select the SATA disk and click the “Remove Attachment” button below the Storage Tree
### Select the SATA Controller and click the “Remove Controller” button below the Storage Tree.
# Select the “Audio” category, deselect “Enable Audio”
# Select the “Network” category
## Ensure that “Attached to” is set to “Bridged Adapter” and NOT NAT
## Select eth0 in the “Name” list
#Click “OK” to save these settings. Review changes on the right side of the Oracle VM VirtualBox Manager.

Installing Windows
------------------------

Select your virtual machine from the Oracle VM VirtualBox Manager window, then click the “Start” button at the top of the window. This will boot the machine from the CD/DVD-ROM as per your setup, and assuming that everything has been done correctly will begin the Windows Install process. Work your way through the wizard, and then when it gets to copying/expanding files it's a great time to get a tea or coffee ;)

You will notice that you cannot use your mouse within your virtual machine until you click within the window somewhere. It then steals your mouse (you can only use it within your virtual machine window) until you press the right-ctrl key on your keyboard. This can be fixed after the install is complete.

Once the install has finished it will reboot and request that you insert a new password. To log in to windows thereafter you will need to press “Ctrl-Alt-Del” to bring up the login screen, but the virtual machine won't accept the character combination when pressed from your keyboard. This is solved by clicking the “Machine” menu at the top of the screen, then selecting “Insert Ctrl-Alt-Del”. You will need to ensure that the virtual machine is not controlling your mouse to perform this function.

When Windows is installed and working
------------------------

# FREE YOUR MOUSE!!! Click on the “Devices” menu at the top of your virtual windows environment (ensure that it isn't controlling your mouse at the time), then select “Install Guest Additions”. This will install some software to make your virtual machine behave a little more sanely, including freeing your mouse pointer so that it no longer gets captured by the window and can be freely moved and used as normal.
# Change the boot order so that your machine no longer attempts to boot from CD/DVD-ROM. It will no longer attempt to install windows as the virtual CD/DVD is now pointing to the file required for “Install Guest Additions”. Regardless of this, it is faster to boot directly from Hard Disk rather than test for other bootable media before booting from Hard Disk.
## Power down your windows environment if running.  The “Settings” button is greyed out an inaccessible to a running machine.
## Select your virtual machine from the Oracle VM VirtualBox Manager and click the “Settings” button.
## Select the “System” category
## Move the Hard Disk to the top location within the boot order, and deselect the CD/DVD-ROM
## Click “OK” to save
# Take Snapshots. These are an excellent way of being able to boot your machine at any given state. A good time for a first snapshot is when you have a fully functioning windows environment with guest additions, logged in. This will save the need to insert the ctrl-alt-del characters from the menu 
## Get your machine to the state you wish to save it in, then click the “machine” menu at the top of the screen, select “Take Snapshot”
## Give your snapshot an appropriate name that well describes the current state. You will probably take multiple snapshots along the way, so it is useful to be able to determine which does what. Add a description if so desired.
## Click the “OK” button to save

At this point you should have a sanely working VirtualBox Windows environment perfect for use with Samba.

QEMU 
------------------------

TODO

KVM 
------------------------

TODO

* If further information is required within this document, please email a request to kyeoh@au1.ibm.com *