Samba for AIX
    <namespace>0</namespace>
<last_edited>2019-05-23T15:28:10Z</last_edited>
<last_editor>Bjacke</last_editor>

=================
How to get Samba running on AIX
=================

You can either compile Samba yourself or install precompiled Samba packages from third party vendors.

Install Samba from source 
------------------------

For various reasons getting Samba built on AIX can be a bit challenging. It is recommended to compile Samba with the native xlc compiler from IBM, you will need to install also a number of other third party packages first, which also depends on the features that you want to compile into your Samba binary.

Install Samba binary packages 
------------------------

Much easier than compiling Samba on AIX on your own is using pre-built binary packages from third party vendors:

* [https://are.com Bull Freeware] has a lot of RPM based AIX packages, also Samba for AIX 6

* [https:///samba-a/ f/ a commercially /Samba distribution for AIX 7 from SerNet