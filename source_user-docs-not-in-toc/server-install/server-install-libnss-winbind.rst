Libnss winbind Links
    <namespace>0</namespace>
<last_edited>2020-08-14T18:01:55Z</last_edited>
<last_editor>Hortimech</last_editor>

<noinclude>
============
Introduction
============
</noinclude>

If you compile Samba yourself, to enable hosts to receive user and group information from a domain using Winbind, you must create two symbolic links in a directory of the operating system's library path. If you are are using Samba packages from your distro, there are usually distro packages to do this for you e.g. libpam-winbind and libnss-winbind on Debian.

.. note:

    Do not copy the library to the directory. Otherwise you must replace it manually after every Samba update.

.. note:

    You only need to do this if you compiled Samba yourself, otherwise your distro will provide packages to do this for you. See your distro documentation for which packages to install.

========================
Determining the Platform
========================

To determine the operating system's platform:

.. code-block::

    $ uname -m

============================================
Locating the ``libnss_winbind.so.2`` Library
============================================

The ``libnss_winbind.so.2`` library is installed in the Samba library directory set at compile time. To locate the folder:

.. code-block::

    $ smbd -b | grep LIBDIR
    LIBDIR: /usr/local/samba/lib/

Link the library from this directory in your operating system's library directory.

==================================
Operating System-specific Examples
==================================

Red Hat-based Operating Systems 
-------------------------------

x86_64
------

.. code-block::

    $ ln -s /usr/local/samba/lib/libnss_winbind.so.2 /lib64/
    $ ln -s /lib64/libnss_winbind.so.2 /lib64/libnss_winbind.so
    $ ldconfig

i686
----

.. code-block::

    $ ln -s /usr/local/samba/lib/libnss_winbind.so.2 /lib/
    $ ln -s /lib/libnss_winbind.so.2 /lib/libnss_winbind.so
    $ ldconfig

Debian-based Operating Systems 
------------------------------

x86_64
------

.. code-block::

    $ ln -s /usr/local/samba/lib/libnss_winbind.so.2 /lib/x86_64-linux-gnu/
    $ ln -s /lib/x86_64-linux-gnu/libnss_winbind.so.2 /lib/x86_64-linux-gnu/libnss_winbind.so
    $ ldconfig

i686
----

.. code-block::

    $ ln -s /usr/local/samba/lib/libnss_winbind.so.2 /lib/i386-linux-gnu/
    $ ln -s /lib/i386-linux-gnu/libnss_winbind.so.2 /lib/i386-linux-gnu/libnss_winbind.so
    $ ldconfig

ARM
---

.. code-block::

    $ ln -s /usr/local/samba/lib/libnss_winbind.so.2 /lib/arm-linux-gnueabihf/
    $ ln -s /lib/arm-linux-gnueabihf/libnss_winbind.so.2 /lib/arm-linux-gnueabihf/libnss_winbind.so
    $ ldconfig

SUSE-based Operating Systems 
----------------------------

x86_64
------

.. code-block::

    $ ln -s /usr/local/samba/lib/libnss_winbind.so.2 /lib64/
    $ ln -s /lib64/libnss_winbind.so.2 /lib64/libnss_winbind.so
    $ ldconfig

i686
----

.. code-block::

    $ ln -s /usr/local/samba/lib/libnss_winbind.so.2 /lib/
    $ ln -s /lib/libnss_winbind.so.2 /lib/libnss_winbind.so
    $ ldconfig