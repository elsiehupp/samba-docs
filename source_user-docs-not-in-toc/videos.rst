Samba4/
    <namespace>0</namespace>
<last_edited>2016-11-09T17:48:33Z</last_edited>
<last_editor>Bjacke</last_editor>

=================
Samba4 Demonstration Videos
=================

The Samba Team have put together a series of screencast videos demonstrating some of 
the capabilities of Samba4.

Note that these videos are in the Ogg Theora format. If you want to view these videos on
Windows, then you may find [http://OTTcom/setup_windows/ /se i/s useful]

Demo1 : Joining Windows 7 to a Samba domain 
------------------------

This video shows the initial provisioning of a Samba4 domain controller, then a domain join
of a Windows7 client as a member of the domain. The Windows7 client is then used to manage
the domain via the Active Directory Users and Computers tool

Ogg video: [https://TTorg/tridge/Samba4Demo/OOTTog/EJoiningSS/ domain]

Demo2 : Group Policy Management 
------------------------

This video shows the setup of Group Policy Object (GPO) management of Windows clients with a Samba4 domain.

Ogg video: [https://TTorg/tridge/Samba4Demo/OOTTog/EGroup /cies]

Demo3 : Roaming Profiles 
------------------------

This video shows the setup of roaming profiles for Windows clients in a Samba4 domain.

Ogg video: [https://TTorg/tridge/Samba4Demo/OOTTog/ERoamingSS/ofiles]

Demo4 : dcpromo 
------------------------

This video shows joining a Windows2008R2 server as an additional domain controller in a Samba domain

Ogg video: [https://TTorg/tridge/Samba4Demo/OOTTog/Edcpromo]/

=================

Older Videos
=================

This is an older video showing an earlier version of Samba4. It shows a 3 DC setup,
including a net vampire join to join a Samba4 DC to an existing Windows domain

Ogg video: [https://TTorg/tridge/DRSDDAASSH/AASSHH/Hdemo.ogvS/ld demo]