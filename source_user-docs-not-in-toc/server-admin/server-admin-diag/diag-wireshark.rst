Wireshark Decryption
    <namespace>0</namespace>
<last_edited>2021-04-20T23:52:42Z</last_edited>
<last_editor>Pshilovsky</last_editor>

=============================
Using the session key (SMB3+)
=============================

Starting from Wireshark **2.5.0** (released Feb 2018) you can pass a list of SessionId -> SessionKey mappings via a table in the SMB2 preferences or command-line.

The session key in this context refers to the cryptographic session keys used in authentication and message signing. It is *not* the same as the CIFS SessionKey.

From MS-SMB2:
<blockquote>
**Session.SessionKey**: The first 16 bytes of the cryptographic key for this authenticated context. If
the cryptographic key is less than 16 bytes, it is right-padded with zero bytes.
</blockquote>

Known limitations
------------------------

* SMB3.0 CCM decryption added in Wireshark 2.6.5 (December 2018)
* SMB3.1.1 CCM decryption added in Wireshark 3.0.0 (February 2019)
* GCM for SMB3.1.1 added in Wireshark 3.1.0 (July 2019)
* **Due to how key generation works, the trace needs to have the initial connection packets (NegProg and SessSetup) present in the trace being decrypted (done using the right session id).**
* Wireshark needs to be built with libgcrypt >= 1.6.0. (you can check in ``wireshark -v`` output)
* Support for multichannel decryption added in Wireshark 3.3 (unreleased).

You can test with the [https://wiki.wireshark.org/SampleCaptures#SMB3_encryption sample traces on the Wireshark wiki]. If decryption doesn't work on those some of the requirement mentioned above are not met.

===============================
Using the server and client decryption keys (SMB3+)
===============================

Starting from Wireshark **3.3.x** (**UNRELEASED** as of Feb 2020) you can pass a list of SessionId => ServerKey,ClientKey via the table in the SMB2 preferences or command-line.

If you are unsure of which key is the server and which key is the client it doesn't matter Wireshark will try both.

**By passing directly the decryption keys instead of the session key wireshark doesn't require the session establishment packets (NegProt & Session Setup) to be in the capture.**

==================
Obtaining the keys
==================

Linux kernel client 
-------------------

For more recent Linux kernels (feature was added in September 2019, Linux kernel version 5.4) SMB3 cryptographic keys can be dumped by using the utility smbinfo (must be run as root or CAP_SYS_ADMIN, cifs-utils version 6.10):

.. code-block::

    smbinfo keys <path-to-a-file-on-an-smb3-mount>

For older kernels the linux client (cifs.ko) can be configured to dump the cryptographic keys it generates (visible in dmesg output) via the **build time** option ``CONFIG_CIFS_DEBUG_DUMP_KEYS``. Keep in mind this is a debugging option and it should not be set on a production system.

You can set this option in your .config file or find it in ``make menuconfig`` by going in:

* File systems
** Network File Systems
*** SMB3 and CIFS support (advanced network filesystem)
**** Enable CIFS debugging routines > Dump encryption keys for offline decryption (Unsafe).

Once enabled, you can mount a share and pass the mount options ``vers=3`` or ``vers=3.11`` to make sure the connection uses SMB3 and ``seal`` to require encryption.

If the connection reaches the Tree Connect stage you should be able to see this in the kernel console (via dmesg) for each session established:

.. code-block::

    CIFS VFS: generate_smb3signingkey: : generated AES session keys
    CIFS VFS: Session Id    bc 1a fc 0d 00 00 00 00
    CIFS VFS: Session Key   34 57 e1 90 4b 94 3a 0d 09 be 22 b9 b9 cf c7 4c
    CIFS VFS: Signing Key   28 bf a3 71 39 e5 05 71 c6 15 58 f3 cf 42 3f 0e
    CIFS VFS: ServerIn Key  79 2a 9a 40 1e cf 16 52 41 a5 ec bd 1c e5 84 17
    CIFS VFS: ServerOut Key c0 73 c8 1d 36 f5 74 3e ef 6e c1 9d 5c e5 cb 71

Samba client and server 
-----------------------

Starting from Samba 4.11, both client and server code can dump the generated keys in the log file using "debug encryption = yes" in smb.conf or via the command line. Note that the option controls both server and client code.

Example:

.. code-block::

    $ smbclient //localhost/scratch --option='debugencryption=yes' -e -mSMB3 -U aaptel%aaptel
    debug encryption: dumping generated session keys
    Session Id    [0000] F0 38 C7 30 00 00 00 00                             .8.0....
    Session Key   [0000] CB 18 C5 26 18 CA 9F 59   44 0A 62 44 63 90 2B BA   ...&...Y D.bDc.+.
    Signing Key   [0000] A6 0B B1 50 1B FD 51 1F   8A 06 00 8C 13 7E 7C 4F   ...P..Q. .....~|O
    ServerIn Key  [0000] 7D 93 ED 2D E6 54 0C 86   8F 3F B5 AE 63 1D BD E7   }..-.T.. .?..c...
    ServerOut Key [0000] 7A D0 18 61 7B E3 E5 2A   33 5E 27 66 D1 6B F9 0E   z..a{..* 3^'f.k..

===============
Setting the key
===============

`File:-decryption.png|thumb|Setting a key:  > Preferences > SMB2, Edit`

**Note**: The kernel prints the Session Id as it is on the wire (little endian). Wireshark decryption expects the Session Id as it is on the wire as well, so you should be able to copy/paste from one to the other (despite showing the Session ID field decoded when dumping packets). 

* Just copy the Session Id and Session Key into wireshark (see screenshot).
* Alternatively you can pass them via the command line (works with wireshark and tshark):

.. code-block::

    tshark -ouat: y_list:3d000094:8f2847263c83dc00621f742dd3f2e7b -r smb3-aes-128-ccm.pcap

==============
Using a keytab
==============

Prerequisite
------------

You only need to do that once:

* Open Wireshark
* Goto: Preferences -> Protocols -> KRB5
* Select: Try to decrypt encrypted Kerberos blobs

Basic decryption
----------------

The easiest way, on a unix-like system is to run

.. code-block::

    wireshark -K <PATH TO KEYTAB> <PCAP FILE>

Note: Wireshark for 64-bit Windows (GUI or command-line) doesn't like the -K flag, [http:SSLLAASS:HHask.wireshark.org/questions/7408/keytab-kerberos run the 32-bit Windows version instead].

The other way, is to specify the keytab in Preferences -> Protocols -> KRB5 -> keytab path

==========================
How to extract the keytab?
==========================

See: `Keytab_Extraction|How to extract a keytab from a windows domain with Samba`

Decrypted AES DCE/RPC
---------------------

To do this, you will need metze's wireshark branch, and his patched verison of MIT Kerberos

http://gitweb.samba.org/?p=metze/wireshark/wip.git;a=shortlog;h=refs/heads/ws-metze-gssapi
git://git.samba.org/metze/wireshark/wip.git ws-metze-gssapi

Also, you will need to apply krb5-1.6-wireshark-hack-01.diff to [http://web.mit.edu/Kerberos/ MIT Kerberos] 1.6, and set LD_LIBRARY_PATH to wherever you put the result.