Samba Member Server Troubleshooting
    <namespace>0</namespace>
<last_edited>2016-12-17T23:35:10Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

===============================

Introduction
===============================

This page will treat common problems when setting up or running a `Setting_up_Samba_as_a_Domain_Member|Samba AD Domain Member`.

=================
SeDiskOperatorPrivilege can't be set
=================

You want to set SeDiskOperatorPrivilege on your member server to manage your share permissions but you get an error like this:

 # net rpc rights grant 'SAMDOM\Domain Admins' SeDiskOperatorPrivilege -U'SAMDOM\administrator'
 Enter SAMDOM\administrator's password:
 Could not connect to server 127.0.0.1
 The username or password was not correct.
 Connection failed: NT_STATUS_LOGON_FAILURE

In this case you need to create a user mapping file (i.e. /etc/samba/user.map) with the following content:

 !root = SAMDOM\Administrator SAMDOM\administrator

After this you need to add a parameter to the [global] section of your smb.conf:

 username map = /etc/samba/user.map

Restart or reload samba and you will then be able to set SeDiskOperatorPrivilege with the administrator account:

 # net rpc rights grant 'SAMDOM\Domain Admins' SeDiskOperatorPrivilege -U'SAMDOM\administrator'

=================
Getent doesn't return domain users or groups
=================

Does the group Domain Users have a gidNumber?
Run this command on the DC:
 ldbsearch -H /usr/local/samba/private/sam.ldb cn=Domain\ Users | grep 'gidNumber'

It should return something like this:

 gidNumber: 10000

If you do not get a result, you need to add a gidNumber to Domain Users

Do you have domain users that are also local users?
Run this on the file server:
 cat /etc/passwd | grep <domain user>

You cannot have local users that are also Domain users. If you have any, decide which one you wish to keep and delete the other.

Does your domain user have a uidNumber?
Run this on the DC:
 ldbsearch -H /var/lib/samba/private/sam.ldb samaccountname=username | grep 'uidNumber'

It should return something like this:

 uidNumber: 10000

If you do not get a result, you need to add a uidNumber to the user using ldbedit

Do you have a keytab on the file server ?

 klist -k
 Keytab name: FILE:/etc/krb5.keytab
 KVNO Principal
 ---- ------------------------

------------------------

------------------------

--
    1 host/Member1.samdom.example.com@SAMDOM.EXAMPLE.COM
    1 host/Member1.samdom.example.com@SAMDOM.EXAMPLE.COM
    1 host/Member1.samdom.example.com@SAMDOM.EXAMPLE.COM
    1 host/Member1.samdom.example.com@SAMDOM.EXAMPLE.COM
    1 host/Member1.samdom.example.com@SAMDOM.EXAMPLE.COM
    1 host/Member1@SAMDOM.EXAMPLE.COM
    1 host/Member1@SAMDOM.EXAMPLE.COM
    1 host/Member1@SAMDOM.EXAMPLE.COM
    1 host/Member1@SAMDOM.EXAMPLE.COM
    1 host/Member1@SAMDOM.EXAMPLE.COM
    1 MEMBER1$@SAMDOM.EXAMPLE.COM
    1 MEMBER1$@SAMDOM.EXAMPLE.COM
    1 MEMBER1$@SAMDOM.EXAMPLE.COM
    1 MEMBER1$@SAMDOM.EXAMPLE.COM
    1 MEMBER1$@SAMDOM.EXAMPLE.COM

If there is no keytab, then check if the computer is joined to the domain.

 sudo net ads testjoin

This should print:

 Join is OK

If the computer is joined to the domain but there is no keytab, then you are probably missing these lines in your smb.conf:

 dedicated keytab file = /etc/krb5.keytab
 kerberos method = secrets and keytab

You should also check if you have this line:

 winbind refresh tickets = Yes

Without it your kerberos tickets will expire and not be renewed.

Add the lines and then leave the domain and rejoin:

 net ads leave -U Administrator

 net ads join -U Administrator

You should now have a keytab, if it is still not there, try creating it manually:

 net ads keytab create -U Administrator

Check the DNS settings of the member server:

First the member servers FQDN: 

 hostname -f

Should return something like this:

 Member1.samdom.example.com

Next, does the domain match the kerberos realm ?

 hostname -d

 samdom.example.com

Finally, is the short hostname only one word ?

 hostname -s

 Member1

If you do not get the correct results, check that /etc/hosts is set correctly:

 127.0.0.1 localhost
 192.168.1.100 Member1.samdom.example.com Member1

For a DHCP client, it should be:

 127.0.0.1 localhost

Check /etc/resolv.conf:

 search samdom.example.com
 nameserver 192.168.1.16   <--- This should be the IP of your DC

Check /etc/krb5.conf:

 [libdefaults]
        default_realm = SAMDOM.EXAMPLE.COM
        dns_lookup_realm = false
        dns_lookup_kdc = true

Check /etc/nsswitch.conf:

Do these lines match these?

 passwd: files winbind
 group:  files winbind
 hosts:   files dns
 networks: files dns

You should also ensure that nscd is not installed, it will interfere with winbind.

If you do get results, but they are not exactly as expected, try removing the winbind cache:

 net cache flush