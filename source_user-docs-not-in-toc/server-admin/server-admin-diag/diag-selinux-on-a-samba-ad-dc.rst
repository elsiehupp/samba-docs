Troubleshooting SELinux on a Samba AD DC
    <namespace>0</namespace>
<last_edited>2017-01-04T16:57:34Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

I had a lot of trouble getting login working for Active Directory users on a Red Hat Enterprise Linux Samba 4 Active Directory domain controller.  Here are some things I learned that I hope will be useful:

1. The official build and deployment guidance `Setting_up_Samba_as_an_Active_Directory_Domain_Controller|Samba AD DC HOWTO` does not address SELinux.  SELinux configuration can be different on every system and distribution, which makes creating specific guidance challenging.  Every other guide I read on the web said to turn SELinux off. By policy at my place of business, we have to have SELinux enabled by policy.  Learning to identify and address the problems caused me a lot of pain, mostly because I didn't know about some amazing tools that are available to help.

When I followed the `Winbindd` instructions to allow login on the AD DC, I got all kinds of errors, with console login, SSH, and graphical login (GDM) all failing.  When I finally determined that SELinux was preventing login, much became clear.

To see if SELinux is causing you problems, first determine if SELinux is running:

 # sestatus

If SELinux is enforcing, it may cause you issues.  If the audit daemon (auditd) is running, SELinux will log its denials.  This will save you a lot of effort trying to configure SELinux, as I'll demonstrate a little later.  You can confirm if SELinux is causing you problems by attempting to log in as an AD user and then grepping the audit log file.  My audit log is in /var/log/audit/audit.log.

 # grep denied /var/log/audit/audit.log

If SELinux is enforcing and you get output -- particularly output related to winbind -- SELinux is likely causing you problems.  Try temporarily putting SELinux into permissive mode and try logging in again.

 # setenforce 0
 # [attempt to log in as an AD user on console interface]

If you can now log in, SELinux is the culprit.  The SELinux audit2allow application will help you create an SELinux module with the appropriate permissions to allow login.  With SELinux in permissive mode, attempt to log in using all of the methods you're going to allow an AD user to use (console, SSH, and graphical login in my case).  In permissive mode, SELinux will not deny access, but it will log what it would have done.  (It's important to do this login step in permissive mode, because otherwise you'll have to do multiple rounds of module creation; you'll only get past the first denial on every round.)

 # cd /tmp
 # grep denied /var/log/audit/audit.log > selinuxloginfails
 # audit2allow -M samba4 -i selinuxloginfails
 # semodule -i samba4
 # setenforce 1

Test logging in on each of the interfaces.  After doing this step I was able to log in as an AD user on the console, but not SSH (due to some security configurations in my sshd_config file that I won't go into here) or the graphical login.  Even on the console, I got some strange errors after I logged in:

 login:
 Password:
 id:ind name for user id 3000018
 id:ind name for user id 3000018
 id:ind name for user id 3000018 could not get database information for UID of current process: \:
    User "???" unknown or no memory to allocate password entry
 ["I have no name!"@server]$ 

This bring me to thing-I've-learned-2:

2. Even if mandatory access controls (SELinux) are configured correctly, discretionary access controls can make your life difficult.

The default umask on my system is 077, so when I built and installed Samba 4 the files were owned by root, and only root could access them.

When I followed the `Winbindd` guidance, I linked to the libraries that were installed in /usr/local/samba/lib, but the directory permissions would not allow applications running under other user permissions to access the libraries.  In this case, id and whoami both failed to get data about the AD user, even after login succeeded, because they were running as the user (testuser) instead of root.  Not only could they not access the libraries, but they couldn't access the winbind daemon, either.

On the console, this mostly just means that it shows you as user "I have no name!", but the X server just completely failed to log me in, even though the user authenticated correctly.  The gdm login interface would succeed, but X would shut down immediately and kick back to the gdm login prompt.

So I had to modify the permissions on directories leading to the relevant files:

 # chmod 755 /usr/local/samba /usr/local/samba/var /usr/local/samba/var/run \
      /usr/local/samba/var/run/winbindd
 # chmod -R 755 /usr/local/samba/lib

This allowed me to log in on gdm and addressed the problem of no user name on the console after login.

If you still have trouble after running these steps, log in on the console as an AD user and run strace on id and whoami.  Pay special attention to errors that say ENOACCES. For X (gdm) debugging, check the ~/.xsession-errors file for the user you tried to log in as.