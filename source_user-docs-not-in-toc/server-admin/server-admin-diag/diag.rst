Samba Troubleshooting
    <namespace>0</namespace>
<last_edited>2013-10-25T20:30:25Z</last_edited>
<last_editor>JelmerVernooij</last_editor>

SELinux 
------------------------

A great resource for learning about SELinux
http://fedoraproject.org/wiki/SELinux

specific samba policy
http://fedoraproject.org/wiki/SELinux/samba

For the most part, the information on the specific Samba policy link above should take care of the things most people need.

How to check whether your problem is already fixed: 
------------------------

For the server, check the release notes for the latest stable Samba (see http://samba.org/samba/history/ for a list of recent releases), which contains a list of major fixes made to the server and related utilities.

For the Linux CIFS client you can see the source file fs/cifs/CHANGES to see the list of bug fixes

I can't join a example.local domain. 
------------------------

You will see an error with 'net -d 10 join ads ..'

.. code-block::

    [2007/10/15 19:45:30, 2] libads/ldap.c:ldap_open_with_timeout(70)
    ould not open LDAP connection to server.example.local:389: Illegal seek

This issues is caused by mDNS as .local is reserved for mDNS. Use example.site.

.. code-block::

    strace ldapsearch -x -h ldap.local
lseek(3, 0, SEEK_CUR)                   = -1 ESPIPE (Illegal seek)

glibc 2.9 and newer
------------------------

Edit /etc/nsswitch.conf and

change

    osts:  files mdns4_minimal [NOTFOUND=return] dns

to

    osts:  files dns

Older glibc versions
------------------------

mDNS can be disabled in /ets/host.conf by adding 'mdns off' but it is currently broken in glibc 2.6.1.