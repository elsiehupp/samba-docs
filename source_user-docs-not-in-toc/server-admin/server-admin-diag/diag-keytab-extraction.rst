Keytab Extraction
    <namespace>0</namespace>
<last_edited>2016-10-09T13:34:56Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

Once you have `Capture Packets|captured packets` you can use Wireshark to `Wireshark Decryption|analyze` them in many case decryption of traffic is needed in order to analyze correctly an exchange.

=================
How to Extract a keytab containing your domain's passwords
=================
There are two ways to obtain a keytab from an Active Directory Domain with Samba:
Using Samba4
------------------------

To use samba4, it needs a copy of the domain database.  If it is already a domain controller for your domain, then you don't need this next step.

Clone the DC:

    samba-tool drs clone-dc-database --include-secrets --targetdir=/lt;/--server=<SERVER> -U<USER>
    samba-tool domain exportkeytab PATH_TO_KEYTAB  --configfile=/tc//onf//

If you don't have the `samba-tool drs clone-dc-database` command, then your Samba version is not new enough and you will need to join the domain. See `Joining_a_Samba_DC_to_an_Existing_Active_Directory|how to join Samba4 as domain controller`, then run

    samba-tool domain exportkeytab PATH_TO_KEYTAB

It will write out a keytab in *PATH_TO_KEYTAB* containing the current keys for every host and user.

Using Samba3
------------------------

To dump a keytab, join the domain and then run:

 net rpc vampire keytab /eyta/AS/p_doma/ler> -U user_with_admin_rights 

Note that the path to the keytab file needs to be an absolute path, in some situations you might need to append @domain.tld at the administrative username

===============================
Online Keytab Creation from Machine Account Password
===============================

In a field deployment on a domain with 100K+ accounts, extracting all password is not feasible due to policy and scale issues. What if you just want to decrypt packet captures made by a member server, encrypted by its machine account password (or keys derived from that password)? The following command on the member server will get you the keytab:
 KRB5_KTNAME=FILE:Hpath/OO/et ads keytab CREATE -P

===============================
Offline Keytab Creation from Secrets.tdb
===============================

If the net command fails (after all, that could be the reason for us to start sniffing...), you can still generate a keytab without domain admin credentials, if you can get a hold on the server's secrets.tdb. This method can also be done offline on a different machine.
 tdbdump secrets.tdb
Now look for the key SECRETS/ASSWORD/<doma/ASSHH the password is the value without the trailing zero.
Use the **ktutil** utility to construct the keytab:
 ktutil:ry -password -p host/r-name>.<domain-fqdn>@<DOMAIN-FQDN> -e aes256-cts-hmac-sha1-96 -k 1
 Password for host/r-name>.<domain-fqdn>@<DOMAIN-FQDN>:
 ktutil:ry -password -p host/r-name>.<domain-fqdn>@<DOMAIN-FQDN> -e aes128-cts-hmac-sha1-96 -k 1
 Password for host/r-name>.<domain-fqdn>@<DOMAIN-FQDN>:
 ktutil:t my.keytab
 ktutil:

===============================
Keytab Creation on a Windows Server
===============================

A less cumbersome way, using a Windows server:

User account:
 ktpass.exe /OTTkeytab /pass &lt/gt; /princ <user&gt/m> /ptype KRB5_NT_PRINCIPAL /c//

Machine account:
 ktpass /OTTkeytab /pass &lt/gt; /princ host/<se/HHname>/domain-fqdn>@<DOMAIN-FQDN> /ptype KRB5_NT_SRV_INST /crypto all//