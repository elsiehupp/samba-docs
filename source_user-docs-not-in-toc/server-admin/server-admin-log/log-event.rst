Event Logging
    <namespace>0</namespace>
<last_edited>2018-09-05T06:32:04Z</last_edited>
<last_editor>Aaronhaslett</last_editor>

(First submitted by Brian Moran @ Centeris)

Samba and Eventlogs 
------------------------

Samba servers now support event logs -- this means that if 
Samba is configured correctly, the usual administration tools 
like event viewer will work against a Samba server. 

To minimally configure Samba to publish event logs, the
eventlogs to list must be specified in smb.conf, and 
eventlog entries must be written to those eventlogs. 

Optionally, a message file can be registered for each 
of the eventlog 'sources' to pretty-print the eventlog 
messages in the eventlog viewer. 

Configuring smb.conf 
------------------------

To specify the list of eventlogs the eventlog list 
command is used. An example which will show four 
eventlogs is 

    ventlog list = Application System Security SyslogLinux

When Samba initially starts, it looks to see if the 
eventlog directory, and a particular log exists; if not, 
the  directory and file are created under LOCK_DIR

Writing EventLog Records 
------------------------

The eventlogadm command is used to write records 
into a particular eventlog. Eventlogadm expects records 
to be on STDIN in the following format

 LEN: 0
 RS1: 1699505740
 RCN: 0
 TMG: 1128631322
 TMW: 1128631322
 EID: 1000 
 ETP: INFO
 ECT: 0 
 RS2: 0
 CRN: 0
 USL: 0
 SRC: cron
 SRN: dmlinux
 STR: (root) CMD ( rm -f /var/spool/cron/lastrun/cron.hourly)
 DAT: 

These fields closely mirror the eventlog structures 
used by the APIs.  The definitions of the fields are 

* LEN: <integer>  The length field is calculated by the eventlogadm program based on the rest of the information in the record.  Zero works well here.
* RS1: 1699505740 A "magic number", the primary purpose of which seems to be to be able to find eventlog records in a  sea of binary data 
* TMG: <integer>  The time the eventlog record was generated;  format is the number of seconds since 00:00:00 :ua: E1, 1970, UTC
* TMW: <integer>  The time the eventlog record was written; format is the number of seconds since 00:00:00 :ua: E1, 1970, UTC
* EID: <integer>  The eventlog ID -- used as a index to a message string in a message DLSamba and Eventlogs
* ETP: <string>   The event type -- one of INFO, ERROR, WARNING, AUDIT SUCCESS, AUDIT FAILURE
* ECT: <integer>  The event category; this depends on the message file -- primarily used as a means of filtering in the eventlog viewer
* RS2: 0 Another reserved field
* CRN: 0 Yet another reserved field
* USL: <integer>  Typically would contain the length of the SID of the user object associated with this event. This is not supported now, so leave this zero.
* SRC: <string>   The source name associated with the event log, e.g. "cron" or "smbd". If a message file is used with an event log, there will be a registry entry for associating this source name with a message file DLL
* SRN: <string>   The name of the machine on which the eventlog was generated. This is typically the host name
* STR: <string>   The text associated with the eventlog. Note that there may be more than one strings in a record
* DAT: <string>   Eventlog records can have binary information associated with them. DAT only supports ASCII strings however

Typically, one would set up a program to gather events, format 
them into records, and pipe them into eventlogadm for a 
particular eventlog:

 $ tail -f /var/log/messages |\
    my_program_to_parse_into_eventlog_records |\
    eventlogadm -o write SyslogLinux

Note that individual records are separated on the input by one 
or more blank lines. In this manner, eventlogadm will just wait 
for more input, writing to the underlying log files as necessary.

Deciphering EventLog entries on the Client 
------------------------

To set up an eventlog source (which is used by the eventlog viewer
program to pretty-print eventlog records), create a message file 
DLL,  then use the eventlogadm program to write the appropriate 
eventlog registry entries:

   $ eventlogadm -o addsource Application MyApplication \
     %SystemRoot%/system32/MyApplication.dll

This will add the key
[HKLM/System/CurrentControlSet/services/Eventlog/Application/MyApplication]
and to that key add value "MyApplication/EventLogMessageFile" 
with a string of %SystemRoot%/system32/MyApplication.dll

If there happens to be a share called [C$] on your samba server, 
and in that share there's a Windows/system32/MyApplication.dll 
file, it will be read by the eventlog viewer application when 
displaying eventlog records to pretty-print your eventlog entries.

Future Support for third party clients
------------------------

In the Windows Server AD world, centralised event log management is architected and managed using third party software which communicates with AD through various methods.
The purpose of this section is to investigate popular event log management software and stake out feasible possibilities for Samba integration with these tools.

Event log management solution architectures can be split into two categories: agent-based and agentless.  In an agentless architecture, the event log manager communicates directly
with the Windows AD server either locally or over the network, usually using remote administration/query protocols like WMI.  In an agent-based architecture, usually
implemented to get around firewall blocking of protocols like WMI, the event log manager communicates with an agent over another protocol like HTTP, while the agent communicates
directly with the Windows AD using the same methods as the link in the agentless architecture.

The basic options for integration are:
===============================
Syslog
===============================

------------------------

* Can be agentless or agent-based.
* Many packages accept messages from syslog servers like rsyslog.
* Easy to set up.
* Samba already uses syslog.
* Messages in syslog are unstructured and unstandardised.  The packages need to be able to parse out a lot of information from events in order to produce useful statistical insights about network health. Configuration of this parsing requires users to identify fields in sample messages and is a difficult and error prone operation. Furthermore, if a domain included both Windows AD servers alongside Samba servers, it would take significant effort from both Samba developers and users to ensure the target software package understands that certain Samba syslog events are equivalent to events collected from Windows servers by other methods.  For example, the user may wish for the event log management software to produce a graph plotting average number of logins per day over time.  In order to achieve this in a mixed AD environment, the software package must know that login events received over syslog are the same as login events received over other transports.

This option is the easiest, but also the lowest quality.

===============================
WMI
===============================

------------------------

* Remote query and management protocol, mature and highly supported.
* Usually only used locally in agentless architectures or between an agent and the AD server.
* Seen as a security risk, often blocked by firewalls.
* Implementation in Samba would require implementation of DCOM, and a revamp of the RPC server in order to implement WMI.

This is the most work-heavy option but is also the only high quality, total solution to the problem space.  Everything supports WMI, and it is highly flexible and easy to use.

===============================
EventLog over RPC
===============================

------------------------

* Bespoke, specialised event log protocol.  
* Not used by default in any product I could find, and unsupported in many.
* Samba implementation may require reverse engineering the custom XML standard developed by Microsoft for this protocol.

This option probably represents a compromise between the first two.  EventLog protocol support is not ubiquitous, but when in place it is easy to use.  The workload for the Samba team would be moderate.

===============================
Faking agents
===============================

------------------------

* Instead of reproducing the actual protocols used by event log management tools, we could develop an agent that could speak to, say, the top three management tools over HTTP or TLS.
* This option needs further study.  I captured the agent-client traffic for two tools, "PA Server Monitor" and "EventLog Analyzer".  The PA Server Monitor communicates over a TLS tunnel which I suspect contains WMI queries.  EventLog Analyzer communicates with its agent over HTTP.  Secure communication takes extra configuration.

This option is weird, risky, and interesting, but could be considered if further investigation into other options reveal dead ends or unforeseen, large problems.

All of these options implicitly include an extra bit of work: Right now, logging in Samba only includes a single level of module designation.  For example, a syslog entry might start with "Samba: :update][20962]", which only tells us that the event happened in the dnsupdate task.  Windows events have many fields and module levels. Furthermore, the module layout does not match that of the Windows AD Server.  Therefore, logging in Samba must be restructured so that the information contained in a single event makes sense in the Windows AD event schema.  This task is of unknown size and may even dwarf the task of implementing one of the remote communications methods listed above.