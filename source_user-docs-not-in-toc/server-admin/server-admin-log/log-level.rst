Setting the Samba Log Level
    <namespace>0</namespace>
<last_edited>2018-03-09T10:17:38Z</last_edited>
<last_editor>BBaumbach</last_editor>

__TOC__

===============================

Introduction
===============================

Setting a higher log level enables you to debug problems with Samba daemons and commands.

Additional Samba logging information:

* `Client_specific_logging|Configure logging for a specific client and debug level changes during runtime`
* `Setting_up_Audit_Logging|Logging of authentication and authorization events`

=================
Setting the Log Level in the smb.conf File
=================

You can set the log level for Samba and all commands shipped with Samba using the ``log level`` parameter in the ``smb.conf`` file.

To set the log level for all debug classes to ``3``:

 log level = 3

To set the general log level to ``3`` and for the ``passdb`` and ``auth`` classes to ``5``:

 log level = 3 passdb::

For further information and a list of the debug classes, see the ``smb.conf (5)`` man page.

=================
Setting the Debug Level for a Command
=================

Samba commands use the log level set in the ``log level`` parameter in the ``smb.conf`` file. For details, see `#Setting_the_Log_Level_in_the_smb.conf_File|Setting the Log Level in the smb.conf File`.

However, you can override this value using the ``-d`` parameter for all Samba commands. For example:

 # net ads join -U administrator -d 1

For details, see the manual page of the Samba command.