Samba Myths
    <namespace>0</namespace>
<last_edited>2015-09-05T23:12:33Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

====================================================
Errors in logs - Transport endpoint is not connected
====================================================

This question comes up endlessly on samba mail list
and the various distribution mail lists.
See [http://lists.samba.org/archive/samba/2005-March/101571.html]
for a definitive answer