Directory Replication
    <namespace>0</namespace>
<last_edited>2013-01-16T02:00:52Z</last_edited>
<last_editor>Doublez13</last_editor>

Introduction
------------------------

The following is a **simple** configuration for synchronizing the directories of two Linux servers that are tied to Samba shares. This can be looked at as a very simple implementation of directory replication, including the "poor mans" namespace. In this configuration, we use a third a party program called Unison to implement simple two way replication between servers. More information about Unison, including detailed manuals and setup information can be found on their [http://www.cis.upenn.edu/~bcpierce/unison/ website].

In this setup, two servers that utilize the local Linux authentication scheme (/etc/passwd, /etc/group, /etc/shadow, and so directories) will be used. In large environments with many users, it is recommended to use external authentication schemes like LDAP. For the purpose of this HOWTO, with our limited number of users, we will be using local authentication.

SSH keys need to be created between the two Linux servers that Unison will utilize to establish SSH sessions. A good guide can be found [https://help.ubuntu.com/community/SSH/OpenSSH/Keys here]

Next, a command needs to created that replicates the content of the directories between the servers, along with attributes like permissions and ownership.

After a solid synchronization between the servers is established, a duplicates of the Samba share configuration needs to be written so that the shares are available independent of what server the client connects to.

The DNS configuration is optional, but it will allow the capability of using one hostname to connect to either server. 

Prerequisites
------------------------

This software should be installed on your servers before you begin the configuration process.

***Samba** The file server 

***Unison** (Most distributions include a precompiled version. The source code can be found [http://www.seas.upenn.edu/~bcpierce/unison//download/releases/stable/ here]

***SSH Server** (check your specific distribution's manual for more information)

***Bind 9 (optional)** (Most distributions include a precompiled  version. The source code can be found [https://www.isc.org/software/bind here]

Setup
------------------------

The following is a list of steps for setting up the directory replication between the Samba file servers. 

===============================
User Synchronization
===============================

------------------------

**This step can be skipped if you want the synchronized files and folder to inherit the owner and group permissions of the account that initializes the synchronization.In most cases, though, you will want the permissions and attributes to sync as well.**

**This step can also be skipped for larger setups using LDAP.** 

As mentioned earlier, in environments with many users and groups, an LDAP backend is a good choice to synchronize the users between the Linux servers, in this tutorial, we will be manually synchronizing the UIDs and GIDs of the users and groups that utilize the file servers. 

On both Linux servers, open up the files */etc/passwd* and */etc/group* so that we can see the UIDs and GIDs. Note the users that will be utilizing the file server. We are not referring to automatically created user accounts that manage programs that are created during installation, but we are referring to user accounts made later for specific users.

The fist thing that we need to do is add the users from both servers to the opposite server. Looking at the user accounts, you should type the following for each user that you don't see in the other server

    seradd -u xxx username

If there are users present that have different UIDs, you can type the following to modify them so that they match.

    sermod -u xxx username

xxx refers to the UID of the user. This should match that of the other server

When this is done, you should then repeat the exact process above with the groups using the groupadd and groupmod commands

Your UIDs and GIDs should now match for the users and groups that you have created.

===============================
SSH Keys
===============================

------------------------

A good tutorial for creating and exporting SSH keys can be found [https://help.ubuntu.com/community/SSH/OpenSSH/Keys here]

We do not want to include passwords on our keys since we want the SSH connections to be able to be run automatically.

We want the SSH keys be be made on a user account that has full permission over the synchronized directories.

===============================
Unison Configuration
===============================

------------------------

For our Unison configuration, you will need to go over all the different options that Unison provides to make a command that fits your needs.
The following is a simple command that can be run to synchronize your directories.

    nison -batch -owner -group /replication/directory ssh://x.x.x.x//replication/directory

This command runs the unison command with the batch, owner, and group options. It replicates the the */replication/directory* on the local server, with that on the remote server. This command also replicates the owner and group attributes, an essential feature for directory replication.

===============================
Crontab Configuration
===============================

------------------------

Once you have your Unison command set up, you can then create a cron job for it using the crontab command. This will allow the command to be executed every so often depending on when you specify. 

    rontab -e

To make a con job that executes the Unison command every minute, simply type

     * * * * /usr/bin/unison -batch -owner -group /local/directory ssh://x.x.x.x//remote/directory

Detailed options can be found on the Unison website.

===============================
Samba Shares
===============================

------------------------

It is recommended that the Samba databases on both servers are the same. On larger setups, a Samba LDAP backend is recommended. On smaller installations utilizing Windbind, this should be easy since the */etc/passwd* files are the same.

If you want the shares of the synchronized directories to be available on both servers, then you need to make sure that you have identical shares on the two servers so that the files and folders will be available to Windows clients on both servers

===============================
DNS Configuration (Optional)
===============================

------------------------

This step assumes that you have a Bind DNS server configured and managing a zone that includes your two Samba server.

Up to this point, you are able to access your synchronized data via *server1.yourdomain.com* and *server2.yourdomain.com*. This step allows you to access both servers via one DNS name. For instance, adding the following records allows you to access either server under one name. If one of the servers is down, your computer should automatically connect to the other.   

    luster     IN     A     192.168.0.2
    luster     IN     A     192.168.0.3

By typing cluster.yourdomain.com, you would be able to connect to either of your servers, depending on whichever one you resolve first