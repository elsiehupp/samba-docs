SWAT2
    <namespace>0</namespace>
<last_edited>2014-06-02T02:24:14Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

SWAT2 is a Python frontend to Samba 4, originally written by Ricardo Velhote.

Repository:

* git://git.samba.org/jelmer/swat.git

=================
Dependencies
=================

* Samba4 (in particular, the Samba 4 Python bindings)
* pylons
* authkit module ("easy_install authkit") on RHEL based distros.  You'll need python-setuptools installed before you can do this.

=================
Installation
=================

There are three ways in which you can run SWAT. Pick one:

Standalone 
------------------------

Use paster:

     paster serve development.ini 

From Samba 
------------------------

First, install SWAT or make sure it is available in the Samba server's Python path:

     ./setup.py install

Make sure that Samba is running the web service by adding the following line to the *[global]* section of the smb.conf file:

     server services = +web

SWAT will now be available at http://localhost:901SSLLA:T

From Apache 
------------------------

TODO