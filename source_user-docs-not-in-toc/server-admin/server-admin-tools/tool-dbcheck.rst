Dbcheck
    <namespace>0</namespace>
<last_edited>2019-07-31T04:03:41Z</last_edited>
<last_editor>Timbeale</last_editor>

The ``samba-tool dbcheck`` utility enables you to detect and fix problems in the Samba AD database.

You must run the check and fix command on every Samba AD DC locally, because some fixes apply to non-replicated attributes and modifications are not replicated to other DCs. The tool cannot be run over LDAP.

To check the AD database, run:

 # samba-tool dbcheck --cross-ncs

The ``--cross-ncs`` option checks *all* AD partitions (naming contexts). Without this option, the tool only checks the main domain partition.

To fix reported errors, run:

 # samba-tool dbcheck --cross-ncs --fix

You will get prompted whether or not you want to fix each individual error. Choosing 'all' will fix all errors that are the exact same type of problem.

If you pass the ``--yes`` parameter to the command, all questions are automatically answered with ``yes``. Note that if you omit the ``--yes`` parameter, the database check executes three ``fsync()`` calls for each object. This can result in a longer run duration. For example, passing the ``--yes`` parameter to the command fixed 3500 objects in 10 seconds in our test environment. Without this parameter, the command required 4 minutes for the same operation.

After a repair, re-check the database to verify a successful operation.

What it does
------------------------

The tool goes through every object in the database and checks it for consistency. There are a number of rules that it looks for, which are problems that have known to occur in the past with Samba. For example, linked attributes are usually bi-directional, so if dbcheck finds a two-way link that's missing a back-link, then that's a problem.

The tool can take some time to run, depending on the size of your database. Not only is it checking every user and every group object, it's checking that every member in the group points to a valid user object, and that user has a matching return link.

Some database problems will propagate throughout the domain DCs, via DRS replication. Other problems might be localized to a specific DC, as they may affect a non-replicated attribute, or how the database contents are stored on disk.

Why run it
------------------------

Imagine an older version of Samba contains a bug that sometimes inserts an incorrect database record. Even after you upgrade Samba to software that has the problem fixed, the incorrect records are still going to remain in your database.

Sometimes a problematic database record may result in an obvious error. However, other times it could result in a more subtle problem. For example, if a user was missing a 'memberOf' backlink, then that user would still appear as a member of the group (i.e. checking the 'member' forward link works correctly), but the group permissions (which check 'memberOf') might not be applied correctly when the user logs in.

As database inconsistencies have the potential to accumulate over time, it is worth running dbcheck regularly.

Precautions
------------------------

The dbcheck tool is modifying your database in order to remove corner-case problems. There is a small risk that the act of fixing the problem could itself have a unforeseen and negative side-effects.

Note that just running the dbcheck reporting on its own (without ``--fix``) is completely safe.

It is worth taking a `Back_up_and_Restoring_a_Samba_AD_DC|domain backup` before using the ``--fix`` option (and it's probably best to take both an online and offline backup).

If fixing dbcheck errors seems to have introduced problems to your DC, you could try the following:
# Use ``samba-tool drs replicate --full-sync --sync-forced`` for each of your partitions. This means the DC receives the domain database in its entirety from another DC. This should force the local DC to overwrite its database with the updated contents.
# If that didn't help, try `Upgrading_a_Samba_AD_DC#Rejoining_the_upgraded_DC|re-joining the DC`, which will completely rebuild the DC's local database.
# Finally, as a last resort you could try reverting back to the domain backup-file. Restoring the domain backup-file involves rejoining all the DCs from scratch, so you would only resort to this if *all* the DCs in the domain had problems.

`Category Directory`