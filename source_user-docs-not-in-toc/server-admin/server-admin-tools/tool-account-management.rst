Account Management Tools
    <namespace>0</namespace>
<last_edited>2017-02-26T20:24:13Z</last_edited>
<last_editor>Mmuehlfeld</last_editor>

.. warning:

   The utilities listed on this page are neither part of Samba nor officially supported by Samba. If you need support for these applications, please see their web sites for further details.

=====================

LAM - LDAP Account Manager
=====================

LDAP Account Manager (LAM) is a web frontend for managing entries in an LDAP directory, such as users and groups.

Homepage: https://www.ldap-account-manager.org

Demo: https://www.ldap-account-manager.org/lamcms/liveDemo

License: GNU General Public License (version 2)

------------------------

Webmin

------------------------

Webmin is a web-based interface for system administration for Unix.

Homepage: http:SSLLAASS:HHwww.webmin.com

Demo: http:SSLLAASS:HHwww.webmin.com/demo.html

License: BSD-like license