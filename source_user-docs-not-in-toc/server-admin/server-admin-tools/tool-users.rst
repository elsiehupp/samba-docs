Adding users with samba tool
    <namespace>0</namespace>
<last_edited>2017-06-11T08:19:49Z</last_edited>
<last_editor>Hortimech</last_editor>

Adding Users into Samba Active Directory

.. warning:

   When running Samba 4 as an Active Directory domain, unlike Samba 3, you cannot have a local Unix user for each Samba user that is created.

There are a couple of ways you can create AD users with samba-tool:

* You can create users that are only Windows users.

* You can creates users that are Windows and Unix users.

=================
Creating a Windows user
=================

To create an Windows user, use the following command:

 /usr/local/samba/bin/samba-tool user create user1

You can, if you so wish, add further options:

{| class="wikitable"
!Option
!Explanation
|-
|``--given-name``
|User's given name.
|-
|``--surname``
|User's surname.
|-
|``--initials``
|User's initials.
|-
|``--userou``
|DN of alternative location (without domainDN counterpart) to default CN=Users in which new user object will be created. E. g. 'OU=<OU name>.
|-
|``--profile-path``
|User's profile path.
|-
|``--script-path``
|User's logon script path.
|-
|``--home-drive``
|User's home drive letter.
|-
|``--home-directory``
|User's home directory path.
|}

=================
Creating a Windows and Unix user
=================

.. note:

    All Active Directory users are primarily Windows users, you need to extend them with further attributes, to make them Unix users as well.

To create a Unix user, you use the same command used to create a Windows, but you must add these options:

{| class="wikitable"
!Option
!Explanation
|-
|``--nis-domain``
|User's Unix/RFC2307 NIS domain.
|-
|``--uid-number``
|User's Unix/RFC2307 numeric UID.
|-
|``--login-shell``
|User's Unix/RFC2307 login shell.
|-
|``--unix-home``
|User's Unix/RFC2307 home directory.
|-
|``--gid-number``
|User's Unix/RFC2307 primary GID number.
|-
|}

There are two other options available, if you do not supply these, they will be set for you:

{| class="wikitable"
!Option
!Explanation
|-
|``--uid``
|User's Unix/RFC2307 username.
|-
|``--gecos``
|User's Unix/RFC2307 GECOS field.
|}

=================
Further options available
=================

There are several other options available, these are applicable whether you are creating a Windows or a Unix user:

{| class="wikitable"
!Option
!Explanation
|-
|``--must-change-at-next-login``
|Force password to be changed on next login.
|-
|``--use-username-as-cn``
|Force use of username as user's CN.
|-
|``--smartcard-required``
|Require a smartcard for interactive logons.
|-
|``--job-title``
|User's job title.
|-
|``--department``
|User's department.
|-
|``--company``
|User's company.
|-
|``--description``
|User's description.
|-
|``--mail-address``
|User's email address.
|-
|``--internet-address``
|User's home page.
|-
|``--telephone-number``
|User's phone number.
|-
|``--physical-delivery-office``
|User's office location.
|-
|}

=================
Adding Unix attributes to a Windows user
=================

If you create a Windows user and then need to make it a Unix user as well, you can do it this way:

Find the SID allocated to the user by using the following command on a Samba DC:

 $ /usr/local/samba/bin/wbinfo --name-to-sid user1

It should display something like this:

 S-1-5-21-4036476082-4153129556-3089177936-1005 SID_USER (1)

Now use this SID to find the users 'xidNumber' : 

 $ /usr/local/samba/bin/wbinfo --sid-to-uid S-1-5-21-4036476082-4153129556-3089177936-1005

Again, the command should return something like this:

 3000011

This user is mapped to the ID '3000011', this is only valid on a DC. If your user will only connect to files stored the DC, you can stop here.

If your user will connect to a Unix domain member and you will be using the winbind 'ad' backend, you must add 'uidNumber' and 'gidNumber' attributes to the users object in /usr/local/samba/private/sam.ldb

One way of doing this, is with the Samba tool 'ldbedit:

 $ ldbedit -e emacs -H /usr/local/samba/private/sam.ldb objectsid=S-1-5-21-4036476082-4153129556-3089177936-1005

.. note:

    You can replace emacs with your editor of choice.

Find the record for 'user1', it will start like this:

 dn: CN=user1,CN=Users,DC=samdom,DC=example,DC=com
 objectClass: top
 objectClass: person
 objectClass: organizationalPerson
 objectClass: user
 cn: user1

If you add attributes similar to these:

 uidNumber: 10000
 gidNumber: 10000

Then save and exit, Samba will then use ID '10000' for the users Unix ID and the group ID '10000'. Before Samba 4.6.0, you will also have to give 'Domain Users' the 'gidNumber' '10000', but from 4.6.0, you can use the 'gidNumber' for any Unix group you have created in AD and this wil become the users primary Unix group.