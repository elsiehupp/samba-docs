Samba-tool ldapcmp
    <namespace>0</namespace>
<last_edited>2015-07-29T21:12:13Z</last_edited>
<last_editor>AceSlash</last_editor>

===============================

Introduction
===============================

samba-tool provides a subcommand for testing LDAP replication between Domain Controllers - regardless if they are running Samba or Windows or mixed.

=================
Restrictions
=================

* The comparisation works via LDAP. So the LDAP server must be up and accessible at port 389.

* It compares values of attributes of objects returned only by wild-card search so no hidden attributes are processed.

* Certain attributes are explicitly ignored, these are non replicating attributes and they always have different values on the corresponding objects in separate DCs.

=================
How to use?
=================

* Compare the entire directory on Domain Controller DC1 and DC2:

 # samba-tool ldapcmp ldap://DC1 ldap:SSLLAASS:HHDC2 -Uadministrator

* Compare single AD partitions on Domain Controller DC1 and DC2:

 # samba-tool ldapcmp ldap://DC1 ldap:SSLLAASS:HHDC2 -Uadministrator domain
 # samba-tool ldapcmp ldap://DC1 ldap:SSLLAASS:HHDC2 -Uadministrator configuration
 # samba-tool ldapcmp ldap://DC1 ldap:SSLLAASS:HHDC2 -Uadministrator schema
 # samba-tool ldapcmp ldap://DC1 ldap:SSLLAASS:HHDC2 -Uadministrator dnsdomain
 # samba-tool ldapcmp ldap://DC1 ldap:SSLLAASS:HHDC2 -Uadministrator dnsforest

=================
Filter non-synced attributes
=================

* Some attribute are [https://lists.samba.org/archive/samba/2012-September/169136.html known to be different between dc], you can filter them out with the "--filter" switch

* Example without any filter on a perfectly working replication:

 **# samba-tool ldapcmp ldap://DC1 ldap:SSLLAASS:HHDC2 -Uadministrator domain**
    assword for [SAMDOM\administrator]:

 * Comparing [DOMAIN] context...

 * Objects to be compared: 308

 Comparing:
 'CN=Builtin,DC=samdom,DC=example,DC=com' [ldap://DC1]
 'CN=Builtin,DC=samdom,DC=example,DC=com' [ldap://DC2]
     Attributes found only in ldap://DC1::
         serverState
     FAILED

 Comparing:
 'DC=samdom,DC=example,DC=com' [ldap://DC1]
 'DC=samdom,DC=example,DC=com' [ldap://DC2]
     Attributes found only in ldap://DC1::
         serverState
         msDS-NcType
     FAILED

 * Result for [DOMAIN]: FAILURE

 SUMMARY
 ---------

 Attributes found only in ldap://DC1::

     msDS-NcType
     serverState
 ERROR: Compare failed: :1

* And with the filter:

 **# samba-tool ldapcmp ldap://DC1 ldap:SSLLAASS:HHDC2 -Uadministrator domain --filter=msDS-NcType,serverState**
 Password for [SAMDOM\administrator]:

 * Comparing [DOMAIN] context...

 * Objects to be compared: 308

 * Result for [DOMAIN]: SUCCESS

* An other attribute is not synced in the "CONFIGURATION" context: **subrefs**, so the correct check with the filter for the whole setup is::

 **# samba-tool ldapcmp ldap://DC1 ldap:SSLLAASS:HHDC2 -Uadministrator --filter=msDS-NcType,serverState,subrefs**