Samba-tool-external
    <namespace>0</namespace>
<last_edited>2011-10-10T18:32:35Z</last_edited>
<last_editor>TheresaHalloran</or>
This wiki page documents the current externals of the samba-tool command in the first table below and proposed externals to the samba-tool command in the second table below.  The purpose of the proposed changes is to make the samba-tool command more consistent and easier to use.  Additionally, help for command completion will be provided in a more consistent manner, again for usability.

**Current commands listed in __init__. in samba 4 Version 4.0.0a.-.a6433**

<h4>samba-tool current commands</  
<table border="1">
<caption>**samba-tool current commands</t;
<tr>
    td>**Ref Num**</
    td>**Subcommand**</
    td>**Description**</
    td>**Parameters**</
    td>**Command specific options**</
    td>**Net command**</
</   
<tr>
    td rowspan="3">1</
    td rowspan="3">acl</
    td rowspan="3">get or set acls on a file</
    td>nt get <file></
    td>--as-sddl
--xattr\-backend=native|tdb
--eadb-file=<file></
    td></
</
<tr>
    td>nt set <file></
    td>--quiet=
--xattr-backend=native|tdb
--eadb-file=<file></
    td></
</
<tr>
    td>ds set <file></
    td>--host=
--car=...ion=allow|deny
--objectdn=
--trusteedn=
--sddl=
--eadb-file=<file></
    td></
</
<tr>
    td rowspan="2">2</
    td rowspan="2">domainlevel</
    td rowspan="2">Raises domain and forest function level</
    td>show</
    td rowspan="2">-H
--quiet
--forest=2003|2008|2008_R2
--domain=2003|2008|2008_R2</
    td rowspan="2"></
</
<tr>
    td>raise</
</
<tr>
    td rowspan="4">3</
    td rowspan="4">drs</
    td rowspan="4">various directory replication services</
    td>bind <dc></
    td></
    td rowspan="4"></
</
<tr>
    td>kcc <dc></
    td></
</
<tr>
    td>replicate <dest_dc> <source_dc> <nc></
    td>--add-ref
--sync-force</
</
<tr>
    td>showrepl <dc></
    td></
</
<tr>
    td>4</
    td>enableaccount</
    td>enable a user</
    td><username></
    td>--filter=</
    td></
</
<tr>
    td>5</
    td>export</
    td>Dumps kerberos keys of the domain into a keytab</
    td>keytab <keytab></
    td></
    td>net export keytab <keytab></
</
<tr>
    td rowspan="3">6</
    td rowspan="3">fsmo</
    td rowspan="3">Makes the target DC transfer or seize fsmo role (server connection needed)
transfer: request the role from current owner
seize:  the role by force, current master is dead</
    td>show</
    td>--url
--force
--role=rid|pdc|infrastructure|schema|naming|all</
    td></
</
<tr>
    td>transfer</
    td>--url
--force
--role=rid|pdc|infrastructure|schema|naming|all</
    td></
</
<tr>
    td>seize</
    td>--url
--force
--role=rid|pdc|infrastructure|schema|naming|all</
    td></
</
<tr>
    td rowspan="4">7</
    td rowspan="4">group</
    td rowspan="4">Add or delete groups or add members to or remove members from a group</
    td>add <groupname></
    td>-H
--groupou=
--group-type=Security|Distribution
--description=
--mail-address=
--notest=</
    td rowspan="4"></
</
<tr>
    td>delete <groupname></
    td>-H</
</
<tr>
    td>addmembers <groupname> <listofmembers></
    td>-H
</
</
<tr>
    td>removemembers <groupname> <listofmembers></
    td>-H
</
</
<tr>
    td rowspan="2">8</
    td rowspan="2">gpo2</
    td rowspan="2">List group policies</
    td>list <username></
    td rowspan="2">-H</
    td rowspan="2"></
</
<tr>
    td>listall</
</
<tr>
    td rowspan="3">9</
    td rowspan="3">join</
    td rowspan="3">Join a domain as either a member or a backup domain controller 
(server connection required)</
    td><dnsdomain> DC</
    td rowspan="3">--server=
--site=</
    td rowspan="3"></
</
<tr>
    td><dnsdomain> RODC</
</
<tr>
    td><dnsdomain> MEMBER</
</
<tr>
    td>10</
    td>ldapcmp</
    td>compare two ldap databases</
    td><url1> <url2> <context1?> <context2?> <context3?></
    td>--two
--quiet
--verbose
--sd
--sort-aces
--view
--base
--base2
--scope</
    td></
</
<tr>
    td>11</
    td>machinepw</
    td>get machine PW out of SAM</
    td><accountname></
    td></
    td>net machinepw <accountname></
</
<tr>
    td>12</
    td>newuser</
    td>Create a new user</
    td><username> <password?></
    td>-H
--must-change-at_next-login
--user-username-as-cn<br.rou
--surname
--given-name
--initials
--profile-path
--script-path
--home-drive
--home-directory
--job-title
--department
--company
--description
--mail-address
--internet-address
--telephone-number
--physical-delivery-office</
    td></
</
<tr>
    td rowspan="2">13</
    td rowspan="2">pwsettings</
    td rowspan="2">Sets password settings</
    td>set</
    td>-H
--quiet
--complexity=on|off|default
--store-plaintext=on|off|default
--history-length=
--min-pwd-length=
--min-pwd-age=
--max-pwd-age=</
    td rowspan="2"></
</
<tr>
    td>show</
    td></
</
<tr>
    td rowspan="2">14</
    td rowspan="2">password</
    td rowspan="2">set or change password, </
    td>set <username> <password></
    td></
    td rowspan="2"></
</
<tr>
    td>change</
    td></
</
<tr>
    td>15</
    td>setexpiry</
    td>Sets the expiration of a user account</
    td><username></
    td>-H
--filter
--days=
--noexpiry</
    td></
</
<tr>
    td>16</
    td>setpassword</
    td>set user password locally, need write access to ldb files</
    td><username?></
    td>-H
--filter
--newpassword
--must-change-at-next-login</
    td></
</
<tr>
    td>17</
    td>time</
    td>Retrieve the time on a remote server (server connection needed)</
    td><servername?></
    td></
    td>net time <servername></
</
<tr>
    td rowspan="2">18</
    td rowspan="2">user</
    td rowspan="2">create or delete a user</
    td>add <username> <password?></
    td></
    td rowspan="2"></
</
<tr>
    td>delete <username></
    td></
</
<tr>
    td>19</
    td>vampire</
    td>Join and synchronise a remote AD domain to the local server
(server connection needed)</
    td>domain</
    td></
    td></
</
</

General options are options that can be used on all commands and are as follows:
<ul>
<li>**Samba Options**</
<ul>
<li> list samba options here***</
</
<li>**Version Options</
<ul>
<li>-V</
<li>--version</
</
<li>**Credential Options</
<ul>
<li>list cred options***</
</
</

Also possibly open for discussion is the formats of some of the global options.  Improvements for improved usability should be considered..

<h4>**samba-tool proposal for command syntax changes**</  
The proposed format for all new /Eexisting functions on the samba-tool command are as follows:
Where is makes sense and is possible, the command syntax will follow the format:
**samba-tool** <object> <action> <parameter(s)> <command specific options> <global options>

Also, help will be improved and made consistent.

<ul>
<li>When the samba-tool command is issued without a subcommand, it will return a list of valid subcommands (it does this today)</
<li>After each subcommand is entered, if more parameters are required a list of what comes next will be shown (sometimes does this today)</
<li>If the command syntax is completely incorrect, will give the format of the subcommand (sometimes does this today)</ 
<li>For each subcommand, help will be provided</
<li>Error handling will be improved, more errors will be caught with useable messages being issued where applicable</
<li>Would a --verbose option make sense on all the commands? consider when implementing (some commands have it today)</
</

<table border="1">
<caption>**samba-tool command proposed syntax changes**</t;
<tr>
    td>**Ref num from previous table**</
    td>**Object**</
    td>**Action**</
    td>**Parameters**</
    td>**Specific Options**</
    td>**Global Options**</
    td>**Comments and Equivalent net command (samba 3)**</
</   
<tr>
    td></
    td>dbcheck</
    td></
    td><DN></
    td></
    td></
    td>should this be db <sp> check?</
</
<tr>
    td rowspan="5"></
    td rowspan="5">delegation</
    td>add-service</
    td rowspan="5"><accountname></
    td rowspan="2"><principal></
    td rowspan="5">Global options</
    td></
</
<tr>
    td>del-service</
    td></
</
<tr>
    td>for-any-protocol</
    td rowspan="2">on | off</
    td></
</
<tr>
    td>for-any-service</
    td></
</
<tr>
    td>show</
    td></
    td></
</
<tr>
    td rowspan="8">2,5,9,11,13</
    td rowspan="8">domain</
    td rowspan="2">level</
    td>show</
    td></
    td>global options</
    td></
</
<tr>
    td>raise</
    td>-H
--quiet
--forest
--domain</
    td>global options</
    td></
</
<tr>
    td>join</
    td><dnsdomain> DC|RODC|MEMBER</
    td>--server=
--site=</
    td>global options</
    td></
</
<tr>
    td>exportkeytab</
    td><keytab></
    td></
    td>global options</
    td></
</
<tr>
    td>machinepassword</
    td><accountname></
    td></
    td>global options</
    td></
</
<tr>
    td rowspan="2">passwordsettings</
    td>show</
    td></
    td rowspan="2">global options</
    td rowspan="2"></
</
<tr>
    td>set</
    td>-H
--quiet
--complexity=on|off|default
--store-plaintext=on|off|default
--history-length=
--min-pwd-length=
--min-pwd-age=
--max-pwd-age=</
</
<tr>
    td>samba3upgrade</
    td><samba3 smb conf></
    td></
    td>global options</
    td></
</
<tr>
    td rowspan="5">3</
    td rowspan="5">drs</
    td>bind</
    td><dc></
    td></
    td>global options</
    td></
</
<tr>
    td>kcc</
    td><dc></
    td></
    td>global options</
    td></
</
<tr>
    td>replicate</
    td><dest_dc> <source_dc> <nc></
    td>--add-ref
--sync-force</
    td>global options</
    td></
</
<tr>
    td>showrepl</
    td><dc></
    td></
    td>global options</
    td></
</
<tr>
    td>options</
    td><dc></
    td>--dsa-option=+|-IS_GC |
--dsa-option=+|-DISABLE_INBOUND_REPL
--dsa-option=+|-DISABLE_OUTBOUND_REPL
--dsa-option=+|-DISABLE_NTDSCONN_XLATE</
    td>global options</
    td></
</
<tr>
    td>1</
    td>dsacl</
    td>set</
    td><file></
    td>--objectdn=objectdn
--car=control right
--action=deny|allow
--trusteedn=trustee-dn</  
    td>global options</
    td>Could combine set and nt into one action setnt</
</
<tr>
    td rowspan="3">6</
    td rowspan="3">fsmo</
    td>show</
    td rowspan="3"></
    td rowspan="3">--url=
--force
--role=rid|pdc|infrastructure|schema|naming|all</
    td rowspan="3">global options</
    td rowspan="3"></
</
<tr>
    td>transfer</
</
<tr>
    td>seize</
</
<tr>
    td rowspan="2">8</
    td rowspan="2">gpo</
    td>list</
    td></
    td>-H</
    td>global options</
    td></
</
<tr>
    td>listall</
    td></
    td>-H</
    td>global options</
    td></
</
</
    td rowspan="4">7</
    td rowspan="4">group</
    td>create</
    td><groupname></
    td>-H
--groupou=
--group-type=Security|Distribution
--description=
--mail-address=
--notest=</
    td>global options</
    td>change "add" to create
more exact
now we have create/ and 
addmembers/removeme/td>/
</
<tr>
    td>delete</
    td><groupname></
    td>-H</
    td>global options</
    td></
</
<tr>
    td>addmembers</
    td><groupname> <listofmembers></
    td>-H</
    td>global options</
    td></
</
<tr>
    td>removemembers</
    td><groupname> <listofmembers></
    td>-H</
    td>global options</
    td></
</
<tr>
    td>10</
    td>ldap</
    td>compare</
    td><url1> <url2> 
<context1?>
<context2?>
<context3?></
    td>--two
--quiet
--verbose
--sd
--sort-aces
--view
--base
--base2
--scope</
    td>global options</
    td>Change to split into ldap compare.  
Not done yet.<.
</
<tr>
    td rowspan="2">1</
    td rowspan="2">ntacl</
    td>get</
    td><file></
    td>--as-sddl
--xattr-backend=native|tdb
--eadb-file=file</
    td>global options</
    td></
</
<tr>
    td>set</
    td><file></
    td>--xattr-backend=native|tdb
--eadb-file=file</
    td>global options</
    td></
</
<tr>
    td></
    td>rodc</
    td>preload</
    td><SID> | <DN> | <accountname></
    td></
    td>global options</
    td></
</
<tr>
    td rowspan="3"></
    td rowspan="3">spn</
    td>add</
    td rowspan="2"><name></
    td rowspan="2"><user></
    td rowspan="3">global options</
    td rowspan="3"></
</
<tr>
    td>delete</
</
<tr>
    td>list</
    td><user></
    td></
</
<tr>
    td></
    td>testparm</
    td></
    td></
    td></
    td>global options</
    td>Prompts for file name, inconsistent...
</
<tr>
    td>17</
    td>time</
    td></
    td><servername?></
    td></
    td>global options</
    td></
</
<tr>
    td rowspan="10">4,12,14,15</
    td rowspan="10">user</
    td>create</
    td><username></
    td>-h,--help
-H URL,--URL=URL
--must-change-at-next-login
etc...
    td>global options</
    td>Changing add to create
The help on this command already says add - create a new user
create makes more sense, add sounds like it already exists and adding it to a group, for instance
opposite of removemembers is addmembers
does this need to support all option supported in the GUI on windows side?</
</
<tr>
    td>delete</
    td><username></
    td></
    td>global options</
    td></
</
<tr>
    td rowspan="4">setexpiry</
    td rowspan="4"><username></
    td>-H help</
    td rowspan="4">global options</
    td rowspan="3">this used to be setexpiry username command</
</
<tr>
    td>--days=int</
</
<tr>
    td>--filter=str</
</
<tr>
    td>--noexpiry</
    td>this might be confusing
--noexpiry changes the password setting to "Never expires"
there is also an account "Never expires" setting which is what I thought this was
the reason I thought this is because the setexpiry --days command sets the account expiration, not the password expiration
--filter needs additional doc.r>the format is --filter=samaccountname=<username>
Also, my understanding is the sam is internal and should not be on the command.<.possibly this parameter should change, as samaccountname is an internal concept, not to be used for an external of a command.<br&gt.nts?
also, I haven't yet figured out the format for second filter parameter
 something like accountexpires=xx (except thats not it!)</
</
<tr>
    td rowspan="2">enable</
    td rowspan="2"><username?></
    td>-H help</
    td rowspan="2">global options</
    td rowspan="2">this used to be enableaccount username command
Do we need a disableaccount as well?
Seems like it should be easy enough to implement.r>--filter needs additional doc
the format is --filter=samaccountname=<username>
Also, my understanding is the sam is internal and should not be on the command.<.possibly this parameter should change, as samaccountname is an internal concept, not to be used for an external of a command.</.
</
<tr>
    td>--filter=str</
</
<tr>
    td>setpassword</
    td><username> <password></
    td>-H
--filter=
--must-change-at-next-login</
    td>global options</
    td>This command combines samba-tool setpassword and samba-tool password set
this password command is intended to admins to set passwords for end users
usually requires admin password for authority
prompts for input if not specified on the command</  
</
<tr>
    td>password</
    td><username> <password></
    td></
    td>global options</
    td>This command is intended for end users to change their password
prompting for input if not specified on the command</
</
<tr>
    td>19</
    td>vampire</
    td></
    td>domain</
    td></
    td>global options</
    td>Keep as vampire command for usability /Ehistorical purposes
Do not change to object action format</td>/
</
</